<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>

<body>
	<%@ include file="../includes/menu.jsp"%>

	<cms:area name="banner" />


	<!--/.Banner-->
	<section class="container sub-nav">
		<cms:area name="navigationArea" />
	</section>

	<section class="container">
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1 txt-center">
				<div class="contentWrap">
					<cms:area name="narrowRowArea" />
				</div>
			</div>
		</div>
	</section>

	
	
	<cms:area name="wideRowArea" />

	<%@ include file="../includes/footer.jsp"%>


</body>
</html>