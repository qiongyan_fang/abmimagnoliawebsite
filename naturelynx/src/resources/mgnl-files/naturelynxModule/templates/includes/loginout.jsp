<div class="sy-user-modal"> <!-- this is the entire modal form, including the background -->
		<div class="sy-user-modal-container"> <!-- this is the container wrapper -->
			<ul class="sy-switcher">
				<li><a href="javascript:void(0);" class="selected">Sign in</a></li>
				<li><a href="javascript:void(0);">Sign Up</a></li>
			</ul>

			<div id="sy-login" class="is-selected"> <!-- log in form -->
				<form class="sy-form row">
					<div class="col-xs-12 fieldset">
						<input type="text" id="username" placeholder="Username">
						<span class="sy-error-message sy-user-msg">Error message here!</span><!--Add "is-visible" class to show erroe message-->
					</div>

					<div class="col-xs-12 fieldset">
						<input type="password" id="password" placeholder="Password">
						<span class="sy-error-message sy-pwd-msg">Error message here!</span>
					</div>

					<div class="col-xs-12 fieldset">
						<div class="row">
							<div class="col-xs-6">
								<input type="checkbox" checked="" id="remember-me">
								<label for="remember-me">Remember me</label>
							</div>
							<div class="col-xs-6">
								<a class="forgot-password" href="#">Forgot your password?</a>
							</div>
						</div>
					</div>

					<div class="col-xs-12 txt-center fieldset">
						<input id="signin" type="button" value="SIGN IN" class="btn dark-btn">
					</div>
				</form>
				
			</div> <!-- login -->

			<div id="sy-signup"> <!-- sign up form -->
				<form class="sy-form row">
					<div class="col-xs-12 fieldset">
						<input type="text" id="su_username" placeholder="Username">
						<span class="sy-error-message su-username-msg">Error message here!</span>
					</div>

					<div class="col-xs-12 fieldset">
						<input type="email" id="su_email"  placeholder="E-mail">
						<span class="sy-error-message su-email-msg">Error message here!</span>
					</div>

					<div class="col-xs-12 fieldset">
						<input type="text"  id="su_location" placeholder="Location">
						<span class="sy-error-message su-location-msg">Error message here!</span>
					</div>
					
					<div class="col-xs-12 fieldset">
						<input type="password" id="su_password" placeholder="Password">
						<span class="sy-error-message su-password-msg">Error message here!</span>
					</div>
					
					<div class="col-xs-12 fieldset">
						<input type="password" id="su_password2" placeholder="Confirm Password">
						<span class="sy-error-message su-password2-msg">Error message here!</span>
					</div>

					<div class="col-xs-12 fieldset">
						<input type="checkbox" id="su_term" id="accept-terms">
						<label for="accept-terms">I agree to the <a href="#">Terms</a></label>
						<span class="sy-error-message su-term-msg">Error message here!</span>
					</div>

					<div class="col-xs-12 txt-center fieldset">
						<input type="button" id="signup" value="SIGN UP" class="btn dark-btn">
					</div>
					<div class="col-xs-12 txt-center fieldset" >
					<div class="sy-error-message su-msg">
					</div>
						
					</div>
				</form>

			</div> <!-- signup -->

			<div id="sy-reset-password"> <!-- reset password form -->
				<p class="sy-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

				<form class="sy-form row">
					<div class="col-xs-12 fieldset">
						<input type="email" placeholder="E-mail" id="reset-email">
						<span class="sy-error-message reset-password-msg"></span>
					</div>

					<div class="col-xs-12 txt-center fieldset">
						<input type="button" id="resetpwd" value="Reset password" class="btn dark-btn">
					</div>
					
					<div class="col-xs-12 txt-center fieldset">
						<a class="back-to-login" href="javascript:void(0);">Back to Sign in</a>
					</div>
				</form>
			</div> <!-- reset-password -->
			
			<a class="sy-close-form" href="javascript:void(0);">Close</a>
			
		</div> <!-- user-modal-container -->
	</div>