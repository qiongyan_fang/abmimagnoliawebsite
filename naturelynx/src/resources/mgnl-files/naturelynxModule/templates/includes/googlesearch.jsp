
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<div>
	<div id='cse' style='z-index: 1000; width: 100%; display: none;'>Loading</div>
	<script>
		$(function() {

			// Handler for .ready() called.
			$("#gsc-i-id1").change(function() {

				$("#q").val($("#gsc-i-id1").val());

			});
		});

		function openSearch() {
			try {

				$("#gsc-i-id1").val($("#q").val());

				$(".gsc-search-button").click();

				if ($("#cse").dialog("option", "width")) {
					$("#cse").dialog('open');
				} else {
					$("#cse").dialog({
						height : 600,
						width : 800,
						title : "Search ABMI.ca"
					});
				}
			} catch (e) {
				$("#cse").dialog({
					height : 600,
					width : 800,
					title : "Search ABMI.ca"
				});
			}
		}
	</script>
	<script src='http://www.google.com/jsapi' type='text/javascript'></script>
	<script type='text/javascript'>
		google.load('search', '1', {
			language : 'en',
			style : google.loader.themes.MINIMALIST
		});
		google
				.setOnLoadCallback(
						function() {
							var customSearchOptions = {};
							// customSearchOptions['overlayResults'] = true;
							var customSearchControl = new google.search.CustomSearchControl(
									'006207892962066587082:sakmm7npo-g',
									customSearchOptions);
							customSearchControl
									.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
							customSearchControl
									.setLinkTarget(GSearch.LINK_TARGET_PARENT);
							var options = new google.search.DrawOptions();
							customSearchControl.draw('cse', options);
						}, true);
	</script>

</div>


<div class="search hidden-sm hidden-xs">
	<div class="push-right">
		<input type="text" placeholder="Search" id="q" name="q"> <input
			type="submit" class="btn" onclick="javascript:openSearch();" value=""
			style="height: 65px; float: right; position: relative; background: url('assets/search.png') no-repeat scroll right 15px center rgb(103, 117, 35)" />

	</div>

</div>