<header class="row-fluid">
		<nav class="navbar navbar-default" role="navigation">
		<div class="logo"><a href="/home.html"></a></div>

<div class="search hidden-sm hidden-xs">
<div class="push-right"><input type="text" style="height:65px;width:210px;" id="q"  name="q">
 

</div>

</div>


    

		
		<div class="top-bar hidden-sm hidden-xs">
			<div class="container hidden-xs hidden-sm">
				<ul>
					<li><a href="/home/projects/applied-research-projects.html">Projects</a></li>
					<li><a href="/home/publications.html">Publications</a></li>
					<li><a href="/home/products-services.html">Products &amp; Services</a></li>
					<li><a href="/home/land-access.html">Land Access</a></li>
					<li><a href="/home/careers.html">Careers</a></li>
					<li><a href="/home/news.html">News</a></li>
				</ul>
			</div>
		</div>
		  <div class="container nav-container">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-menu">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		    </div>
		    <div class="collapse navbar-collapse" id="main-menu">
				<ul class="nav navbar-nav navbar-right">
					
					<li class="li-search-collapse hidden-md hidden-lg">
						<div class="search-collapse">
							<input type="text" placeholder="Search"><div class="search-right"></div>
						</div>
					</li>
					
					<li class="biodiversity"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Biodiversity?</a>
						<ul class="dropdown-menu">
							<li>
									<div class="visible-lg">
										<img src="/docroot/assets/menu_wolf.jpg" width="210" height="190" alt="Menu Wolf">
									</div>
				                    <ul class="list-unstyled">
				                        <li><a href="/home/biodiversity/what-is-biodiversity.html">What is Biodiversity?</a></li>
				                        <li><a href="/home/biodiversity/biodiversity-in-alberta.html">Biodiversity in Alberta</a></li>
				                        <li><a href="/home/biodiversity/healthy-ecosystems">Healthy Ecosystems</a></li>
										<li><a href="/home/biodiversity/you-manage-what-you-measure.html">You Manage What You Measure</a></li> 
				                    </ul>
				                    <ul class="list-unstyled">
				                        <li><a href="/home/biodiversity/better-information-decisions.html">Better Information = Better Decisions</a></li>
				                        <li><a href="/home/biodiversity/biodiversity-climate-change.html">Biodiversity and Climate Change</a></li>
				                    </ul>
				            </li>
				          </ul>
					</li>
					<li class="about"><a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret hidden-lg"></b></a>
						<ul class="dropdown-menu">
							<li>
									<div class="visible-lg">
										<img src="/docroot/assets/menu_hill.jpg" width="210" height="190" alt="Menu Hill">
									</div>
				                    <ul class="list-unstyled">
				                        <li><a href="/home/about-us/executive-message.html">Executive Message</a></li>
				                        <li><a href="/home/about-us1/board-of-directors.html">Board of Directors</a></li>
				                        <li><a href="/home/about-us/our-mission-values.html">Our Mission &amp; Values</a></li>
										<li><a href="/home/about-us/benefits-of-ABMI.html">Benefits of the ABMI</a></li> 
				                   
				                        <li><a href="/home/about-us/our-history.html">Our History</a></li>
				                        <li><a href="/home/about-us/governance-funding.html">Governance &amp; Funding</a></li>
				                    </ul>
				                    <ul class="list-unstyled">
				                        <li><a href="/home/about-us/organization-centres.html">Organization &amp; Centres</a></li>
				                        <li><a href="/home/about-us/abmi-management-team.html">ABMI Management Team</a></li>
										<li><a href="/home/about-us/request-for-proposals.html">Request For Proposals</a></li>
										<li><a href="/home/about-us/international-science-committee.html">International Science Committee</a>
										<li><a href="/home/about-us1/our-partners-sponsors/partners.html">Our Partners & Sponsors</a></li>
										<!-- 
										<li><a href="/home/about-us/abmi-management-team.html">ABMI Management Team</a></li>
				                    -->
				                     </ul>
				                   
				            </li>
				          </ul>
					</li>
					<li class="whatwedo"><a href="#" class="dropdown-toggle" data-toggle="dropdown">What We Do</a>
						<ul class="dropdown-menu">
							<li>
									<div class="visible-lg">
										<img src="/docroot/assets/menu_marsh.jpg" width="210" height="190" alt="Menu Marsh">
									</div>
				                    <ul class="list-unstyled">
										 <li><a href="/home/overview.html">Overview</a></li>				                      
										  <li><a href="/home/applied-research-projects.html">Applied Research Projects</a></li>
				                        <li><a href="/home/what-we-do/communications-data-management.html">Communications & Data Management</a></li>
				                        <li><a href="/home/what-we-do/geomatics-human-footprint.html">Geomatics & Human Footprint</a></li>
										
				                    
				                        <li><a href="/home/what-we-do/monitoring.html">Monitoring</a></li>
				                        <li><a href="/home/what-we-do/scientific-analysis.html">Scientific Analysis</a></li>
				                        </ul>
				                    <ul class="list-unstyled">
				                        
											<li><a href="/home/what-we-do/taxonomy.html">Taxonomy</a></li>
											<li><a href="/home/what-we-do/carioub-monitoring.html">Carioub Monitoring</a></li>
				                        <li><a href="/home/what-we-do/stakeholder-engagement.html">Stakeholder Engagement</a></li>
									
											<li><a href="/home/what-we-do/joint-monitoring-collaborations.html">Joint Monitoring Collaborations</a></li>
											<li><a href="/home/news.html">Events</a></li> 
											<li><a href="/home/events.html">Events</a></li>	
				                    </ul>
				            </li>
				          </ul>
					</li>
					<li class="ourdata"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Our Data</a>
						<ul class="dropdown-menu">
							<li>
									<div class="visible-lg">
										<img src="/docroot/assets/menu_deer.jpg" width="210" height="190" alt="Menu deer">
									</div>
				                    <ul class="list-unstyled">
				                        <li><a href="#">Map Portal</a></li>
				                        <li><a href="#">Raw Data</a></li>
				                        <li><a href="#">Intactness Analysis</a></li>
										<li><a href="#">Protocols</a></li> 
				                    </ul>
				                    <ul class="list-unstyled">
				                        <li><a href="/home/our-data/site-surveyed.html">Sites Surveyed</a></li>
				                        <li><a href="/home/our-data/survey-site-locaitons.html">Survey Site Locations</a></li>
				                    </ul>
				            </li>
				          </ul>
					</li>
					<!-- hidde for initial launch
					<li class="naturelynx"><a href="#" class="dropdown-toggle" data-toggle="dropdown">NatureLYNX</a>
						<ul class="dropdown-menu">
							<li>
									<div class="visible-lg">
										<img src="/docroot/assets/menu_studying.jpg" width="210" height="190" alt="Menu Studying">
									</div>
				                    <ul class="list-unstyled">
				                        <li><a href="#">My NatureLynx</a></li>
				                        <li><a href="#">About</a></li>
				                        <li><a href="#">Explore Map</a></li>
										<li><a href="#">Groups</a></li> 
				                    </ul>
				                    <ul class="list-unstyled">
				                        <li><a href="#">Learn</a></li>
				                        <li><a href="#">Logout</a></li>
				                    </ul>
				            </li>
				          </ul>
					</li>
					 -->
					<li class="contact"><a href="/home/contact-us.html" class="dropdown-toggle" data-toggle="dropdown">Contact Us</a>
						<ul class="dropdown-menu">
							<li>
									<div class="visible-lg">
										<img src="/docroot/assets/menu_chopper.jpg" width="210" height="190" alt="Menu Chopper">
									</div>
				                    <ul class="list-unstyled">
				                        <li><a href="/home/contact-us/abmi-directory.html">ABMI Directory</a></li>
				                        <li><a href="/home/contact-us/media-inquiries.html">Media Inquiries</a></li>
				                    </ul>
				            </li>
				          </ul>
					</li>
					<li class="hidden-md hidden-lg"><a href="/home/projects/applied-research-projects.html">Projects</a></li>
					<li class="hidden-md hidden-lg"><a href="/home/publications.html">Publications</a></li>
					<li class="hidden-md hidden-lg"><a href="/home/products-services.html">Products &amp; Services</a></li>
					<li class="hidden-md hidden-lg"><a href="/home/land-access.html">Land Access</a></li>
					<li class="hidden-md hidden-lg"><a href="/home/careers.html">Careers</a></li>
				</ul>
		    </div>
		  </div>
		</nav>
	</header>