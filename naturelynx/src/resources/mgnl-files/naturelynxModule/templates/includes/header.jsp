<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docroot/naturelynx/images/favicon.ico">
	<title>Naturehood | SPOT. SNAP. POST. </title>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="/docroot/naturelynx/css/reset.css" type="text/css" media="screen" />
    
    <link href="/docroot/naturelynx/css/style.css" rel="stylesheet">
    <link href="/docroot/naturelynx/css/naturelynx_abmi_style.css" rel="stylesheet">
    <link rel="stylesheet" href="/docroot/naturelynx/css/flexslider.css" type="text/css" media="screen" />
    
 	<link href="/docroot/naturelynx/css/naturelynx_googlesearch.css" rel="stylesheet">
 
 	<link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet"> 
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
     <!--JavaScript -->
 
    <script src="/docroot/naturelynx/js/script.js"></script>
  </head>
    <cms:init />