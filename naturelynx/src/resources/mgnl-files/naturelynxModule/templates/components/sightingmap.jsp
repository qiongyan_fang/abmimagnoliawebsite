<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>

<html>

<head>

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" href="/docroot/naturelynx/images/favicon.ico">

    <title>NatureLynx | SPOT. SNAP. POST. </title>

    <link rel="stylesheet" href="/docroot//naturelynx/css/naturelynx.css">
    <link href="/docroot/naturelynx/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="http://leafletjs.com/dist/leaflet.css" />
	
	<link rel="stylesheet" href="http://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.css" />
	<link rel="stylesheet" href="http://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.Default.css" />	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script src="http://leafletjs.com/dist/leaflet.js"></script>

	<style>
	body {

			padding: 0;

			margin: 0;

	}

	html, body, .wrapper, #map {

		height: 95.5%;

	}
	</style>

</head>

  <body class="top-menu-temp">

    <div class="app-nav">
    <!--Top Menu-->
    	<script type="text/javascript" src="/docroot/naturelynx/js/top.js"></script>
    <!--/.Top Menu-->
    </div>
    
    <!--/.navigation -->
<div class="wrapper">
				<input type="text" placeholder="Search here" id="species_name" class="search-box filterinput"  autocomplete="off">
				<input type="hidden" name="speciesId" id="speciesId" value="" />
				<!--                 
                <div class="search-result">
                	<ol id="contents">
                	<li class="result-group selected">
                    	<a href="#">
                            <div class="col-xs-6 txt-left">Brown Beaver</div>
                            <div class="col-xs-6 txt-right"><span class="total-sightings">17</span>Sightings</div>
                        </a>
                    </li>
                    <li class="result-group">
                    	<a href="#" >
                            <div class="col-xs-6 txt-left">Canadian Beaver</div>
                            <div class="col-xs-6 txt-right"><span class="total-sightings">17</span>Sightings</div>
                        </a>
                    </li>
                    <ol>
                </div>
                -->
            <div id="map" stlye={height: 100%;}></div>
</div>
    <!--Bottom Menu-->
    <div class="bottom-menu">
    
    </div>
	<script src="/docroot/naturelynx/js/script.js"></script>
	<script type="text/javascript" src="/docroot/naturelynx/bootstrap/js/bootstrap-typeahead.js"></script>
	<script type="text/javascript" src="/docroot/naturelynx/js/naturelynx.js"></script>
	<script src="http://leaflet.github.com/Leaflet.markercluster/dist/leaflet.markercluster.js"></script>
	<script>
		var map;
		var sightingGroup;
		var contextPath = '${contextPath}';
		var speciesListURL = contextPath + '/species/type_ahead';
		
		$(function() {
			initMap();
			initSpeciesList(speciesListURL,null,callback);
		});				
		
		var callback=function(sid){
			console.log('sid='+sid);
			
			$.ajax({
		        type: "GET", 
		        url: contextPath + '/sightings_by_species?speciesID='+sid,
		        success: function(data){
		        	sightingGroup.clearLayers();
		        	map.removeLayer(sightingGroup);		        	
		        	//console.log(data);
		        	for(var i = 0 ; i < data.length; i++){
		        		  var sighting = data[i];
			        		var marker = L.marker([sighting.lat, sighting.lon],{title :sighting.title});
			        		marker.data = sighting.id;
			        		console.log(marker);
			        		marker.on('click', function(){
			        			window.location.href = contextPath + "/sighting_profile.html?id="+this.data;
			        		});
			        		sightingGroup.addLayer(marker);
		        	  }
		        	  map.addLayer(sightingGroup);
		        	  map._onResize(); 
		        },
		        error: function(err) {		        	
					//throw new Error('Error occurs in the server. Please contact the web master. :: '+ error);
		        }
		    });		
			
		};

		var initMap=function(){	
			map = L.map('map', {zoomControl: false});
			map.addControl( L.control.zoom({position: 'bottomright'}) );
			sightingGroup = new L.MarkerClusterGroup({ spiderfyOnMaxZoom: true, showCoverageOnHover: true, zoomToBoundsOnClick: true });
			
			/*L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
				maxZoom: 18,
				attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
					'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
					'Imagery � <a href="http://mapbox.com">Mapbox</a>',
				id: 'examples.map-i875mjb7'
			}).addTo(map);*/
			
			L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
				maxZoom: 18,
			   attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(map);

			map.on('locationfound', onLocationFound);
			map.on('locationerror', onLocationError);
			map.locate({setView: true, maxZoom: 16});
		};
		
		var onLocationFound=function (e) {
			var radius = e.accuracy / 2;
			map.panTo(e.latlng);
			//L.marker(e.latlng).addTo(map).bindPopup("You are within " + radius + " meters from this point");
			//L.circle(e.latlng, radius).addTo(map);
		};

		var onLocationError=function (e) {
			alert(e.message);
		};

	</script>

</body>

</html>

