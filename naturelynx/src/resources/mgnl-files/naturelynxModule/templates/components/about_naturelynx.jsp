<%@ include file="../includes/taglibs.jsp"%>

    

	
	<section class="container blue parent">
		<div class="row">
			<div class="col-md-8 col-sm-7 child bg-block" style="background: transparent url(&quot;/docroot/naturelynx/images/naturelynx-app.jpg&quot;) repeat scroll 0% 0%; height: 553px;">
			</div>
			<div class="col-md-4 col-sm-5 padding-lg app-info">
            	<h4 class="mrgBs"> <cms:area name="content-01" /></h4>
            	<cms:area name="content-02" />
            	
            </div>
		</div>
	</section>
	
	<section class="container">
		<div class="">
			<div class="col-lg-10 col-lg-offset-1">
				
				<div class="row pdT">
					<div class="col-md-6 mrgBm">
						<cms:area name="content-11" />
					</div>
					<div class="col-md-6 txt-center">
						<cms:area name="content-12" />
					</div>	
						
				</div>
				<div class="row pdTB">
					<div class="col-md-4 col-sm-5 txt-center mrgBm">
		            		<cms:area name="content-21" />
		            </div>
		            <div class="col-md-8 col-sm-7">
						<cms:area name="content-22" />
					</div>
					
				</div>
			</div>
		</div>
	</section>
	<section class="container parent download-app">
		<div class="row">
			<div class="col-md-6 blue  padding-lg">
					<cms:area name="content-31" />
			</div>
			<div class="col-md-6 txt-center padding-lg">
				<img src="/docroot/naturelynx/images/naturelynx-2colors-logo.png" alt="" class="mrgB2em" style="max-height: 63px;">
            		<cms:area name="content-32" />
            		<div class="mrgTm"><a href="#" class="mrgLRB"><img src="/docroot/naturelynx/images/googleplay-icon.png" alt="google play"></a><a href="#" class="mrgLRB"><img src="/docroot/naturelynx/images\appstore-icon.png" alt=""></a>
	            	</div>
            </div>
		</div>
	</section>
	
	<section class="container">
		<div class="txt-center-md">
			<div class="col-lg-10 col-lg-offset-1">
				<div class="row pdT">
					<div class="col-md-3 mrgBm txt-center">
						<img src="/docroot/naturelynx/images/search-icon-xl.png" alt="" style="max-width:206px" class="i-xl">
					</div>	
					<div class="col-md-9">
							<cms:area name="content-41" />
					</div>	
				</div>
				<div class="row pdT order-sec">
					
					<div class="col-md-3 mrgBm txt-center order-2">
						<img src="/docroot/naturelynx/images/QC.png" alt="" style="max-width: 172px;" class="i-xl">
					</div>	
					<div class="col-md-9 order-1">
							<cms:area name="content-42" />		</div>	
					
				</div>
				<div class="row pdTB">
					<div class="col-md-3 mrgBm txt-center">
						<img src="/docroot/naturelynx/images/ethics.png" alt="" style="max-width:144px" class="i-xl">
					</div>	
					<div class="col-md-9">
							<cms:area name="content-43" />					</div>	
				</div>
			</div>
		</div>
	</section>