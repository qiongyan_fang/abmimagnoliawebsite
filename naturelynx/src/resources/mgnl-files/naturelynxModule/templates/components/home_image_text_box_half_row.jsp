<%@ include file="../includes/taglibs.jsp"%>
<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(contentMap))}" />


<c:if test="${content.textPosition ne 'top'}">
	<div class="col-lg-6 home-half-box-color-background home-box-text-${content.textPosition} childh ${content.backgroundColor}"
		> <!--  setting padding space to 0 on image side -->
		<c:if test="${content.textPosition eq 'right'}">
		<div class="col-sm-6 bg-block childh home-half-box-image"
			style="background:rgba(0, 0, 0, 0) url(${imageUrl}) no-repeat scroll left center / cover; ">
		</div>
		</c:if>
		<div class="col-sm-6  home-half-box-color-background-txt-${content.textPosition}"> <!--  add padding to the right side text -->
			<h3>${content.header}</h3>
			<p>${content.description}</p>

			<a class="btn btn-line"
				${fn:startsWith(content.buttonUrl1, "http")?"target=_blank":""}
				href="${content.buttonUrl1}">${content.buttonText1}</a>
			<c:if test="${not empty content.buttonText2}">
				<a class="btn btn-line"
					${fn:startsWith(content.buttonUrl2, "http")?"target=_blank":""}
					href="${content.buttonUrl2}">${content.buttonText2}</a>
			</c:if>
		</div>
		<c:if test="${content.textPosition eq 'left'}">
		<div class="col-sm-6 bg-block childh home-half-box-image"
			style="background:rgba(0, 0, 0, 0) url(${imageUrl}) no-repeat scroll left center / cover; ">
		</div>
		</c:if>
		

		
	</div>

</c:if>


<c:if test="${content.textPosition eq 'top'}">
	<div class="col-lg-6 txt-${content.textAlign} home-half-box-image-background childh"
		style="background:rgba(0, 0, 0, 0) url(${imageUrl}) no-repeat scroll left center / cover; padding: 0 30px 20px; ">
		<h2>${content.header}</h2>
		<p>${content.description}</p>
		<a class="btn btn-line"
			${fn:startsWith(content.buttonUrl1, "http")?"target=_blank":""}
			href="${content.buttonUrl1}">${content.buttonText1}</a>
	</div>

</c:if>
