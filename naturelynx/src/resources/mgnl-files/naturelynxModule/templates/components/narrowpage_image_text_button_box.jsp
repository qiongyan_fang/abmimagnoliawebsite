<%@ include file="../includes/taglibs.jsp"%>
<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(contentMap))}" />
<section
	class="container ${content.backgroundColor} missions-sec parent white-text">
	<div class="row">
		<c:if test="${content.textPosition eq 'right'}">
			<div
				style="background: transparent url(${imageUrl}) repeat scroll 0% 0%;"
				class="col-md-4 col-sm-5 child bg-block"></div>
		</c:if>
		<div class="col-md-8 col-sm-7">
			<div class="txt-wrap">
				<h2>${content.header}</h2>
				<p>${content.description}</p>



				<div class="btn-aside">
					<a class="btn btn-line"
						${fn:startsWith(content.buttonUrl1, "http")?"target=_blank":""}
						href="${content.buttonUrl1}">${content.buttonText1}</a>

					<c:if test="${not empty content.buttonText2}">
						<a class="btn btn-line"
							${fn:startsWith(content.buttonUrl2, "http")?"target=_blank":""}
							href="${content.buttonUrl2}">${content.buttonText2}</a>
					</c:if>
				</div>
			</div>
		</div>

		<c:if test="${content.textPosition eq 'left'}">
			<div
				style="background: transparent url(${imageUrl}) repeat scroll 0% 0%; height: 671px;"
				class="col-md-4 col-sm-5 child bg-block"></div>
		</c:if>
	</div>
</section>



