<%@ include file="../includes/taglibs.jsp"%>
<script>
var targetUrl = "${content.profileUrl}";
var pageSize  = "${content.pageSize}";
</script>
<div class="row filterList">
	<div class="row stickybar-container">


		<div class="col-md-3 col-sm-4 searchby">
			<div class="stickybar">
				<div class="search-block">
				<form action="">
				<input type="hidden" id="f_search_type" value="profile" />
				<input type="hidden" name="group" 
						value="${param.group}"> 
					<input type="search" class="searchbar" id="f_search_box" name="searchName" value="${param.searchName}"
						placeholder="keyword"> <input class="ct-search-submit"
						type="submit" value="">
						</form>
				</div>
				<ul class="sidenav" id="f-profile-group-count">
				
				</ul>
			</div>
		</div>
		<div class="col-md-9 col-sm-8">
			<!--Filter by Alphabet nav will be displayed here using jquery-listnav.js-->
			<ul id="filterByAlphabet" class="listGroup">
				
			</ul>

		</div>
	</div>
</div>
<div class="row">
	<div class="loadMore">
		<span>Click to Load More</span>
	</div>
</div>
