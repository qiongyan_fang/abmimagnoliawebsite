<%@ include file="../includes/taglibs.jsp"%>
<c:set var="iconMap" value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="iconUrl" value="${cmsfn:link(cmsfn:asContentMap(iconMap))}" />

<c:set var="imageMap" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageMap))}" />

<section class="container story-map-text ${content.backgroundColor}">
	<div class="row">
		<c:if test="${content.imagePosition eq 'left'}">
			<div class="col-lg-4 map-img">
				<img alt="" src="${imageUrl}">
			</div>
		</c:if>
		<div class="col-lg-8 story-map-info">
			<c:if test="${not empty iconUrl}">
				<img class="large-icon" src="${iconUrl}">
			</c:if>
			<h2>${content.header}</h2>
			<p>${content.description}</p>
			<c:if test="${not empty content.buttonText}">
			<a class="btn btn-line"
				${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""}
				href="${content.buttonUrl}">${content.buttonText}</a>
				</c:if>
		</div>
		<c:if test="${content.imagePosition eq 'right'}">
			<div class="col-lg-4 map-img">
				<img alt="" src="${imageUrl}">
			</div>
		</c:if>

	</div>
</section>
