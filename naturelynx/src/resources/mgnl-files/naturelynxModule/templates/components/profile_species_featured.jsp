<%@ include file="../includes/taglibs.jsp"%>
  
		<div class="divider"></div>
			    	<div class="row mrgBs featuredL">
				    	<div class="col-md-6">
					    	<a href="#"><img src="images/sampleImg2.jpg" alt=""></a>
				    	</div>
				    	<div class="col-md-6">
					    	<a href="#"><h3>Featured Species</h3></a>
					    	<h4 class="style2 txt-blue"><a href="#">${sppNode.commonName}</a> | <a href="#">${sppNode.scientificName}</a></h4>
					    	<div class="summary  mrgTxs mrgBxs">
								<span class="pawTotal">74</span><span class="picTotal">74</span>
							</div>
					    	<p>${sppNode.description}</p>
					    	<a href="#" class="btn btn-line dark btn-sm mrgTxs">Learn More</a>
				    	</div>
			    	</div>
