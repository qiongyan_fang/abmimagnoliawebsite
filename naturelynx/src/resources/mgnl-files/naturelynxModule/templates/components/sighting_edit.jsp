<div id="species-edit-area" " class="row">
	<div class="row">
		<div class="col-lg-10 col-lg-offset-1">
			<div class="contentWrap">
				<ol class="breadcrumb">
					<li><a href="javascript:void(0);" class="baclToSightings">Sightings</a>
					</li>
					<li class="active">Add Sighting</li>
				</ol>
				<div class="row">
					<form id="sighting-edit" class="form">
						<div class="col-md-6">
							<div class="mrgBl">
								<div class="imgL">
									<div class="upload-sec">
										<div class="drop-area">
											<input type="file"  id="main_image"
												name="files" class="multi with-preview detail-content"  />
												<span class="sy-error-message files-msg">Error message here!</span>
										</div>
									</div>
								</div>
								<div class="imgThumbs">
									<div class="thumbWrapper">
										<div class="upload-sec">
											<div class="drop-area">
												<input type="file" id="image0"
													name="files" class="multi with-preview detail-content" />
											</div>
										</div>
									</div>
									<div class="thumbWrapper">
										<div class="upload-sec">
											<div class="drop-area">
												<input type="file"  id="image1"
													name="files" class="multi with-preview detail-content" />
											</div>
										</div>
									</div>
									<div class="thumbWrapper">
										<div class="upload-sec">
											<div class="drop-area">
												<input type="file"  id="image2"
													name="files" class="multi with-preview detail-content" />
											</div>
										</div>
									</div>
									<div class="thumbWrapper">
										<div class="upload-sec">
											<div class="drop-area">
												<input type="file"  id="image3"
													name="files" class="multi with-preview detail-content" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="postedBy mrgBs">
								<h6 class="mrgBs">Posted By:</h6>
								<div class="listGroup">
									<div class="listItem">
										<div class="cellImg">
											<a href="#" class="roundImg"> <img class="detail-content"
												id="user_image" src="images/img-2.jpg" alt="" />
											</a>
										</div>
										<div class="cellDetails">
											<div class="name">
												<a href="#" id="user_name" class="detail-content">User
													Name</a>
											</div>
											<span id="location" class="detail-content">Edmonton
												Area</span><span class="lineDiv detail-content"
												id="user_sighting_count">100 Sightings</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">

							<div class="inputGroup">
								<input id="species_name" type="text" name="speciesName"
									placeholder="Species Name..." class="mrgBxs detail-content">
								<span class="sy-error-message speciesName-msg">Error message here!</span>
								<input id="species_id" type="hidden" name="speciesId" value="-93000" />
								<select id="species_group"  name="speciesGroupID"
									class="selectpicker formselect mrgBxs">
									<option value="">Select Species Group...</option>
										
								
									
								</select>
								<span class="sy-error-message speciesGroupID-msg">Error message here!</span>
							</div>
							<div class="inputGroup">
								<div class="inputTitle">
									<label class="mrgBxs">When did this sighting happen?</label>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="select_box mrgBxs">
											<input type="text" name="date" placeholder="Date" id="datepicker"
												class="capture-date detail-content"> <span
												class="caret"></span>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="select_box mrgBxs">
											<div class="input-append bootstrap-timepicker">
												<input id="timepicker" name="time" class="detail-content" type="text"
													placeholder="Time"> <span
													class="add-on capture-time"></span> <span class="caret"></span>
													<span class="sy-error-message time-msg">Error message here!</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="inputGroup">
								<div class="inputTitle">
									<label class="mrgBxs">Where was it located?</label>
								</div>
								<input id="edit_sighting_lat" type="text" name="coordinates_lat"
									placeholder="Enter Lattitude like 47.51623"
									class="mrgBxs detail-content">
									<span class="sy-error-message coordinates_lat-msg">Error message here!</span>
								<input id="edit_sighting_long"  type="text" name="coordinates_long" 
								placeholder=" Enter longitude like -122.02625"
									class="mrgBxs detail-content">
									<span class="sy-error-message coordinates_long-msg">Error message here!</span>
								<div class="inputTitle mrgBm">
									<a href="#" class="mrgBxs"><span class="mapPin lg"></span><span>Drop
											Pin on Map</span></a>
								</div>
							</div>
							<div class="inputGroup">
								<div class="inputTitle">
									<label class="mrgBxs">Does it link to a group or
										mission?</label>
								</div>
								<select id="edit_groups" name="groupID"
									class="selectpicker formselect mrgBxs detail-content">
									<option value="">Select Groups...</option>
									<option value="Group1">Group1</option>
									<option value="Group2">Group1</option>
								</select> <select id="edit_missions" name="missionID"
									class="selectpicker formselect mrgBxs detail-content">
									<option value="">Select Missions...</option>
									<option value="Mission1">Mission1</option>
									<option value="Mission2">Mission2</option>
								</select>
							</div>
							<div class="inputGroup">
								<div class="inputTitle">
									<h6>Add Field Notes:</h6>
									<p class="mrgBm">This includes time of year, weather
										conditions, and any additional observations that you noticed
										when you spotted the species in question.</p>
								</div>
								<textarea id="edit_field_ntoes" name="fieldNotes" class="detail-content" rows="10"
									placeholder="Add Field Notes..."></textarea>
							</div>
							<div class="inputGroup">
								<a href="#" id="guideBtn" class="helpBtn" data-toggle="popover"
									data-trigger="hover"
									data-content="<strong>Save Sighting:</strong> Save current sightings to your personal sightings. This option means it wont be public until you publish it from your profile. <br/><br/>
												<strong>Publish Sighting:</strong> Let the world see your sighting right now!"></a>
								<input type="button" id="saveSightingBtn" value="Save Sighting"
									class="btn btn-sm btn-gray"> <input type="button"
									id="publicSightingBtn" value="Publish Sighting"
									class="btn btn-sm btn-orange publishbtn">
							</div>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


</div>