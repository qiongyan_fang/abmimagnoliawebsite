<%@ include file="../includes/taglibs.jsp"%>
<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(contentMap))}" />

<section class="container">
	   <div class="row pdTB">
		   <div class="col-sm-6 txt-center">
			   <img alt="" src="${imageUrl}">
		   </div>
		   <div class="col-sm-6">
			   <h3>${content.header}</h3>
			   ${cmsfn:decode(content).description}
		   </div>	   
	   </div>
   </section>
