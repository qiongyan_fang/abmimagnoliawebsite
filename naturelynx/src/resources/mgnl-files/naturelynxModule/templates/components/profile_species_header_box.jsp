<%@ include file="../includes/taglibs.jsp"%>


<c:set var="contentMap" value="${cmsfn:content(row.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(contentMap))}" />

<c:set var="iconContentMap" value="${cmsfn:content(row.iconUrl,'dam')}" />
<c:set var="iconUrl"
	value="${cmsfn:link(cmsfn:asContentMap(iconContentMap))}" />
<div class="row">
	<ol class="breadcrumb">
		<li><a href="species-profiles.html" class="">Species Profiles</a></li>
		<li class="active">${row.species}</li>
	</ol>
</div>
<div class="row sech">
	<div class="col-md-6 nopadding featuredImg secChildh"
		style="background-image: url('${imageUrl}');">
		<img src="${imageUrl}" alt="" />
	</div>
	<div class="col-md-6 nopadding individualInfo secChildh navyblue">
		<div class="individualTitle">
			<h3>${row.species}</h3>
			<img src="${iconUrl}" alt="" class="profile-icon">
		</div>
		<div class="extraInfo">
			<a href="#" class="col-xs-6 left-sec">
				<h3 id="${fn:replace(row.species, ' ','-')}-monitored"></h3> <span>${content.speciesMonitored}</span>
			</a> <a href="#" class="col-xs-6 right-sec">
				<h3 id="${fn:replace(row.species, ' ','-')}-sighted"></h3> <span>${content.sighted}</span>
			</a>
		</div>
	</div>
</div>




