<%@ include file="../includes/taglibs.jsp"%>
  

	<div class="row species-selection">
		<c:forEach var="row" items="${profileImages}">
			<c:set var="contentMap" value="${cmsfn:content(row.imageUrl,'dam')}" />
			<c:set var="imageUrl"
				value="${cmsfn:link(cmsfn:asContentMap(contentMap))}" />

			<c:set var="iconContentMap"
				value="${cmsfn:content(row.iconUrl,'dam')}" />
			<c:set var="iconUrl"
				value="${cmsfn:link(cmsfn:asContentMap(iconContentMap))}" />
			<div class="col-md-3 col-xs-4 featured-items">
				<a href="${content.searchResultUrl}?group=${row.species}" class="block-item block-link">
					<div class="species-img" style="background: rgba(0, 0, 0, 0) url('${imageUrl}') no-repeat scroll 0 0 / cover ;">
						<div class="overlay-box">
							<div class="text-wrapper">
								<div class="tab-cell">
									<img src="${iconUrl}" alt="" />
									<h4 class="style2">${row.species}</h4>
									<span id="${fn:replace(row.species, ' ','-')}"></span>
								</div>
							</div>
						</div>

					</div>
				</a>
			</div>
		</c:forEach>
	</div>
