
<%@ include file="../includes/taglibs.jsp"%>
	<h4 class="style2 mrgBl">${content.header}</h4>
	<div class="row mrgBxs">
		<div class="col-lg-9 col-sm-8">
			<div class="search-block">
			<form action="${content.searchResultUrl}" method="get">
				<input type ="hidden" id="targetUrl" value="${content.searchResultUrl}" />
				<input type ="hidden" id="f_search_type" value="${content.searchType}" />
				<input type="search" placeholder="${content.placeHolder}" id="f_search_box"
					class="searchbar"> <input type="submit" value=""
					class="ct-search-submit">
				</form>
			</div>
		</div>
		<div class="col-lg-3 col-sm-4">
			<a class="btn btn btn-line dark flt-right" href="${content.searchResultUrl}">${empty content.buttonText?'See All':content.buttonText}</a>
		</div>
	</div>
