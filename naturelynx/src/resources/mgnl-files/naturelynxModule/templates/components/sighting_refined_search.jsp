<!-- refined search -->
	<div id="refined-search-area" class="search-area" >
				<div class="row">
					<div class="col-lg-10 col-lg-offset-1">
						<div class="contentWrap">
						<div class="row">
							<div class="col-md-10 col-lg-8 col-lg-offset-2 col-md-offset-1">
								<div class="search-group">
									<div class="searchBy">
										<div class="title">Search by Individual Species</div>
										<div class="data-selected"></div>
									</div>
									<div class="searchdata">
										<div class="search-block">
											<input type="search" class="searchbar" id="search" placeholder="keyword">
											<input class="ct-search-submit" type="submit" value="">
										</div>
										
										<div class="breadcrumb-block">
											<ol class="breadcrumb">
											  <li><a href="#" class="backTomain">< Species List</a></li>
											  
											</ol>
										</div>
										
										<ul class="group-list">
											<li class="group-title"><strong>All</strong></li>
											<li class="group-title">Birds</li>
											<li class="group-title">Mammals</li>
											<li class="group-title">Plants</li>
											<li class="group-title">Fungi</li>
											<li class="group-title">Mosses/Lichen</li>
											<li class="group-title">Insects</li>
											<li class="group-title">Reptiles/Amphibians</li>
											<li class="group-title">Fish</li>
										</ul>
										
										<div class="sub-block">
											<table cellpadding="0" cellspacing="0" border="0" class="display data-list" id="data-list">
											    <thead>
											        <tr>
											            <th>Common Name</th>
											            <th>Scientific Name</th>
											        </tr>
											    </thead>
											    <tbody>
											        <tr>
											            <td>A_Common Name</td>
											            <td>D_Scientific Name</td>
											        </tr>
											        <tr>
											            <td>B_Common Name</td>
											            <td>B_Scientific Name</td>
											        </tr>
											        <tr>
											            <td>C_Common Name</td>
											            <td>E_Scientific Name</td>
											        </tr>
											        <tr>
											            <td>D_Common Name</td>
											            <td>A_Scientific Name</td>
											        </tr>
											        <tr>
											            <td>E_Common Name</td>
											            <td>C_Scientific Name</td>
											        </tr>
											    </tbody>
											    
											</table>
		
										</div>
		
									</div>
								</div>
									
								<div class="search-group">
									<div class="searchBy">
										<div class="title">Search by Species Group</div>
									</div>
										<div class="searchdata">
											
										</div>
								</div>
								<div class="search-group">
									<div class="searchBy">
										<div class="title">Search by My Activity</div>
									</div>
									<div class="searchdata">
									
									</div>
								</div>
								<div class="search-group">
									<div class="searchBy">
										<div class="title">Search by Verification Level</div>
									</div>
									<div class="searchdata">
									</div>
								</div>
								<div class="search-group">
									<div class="searchBy">
										<div class="title">Search by Region</div>
									</div>
									<div class="searchdata">
									</div>
								</div>
								<div class="search-group">
									<div class="searchBy">
										<div class="title">Search by Time</div>
									</div>
									<div class="searchdata">
											
									</div>
								</div>
							</div>
						</div>
						<div class="txt-center"><button type="submit" class="btn" style="background-color: #539BBB;margin-top: 30px;">See Results</button></div>
						</div>
					</div>
				</div>
				<div class="get-data">
					<div class="row">
						<div class="col-md-6">
							<div class="total-spices"><span id="f_spp_id">0 </span><span>SPECIES IDENTIFIED</span>
							<span><strong id="f_spp_verified">0</strong><strong>SIGHTINGS VERIFIED</strong></span></div>
						</div>
						<div class="col-md-6">
							<a href="#" class="btn btn-line">Advanced</a>
							<a href="javascript:void(0);"  id="download-sighting" class="btn btn-line">Get Data</a>
						</div>
					</div>
				</div>
			</div>
			