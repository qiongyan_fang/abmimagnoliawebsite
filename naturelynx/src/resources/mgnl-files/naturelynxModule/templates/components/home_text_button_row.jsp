<%@ include file="../includes/taglibs.jsp"%>
<c:set var="contentMap" value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(contentMap))}" />


<section
	class="container callout ${empty content.description?'home-short-row-single':'home-short-row-mulitple'} ${content.backgroundColor}">
	<div class="row">
		<c:if test="${empty content.description}">
			<div class="col-lg-9 ">
				<img alt="ABMI Logo" src="${imageUrl}">
				<div class="callout-text">
					<h3>${content.header}</h3>
				</div>
			</div>
		</c:if>
		<c:if test="${not empty content.description}">
			<div class="col-lg-9 ">
				<div class="col-lg-2">
					<img alt="ABMI Logo" src="${imageUrl}">
				</div>
				<div class="callout-text col-lg-10">
					<h3>${content.header}</h3>
					<p>${content.description}</p>
				</div>
			</div>
		</c:if>
		<div class="col-lg-3">
			<c:if test="${not empty content.buttonText}">
				<a class="btn btn-line"
					${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""}
					href="${content.buttonUrl}">${content.buttonText}</a>
			</c:if>

		</div>
	</div>
</section>