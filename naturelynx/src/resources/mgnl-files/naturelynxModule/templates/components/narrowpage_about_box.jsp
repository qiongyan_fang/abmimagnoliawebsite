
<%@ include file="../includes/taglibs.jsp"%>

<div class="row">
	<div class="wrap">
		<div
			class="col-md-10 col-lg-8 col-lg-offset-2 col-md-offset-1 txt-center">
			<div class="content-wrap">
				<c:if test="${not empty content.header}">
					<h2 class="mrgT0 txt-blue">${content.header}</h2>
				</c:if>
				<c:if test="${not empty content.imageheader}">
					<c:set var="contentMap"
						value="${cmsfn:content(content.imageheader,'dam')}" />
					<c:set var="imageUrl"
						value="${cmsfn:link(cmsfn:asContentMap(contentMap))}" />
					<img style="max-height: 63px;" class="mrgB2em" alt=""
						src="${imageUrl}">
				</c:if>
				<c:if test="${not empty content.blueheader}">
					<p class="txtStyle-blue mrgB2em">${content.blueheader}</p>
				</c:if>
				<p>${cmsfn:decode(content).description}</p>
			</div>
		</div>
	</div>
</div>
