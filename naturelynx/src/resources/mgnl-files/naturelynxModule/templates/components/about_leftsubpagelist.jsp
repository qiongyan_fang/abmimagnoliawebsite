
<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- < c : set var="uri" value="{empty currentLink?state.handle:currentLink}" / -->
<c:if test="${content.showparent=='true'}">
	<c:set var="uri" value="${state.mainContentNode.parent.path}" />
</c:if>
<c:if test="${not content.showparent=='true'}">
	<c:set var="uri" value="${empty currentLink?state.handle:currentLink}" />
</c:if>

<div class="row filterList stickybar-container">
	<div class="col-md-3 col-sm-4">
		<ul class="sidenav stickybar" style="top: 136px;">
			<c:forEach items="${subpage}" var="row">

				<c:if test="${row.key eq uri}">
					<li class="selected button"><a
						href="${pageContext.request.contextPath}${row.key}.html">${row.value}
					</a></li>
				</c:if>
				<c:if test="${row.key ne uri}">
					<li class="button"><a
						href="${pageContext.request.contextPath}${row.key}.html">${row.value}</a></li>
				</c:if>

			</c:forEach>
		</ul>

	</div>
	<div class="col-md-9 col-sm-8">
		<cms:area name="rightSideArea" />
	</div>
</div>

