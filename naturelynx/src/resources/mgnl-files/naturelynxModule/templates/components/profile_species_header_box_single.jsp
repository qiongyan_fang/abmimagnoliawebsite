<%@ include file="../includes/taglibs.jsp"%>
<script>
var scientificname ="${param.sname}";
</script>
<div class="row">
	<ol class="breadcrumb">
		<li><a href="${content.profileHomeUrl}" class="">Species
				Profiles</a></li>
		<li><a href="${content.profileListUrl}" class="SpeciesGroup"></a></li>
		<li class="active c-species-name"><span class="CommonName"></span> | <span class="ScientificName"></span></li>
	</ol>
</div>
<div class="row sech">
	<div class="col-md-6 nopadding featuredImg secChildh"
		style="background-image: url('images/sampleImg2.jpg');">
		<a href="#" class="open-modalwindow" data-open-id="album-1"></a>
		<!-- Image Gallery -->
		<div class="album">
			<a rel="album-1" class="facybox" href="images/sampleImg2.jpg"
				title="This is a Caption for Expanded Photo View"><img
				src="images/sampleImg2.jpg" alt="" /></a> <a rel="album-1"
				class="facybox" href="images/img-1.jpg"
				title="This is a Caption for Expanded Photo View"><img
				src="images/img-1.jpg" alt="" /></a>
		</div>
		<!-- /.Image Gallery -->
		<img src="images/sampleImg2.jpg" alt="" />
	</div>

	<div class="col-md-6 nopadding individualInfo secChildh">
		<div class="individualTitle navyblue">
			
				<h3><span class="CommonName"></span>  |  <span class="ScientificName"></span></h3>
								
			<p>
				<strong>Status:</strong> <span id="ABStatus"></span>
			</p>
			<a href="#" class="btn btn-line mrgTs">VIEW RANGE MAP</a>
		</div>
		<div class="extraInfo">
			<a href="#" class="col-xs-6 left-sec"> <img
				src="images/mammals-active-icon.png" alt=""> <!--Other images in same folder (birds-active-icon.png , plants-active-icon.png, mosses-active-icon.png, reptiles-active-icon.png, insects-active-icon.png, fungi-active-icon.png, fish-active-icon.png)-->
				<span>MAMMAL</span>
			</a> <a href="#" class="col-xs-6 right-sec">
				<h3>204</h3> <span>SIGHTINGS</span>
			</a>
		</div>
	</div>
</div>

<div class="row filterList stickybar-container">
	<div class="row">
		<div class="col-md-3 col-sm-4">
			<!--  left side menu -->
			<ul class="sidenav stickybar">
				<li class="button selected"><a onclick="javascript:getSpeciesDescription(this);">DESCRIPTION</a></li>
				<!-- Add selected class for selected state -->
				<li class="button"><a onclick="javascript:getSpeciesSighting(this, 1);">SIGHTINGS</a></li>
				<li class="button"><a href="javascript:void(0)">RESOURCES</a>
					<ul>
						<li><a onclick="javascript:getSpeciesResource(this, 1);">All</a></li>
						<li><a onclick="javascript:getSpeciesResource(this, 2);">Videos</a></li>
						<li><a onclick="javascript:getSpeciesResource(this, 3);">Publications</a></li>
						<li><a onclick="javascript:getSpeciesResource(this, 4);">Links</a></li>
					</ul></li>
			</ul>
		</div>
		<!--  right side -->
		<div class="col-md-9 col-sm-8" id="spp-profile-details"></div>
	</div>
</div>



