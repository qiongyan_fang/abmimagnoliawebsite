<%@ include file="../includes/taglibs.jsp"%>
<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(contentMap))}" />


<div data-diff="200"
	style="background-image: url(${imageUrl});"
	class="home banner fullscreen background banner parallax">
	<img class="banner-img" src="${imageUrl}">
	<div class="banner-txt">
		<h1>${content.header}</h1>
		<c:if test="${not empty content.description }"><p>${content.description }</p></c:if>
		<div class="sign-up-in">
			<a class="cd-signup btn btn-orange"
				${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""}
				title="" href="${content.buttonUrl}">${content.buttonText } </a>
		</div>

		<h6>${content.subheader}</h6>
		${cmsfn:decode(content).additional}
	</div>

</div>