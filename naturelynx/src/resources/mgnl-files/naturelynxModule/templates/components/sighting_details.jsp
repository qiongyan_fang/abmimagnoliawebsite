
<div class="row" id="species-detail-area">
	<div class="row">
		<a href="javascript:void(0);" class="pre-sightings">&nbsp;&nbsp;</a> <a
			href="javascript:void(0);" class="next-sightings">&nbsp;</a>
		<div>
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<div class="contentWrap">
						
						<ol class="breadcrumb">
							<li><a href="javascript:void(0);" class="backToSightings">Sightings</a>
							</li>
							<li class="active"><span class="detail-content"
								id="species_group"></span> Sighting</li>
						</ol>

						<div class="row">
							<div class="col-md-6">
								<div class="mrgBl">
									<div class="imgL">
										<div class="status-icon verified_status">
											<span id="sighting_verify_status">Verified</span>
										</div>
										<img id="sighting_img" class="detail-content"
											src="images/img-1.jpg" alt="" />
									</div>

									<div class="imgThumbs">
										<div class="thumbWrapper">
											<a rel="album-1" class="facybox" href="images/img-1.jpg"
												title="Photo Caption 1"><img src="images/img-1.jpg"
												alt="" /></a>
										</div>
										<div class="thumbWrapper">
											<a rel="album-1" class="facybox" href="images/img-1.jpg"
												title="Photo Caption 2"><img src="images/img-1.jpg"
												alt="" /></a>
										</div>
										<div class="thumbWrapper">
											<a rel="album-1" class="facybox" href="images/img-1.jpg"
												title="Photo Caption 3"><img src="images/img-1.jpg"
												alt="" /></a>
										</div>
										<div class="thumbWrapper">
											<a rel="album-1" class="facybox" href="images/img-1.jpg"
												title="Photo Caption 4"><img src="images/img-1.jpg"
												alt="" /></a>
										</div>
									</div>
								</div>
								<div class="postedBy">
									<h6 class="mrgBs">Posted By:</h6>
									<div class="listGroup">
										<div class="listItem">
											<div class="cellImg">
												<a href="#" class="roundImg"> <img
													id="sighting_user_img" class="detail-content"
													src="images/img-2.jpg" alt="" /> <!--  profile image -->
												</a>
											</div>
											<div class="cellDetails">
												<div class="name">
													<a href="#" id="sighting_user_name" class="detail-content">User
														Name</a>
												</div>
												<span id="sighting_user_area" class="detail-content">Edmonton
													Area</span><span class="lineDiv" id="sighting_user_sighting_count"
													class="detail-content">100 Sightings</span>
											</div>
											<div class="cellAction">
												<button id="follow_user_button" class="action add"></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<h3 id="sighting_spp_name" class="detail-content">Species
									Name</h3>
								<div class="summary mrgBm">
									<span id="sighting_paw_count" class="pawTotal detail-content">74</span><span
										id="sighting_comment_count"
										class="commentsTotal detail-content">74</span>
								</div>
								<div class="speciesD mrgBm">
									<div id="mission_name">
										<h6>Mission:</h6>
										<span><a href="#" id="sighting_mission"
											class="detail-content">Mission Name</a></span>
									</div>
									<div>
										<h6>Location:</h6>
										<span class="detail-content" id="sighting_location">Athabasca
											Area</span><a href="#" class="mapPin"></a>
									</div>
									<div>
										<h6>Time:</h6>
										<span id="sighting_time" class="detail-content">Aug. 16
											2015 - 4:25 PM</span>
									</div>
								</div>
								<h6 class="mrgBm">Field Notes:</h6>
								<p id="sighting_field_note" class="detail-content"></p>
							</div>
						</div>
						<!--Comment Section-->
						<div class="messageBoard mrgTm">
							<h6 class="mrgBm">Message Board:</h6>
							<div class="commenthere form">

								<textarea rows="5" id="new_comment_field"
									placeholder="Start Writing..."></textarea>
								<div class="btn-right">
									<input type="submit" id="new_comment_button"
										class="btn btn-sm btn-gray commentBtn" value="Comment">
								</div>

							</div>
							<div class="commnetslist">
								<ul>
									<li class="singlecomments">
										<div class="roundImg">
											<a href="#"> <img src="images/img-2.jpg" alt="">
											</a>
										</div>
										<div class="commentDetail">
											<h6>Username</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing
												elit. In condimentum consequat laoreet. Mauris sodales
												elementum porttitor. Maecenas sed faucibus nibh. Vestibulum
												ante ipsum primis in faucibus orci luctus et ultrices
												posuere cubilia Curae; Nunc maximus turpis dolor, at
												ultrices lacus efficitur ac. Mauris nec urna ipsum.
												Curabitur rhoncus massa convallis justo suscipit egestas.
												Nulla nibh nisi, pharetra imperdiet rutrum ut, laoreet sit
												amet turpis.</p>
											<div class="comment-actions">
												<div class="comment-reply-link">
													<span class="replycounter">1</span>Reply
												</div>
												<a href="#" class="likeComment"><span
													class="replycounter">1</span>Like</a><a href="#"
													class="deleteComment">Delete</a>
											</div>
											<div class="comment-reply">
												<div class="replied-comment">
													<div class="roundImg">
														<a href="#"> <img src="images/img-2.jpg" alt="">
														</a>
													</div>
													<div class="commentDetail">
														<h6>Username</h6>
														<p>Awesome!</p>
														<div class="comment-actions">
															<a href="#" class="likeComment"><span
																class="replycounter">1</span>Like</a><a href="#"
																class="deleteComment">Delete</a>
														</div>
													</div>
												</div>
												<div class="addReply">
													<form action="#" class="form">
														<textarea rows="5" placeholder="Add Reply..."></textarea>
														<div class="btn-right">
															<input type="submit" class="btn btn-sm replyBtn btn-gray"
																value="Reply">
														</div>
													</form>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="roundImg">
											<a href="#"> <img src="images/img-2.jpg" alt="">
											</a>
										</div>
										<div class="commentDetail">
											<h6>Username</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing
												elit. In condimentum consequat laoreet. Mauris sodales
												elementum porttitor. Maecenas sed faucibus nibh. Vestibulum
												ante ipsum primis in faucibus orci luctus et ultrices
												posuere cubilia Curae; Nunc maximus turpis dolor, at
												ultrices lacus efficitur ac. Mauris nec urna ipsum.
												Curabitur rhoncus massa convallis justo suscipit egestas.
												Nulla nibh nisi, pharetra imperdiet rutrum ut, laoreet sit
												amet turpis.</p>
											<div class="comment-actions">
												<div class="comment-reply-link">Reply</div>
												<a href="#" class="likeComment"><span
													class="replycounter">1</span>Like</a><a href="#"
													class="deleteComment">Delete</a>
											</div>
											<div class="comment-reply">
												<div class="addReply">
													<form action="#" class="form">
														<textarea rows="5" placeholder="Add Reply..."></textarea>
														<div class="btn-right">
															<input type="submit" class="btn btn-sm replyBtn btn-gray"
																value="Reply">
														</div>
													</form>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="roundImg">
											<a href="#"> <img src="images/img-2.jpg" alt="">
											</a>
										</div>
										<div class="commentDetail">
											<h6>Username</h6>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing
												elit. In condimentum consequat laoreet. Mauris sodales
												elementum porttitor. Maecenas sed faucibus nibh. Vestibulum
												ante ipsum primis in faucibus orci luctus et ultrices
												posuere cubilia Curae; Nunc maximus turpis dolor, at
												ultrices lacus efficitur ac. Mauris nec urna ipsum.
												Curabitur rhoncus massa convallis justo suscipit egestas.
												Nulla nibh nisi, pharetra imperdiet rutrum ut, laoreet sit
												amet turpis.</p>
											<div class="comment-actions">
												<div class="comment-reply-link">
													<span class="replycounter">2</span>Reply
												</div>
												<a href="#" class="likeComment"><span
													class="replycounter">1</span>Like</a><a href="#"
													class="deleteComment">Delete</a>
											</div>
											<div class="comment-reply">
												<div class="replied-comment">
													<div class="roundImg">
														<a href="#"> <img src="images/img-2.jpg" alt="">
														</a>
													</div>
													<div class="commentDetail">
														<h6>Username</h6>
														<p>Awesome!</p>
														<div class="comment-actions">
															<a href="#" class="likeComment"><span
																class="replycounter">1</span>Like</a><a href="#"
																class="deleteComment">Delete</a>
														</div>
													</div>
												</div>
												<div class="replied-comment">
													<div class="roundImg">
														<a href="#"> <img src="images/img-2.jpg" alt="">
														</a>
													</div>
													<div class="commentDetail">
														<h6>Username</h6>
														<p>Thanks!</p>
														<div class="comment-actions">
															<a href="#" class="likeComment"><span
																class="replycounter">1</span>Like</a><a href="#"
																class="deleteComment">Delete</a>
														</div>
													</div>
												</div>
												<div class="addReply">
													<form action="#" class="form">
														<textarea rows="5" id="reply" placeholder="Add Reply..."></textarea>
														<div class="btn-right">
															<input type="submit" class="btn btn-sm replyBtn btn-gray"
																value="Reply">
														</div>
													</form>
												</div>
											</div>
										</div>
									</li>

								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="sighting_comment_load_button" class="loadMore">
				<span>Click to Load More</span>
			</div>

		</div>
	</div>
</div>
