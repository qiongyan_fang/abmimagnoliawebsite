<%@ include file="../includes/taglibs.jsp"%>
<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(contentMap))}" />

<c:if test="${not empty imageUrl}">
<c:set var="style">
style="background-image:url(${imageUrl});"
</c:set>
</c:if>
<section class="container"><div class="row">
    <div ${style} class="total-sites col-sm-12">
				<div>${cmsfn:decode(content).text}</div>
			</div>
</div>
</section>
	
