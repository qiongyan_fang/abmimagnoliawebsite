<%@ include file="../includes/taglibs.jsp"%>
<c:set var="imageMap" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageMap))}" />


<section class="container">
	<div class="row">
		<div class="col-xs-12 txt-left"
			style="background:rgba(0, 0, 0, 0) url(${imageUrl}) no-repeat  center bottom ;">
			<div class="row">
				<div class="col-lg-9">
					<h2>${content.header}</h2>
					<p>${content.description}</p>
				</div>
				<div class="col-lg-3">
					<c:if test="${not empty content.buttonText}">
						<a class="btn btn-line mrgTxl"
							${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""}
							href="${content.buttonUrl}">${content.buttonText}</a>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</section>
