
<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<section class="container" id="sighting-page">
	<c:if test="${content.viewType == 'Sighitngs'}">
		<div class="row">

			<!--  sub menu for sighting -->


			<div class="search-sightings-header">
				<a href="#" class="btn dark-btn refineBtn">Refine
					Search</a>
				<div id="sighting-spp" class="sightings-icons">
					<a id="spp99" href="javascript:void(0);" class="all"></a> <a  id="spp1" href="javascript:void(0);" class="bird"></a>
					<!-- Add selected class for selected sightings -->
					<a id="spp2" href="javascript:void(0);" class="mammals"></a> <a id="spp3" href="javascript:void(0);" class="plants"></a> 
					<a id="spp4"
						href="javascript:void(0);" class="fungi"></a> <a id="spp5" href="#" class="mosses"></a>
					<a id="spp6" href="javascript:void(0);" class="insects"></a> <a  id="spp7" href="javascript:void(0);" class="reptiles"></a>
					<a  id="spp9" href="javascript:void(0);" class="fish"></a> <a id="spp8" href="javascript:void(0);"
						class="unknown_s selected"></a>
				</div>
				
				<div  class="sightings-icons">
					<a id="sort4" href="#" class="all"></a><!--  my sighitng -->
				</div>
				
				<div class="sightings-header-right">
					<div class="layout-icons">
						<a  href="javascript:showHideAllAreas(5);" class="globe"></a> <a
							 href="javascript:showHideAllAreas(1);" class="list selected"></a>
						<!-- Add selected class for selected layout -->
					</div>
				</div>
			</div>
		</div>

	</c:if>
	<div class="list-area" id="load-species-list-area">
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1">
				<div class="contentWrap">
					<div class="row listHead">
						<div class="col-sm-6 totalNumb">
							<span id="list-summary"></span>
						</div>
						<div class="col-sm-6 sortBtn">
							<span>Sort By: </span>
							<div class="displayOtions">
								<select class="selectpicker" id="sighting-sort-by"> 1 for Most Recent
									<option value="sort1">NEWEST</option>
									<option value="sort2">Most Paw Printed</option>
									<option value="sort3">Most Commented</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row filterList stickybar-container">
						<div class="col-md-3 col-sm-4">
							<ul id="verify-list" class="sidenav stickybar">
							 
								<li id="verify99" class="button selected btnall"><a href="javascript:void(0);">All</a></li>
								<!-- Add selected class for selected state -->
								<li id="verify0" class="button btnunknown"><a href="javascript:void(0);">Unknown</a></li>
								<li id="verify1" class="button btnpending"><a href="javascript:void(0);">Pending</a></li>
								<li id="verify2" class="button btnverified"><a href="javascript:void(0);">Verified</a></li>
							</ul>
						</div>
						<div class="col-md-9 col-sm-8" id="list-group-div">

							
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="sightinglist_load_button" class="loadMore">
			<span>Click to Load More</span>
		</div>
	</div>

<%@ include file="sighting_refined_search.jsp" %>	
<%@ include file="sighting_details.jsp" %>
<%@ include file="sighting_edit.jsp" %>

</section>



