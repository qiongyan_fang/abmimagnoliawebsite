
<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- < c : set var="uri" value="{empty currentLink?state.handle:currentLink}" / -->
<c:if test="${content.showparent=='true'}">
	<c:set var="uri" value="${state.mainContentNode.parent.path}" />
</c:if>
<c:if test="${not content.showparent=='true'}">
	<c:set var="uri" value="${empty currentLink?state.handle:currentLink}" />
</c:if>



<nav>
	<button type="button" class="navbar-toggle collapsed"
		data-toggle="collapse" data-target="#subnavbar" aria-expanded="false"
		aria-controls="navbar">
		<div class="nav-trigger">
			<span></span>
		</div>
		<span class="sr-only">Select Page</span>
	</button>
	<div style="height: 1px;" aria-expanded="false" id="subnavbar"
		class="navbar-collapse collapse">
		<ul>
			<c:forEach items="${subpage}" var="row">

				<c:if test="${row.key eq uri}">
					<li class="active"><a
						href="${pageContext.request.contextPath}${row.key}.html?scroll=true">${row.value}
					</a></li>
				</c:if>
				<c:if test="${row.key ne uri}">
					<li><a
						href="${pageContext.request.contextPath}${row.key}.html?scroll=true">${row.value}</a></li>
				</c:if>

			</c:forEach>
		</ul>
	</div>
</nav>



