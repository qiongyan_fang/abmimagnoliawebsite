<%@ include file="../includes/taglibs.jsp"%>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="toggle-sec">
	<h4 class="toggle-link style2 open">${content.question}</h4>
	<div class="toggle-box" style="display: block;">
		${cmsfn:decode(content).answer}
	</div>
</div>