var access_token, refresh_token;

var userName, userId, isAdmin = false;
var App = {
	init : function() {
		$(document).ready(function() {
			Users.checkAuthenticated();

			$("#signin").bind("click", function() {
				Users.login();
			});

			$("#signup").bind("click", function() {
				Users.signup();
			});

			$("#resetpwd").bind("click", function() {
				Users.forgotPassword();
			});

			if ($("#new_password").length) {
				$("#new_password").bind("click", function() {
					Users.resetPassword(idParameter);
				});
			}
		});
	},

	splash : function() {
		$('#signin_link').show();
		$('.sy-user-modal').removeClass("is-visible");
		$('#user_info').empty().hide();
	},
	home : function(data) {
		$('#signin_link').hide();
		//		
		// "user_name":"qiongyan","user_id":"310","email":"qfang@ualberta.ca","permission_level":"general_user"
		userName = data.user_name;
		userId = data.user_id;
		if (data.permission_level == "administrator") {
			isAdmin = true;
		} else {
			isAdmin = false;
		}
		$('#user_info').html(
				"<span id='welcome'>Welcome " + data.user_name
						+ "</span> <span id='logout'>Logout</span>"
						+ (data.nUnreadMsg?data.nUnreadMsg:""));
		$('#user_info').show();
		$("#logout").bind("click", function() {
			Users.logout();
		});
		
		$('.sy-user-modal').removeClass("is-visible");

	}
};
App.init();

/**
 * Handle User singup login, logout, forget password, reset password,
 */
var Users = {

	init : function() {
		// $(document).ready(function() {
		// $('#login').live('click', function(e){
		// e.preventDefault();
		// Users.login();
		// });
		// $('#logout').live('click', function(e){
		// e.preventDefault();
		// Users.logout();
		// });
		// });
	}(),
	checkAuthenticated : function() {
		access_token = window.localStorage.getItem('access_token');
		if (access_token == null) {
			App.splash();
		} else {
			Users.checkTokenValid(access_token);
		}
	},
	checkTokenValid : function(access_token) {

		$.ajax(
				{
					type : 'GET',
					url : serverUrl + "/api/getUserDetailByAccessToken",
					beforeSend : function(request) {
						request.setRequestHeader("Authorization", "Bearer "
								+ access_token);
					},
					data : {
						accessToken : access_token
					},
					async : false,

					dataType : 'json'

				}).done(function(data) {

			if (data.error) {
				refresh_token = window.localStorage.getItem('refresh_token');
				if (refresh_token == null) {
					App.splash();
				} else {
					Users.refreshToken(refresh_token);
				}
			} else {

				App.home(data);
			}

		})

		.fail(function(a, b, c) {
			console.log('error');
			console.log(a, b, c);
			refresh_token = window.localStorage.getItem('refresh_token');
			if (refresh_token == null) {
				App.splash();
			} else {
				Users.refreshToken(refresh_token);
			}
		})

		; // end of ajax

	},
	getAccessToken : function() {
		access_token = window.localStorage.getItem('access_token');
		if (access_token == null) {
			return null;
		} else {
			Users.checkTokenValid(access_token);
			access_token = window.localStorage.getItem('access_token');
			return access_token;
		}
	},

	// getUserInfo : function(access_token) {
	// Users.userId = -1;
	// Users.userName = "Users";
	// Users.nUnreadMsg = 0;
	// return;
	// $.ajax({
	// type : "POST",
	// url : serverUrl + "/api/getUserDetailByAccessToken",
	// contentType : "application/json; charset=UTF-8",
	// dataType : "json",
	// beforeSend : function(request) {
	// request.setRequestHeader("Authorization", "Bearer "
	// + access_token);
	// },
	// data : {
	// accessToken : access_token
	// }).
	//
	// done( function(data) {
	// if (data.error) {
	// alert(data.error);
	// } else {
	// return data;
	// }
	// }).
	// fail( function(a, b, c) {
	// console.log(a, b, c);
	// return null;
	// }
	// }));
	// },

	refreshToken : function(refreshToken) {
		$.ajax({
			type : "POST",
			url : serverUrl + "/refreshOAuth2AccessToken",
			contentType : "application/json; charset=UTF-8",
			dataType : "json",
			data : JSON.stringify({
				refreshToken : refreshToken
			})

		}).done(
				function(data) {
					if (data.error) {
						alert(data.error);
					} else {
						window.localStorage.setItem('access_token',
								data.access_token);
						window.localStorage.setItem('refresh_token',
								data.refresh_token);
						access_token = window.localStorage
								.getItem('access_token');
						refresh_token = window.localStorage
								.getItem('refresh_token');

						Users.checkTokenValid(access_token);
						// App.home();
					}
				}).fail(function(a, b, c) {
			console.log(a, b, c);
		});
	},
	login : function() {

		var username = $("#username").val();
		var password = md5($("#password").val());
		var rememberme = $("#remember-me:checked").val();
		$(".sy-user-error-message").text("");
		$(".sy-pwd-error-message").text("");
		$(".sy-error-message").removeClass("is-visible");

		if (username === undefined || username == null || username == "") {
			$(".sy-user-msg").text("User name can't be empty");
			$(".sy-user-msg").addClass("is-visible");
			return;
		}

		if (password === undefined || password == null || password == "") {
			$(".sy-pwd-msg").text("password can't be empty");
			$(".sy-pwd-msg").addClass("is-visible");
			return;
		}

		$.ajax({
			url : serverUrl + "/getOAuth2AccessToken",
			type : "POST",
			contentType : "application/json; charset=UTF-8",
			data : JSON.stringify({

				userName : username,
				password : password

			})
		}).done(
				function(data) {
					if (data.error) {
						$(".sy-pwd-msg").text(data.error_description);
						$(".sy-pwd-msg").addClass("is-visible");
					} else {
						window.localStorage.setItem('access_token',
								data.access_token);
						window.localStorage.setItem('refresh_token',
								data.refresh_token);
						access_token = window.localStorage
								.getItem('access_token');
						refresh_token = window.localStorage
								.getItem('refresh_token');
						Users.checkTokenValid(access_token);
						
						
					}
				}).fail(function(a, b, c) {
			console.log(a, b, c);
		});

	},
	logout : function() {
		localStorage.removeItem('access_token');
		localStorage.removeItem('refresh_token');
		access_token = window.localStorage.getItem('access_token');
		refresh_token = window.localStorage.getItem('refresh_token');

		userId = -1;
		userName = "";
		nUnreadMsg = 0;

		App.splash();
	},
	signup : function() {

		var username = $("#su_username").val();
		var password = $("#su_password").val();
		var email = $('#su_email').val();
		var location = $('#su_location').val();
		// $(".su-username-msg").text("");
		// $(".su-password-msg").text("");
		// $(".su-password2-msg").text("");
		// $(".su-term-msg").text("");
		// $(".sy-user-error-message").text("");
		// $(".sy-pwd-error-message").text("");
		$(".sy-error-message").text("");
		$(".sy-error-message").removeClass("is-visible");

		var bValid = true;
		if (username === undefined || username == null || username == "") {
			$(".su-username-msg").text("User name can't be empty");
			$(".su-username-msg").addClass("is-visible");
			bValid = false;
		}

		if (username === undefined || username == null || username == "") {
			$(".su-email-msg").text("Email can't be empty");
			$(".su-email-msg").addClass("is-visible");
			bValid = false;
		}

		if (password === undefined || password == null || password == "") {
			$(".su-password-msg").text("password can't be empty");
			$(".su-password-msg").addClass("is-visible");
			bValid = false;
		}

		if (location === undefined || location == null || location == "") {
			$(".su-location-msg").text("location can't be empty");
			$(".su-location-msg").addClass("is-visible");
			bValid = false;
		}

		if (!($("#su_password").val() == $("#su_password2").val())) {
			$(".su-password-msg").text("passwords don't match");
			$(".su-password2-msg").text("passwords don't match");
			$(".su-password-msg").addClass("is-visible");
			$(".su-password2-msg").addClass("is-visible");
			bValid = false;
		}

		if (!$("#su_term").is(':checked')) {
			$(".su-term-msg").text("You must first agree to the terms");
			$(".su-term-msg").addClass("is-visible");
			bValid = false;
		}

		if (!bValid)
			return;

		var pwd = md5(password);
		pwd = btoa(pwd);
		var jsonData = {
			userName : username,
			email : email,
			password : pwd,
			location : location
		};

		$.ajax({
			type : "POST",
			url : serverUrl + "/signup",
			data : jsonData
		}).done(function(data) {
			if (data.is_error) {
				$(".su-password-msg").text(data.message);
				$(".su-password-msg").addClass("is-visible");
			} else {
				$("#su-msg").text(data.message);

			}
		});

	},
	forgotPassword : function() {
		$(".sy-error-message").removeClass("is-visible");
		$.ajax({
			type : "GET",
			url : serverUrl + "/passwdForget",
			data : {
				email : $('#reset-email').val()
			}
		}).done(function(data) {
			if (data.is_error) {

				$(".reset-password-msg").text(data.message);
				$(".reset-password-msg").addClass("is-visible");
			} else {
				$(".reset-password-msg").text(data.message);
				$(".reset-password-msg").addClass("is-visible");
			}
		});

	},
	resetPassword : function(idParameter) { /*
											 * used for when a user forgets the
											 * password, and request a new one.
											 * this will be called after the
											 * user gets a email, and link to a
											 * new page, and entered two
											 * passwords and then submitted
											 */
		if (!idParameter) {
			return;
		}

		var password = $("#password").val();
		$(".sy-pwd-error-message").text("");
		$(".sy-error-message").removeClass("is-visible");

		var bValid = true;

		if (password === undefined || password == null || password == "") {
			$(".sy-pwd-msg").text("password can't be empty");
			$(".sy-pwd-msg").addClass("is-visible");
			bValid = false;
		}

		if (!($("#su_password").val() == $("#su_password2").val())) {
			$(".su-password-msg").text("passwords don't match");
			$(".su-password2-msg").text("passwords don't match");
			$(".su-password-msg").addClass("is-visible");
			$(".su-password2-msg").addClass("is-visible");
			bValid = false;
		}

		var pwd = md5(password);
		pwd = btoa(pwd);

		$.ajax({
			type : "GET",
			url : serverUrl + "/reset",
			data : {
				passwordNew : pwd,
				id : idParameter
			}
		}).done(function(data) {
			if (data.is_error) {

				$(".reset-password-msg").text(data.message);
				$(".reset-password-msg").addClass("is-visible");
			} else { // show success message
				showMessage(data.message);
				
				
			}
		});

	},

	changePassword : function() {
		/*
		 * used for when a user logs in and change his password
		 */

		var password = $("#password").val();
		$(".sy-pwd-error-message").text("");
		$(".sy-error-message").removeClass("is-visible");

		var bValid = true;

		if (password === undefined || password == null || password == "") {
			$(".sy-pwd-msg").text("password can't be empty");
			$(".sy-pwd-msg").addClass("is-visible");
			bValid = false;
		}

		if (!($("#su_password").val() == $("#su_password2").val())) {
			$(".su-password-msg").text("passwords don't match");
			$(".su-password2-msg").text("passwords don't match");
			$(".su-password-msg").addClass("is-visible");
			$(".su-password2-msg").addClass("is-visible");
			bValid = false;
		}

		var pwd = md5(password);
		pwd = btoa(pwd);

		$.ajax({
			type : "POST",
			url : serverUrl + "/newPassword",
			data : {
				passwordNew : pwd,
				userID : userId
			}
		}).done(function(data) {
			if (data.is_error) {

				$(".reset-password-msg").text(data.message);
				$(".reset-password-msg").addClass("is-visible");
			} else {
				$(".reset-password-msg").text(data.message);
				$(".reset-password-msg").addClass("is-visible");
				User.logout();
			}
		});

	},

	updateProfile : function() {
		$.ajax({
			type : "GET",
			url : serverUrl + "/api/updateProfile",
			data : {
				email : $('#EmailForgot').val()

			}
		}).done(function(data) {
			console.log(data);
		});

	}

};

function attachToken(request) {
	request.setRequestHeader("Authorization", "Bearer " + access_token);
}
