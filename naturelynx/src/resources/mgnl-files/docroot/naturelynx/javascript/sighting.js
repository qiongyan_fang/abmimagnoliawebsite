/*1 for Birds
2 for Mammals
3 for Plants 
4 for Fungi  
5 for Bryophytes and Lichens
6 for Insects               
7 for Reptiles and Amphibians
8 for Others                 
9 for Fish
 */
var speciesGroupList =  {1:"Birds", 2:"Mammals", 3:"Vascular Plants", 4:"Fungi", 5:"Bryophytes and Lichens", 6:"Insects", 
		7:"Reptiles and Amphibians", 8:"Others", 9:"Fish" };


var speciesGroup = [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]; // currently selected species groups: in the horizontal bar
var sortType = 1; /*
					 * 1 for Most Recent 2 for Most Paw Printed 3 for Most
					 * Commented 4 for My Sightings
					 */
var verifyStatus = [ 0, 1, 2 ]; /* 0 for unknown;1 for pending;2 for verified */

var verifyStatusStr = [ "Unknown", "Pending", "Verified" ]; // css class

var listArea = "#load-species-list-area", searchArea = "#refined-search-area", detailArea = "#species-detail-area", editArea = "#species-edit-area";

var bDisplayNextSightingDetail = false;
// names

var sightingScrollArea = "#list-group-div";
$(document).ready(
		function() {
			if ($("#sighting-page").length) {

				showHideAllAreas(1);

				initSightingSelection();
				changeSortType();
				displaySightingList(speciesGroup, verifyStatus, sortType);

				$(".refineBtn").bind("click", function() {
					showHideAllAreas(2);

				});

				$.each(speciesGroupOptions, function(index, value) {
					$('select#species_group').append(
							'<option value="' + value[0] + '" >' + value[1]
									+ '</option>');

				});

				$('select#species_group').selectpicker('refresh'); // refresh
																	// selectpick
				
				
				$("#download-sighting").bind("click", downloadSighting);
			}
		});

/*
 * 1: show list, 2. search 3. detail species 4. edit species
 */
function showHideAllAreas(areaToShow, sightingId) {
	if (areaToShow == 2)
		$(searchArea).show();
	else
		$(searchArea).hide();

	if (areaToShow == 1) {
		$(listArea).show();

		if (sightingId) {
			// scroll to the sighting just viewed
			$(window).scrollTop(
					$("#sighting" + sightingId).offset().top
							- $("#sighting" + sightingId).outerHeight(true));

		} else {
			$(window).scrollTop($(".search-sightings-header").offset().top);
		}
	} else
		$(listArea).hide();

	if (areaToShow == 3)
		$(detailArea).show();
	else
		$(detailArea).hide();

	if (areaToShow == 4) {
		$(editArea).show();
		$("#species-edit-area .sy-error-message").text("");
		$("#species-edit-area .sy-error-message").removeClass("is-visible");	
	}
	else
		$(editArea).hide();

}
/* load current selection from cookies */
function initSightingSelection() {
	/*
	 * if any of the species icon is clicked, update speciesGroup array and save
	 * to cookies
	 */
	$.each($("#sighting-spp a"), function(index, row) {
		$(row).bind("click", function() {
			changeSpeciesSelection(row, true)
		});

	});

	changeSpeciesSelection("#spp99", false);

	$.each($("#verify-list li"), function(index, row) {
		$(row).bind("click", function() {
			changeVerificationSelection(row, true);
		});

	});

}

/**
 * triggers when a user click or unclick a species icon
 * 
 */
function changeSpeciesSelection(sppRow, bQueryNow) {
	var prefix = "spp";
	var sppDiv = $(sppRow);
	var sppId = Number(sppDiv.attr('id').substring(prefix.length));

	if (sppId == 99) { // for all spp, when selected, highlight all species
		// group,
		if (!sppDiv.hasClass("selected")) {
			$("#sighting-spp a").addClass("selected");
			speciesGroup = [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ];
		} else {
			$("#sighting-spp a").removeClass("selected");
			speciesGroup = [];
		}
	} else {
		var sppIndex = speciesGroup.indexOf(sppId);
		if (!sppDiv.hasClass("selected")) {
			sppDiv.addClass("selected");

			if (sppIndex == -1) { /* add to the array if not inside */
				speciesGroup.push(sppId);
			}
		} else {

			sppDiv.removeClass("selected");
			if (sppIndex > -1) {
				speciesGroup.splice(sppIndex, 1);
			}

		}
	}

	/**
	 * make the "All" icon show and hide
	 */
	if (speciesGroup.length >= 9) {
		$("#spp0").addClass("selected");
	} else {
		$("#spp0").removeClass("selected");
	}

	if (bQueryNow) {
		displaySightingList();
	}

}

/**
 * triggers when a user click or unclick a species icon
 * 
 */
function changeVerificationSelection(row, bQueryNow) {
	var prefix = "verify";
	var verifyDiv = $(row);
	var verifyId = Number(verifyDiv.attr('id').substring(prefix.length));

	if (verifyId == 99) { // for all verify, when selected, highlight all
		// species group,
		if (!verifyDiv.hasClass("selected")) {
			$("#verify-list li").addClass("selected");
			verifyStatus = [ 0, 1, 2 ];
		} else {
			$("#verify-list li").removeClass("selected");
			verifyStatus = [];
		}
	} else {
		$("#verify-list li").removeClass("selected");
		verifyStatus = [];

		var verifyIndex = verifyStatus.indexOf(verifyId);
		if (!verifyDiv.hasClass("selected")) {
			verifyDiv.addClass("selected");

			if (verifyIndex == -1) { /* add to the array if not inside */
				verifyStatus.push(verifyId);
			}
		} else {

			verifyDiv.removeClass("selected");
			if (verifyIndex > -1) {
				verifyStatus.splice(verifyIndex, 1);
			}

		}
	}

	if (bQueryNow) {
		displaySightingList();
	}
}

/* bind event for sorting selection and my sighting icon */
function changeSortType() {
	var prefix = "sort";
	$("#sighting-sort-by").change(
			function() {
				sortType = Number($("#sighting-sort-by option:selected").val()
						.substring(prefix.length));
				displaySightingList();
			});

	/* my sighting icon */
	$("#sort4").bind("click", function() {
		sortType = 4;
		displaySightingList();
	});
}

/* use scroll pagination */
var listSpeciesPagination;
function displaySightingList() {
	showHideAllAreas(1);
	var areaDiv = $(sightingScrollArea);
	areaDiv.empty();

	$("#sightinglist_load_button").hide();
	if (speciesGroup == null || speciesGroup.length == 0) {
		$("#list-group-div").html("Please first select a species group!");
		return;
	}

	if (verifyStatus == null || verifyStatus.length == 0) {
		$("#list-group-div").html("Please first select a verification status!");
		return;
	}
	// init scroll pagination
	listSpeciesPagination = $("#list-group-div")
			.scrollPaginationFixedSize(
					{
						url : serverUrl
								+ "/api/getSightingBySpeciesGroupVerifyStatusWithPagination", // the
						// url
						// you
						// are
						// fetching the results
						pageIndex : 0, // start from 0
						scroll : false,
						beforeSend : true,
						pageSize : 10,
						params : {

							speciesGroup : speciesGroup,
							verifyStatus : verifyStatus,

							sortType : sortType,
							userID : userId

						} // these are the variables you can pass to the
						// request, for example:
						// children().size() to know which page you are
						,
						itemDiv : "#list-group-div .listgroup",

						callback : displaySightingItems,
						loadMoreButton : $("#sightinglist_load_button")
					});

}

/* display sighting results */
function displaySightingItems(data) {
	var areaDiv = $(sightingScrollArea);

	$.each(data, function(index, row) {

		var htmStr = '<div class="listgroup" id="sighting' + row.id + '">';
		htmStr += '<div class="itemImg '
				+ verifyStatusStr[row.verify_status].toLowerCase() + 'S">';
		htmStr += '<img src="' + row.img_url + '" alt="" />';
		htmStr += '</div>';
		htmStr += '<div class="listDetail">';
		htmStr += '<h4>';
		htmStr += '	<a href="javascript:showSpeciesDetails(' + row.id + ');">'
				+ row.title + '</a>';
		htmStr += '</h4>';
		htmStr += '<span>' + row.location + '</span><span class="lineDiv">'
				+ row.title + '</span>';
		htmStr += '<div class="summary">'; // tag0: users can
		// add paw , tag1:
		// users can remove
		// paw
		htmStr += '	<span onclick="javascript:likeSighting(this,' + row.id
				+ ');" class="pawTotal tag' + row.paw_status + '" >'
				+ row.paw_count // paw_status
				+ '</span><span  data-id="' + row.id
				+ '" class="commentsTotal">' + row.comment_count + '</span>';
		htmStr += '	</div>';

		htmStr += '<a  href="javascript:showSpeciesDetails(' + row.id + ');" ';
		htmStr += 'class="btn btn-line dark btn-sm">View</a>';

		if (userId == row.user_id || isAdmin) {
			htmStr += '<a href="javascript:delSighting(' + row.id + ');" ';
			htmStr += 'class="btn btn-line dark btn-sm">Delete</a>';
		}

		if (isAdmin) {

			// feature or unfeatur this sighting
			htmStr += '<a   href="javascript:tagFeatureSighting(' + row.id
					+ ');" ';
			htmStr += 'class="btn btn-line dark btn-sm">Featured</a>';

		}

		htmStr += '</div>';
		htmStr += '</div>';

		areaDiv.append(htmStr);

		if (bDisplayNextSightingDetail && index == 0) { // when
			// bDisplayNextSightingDetail
			// is
			// true,
			// open
			// the
			// first
			// species
			// details.
			// used
			// when
			// users are in detailed view, but run to the last
			// one loaded. on the backend,
			// we need to first load the new sightings and then
			// display the first one in detailed
			// in detailed windows.
			showSpeciesDetails(row.id);
			bDisplayNextSightingDetail = false;
		}
	});

	if (data.length == 0) {
		$("#sightinglist_load_button").hide();
	}

	// rerender stickybar
	$(listArea + " .stickybar-container").stickybar({
		label : "reload" + data[0].id
	});
}

/** manage sightings: remove sighting, paw or unpaw, tag feature/untag feature */
function delSighting(sightingID) {
	$.sDialog({
		skin : "block",
		content : "Are You Sure to delete one sighting?",
		okBtnText : "Yes",
		cancelBtnText : "Cancel",
		width : 250,
		okFn : function() {
			$.ajax(
					{
						type : "GET",
						url : serverUrl + "/api/delSighting",
						data : {
							sightingID : sightingID,
							userID : userId
						},
						dataType : "json",
						beforeSend : attachToken
						
					}).done(function(data) {

				$("#sighting" + sightingID).remove();
				showMessage(data.message);
			});
		},
		cancelFn : function() {

		}

	});

}

function tagFeatureSighting(sightingID) {
	$.ajax(
			{
				type : "POST",
				url : serverUrl + "/api/setSightingFeatured",
				beforeSend : attachToken,
				dataType : "json",
				data : {
					sightingID : sightingID,
					featured : 1
				/*
				 * 1 to set the sighting to be featured, 0 to remove the
				 * sighting to be featured
				 */

				}
			}).done(function(data) {
		console.log(data);
	});

}

/*
 * add or remove paw:
 * 
 * if succesful change style, and change counter
 */
function likeSighting(divObj, sightingID) {

	console.log("tag " + divObj);
	var url;
	var bAdd = false;
	if ($(divObj).hasClass("tag0")) {

		url = "/api/addPawToSighting"; // add
		bAdd = true;
	} else {
		url = "/api/removePawFromSighting"; // remove
	}

	$.ajax(
			{
				type : "POST",
				url : serverUrl + url,
				beforeSend : attachToken,
				
				dataType : "json",
				data : {
					sightingID : sightingID,
					userID : userId
				}
			}).done(function(data) {
		_updatePawCount(data, divObj, bAdd);

	});

}

/*******************************************************************************
 * show species details
 * 
 ******************************************************************************/
/**
 * move to previous or next sighting while in the detailed species page load
 * more data when reach the end of sighting list
 */
function showSpeciesDetailsNextPre(sightingId, bNext) {

	var newSightingId;
	var prefix = "sighting"; // for ids
	if (bNext && $("#sighting" + sightingId).next(".listgroup").length) {
		newSightingId = Number($("#sighting" + sightingId).next(".listgroup")
				.attr("id").substring(prefix.length));
		showSpeciesDetails(newSightingId);

	} else if (!bNext && $("#sighting" + sightingId).prev(".listgroup").length) {
		newSightingId = Number($("#sighting" + sightingId).prev(".listgroup")
				.attr("id").substring(prefix.length));

		showSpeciesDetails(newSightingId);

	}

	else if (bNext
			&& $("#sighting" + sightingId).next(".listgroup").length == 0
			&& $("#sightinglist_load_button").hasClass("hasMore")) {
		/*
		 * // when bDisplayNextSightingDetail is true, open the first species
		 * details. used when users are in detailed view, but run to the last
		 * one loaded. on the backend, we need to first load the new sightings
		 * and then display the first one in detailed in detailed windows.
		 * 
		 */
		bDisplayNextSightingDetail = true;
		/*
		 * the trigger will only run after the current function finish running
		 * (showSpeciesDetailsNextPre) so the codes after the trigger will run
		 * before the trigger, so I have to embed codes inside the trigger
		 * callback function.
		 */
		$("#sightinglist_load_button").triggerHandler("click");

	}

}

/*******************************************************************************
 * show species details
 */
function showSpeciesDetails(sightingId) {
	/* show species detail area */

	showHideAllAreas(3);

	/* fetch data and display them */
	$.ajax({
		type : "GET",
		url : serverUrl + "/api/getSighting",
		dataType : "json",
		data : {
			sightingID : sightingId
		},
		beforeSend : attachToken
	}).done(displaySightingDetails);

	/* fetch comments */
	loadComments(sightingId);
}

/* display detailed sighting information */
function displaySightingDetails(row, hasMore) {

	$(detailArea + " .detail-content").empty();

	$(window).scrollTop($(".search-sightings-header").offset().top); // move
	// so
	// the
	// menu
	// bar
	// is at
	// top
	// of
	// the
	// browser
	var sightingId = row.id;

	$(".backToSightings").unbind();
	$(".backToSightings").bind("click", function() { /*
														 * hide species detail,
														 * back to species list
														 */

		showHideAllAreas(1, sightingId);

	});

	$(".pre-sightings").unbind().hide();
	$(".next-sightings").unbind().hide();

	// always show next, if at the end, load more, if no more to load, hide it.
	if ($(detailArea + " #sightinglist_load_button").hasClass("hasMore")
			|| $("#sighting" + sightingId).next(".listgroup").length) {
		$(".next-sightings").show();
		$(".next-sightings").bind("click", function() {
			showSpeciesDetailsNextPre(row.id, true);
		});
	}

	if ($("#sighting" + sightingId).prev(".listgroup").length) {
		$(".pre-sightings").show();
		$(".pre-sightings").bind("click", function() {
			showSpeciesDetailsNextPre(row.id, false);
		});

	}

	$(detailArea + " img.detail-content").attr("src", "");
	$("#new_comment_field").val("");

	$("#species_group").text(row.species_group);

	$("#sighting_user_area").text(row.location);
	$("#sighting_time").text(row.created_at);
	$("#sighting_img").attr("src", mediaServerUrl + row.img_url);

	$(detailArea + " .imgThumbs").empty();
	$.each(row.img_dtos, function(index, img) {
		var imgLink = mediaServerUrl + img.folder + img.name;
		$(detailArea + " .imgThumbs").append(
				'<div class="thumbWrapper">'
						+ '<a rel="album-1" class="facybox" href="' + imgLink
						+ '" title="Photo"><img src="' + imgLink
						+ '" alt=""/></a>' + '</div>');

	});

	$("#sighting_user_img").attr("src", row.simple_user_dto.user_img_url);
	$("#sighting_user_name").text(row.simple_user_dto.user_name);
	$("#sighting_user_area").text("src", row.simple_user_dto.user_city);
	$("#sighting_user_sighting_count").text(
			row.simple_user_dto.total_sighitngs
					+ (row.simple_user_dto.total_sighitngs > 1 ? " Sightings"
							: " Sighting"));

	// $("#sighting_user_add_button") ?

	$("#sighting_spp_name").text(row.title);
	$("#sighting_paw_count").text(row.paw);

	$("#sighting_paw_count").unbind().click(function() {
		likeSighting(this, row.id);
	});
	$("#sighting_paw_count").removeClass("tag0");
	$("#sighting_paw_count").removeClass("tag1");
	$("#sighting_paw_count").addClass("tag" + row.paw_status);

	$("#sighting_comment_count").text(row.comment_cnt);
	if (row.mission) {
		$("#mission_name").show();
		$("#sighting_mission").text(row.mission);
	} else {
		$("#mission_name").hide();

	}

	$("#sighitng_location").text(row.location);
	$("#sighitng_time").text(row.updated_at);
	$("#sighting_field_note").text(row.description);

	$("#sighting_verify_status").text(verifyStatusStr[row.verify_status]);
	$.each(verifyStatusStr, function(index, str) {
		$("#sighting_verify_status").parent().removeClass(
				str.toLowerCase() + "_status");
	});

	$("#sighting_verify_status").parent().addClass(
			verifyStatusStr[row.verify_status].toLowerCase() + "_status");

	$("#new_comment_button").unbind();
	$("#new_comment_button").bind("click", function() {
		postComments("", row.id)
	});

}

/*******************************************************************************
 * ** Comments part: add, display, reply, like, delete etc.
 * 
 ******************************************************************************/
function loadComments(sightingId) {
	var areaDiv = $(".commnetslist ul");
	areaDiv.empty();

	$('#sighting_comment_load_button').hide();

	// init scroll pagination
	$(".commnetslist").scrollPaginationFixedSize({
		url : serverUrl + "/api/getSightingCommentsBySightingIDWithPagination", // the
		// url
		// you
		// are
		// fetching the results
		pageIndex : 0, // start from 0
		scroll : false,
		beforeSend : true,
		pageSize : 10,
		params : {
			sightingID : sightingId,
			page : 0
		// to be fixed.
		}

		// these are the variables you can pass to the
		// request, for example:
		// children().size() to know which page you are
		,
		itemDiv : ".commnetslist .singlecomments",

		callback : displayComments,
		loadMoreButton : $("#sighting_comment_load_button")
	});
}

function displayComments(data) {
	var areaDiv = $(".commnetslist");

	$.each(data, function(index, row) {
		_displayOneComment(row, row.id, true);
	});

	if (data.length == 0) {
		$("sighting_comment_load_button").hide();
	}
}

function displayReplyComments(commentID) {

	$.ajax({
		type : "GET",
		url : serverUrl + "/api/getSightingCommentInfo",
		beforeSend : attachToken,
		dataType : "json",
		data : {
			commentID : commentID
		}
	}).done(function(data) {

		$.each(data.replies, function(index, row) {
			_displayOneComment(row, commentID, false);

		}); // end of each
	});

}

function _displayOneComment(row, commentID, bMainComment) {

	var htmStr = "";

	if (bMainComment) {
		htmStr += '<li class="singlecomments" id="comment' + row.id + '">';
	} else {
		htmStr += '<li class="replied-comment" id="reply' + row.id + '">';
	}

	htmStr += '<div class="roundImg">	<a href="#"><img src="' + row.img
			+ '" alt="">		</a>	</div>';
	htmStr += '<div class="commentDetail">';
	htmStr += '	<h6>' + row.user_name + '</h6>';
	htmStr += '<p>' + row.created_at + " " + row.content + '</p>';
	// data-xxx where xx has to be lower cases
	htmStr += '<div class="comment-actions"><div class="comment-reply-link" data-comment-id="'
			+ row.id
			+ '"><span class="replycounter">'
			+ row.reply_cnt
			+ '</span>Reply</div><a href="javascript:void(0)" class="likeComment mainlikecomment"><span class="tag'
			+ row.paw_status
			+ ' replycounter likecommentcounter">'
			+ row.paw_cnt

			+ '</span>Like</a><a href="javascript:void(0)" class="deleteComment">Delete</a></div>';

	if (bMainComment) {
		htmStr += '<div class="comment-reply">';

		htmStr += '<div class="addReply form">';

		htmStr += '<textarea rows="5" id="new_comment_field' + row.id
				+ '" placeholder="Add Reply..."></textarea>';
		htmStr += '<div class="btn-right">';
		htmStr += '<input type="submit" id="new_comment_button' + row.id
				+ '" class="btn btn-sm replyBtn btn-gray" value="Reply">';
		htmStr += '	</div>';

		htmStr += '</div>';
		htmStr += '</div>';
	}

	htmStr += '</div>';
	htmStr += '</li>';

	if (bMainComment) {
		$(".commnetslist ul").append(htmStr);
		$("#new_comment_button" + row.id).unbind().bind("click", function() {
			postComments(row.id, row.sighting_id, row.id);
		});

		$("#comment" + row.id + " .mainlikecomment").unbind().bind(
				"click",
				function() {
					likeComments(row.id, 1, "#comment" + row.id
							+ " .mainlikecomment .likecommentcounter");
				});

		$("#comment" + row.id + " .deleteComment").unbind().bind("click",
				function() {
					delComments(row.id, 1);
				});

		// --------------------------------------------
		// Comment section
		// --------------------------------------------

		$(".comment-reply-link").unbind().click(
				function() {
					jQuery(this).next(".comment-reply").slideToggle();
					jQuery(this).parents('.comment-actions').next(
							".comment-reply").slideToggle();

					if (!$(this).data("replyload")) {
						displayReplyComments(jQuery(this).data("comment-id"));
						$(this).data("replyload", true)
					}
				});
	} else {

		$("#comment" + commentID + " .addReply").before(htmStr);

		$("#reply" + row.id + " .likeComment").unbind().bind(
				"click",
				function() {
					likeComments(row.id, 2, "#reply" + row.id
							+ " .likecommentcounter");
				});

		$("#reply" + row.id + " .deleteComment").unbind().bind("click",
				function() {
					delComments(row.id, 2);
				});
	}

}
/**
 * add comments (first level comments or reply to comments). only two levels.
 * 
 * @param sightingID
 * @param commentID
 */
function postComments(textAreaSuffix, sightingID, commentID, replyID) {

	if (!commentID) {
		$.ajax({
			type : "POST",
			url : serverUrl + "/api/comments",
			data : {
				userID : userId,
				comment : $("#new_comment_field" + textAreaSuffix).val(),
				sightingID : sightingID
			},
			beforeSend : attachToken,
			dataType : "json"
		}).done(function(data) {
			if (!data.is_error) {
				var row = {
					id : data.id,
					sighting_id : sightingID,
					user_name : userName,
					content : $("#new_comment_field" + textAreaSuffix).val(),
					reply_cnt : 0,
					paw_cnt : 0,
					paw_status : 0
				};

				_displayOneComment(row, commentID, true);

			}

		});

	}

	else {
		$.ajax({
			type : "POST",
			url : serverUrl + "/api/replySightingComment",
			beforeSend : attachToken,
			dataType : "json",
			data : {
				sightingID : sightingID,
				userID : userId,
				replyID : commentID,
				commentID : 0,
				comment : $("#new_comment_field" + textAreaSuffix).val()
			}
		}).done(function(data) {
			if (!data.is_error) {
				var row = {
					id : data.repliedCommentID,
					sighting_id : sightingID,
					user_name : userName,
					content : $("#new_comment_field" + textAreaSuffix).val(),
					reply_cnt : 0,
					paw_cnt : 0,
					paw_status : 0
				};

				_displayOneComment(row, commentID, false);

			}

		});
	}
}

/**
 * delet comments
 * 
 * @param commentID
 */
function delComments(commentID, commentType) {
	$.ajax({
		type : "POST",
		url : serverUrl + "/api/deleteSightingComment",
		beforeSend : attachToken,
		dataType : "json",
		data : {
			commentID : commentID,
			commentType : commentType
		// I don't know why we need this
		}
	}).done(function(data) {
		if (commentType == 2) {
			$("#reply" + commentID).remove();
		} else {
			$("#comment" + commentID).remove();
		}
	}

	);
}

/**
 * paw or unpaw a comment. the commentType 1: comments, 2. replied comments. * I
 * don't know why we need this parameter*
 * 
 * @param commentID
 * @param bPaw
 * @param commentTYpe
 */
function likeComments(commentID, commentType, divObj) {

	var url;
	var bAddLike = false;
	if ($(divObj).hasClass("tag0")) {

		url = "/api/pawSightingComment"; // add
		bAddLike = true;
	} else {
		url = "/api/deletePawSightingComment"; // remove
	}

	$.ajax({
		type : "POST",
		url : serverUrl + url,
		beforeSend : attachToken,

		dataType : "json",
		data : {
			commentID : commentID,
			userID : userId,
			commentType : commentType
		}
	}).done(function(data) {
		_updatePawCount(data, divObj, bAddLike)

	});
}

function _updatePawCount(data, divObj, bAdd) {
	if (data.Success) {
		if (bAdd) {
			$(divObj).removeClass("tag0");
			$(divObj).addClass("tag1");

			try {

				$(divObj).text(Number($(divObj).text()) + 1);
			} catch (e) {
				$(divObj).text(1);
			}
		} else {
			$(divObj).removeClass("tag1");
			$(divObj).addClass("tag0");

			try {
				$(divObj).text(Number($(divObj).text()) - 1);
			} catch (e) {
				$(divObj).text(0);
			}
		}
	} else {
		console.log(data);
	}
}

/*******************************************************************************
 * this section for editing sighting (upload new ones)
 * 
 * *******************************
 ******************************************************************************/
function editSighting(sightingId) {
	showHideAllAreas(4);
	$("#saveSightingBtn").unbind();
	$("#publicSightingBtn").unbind();

	if (sightingId) { /*
						 * for editing sighting, read data and fill them in the
						 * form /* fetch data and display them
						 */
		$.ajax({
			type : "GET",
			url : serverUrl + "/api/getSighting",
			dataType : "json",
			data : {
				sightingID : sightingId
			},
			beforeSend : attachToken
		}).done(_fillEdit);
	} else {

		$("#saveSightingBtn").unbind().bind("click", function() {
			saveSighting(false);
		});
		$("#publicSightingBtn").unbind().bind("click", function() {
			saveSighting(true);
		});

	}
}

function _fillEdit(data) {
	$(".detail-content").empty();

	$(window).scrollTop($(".search-sightings-header").offset().top); // scroll
	$("#species-edit-area .sy-error-message").text("");
	$("#species-edit-area .sy-error-message").removeClass("is-visible");																	// up

	var sightingId = data.id;

	$(".backToSightings").unbind();
	$(".backToSightings").bind("click", function() { /*
														 * hide species detail,
														 * back to species list
														 */
		showHideAllAreas(1, sightingId);

	});

	$("img.detail-content").attr("src", "");

	var editDiv = "#sighting-edit";
	/*
	 * background-image:
	 * url("http://136.159.118.70:8080/mediaServer/resources/uploaded/avatars/1761997806.jpg");
	 * background-size: cover; &=
	 */
	$("#sighting_user_area").text(row.location);
	$("#sighting_time").text(row.created_at);
	$(editDiv + " #main_img").parent().css("background-image",
			mediaServerUrl + row.img_url);
	$(editDiv + " #main_img").parent().css("background-size", "cover");

	$(editDiv + " .drop-area").css("background-image", "");

	$.each(row.img_dtos, function(index, img) {
		var imgLink = mediaServerUrl + img.folder + img.name;
		$(" #image" + index).parent().css("background-image", imgLink);
		$(" #image" + index).parent().css("background-size", "cover");

	});

	$(editDiv + " #user_image").attr("src", row.simple_user_dto.user_img_url);
	$(editDiv + " #user_name").text(row.simple_user_dto.user_name);
	$(editDiv + " #location").text(row.simple_user_dto.user_city);
	$(editDiv + " #user_sighting_count").text(
			row.simple_user_dto.total_sighitngs
					+ (row.simple_user_dto.total_sighitngs > 1 ? " Sightings"
							: " Sighting"));

	$(editDiv + " #species_name").val();
	$(editDiv + " #species_group").val();

	$(editDiv + " #datepicker").val();
	$(editDiv + " #timepicker").val();
	$(editDiv + " #edit_sighting_location").val();
	$(editDiv + " #edit_field_ntoes").val();

	// $("#sighting_user_add_button") ?

	// Set height - Image upload Drop area
	var dropW = $(editArea + " .drop-area").width();
	jQuery(".sighting-edit .drop-area").css("height", dropW);

	/*
	 * fill in group $.ajax({ type: "GET", url: serverUrl +
	 * "/api/getGroupsByUserID", beforeSend: function(request) {
	 * request.setRequestHeader("Authorization", "Bearer
	 * 9668faf7-101c-45dc-a584-cc7479e9f234"); }, data: { userID : 144 },
	 * success: function(data) { console.log(data); } });
	 * 
	 * 
	 * $.ajax({ type: "GET", url: serverUrl + "/api/getMissionsByUserID",
	 * beforeSend: function(request) { request.setRequestHeader("Authorization",
	 * "Bearer 9668faf7-101c-45dc-a584-cc7479e9f234"); }, data: { userID : 144 },
	 * success: function(data) { console.log(data); } });
	 */
	$("#saveSightingBtn").unbind().bind("click", function() {
		saveSighting(false, sightingId);
	});
	$("#publicSightingBtn").unbind().bind("click", function() {
		saveSighting(true, sightingId);
	});

}

function saveSighting(toPublish, sightingId) {
	/*
	 * first need to do validate: required: image, speciesGroupID, lat/long, speciesName
	 * */
	$("#species-edit-area .sy-error-message").text("");
	$("#species-edit-area .sy-error-message").removeClass("is-visible");	
	
	var form = $("#sighting-edit")[0];
	var formData = new FormData(form);
	
	/*
	 * first need to do validate: required: image, speciesGroupID, lat/long, speciesName
	 * */
	var fieldName 	= 	["files","speciesGroupID", "coordinates_lat", "coordinates_long", "speciesName" ];
	var bValid 		=	true;
	$.each(fieldName, function(index, value) {
		if (formData.get(value)== null || formData.get(value) == "" || formData.get(value) .size == 0){
			bValid = false;
			$("#species-edit-area ." + value + "-msg").text("Value required.");
			$("#species-edit-area ." + value + "-msg").addClass("is-visible");
		}
	});
	
	if (!bValid){
		return;
	}
	
	formData.append("userID", userId);

	formData.append("published", (toPublish ? 1 : 0));
	formData.append("autoFill", 0); // manual entered
	formData.append("uploadedDevice", "desktop");
	var combinedCoordinate = "(" + formData.get("coordinates_lat") + "," + formData.get("coordinates_long") + ")";
	formData.append("coordinates", combinedCoordinate);
	$.ajax({
		type : "POST",
		url : serverUrl + "/api/addSighting",
		beforeSend : attachToken,
		data : formData,
		mimeType : "multipart/form-data",
		contentType : false,
		cache : false,
		processData : false,
		dataType : "json"
	}).done(function(data) {
		if (data.sightingID  > 0){
			displaySightingList();
		}
		else{
			showMessage("Adding sighting failed!")
		}
	});

}



/* add file download advanced search */
document.write('<script type="text/javascript" src="/docroot/naturelynx/javascript/sighting_advanced.js"></script>');