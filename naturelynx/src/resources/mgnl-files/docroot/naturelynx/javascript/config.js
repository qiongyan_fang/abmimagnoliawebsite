
var serverUrl = "http://naturelynx-api.sensorup.com:8080/NatureLynxAPI";

var mediaServerUrl = "http://136.159.118.70:8080/mediaServer/";

var speciesGroupOptions =  [
[1, "Birds"],
[2 , "Mammals"],
[3 , "Plants"],
[4 , "Fungi"],
[5 , "Bryophytes and Lichens"],
[6 , "Insects"],
[7 , "Reptiles and Amphibians"],
[8 , "Others"],                 
[9 , "Fish"]
];

var gfollowUserButton = $("#follow_user_button");

function showMessage(msg){
	$.sDialog({
		skin : "green",
		content : msg,

		"cancelBtn" : false,
		width : 250

	});
}