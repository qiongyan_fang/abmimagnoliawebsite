

/*common apis to 
 * 1. get user profiles, # of notifications, etc.
 * 2. total species IDs, verified.
 */

function displayHomeSppNum(){
	
	if ($("#speciesIdentified").length){
		// first get species identified counts
			$.ajax({
		
				type: "GET", 
				    url: serverUrl + "/api/getSpeciesIdentified",
				   
				   success: function(data) {
					   $("#f_spp_id").text(data);
						  
						  $('#f_spp_id').counterUp({
						        delay: 50,
						        time: 1000
						    });
				  
				   }
				});    
	
		// get species verified counts. ideally these two apis can be merged.
			$.ajax({
		
				type: "GET", 
				    url: serverUrl + "/api/getSpeciesVerified",
				   
				   success: function(data) {
					    $("#f_spp_verified").text(data.count);
						  $('#f_spp_verified').counterUp({
						        delay: 50,
						        time: 1000
						    });

				   }
				});    
	}
}


$(document).ready(
		function() {
			
		});


/** add follow to another user */
function followUser() {
	
	var other_userId = gfollowUserButton.data("userId");
	var bAdd = true;
	if (gfollowUserButton.hasClass("added")){
		bAdd = false;
	}
	
	
var url = "";
if(bAdd){
	url =   "/api/addFollow";
}
else {
	url =   "/api/delFollow";

}
$.ajax({
    type: "POST", 
    url: serverUrl + url,
    beforeSend: attachToken,
    data: {
         followerID : other_userId,
         followeeID : userId
    }}.done (function(data){
    	if (!data.is_error) {
	    	if (bAdd) {
	    		gfollowUserButton.removeClass("add");
	    		gfollowUserButton.addClass("added");
	    	}
	    	else{
	    		gfollowUserButton.removeClass("added");
	    		gfollowUserButton.addClass("add");
	    	}
    	}
    	else{
    		showMessage(data.message);
    	}
    });
    
 }