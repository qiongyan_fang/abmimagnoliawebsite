
jQuery(document).ready(function($) {
	
		//search toggle
		$(".searchdata").hide();
		jQuery(".searchBy .title").click(function () {
			jQuery(this).parents('.search-group').toggleClass("open");
			jQuery(this).parent('.searchBy').next(".searchdata").slideToggle();
		});
		
		
		//search temp
		$(".breadcrumb-block, .sub-block").hide();
		jQuery('.group-title').click(function () {
			$(".group-list, .search-block").hide();
			
			var items = '<li class="active"> ' + $(this).text() + '</li>';
			$(".breadcrumb").append(items);
			$(".breadcrumb-block, .sub-block").show();		
		});
		
		
		
		jQuery('.backTomain').click(function () {
			 $(".breadcrumb .active").remove()
			$(".breadcrumb-block, .sub-block").hide();
			$(".group-list, .search-block").show();
		});
		
		$('.data-list tbody tr').click(function () {
			
			var first = $(this).children().first().text();
			var last = $(this).children().first().next().text();
			//console.log('add: '+first+' '+last);		
		    
		    $(".data-selected").append('<div class="item"><span>'+first+'</span><span>'+last+'</span></div>');
		    
		    $(this).fadeOut(300);
		    
		    $('.data-selected .item').click(function(){
			   
			   var findfirst = $(this).children().first().text();
			   //console.log('remove: '+findfirst);
			   
			   $('.data-list tbody tr:contains('+findfirst+')').filter(function() {
				  return findfirst
				}).fadeIn(400);
			   
			   $(this).fadeOut(300,function(){$(this).remove()});	
			     
			});
			
		});
		
	
});  //End Document Ready 


$(window).load(function() {
	
	
	//--------------------------------------------
	// Sightings page Sarch data table
	//--------------------------------------------
	$('#data-list').dataTable({
	    "aaSorting": [[0, "desc"]]
	});
	
	
 });   //End Window Load