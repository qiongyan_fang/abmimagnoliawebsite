/**/

$(document).ready(
		function() {

			displayHomeSppNum();

			if ($("#f_search_box").length) {
				var searchType = $("#f_search_type").val();
				if (searchType == 'profile') {
					autoFillProfileName();
				} else if (searchType == 'group') {
					console.log("search for group");
				} else if (searchType == 'mission') {
					console.log("search for mission");
				}
			}

			if ($("#f-profile-group-count").length) { // show how many species
				// profile in each
				// group, and highlight
				// current group
				displayProfileGroupCount($.url("path"), $.url("?group"));
				displayProfileList(targetUrl, $.url("?group"), $
						.url("?searchName"), pageSize);
			}

			if ($("#spp-profile-details").length) {
				getSpeciesDescription(null, "Medicago sativa")
			}

		});

/**
 * display the species id and verificaiton number on home page
 */
function displayHomeSppNum() {

	if ($("#f_spp_id").length) {
		// first get species identified counts
		$.ajax({

			type : "GET",
			url : serverUrl + "/getSpeciesIdentified",
			dataType : "json"
		}).done(function(data) {
			$("#f_spp_id").text(data.Count);

			$('#f_spp_id').counterUp({
				delay : 50,
				time : 1000
			});

		});

		// get species verified counts. ideally these two apis can be merged.
		$.ajax({

			type : "GET",
			url : serverUrl + "/getSpeciesVerified",
			dataType : "json"
		}).done(function(data) {
			$("#f_spp_verified").text(data.Count);

			$('#f_spp_verified').counterUp({
				delay : 50,
				time : 1000
			});

		});

	}// end of if
}

function autoFillProfileName() {

	var rootPath = "/.ajax/speciesProfile/"
	var searchAutoCompleteUrl = rootPath + "getAutoFillSpeciesName";
	$("#f_search_box").autocomplete({
		source : function(request, response) {

			$.ajax({

				url : searchAutoCompleteUrl,

				dataType : "json",

				data : {
					mask : request.term

				},

				success : function(data) {

					response(data);

				}

			});

		},

		minLength : 2

	});

}

function displayProfileGroupCount(url, currentGroup) {

	if (!currentGroup) {
		currentGroup = "All";
	}

	$.ajax({
		method : "get",
		url : "/.ajax/speciesProfile/getSpeciesCount",
		dataType : "json"
	})

	.done(
			function(data) {
				var totalCount = 0;
				$.each(data,
						function(key, value) {
							totalCount += value;
							var htmStr = "<li class='button "
									+ (currentGroup.toUpperCase() == key
											.toUpperCase() ? "selected" : "")
									+ " '><a href='" + url + "?group=" + key
									+ "'>" + key + "(" + value + ")</a></li>";
							$("#f-profile-group-count").append(htmStr);
						});

			});

}

/**
 * display species profiles list in scroll/click to load more display
 */
function displayProfileList(targetUrl, currentGroup, searchName, pageSize) {
	$("#filterByAlphabet").scrollPagination({
		url : "/.ajax/speciesProfile/getSpeciesList", // the url you are
		// fetching the results
		offset : 1,
		scroll : false,
		params : {
			speciesGroup : (currentGroup == "All" ? "" : currentGroup),
			searchName : searchName,
			pageSize : pageSize

		} // these are the variables you can pass to the request, for example:
		// children().size() to know which page you are
		,
		itemDiv : "#filterByAlphabet .profileName",
		targetUrl : targetUrl,
		callback : displayProfileItems
	});

}

function displayProfileItems(data, targetUrl) {
	/* first get current selected letter */
	var currentSelected = $("#filterByAlphabet .ln-selected").text()
			.toLowerCase();

	$.each(data.items, function(index, row) {
		var leading = (row.commonName == "" ? row.scientificName
				: row.commonName).substring(0, 1).toLowerCase();
		var classDisplay = "listNavShow";
		if (currentSelected != "" && currentSelected != "all"
				&& leading != currentSelected)
			classDisplay = "listNavHide";

		var htmStr = '<li class="ln-' + leading + ' ' + classDisplay
				+ '"><div class="itemData"><div class="cellImg"> '
				+ '<a href="' + targetUrl + '?sname=' + row.scientificName
				+ '" class="roundImg"> ' + '<img src="' + row.porfileImage
				+ '" alt=""> ' + '</a>' + '</div>'
				+ '<div class="cellDetails">' + '	<span class="profileName">'
				+ row.commonName + '</span><span class="lineDiv">'
				+ row.scientificName + ' </span><span class="lineDiv">'
				+ row.status + '</span>' + '</div>'
				+ '<div class="cellAction">' + '	<a href="' + targetUrl
				+ '?sname=' + row.scientificName + '" class="action more"></a>'
				+ '</div>' + '</div>' + '</li>';
		$("#filterByAlphabet").append(htmStr);
	});

	if (data.length == 0) {
		$(".loadMore").hide();
	}

}