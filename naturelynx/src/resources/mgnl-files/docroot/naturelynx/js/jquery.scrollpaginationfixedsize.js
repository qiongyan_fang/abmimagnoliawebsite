(function($) {

	$.fn.scrollPaginationFixedSize = function(options) {
		
		var settings = { 
			pageSize     : 50, // The number of posts per scroll to be loaded
			pageIndex  : 0, // Initial pageIndex, begins at 0 in this case
			error   : 'No More Posts!', // When the user reaches the end this is
										// the message that is
			                            // displayed. You can change this if you
										// want.
			delay   : 500, // When you scroll down the posts will load after a
							// delayed amount of time.
			               // This is mainly for usability concerns. You can
							// alter this as you see fit
			scroll  : true ,// The main bit, if set to false posts will not load
							// as the user scrolls.
			               // but will still load if the user clicks.
			
			message: "Click to load more",
			url		: "",
			params	: {},
			targetUrl: "",
			callback: null,
			itemDiv: "",
			loadMoreButton: $(".loadMore")
			
		}
		
		// Extend the options so they work with the plugin
		if(options) {
			$.extend(settings, options);
		}
		
		// For each so that we keep chainability.
		return this.each(function() {		
			
			// Some variables
			var $this = $(this);
			var $settings = settings;
			var pageIndex = $settings.pageIndex;
			var busy = false; // Checks if the scroll action is happening
			                  // so we don't run it multiple times
			
			/*
			 * // Custom messages based on settings if($settings.scroll == true)
			 * $initmessage = 'Scroll for more or click here'; else $initmessage =
			 * 'Click for more';
			 *  // Append custom messages and extra UI $this.append('<div
			 * class="content"></div><div class="loading-bar">'+$initmessage+'</div>');
			 */
			function getData() {
				var currentParam = $settings.params;
				if (isNaN(pageIndex)){
					currentParam.pageIndex = 0;
				}
				else
					currentParam.pageIndex = pageIndex ;
				
				currentParam.pageSize = $settings.pageSize;
				$.ajax({
					method: "get",	
					url:  $settings.url,
					beforeSend: attachToken, 
					data: currentParam,
					dataType: "json"})

				  .done(function( data ) {
					// Change loading bar content (it may have been altered)
						$settings.loadMoreButton.html($settings.message);
						$settings.loadMoreButton.show();	
						// If there is no data returned, there are no more posts
						// to be shown. Show error
						if(data == "" || data.length == 0 ||  data.error) {  // joan added so something you can use [] or { items:[] }
							$settings.loadMoreButton.html($settings.error);	
							$settings.loadMoreButton.hide();
							if($settings.loadMoreButton.hasClass("hasMore")){
								$settings.loadMoreButton.removeClass("hasMore");
							}
						}
						else {
							
							// Offset increases
							pageIndex ++;
							    
							// Append the data to the content div
						    $settings.callback(data, $settings.targetUrl);
							
							
							if (data.length < $settings.pageSize){ // load less than asked for
								$settings.loadMoreButton.hide();
								if($settings.loadMoreButton.hasClass("hasMore")){
									$settings.loadMoreButton.removeClass("hasMore");
								}
							}
							
							if(!$settings.loadMoreButton.hasClass("hasMore")){
								$settings.loadMoreButton.addClass("hasMore");
							}
						}
						
						// No longer busy!
						busy = false;
				  })
				  .fail(function(){ busy= false;})
				
			
					
			}	
			
			getData(); // Run function initially
			
			// If scrolling is enabled
			if($settings.scroll == true) {
				// .. and the user is scrolling
				$(window).scroll(function() {
					
					// Check the user is at the bottom of the element
					if($(window).scrollTop() + $(window).height() > $this.height() && !busy) {
						
						// Now we are working, so busy is true
						busy = true;
						
						// Tell the user we're loading posts
						$settings.loadMoreButton.html('Loading Posts');
						
						// Run the function to fetch the data inside a delay
						// This is useful if you have content in a footer you
						// want the user to see.
						setTimeout(function() {
							
							getData();
							
						}, $settings.delay);
							
					}	
				});
			}
			
			// Also content can be loaded by clicking the loading bar/
			$settings.loadMoreButton.unbind();
			$( "div" ).on( "click", function( event, person ) {
				  

				});

//			$( "div" ).trigger( "click", { name: "Jim" } );


				
			$settings.loadMoreButton.on("click", function(event, param) {
			
				if(busy == false) {
					busy = true;
					getData();
				}
			
			});
			
		});
	}

})(jQuery);
