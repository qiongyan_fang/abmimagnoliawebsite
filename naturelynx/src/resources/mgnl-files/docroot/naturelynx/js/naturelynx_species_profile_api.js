/**/



function getSpeciesDescription(element, sname){
	if ($(element)){
		$(element).parent().parent().find(".button").removeClass("selected");
		$(element).parent().addClass("selected");
	}
	$.get("/.ajax/speciesProfile/getSingleSpeciesProfile", {scientificname:sname, forApps:true}, 
			function(data){
		$(".CommonName").text(data.CommonName);
		$(".ScientificName").text(sname);
		$("#ABStatus").text(data.ABStatus);
		$(".SpeciesGroup").text(data.SpeciesGroup);
		
		$("#spp-profile-details").empty();
		var htmlStr = '<div class="commonly-mistaken">' +
		'<span class="mistaken-btn">COMMONLY MISTAKEN FOR: <span class="txt-blue">(3)</span></span>' +
		'<div class="mistaken-list">' +
			'<div class="listGroup">' ;
				/* <div class="listItem">
					<div class="cellImg">
						<a href="species-profile-page.html" class="roundImg">
							<img src="images/sampleImg4.png" alt="">
						</a>
					</div>
					<div class="cellDetails">
						<span class="profileName">Foxes</span><span class="lineDiv">Vulpes</span><span class="lineDiv">At Risk</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
ullamcorper augue ut lorem tincidunt, et eleifend nibh vulputate. </p>
					</div>
					<div class="cellAction">
					<a href="species-profile-page.html" class="action more"></a>
				</div>
				</div> */
				
			htmlStr += '	</div>' +
		'</div>' + 
		'</div>';
	
		htmlStr += '<h4 class="style2">Description</h4>' +
		'	<h6>Last Updated: <span class="style2">' + data.QADate  + '</span></h6> ' +
	'<p>' +  data.AppDescription + '</p>';
	
		htmlStr += '<h4 class="style2 mrgTm">Identify Me:</h4>' + data.identifyMe;
	
		htmlStr += '	<h4 class="style2 mrgTm">Family Tree:</h4>' + data.AppFamilyTree;
		htmlStr += '	<h4 class="style2 mrgTm">Text:</h4>' + data.text;
		htmlStr += '	<h4 class="style2 mrgTm">Get to Know Me Better::</h4>' + data.text;
	
		$("#spp-profile-details").append(htmlStr);

		
			}
	
	,"json"
	
	);
}

function displaySpeciesResources(){
	
}