
/**
 * almost every page has a login button. it controls login and log out and signup
 */

$(function(){
	
	if(loginError){	//when login is failed
		$('#loginModal').modal('show');
	}
	
	
	
	$('#signUpBtn').click(function(){	//when a user clicks a sign up button.
		var paramArray = $('#signUpForm').serializeArray();
		var params = {};
		for(var i = 0 ; i < paramArray.length ; i++){	//gets every form in the sign up page
			params[paramArray[i].name] = paramArray[i].value; 
		}
		
		var errorConsole = $('#signUpError');
		
		jQuery.ajax({	//sends a request
		    type: 'post',
		    url: contextPath+'/signup',
		    data: params,
		    success : function(data){
		    	if(data.isError)
		    		errorConsole.html(data.message);
		    	else
		    		login(params);
		    },
		    error : function(){
		    	errorConsole.html('Error occurs in the server. Please contact the web master.');
		    }
		});
	});
	
	
	var login = function(params){	//login method
		var errorConsole = $('#signUpError');
		jQuery.ajax({	//sends a request
		    type: 'post',
		    url: contextPath+'/auto_login',
		    data: {id : params.email, password : params.password},
		    success : function(data){
		    	location.href= data;
		    },
		    error : function(){
		    	errorConsole.html('Error occurs in the server. Please contact the web master.');
		    }
		});
	};
	
});