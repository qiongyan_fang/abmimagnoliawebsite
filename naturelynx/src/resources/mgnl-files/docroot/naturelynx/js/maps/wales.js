var page = 1,$tiles = $('#tiles'),$handler = $('li', $tiles),
			isLoading = false,
			//apiURL = 'get_all_sightings',
			lastRequestTimestamp = 0,
			fadeInDelay = 1;
var $window = $(window),$document = $(document);

			// Prepare layout options.
			var options = {
				autoResize: true, // This will auto-update the layout when the browser window is resized.
				container: $('#tiles'), // Optional, used for some extra CSS styling
				offset: 2, // Optional, the distance between grid items
				itemWidth: 210 // Optional, the width of a grid item
			};


			/**
			 * Refreshes the layout.
			 */
			function applyLayoutWithCallback($newImages, callback){
				//var loadCount=0;
				options.container.imagesLoaded(function(){
					// Destroy the old handler
					if ($handler.wookmarkInstance) {
						$handler.wookmarkInstance.clear();
					}
	
					//loadCount++;
					//console.log(loadCount);
					// Create a new layout handler.
					$tiles.append($newImages);
					$handler = $('li', $tiles);
					//console.log($handler);
					$handler.wookmark(options);
	
					var count=0;
					// Set opacity for each new image at a random time
					$newImages.each(function(){
						var $self = $(this);
						setTimeout(function(){
							$self.css('opacity', 1);
							count++;
							if(count==$newImages.size()){
								console.log(count+'=='+$newImages.size());
								callback();
							}
						}, Math.random() * fadeInDelay);
					});
					
				});
			};
			
			
			
			/**
			 * Refreshes the layout.
			 */
			function refreshLayout(){
				$handler = $('li', $tiles);
				//$handler.wookmark();		
				$handler.trigger('refreshWookmark');
			};
			function applyLayout($newImages){
				options.container.imagesLoaded(function(){
					// Destroy the old handler
					if ($handler.wookmarkInstance) {
						$handler.wookmarkInstance.clear();
					}
	
					// Create a new layout handler.
					$tiles.append($newImages);
					$handler = $('li', $tiles);
					$handler.wookmark(options);
	
					// Set opacity for each new image at a random time
					$newImages.each(function(){
						var $self = $(this);
						setTimeout(function(){
							$self.css('opacity', 1);
						}, Math.random() * fadeInDelay);
					});
				});
			};			