var $city=null, $province = 'unknown';
/**
 * almost every page has a login button. it controls login and log out and signup
 */

	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};	
	
	function goBack() {
	    window.history.back();
	};
	
	
	var getTwoDigits=function(n){
		return n<10? '0'+n:''+n;
	};
	
	var getAddressByLatLng = function(lat, lng, callback){ //this is used to get an address of a spot by passing latitude and longitude over Google. ex: 134.32, -85.12
        //var city=null, province = 'unknown';
		$.ajax({
	          url: "http://maps.googleapis.com/maps/api/geocode/json",
	          type:'get',
	          async : false,
	          data: {
	            latlng: lat+','+lng,
	            sensor : false,
	            language : 'en'
	          },
	          success: function( data ) {
	            $.each(data.results[0].address_components, function(idx, val){
	            	if(val.types[0] == 'locality')
						$city = val.short_name;		            		
	            	else if(val.types[0] == "administrative_area_level_1")
	            		$province = val.short_name;
	            });
	            callback($city,$province,lat,lng);
	          }
		  });
	};
	

	var initCityList=function(city_list_url){
		
	    $('#cityList').typeahead({
	    	minLength: 3,
	        source: function (q, process) {
	            return $.get(city_list_url, {
	                q: q
	            }, function (response) {
	                var data = [];
	                for (var i in response) {
	                    data.push(response[i].id + "#" +response[i].name + "#" +  response[i].lat + "#" + response[i].lon);
	                }
	                return process(data);
	            });
	        },
	        highlighter: function (item) {
	            var parts = item.split('#'),
	                html = '<div class="typeahead">';
	            var name=parts[1];
	            var lat=parts[2];
	            var lon=parts[3];
	            if(!(!name  || name==='null'))	{            
		            html += '<div><strong>' + name + '</strong></div>';
		            //html += '<div class="clearfix"></div>';
	            }
	            html += '</div>';
	            return html;
	        },
	        updater: function (item) {
	            var parts = item.split('#');
	            $( "#cityId" ).val( parts[0] );
	            return parts[1];
	        },
	    });		
	};
	

	
	var initSpeciesList=function(species_list_url,species_group, callback){
	    $('#species_name').typeahead({
	    	minLength: 3,
	    	items: 10,
	        source: function (q, process) {
	        	$( "#speciesId" ).val('');
	            return $.get(species_list_url, {
	                q: q,
	                species_group: species_group
	            }, function (response) {
	                var data = [];
	                for (var i in response) {
	                    data.push(response[i].id + "#" +response[i].common_name + "#" +  response[i].scientific_name + "#" + response[i].species_group_id);
	                }
	                return process(data);
	            });
	        },
	        matcher: function (item) {
	        	  var kw1 = this.query.toLowerCase().replace(" ", "-");
	        	  var kw2 = this.query.toLowerCase().replace("-", " ");
                  return ~item.toLowerCase().indexOf(kw1) || ~item.toLowerCase().indexOf(kw2);
            },
	        highlighter: function (item) {
	            var parts = item.split('#'),
	                html = '<div class="typeahead">';
	            var common_name=parts[1];
	            var scientific_name=parts[2];
	            var species_group_id=parts[3];
	            if(!(!common_name  || common_name==='null'))	{            
		            html += '<div><strong>' + common_name + '</strong></div>';
		            //html += '<div class="clearfix"></div>';
	            }
	            html += '</div>';
	            return html;
	        },
	        updater: function (item) {
	            var parts = item.split('#');
	            var sid=parts[0];
	            $( "#species_name" ).val(sid);
	            $( "#speciesId" ).val(sid);
	            callback(sid);
	            return parts[1];
	        },
	    });		
	};	