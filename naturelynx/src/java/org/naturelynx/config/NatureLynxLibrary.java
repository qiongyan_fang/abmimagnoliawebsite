package org.naturelynx.config;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

public class NatureLynxLibrary {
	public static String  ROOT_PATH = "/naturelynx"; 
	public static String[] AllSpeciesGroups = { "All", "Birds", "Bryophytes",
		"Fish", "Fungi", "Insects", "Lichens", "Mammals", "Reptiles",
		"Soil Mites", "Plants" };
	
	/* use to limit the possible template pages when adding a new page.*/
	public static boolean checkAvailability(Node websiteNode){
		try {
			if (websiteNode.getPath().startsWith(NatureLynxLibrary.ROOT_PATH)){
				
				return true;
			}else{
				
				return false;
			}
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	/* */
	public static List<String> getSpeciesVerificationStatus() {
		ArrayList<String> boxSizeList = new ArrayList<String>();
		boxSizeList.add("verified");
		boxSizeList.add("pending");
		

		return boxSizeList;
	}

	
	public static List<String> getBackgroundColor() {
		ArrayList<String> boxSizeList = new ArrayList<String>();
		boxSizeList.add("yellow");
		boxSizeList.add("orange");
		boxSizeList.add("green");
		boxSizeList.add("blue");
		boxSizeList.add("red");
		boxSizeList.add("navyblue");
		

		return boxSizeList;
	}

	
	public static List<String> getHomeSmallBoxImagePosition() {
		ArrayList<String> boxSizeList = new ArrayList<String>();
		boxSizeList.add("left");
		boxSizeList.add("right");
		boxSizeList.add("top");
		

		return boxSizeList;
	}
}
