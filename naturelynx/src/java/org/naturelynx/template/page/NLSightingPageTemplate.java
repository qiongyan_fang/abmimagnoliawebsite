package org.naturelynx.template.page;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletResponse;


import org.naturelynx.config.NatureLynxLibrary;
import org.naturelynx.template.component.NLBannerComponent;
import org.naturelynx.template.component.NLWidePageSectionComponent;
import org.naturelynx.template.component.subnav.NLSubPageListComponent;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.ClearStyleComponent;
import abmi.module.template.component.PlainHyperlinkComponent;
import abmi.module.template.component.PlainTextComponent;
import abmi.module.template.component.RichTextComponent;



@Controller
@Template(title = "NatureLynx Sighting Page", id = "naturelynx:pages/sightingpage")
public class NLSightingPageTemplate {

	@RequestMapping("/nlsightingpage")
	public String render(Node page, ModelMap model, HttpServletResponse response)
			throws RepositoryException {

		
		return "pages/sighting_page.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(cfg.fields.text("title").label("Title"),
				  cfg.fields.text("keyword").label("keywords").description("The keywords about this page"),
				cfg.fields
				.checkbox("hideInNavigation").buttonLabel("").label("Hide in navigation")
				.description("Check this box to hide this page in navigation"));
	}

	
	
	@Available
	public boolean isAvailable(Node websiteNode) {
		boolean isAvailable = NatureLynxLibrary.checkAvailability(websiteNode);
		
		return isAvailable;

	}
	
	 
	
	
    @Controller
    @Area(value = "topMenuArea", title = "Top Navigation Bar")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class topMenuArea {

        @RequestMapping("/nlsightingpage/topMenuBar")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "mainMenuArea", title = "Main Navigation Bar")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class mainMenuArea {

        @RequestMapping("/nlsightingpage/mainMenuBar")
        public String render() {
            return "areas/listarea.jsp"; // using <li> to connect all components
        }
    }
    
    /**
     * banner area.
     */
    @Area(value="banner", title = "General Banner Area", maxComponents=1)
    @Controller
    @AvailableComponentClasses({NLBannerComponent.class})
    @Inherits(components = ComponentInheritanceMode.FILTERED)
    public static class HomeBannerArea {

        @RequestMapping("/nlsightingpage/banner")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Area(value="navigationArea", title = "Navigation Area", maxComponents=1)
    @Controller
    @AvailableComponentClasses({NLSubPageListComponent.class})
    @Inherits(components = ComponentInheritanceMode.FILTERED)
    
    public static class navigationArea {

        @RequestMapping("/nlsightingpage/navigationArea")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
  
    
   
    @Area(value="wideRowArea", title = "Wide Section Area")
    @Controller
    @AvailableComponentClasses({NLWidePageSectionComponent.class,RichTextComponent.class,ClearStyleComponent.class, PlainTextComponent.class})
    @Inherits(components = ComponentInheritanceMode.FILTERED)
    public static class wideArea {

        @RequestMapping("/nlsightingpage/wideSectionArea")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
  

    @Controller
    @Area(value = "footer", title = "Footer Menu")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class FooterMenuArea {

        @RequestMapping("/nlsightingpage/footermenu")
        public String render() {
            return "areas/listarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "finePrintFooter", title = "Fine Print Footer Links")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class,PlainTextComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class FineFooterMenuArea {

        @RequestMapping("/nlsightingpage/finefootermenu")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }

}
