package org.naturelynx.template.page;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletResponse;


import org.naturelynx.template.component.home.NLHightLightBoxComponent;
import org.naturelynx.template.component.home.NLHomeBannerComponent;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.PlainHyperlinkComponent;
import abmi.module.template.component.PlainTextComponent;
import abmi.module.template.component.TextComponent;


@Controller
@Template(title = "NatureLynx Home Page", id = "naturelynx:pages/home")
public class NLHomePageTemplate {

	@RequestMapping("/nlhomepage")
	public String render(Node page, ModelMap model, HttpServletResponse response)
			throws RepositoryException {

		return "pages/main.jsp";
	}


	 
    @Controller
    @Area(value = "topMenuArea", title = "Top Navigation Bar")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class})
    public static class topMenuArea {

        @RequestMapping("/nlhomepage/topMenuBar")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "mainMenuArea", title = "Main Navigation Bar")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class})
    public static class mainMenuArea {

        @RequestMapping("/nlhomepage/mainMenuBar")
        public String render() {
            return "areas/listarea.jsp"; // using <li> to connect all components
        }
    }
    
    /**
     * jumbotron area.
     */
    @Area(value="banner", title = "Home Banner Area", maxComponents=1)
    @Controller
    @AvailableComponentClasses({NLHomeBannerComponent.class})
    
    public static class HomeBannerArea {

        @RequestMapping("/nlhomepage/banner")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Area(value="sppIdArea", title = "Species Id area", maxComponents=1)
    @Controller
    @AvailableComponentClasses({TextComponent.class})
    
    public static class SppIDArea {

        @RequestMapping("/nlhomepage/sppIdArea")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Area(value="sppVerifyArea", title = "Species Verify area", maxComponents=1)
    @Controller
    @AvailableComponentClasses({TextComponent.class})
    
    public static class SppVerifyArea {

        @RequestMapping("/nlhomepage/sppVerifyArea")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
   
  
    /* rows with two columns layout, users to add news learn more etc. */
    @Controller
    @Area(value = "rowArea", title = "Row Area")
    @Inherits
    @AvailableComponentClasses({NLHightLightBoxComponent.class})
  
    public static class RowArea {

        @RequestMapping("/nlhomepage/rowarea")
        public String render() {
            return "areas/main_rowarea.jsp";
        }
    }
    
    

    @Controller
    @Area(value = "footer", title = "Footer Menu")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class})
    public static class FooterMenuArea {

        @RequestMapping("/nlhomepage/footermenu")
        public String render() {
            return "areas/listarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "finePrintFooter", title = "Fine Print Footer Links")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class,PlainTextComponent.class})
    public static class FineFooterMenuArea {

        @RequestMapping("/nlhomepage/finefootermenu")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }

    @TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(
				  cfg.fields.text("keyword").label("keywords").description("The keywords about this page"),
				cfg.fields
				.checkbox("hideInNavigation").buttonLabel("").label("Hide in navigation")
				.description("Check this box to hide this page in navigation"));
	}

}
