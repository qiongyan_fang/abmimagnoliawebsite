package org.naturelynx.template.page;

import java.io.IOException;

import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;

import info.magnolia.module.blossom.annotation.Template;



import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletResponse;


import org.naturelynx.config.NatureLynxLibrary;
import org.naturelynx.template.component.NLBannerComponent;
import org.naturelynx.template.component.subnav.NLSubPageListComponent;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.PlainHyperlinkComponent;
import abmi.module.template.component.PlainTextComponent;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Naturelynx Blank Page", id = "naturelynx:pages/nlblank")
public class NLBlankPageTemplate {

    @RequestMapping("/nlblank")
    public String render(Node page, ModelMap model,HttpServletResponse response) throws RepositoryException {

    	
    	 if (MgnlContext.getAggregationState().isPreviewMode()
    			 
    			){
    		
				try {
					response.sendRedirect(TemplateUtil.getFirstChildPagePath(page)+ ".html");  
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
    	 }

    	 return "pages/narrow_one_column_page.jsp";
    }
    
    @Available
  	public boolean isAvailable(Node websiteNode) {

  		
  		return NatureLynxLibrary.checkAvailability(websiteNode);

  	}

    @Controller
    @Area(value = "topMenuArea", title = "Top Navigation Bar")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class topMenuArea {

        @RequestMapping("/nlblankpage/topMenuBar")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "mainMenuArea", title = "Main Navigation Bar")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class mainMenuArea {

        @RequestMapping("/nlblankpage/mainMenuBar")
        public String render() {
            return "areas/listarea.jsp"; // using <li> to connect all components
        }
    }
    
    /**
     * banner area.
     */
    @Area(value="banner", title = "General Banner Area", maxComponents=1)
    @Controller
    @AvailableComponentClasses({NLBannerComponent.class})
    public static class HomeBannerArea {

        @RequestMapping("/nlblankpage/banner")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Area(value="navigationArea", title = "Navigation Area", maxComponents=1)
    @Controller
    @AvailableComponentClasses({NLSubPageListComponent.class})
    
    public static class navigationArea {

        @RequestMapping("/nlblankpage/navigationArea")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
  
    
   
  

    @Controller
    @Area(value = "footer", title = "Footer Menu")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class FooterMenuArea {

        @RequestMapping("/nlblankpage/footermenu")
        public String render() {
            return "areas/listarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "finePrintFooter", title = "Fine Print Footer Links")   
    @AvailableComponentClasses({PlainHyperlinkComponent.class,PlainTextComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class FineFooterMenuArea {

        @RequestMapping("/nlblankpage/finefootermenu")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }

	
}
