package org.naturelynx.template.component.loadmoreview;

import java.util.Arrays;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import info.magnolia.ui.form.config.TabBuilder;

import info.magnolia.ui.framework.config.UiConfig;


import org.naturelynx.template.component.NLWidePageSectionComponent;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "List of Sightings", id = "naturelynx:components/sightinglistview")
@TemplateDescription("List of Sightings")
@NLWidePageSectionComponent
public class NLSightingListComponent {

	@RequestMapping("/nlsightinglistview")
	public String render(Node content) {
		

		return "components/sightinglistview.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		String[] viewType = {"Sighitngs", "Missions", "Groups"}; 
		tab.fields(

			
			
				cfg.fields.select("viewType").options(Arrays.asList(viewType)).label("View Type"),
				cfg.fields.text("itemsPerPage").label("Items/Page").description(
						"The nubmer of items loaded per page or clicking of load more"),
				cfg.fields
						.link("buttonUrl")
						.label("Detail view Url")
						.appName("pages")
						.targetWorkspace("website")
						.description(
								"select the detailed page url which will be linked to the view button"),
				cfg.fields.text("buttonText").label("Button Text").defaultValue("View")

		);
	}
}
