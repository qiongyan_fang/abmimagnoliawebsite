package org.naturelynx.template.component.home;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Common  Button", id = "naturelynx:components/commonbutton")
@TemplateDescription("Common Button")
//@MainContent
//@FullWidthContent
public class NLButtonComponent {
	@RequestMapping("/nlcommonbutton")
	public String render() {

		return "components/commonbutton.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				cfg.fields.text("url").label("Link Url"),
				cfg.fields.text("buttontext").label("Button Text")			
		);
	}
}
