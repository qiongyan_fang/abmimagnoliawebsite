package org.naturelynx.template.component.home;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.CompositeFieldBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueSubChildrenNodePropertiesTransformer;
import info.magnolia.ui.framework.config.UiConfig;

import org.naturelynx.config.NatureLynxLibrary;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Recent Sighting", id = "naturelynx:components/recentsighting")
@TemplateDescription("Big Image banner")
@NLHightLightBoxComponent

public class NLHomeRecentSightingComponent {

	@RequestMapping("/nlrecentsighting")
	public String render(Node content, ModelMap model) {

		model.put("sightings", TemplateUtil.getMultiField(content, "sightings"));

		return "components/home_recent_sighting.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(
				cfg.fields.select("backgroundColor").label("Background Color")
						.options(NatureLynxLibrary.getBackgroundColor()),

				cfg.fields.link("iconUrl").label("Icon Image")
						.appName("assets").targetWorkspace("dam"),

				cfg.fields.text("header").label("Top Header"),
				cfg.fields.text("description").label("Description"),

				cfg.fields
						.link("buttonUrl")
						.label("Button Url")
						.appName("pages")
						.targetWorkspace("website")
						.description(
								"select a page, or enter external page link starts with http"),
				cfg.fields.text("buttonText").label("Button Text"),
				cfg.fields
						.multi("sightings")
						.label("Sightings")
						.field(new CompositeFieldBuilder("headertextcompo")
								.fields(cfg.fields.text("sppname").label(
										"Speices"))
								.fields(cfg.fields
										.select("sppstatus")
										.label("ID Status")
										.options(
												NatureLynxLibrary
														.getSpeciesVerificationStatus())
										.description(
												"Species Verificaiton Status"))
								.fields(cfg.fields.link("sppurl")
										.label("Image").appName("assets")
										.targetWorkspace("dam")))
						.transformerClass(
								MultiValueSubChildrenNodePropertiesTransformer.class)

		);
	}
}
