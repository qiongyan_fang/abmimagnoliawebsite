package org.naturelynx.template.component.home;



import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Component with two areas arranged as columns.
 */
@Controller
@Template(id="naturelynx:components/nlHomeTwoColumn", title="Home Row with 2 Columns")
@NLHightLightBoxComponent
public class NLHomeTwoColumnComponent {

    /**
     * Left column.
     */
    @Area(value="left",  maxComponents=1)
    @Controller
    @AvailableComponentClasses(NLHomeHalfImageButtonComponent.class)
    public static class LeftArea {

        @RequestMapping("/nlHomeTwoColumn/left")
        public String render() {
            return "areas/left.jsp";
        }
    }

    /**
     * Right column.
     */
    @Area(value="right",  maxComponents=1)
    @Controller
    @AvailableComponentClasses(NLHomeHalfImageButtonComponent.class)
    public static class RightArea {

        @RequestMapping("/nlHomeTwoColumn/right")
        public String render() {
            return "areas/right.jsp";
        }
    }

    @RequestMapping("/nlHomeTwoColumn")
    public String render() {
        return "components/home_split_row_area.jsp";
    }
}

