package org.naturelynx.template.component;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "NL Reset Password Area", id = "naturelynx:components/nlreset_password")
@TemplateDescription("NL Reset Password Area")
@NLNarrowPageSectionComponent
@NLWidePageSectionComponent
public class NLResetPasswordComponent {

	  @RequestMapping("/nlresetpasswordd")
	    public String render(Node content) {

			  return "components/reset_password.jsp";
	    }

	 
	 
}
