package org.naturelynx.template.component;


import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "NL Image Banner", id = "naturelynx:components/normalbanner")
@TemplateDescription("Image banner")

public class NLBannerComponent {

	  @RequestMapping("/nlnormalbanner")
	    public String render(Node content) {
			  return "components/general_banner.jsp";
	    }

	 
	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
	    			    cfg.fields.link("imageUrl").label("Background Image").appName("assets").targetWorkspace("dam"),
	    			
	    			    cfg.fields.text("header").label("Top Header"),
	    			    cfg.fields.text("description").label("Description"),
	    			    cfg.fields.text("subheader").label("Small Header"),
	    			    cfg.fields.link("buttonUrl").label("Button Url").appName("pages").targetWorkspace("website").description("select a page, or enter external page link starts with http"),
	    			    cfg.fields.text("buttonText").label("Button Text"),
	    			    
	    			    cfg.fields.richText("additional").label("Extra information").description("put extra information here: can be text or images"),
	    			    cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
	         );
	    }
}
