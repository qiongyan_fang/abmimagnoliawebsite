package org.naturelynx.template.component;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import info.magnolia.ui.form.config.TabBuilder;

import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Image Text Button Row", id = "naturelynx:components/imagetextbuttonrow")
@TemplateDescription("Text Button Row")
@NLWidePageSectionComponent
public class NLTextButtonRowComponent {

	@RequestMapping("/nlimagetextbuttonrow")
	public String render(Node content) {
		// boolean bMainPage = false;
		// try{
		// bMainPage = content.getProperty("ismainpage").getBoolean();
		// }
		// catch(Exception e){}
		//
		// if (bMainPage)
		// return "components/jumbotron.jsp";
		// else

		return "components/image_text_button_row.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(

				cfg.fields.link("imageUrl").label("Background Image")
						.appName("assets").targetWorkspace("dam"),

				cfg.fields.text("header").label("Top Header"),
				cfg.fields.text("description").label("Description"),

				cfg.fields
						.link("buttonUrl")
						.label("Button Url")
						.appName("pages")
						.targetWorkspace("website")
						.description(
								"select a page, or enter external page link starts with http"),
				cfg.fields.text("buttonText").label("Button Text")

		);
	}
}
