package org.naturelynx.template.component;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "NL Text Box", id = "naturelynx:components/nlaboutusnarrowpage")
@TemplateDescription("Big Image banner")
@NLNarrowPageSectionComponent
public class NLAboutSectionComponent {

	  @RequestMapping("/nlaboutusnarrowpage")
	    public String render(Node content) {
//		  boolean bMainPage = false;
//		  try{
//			  bMainPage = content.getProperty("ismainpage").getBoolean();
//		  }
//		  catch(Exception e){}
//		  
//		  if (bMainPage)
//	        return "components/jumbotron.jsp";
//		  else
			
			  return "components/narrowpage_about_box.jsp";
	    }

	 
	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	    	
	    			
	    			    cfg.fields.text("header").label("Top Header"),
	    			    cfg.fields.link("imageheader").label("Image Header (optional)").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.text("blueheader").label("Blue SubHeader"),
	    			    cfg.fields.text("description").label("Description"),
	    			    cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
	    			  
	         );
	    }
}
