package org.naturelynx.template.component;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.TextContent;

@Controller
@Template(title = "NL Padding Area", id = "naturelynx:components/nlpadding")
@TemplateDescription("NL Padding Area")
@NLNarrowPageSectionComponent
@NLWidePageSectionComponent
public class NLPaddingComponent {

	  @RequestMapping("/nlpadding")
	    public String render(Node content) {

			  return "components/padding_area.jsp";
	    }

	 
	  	@Area("area")
		@Controller
		@AvailableComponentClasses({NLNarrowPageSectionComponent.class, NLWidePageSectionComponent.class, TextContent.class})
		public static class LeftArea {

			@RequestMapping("/nlpadding/area")
			public String render() {
				return "areas/generalarea.jsp";
			}
		}
}
