package org.naturelynx.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;




@Controller

@Template(title = " Sighting Maps", id = "naturelynx:components/customheightimagebox")
@NLNarrowPageSectionComponent
public class SightingMapComponent {

	  @RequestMapping("/sightingmap")
	    public String render(Node content, ModelMap model ) {
	    

			  return "components/imageboxcustomheight.jsp";
			  
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    
		 
	    	 tab.fields(
	    			
	         );
	    }
}

