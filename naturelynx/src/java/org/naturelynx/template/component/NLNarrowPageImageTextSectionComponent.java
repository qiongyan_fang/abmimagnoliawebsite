package org.naturelynx.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;


import org.naturelynx.config.NatureLynxLibrary;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;


@Controller
@Template(title = "Image box with two buttons", id = "naturelynx:components/nlnarrowpagetextimagebutton")
@NLWidePageSectionComponent
@NLNarrowPageSectionComponent
public class NLNarrowPageImageTextSectionComponent {

	  @RequestMapping("/nlnarrowpagetextimagebutton")
	    public String render(Node content) {
//		  boolean bMainPage = false;
//		  try{
//			  bMainPage = content.getProperty("ismainpage").getBoolean();
//		  }
//		  catch(Exception e){}
//		  
//		  if (bMainPage)
//	        return "components/jumbotron.jsp";
//		  else
			
			  return "components/narrowpage_image_text_button_box.jsp";
	    }

	 
	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	    	
	    			
	    			 	cfg.fields.select("textPosition").label("Text Position").options(TemplateUtil.getPostition().keySet()),
	    			
	    			
	    			    cfg.fields.text("header").label("Top Header"),
	    			    cfg.fields.text("description").label("Description"),
	    			  
	    			    cfg.fields.link("buttonUrl1").label("Button Url 1").appName("pages").targetWorkspace("website").description("select a page, or enter external page link starts with http"),
	    			    cfg.fields.text("buttonText1").label("Button Text 1"),
	    			  
	    			    cfg.fields.link("buttonUrl2").label("Button Url 2").appName("pages").targetWorkspace("website").description("select a page, or enter external page link starts with http"),
	    			    cfg.fields.text("buttonText2").label("Button Text 2"),
	    			    cfg.fields.link("imageUrl").label("Big Image").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.select("backgroundColor").label("Background Color").options(NatureLynxLibrary.getBackgroundColor()).description("Optional use if not use image as background")
	       
	    			  
	         );
	    }
}
