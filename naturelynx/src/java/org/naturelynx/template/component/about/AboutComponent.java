package org.naturelynx.template.component.about;

import javax.jcr.Node;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import org.naturelynx.template.component.NLWidePageSectionComponent;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;


import abmi.module.template.component.CommonButtonComponent;

import abmi.module.template.component.PlainTextComponent;

import abmi.module.template.component.TextContent;

/**
 * About Page 
 * @author Qiongyan
 *
 */
@Controller
@Template(title = "About - home", id = "naturelynx:components/about_naturelynx")
@TemplateDescription("About - home")
@NLWidePageSectionComponent
public class AboutComponent {
	@RequestMapping("/nlabout_naturelynx")
	public String render(Node content,ModelMap model) {
		
	
		return "components/about_naturelynx.jsp";
	}


	
  
    /**
     * .
     */
    @Area(value="content-01")
    @Controller
    @AvailableComponentClasses(PlainTextComponent.class)
    public static class Content01 {

        @RequestMapping("/nlabout_naturelynx/Content01")
        public String render() {
            return "areas/left.jsp";
        }
    }
    
    /**
     * .
     */
    @Area(value="content-02")
    @Controller
    @AvailableComponentClasses(CommonButtonComponent.class)
    public static class Content02 {

        @RequestMapping("/nlabout_naturelynx/Content02")
        public String render() {
            return "areas/left.jsp";
        }
    }
    
    /**
     * .
     */
    @Area(value="content-11")
    @Controller
    @AvailableComponentClasses(TextContent.class)
    public static class Content11 {

        @RequestMapping("/nlabout_naturelynx/Content11")
        public String render() {
            return "areas/left.jsp";
        }
    }
    /**
     * .
     */
    @Area(value="content-12")
    @Controller
    @AvailableComponentClasses(TextContent.class)
    public static class Content12 {

        @RequestMapping("/nlabout_naturelynx/Content12")
        public String render() {
            return "areas/left.jsp";
        }
    }

    /**
     * .
     */
    @Area(value="content-21")
    @Controller
    @AvailableComponentClasses(TextContent.class)
    public static class Content21 {

        @RequestMapping("/nlabout_naturelynx/Content21")
        public String render() {
            return "areas/left.jsp";
        }
    }
    /**
     * .
     */
    @Area(value="content-22")
    @Controller
    @AvailableComponentClasses(TextContent.class)
    public static class Content22 {

        @RequestMapping("/nlabout_naturelynx/Content22")
        public String render() {
            return "areas/left.jsp";
        }
    }
    
    /**
     * .
     */
    @Area(value="content-31")
    @Controller
    @AvailableComponentClasses(TextContent.class)
    public static class Content31 {

        @RequestMapping("/nlabout_naturelynx/Content31")
        public String render() {
            return "areas/left.jsp";
        }
    }
    /**
     * .
     */
    @Area(value="content-32")
    @Controller
    @AvailableComponentClasses(TextContent.class)
    public static class Content32 {

        @RequestMapping("/nlabout_naturelynx/Content32")
        public String render() {
            return "areas/left.jsp";
        }
    }
    
    /**
     * .
     */
    @Area(value="content-41")
    @Controller
    @AvailableComponentClasses(TextContent.class)
    public static class Content41 {

        @RequestMapping("/nlabout_naturelynx/Content41")
        public String render() {
            return "areas/left.jsp";
        }
    }
    /**
     * .
     */
    @Area(value="content-42")
    @Controller
    @AvailableComponentClasses(TextContent.class)
    public static class Content42 {

        @RequestMapping("/nlabout_naturelynx/Content42")
        public String render() {
            return "areas/left.jsp";
        }
    }
    
    @Area(value="content-43")
    @Controller
    @AvailableComponentClasses(TextContent.class)
    public static class Content43 {

        @RequestMapping("/nlabout_naturelynx/Content43")
        public String render() {
            return "areas/left.jsp";
        }
    }
}
