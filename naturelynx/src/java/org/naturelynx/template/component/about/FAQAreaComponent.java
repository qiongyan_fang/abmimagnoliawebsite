package org.naturelynx.template.component.about;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import org.naturelynx.template.component.NLNarrowPageSectionComponent;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "About - FAQ Item Area", id = "naturelynx:components/nlfaqboxarea")
@TemplateDescription("About - FAQ Items")
@NLNarrowPageSectionComponent
public class FAQAreaComponent {

	@Area("FAQArea")
	@Controller
	@AvailableComponentClasses(FAQStyleBlockComponent.class)
	public static class LeftArea {

		@RequestMapping("/nlfaqboxarea/singlefaq")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@RequestMapping("/faqboxarea")
	public String render() {
		return "components/about_faqboxareacomponent.jsp";
	}
}
