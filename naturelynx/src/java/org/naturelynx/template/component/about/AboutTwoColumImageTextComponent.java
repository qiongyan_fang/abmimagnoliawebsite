
package org.naturelynx.template.component.about;


import javax.jcr.Node;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;


import org.naturelynx.template.component.NLWidePageSectionComponent;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "About - Two Column Image Text", id = "naturelynx:components/about_twocolumnimagetext")
@TemplateDescription("About - Two Column Image Text")
@NLWidePageSectionComponent
public class AboutTwoColumImageTextComponent {
	 @RequestMapping("/about_twocolumnimagetext")
	    public String render(Node content) {
	
		 
		 return "components/about_twocolumnimagetext.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	    	tab.fields(
	    		     cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),
	               
	                 cfg.fields.text("header").label("Header"),
	                 cfg.fields.richText("description").label("description")

	                 
	         );
	    }
}
