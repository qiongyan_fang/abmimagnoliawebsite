
package org.naturelynx.template.component.about;


import javax.jcr.Node;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "About FAQ Single Item", id = "naturelynx:components/nlfaqbox")
@TemplateDescription("About FAQ Single Item")

public class FAQStyleBlockComponent {
	 @RequestMapping("/nlfaqbox")
	    public String render(Node content) {
	
		 
		 return "components/about_faqbox.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	    	tab.fields(
//	    		     cfg.fields.link("url").label("Link Url").appName("pages").targetWorkspace("website"),
	                 cfg.fields.text("question").label("Question"),
	                 cfg.fields.richText("answer").label("Answer")
//	                 cfg.fields.hidden("componentindex")
	                 
	         );
	    }
}
