
package org.naturelynx.template.component.about;


import javax.jcr.Node;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;


import org.naturelynx.template.component.NLWidePageSectionComponent;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "About - ABMI Green Banner", id = "naturelynx:components/nlabmigreenban")
@TemplateDescription("About - ABMI Green Banner")
@NLWidePageSectionComponent
public class AboutABMIGreenBanComponent {
	 @RequestMapping("/nlabmigreenban")
	    public String render(Node content) {
	
		 
		 return "components/about_abmigreenbanner.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	    	tab.fields(
	    		     cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),
	               
	                 cfg.fields.text("text").label("Text").description("use <span> to bold text")

	                 
	         );
	    }
}
