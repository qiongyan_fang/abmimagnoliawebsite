package org.naturelynx.template.component.about;


import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.naturelynx.template.component.NLNarrowPageSectionComponent;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.ClearStyleComponent;
import abmi.module.template.component.PlainTextComponent;
import abmi.module.template.component.RichTextComponent;

@Controller
@Template(title = "About - Left Sub Page List Box", id = "naturelynx:components/nlaboutleftpagelist")
@TemplateDescription("About - Left Sub Page List Box")
@NLNarrowPageSectionComponent
public class LeftNavigationComponent {

	@RequestMapping("/nlaboutleftpagelist")
	public String render(ModelMap model, Node content)
			throws RepositoryException {

		String path = content.getProperty("rootpath").getString();
		Map<String, String> subPageList = new LinkedHashMap<String, String>();
		boolean bIncludeRoot = true;
		try {
			bIncludeRoot = PropertyUtil.getBoolean(content, "includeroot",
					false);

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (bIncludeRoot) {
			Node rootNode = content.getSession().getNode(path);
			String title = "";
			try {
				title = content.getProperty("roottext").getString();
			} catch (Exception e) {
				title = PropertyUtil.getString(rootNode, "title", "");
			}
			subPageList.put(rootNode.getPath(), title);

		}

		for (Node node : NodeUtil.getNodes(content.getSession().getNode(path),
				NodeTypes.Page.NAME)) {
			if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {

				subPageList.put(node.getPath(),
						PropertyUtil.getString(node, "title", ""));
			}
		}

		model.put("subpage", subPageList);

		return "components/about_leftsubpagelist.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(

				cfg.fields.link("rootpath").label("Link Url").appName("pages")
						.targetWorkspace("website"),

				cfg.fields.checkbox("includeroot")
						.buttonLabel("include the root page in the navigation")
						.label("Include the root page?"),
				cfg.fields.text("roottext").label(
						"Text of the root page (default:title)"),

				cfg.fields.checkbox("inheritable").buttonLabel("")
						.label("Inheritable").description("Show in Subpages?")

		);
	}

	@Area(value = "rightSideArea", title = "Section Area With Padding")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			ClearStyleComponent.class, PlainTextComponent.class })
	public static class narrowArea {

		@RequestMapping("/nlaboutleftpagelist/rightSideArea")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
}