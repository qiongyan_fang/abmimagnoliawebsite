package org.naturelynx.template.component.profile;

import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.naturelynx.template.component.NLNarrowPageSectionComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Profile - Single Species Header Box", id = "naturelynx:components/profilesinglespeciesheader")
@TemplateDescription("Profile - Single Species Header Box")
@NLNarrowPageSectionComponent
public class ProfileSingleSpeciesHeaderBoxComponent {
	private static final Logger log = LoggerFactory
			.getLogger(ProfileSingleSpeciesHeaderBoxComponent.class);

	@RequestMapping("/profilesinglespeciesheader")
	public String render(Node content, ModelMap model,
			HttpServletRequest request) {

		log.info("sname= {}" , request.getParameter("sname"));
		return "components/profile_species_header_box_single.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				cfg.fields.link("profileHomeUrl").label("overview profile url")
						.appName("pages").targetWorkspace("website"),
				cfg.fields.link("profileListUrl").label("group profile url")
						.appName("pages").targetWorkspace("website"));
	}
}
