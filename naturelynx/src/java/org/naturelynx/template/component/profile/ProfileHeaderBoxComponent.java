package org.naturelynx.template.component.profile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.CompositeFieldBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueSubChildrenNodePropertiesTransformer;
import info.magnolia.ui.framework.config.UiConfig;

import org.naturelynx.config.NatureLynxLibrary;
import org.naturelynx.template.component.NLNarrowPageSectionComponent;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;


import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Profile - Species Header Box", id = "naturelynx:components/profilespeciesheader")
@TemplateDescription("Profile - Species Header Box")
@NLNarrowPageSectionComponent
public class ProfileHeaderBoxComponent {
	@RequestMapping("/profilespeciesheader")
	public String render(Node content,ModelMap model,HttpServletRequest request) {
		
		ArrayList<Map<String,String>> images = TemplateUtil.getMultiField(content, "profileImages");
		String groupName = request.getParameter("group");
		if (groupName == null || !Arrays.asList(NatureLynxLibrary.AllSpeciesGroups).contains(groupName)) {
			groupName = "All";
		}
		
		for (Map<String,String> single:images) {
			if (single.get("species").equals(groupName)) {
				model.put("row", single);
			}
		}
		
		return "components/profile_species_header_box.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				 cfg.fields.text("header").label("Header"),
				 cfg.fields.link("profileHomeUrl").label("overview profile url").appName("pages").targetWorkspace("website"),
					cfg.fields.multi("profileImages").label("Detail").field(new CompositeFieldBuilder("profilecompo")
					.fields(cfg.fields
							.select("species").label("Species Group")
							.options(Arrays.asList(NatureLynxLibrary.AllSpeciesGroups))). fields(cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam")).
							 fields(cfg.fields.link("iconUrl").label("Image").appName("assets").targetWorkspace("dam"))
							).transformerClass(MultiValueSubChildrenNodePropertiesTransformer.class)	,		
							 cfg.fields.text("speciesMonitored").label("Text Species Monitored").defaultValue("Species Monitored"),
							 cfg.fields.text("sighting").label("Text Sighting").defaultValue("Sighting")
		);
	}
}
