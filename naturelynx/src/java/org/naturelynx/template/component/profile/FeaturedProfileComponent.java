package org.naturelynx.template.component.profile;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;


import org.naturelynx.template.component.NLNarrowPageSectionComponent;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Profile - Featured Species", id = "naturelynx:components/profilespeciesfeatured")
@TemplateDescription("Profile - Featured Species")
@NLNarrowPageSectionComponent
public class FeaturedProfileComponent {
	@RequestMapping("/nlprofilespeciesfeatured")
	public String render(Node content, ModelMap model) {

		Session session;
		try {
			session = MgnlContext.getJCRSession("profiles");
			Node sppNode = session.getNode(PropertyUtil.getString(content,
					"sppPath", "/"));
			Map<String, String> resultMap = new HashMap<String, String>();
			resultMap.put("commonName",
					PropertyUtil.getString(sppNode, "CommonName", ""));
			resultMap.put("scientificName",
					PropertyUtil.getString(sppNode, "ScientificName"));
			resultMap.put("description",
					PropertyUtil.getString(sppNode, "AppDescription", ""));
			model.put("sppNode", resultMap);
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "components/profile_species_featured.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				cfg.fields.text("header").label("Header"),
				cfg.fields.link("sppPath").label("Species Name")
						.appName("species-profile").targetWorkspace("profiles"));
	}
}
