package org.naturelynx.template.component.profile;

import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.naturelynx.template.component.NLNarrowPageSectionComponent;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Profile - Species List", id = "naturelynx:components/profilespecieslist")
@TemplateDescription("Profile - Species List")
@NLNarrowPageSectionComponent
public class ProfileListComponent {
	@RequestMapping("/profilespecieslist")
	public String render(Node content,ModelMap model,HttpServletRequest request) {
		
				
		return "components/profile_species_list.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				 cfg.fields.text("pageSize").label("Rows per load"),
				 cfg.fields.link("profileUrl").label("single profile url").appName("pages").targetWorkspace("website")
		);
	}
}
