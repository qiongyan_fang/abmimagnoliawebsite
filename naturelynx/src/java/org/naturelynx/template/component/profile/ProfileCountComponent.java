package org.naturelynx.template.component.profile;

import java.util.Arrays;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.CompositeFieldBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueSubChildrenNodePropertiesTransformer;
import info.magnolia.ui.framework.config.UiConfig;


import org.naturelynx.template.component.NLNarrowPageSectionComponent;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.api.controller.SpeciesProfileParameters;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Profile - Species Count", id = "naturelynx:components/profilespeciescount")
@TemplateDescription("Profile - Species Count")
@NLNarrowPageSectionComponent
public class ProfileCountComponent {
	@RequestMapping("/nlprofilespeciescount")
	public String render(Node content,ModelMap model) {
		model.put("profileImages", TemplateUtil.getMultiField(content, "profileImages"));
		return "components/profile_species_count.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				 cfg.fields.text("header").label("Search Header"),
				 cfg.fields.link("searchResultUrl").label("Search Page").appName("pages").targetWorkspace("website"),
				cfg.fields.multi("profileImages").label("Detail").field(new CompositeFieldBuilder("profilecompo")
				.fields(cfg.fields
						.select("species").label("Species Group")
						.options(Arrays.asList(SpeciesProfileParameters.AllSpeciesGroups))).
				
				 fields(cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam")).
				 fields(cfg.fields.link("iconUrl").label("Image").appName("assets").targetWorkspace("dam"))
						).transformerClass(MultiValueSubChildrenNodePropertiesTransformer.class)			
					
		);
	}
}
