package org.naturelynx.template.component.subnav;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;



import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Sub Page List Box", id = "naturelynx:components/nlsubpagelist")
@TemplateDescription("sub page List")

public class NLSubPageListComponent {

	
	 @RequestMapping("/nlsubpagelist")
	  public String render(ModelMap model, Node content) throws RepositoryException {
		 
		        String path = content.getProperty("rootpath").getString();
		        Map<String, String> subPageList = new LinkedHashMap<String, String>();
		        boolean bIncludeRoot = true;
		        try{
		        	bIncludeRoot =  PropertyUtil.getBoolean(content,"includeroot", false);
		        	
		        }catch(Exception e){
		        	e.printStackTrace();
		        }
		    	
		        if (bIncludeRoot){
		        	Node rootNode = content.getSession().getNode(path);
		        	String title = "";
		        	try{
		        		title = content.getProperty("roottext").getString();
		        	}
		        	catch(Exception e){
		        		title = PropertyUtil.getString(rootNode, "title", "");
		        	}
		        	subPageList.put(rootNode.getPath(),title );	 
		        	
		        }
		    
		        for (Node node : NodeUtil.getNodes(content.getSession().getNode(path), NodeTypes.Page.NAME)) {
		            if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {

		            	subPageList.put(node.getPath(), PropertyUtil.getString(node, "title", ""));		            	
		             }
		        }
		        
		        model.put("subpage", subPageList);
		      
	        return "components/subpagelist.jsp";
	    }

	 @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
	    			 cfg.fields.link("rootpath").label("Link Url").appName("pages").targetWorkspace("website"),

	    			 cfg.fields.checkbox("includeroot").buttonLabel("include the root page in the navigation").label("Include the root page?"),
	    			 cfg.fields.text("roottext").label("Text of the root page (default:title)"),

	    			 cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")

	         );
	    }
	 
	 
}
