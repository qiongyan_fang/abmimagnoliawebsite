package org.naturelynx.template.component;

import java.util.Arrays;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Search Box", id = "naturelynx:components/nlsearch")
@TemplateDescription("Search Box")
@NLNarrowPageSectionComponent
public class NLSearchRowComponent {

	  @RequestMapping("/nlsearch")
	    public String render(Node content) {
//		  boolean bMainPage = false;
//		  try{
//			  bMainPage = content.getProperty("ismainpage").getBoolean();
//		  }
//		  catch(Exception e){}
//		  
//		  if (bMainPage)
//	        return "components/jumbotron.jsp";
//		  else
			
			  return "components/narrowpage_search.jsp";
	    }

	 
	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	String searchType [] = {"profile", "group", "mission"};
	    	 tab.fields(
	    	
	    			
	    			    cfg.fields.text("header").label("Top Header"),
	    			    cfg.fields.text("placeHolder").label("Place Holder"),
	    			    cfg.fields.text("buttonText").label("Button Text"),
	    			    cfg.fields.link("searchResultUrl").label("Target Page").appName("pages").targetWorkspace("website"),
	    			    cfg.fields.select("searchType").label("Search in").options(Arrays.asList(searchType))
	    			  
	         );
	    }
}
