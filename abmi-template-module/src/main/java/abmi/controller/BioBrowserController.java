package abmi.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import abmi.model.datatable.SpeciesDataTableCriterias;
import abmi.model.services.biobrowser.BioBrowserService;
import abmi.model.services.biobrowser.GraphService;
import abmi.model.services.biobrowser.PdfDownload;

import abmi.model.services.biobrowser.species.SpeciesProfileContent;
import abmi.model.services.guilds.GuildServices;
import abmi.model.util.CommonUtil;

@Controller
@RequestMapping(value = "/bioBrowser")
public class BioBrowserController {

	@Autowired
	BioBrowserService bioBrowserService;
	@Autowired
	GraphService graphService;

	@Autowired
	GuildServices guildServices;
	
	

	private static final Logger log = LoggerFactory.getLogger(BioBrowserController.class);

	/**
	 * read list of species from database (information include rank name?,
	 * scientific names, common names, profile images) has more records for
	 * "loading more"?
	 * 
	 * @param groupName
	 *            Birds, Vascular Plants etc
	 * @param startFrom
	 *            the first record to read (for paging display)
	 * @param count
	 *            total records to read (for paging display)
	 * @return
	 */
	/*
	 * @RequestMapping(value = "/getSpeciesList_plain", produces =
	 * "application/json", method=RequestMethod.GET) public @ResponseBody
	 * Map<String, Object> getSpeciesListPlain(
	 * 
	 * @RequestParam(value = "group", required = false) String groupName,
	 * 
	 * @RequestParam(value = "speciesName", required = false) String
	 * speciesName, /*can be both common or scientific names *
	 * 
	 * @RequestParam(value = "total", required = false) Long total,
	 * 
	 * @RequestParam(value = "start", required = false) Integer startFrom,
	 * 
	 * @RequestParam(value = "length", required = false) Integer count,
	 * 
	 * @RequestParam(value = "order", required = false) List<HashMap<String,
	 * String>> order,
	 * 
	 * @RequestParam(value = "order", required = false) String[][] order2,
	 * 
	 * HttpServletRequest request
	 * 
	 * ) { System.out.println("getParameterMap =" + request.getParameterMap());
	 * System.out.println("getParameterNames =" + request.getParameterNames());
	 * return null;//bioBrowserService.getSpeciesList(groupName, speciesName,
	 * total, startFrom, count, order, CommonUtil.getServerPath(request));
	 * 
	 * }
	 * 
	 * @RequestMapping(value = "/getSpeciesList_all", produces =
	 * "application/json", method=RequestMethod.GET) public @ResponseBody
	 * Map<String, Object> getSpeciesListAll(
	 * 
	 * @ModelAttribute SpeciesDataTableCriterias spp,
	 * 
	 * HttpServletRequest request
	 * 
	 * ) {
	 * 
	 * return bioBrowserService.getSpeciesList(spp,
	 * CommonUtil.getServerPath(request));
	 * 
	 * }
	 */
	@RequestMapping(value = "/getSpeciesList", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getSpeciesList(@ModelAttribute SpeciesDataTableCriterias spp,

			HttpServletRequest request

	) {

		return bioBrowserService.getSpeciesList(spp, CommonUtil.getServerPath(request));

	}

	/**
	 * 
	 * @param groupId
	 * @param speciesName
	 * @return
	 */
	@RequestMapping(value = "/getAutoCompeleteSpecies", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody List<String> autoFillSpeciesName(
			@RequestParam(value = "groupId", required = false) Long groupId,
			@RequestParam(value = "mask", required = false) String speciesName /*
																				 * can
																				 * be
																				 * both
																				 * common
																				 * or
																				 * scientific
																				 * names
																				 */

	) {
		return bioBrowserService.getAutoCompleteSpeciesList(groupId, speciesName);

	}
	
	
	@RequestMapping(value = "/getSpeciesIds", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> autoFillSpeciesName(
		
			@RequestParam(value = "speciesName", required = false) String speciesName /*
																				 * can
																				 * be
																				 * both
																				 * common
																				 * or
																				 * scientific
																				 * names
																				 */

	) {
		return bioBrowserService.getIdsFromName(speciesName);

	}

	

	@RequestMapping(value = "/geSpeciesDetection", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> geSpeciesDetection(
			@RequestParam(value = "scientificName", required = true) String scientificName,
			@RequestParam(value = "groupId", required = false) Long groupId

	) {
		return bioBrowserService.getSpeciesDisribution(scientificName, groupId);

	}

	@RequestMapping(value = "/geSpeciesGraphs", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody Map<String, Map<String, String>> geSpeciesGraphs(
			@RequestParam(value = "tsn", required = true) int tsn,
			@RequestParam(value = "refresh", required = false) Boolean bCreateNew

	) {
		return graphService.getAllGraphs(tsn, (bCreateNew == null ? false : bCreateNew));

	}

	@RequestMapping(value = "/geSpeciesPDF", produces = "application/json", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> generateSpeciesPDF(
			@RequestParam(value = "tsn", required = true) Integer tsn,
			@RequestParam(value = "data", required = true) String content,
			HttpServletRequest request

	) {
		Gson g = new Gson();
		List<Map<String, Object>> profile;
		Map<String, String> result = new HashMap<String, String>();
		try {
			profile = (List<Map<String,Object>>) g.fromJson(URLDecoder.decode(content, "ISO-8859-1") ,
					List.class);
		
		
		PdfDownload pdfDownload = new PdfDownload ();
		pdfDownload.setRootPath(graphService.getRootPath());
		result.put("path", pdfDownload.createPDFBackEnd(bioBrowserService.getSppMixedRow(tsn) , bioBrowserService.getSppGraphStatus(tsn), profile));
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;

	}
	
	/**get a single species's guild property.
	 * 
	 * @param tsn
	 * @param guilds
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/geSpeciesGuilds", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody Map<String, Map<String,String>> getSpeciesGuilds(
			@RequestParam(value = "tsn", required = true) Integer tsn,
			@RequestParam(value = "name", required = true) String guildName,
			@RequestParam(value = "guilds[]", required = true) String[] guilds,
			HttpServletRequest request

	) {
		
		
		return guildServices.getGuilds(tsn, guildName, Arrays.asList(guilds));

	}
	
	/**
	 * get species count for all or the specified group. the value will be shown in the count up box in web pages
	 * @param groupId
	 * @param request
	 * @return
	 */
	
	@RequestMapping(value = "/getSpeciesCounter", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody Map<String, Long> getSpeciesCounter(
			@RequestParam(value = "groupId", required = false) Integer groupId,
			
			HttpServletRequest request

	) {		
		
		return bioBrowserService.getSpeciesCounter(groupId);

	}
	
	
	/**
	 * get species count for all or the specified group. the value will be shown in the count up box in web pages
	 * @param groupId
	 * @param request
	 * @return
	 */
	
	@RequestMapping(value = "/getPreviousNextTsn", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getSpeciesPrevNext(
			@RequestParam(value = "tsn", required = true) Integer tsn

	) {		
		
		return bioBrowserService.getPrevNextTsn(tsn);

	}
	
	
	@RequestMapping(value = "/getSpeciesName", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getSpeciesNames(
			@RequestParam(value = "name", required = true) String name

	) {		
		
		return bioBrowserService.getSpeciesNames(name);
		

	}
	
}
