package abmi.controller;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import abmi.model.services.ProjectPropertyService;
import abmi.model.services.RegionsService;
import abmi.model.services.rawdata.DataCategoryService;
import abmi.model.services.rawdata.DownloadPackageGenerator;
import javax.servlet.http.HttpServletRequest;
import abmi.model.services.rawdata.RawdataQueryCondition;
import abmi.model.util.WebContentSender;

@Controller
@RequestMapping(value = "/rawdata")
public class RawdataController {
	private static final Logger log = LoggerFactory
			.getLogger(RawdataController.class);
	@Autowired
	DataCategoryService dataDao;
	@Autowired
	RegionsService regionDao;
	@Autowired
	ProjectPropertyService prjDao;
	
	@Autowired
	DownloadPackageGenerator zipFileDAO;

	@RequestMapping(value = "/getDataCategory", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody
	List<Map<String,Object>> getAllProtocolVariable(

	) {

		return dataDao.getAPIAllProtocolVariable();
	}

	@RequestMapping(value = "/getRegion", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody
	List<Map<String,Object>> getRegion(

	) {

		return this.dataDao.getAPIAllRegion();
	}

	@RequestMapping(value = "/getRotation", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody
	List<Map<String,Object>> getRotation(

	) {

		return dataDao.getAPIRotationOptions();
	}

	@RequestMapping(value = "/getOffgridOption", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody
	List<Map<String,Object>> getOffgridOption(

	) {

		return dataDao.getAPICoreOffgridOptions();
	}

	
	
	
	@RequestMapping(value = "/getRawdataFile", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody
	Map<String, String> getRawdataFile(
			@RequestParam(value = "rawdata", required = true) Integer[] rawdata,
			@RequestParam(value = "rotation", required = false) Integer[] rotation,
			@RequestParam(value = "region", required = false) Integer[] region,
			@RequestParam(value = "offgridId", required = true) Integer offgridId,
			HttpServletRequest request

	) {
		
		log.debug("start to generate zip raw data file:" );
	
//		log.debug("request:" + request.getParameterMap());
		
		HashMap<String, String> result = new HashMap<String, String>();

		RawdataQueryCondition queryCondition = new RawdataQueryCondition();
		queryCondition.setProjectPropertyService(this.prjDao); // autowired not
																// working on
																// "new" object
		queryCondition.setDataCategoryService(dataDao);
		//
		queryCondition.setRegionsService(regionDao);

		if (rotation == null)
			queryCondition.setmRotationList(null);
		else
			queryCondition.setmRotationList(Arrays.asList(rotation));
		
		if (region == null)
			queryCondition.setmRegionIds(null);
		else
		queryCondition.setmRegionIds(Arrays.asList(region));
		
		queryCondition.setOffgridId(offgridId);
		
	
		

//		DownloadPackageGenerator downloadPackage = new DownloadPackageGenerator(
//				em);
		try {
			 zipFileDAO.setGeneralQuery(queryCondition,
					Arrays.asList(rawdata), null);
			 
			File zipFile = zipFileDAO.createZipFile();
			result.put("file", zipFile.getName());
			if ( zipFile.length() < 1024*1024)
				result.put("filesize", zipFile.length() / 1024 + "KB");
			else
				result.put("filesize", zipFile.length() / 1024/1024 + "MB");
			

	          WebContentSender.registerFileForDeletion(zipFile, 
	                                                      request.getSession());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

}
