package abmi.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import abmi.model.services.publication.PublicationService;

@Controller
@RequestMapping(value = "/publication")
public class PublicationController {

	// @Autowired
	// BioBrowserService bioBrowserService;

	private static final Logger log = LoggerFactory
			.getLogger(PublicationController.class);

	/**
	 * read list of species from database (information include rank name?,
	 * scientific names, common names, profile images) has more records for
	 * "loading more"?
	 * 
	 * @param groupName
	 *            Birds, Vascular Plants etc
	 * @param startFrom
	 *            the first record to read (for paging display)
	 * @param count
	 *            total records to read (for paging display)
	 * @return
	 */
	@RequestMapping(value = "/getPublication", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getPublication(
			@RequestParam(value = "root", required = false) String rootPath,
			@RequestParam(value = "documenttype[]", required = false) int[] documentTypes,
			@RequestParam(value = "subject[]", required = false) int[] subjects, /*
																				 * can
																				 * be
																				 * both
																				 * common
																				 * or
																				 * scientific
																				 * names
																				 */
			@RequestParam(value = "archive[]", required = false) String[] archives,
			@RequestParam(value = "tag", required = false) String[] tags,
			@RequestParam(value = "descriptionCount", required = false) int descriptionCount,
			@RequestParam(value = "titleCount", required = false) int titleCount,
			@RequestParam(value = "sortBy", required = false) String sortBy,
			@RequestParam(value = "offset", required = false) Long offset,
			@RequestParam(value = "pageSize", required = false) Long fetchSize,
			@RequestParam(value = "featuredOnly", required = false) Boolean showFeaturedOnly,

			HttpServletRequest request

	) {
		if (rootPath == null) {
			rootPath = " /home/publications";

		}

		if (showFeaturedOnly != null && showFeaturedOnly) {
			return PublicationService.getFeaturedPublication(rootPath,
					descriptionCount, titleCount,
					(offset == null ? 1L : offset), (fetchSize == null ? 20L
							: fetchSize));// b
		} else {
			Boolean sortByRelevant = ("hit".equals(sortBy));
			if ((subjects == null ? 0 : subjects.length)
					+ (documentTypes == null ? 0 : documentTypes.length) <= 1) {
				sortByRelevant = false;
			}
			return PublicationService.QueryPage(rootPath, documentTypes,
					subjects, archives, tags, sortByRelevant, descriptionCount,
					titleCount, (offset == null ? 1L : offset),
					(fetchSize == null ? 20L : fetchSize));// bioBrowserService.getSpeciesList(groupName,
															// speciesName,
															// total, startFrom,
															// count, order,
															// CommonUtil.getServerPath(request));
		}
	}

	@RequestMapping(value = "/getPublicationTags", produces = "application/json", method = RequestMethod.GET)
	public @ResponseBody
	LinkedHashMap<String, Object> getPublicationTags(

	) {

		return PublicationService.getPublicationSearchTags();

	}
}
