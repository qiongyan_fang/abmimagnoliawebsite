package abmi.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import abmi.module.template.util.TemplateUtil;

@Controller
@RequestMapping(value = "/glossary")
public class JCRNodeController {
//	@Autowired CacheRawdataService cachRaw;
	
	/**
	 * get glossary from component defined in a page. not ideal
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/getGlossaryDetails",  produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody Map<String, String> getUserGrid(
			@RequestParam(value = "name", required = false) String name

	) {
		HashMap<String, String> result = new HashMap<String, String>();
		result.put("name", name);
		String detail = TemplateUtil.getOneGlossaryItem(name);

		result.put("detail", detail );
	
		return result;
	}
/*
	@RequestMapping(value = "/getGlossary", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody Map<String, String> getGlossary(
			@RequestParam(value = "name", required = false) String name

	) {
		HashMap<String, String> result = new HashMap<String, String>();
		result.put("name", name);
		String detail = TemplateUtil.getOneGlossaryItem(name);

		result.put("detail", "my details" );
	
		return result;
	}

	

	*/
	
	/**
	 * get glossary from glossary workspace. this is the better way.
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/getGlossaryDefinition", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody Map<String, String> getGlossaryDefintion(
			@RequestParam(value = "name", required = false) String name

	) {
		HashMap<String, String> result = new HashMap<String, String>();
		result.put("name", name);
		String detail = TemplateUtil.getOneItemFromGlossaryWorkSpace(name);

		result.put("detail",  detail);
	
		return result;
	}

}
