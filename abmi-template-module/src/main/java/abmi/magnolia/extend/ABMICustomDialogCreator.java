package abmi.magnolia.extend;

import info.magnolia.module.blossom.dialog.DefaultDialogCreator;
import info.magnolia.module.blossom.dialog.DialogCreationContext;
import info.magnolia.module.blossom.dialog.DialogFactoryMetaData;
import info.magnolia.ui.form.definition.TabDefinition;
import info.magnolia.ui.form.field.definition.FieldDefinition;
import info.magnolia.ui.form.field.definition.RichTextFieldDefinition;

/**
http://forums.magnolia-cms.com/forum/thread.html?threadId=ff6c1cff-ab74-483b-a3cf-e02b963dc9f5#f63e6592-12c0-4c09-92be-8970e27e487d

	It needs to be configured in both the TemplateExporter and DialogExporter.

	<bean class="info.magnolia.module.blossom.template.TemplateExporter">

<property name="dialogDescriptionBuilder">

  <bean class="info.magnolia.module.blossom.dialog.DialogDescriptionBuilder">

    <property name="dialogCreator">

      <bean class="abmi.magnolia.extend.CustomDialogCreator" />

    </property>

  </bean>

</property>

</bean>



<bean class="info.magnolia.module.blossom.dialog.DialogExporter">

<property name="dialogDescriptionBuilder">

  <bean class="info.magnolia.module.blossom.dialog.DialogDescriptionBuilder">

    <property name="dialogCreator">

      <bean class="abmi.magnolia.extend.CustomDialogCreator" />

    </property>

  </bean>

</property>

</bean>

**/

public class ABMICustomDialogCreator extends DefaultDialogCreator {



    @Override

    public void createDialog(DialogFactoryMetaData metaData, DialogCreationContext context) throws Exception {

        super.createDialog(metaData, context);

        for (TabDefinition tab : context.getDialog().getForm().getTabs()) {

            for (FieldDefinition field : tab.getFields()) {

                // change the field here
            	if (field instanceof RichTextFieldDefinition){

            		((RichTextFieldDefinition) field).setSource(true);
            		((RichTextFieldDefinition) field).setImages(true);
            		((RichTextFieldDefinition) field).setTables(true);
            		((RichTextFieldDefinition) field).setAlignment(true);
            		
            		((RichTextFieldDefinition) field).setColors("135487,9B5407,104970,62AFA0,9B8732,701116,35340F");
//            		((RichTextFieldDefinition) field).setConfigJsFile("/docroot/js/ckeditor/ckeditorconfig.js"); // try it here
            		
            		
            	}

            }

        }

    }

}