package abmi.magnolia.extend;

import info.magnolia.cms.security.AccessDeniedException;
import info.magnolia.cms.security.AccessManager;
import info.magnolia.cms.security.Permission;
import info.magnolia.commands.impl.BaseRepositoryCommand;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.importexport.DataTransporter;
import info.magnolia.repository.RepositoryConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import abmi.model.util.ModuleParameters;

/**
 * Generic Export Command.<br>
 * <br>
 * 
 * Get as Input parameter: <br>
 * Workspace and node path to export <br>
 * <br>
 * Set as output: <br>
 * Fill the OutputStream with an XML representation of the Node structure<br>
 * Define the MimeType and File name.
 */
public class ABMIExportCommand extends BaseRepositoryCommand {

	private static final Logger log = LoggerFactory
			.getLogger(ABMIExportCommand.class);

	public static final String MIME_TEXT_XML = "application/octet-stream";
	public static final String MIME_GZIP = "application/x-gzip";
	public static final String MIME_APPLICATION_ZIP = "application/zip";

	private String ext = ".xml";
	public static final String EXPORT_EXTENSION = "ext";
	private boolean format = true;
	public static final String EXPORT_FORMAT = "format";
	private boolean keepHistory = true;
	public static final String EXPORT_KEEP_HISTORY = "keepHistory";
	private String fileName;
	private String outputPath;

	public String getOutputPath() {
		return outputPath;
	}

	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	public static final String EXPORT_FILE_NAME = "fileName";
	private String mimeExtension;
	public static final String EXPORT_MIME_EXTENSION = "mimeExtension";

	@Override
	public boolean execute(Context context) throws Exception {
		setRepository(StringUtils.isBlank(getRepository()) ? RepositoryConstants.WEBSITE
				: getRepository());
		setPath(StringUtils.isBlank(getPath()) ? "/" : getPath());

		log.debug(
				"Will export content from {} repository with uuid {} and path {}",
				new Object[] { getRepository(), getUuid(), getPath() });

		// Check Permission
		if (!checkPermissions(getRepository(), getPath(), Permission.READ)) {
			// escape to prevent XSS attack
			throw new AccessDeniedException(
					"Read permission needed for export. User not allowed to READ path ["
							+ StringEscapeUtils.escapeHtml4(getPath()) + "]");
		}
		// Define the MimeType based on the extension
		setMimeExtension(getContentType(ext));

		String pathName = DataTransporter.createExportPath(getPath());
		pathName = DataTransporter.encodePath(pathName, DataTransporter.DOT,
				DataTransporter.UTF8);
		if (DataTransporter.DOT.equals(pathName)) {
			// root node
			pathName = StringUtils.EMPTY;
		}

		String timeStamp = ModuleParameters.sdfull.format(new Date());
		setFileName(getRepository() + pathName + timeStamp + getExt());
		File outputFile = new File(getOutputPath(), getFileName());

		FileOutputStream fos = new FileOutputStream(outputFile);

		try {
			// Create the XML representation of the Node structure
			DataTransporter.executeExport(fos, keepHistory, format,
					MgnlContext.getJCRSession(getRepository()), getPath(),
					getRepository(), getExt());
			// Set the file name.

		} catch (RuntimeException e) {
			throw e;
		}

		return true;
	}

	/**
	 * Uses access manager to authorize this request.
	 * 
	 * @return boolean true if read access is granted
	 */
	public boolean checkPermissions(String repository, String basePath,
			long permissionType) {

		AccessManager accessManager = MgnlContext.getAccessManager(repository);
		if (accessManager != null) {
			if (!accessManager.isGranted(basePath, permissionType)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Define the Content Type based on the requested extension.
	 */
	private String getContentType(String ext) {
		if (ext.equalsIgnoreCase(DataTransporter.ZIP)) {
			return MIME_APPLICATION_ZIP;
		} else if (ext.equalsIgnoreCase(DataTransporter.GZ)) {
			return MIME_GZIP;
		} else {
			return MIME_TEXT_XML;
		}
	}

	/**
	 * @return the ext
	 */
	public String getExt() {
		return ext;
	}

	/**
	 * @param ext
	 *            the ext to set
	 */
	public void setExt(String ext) {
		this.ext = ext;
	}

	/**
	 * @return the format
	 */
	public boolean isFormat() {
		return format;
	}

	/**
	 * @param format
	 *            the format to set
	 */
	public void setFormat(boolean format) {
		this.format = format;
	}

	/**
	 * @return the keepHistory
	 */
	public boolean isKeepHistory() {
		return keepHistory;
	}

	/**
	 * @param keepHistory
	 *            the keepHistory to set
	 */
	public void setKeepHistory(boolean keepHistory) {
		this.keepHistory = keepHistory;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the mimeExtension
	 */
	public String getMimeExtension() {
		return mimeExtension;
	}

	/**
	 * @param mimeExtension
	 *            the mimeExtension to set
	 */
	public void setMimeExtension(String mimeExtension) {
		this.mimeExtension = mimeExtension;
	}

}
