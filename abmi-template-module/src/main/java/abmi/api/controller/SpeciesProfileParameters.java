package abmi.api.controller;

public class SpeciesProfileParameters {
	public static String[] Profile_Image_Fields = {"miniCircle","mainBanner", "speciesphoto",
			"malephoto", "femalephoto" };

	public static String Web_Photo_Field_Name = "WebPhotos";
	public static String Scientific_Field_Name = "ScientificName";
	public static String Common_Field_Name = "CommonName";
	public static String ABStatus_Field_Name = "ABStatus";
	public static String Apps_Field_Prefix = "App";
	public static String Web_Field_Prefix = "Web";
	public static String PROFILE_IMAGE_NODE = "Web";
	public static String WEB_PHOTO_SUB_FILED_NAME = "Image";

	public static String[] AllSpeciesGroups = { "All", "Birds", "Bryophytes",
			 "Lichens", "Mammals", "Reptiles",
			"Soil Mites", "Plants" };

	public static String Profile_Mini_Circle = "miniCircle";
	public static String Profile_Main_Banner = "mainBanner";
}
