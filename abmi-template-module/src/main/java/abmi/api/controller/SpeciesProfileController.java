package abmi.api.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import abmi.api.services.speciesprofiles.SpeciesProfileDAO;
import abmi.model.util.CommonUtil;

@Controller
@RequestMapping(value = "/speciesProfile")
public class SpeciesProfileController {

	/**
	 * get species profile information
	 * 
	 * @param scientificName
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getSingleSpeciesProfile", headers = "Accept=*/*", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getProfile(
			@RequestParam(value = "scientificName", required = false) String scientificName,
			@RequestParam(value = "forApps", required = false) Boolean bForApps, /**
			 * 
			 * 
			 * 
			 * if it is for apps, fetch information related to apps only
			 **/
			HttpServletRequest request

	) {

		Map<String, Object> detail = SpeciesProfileDAO.getSingleSpeciesProfile(
				bForApps, scientificName, CommonUtil.getServerPath(request));

		return detail;
	}

	@RequestMapping(value = "/getSpeciesList", headers = "Accept=*/*", produces = "application/json", method=RequestMethod.GET)
	/**
	 * 
	 * @param speciesGroup
	 * @param speciesName
	 * @param bStartsWith
	 * @param totalRow
	 * @param startRow
	 * @param fetchSize
	 * @param request
	 * @return
	 */
	public @ResponseBody
	Map<String, Object> getListOfSpecies(

			@RequestParam(value = "speciesGroup", required = false) String speciesGroup,
			@RequestParam(value = "searchName", required = false) String speciesName,
			@RequestParam(value = "isStartsWith", required = false) Boolean bStartsWith, /*
																						 * default
																						 * :
																						 * false
																						 * ,
																						 * true
																						 * :
																						 * starts
																						 * ,
																						 * false
																						 * :
																						 * contains
																						 */
			@RequestParam(value = "totalRow", required = false) Long totalRow,
			@RequestParam(value = "offset", required = false) Long startRow,
			@RequestParam(value = "pageSize", required = false) Long fetchSize,
			HttpServletRequest request

	) {
		
		

		Map<String, Object> detail = SpeciesProfileDAO.getListOfSpeices(
				speciesGroup, speciesName, (bStartsWith == null ? false
						: bStartsWith), totalRow, startRow, fetchSize,
				CommonUtil.getServerPath(request));

		return detail;
	}

	@RequestMapping(value = "/getSpeciesCount", headers = "Accept=*/*", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody
	LinkedHashMap<String, Long> getSpeciesCount(

	) {

		LinkedHashMap<String, Long> detail = SpeciesProfileDAO.getSpeciesProfileCount();

		return detail;
	}

	
	@RequestMapping(value = "/getAutoFillSpeciesName", headers = "Accept=*/*", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody
	List<String> getAutoFillSpeciesName(
			@RequestParam(value = "group", required = false) String groupName,
			@RequestParam(value = "mask", required = false) String speciesName /*can be both common or scientific names */


	) {

		return SpeciesProfileDAO.getAutoCompleteSpeciesList(groupName, speciesName);

		
	}

}
