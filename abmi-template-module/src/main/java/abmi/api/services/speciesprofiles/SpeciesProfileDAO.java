package abmi.api.services.speciesprofiles;

import info.magnolia.cms.util.QueryUtil;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.jcr.util.SessionUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Binary;
import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFactory;
import javax.jcr.query.InvalidQueryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import abmi.api.controller.SpeciesProfileParameters;
import abmi.module.template.util.TemplateUtil;

public class SpeciesProfileDAO {

	/**
	 * select * from [mgnl:content] where upper(ScientificName)='TEST'
	 * 
	 * @param scientificName
	 * @return
	 */
	private static final Logger log = LoggerFactory.getLogger(SpeciesProfileDAO.class);

	/**
	 * 
	 * @param bForApps
	 * @param scientificName
	 * @param serverPath
	 * @return
	 */
	static public Map<String, Object> getSingleSpeciesProfile(Boolean bForApps, String scientificName,
			String serverPath) {

		Map<String, Object> speciesResult = new HashMap<String, Object>();
		if (scientificName == null || "".equals(scientificName)) {
			speciesResult.put("error", "No Scientific Name was specified");
			return speciesResult;
		}

		try {
			String query = "select * from [mgnl:content] where upper(" + SpeciesProfileParameters.Scientific_Field_Name
					+ ")='" + scientificName.toUpperCase() + "'";
			NodeIterator results = QueryUtil.search("profiles", query);

			if (results.hasNext()) {
				Node node = (Node) results.next();

				PropertyIterator pIter = node.getProperties();
				while (pIter.hasNext()) {

					Property property = pIter.nextProperty();
					if (!property.getName().startsWith("mgnl:") && !property.getName().startsWith("jcr:") && ((bForApps
							&& !property.getName().startsWith(SpeciesProfileParameters.Web_Field_Prefix)) /*
																											 * search
																											 * apps
																											 * fields
																											 * ,
																											 * don
																											 * 't
																											 * included
																											 * web
																											 * field
																											 */
							|| (!bForApps
									&& !property.getName().startsWith(SpeciesProfileParameters.Apps_Field_Prefix)) /*
																													 * search
																													 * web
																													 * fields
																													 * ,
																													 * don
																													 * 't
																													 * included
																													 * app
																													 * field
																													 */
					)) {
						speciesResult.put(property.getName().toString(), property.getString());
					}
				}
				// get node's children nodes for properties

				for (Node subNode : NodeUtil.getNodes(node)) {
					speciesResult.put(subNode.getName(),
							TemplateUtil.getMultiFieldREST(node, subNode.getName(), serverPath));
				}

				return speciesResult;

			} else {
				speciesResult.put("error", "species profiles not found for (" + scientificName + ")");
				return speciesResult;
			}

		} catch (LoginException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		speciesResult.put("error", "error occured when searching for (" + scientificName + ")");

		return speciesResult;

	}

	/**
	 * 
	 * @param scientificName
	 * @param imageType
	 * @param serverPath
	 * @return
	 */
	static public String getSingleSpeciesProfileImage(String scientificName, String imageType, String serverPath) {

		if (scientificName == null || "".equals(scientificName)) {

			return null;
		}

		try {
			String query = "select * from [mgnl:content] where upper(" + SpeciesProfileParameters.Scientific_Field_Name
					+ ")='" + scientificName.toUpperCase() + "'";
			NodeIterator results = QueryUtil.search("profiles", query);

			if (results.hasNext()) {

				Node node = (Node) results.next();

				return getProfilePhotoREST(node, SpeciesProfileParameters.Web_Photo_Field_Name,
						Arrays.asList(imageType), serverPath);

			} else {

				return null;
			}

		} catch (LoginException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		return null;

	}

	/**
	 * 
	 * @param scientificName
	 * @param imageType
	 * @param serverPath
	 * @return
	 */
	static public byte[] getSingleSpeciesProfileImageByte(String scientificName, String imageType) {

		if (scientificName == null || "".equals(scientificName)) {

			return null;
		}

		try {
			String query = "select * from [mgnl:content] where upper(" + SpeciesProfileParameters.Scientific_Field_Name
					+ ")='" + scientificName.toUpperCase() + "'";
			NodeIterator results = QueryUtil.search("profiles", query);

			if (results.hasNext()) {

				Node content = (Node) results.next();

				try {

					for (Node node : NodeUtil
							.getNodes(content.getNode(SpeciesProfileParameters.Web_Photo_Field_Name))) {

						if (imageType.equals(PropertyUtil.getString(node, "imageType", ""))) {

							String path = PropertyUtil.getString(node, "imageLink", "");

							Node subnode = SessionUtil.getNode("dam", path);

							// Binary bin =
							// subnode.getProperty("jcr:data").getBinary();
							Binary bin = subnode.getNode("jcr:content").getProperty("jcr:data").getBinary();
							InputStream is = bin.getStream();

							ByteArrayOutputStream buffer = new ByteArrayOutputStream();
							int nRead;
							byte[] data = new byte[1024];
							try {
								while ((nRead = is.read(data, 0, data.length)) != -1) {
									buffer.write(data, 0, nRead);
								}

								buffer.flush();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							return buffer.toByteArray();

						}

					}
				} catch (javax.jcr.PathNotFoundException err) {

				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {

				return null;
			}

		} catch (LoginException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		return null;

	}

	/**
	 * 
	 * @param speciesGroup
	 * @param name
	 * @param bStartsWith
	 * @param totalRow
	 * @param startRow
	 * @param fetchSize
	 * @param serverPath
	 * @return
	 */
	static public Map<String, Object> getListOfSpeices(String speciesGroup, String name, Boolean bStartsWith,
			Long totalRow, Long startRow, Long fetchSize, String serverPath) {

		log.debug(" 1." + speciesGroup + " 2." + name + " empty ? " + (name != null && !"".equals(name)));

		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> speciesResult = new ArrayList<Map<String, Object>>();

		String query = "select * from [mgnl:content] ";
		String connectStr = " where ";
		if (name != null && !"".equals(name)) {

			query += connectStr + " (upper(" + SpeciesProfileParameters.Scientific_Field_Name + ") like '"
					+ (bStartsWith ? "" : "%") + name.toUpperCase() + "%' or" + " upper("
					+ SpeciesProfileParameters.Common_Field_Name + ") like '" + (bStartsWith ? "" : "%")
					+ name.toUpperCase() + "%')";

			connectStr = " and ";
		}

		if (speciesGroup != null && !"".equals(speciesGroup)) {
			query += connectStr + " ISDESCENDANTNODE([/" + speciesGroup + "])";
		}

		query += " order by " + SpeciesProfileParameters.Common_Field_Name;

		try {

			NodeIterator queryResults = QueryUtil.search("profiles", query);

			if (totalRow == null || totalRow < 1) {
				// NodeIterator results = QueryUtil.search("profiles", query);

				totalRow = queryResults.getSize();

			}

			if (startRow == null || startRow < 0) {
				startRow = 1l;
			}

			if (fetchSize == null || fetchSize < 1) {
				fetchSize = 25l;
			}

			long count = 1;
			while (queryResults.hasNext() && (totalRow == -1 || count <= startRow + fetchSize)) {
				Node node = (Node) queryResults.next();
				if (count >= startRow && count < startRow + fetchSize) {
					Map<String, Object> valueMap = new HashMap<String, Object>();
					valueMap.put("scientificName",
							PropertyUtil.getString(node, SpeciesProfileParameters.Scientific_Field_Name));
					valueMap.put("commonName",
							PropertyUtil.getString(node, SpeciesProfileParameters.Common_Field_Name, ""));
					valueMap.put("status",
							PropertyUtil.getString(node, SpeciesProfileParameters.ABStatus_Field_Name, ""));
					valueMap.put("porfileImage",
							getProfilePhotoREST(node, SpeciesProfileParameters.Web_Photo_Field_Name,
									Arrays.asList(SpeciesProfileParameters.Profile_Image_Fields), serverPath));

					speciesResult.add(valueMap);
				}
				count++;
			}

			if (totalRow < count) {
				totalRow = count;
			}

			resultMap.put("totalRow", totalRow);
			resultMap.put("startRow", startRow);
			resultMap.put("fetchSize", fetchSize);

			resultMap.put("items", speciesResult);
			return resultMap;

		} catch (LoginException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		resultMap.put("error", "error occured when searching for (" + speciesGroup + " " + name + ")");

		return resultMap;
	}

	/**
	 * used for rest, have abosolute path to assets it get the first hit image
	 * of given image type.
	 * 
	 * @param content
	 * @param fieldName
	 * @return
	 */
	public static String getProfilePhotoREST(Node content, String fieldName, List<String> imageTypes,
			String serverPath) {

		try {
			for (String imageType : imageTypes) {
				for (Node node : NodeUtil.getNodes(content.getNode(fieldName))) {

					if (imageType.equals(PropertyUtil.getString(node, "imageType", ""))) {
						String path = PropertyUtil.getString(node, "imageLink", "");
						return TemplateUtil.getAssetPath(serverPath, path);
					}
				}

			}
		} catch (javax.jcr.PathNotFoundException err) {

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "";
	}

	public static LinkedHashMap<String, Long> getSpeciesProfileCount() {
		LinkedHashMap<String, Long> resultMap = new LinkedHashMap<String, Long>();
		for (String speciesGroup : SpeciesProfileParameters.AllSpeciesGroups) {
			resultMap.put(speciesGroup, getSingleSpeciesProfileCount(speciesGroup.equals("All") ? "" : speciesGroup));
		}

		return resultMap;
	}

	static Long getSingleSpeciesProfileCount(String speciesGroup) {

		String query = "select * from [mgnl:content] ";
		String connectStr = " where ";

		if (speciesGroup != null && !"".equals(speciesGroup)) {
			query += connectStr + " ISDESCENDANTNODE([/" + speciesGroup + "])";
		}

		NodeIterator queryResults;
		try {

			queryResults = QueryUtil.search("profiles", query);
			long count = queryResults
					.getSize(); /**
								 * I tried to use getSize(), but getSize returns
								 * -1, so I have to loop through resutls to get
								 * count
								 **/
			if (count == -1) {
				count = 0;
				while (queryResults.hasNext()) {
					queryResults.next();
					count++;
				}
			}

			return count;
		} catch (InvalidQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0L;
	}

	static public List<String> getAutoCompleteSpeciesList(String groupName, String speciesName) {

		List<String> resultList = new ArrayList<String>();

		if (speciesName == null || speciesName.length() == 0) {

			return resultList;
		}
		/* create order by Caluse */

		try {
			String query = "select * from [mgnl:content] where (upper(" + SpeciesProfileParameters.Scientific_Field_Name
					+ ") like '%" + speciesName.toUpperCase() + "%'  or upper("
					+ SpeciesProfileParameters.Common_Field_Name + ") like '%" + speciesName.toUpperCase() + "%')";
			NodeIterator results = QueryUtil.search("profiles", query);
			while (results.hasNext()) {
				Node node = (Node) results.next();
				String sname = PropertyUtil.getString(node, SpeciesProfileParameters.Scientific_Field_Name, "");
				String cname = PropertyUtil.getString(node, SpeciesProfileParameters.Common_Field_Name, "");
				if (cname != null && cname.toLowerCase().indexOf(speciesName.toLowerCase()) != -1) {
					resultList.add(cname);
				} else {
					resultList.add(sname);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultList;
	}
}
