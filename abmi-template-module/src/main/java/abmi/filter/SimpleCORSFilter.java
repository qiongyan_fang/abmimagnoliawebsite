package abmi.filter;

import info.magnolia.cms.filters.AbstractMgnlFilter;

import java.io.IOException;

import javax.servlet.FilterChain;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**Why this is need?
 * 1. to send ajax request to a cross domain server. by default "Access-cotnrol-allow-orgin" is not set. so on the server side, it has be 
 * to set to accept the incoming server ip/domain, or simply set it as *
 * 
 * 2. this is a magnolia filter by extending abstractMgnlFilter. it is the same or regular filter by implements Filter
 * 3. configuration: 
 * 		in regular java, register the filter in web.xml
 * 		in magnolia: register the filter in the /server/filter/ after contentType Filter
 * 				CORSFilter
 * 					-[x]bypasses
 * 							-[allExceptAjaxRequest]
 * 									-. class info.magnolia.voting.voters.URIStartsWithVoter
 * 									-. pattern	/.ajax
 * 
 * 					-class abmi.filter.SimpleCORSFilter
 * 
 * 		however /docroot is not by passes, so I hard coded it in the filter.
 * 
 * @author Qiongyan
 *
 */

public class SimpleCORSFilter extends AbstractMgnlFilter  {

	private static final Logger log = LoggerFactory.getLogger(SimpleCORSFilter.class);

	
  
	
	@Override
	public void doFilter(HttpServletRequest request,
			HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		if (!request.getRequestURI().startsWith("/docroot")){
				
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
			response.setHeader("Access-Control-Max-Age", "3600");
			response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		}
			
			chain.doFilter(request, response);
	
	}
		
	
}