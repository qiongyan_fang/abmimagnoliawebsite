package abmi.model.datatable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataTableCriterias {
	 private int draw;
	    private int start;
	    private int length;

	    private Map<SearchCriterias, String> search;

	    private List<Map<OrderCriterias, String>> order;

	    private List<Map<Column,String>> columns;

	    public DataTableCriterias() {
	    	columns = new ArrayList<Map<Column,String>>();
	    	order = new ArrayList<Map<OrderCriterias, String>> ();
	    	search = new HashMap<SearchCriterias, String> ();
	    }
	    
	    public List<Map<Column, String>> getColumns() {
			return columns;
		}

		public void setColumns(List<Map<Column, String>> columns) {
			this.columns = columns;
		}

		public enum SearchCriterias {
	        value,
	        regex
	    }

	    public int getDraw() {
			return draw;
		}

		public void setDraw(int draw) {
			this.draw = draw;
		}

		public int getStart() {
			return start;
		}

		public void setStart(int start) {
			this.start = start;
		}

		public int getLength() {
			return length;
		}

		public void setLength(int length) {
			this.length = length;
		}

		public Map<SearchCriterias, String> getSearch() {
			return search;
		}

		public void setSearch(Map<SearchCriterias, String> search) {
			this.search = search;
		}

		public List<Map<OrderCriterias, String>> getOrder() {
			return order;
		}

		public void setOrder(List<Map<OrderCriterias, String>> order) {
			this.order = order;
		}

		

		public enum OrderCriterias {
	        column,
	        dir
	    }

	    public enum Column {
//	        private String data;
//	        private String name;
//	        private boolean searchable;
//	        private boolean orderable;
//	     
//			private Map<SearchCriterias, String> search;
	    	 data,
	         name,
	         searchable,
	         orderable,
	         searchValue,
	         searchRegex
	    }
}
