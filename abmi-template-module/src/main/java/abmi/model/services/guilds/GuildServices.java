package abmi.model.services.guilds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;


import abmi.model.entity.guild.SppGuildMap;
import abmi.model.entity.guild.SppGuildValue;
import abmi.model.util.CommonUtil;

@Service
public class GuildServices {
	@PersistenceContext(unitName = "data")
	EntityManager em;
	
	/**
	 * 
	 * @param tsn
	 * @param guilds
	 * @return
	 */
	public Map<String, Map<String,String>> getGuilds(int tsn, String guildName, List<String> guilds){
		TypedQuery<SppGuildMap> query = em.createQuery(
				"select  p from SppGuildMap p where p.abmiTaxonomicUnit.atuTsnid =:tsn and "
				+ " p.speciesGuildDefinition.sgdName in :guilds",
				SppGuildMap.class);
		
		query.setParameter("tsn", tsn);
		query.setParameter("guilds", guilds);

		Map<String, Map<String,String>> mapResult = new HashMap<String, Map<String,String>>();
		
		Map<String, String> mapRow = new HashMap<String, String>();
		try {
			for (SppGuildMap singleMap: CommonUtil.emptyIfNull(query.getResultList())){
				String values = "";
				boolean bEditable = (singleMap.getSpeciesGuildDefinition().getSgdEditable().intValue() == 1);
				
				for (SppGuildValue singleValue: CommonUtil.emptyIfNull(singleMap.getSppGuildValues())){
					if (bEditable){
						values += singleValue.getSgvValue() + " ";
					}
					else{
						values += singleValue.getSppGuildDefinitionOption().getSgdoOptions();
					}
				}
				
			
				mapRow.put(singleMap.getSpeciesGuildDefinition().getSgdName(), values);
				
				
			}
			
			if (mapRow!= null && mapRow.size() > 0){
				mapResult.put(guildName, mapRow);
			}
			
			
			
		} catch (Exception e) {
			
		}
		
		return mapResult;
	}
	
	
}
