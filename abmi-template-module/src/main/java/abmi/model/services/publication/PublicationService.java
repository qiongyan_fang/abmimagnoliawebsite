package abmi.model.services.publication;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import org.apache.jackrabbit.value.ValueFactoryImpl;
import org.springframework.stereotype.Service;

import abmi.model.util.ModuleParameters;
import abmi.module.template.util.TemplateUtil;

@Service
public class PublicationService {

	static public Map<String, Object>  QueryPage(String rootPath, int[] documentTypes, int[] subjects,
			String[] archives, String[] tags, Boolean isSortByRelevant, Integer descriptionCount, Integer titleCount,
			Long start, Long fetchSize) {

		try {
			Session session = MgnlContext.getJCRSession("website");

			Calendar cal = Calendar.getInstance();
			String todayStr = ValueFactoryImpl.getInstance().createValue(cal).getString();

			String countQueryString = "select * from [mgnl:page] as t where  (hideInNavigation is null or hideInNavigation = false) and "
					+ " publishdate <= cast('" + todayStr + "' as date) and "
					+ /*
						 * after publishdate
						 */
					"ISDESCENDANTNODE([" + rootPath + "]) ";

			String sqlConnector = " and ";
			String documentSQL = "";
			for (int i = 0; documentTypes != null && i < documentTypes.length; i++) {
				String fieldName = "documenttype";

				if (documentTypes[i] < 10) {
					fieldName = "documentmaintype";
				}
				documentSQL += "(" + fieldName + " like '%," + documentTypes[i] + "' or " + fieldName + " like '%,"
						+ documentTypes[i] + "%,' or " + fieldName + " like '" + documentTypes[i] + ",%'  or "
						+ fieldName + " ='" + documentTypes[i] + "') ";
				if (i < documentTypes.length - 1) {
					documentSQL += " or ";
				}
			}

			if (documentSQL.length() > 1) {
				countQueryString += sqlConnector + "(" + documentSQL + ")";
			}
			String subjectSQL = "";
			for (int i = 0; subjects != null && i < subjects.length; i++) {
				subjectSQL += "  (docsubject = '" + subjects[i] + "'  or docsubject like '%," + subjects[i]
						+ ",%'  or docsubject like '%," + subjects[i] + "'  or docsubject like '" + subjects[i]
						+ ",%')";
				if (i < subjects.length - 1) {
					subjectSQL += " or ";
				}
			}

			if (subjectSQL.length() > 1) {
				countQueryString += sqlConnector + "(" + subjectSQL + ")";
			}
			String timeSQL = "";
			for (int i = 0; archives != null && i < archives.length; i++) {

				String newQueryValue = "";
				if (archives[i].indexOf(" ") != -1) {
					newQueryValue = archives[i].replace(" ", "%");
				} else {
					newQueryValue = "%" + archives[i];
				}
				timeSQL += "  displaydate  like '" + newQueryValue + "' ";

				if (i < archives.length - 1) {
					timeSQL += " or ";
				}

			}
			if (timeSQL.length() > 1) {
				countQueryString += sqlConnector + "(" + timeSQL + ")";
			}
			String tagSQL = "";
			for (int i = 0; tags != null && i < tags.length; i++) {
				if (tags[i] == null || "".equals(tags[i])) {
					continue;
				}
				tagSQL += "  dockeyword like '%" + tags[i].trim() + "%'";
				if (i < tags.length - 1) {
					tagSQL += " or ";
				}

			}
			if (tagSQL.length() > 1) {
				countQueryString += sqlConnector + "(" + tagSQL + ")";
			}

			QueryManager manager = session.getWorkspace().getQueryManager();

			String queryString = countQueryString;

			if (isSortByRelevant == null || isSortByRelevant == false) {
				queryString += " order by  publishdate desc, title";
			}

			// System.out.println(queryString);
			javax.jcr.query.Query query = manager.createQuery(queryString, javax.jcr.query.Query.JCR_SQL2);

			QueryResult result = query.execute();

			NodeIterator nodeIter = result.getNodes();

			// String month =
			// AllPublicationPageTemplate.sdf.format(calendar.getTime());
			// int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			
			
			Map<String, Object> resultMap = new HashMap<String, Object>();
			ArrayList<Map<String, Object>> selectedList = new ArrayList<Map<String, Object>>();
			Map<String, Integer> matchList = new HashMap();
			long count = 1;
			long totalSize = nodeIter.getSize();

			
			resultMap.put("total", totalSize);
			

			while (nodeIter != null && nodeIter.hasNext()) {

				Node node = (Node) nodeIter.next();

				if (isSortByRelevant != null && isSortByRelevant == true) {
					matchList.put(node.getPath(), countMatches(node, isSortByRelevant, documentTypes, subjects));
				} else {

					if (count >= start && count < start + fetchSize) {
						selectedList.add(getOverviewPublicationInfo(node,

								descriptionCount == null ? 200 : descriptionCount,
								titleCount == null ? 30 : titleCount));
					}
					count++;
					if (count > start + fetchSize) {
						break;
					}
				}
			}

			if (isSortByRelevant != null && isSortByRelevant == true) {

				Map<String, Integer> sortedMap = sortByComparator(matchList);
				count = 1;
				for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
					if (count >= start && count < start + fetchSize) {
						selectedList.add(getOverviewPublicationInfo(session.getNode(entry.getKey()),

								descriptionCount == null ? 200 : descriptionCount,
								titleCount == null ? 30 : titleCount));
					}
					count++;
					if (count > start + fetchSize) {
						break;
					}
				}
			}

			resultMap.put("items", selectedList);
			return resultMap;

		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static Integer countMatches(Node subNode, Boolean isSortByRelevant, int[] documentTypes, int[] subjects) {
		int matchHit = 0;
		if (isSortByRelevant != null && isSortByRelevant == true) {

			String documentTypeContent = PropertyUtil.getString(subNode, "documenttype", "");
			String documentMainTypeContent = PropertyUtil.getString(subNode, "documentmaintype", "");
			for (int i = 0; documentTypes != null && i < documentTypes.length; i++) {
				if (documentTypes[i] < 10) {
					if (documentMainTypeContent.indexOf(documentTypes[i]) != -1) {
						matchHit++;
					}
				} else if (documentTypeContent.indexOf(documentTypes[i]) != -1) {
					matchHit++;
				}

			}

			String subjectContent = PropertyUtil.getString(subNode, "docsubject", "");
			for (int i = 0; subjects != null && i < subjects.length; i++) {
				if (subjectContent.indexOf(subjects[i]) != -1) {
					matchHit++;
				}
			}

		}

		return matchHit;

	}

	public static Map<String, Object> getOverviewPublicationInfo(Node subNode,

			int descriptionCount, int titleCount) {
		Map<String, Object> singleProject = new LinkedHashMap<String, Object>();

		singleProject.put("id",
				TemplateUtil.getCharByNumber(PropertyUtil.getString(subNode, "fileid", ""), titleCount));
		
		singleProject.put("doctitle",
				TemplateUtil.getCharByNumber(PropertyUtil.getString(subNode, "title", ""), titleCount));

		singleProject.put("author", PropertyUtil.getString(subNode, "author", ""));

		try {
			singleProject.put("displaydate", PropertyUtil.getString(subNode, "displaydate",
					ModuleParameters.sdfull.format(PropertyUtil.getDate(subNode, "publishdate").getTime())));
		} catch (Exception e) {
			e.printStackTrace();
		}

		singleProject.put("versioncount", PropertyUtil.getLong(subNode, "versioncount", 0L));

		String doi = PropertyUtil.getString(subNode, "doi", "");
		if ("".equals(doi)) {
			String link = PropertyUtil.getString(subNode, "viewlink", "");
			singleProject.put("viewlink", link);

			Map<String, String> fileProperties = TemplateUtil.getAssetsProperty(link);
			if (fileProperties != null) {
				singleProject.put("extension", fileProperties.get("extension"));
				singleProject.put("size", fileProperties.get("size"));
			}
		} else {
			singleProject.put("doi", doi);
		}

		String flipbookLink = PropertyUtil.getString(subNode, "flipbooklink", "");
		String flipbookLinkText = PropertyUtil.getString(subNode, "flipbooklinktext", "");
		singleProject.put("flipbookLink", flipbookLink);
		singleProject.put("flipbookLinkText", flipbookLinkText);

		// extra information for detailed publication views

		String coverImageUrl = PropertyUtil.getString(subNode, "coverImageUrl", "");
		singleProject.put("coverImageUrl", TemplateUtil.getAssetPath("", coverImageUrl));

		// use org image path for large image <img alt=""
		// src="/.imaging/coverthumbnail/dam//home/publications/images/401-450/410_non_native_KirstanTereschyn_Cirsium_arvense_nearCastorAB--2-.">
		singleProject.put("imageUrl", PropertyUtil.getString(subNode, "imageUrl", ""));

		singleProject.put("description",
				TemplateUtil.getCharByNumber(PropertyUtil.getString(subNode, "description", ""), descriptionCount));

		singleProject.put("hasMultiversion", PropertyUtil.getBoolean(subNode, "ismultiversion", false));

		try {
			singleProject.put("parentNodeName", PropertyUtil.getString(subNode.getParent(), "name", ""));
		} catch (Exception e) {

		}

		try {
			singleProject.put("path", subNode.getPath());
		} catch (Exception e) {

		}

		return singleProject;
	}

	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap) {

		// Convert Map to List
		List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**/
	public static LinkedHashMap<String, Object> getPublicationSearchTags() {
		return TemplateUtil.getPublicationTags(false);

	}

	static public Map<String, Object> getFeaturedPublication(String rootPath, Integer descriptionCount,
			Integer titleCount, Long start, Long fetchSize) {

		try {
			Session session = MgnlContext.getJCRSession("website");

			ArrayList<String> featuredPath = TemplateUtil.getMultiFieldValues(session.getNode(rootPath), "featured");
			Map<String, Object> resultMap = new HashMap<String, Object>();
			ArrayList<Map<String, Object>> selectedList = new ArrayList<Map<String, Object>>();
			Map<String, Integer> matchList = new HashMap();
			long count = 1;
		
			long totalSize = featuredPath.size();
			

			for (String itemPath : TemplateUtil.emptyIfNull(featuredPath)) {
				if (count >= start && count < start + fetchSize) {
					selectedList.add(getOverviewPublicationInfo(session.getNode(itemPath),
							descriptionCount == null ? 200 : descriptionCount, titleCount == null ? 30 : titleCount));
				}
				count++;

				if (count >= start + fetchSize) {
					break;
				}

			}

			resultMap.put("total", totalSize);
			resultMap.put("items", selectedList);
			return resultMap;

		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
