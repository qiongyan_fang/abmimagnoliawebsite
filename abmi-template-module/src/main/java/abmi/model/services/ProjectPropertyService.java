package abmi.model.services;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import abmi.model.entity.metadata.GProjectSetting;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProjectPropertyService {
	private static final Logger log = LoggerFactory
			.getLogger(ProjectPropertyService.class);

	@PersistenceContext(unitName = "data")
	private EntityManager em;

	String rootPath, tempPath;

	String metadataPdfPath, methodPdfPath, rawdatacoverxmlPath,
			rawdatacoverHeaderImagPath;

	public ProjectPropertyService() {
		
	}

	public String getRootPath() {
		if (rootPath != null) {
			return rootPath;
		}
		String os = System.getProperty("os.name").toLowerCase();
		if (os.indexOf("window") != -1) {
			try {
				this.rootPath = getProperty("FILE_ROOT_PATH_WINDOWS");
			} catch (Exception e) {
				this.rootPath = "c:\\ABMITMP";

			}
		} else if (os.indexOf("linux") != -1 || os.indexOf("unix") != -1) {
			this.rootPath = getProperty("FILE_ROOT_PATH_UNIX");

		} else {
			this.rootPath = "/ABMIWEBDATA";
		}
//		log.debug(os + " RootPATH" + rootPath);

		return rootPath;

	}

	/**
	 * 
	 * @param propertyName
	 * @return
	 */
	public String getProperty(String propertyName) {

		try {

			GProjectSetting row = em.find(GProjectSetting.class, propertyName);
			if (row == null)
				return null;

			return row.getGpsValue();
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		return null;
	}

	/**
	 * 
	 * @param propertyName
	 * @return
	 */
	public String getPath(String propertyName) {
		String root = getRootPath();
		if (root == null)
			return null;
		return root + "/" + getProperty(propertyName) + "/";
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	/**
	 * 
	 * @return
	 */
	public String getTempPath() {

		if (this.tempPath != null) {

			return this.tempPath;
		}
		String path = getProperty("TEMP_PATH");

		if (path == null || path.trim().equals(""))
			tempPath = System.getProperty("java.io.tmpdir");
		else
			tempPath = path;

		if (tempPath == null) {
			throw new RuntimeException(
					"Temporary directory system property (java.io.tmpdir) is null");
		}

		return tempPath;
	}

	/**
	 * 
	 * @return
	 */
	public String getMetadataPdfPath() {
		if (metadataPdfPath != null)
			return metadataPdfPath;
		metadataPdfPath = getPath("RAW_METADATA_DOCUMENT_PATH");

		return metadataPdfPath;
	}

	String rawCsvPath;

	public String getRawCsvPath() {
		if (rawCsvPath != null)
			return rawCsvPath;
		rawCsvPath = getPath("ARCHIVE_PATH");

		return rawCsvPath;
	}

	public String getMethodPdfPath() {
		if (methodPdfPath != null)
			return methodPdfPath;
		methodPdfPath = getPath("PROTOCOL_DESCRIPTION_PDF_PATHS");

		return methodPdfPath;
	}

	public String getRawdatacoverxmlPath() {

		if (rawdatacoverxmlPath != null)
			return rawdatacoverxmlPath;
		rawdatacoverxmlPath = getPath("RAWDATA_DISCRIBER_ XML_TEMPLATE_PATH");
		return rawdatacoverxmlPath;

	}

	public String getRawdatacoverHeaderImagPath() {

		if (rawdatacoverHeaderImagPath != null)
			return rawdatacoverHeaderImagPath;
		rawdatacoverHeaderImagPath = getPath("RAWDATA_DISCRIBER_HEADER_IMAGE_PATH");
		return rawdatacoverHeaderImagPath;
	}

	public List<String> getAdditionalDownloadFiles() {
		TypedQuery<String> docQuery = em.createQuery(
				"SELECT p.gdfFileName FROM GAdditionalDownloadFile p",
				String.class);

		return docQuery.getResultList();
	}

}
