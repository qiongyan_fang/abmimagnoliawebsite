package abmi.model.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import abmi.model.entity.gis.RegionInfo;
import abmi.model.entity.gis.RegionLayer;
import abmi.model.entity.gis.RegionSiteMap;
import abmi.model.services.rawdata.RawdataQueryCondition;
import abmi.model.util.CommonUtil;

@Service
public class RegionsService {

	@PersistenceContext(unitName="data")
	private EntityManager em;

	public RegionsService(EntityManager em) {

		this.em = em;
	}

	public RegionsService() {

	}

	public List<String> getRegionNamesByIds(List<Integer> regionIDList) {
		
		if (regionIDList == null || regionIDList.size() == 0)
			return null;
		String queryCategoryStr = "SELECT p  FROM RegionInfo p where p.riId in :regionIdList";

		TypedQuery<RegionInfo> queryCategory = em.createQuery(
				queryCategoryStr, RegionInfo.class);

		queryCategory.setParameter("regionIdList", regionIDList);
		List<String> result = new ArrayList<String>();
		for (RegionInfo row : queryCategory.getResultList()){
			result.add(row.getRegionLayer().getRlName()+ " - " + row.getRiName()); 
		}
		
		return result;
	}
	
	
	public boolean isAllAlberta(List<Integer> regionList){
		if (regionList == null || regionList.size() == 0 )
			return true;
		
		TypedQuery<RegionLayer> rotationQuery = em.createQuery(
				"Select p.regionLayer from RegionInfo p where p.riId in :ids", RegionLayer.class);
		
		rotationQuery.setParameter("ids", regionList);
		for (RegionLayer row: rotationQuery.getResultList()){
			if (row.getRegionInfos().size() == regionList.size()){
				return true;
			}
			else{
				return false;
			}
		}
		
		return true;
	}
	
	
	public List<String> getSiteListByRegionIds(List<Integer> regionIDList) {
		String queryCategoryStr = "SELECT distinct p.rsmSiteId FROM RegionSiteMap p where p.regionInfo.riId in :regionIdList ";

		TypedQuery<String> queryCategory = em.createQuery(
				queryCategoryStr, String.class);

		queryCategory.setParameter("regionIdList", regionIDList);

		return queryCategory.getResultList();

	}

	/**
	 * 
	 * @param regionIDList
	 * @param rotations
	 * @param coreDataType
	 * @return
	 */
	public List<String> getSiteListFromPublicRegions(
			List<Integer> regionIDList, List<String> rotations,
			int coreDataType) {

		List<String> siteList = new ArrayList<String>();
		String regionSiteVo_regionnameColumnName = "RI_ID";
		try {

			String query = "SELECT   distinct GSA_SITE_ID "
					+ "   FROM   GIS.REGION_INFOS GIS_REGION_INFOS,   "
					+ "     GIS.REGION_SITE_MAPS GIS_REGION_SITE_MAPS,  "
					+ "      G_SITE_ACCESS_V,        G_ROTATIONS   "
					+ " where RI_ID = RSM_RI_ID  "
					+ "  and GSA_YEAR BETWEEN GR_START_YEAR and GR_END_YEAR";

			/** depends on query type */
			if (RawdataQueryCondition.ABMI_CORE_DATA == coreDataType) {
				query += " and RSM_SITE_ID = SITE_ID  and site_id < 1657 ";
			} else if (RawdataQueryCondition.ABMI_OFFGRID_DATA == coreDataType) {
				query += " and RSM_SITE_ID = CORE_SITE_ID  and site_id >= 1657 ";
			} else if (RawdataQueryCondition.ABMI_CORE_AND_OFFGRID_DATA == coreDataType) {
				query += " and RSM_SITE_ID = CORE_SITE_ID";
			} else {
				return null;
			}

			String regionSql = CommonUtil.createSqlFromArrayList(
					regionSiteVo_regionnameColumnName, regionIDList, "");
			if (!"".equals(regionSql)) {
				query += " and " + regionSql;
			}

			String rotationSql = CommonUtil.createSqlFromArrayList(
					"GR_ROTATION_NAME", rotations, "'");

			if (!"".equals(rotationSql)) {
				query += " and " + rotationSql;
			}

			// output to file
			for (Object row : em.createNativeQuery(query).getResultList()) {

				siteList.add(row.toString());
			}


			return siteList;

		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}

	
	public List<Integer> getCoreSiteListFromPublicRegions(
			List<Integer> regionIDList
			) {

		List<Integer> siteList = new ArrayList<Integer>();
		
		try {

			TypedQuery<RegionInfo> query = em.createQuery("select p from RegionInfo p where p.riId in :riId", RegionInfo.class);
			query.setParameter("riId", regionIDList);
			for(RegionInfo row: CommonUtil.emptyIfNull(query.getResultList())){
				for (RegionSiteMap siteRow :CommonUtil.emptyIfNull(row.getRegionSiteMaps())){
					if (!siteList.contains(siteRow.getRsmSiteId())){
						siteList.add(siteRow.getRsmSiteId());
					}
				}
			}
			
			return siteList;
			
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}
	
	public List<Integer> getCoreSiteListFromPublicRegions(
			Integer regionId	) {

		List<Integer> regionIdList = new ArrayList<Integer>();
		regionIdList.add(regionId);
		 
		return this.getCoreSiteListFromPublicRegions(regionIdList);
	}

	/**
	 * 
	 * @param regionIDList
	 * @param rotations
	 * @param coreDataType
	 * @param protocolType
	 * @return
	 */
	public Map<String, Integer> getSiteInfo(List<Integer> regionIDList,
			List<String> rotations, int coreDataType, String protocolType) {

		String queryStr = "SELECT gis.SITE_REGION_QUERY_new.GetSiteNoByRegionName(?) allSites, "
				+ " gis.SITE_REGION_QUERY_new.GetSampledSiteNoByRegionName(?, ?, ?, ?, ?, ?) sampledSites, "
				+ " gis.SITE_REGION_QUERY_new.getprotectedsitenobyregionname(?) protectedSites "
				+ " from dual";

		Query query = em.createNativeQuery(queryStr);
		String regionSql = CommonUtil.createSqlFromArrayListWithoutFieldName(
				regionIDList, "");

		
		query.setParameter(1, regionSql);
		query.setParameter(2, regionSql);
		query.setParameter(3, CommonUtil
				.createSqlFromArrayListWithoutFieldName(rotations, "'"));

		/* choose one protocol, */
		query.setParameter(4, ("terrestrial".equals(protocolType) ? 1 : 0));
		query.setParameter(5, ("wetland".equals(protocolType) ? 1 : 0));
		query.setParameter(6, ("terrestrial".equals(protocolType) ? 1 : 0));

		query.setParameter(7, coreDataType);

		query.setParameter(8, regionSql);
		Map<String, Integer> returnList = new HashMap<String, Integer>();
		for (Object rowObj : query.getResultList()) {
			Object[] row = (Object[]) rowObj;

			returnList.put("total", new Integer(row[0].toString()));
			returnList.put("sampled", new Integer(row[1].toString()));
			try {
			returnList.put("parkpercentage",
					new Integer(new Double(row[2].toString()).intValue()));
			}catch(Exception e) {
				returnList.put("parkpercentage",
						new Integer(-1));
			}

			return returnList;

		}

		return null;

	}
}
