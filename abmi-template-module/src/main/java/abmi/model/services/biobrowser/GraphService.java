package abmi.model.services.biobrowser;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.ui.RectangleInsets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import abmi.model.entity.biobrowser.graph.WebGraphTreedX;

import abmi.model.entity.biobrowser.WebSpeciesViewV;
import abmi.model.entity.biobrowser.graph.WebGraphAvailablity;
import abmi.model.entity.biobrowser.graph.WebGraphType;
import abmi.model.services.ProjectPropertyService;
import abmi.model.services.biobrowser.chart.FewDetectionChart;
import abmi.model.services.biobrowser.chart.LinearXYLineChart;
import abmi.model.services.biobrowser.chart.SectorEffectorChart;
import abmi.model.services.biobrowser.chart.SpeciesAbundanceBarLineChart;
import abmi.model.services.biobrowser.chart.TreeSiteBarChart;
import abmi.model.util.ModuleParameters;

@Service
public class GraphService {

	@PersistenceContext(unitName = "data")
	EntityManager em;

	@Autowired
	ProjectPropertyService projectPropertyService;

	@Autowired
	BioBrowserService bioBrowserService;

	public String getRootPath() {
		return rootPath;
	}

	String rootPath;
	final String GRAPH_PATH = "WEB_GRAPH_PATH";

	static public final String imageExtention = ".png";
	// need to match database values: WEB_GRAPH_TYPES.wgt_name
	public static final String LINEAR_FOOTPRINT_GRAPH = "Relationship to Linear Footprint";
	public static final String SECTOR_EFFECTS_GRAPH = "Human Footprint Effects";
	public static final String NON_TREED_SITE_GRAPH = "Non-Treed or Treed Sites";
	public static final String SPECIES_ABUNDANCE_GRAPH = "species relative abundance";
	public static final String FEW_DETCTION_SPECIES_NORTH_GRAPH = "Few Detection North";
	public static final String FEW_DETCTION_SPECIES_SOUTH_GRAPH = "Few Detection South";

	final String[] DataCompletion = { "Full", "Few", "None" };

	/**
	 * get Linear human footprint relation graphs
	 * 
	 * @return
	 */

	public GraphService() {

		// for testing purpose

		if (em == null) {
			em = (EntityManager) Persistence.createEntityManagerFactory(ModuleParameters.DATA_ENTITY_MANAGER)
					.createEntityManager();
		}

		if (projectPropertyService == null) {
			projectPropertyService = new ProjectPropertyService();
			projectPropertyService.setEm(em);
		}
		rootPath = projectPropertyService.getPath(this.GRAPH_PATH);
	}

	public void createGraphsForAllSpp() {
		TypedQuery<Integer> query = this.em
				.createQuery("select  p.wsbTsn from WebSpeciesViewV p", Integer.class);

		for (Integer row : query.getResultList()) {
			this.getAllGraphs(row, true);
			System.out.println(row);
		}

	}

	/**used on website to get path, or create graphs and then get path.
	 * 
	 * @param tsn
	 * @param bOverwrite
	 * @return
	 */
	public Map<String, Map<String, String>> getAllGraphs(int tsn, boolean bOverwrite) {
		Map<String, Map<String, String>> resultMap = new HashMap<String, Map<String, String>>();
		WebSpeciesViewV sppRow = bioBrowserService.getSppMixedRow(tsn);

		WebGraphAvailablity graphStatus = bioBrowserService.getSppGraphStatus(tsn);

		if (graphStatus == null) {
			return null;
		}
		/*-----------------------------------------------------------------------------------------------------------
		 * when forest model is few detection, we will have 
		 * 1. few detection graph for north
		 ----------------------------------------------------------------------------------------------------------*/

		
		if (this.DataCompletion[1].equalsIgnoreCase(graphStatus.getWebForestModel().getWgmtCode())) {
			try {

				Map<String, String> path = getGraph(GraphService.FEW_DETCTION_SPECIES_NORTH_GRAPH, sppRow, tsn, bOverwrite,
						true);
				resultMap.put("few_detection_north_graph", path);

				path = getGraph(GraphService.FEW_DETCTION_SPECIES_SOUTH_GRAPH, sppRow, tsn, bOverwrite, false);

				resultMap.put("few_detection_south_graph", path);

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		
		} 
		
	
		else
		
	
		/*-----------------------------------------------------------------------------------------------------------
		 * when forest model is full, we will have 
		 * 1. species abundance for forest
		 * 2. linear human footprint north
		 * 3. sector effect north
		 ----------------------------------------------------------------------------------------------------------*/
		if (this.DataCompletion[0].equalsIgnoreCase(graphStatus.getWebForestModel().getWgmtCode())) {
			try {

				Map<String, String> path = getGraph(GraphService.SPECIES_ABUNDANCE_GRAPH, sppRow, tsn, bOverwrite, null);
				resultMap.put("species_graph", path);

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {

				Map<String, String> path = getGraph(GraphService.LINEAR_FOOTPRINT_GRAPH, sppRow, tsn, bOverwrite, true);
				resultMap.put("linear_north_graph", path);
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {

				Map<String, String> path = getGraph(GraphService.SECTOR_EFFECTS_GRAPH, sppRow, tsn, bOverwrite, true);
				resultMap.put("sector_effect_north_graph", path);
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}

		
		/*-----------------------------------------------------------------------------------------------------------
		 * when prairie model is few detection, we will have 
		 * 1. few detection graph for south
		 ----------------------------------------------------------------------------------------------------------*/
		
		
		if (this.DataCompletion[1].equalsIgnoreCase(graphStatus.getWebPrairieModel().getWgmtCode())) {
			try {

				Map<String, String> path = getGraph(GraphService.FEW_DETCTION_SPECIES_SOUTH_GRAPH, sppRow, tsn, bOverwrite,
						false);

				resultMap.put("few_detection_south_graph", path);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else 

		/*-----------------------------------------------------------------------------------------------------------
		 * when prairie model is full, we will have 
		 * 1. species abundance for prairie (ie. treed and non-tree )
		 * 2. linear human footprint south
		 * 3. sector effect south
		 ----------------------------------------------------------------------------------------------------------*/
		
		if (this.DataCompletion[0].equalsIgnoreCase(graphStatus.getWebPrairieModel().getWgmtCode())) {
			try {
				Map<String, String> path = getTreeGraphs(GraphService.NON_TREED_SITE_GRAPH, sppRow, tsn, bOverwrite);
				Map<String, String> treePath = new HashMap<String, String> ();
				treePath.put("path", path.get("treed_graph"));
				
				resultMap.put("treed_graph", treePath);

				Map<String, String> nonTreePath = new HashMap<String, String> ();
				nonTreePath.put("path", path.get("nontreed_graph"));
				
				resultMap.put("non_treed_graph", nonTreePath);

			} catch (Exception e) {
				System.out.println("can't create graph for tsn = " + tsn); //  +" " + sppRow.getWsbCommonName() + " " + sppRow.getWsbScientificName());
				e.printStackTrace();
			}

			try {

				Map<String, String> path = getGraph(GraphService.LINEAR_FOOTPRINT_GRAPH, sppRow, tsn, bOverwrite, false);

				resultMap.put("linear_south_graph", path);

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {

				Map<String, String> path = getGraph(GraphService.SECTOR_EFFECTS_GRAPH, sppRow, tsn, bOverwrite, false);

				resultMap.put("sector_effect_south_graph", path);

			} catch (Exception e) {
				e.printStackTrace();
			} 
		}

		return resultMap;
	}

	/**
	 * 
	 * @param graphType
	 * @param sppRow
	 * @param tsn
	 * @param bOverwrite
	 * @return
	 */
	Map<String, String> getTreeGraphs(String graphType, WebSpeciesViewV sppRow, int tsn, boolean bOverwrite) {

		if (!GraphService.NON_TREED_SITE_GRAPH.equals(graphType)) {
			return null;

		}

		Map<String, String> resultMap = new HashMap<String, String>();
		if (sppRow == null) {
			sppRow = bioBrowserService.getSppMixedRow(tsn);
			if (sppRow == null) { // if species can't be found, don't do
									// anything
				return null;
			}
		}

		String treedPath = GraphService.getGraphPath(sppRow, graphType, null, true);
		String nonTreedPath = GraphService.getGraphPath(sppRow, graphType, null, false);

		File treedGraph = new File(rootPath + treedPath);
		File blankTreedGraph = new File(rootPath + treedPath + GraphService.imageExtention);

		File nonTreedGraph = new File(rootPath + nonTreedPath);
		File blankNonTreedGraph = new File(rootPath + nonTreedPath + GraphService.imageExtention);

		if (bOverwrite) {
			/* remove existing files */
			if (blankTreedGraph.exists()) {
				blankTreedGraph.delete();
			}

			if (treedGraph.exists()) {
				treedGraph.delete();
			}

			if (blankNonTreedGraph.exists()) {
				blankNonTreedGraph.delete();
			}

			if (nonTreedGraph.exists()) {
				nonTreedGraph.delete();
			}

		} else {
			if (treedGraph.exists() && nonTreedGraph.exists()) { // only both
																	// exist,
																	// return
																	// the file
																	// names
																	// direclty
				resultMap.put("treed_graph", treedPath);
				resultMap.put("nontreed_graph", nonTreedPath);

				resultMap.put("blank", "false");
				return resultMap;
			}

			if (blankNonTreedGraph.exists() && blankTreedGraph.exists()) {

				resultMap.put("treed_graph", treedPath + GraphService.imageExtention);
				resultMap.put("nontreed_graph", nonTreedPath + GraphService.imageExtention);
				resultMap.put("blank", "true");
				return resultMap;

			}
		}

		TreeSiteBarChart chart = new TreeSiteBarChart();
		chart.setEm(em);
		/**
		 * create a new graph
		 */
		boolean[] bCreated = chart.createChart(this.getWebGraphType(GraphService.NON_TREED_SITE_GRAPH), tsn,
				rootPath + treedPath, rootPath + nonTreedPath);

		/**
		 * no data available, create a blank image
		 */
		if (!bCreated[0]) {
			if (createBlankImage(rootPath + treedPath + GraphService.imageExtention)) {
				resultMap.put("treed_graph", treedPath + GraphService.imageExtention);
				resultMap.put("nontreed_graph", nonTreedPath + GraphService.imageExtention);
				resultMap.put("treed_path_blank", "true");

			}
		} else {
			resultMap.put("treed_graph", treedPath);

		}

		if (!bCreated[1]) {
			if (createBlankImage(rootPath + nonTreedPath + GraphService.imageExtention)) {

				resultMap.put("nontreed_graph", nonTreedPath + GraphService.imageExtention);
				resultMap.put("nontreed_path_blank", "true");

			}
		} else {
			resultMap.put("nontreed_graph", nonTreedPath);

		}
		return resultMap;
	}

	public Map<String, String> getGraph(String graphType, WebSpeciesViewV sppRow, int tsn, boolean bOverwrite,
			Boolean bNorth) {

		Map<String, String> resultMap = new HashMap<String, String>();
		if (sppRow == null) {
			sppRow = bioBrowserService.getSppMixedRow(tsn);
			if (sppRow == null) { // if species can't be found, don't do
									// anything
				return null;
			}
		}

		String path = GraphService.getGraphPath(sppRow, graphType, bNorth, null);
		//
		// if (this.NON_TREED_SITE_GRAPH.equals(graphType) ) {
		// path = this.getTreeGraphPath(sppRow, this.NON_TREED_SITE_GRAPH,
		// bTree);
		// } else if (this.LINEAR_FOOTPRINT_GRAPH.equals(graphType)) {
		// path = this.getLinearGraphPath(sppRow, graphType, bNorth);
		// } else if (this.SPECIES_ABUNDANCE_GRAPH.equals(graphType) ||
		// this.FEW_DETCTION_SPECIES_NORTH_GRAPH.equals(graphType) ||
		// this.FEW_DETCTION_SPECIES_SOUTH_GRAPH.equals(graphType)) {
		// path = this.getGraphPath(sppRow, graphType);
		// }
		//
		// else if (this.SECTOR_EFFECTS_GRAPH.equals(graphType)) {
		// path = this.getLinearGraphPath(sppRow, graphType, bNorth);
		// } else {
		// return null;
		// }

		File graph = new File(rootPath + path);
		File blankGraph = new File(rootPath + path + GraphService.imageExtention);

		if (bOverwrite) {

			if (blankGraph.exists()) {
				blankGraph.delete();
			}

			if (graph.exists()) {
				graph.delete();
			}
		} else {
			if (graph.exists()) {
				resultMap.put("path", path);
				/*--------------------------------------------------------------
				 * Only the first large graphs can be zoom in, so provide an large
				 * graph image.
				 --------------------------------------------------------------*/
				if (GraphService.SPECIES_ABUNDANCE_GRAPH.equals(graphType)) {
					resultMap.put("large_path", path + "_large.png");
				}
				resultMap.put("blank", "false");
				return resultMap;
			}

			if (blankGraph.exists()) {

				resultMap.put("path", path + GraphService.imageExtention);
				if (GraphService.SPECIES_ABUNDANCE_GRAPH.equals(graphType)) {
					resultMap.put("large_path", path + GraphService.imageExtention);
				}
				resultMap.put("blank", "true");
				return resultMap;

			}
		}

		// otherwise, create a new graph
		// first get metadata and data, and then pass data to chart and create a
		// graph
		// Linear feature

		boolean bCreated = false;
		if (GraphService.LINEAR_FOOTPRINT_GRAPH.equals(graphType)) {
			LinearXYLineChart chart = new LinearXYLineChart();
			chart.setEm(em);
			bCreated = chart.createChart(this.getWebGraphType(graphType), tsn, rootPath + path, bNorth);
		} else if (GraphService.SPECIES_ABUNDANCE_GRAPH.equals(graphType)) {
			SpeciesAbundanceBarLineChart barChart = new SpeciesAbundanceBarLineChart();
			barChart.setEm(em);
			bCreated = barChart.createChart(this.getWebGraphType(graphType), tsn, rootPath + path, 1,
					new RectangleInsets(10, 0, 43, 48));

			barChart.createChart(this.getWebGraphType(graphType), tsn, rootPath + path + "_large.png", 2,
					new RectangleInsets(10, 0, 58, 55));
		} else if (GraphService.SECTOR_EFFECTS_GRAPH.equals(graphType)) {
			SectorEffectorChart barChart = new SectorEffectorChart();
			barChart.setEm(em);
			bCreated = barChart.createChart(this.getWebGraphType(graphType), tsn, rootPath + path, bNorth);
		} else if (GraphService.FEW_DETCTION_SPECIES_NORTH_GRAPH.equals(graphType)
				|| GraphService.FEW_DETCTION_SPECIES_SOUTH_GRAPH.equals(graphType)) {

			FewDetectionChart barChart = new FewDetectionChart();
			barChart.setEm(em);
			bCreated = barChart.createChart(this.getWebGraphType(graphType), tsn, rootPath + path, bNorth);
		}

		if (bCreated) {
			resultMap.put("path", path);
			if (GraphService.SPECIES_ABUNDANCE_GRAPH.equals(graphType)) {
				resultMap.put("large_path", path + "_large.png");
			}
			resultMap.put("blank", "false");
			return resultMap;
		}

		/**
		 * no data available, create a blank image
		 */
		if (createBlankImage(rootPath + path + GraphService.imageExtention)) {
			resultMap.put("path", path + GraphService.imageExtention);
			resultMap.put("blank", "true");
			return resultMap;
		}

		/*
		 * failed return nothing.
		 */
		return null;

	}

	/**
	 * 
	 * @param graphType
	 * @return
	 */
	WebGraphType getWebGraphType(String graphType) {
		TypedQuery<WebGraphType> query = em.createQuery("select  p from WebGraphType p where p.wgtName =:graphType",
				WebGraphType.class);

		query.setParameter("graphType", graphType);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public static String getGraphPath(WebSpeciesViewV sppRow, String graphType, Boolean bNorth, Boolean bTree) {
		// path format:
		// path/<species group>/<graph type>/species.png

		String path = "//charts//" + sppRow.getSppGroup().getSgGroupName().replaceAll(" ", "_") + "//"
				+ graphType.replaceAll(" ", "_") + "//" + (bNorth == null ? "" : (bNorth ? "north" : "south")) + "//"
				+ (bTree == null ? "" : (bTree ? "treed" : "non-treed")) + "//"
				+ sppRow.getWsbScientificName().replace(" ", "").replaceAll("/", "&")
				+ (bNorth == null ? "" : (bNorth ? "_north" : "_south"))
				+ (bTree == null ? "" : (bTree ? "_treed" : "_non-treed")) + imageExtention;

		return path;

	}

	/*
	 * public static String getLinearGraphPath(WebSpeciesViewV sppRow, String
	 * graphType, boolean bNorth) { // path format: // path/<species
	 * group>/<graph type>/species.png
	 * 
	 * String path = "//" +
	 * sppRow.getSppGroup().getSgGroupName().replaceAll(" ", "_") + "//" +
	 * graphType.replaceAll(" ", "_") + "//" + (bNorth ? "north" : "south") +
	 * "//"
	 * 
	 * + sppRow.getWsbScientificName().replace(" ", "") + (bNorth ? "_north" :
	 * "_south") + imageExtention;
	 * 
	 * return path;
	 * 
	 * }
	 * 
	 * public static String getTreeGraphPath(WebSpeciesViewV sppRow, String
	 * graphType, boolean bTree) { // path format: // path/<species
	 * group>/<graph type>/species.png
	 * 
	 * String path = "//" +
	 * sppRow.getSppGroup().getSgGroupName().replaceAll(" ", "_") + "//" +
	 * graphType.replaceAll(" ", "_") + "//" + (bTree ? "treed" : "non-treed") +
	 * "//" + sppRow.getWsbScientificName().replace(" ", "") + (bTree ? "_treed"
	 * : "_non-treed") + imageExtention;
	 * 
	 * return path;
	 * 
	 * }
	 * 
	 * public static String getGraphPath(WebSpeciesViewV sppRow, String
	 * graphType) { // path format: // path/<species group>/<graph
	 * type>/species.png
	 * 
	 * String path = "//" +
	 * sppRow.getSppGroup().getSgGroupName().replaceAll(" ", "_") + "//" +
	 * graphType.replaceAll(" ", "_") + "//" +
	 * sppRow.getWsbScientificName().replace(" ", "") + imageExtention;
	 * 
	 * return path;
	 * 
	 * }
	 */

	/**
	 * if no data available, create a 1x1 pixels blank image.
	 * 
	 * @param path
	 * @return
	 */
	boolean createBlankImage(String path) {
		CategoryPlot categoryplot = new CategoryPlot(null, null, null, null);

		JFreeChart chart = new JFreeChart("", null, categoryplot, false);

		chart.setBackgroundPaint(null);
		chart.setBackgroundImageAlpha(0.0f);
		ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());

		File graphFile = new File(path);
		graphFile.getParentFile().mkdirs();
		if (!graphFile.exists() && !graphFile.isDirectory()) {
			try {
				graphFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			ChartUtilities.saveChartAsPNG(graphFile, chart, 1, 1, info);

			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * 
	 * @param chart
	 * @param graphType
	 * @param path
	 * @return
	 */
	public static boolean saveChartAsImage(JFreeChart chart, WebGraphType graphType, String path) {
		return saveChartAsImage(chart, graphType, path, 1, new RectangleInsets(10, 0, 0, 0));
	}

	/**
	 * 
	 * @param chart
	 * @param graphType
	 * @param path
	 * @param scale
	 *            change width and height by multipling this scale
	 * @return
	 */
	public static boolean saveChartAsImage(JFreeChart chart, WebGraphType graphType, String path, int scale,
			RectangleInsets padding) {
		chart.setBackgroundPaint(new Color(255, 255, 255, 255));
		chart.setBackgroundPaint(null);
		chart.setBackgroundImageAlpha(0.0f);
		chart.setPadding(padding); // put some padding
									// on top the chart,
									// so the lables can
									// be more visible

		ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());

		File graphFile = new File(path);
		graphFile.getParentFile().mkdirs();
		if (!graphFile.exists() && !graphFile.isDirectory()) {
			try {
				graphFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			ChartUtilities.saveChartAsPNG(graphFile, chart, graphType.getWgtWidth() * scale,
					graphType.getWgtHeight() * scale, info);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
