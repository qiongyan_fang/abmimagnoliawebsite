package abmi.model.services.biobrowser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import abmi.model.datatable.DataTableCriterias.Column;
import abmi.model.datatable.SpeciesDataTableCriterias;
import abmi.model.datatable.DataTableCriterias.OrderCriterias;
import abmi.model.entity.biobrowser.SpeciesPrevNextV;
import abmi.model.entity.biobrowser.SppDetectionCount;
import abmi.model.entity.biobrowser.WebSpeciesViewV;
import abmi.model.entity.biobrowser.graph.WebGraphAvailablity;
import abmi.model.util.CommonUtil;

@Service
@Transactional
public class BioBrowserService {
	private static final Logger log = LoggerFactory.getLogger(BioBrowserService.class);
	final static Integer DEFAULT_RECORD_COUNT = 20;
	@PersistenceContext(unitName = "data")
	EntityManager em;

	@Autowired
	GraphService graphService;
	/**
	 * 
	 * @param groupName
	 * @param total
	 * @param startFrom
	 * @param count
	 * @param orderField
	 * @param isAsc
	 * @param serverPath
	 * @return
	 */
	String[] orderFields = { "", "summaryType", "wsbCommonName", "wsbScientificName", "sppLocationType.sltDisplay" };

	// String[] location = { "NONE", "Terrestrial", "Aquatic",
	// "Terrestrial, Aquatic" };

	final String PlantGroupName = "Vascular Plants";

	/**
	 * 
	 * @param spp
	 * @param serverPath
	 * @return
	 * 
	 * 		public Map<String, Object>
	 *         getSpeciesListAll(SpeciesDataTableCriterias spp, String
	 *         serverPath) {
	 * 
	 *         Map<String, Object> resultMap = new HashMap<String, Object>();
	 * 
	 *         int startFrom = spp.getStart(); int count = spp.getLength();
	 *         create order by Caluse *
	 * 
	 *         String orderBy = " "; String connector = " order by "; for
	 *         (Map<OrderCriterias, String> singleOrder : spp.getOrder()) {
	 * 
	 *         Iterator it = singleOrder.keySet().iterator(); String fieldName =
	 *         null; String direction = ""; while (it.hasNext()) {
	 *         OrderCriterias row = (OrderCriterias) it.next();
	 * 
	 *         try { if (row.name().equals("column")) { fieldName =
	 *         orderFields[Integer.parseInt(singleOrder .get(row))]; } else if
	 *         (row.name().equals("dir")) { direction = singleOrder.get(row); }
	 * 
	 *         } catch (NumberFormatException err) {
	 * 
	 *         }
	 * 
	 *         } // orderBy += connector + " p" +
	 *         orderFields[singleOrder.get(row)]; if (fieldName != null) {
	 *         orderBy += connector + " p." + fieldName + " " + direction;
	 *         connector = ","; }
	 * 
	 *         }
	 * 
	 *         System.out.println("orderBy =" + orderBy); String selectSQL =
	 *         "from WebSpeciesBrowser p where p.sppSummaryType.wsbSummaryType
	 *         is not null and (:groupName is null or p.wsbGroupName =
	 *         :groupName) " ; // if (spp.getSpeciesName() != null) { //
	 *         selectSQL += // " and ( upper(p.wsbScientificName) like
	 *         upper(:speciesName)" // + " or upper(p.wsbCommonName) like
	 *         upper(:speciesName))"; // }
	 * 
	 *         String queryStr = "select p " + selectSQL + orderBy;
	 *         TypedQuery<WebSpeciesBrowser> query = em.createQuery(queryStr,
	 *         WebSpeciesBrowser.class);
	 * 
	 *         query.setParameter("groupName", spp.getGroupName()); // if
	 *         (spp.getSpeciesName() != null) { //
	 *         query.setParameter("speciesName", "%" // +
	 *         spp.getSpeciesName().toUpperCase() + "%"); // }
	 *         query.setFirstResult(startFrom); query.setMaxResults(count);
	 * 
	 *         List<HashMap<String, String>> speciesRows = new
	 *         ArrayList<HashMap<String, String>>(); for (WebSpeciesBrowser row
	 *         : CommonUtil.emptyIfNull(query .getResultList())) {
	 *         HashMap<String, String> singleRow = new HashMap<String,
	 *         String>(); singleRow.put("rank", row.getWsbRankName());
	 *         singleRow.put("id", row.getWsbId() + "");
	 *         singleRow.put("common_name", row.getWsbCommonName());
	 *         singleRow.put("scientific_name", row.getWsbScientificName()); if
	 *         (row.getWsbGroupName().equals(PlantGroupName) &&
	 *         row.getSppLocationType() != null
	 * 
	 *         ) { singleRow.put("protocol_location",
	 *         row.getSppLocationType().getSltDisplay()); }
	 *         singleRow.put("summary_type",
	 *         row.getSppSummaryType().getSstDisplay() ); singleRow.put(
	 *         "image", SpeciesProfileDAO.getSingleSpeciesProfileImage(
	 *         row.getWsbScientificName(), serverPath));
	 * 
	 *         speciesRows.add(singleRow); }
	 * 
	 *         resultMap.put("data", speciesRows); resultMap.put("start",
	 *         startFrom); // if (startFrom + count - 1 >= total) { //
	 *         resultMap.put("hasMore", "false"); // } else { //
	 *         resultMap.put("hasMore", "true"); // } return resultMap; }
	 */
	/**
	 * 
	 * @param spp
	 * @param serverPath
	 * @return
	 */
	public Map<String, Object> getSpeciesList(SpeciesDataTableCriterias spp, String serverPath) {

		Map<String, Object> resultMap = new HashMap<String, Object>();

		int startFrom = spp.getStart();
		int count = spp.getLength();
		/* create order by Caluse */

		String orderBy = "";
		String connector = " order by ";
		for (Map<OrderCriterias, String> singleOrder : spp.getOrder()) {

			Iterator it = singleOrder.keySet().iterator();
			String fieldName = null;
			String direction = "";
			while (it.hasNext()) {
				OrderCriterias row = (OrderCriterias) it.next();

				try {
					if (row.name().equals("column")) {
						fieldName = orderFields[Integer.parseInt(singleOrder.get(row))];
					} else if (row.name().equals("dir")) {
						direction = singleOrder.get(row);
					}
				} catch (NumberFormatException err) {

				}

			}

			// orderBy += connector + " p" + orderFields[singleOrder.get(row)];
			if (fieldName != null && !fieldName.equals("")) {
				orderBy += connector + " p." + fieldName + " " + direction;
				connector = ",";
			}

		}

		/*
		 * -------------------------------------------------------------- add
		 * extra order by common name (for birds, mammals, vascular plants) or
		 * scientific name for the rest of the taxa
		 * --------------------------------------------------------------
		 */
		try {
			if (orderBy.equals("")) {
				if (spp == null || spp.getGroupId() == null || spp.getGroupId() == 1 || spp.getGroupId() == 2
						|| spp.getGroupId() == 7) {
					orderBy += " order by p.wsbCommonName ";
				} else {
					orderBy += " order by p.wsbScientificName ";
				}
			} else {

				if (spp == null || spp.getGroupId() == null || spp.getGroupId() == 1 || spp.getGroupId() == 2
						|| spp.getGroupId() == 7) {
					if (orderBy.indexOf("wsbCommonName") == -1) {
						orderBy += " , p.wsbCommonName ";
					}
				} else {

					if (orderBy.indexOf("wsbScientificName") == -1) {

						orderBy += " , p.wsbScientificName ";

					}
				}
			}
		} catch (Exception e) {

		}

		// System.out.println(orderBy);

		String selectSQL = "from WebSpeciesViewV p where  (:groupId is null or p.wgaSppGroupId = :groupId) ";
		for (Map<Column, String> singleOrder : spp.getColumns()) {

			Iterator it = singleOrder.keySet().iterator();
			String queryValue = "";
			String fieldName = "";
			while (it.hasNext()) { /* this loops the same column values */
				Column row = (Column) it.next();
				if ("searchValue".equals(row.name())) { // first check name
					queryValue = singleOrder.get(row).trim(); // then store
																// values
				} else if ("data".equals(row.name())) {
					fieldName = singleOrder.get(row).trim();
				}
			}

			// create sql based on query values
			if (queryValue != null && !("".equals(queryValue))) {
				if ("summary_type".equals(fieldName)) {

					selectSQL += " and  upper(p.summaryType) like '" + queryValue.toUpperCase() + "%'";

				} else if ("common_name".equals(fieldName)) {
					if (queryValue.length() == 1) {
						selectSQL += " and  upper(p.wsbCommonName) like '" + queryValue.toUpperCase() + "%' ";
					} else {
						selectSQL += " and ( upper(p.wsbScientificName) like '%" + queryValue.toUpperCase() + "%'"
								+ " or ( upper(p.wsbCommonName) like '%" + queryValue.toUpperCase() + "%' ) )";
					}
				} else if ("scientific_name".equals(fieldName)) {
					if (queryValue.length() == 1) {
						selectSQL += " and ( upper(p.wsbScientificName) like '" + queryValue.toUpperCase() + "%')";

					} else {
						selectSQL += " and ( upper(p.wsbScientificName) like '%" + queryValue.toUpperCase() + "%'"
								+ " or ( upper(p.wsbCommonName) like '%" + queryValue.toUpperCase() + "%' ) )";
					}
				}
			}

		}

		/*
		 * get total count here
		 */
		String countQueryStr = "select count(p) " + selectSQL;
		TypedQuery<Long> countQuery = em.createQuery(countQueryStr, Long.class);
		countQuery.setParameter("groupId", spp.getGroupId());

		long totalCount = countQuery.getResultList().get(0);

		/* get actual data here */
		String queryStr = "select p " + selectSQL + orderBy;
		TypedQuery<WebSpeciesViewV> query = em.createQuery(queryStr, WebSpeciesViewV.class);

		query.setParameter("groupId", spp.getGroupId());

		/* set start and end */
		query.setFirstResult(startFrom);
		query.setMaxResults(count);

		List<HashMap<String, String>> speciesRows = new ArrayList<HashMap<String, String>>();
		for (WebSpeciesViewV row : CommonUtil.emptyIfNull(query.getResultList())) {
			HashMap<String, String> singleRow = new HashMap<String, String>();
			// singleRow.put("rank", row.getWsbRankName());
			singleRow.put("id", row.getWgaId() + "");
			singleRow.put("tsn", row.getWsbTsn().toString());
			singleRow.put("common_name", row.getWsbCommonName());
			String scientificName = row.getWsbScientificName();
			singleRow.put("scientific_name", scientificName);
			/*
			 * if (row.getSppGroup().getSgGroupName().equals(PlantGroupName) &&
			 * row.getSppLocationType() != null
			 * 
			 * ) { singleRow.put("protocol_location",
			 * row.getSppLocationType().getSltDisplay()); }
			 */
			singleRow.put("summary_type", row.getSummaryType());

			if (new File(graphService.getRootPath() + "/profiles/" + scientificName.replace(" ", "-") + "-small.jpg")
					.exists()) {
				singleRow.put("image", "/profiles/" + scientificName.replace(" ", "-") + "-small.jpg");
			} else {
				singleRow.put("image", "");
			}

			speciesRows.add(singleRow);
		}

		resultMap.put("data", speciesRows);
		resultMap.put("start", startFrom);
		resultMap.put("draw", spp.getDraw());
		resultMap.put("recordsTotal", totalCount);
		resultMap.put("recordsFiltered", totalCount);

		// if (startFrom + count - 1 >= total) {
		// resultMap.put("hasMore", "false");
		// } else {
		// resultMap.put("hasMore", "true");
		// }
		return resultMap;
	}

	/**
	 * 
	 * @param groupName
	 * @param speciesName
	 * @param serverPath
	 * @return
	 */
	public List<String> getAutoCompleteSpeciesList(Long groupId, String speciesName) {

		List<String> resultList = new ArrayList<String>();

		if (speciesName == null || speciesName.length() == 0) {

			return resultList;
		}
		/* create order by Caluse */

		String orderBy = " order by p.wsbCommonName, p.wsbScientificName";

		String queryStr = "select p from WebSpeciesViewV p where (:groupId is null or p.wgaSppGroupId = :groupId)"
				+ " and (upper(p.wsbScientificName) like upper(:speciesName)"
				+ " or upper(p.wsbCommonName) like upper(:speciesName)) " + orderBy;
		TypedQuery<WebSpeciesViewV> query = em.createQuery(queryStr, WebSpeciesViewV.class);

		query.setParameter("groupId", groupId);

		query.setParameter("speciesName", (speciesName == null ? "###" : "%" + speciesName.toUpperCase() + "%"));

		for (WebSpeciesViewV row : CommonUtil.emptyIfNull(query.getResultList())) {

			// if common names contains the query codes then use common names
			if (row.getWsbCommonName() != null
					&& row.getWsbCommonName().toLowerCase().indexOf(speciesName.toLowerCase()) != -1) {
				resultList.add(row.getWsbCommonName());
			} else {
				resultList.add(row.getWsbScientificName());
			}

		}

		return resultList;
	}

	/**
	 * get visited sites and detected sites for a species
	 * 
	 * @param scientificName
	 * @param speciesGrop
	 */
	public Map<String, String> getSpeciesDisribution(String scientificName, Long speciesGroup) {

		Map<String, String> detectionMap = new HashMap<String, String>();
		// get species group name from database if not provided.
		if (speciesGroup == null && "".equals(speciesGroup)) {
			speciesGroup = this.getGroupIdFromScientificName(scientificName);

			if (speciesGroup == null) {
				return detectionMap;
			}
		}

		try {
			// first get all sites visited for this species group
			TypedQuery<String> siteQuery = em.createQuery(
					"select distinct CAST(p.wsvSite as Varchar2(4))   from WebSiteVisited p where p.sppGroup.sgId =:groupdId",
					String.class);

			siteQuery.setParameter("groupdId", speciesGroup);

			String visitedSites = "";
			for (String site : siteQuery.getResultList()) {
				visitedSites += (site + ",");
			}
			// second get all sites detected for this species group and
			// scientific name

			TypedQuery<String> speciesQuery = em.createQuery(
					"select distinct CAST(p.wsdSite as Varchar2(4)) from WebSpeciesDetection p where p.webSpeciesBrowser.sppGroup.sgId =:groupdId and p.webSpeciesBrowser.wsbScientificName = :scientificName",
					String.class);

			speciesQuery.setParameter("groupdId", speciesGroup);
			speciesQuery.setParameter("scientificName", scientificName);

			String detectedSites = "";
			for (String site : speciesQuery.getResultList()) {
				detectedSites += (site + ",");
			}

			if (visitedSites.length() > 1) {
				detectionMap.put("visitedSites", visitedSites.substring(0, visitedSites.length() - 1));
			}
			if (detectedSites.length() > 1) {
				detectionMap.put("detectedSites", detectedSites.substring(0, detectedSites.length() - 1));
			}
		} catch (Exception e) {
			e.printStackTrace();
			// if any errors:
			detectionMap.put("error", "true");
			return detectionMap;
		}
		return detectionMap;
	}

	/**
	 * get species information by either scientific or common name
	 * 
	 * @param speciesName
	 * @return
	 */
	public Map<String, String> getIdsFromName(String speciesName) {
		TypedQuery<WebSpeciesViewV> query = em.createQuery(
				"select  p from WebSpeciesViewV p where upper(p.wsbScientificName) =:speciesName or upper(p.wsbCommonName) = :speciesName",
				WebSpeciesViewV.class);

		query.setParameter("speciesName", speciesName.toUpperCase());

		Map<String, String> resultMap = new HashMap<String, String>();

		try {
			WebSpeciesViewV row = query.getSingleResult();
			resultMap.put("groupId", row.getSppGroup().getSgId().toString());
			resultMap.put("tsn", row.getWsbTsn().toString());
			resultMap.put("sname", row.getWsbScientificName());

			return resultMap;
			// resultMap.put("groupId", row.);
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * get GroupId from scientific names.
	 * 
	 * @param scientificName
	 * @return
	 */
	private Long getGroupIdFromScientificName(String scientificName) {
		TypedQuery<Long> query = em.createQuery(
				"select  p.sppGroup.sgId from WebSpeciesViewV p where upper(p.wsbScientificName) =:scientificName",
				Long.class);

		query.setParameter("scientificName", scientificName.toUpperCase());

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * get GroupId from tsn
	 * 
	 * @param tsn
	 * @return
	 */
	private Integer getGroupIdFromTsn(int tsn) {
		WebSpeciesViewV row = getSppRow(tsn);

		return row.getSppGroup().getSgId();
	}

	/**
	 * get species row by tsn. the species row include species information such
	 * as scientific names, common name, groupIds, groupd Names etc.
	 * 
	 * @param tsn
	 * @return
	 */
	public WebSpeciesViewV getSppRow(int tsn) {
		TypedQuery<WebSpeciesViewV> query = em.createQuery("select  p from WebSpeciesViewV p where p.wsbTsn =:tsn",
				WebSpeciesViewV.class);

		query.setParameter("tsn", tsn);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public WebSpeciesViewV getSppMixedRow(int tsn) {
		TypedQuery<WebSpeciesViewV> query = em.createQuery("select  p from WebSpeciesViewV p where p.wsbTsn =:tsn",
				WebSpeciesViewV.class);

		query.setParameter("tsn", tsn);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public WebGraphAvailablity getSppGraphStatus(int tsn) {
		TypedQuery<WebGraphAvailablity> query = em
				.createQuery("select  p from WebGraphAvailablity p where p.wgaTsn =:tsn", WebGraphAvailablity.class);

		query.setParameter("tsn", tsn);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param groupId
	 * @return
	 */
	public Map<String, Long> getSpeciesCounter(Integer groupId) {
		Map<String, Long> resultMap = new HashMap<String, Long>();

		if (groupId != null) {

			TypedQuery<Long> query = em.createQuery(
					"select  p.sppCount from SppDetectionCount p where p.sppGroupId =:sppGroupId", Long.class);

			query.setParameter("sppGroupId", groupId);

			try {
				resultMap.put("count", query.getSingleResult());
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

		} else {
			TypedQuery<SppDetectionCount> query = em.createQuery("select p from SppDetectionCount p ",
					SppDetectionCount.class);

			try {
				Long totalCount = 0l;
				for (SppDetectionCount row : query.getResultList()) {
					resultMap.put("count-" + row.getSppGroupId(), row.getSppCount().longValue());

					totalCount += row.getSppCount();

				}
				resultMap.put("count", totalCount);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return resultMap;

	}

	/**
	 * provide a tsn, return the previous and next tsn id (ordered by either
	 * common name or scientific name).
	 * 
	 * @param tsn
	 * @return
	 */
	public Map<String, Object> getPrevNextTsn(Integer tsn) {

		Map<String, Object> resultMap = new HashMap<String, Object>();

		TypedQuery<SpeciesPrevNextV> query = em.createQuery("select  p from SpeciesPrevNextV p where p.wsbTsn =:tsn",
				SpeciesPrevNextV.class);

		query.setParameter("tsn", tsn);
		try {
			SpeciesPrevNextV row = query.getSingleResult();
			try {
				if (row.getRowAsc() > 1) { // row asc == 1 then it is the first
											// species within group
					resultMap.put("previous", row.getPreviousTsn());
					WebSpeciesViewV preRow = this.getSppRow(row.getPreviousTsn());
					resultMap.put("previousCommon", preRow.getWsbCommonName());
					resultMap.put("previousScientific", preRow.getWsbScientificName());

				}
			} catch (Exception preError) {
				log.error(preError.getMessage() + "\n failed to get previous records for tsn=" + tsn);
			}
			try {
				if (row.getRowDesc() > 1) { // row desc == 1 then it is the last
											// species within group, so no next
					resultMap.put("next", row.getNextTsn());
					WebSpeciesViewV nextRow = this.getSppRow(row.getNextTsn());
					resultMap.put("nextCommon", nextRow.getWsbCommonName());
					resultMap.put("nextScientific", nextRow.getWsbScientificName());

				}
			} catch (Exception nextError) {
				log.error(nextError.getMessage() + "\n failed to get next records for tsn=" + tsn);
			}
		} catch (Exception e) {
			log.error(e.getMessage() + "\n failed to get next records for tsn=" + tsn);
		}
		return resultMap;
	}

	public Map<String, String> getSpeciesNames(String name) {
		TypedQuery<WebSpeciesViewV> query = em
				.createQuery("select  p from WebSpeciesViewV p where upper(p.wsbScientificName) =:name"
						+ " or upper(p.wsbCommonName) = :name", WebSpeciesViewV.class);

		query.setParameter("name", name.toUpperCase());

		Map<String, String> resultMap = new HashMap<String, String>();

		try {
			WebSpeciesViewV row = query.getSingleResult();
			resultMap.put("scientific_name", row.getWsbScientificName());
			resultMap.put("common_name", row.getWsbCommonName());
			resultMap.put("group_name", row.getSppGroup().getSgGroupName());

		} catch (Exception e) {
			resultMap.put("error", name + " can't be matched in ABMI taxonomic database.");
		}

		return resultMap;
	}

}
