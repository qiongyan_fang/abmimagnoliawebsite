package abmi.model.services.biobrowser.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Paint;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLabelLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedRangeCategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.category.DefaultIntervalCategoryDataset;
import org.jfree.ui.RectangleInsets;
import org.jfree.util.ShapeUtilities;

import com.itextpdf.text.pdf.BaseFont;

import abmi.model.entity.biobrowser.graph.WebGraphCategoryDetail;
import abmi.model.entity.biobrowser.graph.WebGraphData;
import abmi.model.entity.biobrowser.graph.WebGraphSery;
import abmi.model.entity.biobrowser.graph.WebGraphType;
import abmi.model.services.biobrowser.GraphService;
import abmi.model.util.CommonUtil;
import abmi.model.util.chart.ext.ABMICategoryAxis;
import abmi.model.util.chart.ext.ABMIJFreeChart;
import abmi.model.util.chart.ext.ABMILabel;
import abmi.model.util.chart.renderer.ABMILineAndShapeCategoryRenderer;
import abmi.model.util.chart.renderer.ABMIMultiColorBarRenderer;
import abmi.model.util.chart.renderer.IntervalLineRenderer;

/**
 * this class reads data from database, and created a compicated compositeplots.
 * 
 * @author Qiongyan
 *	TODO: adjust space on YAxis.
 */
public class SpeciesAbundanceBarLineChart {
	@PersistenceContext(unitName = "data")
	EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	
	ArrayList<String> mainColorCodes;
	ArrayList<String> otherColorCodes;

	String fontName = "Open Sans";
	/*--------------------------------------------------------------------------------------------
	 * get all Bar dataset.
	 --------------------------------------------------------------------------------------------*/
	ArrayList<Map<String, Object>> getAllBarDataset(int tsn) {
		mainColorCodes = new ArrayList<String>();
		/**
		 * 
		 */
		TypedQuery<WebGraphSery> countQuery = em
				.createQuery(
						"select  p from WebGraphSery p where p.webGraphType.wgtId = 1 and p.webGraphCategory is not null order by p.wgsOrder",
						WebGraphSery.class);

		ArrayList<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		for (WebGraphSery row : countQuery.getResultList()) {
			Map<String, Object> dataset = this.getMainSeriesData(tsn, row.getWgsLabel());
			if (dataset!= null) {
				resultList.add(dataset);
				mainColorCodes.add(row.getWgsColor());
			}
			
		}

		return resultList;
	}

	/*------------------------------------------------------------------------------------------------------------------------------------------
	 * all the category keys, series keys have to be same. i.e. same type.
	 * integer and String will be treated as two keys.
	 * 
	 * @param tsn
	 * @param seriesLabel
	 * @return
	 ------------------------------------------------------------------------------------------------------------------------------------------*/
	Map<String, Object> getMainSeriesData(int tsn, String seriesLabel) {

		/*
		 * read data from database when graph type is 1: species relative
		 * abundance
		 */
		TypedQuery<WebGraphData> query = em
				.createQuery(
						"select  p from WebGraphData p where p.wgdTsn =:tsn and p.webGraphSery.webGraphType.wgtId=1 and p.webGraphSery.wgsLabel=:seriesLabel and p.wgdTreeSize is not null order by p.wgdTreeSize",
						WebGraphData.class);

		query.setParameter("tsn", tsn);
		query.setParameter("seriesLabel", seriesLabel);

		int index = 0;

		DefaultCategoryDataset mainCategory = new DefaultCategoryDataset();
		DefaultCategoryDataset ccCategory = new DefaultCategoryDataset();

		ArrayList<Double> startList = new ArrayList<Double>();
		ArrayList<Double> endList = new ArrayList<Double>();
		ArrayList<String> categoryList = new ArrayList<String>();

		ArrayList<Double> ccStartList = new ArrayList<Double>();
		ArrayList<Double> ccEndList = new ArrayList<Double>();
		ArrayList<String> ccCategoryList = new ArrayList<String>();

		for (WebGraphData row : CommonUtil.emptyIfNull(query.getResultList())) {
			if (row.getWgdIsMain() == 1) {
				startList.add(row.getWgdLower());
				endList.add(row.getWgdUpper());
				categoryList.add(row.getWgdTreeSize().toString());

				if (row.getWgdTreeSize() != null) {
					mainCategory.addValue(row.getWgdMean(), row
							.getWebGraphSery().getWgsLabel(), row
							.getWgdTreeSize().toString());
				}

			} else {
				ccStartList.add(row.getWgdLower());
				ccEndList.add(row.getWgdUpper());
				ccCategoryList.add(row.getWgdTreeSize().toString());

				if (row.getWgdTreeSize() != null) {
					ccCategory.addValue(row.getWgdMean(), row.getWebGraphSery()
							.getWgsLabel(), row.getWgdTreeSize().toString());
				}
			}

		}

		if (mainCategory.getColumnCount() == 0
				&& mainCategory.getRowCount() == 0) {
			return null;
		}
		// as ccCategory has fewer categories, add the missing values as null,
		// otherwise the plots don't align
		if (ccCategoryList.size() > 0
				&& ccCategoryList.size() != categoryList.size()) {
			for (int i = ccCategoryList.size(); i < categoryList.size(); i++) {
				ccStartList.add(null);
				ccEndList.add(null);
				ccCategoryList.add(categoryList.get(i).toString());

				ccCategory.addValue(null, seriesLabel, categoryList.get(i)
						.toString());

			}
		}

		// initiate categoryDataset
		String[] singleSeriesKeys = new String[1];
		singleSeriesKeys[0] = seriesLabel;
		Double[][] starts = new Double[1][startList.size()];
		starts[0] = startList.toArray(new Double[startList.size()]);

		Double[][] ends = new Double[1][endList.size()];
		ends[0] = endList.toArray(new Double[endList.size()]);

		DefaultIntervalCategoryDataset intervalCategoryDataset = new DefaultIntervalCategoryDataset(
				singleSeriesKeys, categoryList.toArray(new String[categoryList
						.size()]), starts, ends);

		Double[][] ccStarts = new Double[1][ccStartList.size()];
		ccStarts[0] = ccStartList.toArray(new Double[ccStartList.size()]);

		Double[][] ccEnds = new Double[1][ccEndList.size()];
		ccEnds[0] = ccEndList.toArray(new Double[ccEndList.size()]);

		DefaultIntervalCategoryDataset ccIntervalCategoryDataset = new DefaultIntervalCategoryDataset(
				singleSeriesKeys,
				ccCategoryList.toArray(new String[ccCategoryList.size()]),
				ccStarts, ccEnds);

		HashMap<String, Object> resultDataset = new HashMap<String, Object>();
		resultDataset.put("mainBar", mainCategory);
		resultDataset.put("ccCenterDot", ccCategory);
		resultDataset.put("mainInterval", intervalCategoryDataset);
		resultDataset.put("ccInterval", ccIntervalCategoryDataset);

		return resultDataset;
	}

	/*------------------------------------------------------------------------------------------------------------------------------------------
	 * get the bar dataset for far right bars. they don't have tree age values
	 * 
	 * @param tsn
	 * @return
	 ------------------------------------------------------------------------------------------------------------------------------------------*/
	Map<String, Object> getSimpleSeriesData(int tsn) {
		otherColorCodes = new ArrayList<String>();
		/*
		 * read data from database when graph type is 1: species relative
		 * abundance
		 */
		TypedQuery<WebGraphData> query = em
				.createQuery(
						"select  p from WebGraphData p where  p.webGraphSery.webGraphType.wgtId=1 and p.wgdTsn =:tsn and p.wgdTreeSize is null order by p.webGraphSery.wgsOrder",
						WebGraphData.class);

		query.setParameter("tsn", tsn);

		DefaultCategoryDataset mainCategory = new DefaultCategoryDataset();

		ArrayList<Double> startList = new ArrayList<Double>();
		ArrayList<Double> endList = new ArrayList<Double>();
		ArrayList<String> categoryList = new ArrayList<String>();

		for (WebGraphData row : CommonUtil.emptyIfNull(query.getResultList())) {

			startList.add(row.getWgdLower());
			endList.add(row.getWgdUpper());
			categoryList.add(row.getWebGraphSery().getWgsLabel());

			mainCategory.addValue(row.getWgdMean(), "other", row
					.getWebGraphSery().getWgsLabel());

			otherColorCodes.add(row.getWebGraphSery().getWgsColor());

		}

		String[] singleSeriesKeys = new String[1];
		singleSeriesKeys[0] = "other";
		Double[][] starts = new Double[1][startList.size()];
		starts[0] = startList.toArray(new Double[startList.size()]);

		Double[][] ends = new Double[1][endList.size()];
		ends[0] = endList.toArray(new Double[endList.size()]);

		DefaultIntervalCategoryDataset intervalCategoryDataset = new DefaultIntervalCategoryDataset(
				singleSeriesKeys, categoryList.toArray(new String[categoryList
						.size()]), starts, ends);

		HashMap<String, Object> resultDataset = new HashMap<String, Object>();
		resultDataset.put("mainBar", mainCategory);
		resultDataset.put("mainInterval", intervalCategoryDataset);

		return resultDataset;
	}

	/**
	 * 
	 * @param graphType
	 * @param tsn
	 * @param path
	 * @return
	 */
	public boolean createChart(WebGraphType graphType, int tsn, String path, int scale, RectangleInsets padding) {
		/**
		 * first plot
		 * 
		 */
//		 InputStream is = this.getClass().getClassLoader().getResourceAsStream("c:\\Windows\\Fonts\\OpenSans-Regular.ttf");
//		 try {
//			font = Font.createFont(Font.TRUETYPE_FONT, is);
//		
//		} catch (FontFormatException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		
		
		Font labelFont = new Font(fontName, Font.BOLD, 8 + 5*scale);

		// main plot
		NumberAxis numberAxis = new NumberAxis(graphType.getWgtValueLabel());

		CombinedRangeCategoryPlot combinedrangecategoryplot = new CombinedRangeCategoryPlot(
				numberAxis);

		numberAxis.setAxisLineStroke(new BasicStroke(2f));
		numberAxis.setLabelFont(labelFont);
		numberAxis.setTickLabelInsets(new RectangleInsets(0,0,0,0)); // I thought this method will adjust spacing on labels, but it affects tick numbers.
		numberAxis.setTickLabelFont( new Font(fontName, 1, 8*scale));
		/**
		 * first plot -- high low lines
		 */

		ArrayList<Map<String, Object>> resultDataset = this
				.getAllBarDataset(tsn);

		if (resultDataset == null || resultDataset.size() ==0 ) {// no data available
			return false;
		}
		for (int dataIndex = 0; dataIndex < resultDataset.size(); dataIndex++) {
			Map<String, Object> row = resultDataset.get(dataIndex);
			/* line plot */

			//
			// resultDataset.put("mainBar", mainCategory);
			// resultDataset.put("ccCenterDot", ccCategory);
			// resultDataset.put("mainInterval", intervalCategoryDataset);
			// resultDataset.put("ccInterval", ccIntervalCategoryDataset);
			// IntervalBarRenderer intervalBar = new IntervalBarRenderer();
			DefaultIntervalCategoryDataset mainIntervalDataset = (DefaultIntervalCategoryDataset) row
					.get("mainInterval");
			DefaultIntervalCategoryDataset ccIntervalDataset = (DefaultIntervalCategoryDataset) row
					.get("ccInterval");

			DefaultCategoryDataset mainBarDataset = (DefaultCategoryDataset) row
					.get("mainBar");
			DefaultCategoryDataset ccDotDataset = (DefaultCategoryDataset) row
					.get("ccCenterDot");

			/**
			 * dotted line
			 */

			ABMILineAndShapeCategoryRenderer lineRenderer = new ABMILineAndShapeCategoryRenderer();

			lineRenderer.setBaseStroke(new BasicStroke(1f));
			lineRenderer.setSeriesPaint(0, Color.decode("#663300"));
			lineRenderer.setOffsetX(0.1);
			lineRenderer.setSeriesShape(0, ShapeUtilities.createDiamond(5));
			
			lineRenderer.setSeriesFillPaint(0, Color.black);
			lineRenderer.setBaseStroke(new BasicStroke(1.5f,
					BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 0,
					new float[] { 2 }, 0));
			lineRenderer.setSeriesVisibleInLegend(false);

			lineRenderer.setItemMargin(0);

			CategoryAxis subCategoryAxis = new CategoryAxis(mainBarDataset
					.getRowKey(0).toString());
			subCategoryAxis.setMaximumCategoryLabelWidthRatio(5f);
			int i = 0;
			for (Object categoryKey : mainBarDataset.getColumnKeys()) {
				if (i % 2 == 1) {
					subCategoryAxis.setTickLabelPaint(categoryKey.toString(),
							new Color(0, 0, 0, 0));

				}
				i++;
			}

			subCategoryAxis.setTickMarksVisible(false);
			subCategoryAxis.setLabelFont(labelFont);
			subCategoryAxis.setLabelPaint(Color.decode(this.mainColorCodes
					.get(dataIndex)));

			subCategoryAxis.setAxisLineStroke(new BasicStroke(2f));
			subCategoryAxis.setAxisLinePaint(new Color(0, 0, 0));
			subCategoryAxis.setTickLabelInsets(new RectangleInsets(0,0,0,0));
			subCategoryAxis.setTickLabelFont( new Font(fontName, 1, 8*scale));
			CategoryPlot categoryplot = new CategoryPlot(ccDotDataset,
					subCategoryAxis, null, lineRenderer);

			/* high low line 1 */
			IntervalLineRenderer minMaxRenderer = new IntervalLineRenderer();

			minMaxRenderer.setBaseStroke(new BasicStroke(1.2f));
			minMaxRenderer.setPaint(Color.decode("#666600"));
			// minMaxRenderer.setOffsetX(-0.1);
			minMaxRenderer.setSeriesVisibleInLegend(false);
			minMaxRenderer.setItemMargin(0);

			categoryplot.setDomainGridlinesVisible(true);
			categoryplot.setDataset(1, mainIntervalDataset);
			categoryplot.setRenderer(1, minMaxRenderer);
			categoryplot.setDomainGridlinesVisible(false);
			categoryplot.setOutlineVisible(false);

			/* high low line 2 */

			IntervalLineRenderer minMaxRenderer2 = new IntervalLineRenderer();
			minMaxRenderer2.setBaseStroke(new BasicStroke(1f));
			minMaxRenderer2.setPaint(Color.decode("#333333"));
			minMaxRenderer2.setOffsetX(0.1);
			minMaxRenderer2.setSeriesVisibleInLegend(false);

			minMaxRenderer2.setItemMargin(0);
			categoryplot.setDataset(2, ccIntervalDataset);
			categoryplot.setRenderer(2, minMaxRenderer2);

			/**
			 * main bar plot
			 */
			Paint[] barPaint = new Paint[1];
			barPaint[0] = Color.decode(mainColorCodes.get(dataIndex));
			ABMIMultiColorBarRenderer renderer = new ABMIMultiColorBarRenderer(
					barPaint);
			renderer.setShadowVisible(false);
			renderer.setBarPainter(new StandardBarPainter());
			renderer.setSeriesVisibleInLegend(false);
			renderer.setMaximumBarWidth(0.1);

			renderer.setItemMargin(0.0);

			// CategoryPlot categoryplot = new CategoryPlot(
			// categoryBardataset, subCategoryAxis, null,
			// renderer);

			categoryplot.setDataset(3, mainBarDataset);
			categoryplot.setRenderer(3, renderer);

			combinedrangecategoryplot.add(categoryplot, 9); // 9 is the weight.
															// make it easier I
															// used the number
															// of bars

			categoryplot.setOutlinePaint(Color.white);
			categoryplot.setBackgroundPaint(null);
			categoryplot.setBackgroundAlpha(0.0f); // make plot transparent

		

		}

		combinedrangecategoryplot.setGap(0);
		combinedrangecategoryplot.setOutlinePaint(Color.white);

		/**
		 * --------------------------- second group , another plot
		 * 
		 * resultDataset.put("mainBar", mainCategory);
		 * resultDataset.put("mainInterval", intervalCategoryDataset);
		 */

		HashMap<String, Object> simpleDataMap = (HashMap<String, Object>) this
				.getSimpleSeriesData(tsn);
		DefaultCategoryDataset barCategorydataset = (DefaultCategoryDataset) simpleDataMap
				.get("mainBar");
		DefaultIntervalCategoryDataset intervalDataset2 = (DefaultIntervalCategoryDataset) simpleDataMap
				.get("mainInterval");

		ABMICategoryAxis categoryaxis1 = new ABMICategoryAxis("");
		// categoryaxis1.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		categoryaxis1.setMaximumCategoryLabelWidthRatio(5F);
		categoryaxis1.setMaximumCategoryLabelLines(3);
		categoryaxis1.setCategoryMargin(0);
		categoryaxis1.setLowerMargin(0);
		categoryaxis1.setUpperMargin(0);
		categoryaxis1.setTickLabelsVisible(false);
		categoryaxis1.setTickLabelFont( new Font(fontName, 1, 7*scale));
		categoryaxis1.setCategoryPaintCodes(this.otherColorCodes);
		categoryaxis1.setSubLabelFont(labelFont);
		categoryaxis1.setAxisLineStroke(new BasicStroke(2f));
		categoryaxis1.setAxisLinePaint(new Color(0, 0, 0));
		
		

		/**
		 * column=Grass column=Shrub column=Wetland column=Cultivated HF
		 * column=Urban/Industry HF
		 * 
		 * mannually adjust label space not used anymore use verticle label
		 */
		
		categoryaxis1.setSubCategoryLabelAngle(Math.PI/4);
		for (int keyIndex = 0; keyIndex <barCategorydataset.getColumnCount(); keyIndex ++ ){
			categoryaxis1.addSubCategory(barCategorydataset.getColumnKey(keyIndex),
					new Point2D.Double(barCategorydataset.getColumnKey(keyIndex).toString().length()*3, 0)
					);
		}
	/*
		categoryaxis1.addSubCategory(barCategorydataset.getColumnKey(0),	new Point2D.Double(20, 5));

		categoryaxis1.addSubCategory(barCategorydataset.getColumnKey(1),
				new Point2D.Double(0, 5 + scale*12));

		categoryaxis1.addSubCategory(barCategorydataset.getColumnKey(2),
				new Point2D.Double(0, 5));

		categoryaxis1.addSubCategory(barCategorydataset.getColumnKey(3),
				new Point2D.Double(5, 5 + scale*12 ));

		categoryaxis1.addSubCategory(barCategorydataset.getColumnKey(4),
				new Point2D.Double(-30, 5+scale*25));
		
		categoryaxis1.addSubCategory(barCategorydataset.getColumnKey(5),
				new Point2D.Double(0, 5));
		
		
		categoryaxis1.addSubCategory(barCategorydataset.getColumnKey(6),
				new Point2D.Double(0, 5));
		*/
	
		/**
		 * high low lines
		 * 
		 */

		IntervalLineRenderer minMaxRenderer = new IntervalLineRenderer();

		minMaxRenderer.setBaseStroke(new BasicStroke(1.2f));
		minMaxRenderer.setPaint(Color.black);
		// minMaxRenderer.setOffsetX(-0.1);
		minMaxRenderer.setSeriesVisibleInLegend(false);
		minMaxRenderer.setItemMargin(0);

		CategoryPlot lastCategoryplot = new CategoryPlot(intervalDataset2,
				categoryaxis1, null, minMaxRenderer);
		lastCategoryplot.setDomainGridlinesVisible(false);
		lastCategoryplot.setOutlineVisible(false);

		// categoryaxis1.setFixedDimension(23); // doesn't work
		ABMIMultiColorBarRenderer barRenderer = new ABMIMultiColorBarRenderer(
				this.otherColorCodes);

		barRenderer
				.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());

		barRenderer.setMaximumBarWidth(0.1);
		barRenderer.setSeriesVisibleInLegend(false);
		barRenderer.setShadowVisible(false);
		barRenderer.setBarPainter(new StandardBarPainter());
		barRenderer.setItemMargin(0);
	
		lastCategoryplot.setDataset(1, barCategorydataset);
		lastCategoryplot.setRenderer(1, barRenderer);
		lastCategoryplot.setOutlineVisible(false);
		
		
		lastCategoryplot.setBackgroundPaint(null);
		lastCategoryplot.setBackgroundAlpha(0.0f); // make plot transparent
		combinedrangecategoryplot.add(lastCategoryplot, 8); // 5: is the weight,
															// porpotion; or use
															// the number of
															// bars

		NumberAxis yAxis = (NumberAxis) combinedrangecategoryplot.getRangeAxis();
		yAxis.setAutoRange(true);
		yAxis.setTickUnit(new NumberTickUnit(yAxis.getUpperBound()/4.0));
		/**
		 * create chart
		 */

		ABMIJFreeChart chart = new ABMIJFreeChart(graphType.getWgtTitle(),
				labelFont, combinedrangecategoryplot, false);

		ABMILabel note = new ABMILabel("Age", labelFont,
				new Color(0, 0, 0), 10+ 10*scale, graphType.getWgtHeight()*scale - (38 )*scale - (int)padding.getBottom() );
		
		chart.addNote(note);
		// hide default legend.
		chart.removeLegend();
		
		GraphService.saveChartAsImage(chart, graphType, path, scale, padding);
		
		return true;
		
	}

}
