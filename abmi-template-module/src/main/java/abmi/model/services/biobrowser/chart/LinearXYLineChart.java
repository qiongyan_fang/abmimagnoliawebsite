package abmi.model.services.biobrowser.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;

import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CategoryPlot;

import org.jfree.chart.plot.PlotOrientation;

import org.jfree.chart.renderer.category.LineAndShapeRenderer;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleInsets;
import org.jfree.util.UnitType;

import abmi.model.entity.biobrowser.graph.WebGraphCategoryDetail;
import abmi.model.entity.biobrowser.graph.WebGraphLinearData;
import abmi.model.entity.biobrowser.graph.WebGraphSery;
import abmi.model.entity.biobrowser.graph.WebGraphType;

import abmi.model.services.biobrowser.GraphService;
import abmi.model.util.CommonUtil;

public class LinearXYLineChart {
	@PersistenceContext(unitName = "data")
	EntityManager em;

	double maxValue;
	String fontName = "Open Sans";
	/**
	 * get linear 10% abundance values for both north (forest) and
	 * south(prairie) regions.
	 * 
	 * @param tsn
	 * @param labels
	 * @param categoryKeys
	 * @param bNorth
	 * @return
	 */
	DefaultCategoryDataset getData(int tsn, HashMap<String, String> labels, ArrayList<String> categoryKeys,
			boolean bNorth) {

		DefaultCategoryDataset category = new DefaultCategoryDataset();

		// String[] rowKeys = { "0","0", "0", "0", "0", "0", "0", "0", "0" };

		TypedQuery<WebGraphLinearData> query = em.createQuery(
				"select  p from WebGraphLinearData p where p.wgldTsn =:tsn and p.wgldIsNorth=:bNorth",
				WebGraphLinearData.class);

		query.setParameter("tsn", tsn);
		query.setParameter("bNorth", (bNorth ? 1 : 0));

		for (WebGraphLinearData row : CommonUtil.emptyIfNull(query.getResultList())) {

			category.addValue(row.getWgldStart(), labels.get("soft"), categoryKeys.get(0));
			category.addValue(row.getWgldSoft(), labels.get("soft"), categoryKeys.get(1));

			category.addValue(row.getWgldStart(), labels.get("hard"), categoryKeys.get(0));
			category.addValue(row.getWgldHard(), labels.get("hard"), categoryKeys.get(1));

			this.maxValue = Math.max(this.maxValue, row.getWgldStart());
			this.maxValue = Math.max(this.maxValue, row.getWgldSoft());
			this.maxValue = Math.max(this.maxValue, row.getWgldHard());
		}
		if (category.getColumnCount() == 0) {
			return null;
		}
		return category;

	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public boolean createChart(WebGraphType graphType, int tsn, String path, Boolean bNorth) {
		HashMap<String, String> labels = new HashMap<String, String>();
		ArrayList<String> categoyList = new ArrayList<String>();
		for (WebGraphSery series : graphType.getWebGraphSeries()) { // acutally
																	// only two:
																	// one is
																	// soft
																	// liear,
																	// the other
																	// is hard
																	// linear
			if (series.getWgsLabel().toLowerCase().indexOf("soft") != -1) {
				labels.put("soft", series.getWgsLabel());
				labels.put("softColor", series.getWgsColor());
			} else {
				labels.put("hard", series.getWgsLabel());
				labels.put("hardColor", series.getWgsColor());
			}

			for (WebGraphCategoryDetail detail : series.getWebGraphCategory().getWebGraphCategoryDetails()) {
				if (!categoyList.contains(detail.getWgcLabel()))
					categoyList.add(detail.getWgcLabel());
			}
		}

		DefaultCategoryDataset dataset = this.getData(tsn, labels, categoyList, bNorth);

		if (dataset == null) {
			return false;
		}

		JFreeChart chart = ChartFactory.createLineChart(graphType.getWgtTitle(), graphType.getWgtDomainLabel(),
				graphType.getWgtValueLabel(), dataset, PlotOrientation.VERTICAL, true, true, false);

		chart.setBackgroundPaint(new Color(255, 255, 255, 255)); // set
																	// background
																	// as
																	// transparent
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		plot.setBackgroundPaint(null);
		plot.setBackgroundAlpha(0.0f); // make plot transparent

		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);

		// configure plot; line and shapes
		LineAndShapeRenderer xylineandshaperenderer = (LineAndShapeRenderer) plot.getRenderer();
		xylineandshaperenderer.setShapesVisible(true);
		xylineandshaperenderer.setShapesFilled(true);

		// set color and shape
		xylineandshaperenderer.setSeriesPaint(0, Color.decode(labels.get("softColor")));
		Shape circle = new Ellipse2D.Double(-8, -8, 16, 16);
		xylineandshaperenderer.setSeriesShape(0, circle);

		Shape square = new Rectangle2D.Double(-8, -8, 16, 16);
		xylineandshaperenderer.setSeriesShape(1, square);

		xylineandshaperenderer.setSeriesPaint(1, Color.decode(labels.get("hardColor")));

		// set thickness of the line
		xylineandshaperenderer.setStroke(new BasicStroke(2.5f));

		// set y axis label, and line

		this.maxValue *= 1.2;
		double upperBound = 0;
		if (this.maxValue < 0.005) {
			upperBound = (Math.round(maxValue * 10000 + 0.5)) / 1000.0;
		} else if (this.maxValue < 0.05) {
			upperBound = (Math.round(maxValue * 1000 + 0.5)) / 1000.0;
		} else if (this.maxValue < 0.5) {
			upperBound = (Math.round(maxValue * 100 + 0.5)) / 100.0;
		} else {
			upperBound = (Math.round(maxValue * 10 + 0.5)) / 10.0;
		}

		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		yAxis.setAutoRangeIncludesZero(true);
		yAxis.setLabelFont(new Font(fontName, Font.BOLD, 15));
		yAxis.setVerticalTickLabels(true);

		// System.out.println("tick unit" + (this.maxValue/2.0));
		yAxis.setTickUnit(new NumberTickUnit(upperBound / 2.0));
		yAxis.setUpperBound(upperBound);
		yAxis.setAxisLineStroke(new BasicStroke(1.5f));
		yAxis.setAxisLinePaint(new Color(0, 0, 0));
		yAxis.setAutoTickUnitSelection(false);

		// configure xAxis

		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(new Color(100, 100, 100));
		plot.setRangeAxis(yAxis);

		CategoryAxis xAxis = plot.getDomainAxis();
		xAxis.setLabelFont(new Font(fontName, Font.BOLD, 10));
		xAxis.setAxisLinePaint(new Color(0, 0, 0));
		xAxis.setAxisLineStroke(new BasicStroke(1.5f));
		plot.setDomainAxis(xAxis);

		plot.setOutlineVisible(false);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
		// put legend inside plot using the custom annotation class
		// plot.addAnnotation(new CategoryTitleAnnotation(chart.getLegend()));
		// // embed
		// region
		// in
		// plot
		// hide default region.
		// chart.removeLegend();
		
		BlockBorder noBorder = new BlockBorder(new RectangleInsets(
		        UnitType.ABSOLUTE, 0.0, 0.0, 0.0, 0.0), new Color(255, 255, 255, 255));
		
		
		chart.getLegend().setFrame(noBorder);
//		chart.getLegend().setBackgroundPaint(new Color(255, 255, 255, 255));
		chart.getLegend().setItemFont(new Font(fontName, Font.PLAIN, 12));
		chart.getLegend().setBackgroundPaint(null);
		
		
		RectangleInsets padding = new RectangleInsets(20, 0, 0, 10);
		return GraphService.saveChartAsImage(chart, graphType, path, 1, padding);
	}
}
