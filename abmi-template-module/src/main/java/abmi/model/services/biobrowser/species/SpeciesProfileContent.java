package abmi.model.services.biobrowser.species;

import java.util.List;
import java.util.Map;

public class SpeciesProfileContent {

	/*
	 * {Header1, header2, header3}
title,
description

List<Graphs>

Graph: 

	 */
	
	Map<String,String> section;

	Map<String,String> readMore;
	
	SpeciesProfileGraph graph;

	public SpeciesProfileGraph getGraph() {
		return graph;
	}
	public void setGraph(SpeciesProfileGraph graph) {
		this.graph = graph;
	}
	
//	public List<SpeciesProfileGraph> getGraphSection() {
//		return graphSection;
//	}
//	public void setGraphSection(List<SpeciesProfileGraph> graphSection) {
//		this.graphSection = graphSection;
//	}
	
	public String toString(){
		String output = "";
	
	
		
		
	
		
		return output;
	}
	public Map<String, String> getSection() {
		return section;
	}
	public void setSection(Map<String, String> section) {
		this.section = section;
	}
	public Map<String, String> getReadMore() {
		return readMore;
	}
	public void setReadMore(Map<String, String> readMore) {
		this.readMore = readMore;
	}
}
