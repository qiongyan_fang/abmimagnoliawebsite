package abmi.model.services.biobrowser.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jaxen.function.StartsWithFunction;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;

import org.jfree.chart.renderer.xy.StandardXYBarPainter;

import org.jfree.ui.RectangleInsets;
import org.jfree.ui.TextAnchor;

import abmi.model.util.chart.ext.SimpleIntervalXYDataset;

import abmi.model.entity.biobrowser.graph.WebGraphSectorData;

import abmi.model.entity.biobrowser.graph.WebGraphType;
import abmi.model.services.biobrowser.GraphService;
import abmi.model.util.CommonUtil;

import abmi.model.util.chart.renderer.ABMIMultiColorXYBarRenderer;

public class SectorEffectorChart {
	double maxValue = -10000;
	double minValue = 10000;
	@PersistenceContext(unitName = "data")
	EntityManager em;
	String fontName = "Open Sans";

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	ArrayList<String> colors;
	ArrayList<String> labels;

	SimpleIntervalXYDataset getData(int tsn, boolean bNorth) {

		this.colors = new ArrayList<String>();
		this.labels = new ArrayList<String>();

		// String[] rowKeys = { "0","0", "0", "0", "0", "0", "0", "0", "0" };

		TypedQuery<WebGraphSectorData> query = em.createQuery(
				"select  p from WebGraphSectorData p where p.wgsdTsn =:tsn and p.webGraphSectorArea.wgsaIsNorth=:bNorth"
						+ " order by p.webGraphSectorArea.webGraphSery.wgsOrder",
				WebGraphSectorData.class);

		query.setParameter("tsn", tsn);
		query.setParameter("bNorth", (bNorth ? 1 : 0));

		ArrayList<Double> breaks = new ArrayList<Double>();
		ArrayList<Double> yValues = new ArrayList<Double>();
		for (WebGraphSectorData row : CommonUtil.emptyIfNull(query.getResultList())) {

			double unitValue = Math.round(row.getWgsdUniteffect()  + 0.5) ;

			breaks.add(row.getWebGraphSectorArea().getWgsaArea());
			yValues.add(unitValue);
			labels.add(row.getWebGraphSectorArea().getWebGraphSery().getWgsLabel());
			colors.add(row.getWebGraphSectorArea().getWebGraphSery().getWgsColor());
			if (row.getWgsdUniteffect() > this.maxValue) {
				this.maxValue = unitValue;
			}

			if (row.getWgsdUniteffect() < this.minValue) {
				this.minValue = unitValue;
			}

		}
		if (breaks.size() == 0)
			return null;
		Double[] starts = new Double[breaks.size()];
		Double[] ends = new Double[breaks.size()];
		starts[0] = 0d;
		for (int i = 0; i < breaks.size(); i++) {
			if (i > 0) {
				starts[i] = starts[i - 1] + breaks.get(i - 1);
			}

			ends[i] = breaks.get(i) + starts[i];
		}

		SimpleIntervalXYDataset dataset = new SimpleIntervalXYDataset(labels.toArray(new String[labels.size()]), starts,
				ends, yValues.toArray(new Double[yValues.size()]));

		return dataset;

	}

	// SimpleIntervalXYDataset getLegendData(){
	// SimpleIntervalXYDataset dataset = new SimpleIntervalXYDataset();
	// for (int i=0; i < this.labels.size(); i++) {
	//
	// double[][] xy = new double[2][1];
	// xy[0][0] = 0;
	// xy[1][0] = 0;
	// dataset.addSeries(this.labels.get(i), xy);
	// }
	//
	// return dataset;
	//
	// }
	public boolean createChart(WebGraphType graphType, int tsn, String path, Boolean bNorth) {

		SimpleIntervalXYDataset dataset = this.getData(tsn, bNorth);

		if (dataset == null) {
			return false;
		}

		JFreeChart chart = ChartFactory.createXYBarChart(graphType.getWgtTitle(), graphType.getWgtDomainLabel(), false,
				graphType.getWgtValueLabel(), dataset, PlotOrientation.VERTICAL, true, true, false);

		chart.setBackgroundPaint(new Color(255, 255, 255, 255)); // set
																	// background
																	// as
																	// transparent
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setBackgroundPaint(null);
		plot.setBackgroundAlpha(0.0f); // make plot transparent

		plot.setDomainGridlinesVisible(false);
		plot.setRangeGridlinePaint(Color.white);

		// configure plot; line and shapes
		ABMIMultiColorXYBarRenderer renderer = new ABMIMultiColorXYBarRenderer(this.colors);
		renderer.setShadowVisible(false);
		renderer.setBarPainter(new StandardXYBarPainter());

		renderer.setBaseItemLabelGenerator(new StandardXYItemLabelGenerator("{2}"));

		renderer.setSeriesItemLabelFont(0, new Font(fontName, Font.BOLD, 11));
		renderer.setSeriesItemLabelsVisible(0, true);

		// new ItemLabelPosition(ItemLabelAnchor.OUTSIDE3,
		// TextAnchor.BOTTOM_LEFT, TextAnchor.BOTTOM_LEFT, 45.0));

		// renderer.setSeriesPositiveItemLabelPosition( 0, new
		// ItemLabelPosition(ItemLabelAnchor.OUTSIDE3, TextAnchor.BOTTOM_LEFT,
		// TextAnchor.BOTTOM_LEFT, 45.0));
		plot.setRenderer(renderer);
		plot.setBackgroundPaint(new Color(237, 240, 250));
		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		// NumberAxis yAxis = new NumberAxis(null);

		yAxis.setLabelFont(new Font(fontName, Font.BOLD, 15));
		yAxis.setUpperBound(Math.max(100, maxValue * 1.2));
		yAxis.setLowerBound(Math.min(-100, this.minValue * 1.2));
		yAxis.setAxisLineStroke(new BasicStroke(1.5f));
		yAxis.setAxisLinePaint(new Color(0, 0, 0));
//		double range = Math.max(100, maxValue * 1.2) - Math.min(-100, this.minValue * 1.2);
//		int unit = 10;
//		if (range >= 100)
//			unit = (int) ((Math.round(range * 0.1) * 10) / 10);
//
//		yAxis.setTickUnit(new NumberTickUnit(unit, new DecimalFormat("##0.#%")));

		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(new Color(100, 100, 100));
		plot.setRangeAxis(yAxis);
		plot.setOutlineVisible(false);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
		// configure xAxis
		ValueAxis xAxis = plot.getDomainAxis();
		xAxis.setAutoRange(true);
		xAxis.setLowerBound(xAxis.getUpperBound() * -0.01);
		xAxis.setAxisLineStroke(new BasicStroke(1.5f));
		xAxis.setAxisLinePaint(new Color(0, 0, 0));

		// put legend inside plot using the custom annotation class
		BlockBorder noBorder = new BlockBorder(new Color(255, 255, 255, 255));
		
		
		chart.getLegend().setFrame(noBorder);
//		chart.getLegend().setBackgroundPaint(new Color(255, 255, 255, 255));
		chart.getLegend().setItemFont(new Font(fontName, Font.PLAIN, 12));
		chart.getLegend().setBackgroundPaint(null);
		// chart.removeLegend();

		return GraphService.saveChartAsImage(chart, graphType, path);
	}
}
