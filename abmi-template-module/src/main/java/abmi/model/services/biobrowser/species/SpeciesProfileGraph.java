package abmi.model.services.biobrowser.species;

import java.util.List;
import java.util.Map;

public class SpeciesProfileGraph {

	
/*	spp[title] =""
spp[type] = forest
spp[for] = 0
graphDetail[0][link] =
graphDetail[0][header] =
graphDetail[0][type] =
graphDetail[0][description] =
graphDetail[1][link] =
graphDetail[1][header] =
graphDetail[1][type] =
graphDetail[1][description] =
readMore[0][header]
readMore[0][content]
readMore[1][header]
readMore[1][content*/
	
	// to used it less generalized. to limited the pass in parameter dimensions.
	/**
	 * final String LINEAR_FOOTPRINT_GRAPH = "Relationship to Linear Footprint";
	final String SECTOR_EFFECTS_GRAPH = "Human Footprint Effects";
	final String NON_TREED_SITE_GRAPH = "Non-Treed or Treed Sites";
	final String SPECIES_ABUNDANCE_GRAPH = "species relative abundance";
	 final String LINEAR_FOOTPRINT_GRAPH = "Relationship to Linear Footprint";
		final String SECTOR_EFFECTS_GRAPH = "Human Footprint Effects";
		final String NON_TREED_SITE_GRAPH = "Non-Treed or Treed Sites";
		final String SPECIES_ABUNDANCE_GRAPH = "species relative abundance";
		 */
	

	Map<String,String> headers;
	 
	String method;
	String limitation;
	String graph1;
	String graph2;

	String graph3;
	String description1;
	String description2;
	String description3;
	
	

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getLimitation() {
		return limitation;
	}

	public void setLimitation(String limitation) {
		this.limitation = limitation;
	}

	public String getGraph1() {
		return graph1;
	}

	public void setGraph1(String graph1) {
		this.graph1 = graph1;
	}

	public String getGraph2() {
		return graph2;
	}

	public void setGraph2(String graph2) {
		this.graph2 = graph2;
	}

	public String getGraph3() {
		return graph3;
	}

	public void setGraph3(String graph3) {
		this.graph3 = graph3;
	}

	public String getDescription1() {
		return description1;
	}

	public void setDescription1(String description1) {
		this.description1 = description1;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public String getDescription3() {
		return description3;
	}

	public void setDescription3(String description3) {
		this.description3 = description3;
	}

	
	

	public String toString(){
		String output = "";
		output += " headers = " + headers.toString();
		
		
		return output;
	}
	
	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	
}
