package abmi.model.services.biobrowser;

import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.BaseFont;

/**
 * Use to define colors, fonts, images
 * 
 * @author Qiongyan
 *
 */
public class PdfDownloadStyles {

	/*
	 * Blue Banner background: #0E3444 (14,52,68) Orange header: #cc6046
	 * (204,96,70) Orange font: #751e11 (scientific names) (117,30,17) Header
	 * color: #0e3443 (14,52,67) Green graph header: #748938 (116,137,56) Orange
	 * graph header: #ca7a44 (202,122,68) Method limitation header: #8d1b1d
	 * (141,27,29)
	 * 
	 * 
	 */
	 public BaseColor blueBGColor = new BaseColor(14, 52, 68);
	 public BaseColor orangeBGColor = new BaseColor(204, 96, 70);
	 public BaseColor mapBGColor = new BaseColor(249, 239, 233);
	 
	 public BaseColor orangeColor = new BaseColor(117, 30, 17); // for
																		// scientific
																		// names

	 public BaseColor mainHeadingColor = new BaseColor(14, 52, 67);
	 public BaseColor greenGraphHeadingColor = new BaseColor(116, 137, 56);
	 public BaseColor orangeGraphHeadingColor = new BaseColor(202, 122, 68);

	 public BaseColor methodHeadingColor = new BaseColor(141, 27, 29);
	 public BaseColor headerFooterColor = new BaseColor(109, 110, 112);

	 public Font headingFont;
	 public Font headingWhiteFont;
	 public Font greenGraphHeadingFont;
	 public Font orangeGreenHeadingFont;

	 public Font methodHeadingFont;

	 public Font bigSpeciesHeadingFont;
	 public Font smallSpeciesHeadingFont;

	 public Font textFont;
	 public Font headerFooterFont, headerFooterWhiteFont;

	// paths can be either url or absolute path on server computer
	 public String mapPath =  "http://species.abmi.ca/contents/species/";
	 public String abmiHeaderImagePath = "/assets/abmi_logo.png";
	 public String forestIcon = "/assets/Forested-Region.png";
	 public String prairieIcon = "/assets/Prairie-Region.png";

	 public String headerText = "ABMI Species Profile Series";

	 public String author = "Alberta Biodiversity Monitoring Insititue";
	 public String footerText = "www.abmi.ca";

	 public String[] mainheader = { "Introduction", "Habitat & Human Footprint Associations",
			"Impacts of Human Footprint", "Predicted Relative Abundance", "Impacts of Climate Change", "Other Issues",
			"References" };

	 public String[] keywords = { "banner", "introduction", "hfAssociations", "graphForestAssociation",
			"graphPrairie", "graphLinearFP", "impactFP", "map", "climate", "reference" };

	// first graph only one,
	 public String speciesForestGraphHeader = "Species-habitat Associations in the Forested Region";

	// second graph, has a pair of graphs. with non-tree or tree
	 public String speciesPrairieGraphHeader = "Species-habitat Associations in the Prairie Region";

	 public String nonTreedGraphHeader = "Non-Treed Sites in the Prairie Region";
	 public String treedGraphHeader = "Treed Sites in the Prairie Region";

	// third graph
	 public String linearFPHeader = "Relationship to Linear Footprint";

	 public String linearFPForestGraphHeader = "Relationship to Linear Footprint in the Forest Region";
	 public String linearFPPrairieGraphHeader = "Relationship to Linear Footprint in the Prairie Region";

	 public String hfEffectForestGraphHeader = "Human Footprint Effects in the Forested Region";
	 public String hfEffectPrairieGraphHeader = "Human Footprint Effects in the Prairie Region";
	 
	 public String hfFewDetectionNorthGraphHeader = "Habitat Associations for Species with Few Detection in the Forested Region";
	 public String hfFewDetectionSouthGraphHeader = "Habitat Associations for Species with Few Detection in the Prairie Region";

	 public String predictedRAHeader = "Predicted Relative Abundance";
	// forth graph, 3 maps
	 public String[] mapHeader = { "Reference Conditions", "Current Conditions", "Difference Conditions" };

	// reference subheaders
	 public String referenceSubHeader = "References";
	 public String dataSourceSubHeader = "Data Sources";
	 public String creditSubHeader = "Photo & Credits";
	 public String citationSubHeader ="Recommended Citation";
	 public	BaseFont osFont;
	public PdfDownloadStyles() {
		String[] FONTS[] = { { "c:/windows/fonts/RobotoSlab-Regular.ttf", BaseFont.WINANSI },
				{ "c:/windows/fonts/RobotoSlab-Bold.ttf", BaseFont.WINANSI },
				{ "c:/windows/fonts/OpenSans-Regular.ttf", BaseFont.WINANSI },
				{ "c:/windows/fonts/OpenSans-Bold.ttf", BaseFont.WINANSI },
				{ "c:/windows/fonts/OpenSans-SemiboldItalic", BaseFont.WINANSI } };

		try {
			BaseFont rsFont, rsbFont ;
			// get fonts
			try{
				rsFont = BaseFont.createFont(FONTS[0][0], FONTS[0][1], BaseFont.EMBEDDED);
			}catch(Exception e){
				rsFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI,
						false);
			}
			
			try{
				rsbFont = BaseFont.createFont(FONTS[1][0], FONTS[1][1], BaseFont.EMBEDDED);
			}catch(Exception e){
				rsbFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI,
						false);
			}

			osFont = BaseFont.createFont(FONTS[2][0], FONTS[2][1], BaseFont.EMBEDDED);

			headingFont = new Font(rsFont, 16,  Font.NORMAL, mainHeadingColor);
			headingWhiteFont = new Font(rsFont, 18, Font.NORMAL, BaseColor.WHITE);
			greenGraphHeadingFont = new Font(rsFont, 14, Font.NORMAL, greenGraphHeadingColor);
			orangeGreenHeadingFont = new Font(rsFont, 14, Font.NORMAL, orangeGraphHeadingColor);

			methodHeadingFont = new Font(rsFont, 12, Font.NORMAL, methodHeadingColor);

			bigSpeciesHeadingFont = new Font(rsFont, 14, Font.NORMAL, BaseColor.WHITE);
			smallSpeciesHeadingFont = new Font(rsFont, 12, Font.ITALIC, orangeColor);

			textFont = new Font(osFont, 9, Font.NORMAL, BaseColor.BLACK);

			headerFooterFont = new Font(rsFont, 6, Font.NORMAL, headerFooterColor);
			headerFooterWhiteFont = new Font(rsFont, 6, Font.NORMAL, BaseColor.WHITE);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void setRootPath(String rootPath){
		this.forestIcon = rootPath + this.forestIcon ;
		this.prairieIcon = rootPath + this.prairieIcon ;
		this.abmiHeaderImagePath = rootPath + this.abmiHeaderImagePath;
	}
	
}
