package abmi.model.services.biobrowser;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import abmi.api.services.speciesprofiles.SpeciesProfileDAO;
import abmi.model.entity.biobrowser.WebSpeciesViewV;
import abmi.model.entity.biobrowser.graph.WebGraphAvailablity;

import abmi.model.util.CommonUtil;
import abmi.model.util.pdf.SpeciesProfilePDFHeaderFooter;

/**
 * this class read data from repository and database, and some are from website.
 * and combined them to create a pdf species profile download. it is in use.
 * 
 * @author Qiongyan
 *
 */
public class PdfDownload {
	private static final Logger log = LoggerFactory.getLogger(PdfDownload.class);
	String rootPath;

	PdfDownloadStyles styleObj;
	String[] gExternalSppFolder = { "birds", "vplants", "mites", "lichens", "mosses", "", "mammals" }; // species
	String CSS = "p {margin-bottom:5px; font-size: 10px; line-height:13px;} "
			+ " ul {   list-style-type: disc;} "
			+ " li { margin-bottom:5px;font-size: 10px; line-height:13px;} ";

	final String FULL = "Full";
	final String FEW = "Few";
	final String NONE = "None";

	public PdfDownload() {
		styleObj = new PdfDownloadStyles();
	}

	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
		styleObj.setRootPath(rootPath);
	}

	String description = "description";

	int groupId, tsn;
	String commonName, scientificName;

	int imageHeight = 180;

	public String createPDFBackEnd(WebSpeciesViewV sppRow, WebGraphAvailablity statusRow,
			List<Map<String, Object>> webContent) {

		if (statusRow == null) {
			return null;
		}
		scientificName = sppRow.getWsbScientificName();
		groupId = sppRow.getSppGroup().getSgId();
		commonName = sppRow.getWsbCommonName();
		tsn = sppRow.getWsbTsn();

		String forestModel = statusRow.getWebForestModel().getWgmtCode();
		String prairieModel = statusRow.getWebPrairieModel().getWgmtCode();

		/* used to determine the graph table column size */
		int fullCount = 0;
		if (FULL.equals(forestModel) && FULL.equals(prairieModel)) {
			fullCount = 2;
		} else if (FULL.equals(forestModel) || FULL.equals(prairieModel)) {
			fullCount = 1;
		}

		/*--------------------------------------------------
		 * if the pdf already exists, return it.
		 *-------------------------------------------------*/
		File outputPdf = new File(
				this.rootPath + "/report/" + sppRow.getSppGroup().getSgGroupName().replaceAll(" ", "_") + "\\"
						+ scientificName.replaceAll(" ", "_") + "_tmp.pdf");
		String pathStr = "/report/" + sppRow.getSppGroup().getSgGroupName().replaceAll(" ", "_") + "\\"
				+ scientificName.replaceAll(" ", "_") + ".pdf";
		File outputPdfDest = new File(this.rootPath + pathStr);

		if (outputPdfDest.exists()) {
			return pathStr;
		}
		// create folders
		outputPdf.getParentFile().mkdirs();

		/*--------------------------------------------------
		 * otherwise generate on the fly
		 *-------------------------------------------------*/
		Document document = null;
		PdfWriter writer = null;
		try {

			Map<String, Object> profiles = SpeciesProfileDAO.getSingleSpeciesProfile(false, scientificName, "");

			/*--------------------------------------------------
			 * Define document pdf margin size
			 *--------------------------------------------------*/
			document = new Document();
			document.setPageSize(PageSize.LETTER); // 612, 792
			document.setMargins(36, 36, 50, 72); // left, right,
													// top, bottom
			// document.setMarginMirroring(false);
			document.addAuthor(styleObj.author);

			/*--------------------------------------------------
			 * step 2: we create a writer that listens to the document to
			 * 1. create header and footer
			 * 2. page numbers
			 *--------------------------------------------------*/

			writer = PdfWriter.getInstance(document, new FileOutputStream(outputPdf));

			SpeciesProfilePDFHeaderFooter pageHeaderFooterEvent = new SpeciesProfilePDFHeaderFooter();
			pageHeaderFooterEvent.setStyleObj(styleObj);
			pageHeaderFooterEvent.setHeaderBannerText(styleObj.headerText);
			pageHeaderFooterEvent.setHeaderImage(styleObj.abmiHeaderImagePath);
			pageHeaderFooterEvent.setHeaderText(styleObj.headerText + ": " + CommonUtil.nvl(commonName, scientificName));

			pageHeaderFooterEvent.setFooterText(styleObj.footerText);
			pageHeaderFooterEvent.setFootImage(styleObj.abmiHeaderImagePath);

			writer.setPageEvent(pageHeaderFooterEvent);
			writer.setViewerPreferences(PdfWriter.PageModeUseOutlines);

			/*--------------------------------------------------
			 * open document
			 *--------------------------------------------------*/
			document.open();

			/*--------------------------------------------------
			 * add first page blue banners  and profile banner( tables with species profile images, distribution maps, and 
			 * species information (names, description and guilds)
			 *--------------------------------------------------*/
			Map<String, Object> introductionBanner = webContent.get(0); // introduction
			addBanner(document);
			addProfileBanner(document, profiles, introductionBanner);

			/*--------------------------------------------------
			 * 	 main content introduction (content from html page) *
			 *--------------------------------------------------*/

			/*-------------------------------------------------------------------
			 * when either forest or prairie model are not none, we need to display
			 * the habitat header,
			 * 
			 * 
			 ------------------------------------------p---------------------------*/
			if (!NONE.equals(forestModel) || !NONE.equalsIgnoreCase(prairieModel)) {
				Map<String, Object> introductionSection = webContent.get(1); // introduction
				String introductionHtml = "";

				try {
					introductionHtml = (String) introductionSection.get(description);
					introductionHtml += ((Map<String, String>) introductionSection.get("readMore")).get("Read More")
							.trim();

				} catch (Exception e) {

				}

				/* only display introduction headers when the introduction contents are not empty */
				if (introductionHtml != null && !introductionHtml.equals("null") && introductionHtml.length() > 0){
					addHeaderParagrph(document, styleObj.mainheader[0], introductionHtml);
				}
				
				/*------------------------------------------------------------------------------------------
				 * For Full Model species, can we have a page break so the Habitat&Human footprint associations sections starts of the next page?
				 ---------------------------------------------------------------------------------------------------------------------------------------*/
				if ( FULL.equals(forestModel) || FULL.equals(prairieModel)){ 
					document.newPage();
				}

				addHeaderParagrph(document, styleObj.mainheader[1], (String) profiles.get("WebHabitat"));
			}

			/* display north few detection graph */
			if (FEW.equals(forestModel)) {
				try {
					Map<String, Object> fewDetectionNorthGraphSect = webContent.get(8); // tree
					// graph

					// String graphHeaderIcon[] = {};
					String graphPath[] = { GraphService.getGraphPath(sppRow,
							GraphService.FEW_DETCTION_SPECIES_NORTH_GRAPH, true, null) };
					// String graphHeaderText[] = {
					// styleObj.speciesForestGraphHeader };

					String contentHtml[] = { "" };

					addGraphParagraph(document, styleObj.hfFewDetectionNorthGraphHeader, true, null, null, graphPath,
							contentHtml, (Map<String, Object>) fewDetectionNorthGraphSect.get("readMore"), imageHeight);
				} catch (Exception e) {
					e.printStackTrace();
					log.error(e.getMessage());
				}
			}

			if (FULL.equals(forestModel)) {
				/*--------------------------------------------------
				 *  forest header: Habitat & Human Footprint Associations
				 *--------------------------------------------------*/

				Map<String, Object> forestSect = webContent.get(2); //

				// addHeaderParagrph(document, styleObj.mainheader[1], (String)
				// profiles.get("WebHabitat"));

				addAllReadMoreParagraph(document, (Map<String, Object>) forestSect.get("readMore"));

				/*--------------------------------------------
				 * first graph, big one
				 --------------------------------------------*/

				try {
					// Map<String, Object> bigGraphSect = webContent.get(2); //
					// tree
					//// // graph

					// String graphHeaderIcon[] = {};
					String graphPath[] = {
							GraphService.getGraphPath(sppRow, GraphService.SPECIES_ABUNDANCE_GRAPH, null, null) };
					// String graphHeaderText[] = {
					// styleObj.speciesForestGraphHeader };

					String contentHtml[] = { (String) profiles.get("WebForestedRegionGraphText") };

					addGraphParagraph(document, styleObj.speciesForestGraphHeader, true, null, null, graphPath,
							contentHtml, null, 0);
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			/*
			 * -------------------------------------------- when prairieModel is
			 * FULL second graph, side by side tree and non-tree graph and their
			 * descriptions only display one if the other one is not full model
			 * --------------------------------------------
			 */
			if (FULL.equalsIgnoreCase(prairieModel)) {

				try {
					Map<String, Object> treeGraphSect = webContent.get(3); // tree
																			// graph

					// graph
					Boolean graphHeaderIcon[] = null;

					String graphPath[] = {
							GraphService.getGraphPath(sppRow, GraphService.NON_TREED_SITE_GRAPH, null, false),
							GraphService.getGraphPath(sppRow, GraphService.NON_TREED_SITE_GRAPH, null, true), };
					String contentHtml[] = {
							(String) profiles.get("WebNonTreeGraphText") + (String) profiles.get("WebTreeGraphText") };

					String graphHeaderText[] = { styleObj.nonTreedGraphHeader, styleObj.treedGraphHeader };

					addGraphParagraph(document, styleObj.speciesPrairieGraphHeader, false, graphHeaderIcon,
							graphHeaderText, graphPath, contentHtml,
							(Map<String, Object>) treeGraphSect.get("readMore"), imageHeight);

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			/*--------------------------------------------
			 *  third graph, side by side linear human footprint graphs
			 *  
			 *  when only one model is full only display one column
			 --------------------------------------------*/
//			LineSeparator line = new LineSeparator();
//			line.setOffset(10);
//
//			document.add(line);
			
			/*------------------------------------------------------------------------------------------
			 * For Full Model species, can we have a page break so the Habitat&Human footprint associations sections starts of the next page?
			 ---------------------------------------------------------------------------------------------------------------------------------------*/
			if ( FULL.equals(forestModel) && FULL.equals(prairieModel)){ 
				document.newPage();
			}

			if (FULL.equals(forestModel) || FULL.equals(prairieModel)) {
				try {
					Map<String, Object> linearGraphSect = webContent.get(4); //

					String graphPath[] = new String[fullCount];
					Boolean graphHeaderIcon[] = new Boolean[fullCount];

					String contentHtml[] = new String[fullCount];
					String graphHeaderText[] = new String[fullCount];
					int index = 0;
					if (FULL.equals(forestModel)) {
						graphHeaderIcon[index] = true;
						graphHeaderText[index] = styleObj.linearFPForestGraphHeader;
						contentHtml[index] = (String) profiles.get("WebFLinearHFGraphText"); // forest
						graphPath[index++] = GraphService.getGraphPath(sppRow, GraphService.LINEAR_FOOTPRINT_GRAPH,
								true, null);

					}

					if (FULL.equals(prairieModel)) {
						graphHeaderIcon[index] = false;
						graphPath[index] = GraphService.getGraphPath(sppRow, GraphService.LINEAR_FOOTPRINT_GRAPH, false,
								null);
						graphHeaderText[index] = styleObj.linearFPPrairieGraphHeader;
						contentHtml[index] = (String) profiles.get("WebPLinearHFGraphText"); // prairie
					}

					addGraphParagraph(document, styleObj.linearFPHeader, null, graphHeaderIcon, graphHeaderText,
							graphPath, contentHtml, (Map<String, Object>) linearGraphSect.get("readMore"), 180);

				} catch (Exception e) {
					e.printStackTrace();
					log.error(e.getMessage());
				}

				/*----------------------------------------------------------------------------------------
				 *  forth graph, side by side, sector effect graphs
				 ----------------------------------------------------------------------------------------*/

				addHeaderParagrph(document, styleObj.mainheader[2], (String) profiles.get("WebHFEffect"));

				try {
					String graphPath[] = new String[fullCount];
					Boolean graphHeaderIcon[] = new Boolean[fullCount];

					String contentHtml[] = new String[fullCount];
					String graphHeaderText[] = new String[fullCount];
					int index = 0;
					if (FULL.equals(forestModel)) {
						graphHeaderIcon[index] = true;
						graphHeaderText[index] = styleObj.hfEffectForestGraphHeader;
						contentHtml[index] = (String) profiles.get("WebHFForestRegionGraph");
						graphPath[index++] = GraphService.getGraphPath(sppRow, GraphService.SECTOR_EFFECTS_GRAPH, true,
								null);

					}

					if (FULL.equals(prairieModel)) {
						graphHeaderIcon[index] = false;
						graphPath[index] = GraphService.getGraphPath(sppRow, GraphService.SECTOR_EFFECTS_GRAPH, false,
								null);
						graphHeaderText[index] = styleObj.hfEffectPrairieGraphHeader;
						contentHtml[index] = (String) profiles.get("WebPrarieRegionGraphText");
					}

					Map<String, Object> hfImpactSect = webContent.get(5); // tree

					addGraphParagraph(document, null, null, graphHeaderIcon, graphHeaderText, graphPath, contentHtml,
							(Map<String, Object>) hfImpactSect.get("readMore"), imageHeight);

				} catch (Exception e) {
					e.printStackTrace();
					log.error(e.getMessage());
				}
			}

			/* display south few detection graph */
			if (FEW.equals(prairieModel)) {
				try {
					Map<String, Object> fewDetectionSouthGraphSect = webContent.get(9); // tree
					// graph

					// String graphHeaderIcon[] = {};
					String graphPath[] = { GraphService.getGraphPath(sppRow,
							GraphService.FEW_DETCTION_SPECIES_SOUTH_GRAPH, false, null) };
					// String graphHeaderText[] = {
					// styleObj.speciesForestGraphHeader };

					String contentHtml[] = { "" };

					addGraphParagraph(document, styleObj.hfFewDetectionSouthGraphHeader, false, null, null, graphPath,
							contentHtml, (Map<String, Object>) fewDetectionSouthGraphSect.get("readMore"), imageHeight);
				} catch (Exception e) {
					e.printStackTrace();
					log.error(e.getMessage());
				}
			}

			/*----------------------------------------------------------------------------------------
			 *  description
			 *  3 maps current reference and difference			 * 
			 ----------------------------------------------------------------------------------------*/
			if (FULL.equals(forestModel) || FULL.equals(prairieModel)) {
				addHeaderParagrph(document, styleObj.mainheader[3], (String) profiles.get("WebRange"));

				try {
					Map<String, Object> mapSect = webContent.get(6); // tree

					Map<String, Object> graph = (Map<String, Object>) mapSect.get("graph");
					String externalMapUrl = styleObj.mapPath + gExternalSppFolder[groupId - 1] + "/";
					String sppName = "";
					if (groupId == 1 || groupId == 7) {
						sppName = commonName.replaceAll(" ", "");
						sppName = sppName.replaceAll("'", "");
					} else if (groupId == 2 || groupId == 4 || groupId == 5) { // plant/moss/lichen
																				// use
																				// .
						sppName = scientificName.replaceAll(" ", ".");
					} else {
						sppName = scientificName.replaceAll(" ", "");
					}
					sppName = sppName.replaceAll("-", "");
					String map1 = externalMapUrl + "/map-rf/" + sppName + ".png";
					String map2 = externalMapUrl + "/map-cr/" + sppName + ".png";
					String map3 = externalMapUrl + "/map-df/" + sppName + ".png";

					String graphPath[] = { map1, map2, map3 };

					String contentHtml[] = { this.getRefMapText(), this.getCurrentMapText(),
							(String) profiles.get("WebDifferenceMapText") };

					String graphHeaderText[] = styleObj.mapHeader;
					addGraphParagraph(document, null, null, null, graphHeaderText, graphPath, contentHtml,
							(Map<String, Object>) mapSect.get("readMore"), 250);

				} catch (Exception e) {
					e.printStackTrace();
					log.error(e.getMessage());
				}
			}

			/*----------------------------------------------------------------------------------------
			 * Climate change
			 ----------------------------------------------------------------------------------------*/
			/*
			 * addHeaderParagrph(document, styleObj.mainheader[4], "");
			 * 
			 * addHeaderParagrph(document, "Results Coming Soon", "",
			 * styleObj.textFont);
			 */

			/*----------------------------------------------------------------------------------------
			 * Other Issues
			 ----------------------------------------------------------------------------------------*/
			String otherIssues = (String) profiles.get("WebOtherIssue");
			if (otherIssues != null && otherIssues.length() > 5) {
				addHeaderParagrph(document, styleObj.mainheader[5], otherIssues, styleObj.headingFont, true);
			}

			/*----------------------------------------------------------------------------------------
			 * Reference sections
			 ----------------------------------------------------------------------------------------*/
			// reference, data source, citation etc. // graph

			addHeaderParagrph(document, styleObj.mainheader[6], "");

			if ((String) profiles.get("WebReference") != null) {
				addHeaderParagrph(document, styleObj.referenceSubHeader, (String) profiles.get("WebReference"),
						styleObj.methodHeadingFont, false);
			}

			Map<String, Object> refSect = (Map<String, Object>) webContent.get(7).get("section"); // tree

			addHeaderParagrph(document, styleObj.dataSourceSubHeader, (String) refSect.get("data-source"),
					styleObj.methodHeadingFont, false);

//			addHeaderParagrph(document, styleObj.dataSourceSubHeader, (String) refSect.get("data-source"),
//					styleObj.methodHeadingFont, false);
			
			addHeaderParagrph(document, styleObj.citationSubHeader, (String) refSect.get("citation"),
					styleObj.methodHeadingFont, false);

			document.close();
			writer.close();

			this.addFooter(outputPdf.getAbsolutePath(), outputPdfDest.getAbsolutePath());

			return pathStr;
		} catch (

		Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			if (document != null)
				document.close();

			if (writer != null)
				writer.close();

			try {
				if (outputPdfDest != null && outputPdfDest.exists()) {
					outputPdfDest.delete();
				}
			} catch (Exception e11) {
			}

		}
		return null;
	}

	/**
	 * first page's blue banner
	 * 
	 * @param document
	 */
	void addBanner(Document document) {
		try {
			PdfPTable headerTable = new PdfPTable(2);
			headerTable.getDefaultCell().setBorder(0);
//			headerTable.setSpacingAfter(10f);
			headerTable.setSpacingBefore(-10f);
			headerTable.getDefaultCell().setPadding(0f);

			headerTable.getDefaultCell().setBackgroundColor(styleObj.blueBGColor);

			float widthCells[] = { 50, 500 };

			headerTable.setWidths(widthCells);
			headerTable.setWidthPercentage(100f);

			Image img = Image.getInstance(styleObj.abmiHeaderImagePath);
			img.scaleAbsolute(35, 35);
			PdfPCell cell = new PdfPCell(img, false);
			cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
			cell.setBackgroundColor(styleObj.greenGraphHeadingColor);
			cell.setBorder(0);

			headerTable.addCell(cell);

			Phrase textHeader = new Phrase(styleObj.headerText, styleObj.headingWhiteFont);
			PdfPCell cell2 = new PdfPCell(textHeader);

			cell2.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			cell2.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
			cell2.setPadding(10);
			cell2.setBackgroundColor(styleObj.blueBGColor);
			cell2.setBorder(0);
			cell2.setFixedHeight(50);

			headerTable.addCell(cell2);

			document.add(headerTable);
		} catch (Exception e) {

		}
	}

	/**
	 * 
	 * @param src
	 * @param dest
	 * @throws IOException
	 * @throws DocumentException
	 */
	void addFooter(String src, String dest) {
		PdfReader reader = null;
		PdfStamper stamper = null;
		try {
			reader = new PdfReader(src);

			int n = reader.getNumberOfPages();
			stamper = new PdfStamper(reader, new FileOutputStream(dest));
			PdfContentByte pagecontent;
			for (int i = 1; i <= n; i++) {
				
				pagecontent = stamper.getOverContent(i);
				if (i < n) {
					float widthCells[] = { 50, 500, 50 };
					PdfPTable table = new PdfPTable(3);
					table.setTotalWidth(550);
					table.setWidths(widthCells);

					PdfPCell cell = new PdfPCell(new Phrase("Page " + (i), styleObj.headerFooterFont));

					cell.setBorder(0);
					cell.setPaddingLeft(0);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);
					try {
						Image img = Image.getInstance(styleObj.abmiHeaderImagePath);
						img.scaleToFit(10, 10);
						cell = new PdfPCell(img);
					} catch (Exception e) {
						cell = new PdfPCell();
					}
					cell.setBorder(0);
					cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);

					cell = new PdfPCell(new Phrase("www.abmi.ca", styleObj.headerFooterFont));

					cell.setBorder(0);
					cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);
					// If you have more than one cell in a row, you need to set
					// the background for all cells. Note that I'm defining a
					// total width for the table (523 is the width of the page
					// minus the margins). The total width is needed because
					// we'll add the table using writeSelectedRows():

					table.writeSelectedRows(0, -1, 36, 34, pagecontent);
				} else { // last page add a table
					PdfPTable table = new PdfPTable(3);
					table.setTotalWidth(523);
					Image img = Image.getInstance(styleObj.abmiHeaderImagePath);
					img.scaleToFit(20, 20);
					PdfPCell cell = new PdfPCell(img);
					cell.setBackgroundColor(styleObj.blueBGColor);
					cell.setBorder(0);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);

					cell = new PdfPCell(new Phrase(styleObj.headerText, styleObj.headerFooterWhiteFont));
					cell.setBackgroundColor(styleObj.blueBGColor);
					cell.setBorder(0);
					cell.setPaddingLeft(5);
					cell.setPaddingTop(5);
					cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);

					cell = new PdfPCell(new Phrase("www.abmi.ca", styleObj.headerFooterWhiteFont));
					cell.setBackgroundColor(styleObj.blueBGColor);
					cell.setBorder(0);
					cell.setPaddingTop(5);
					cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);
					// If you have more than one cell in a row, you need to set
					// the background for all cells. Note that I'm defining a
					// total width for the table (523 is the width of the page
					// minus the margins). The total width is needed because
					// we'll add the table using writeSelectedRows():

					table.writeSelectedRows(0, -1, 36, 64, pagecontent);
				}

			}

			stamper.close();
			reader.close();

			File srcFile = new File(src);
			srcFile.delete();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (stamper != null) {
				try {
					stamper.close();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			if (reader != null) {
				reader.close();
			}
		}

	}

	/**
	 * species profile banner, similar as the web page renderes
	 * 
	 * @param document
	 * @param profileRow
	 * @param webContent
	 */
	void addProfileBanner(Document document, Map<String, Object> profileRow, Map<String, Object> webContent) {
		/* banner graph / text / distribution maps */

		String image = this.rootPath + "/profiles/" + scientificName.replace(" ", "-") + "-large.jpg";

		File imageFile = new File(image);

		String map = this.rootPath + "\\distributionMaps\\" + groupId + "_" + tsn + ".png";

		String headerCSS = "p { font-size:14px; color:#ffffff; margin-bottom:10px; }  "
				+ " .orange {font-size:14px; color:#751e11; margin-bottom:15px;}"
				+ " .smallnote {margin-top:15px; font-size:10px; color:#ffffff;}";
		String smallHeaderCSS = "p { color:#ffffff;  font-size:12px; } ";
		PdfPTable tb;

		if (!imageFile.exists()) {
			tb = new PdfPTable(2);

			float widthCells1[] = { 400, 200 };
			try {
				tb.setWidths(widthCells1);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				log.error(e1.getMessage());
			}
		} else {
			tb = new PdfPTable(3);
			Image img;
			try {
				img = Image.getInstance(image);
//				img.scaleAbsoluteWidth(150);
				img.setScaleToFitHeight(true);
				PdfPCell imgCell = new PdfPCell(img, true);
				imgCell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
				imgCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
				imgCell.setBackgroundColor(styleObj.orangeBGColor);
				imgCell.setBorder(0);
				imgCell.setPadding(0);
				// imgCell.setFixedHeight(250); // manually set cell height as
				// 620 to match profile height
				tb.addCell(imgCell);
				float widthCells1[] = { 200, 400, 200 };
				try {
					tb.setWidths(widthCells1);
				} catch (Exception e1) {

					log.error(e1.getMessage());
				}

			} catch (Exception e1) {

				log.error(e1.getMessage());
				tb = new PdfPTable(2);
				float widthCells1[] = { 400, 200 };
				try {
					tb.setWidths(widthCells1);
				} catch (Exception ex) {

					log.error(e1.getMessage());
				}
			}

		}
		tb.getDefaultCell().setBorder(0);
		tb.setSpacingAfter(0f);
		tb.setSpacingBefore(0f);
		tb.getDefaultCell().setPadding(0f);
		tb.setWidthPercentage(100f);

		tb.getDefaultCell().setBackgroundColor(styleObj.orangeBGColor);
		// add names
		PdfPCell cell = new PdfPCell();
		cell.setBorder(0);
		cell.setPadding(10);
		cell.setFixedHeight(200);
		cell.setBackgroundColor(styleObj.orangeBGColor);
		Paragraph namePar = new Paragraph();
		namePar.add(new Paragraph(commonName, styleObj.bigSpeciesHeadingFont));
		namePar.add(new Paragraph(scientificName, styleObj.smallSpeciesHeadingFont));
		namePar.setSpacingAfter(20);
		cell.addElement(namePar);

		// add descriptions

		try {
			String descriptionHtml = (String) profileRow.get("WebMainDescription");

			if (descriptionHtml != null) {

				for (Element e : parseHTML(descriptionHtml, headerCSS)) {
					cell.addElement(e);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}

		Paragraph spacePar = new Paragraph("");

		spacePar.setSpacingAfter(20);
		cell.addElement(spacePar);

		try {
			String details = (String) webContent.get("details");
			if (details != null) {
				
				for (Element e : parseHTML(details, smallHeaderCSS)) {

					cell.addElement(e);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		tb.addCell(cell);
		// add map
		Image mapImg;
		try {
			mapImg = Image.getInstance(map);
//			mapImg.scaleAbsoluteWidth(150);
			mapImg.setScaleToFitHeight(true);
			PdfPCell mapCell = new PdfPCell(mapImg, true);
			mapCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			mapCell.setBackgroundColor(styleObj.mapBGColor);
			mapCell.setBorder(0);
			mapCell.setPadding(0);
			// mapCell.setFixedHeight(250); // manually set map height as 620 to
			// match profile height
			tb.addCell(mapCell);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			tb.addCell(new PdfPCell());
		}

		try {
			
			document.add(tb);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	}

	/**
	 * 
	 * @param doc
	 * @param headerLabel
	 * @param contentHtml
	 */
	void addHeaderParagrph(Document doc, String headerLabel, String contentHtml) {

		addHeaderParagrph(doc, headerLabel, contentHtml, styleObj.headingFont, true);
	}

	/**
	 * add header with html content below
	 * 
	 * @param doc
	 * @param headerLabel
	 * @param contentHtml
	 * @param font
	 */
	void addHeaderParagrph(Document doc, String headerLabel, String contentHtml, Font font, boolean mainHeader) {

		Paragraph header = new Paragraph();
		header.add(new Chunk(headerLabel, font));
		header.setSpacingBefore(mainHeader?10:3);
		header.setSpacingAfter(mainHeader?5:3);
//		header.setSpacingAfter(5);

		try {
			doc.add(header);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			log.error(e1.getMessage());
		}

		Paragraph paragraph = new Paragraph();

//		paragraph.setSpacingAfter(3);

		try {
			if (contentHtml != null) { // only process when it is not null

				for (Element e : parseHTML(contentHtml, CSS)) {
					paragraph.add(e);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}

		try {
			doc.add(paragraph);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			log.error(e1.getMessage());
		}
	}

	/**
	 * 
	 * @param doc
	 * @param headerLabel
	 * @param headerIcon
	 * @param graphHeaderText
	 * @param graphPath
	 * @param contentHtml
	 * @param font
	 */

	// styleObj.forestIcon, styleObj.prairieIcon

	void addGraphParagraph(Document doc, String headerLabel, Boolean headerIcon, Boolean graphHeaderIcon[],
			String[] graphHeaderText, String[] graphPath, String[] contentHtml, Map<String, Object> readMore,
			int imageHeight) {

	

		float percentIcon = 3;
		float[] innerWidthCells = { percentIcon, 100 - percentIcon };
		// if header icon provided, display icon and text in a table
		if (headerIcon != null) { // header line

			PdfPTable innerTable = new PdfPTable(2);

			try {
				innerTable.setWidths(innerWidthCells);
			} catch (Exception e) {

				log.error(e.getMessage());
			}
			innerTable.setWidthPercentage(100f);

			innerTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			innerTable.setPaddingTop(0);
			innerTable.getDefaultCell().setBorder(0);
			innerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
			
			Image icon;
			try {
				icon = Image.getInstance(headerIcon ? styleObj.forestIcon : styleObj.prairieIcon);
				// System.out.println("HeaderIcon=" + headerIcon + " icon=" +
				// (headerIcon ? styleObj.forestIcon : styleObj.prairieIcon));

				// icon.scaleAbsolute(40, 40);
				icon.scaleAbsolute(10, 20); // .setScaleToFitHeight(true);
				PdfPCell iconCell = new PdfPCell(icon, false);
				iconCell.setPadding(0);
				iconCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
				iconCell.setBorder(0);
				innerTable.addCell(iconCell);
			} catch (Exception e) {

				log.error(e.getMessage());
			}
			PdfPCell cell = new PdfPCell();
			Paragraph headerParagraph = new Paragraph(10);
			headerParagraph.setSpacingAfter(10);
			headerParagraph.add(new Chunk(headerLabel,
					headerIcon ? styleObj.greenGraphHeadingFont : styleObj.orangeGreenHeadingFont));
			cell.addElement(headerParagraph);
			cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
			cell.setBorder(0);
			cell.setPadding(0);
			cell.setPaddingLeft(10);
			innerTable.addCell(cell);
			
			try {
				// innerTable.addCell(bigCell);
				innerTable.setSpacingBefore(10);
				doc.add(innerTable);
			} catch (Exception e) {

				log.error(e.getMessage());
			}
		} else { /* display a text line */
		
			if (headerLabel != null) {
				Paragraph header = new Paragraph(10);
				header.setSpacingAfter(5);
				header.add(new Chunk(headerLabel, styleObj.greenGraphHeadingFont));
				
				try {
					header.setSpacingBefore(10);
					doc.add(header);

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					log.error(e1.getMessage());
				}
			}

			
		}

		this.addAllReadMoreParagraph(doc, readMore);

		/* first create tables */

		if (graphPath != null || graphPath.length > 0) {
			int nColumn = graphPath.length;

			PdfPTable table = new PdfPTable(nColumn);
			table.setKeepTogether(true);
			float[] widthCells = new float[nColumn];
			for (int i = 0; i < nColumn; i++) {
				widthCells[i] = 100 / nColumn;
			}
			try {
				table.setWidths(widthCells);
			} catch (Exception e) {

				log.error(e.getMessage());
			}
			table.setWidthPercentage(100);
			table.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(0);
			table.getDefaultCell().setPadding(0);
			table.setPaddingTop(0);
			
			/* add content to table cells */

			// first row, all headers
			if (graphHeaderIcon != null && graphHeaderIcon.length == nColumn) { // if
																				// there
																				// are
																				// indicators
																				// showing
																				// forest
																				// or
																				// prairie

				innerWidthCells[0] = percentIcon * nColumn;
				innerWidthCells[1] = 100 - percentIcon * nColumn;

				for (int i = 0; i < nColumn; i++) {
					PdfPCell bigCell = new PdfPCell();
					PdfPTable innerTable = new PdfPTable(2);

					try {
						innerTable.setWidths(innerWidthCells);
					} catch (Exception e) {

						log.error(e.getMessage());
					}
					innerTable.setWidthPercentage(100f);

					Image icon;
					try {
						icon = Image.getInstance(graphHeaderIcon[i] ? styleObj.forestIcon : styleObj.prairieIcon);

						// icon.setScaleToFitHeight(true);
						icon.scaleAbsolute(10, 20);
						PdfPCell iconCell = new PdfPCell(icon, false); // dont use auto fit 
						iconCell.setBorder(0);
						iconCell.setPaddingTop(0);

						iconCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
						innerTable.addCell(iconCell);
					} catch (Exception e) {

						log.error(e.getMessage());
					}

					/* header line */

					PdfPCell cell = new PdfPCell();
					
					Paragraph headerParagraph = new Paragraph(20);
					
					headerParagraph.setSpacingBefore(0);
					headerParagraph.add(new Chunk(graphHeaderText[i],
							graphHeaderIcon[i]? styleObj.greenGraphHeadingFont : styleObj.orangeGreenHeadingFont));
					
					cell.addElement(headerParagraph);
					cell.setBorder(0);
					cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
					cell.setPadding(0);
					cell.setPaddingLeft(10);

					innerTable.addCell(cell);
					innerTable.setPaddingTop(0);
					innerTable.getDefaultCell().setBorder(0);
					innerTable.getDefaultCell().setPadding(0);
					bigCell.setPaddingTop(0);

					bigCell.addElement(innerTable);
					bigCell.setBorder(0);
					table.addCell(bigCell);
			
				}
			} else if (graphHeaderText != null) {
				for (int i = 0; i < nColumn && i < graphHeaderText.length; i++) {
					PdfPCell cell = new PdfPCell();
					cell.addElement(new Chunk(graphHeaderText[i], styleObj.greenGraphHeadingFont));
					cell.setBorder(0);
					table.addCell(cell);
				}
			}

			// second row, all images

			for (int i = 0; i < nColumn; i++) {

				Image graph;
				try {
					if (graphPath[i] != null && graphPath[i].length() > 4) {
						if (graphPath[i].startsWith("http:") || graphPath[i].startsWith("https:")) {
							graph = Image.getInstance(graphPath[i]);
						} else {// add path if located on hard drive
							graph = Image.getInstance(this.rootPath + graphPath[i]);
						}

						graph.setScaleToFitHeight(true);
						PdfPCell graphCell = new PdfPCell(graph, true);
						graphCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						if (imageHeight > 0) {
							graphCell.setFixedHeight(imageHeight);
						}
						graphCell.setPadding(10);
						graphCell.setBorder(0);
						table.addCell(graphCell);
					}
				} catch (Exception e) {

					log.error(e.getMessage());
					PdfPCell graphCell = new PdfPCell();
					graphCell.addElement(new Chunk(""));
					graphCell.setBorder(0);
					table.addCell(graphCell);
				}

			}

			// thrid row, all description
			if (contentHtml != null) {
				if (contentHtml.length == nColumn) { // if there are one
														// description html for
														// each cells, add them
														// in

					for (int i = 0; i < nColumn; i++) {
						PdfPCell cell = new PdfPCell();

						try {
							if (contentHtml != null && contentHtml[i] != null) {

								for (Element e : parseHTML(contentHtml[i], CSS)) {
									cell.addElement(e);
								}
							}
						} catch (Exception e) {

							log.error(e.getMessage());
							cell.addElement(new Chunk(""));
						}

						cell.setBorder(0);
						table.addCell(cell);
					}
				} else if (contentHtml.length == 1) { // if there are only one
														// description together,
														// add them in as one
														// row
					PdfPCell cell = new PdfPCell();

					try {
						if (contentHtml != null && contentHtml[0] != null) {

							for (Element e : parseHTML(contentHtml[0], CSS)) {
								cell.addElement(e);
							}
						}
					} catch (Exception e) {

						log.error(e.getMessage());
					}

					cell.setBorder(0);
					cell.setColspan(nColumn);
					cell.setPaddingTop(0);
					table.addCell(cell);

				} else {
					log.error("the size of html doesn't match the number of graphs");
				}
			}

			// add table in
			try {
//				table.setSpacingAfter(20);
				doc.add(table);
			} catch (Exception e1) {

				log.error(e1.getMessage());
			}
		}

	}

	/**
	 * 
	 * @return
	 */
	String getRefMapText() {
		String text = "<p>The reference condition shows the predicted relative abundance of the"
				+ CommonUtil.nvl(commonName, scientificName)
				+ " after all human footprint had been backfilled based on native vegetation in the surrounding area.</p>";
		return text;
	}

	/**
	 * 
	 * @return
	 */
	String getCurrentMapText() {
		String text = "<p>The current condition is the predicted relative abundance of the "
				+ CommonUtil.nvl(commonName, scientificName)
				+ " taking current human footprint (circa 2012) into account. </p>";

		return text;
	}

	/**
	 * 
	 * @param doc
	 * @param readMore
	 */
	void addAllReadMoreParagraph(Document doc, Map<String, Object> readMore) {
		if (readMore == null) {
			return;
		}

		for (Map.Entry<String, Object> entry : CommonUtil.emptyIfNull(readMore.entrySet())) {
			String key = entry.getKey();
			String value = entry.getValue().toString();

			addHeaderParagrph(doc, key.equals("Read More") ? "" : key, value, styleObj.methodHeadingFont, false); // don't
																											// show
																											// read
																											// more
		}
	}

	ElementList parseHTML(String html, String css) {

		String output = "";
		try {
			// System.out.println("html\n" + html);
			/* From UTF-8 to ISO-8859-1 */

			// output = new String(html.getBytes("UTF-8"), "ISO-8859-1");
			// System.out.println("output UTF to ISO \n" + output);
			//
			// output = new String(html.getBytes("CP-1252 "), "UTF-8");
			// System.out.println("output - CP -1252 \n" + output);

			/* From ISO-8859-1 to UTF-8 */
			output = new String(html.trim().getBytes("ISO-8859-1"), "UTF-8");
			// System.out.println("output\n" + output);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			output = html;
		}

		try {
			return XMLWorkerHelper.parseToElementList(output, css);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ElementList();
	}

}
