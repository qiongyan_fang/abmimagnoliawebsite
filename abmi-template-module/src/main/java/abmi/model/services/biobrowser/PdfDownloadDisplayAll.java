package abmi.model.services.biobrowser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.text.pdf.PdfWriter;

import abmi.api.services.speciesprofiles.SpeciesProfileDAO;

import abmi.model.entity.biobrowser.WebSpeciesViewV;

import abmi.model.util.CommonUtil;
import abmi.model.util.pdf.SpeciesProfilePDFHeaderFooter;

/**
 * this class read data from repository and database, and some are from website.
 * and combined them to create a pdf species profile download. it is in use.
 * 
 * @author Qiongyan
 *
 */
public class PdfDownloadDisplayAll {
	private static final Logger log = LoggerFactory.getLogger(PdfDownloadDisplayAll.class);
	String rootPath;

	PdfDownloadStyles styleObj;
	String[] gExternalSppFolder = { "birds", "vplants", "mites", "lichens", "mosses", "", "mammals" }; // species

	public PdfDownloadDisplayAll() {
		styleObj = new PdfDownloadStyles();
	}

	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	String description = "description";

	int groupId, tsn;
	String commonName, scientificName;

	public String createPDFBackEnd(WebSpeciesViewV sppRow, List<Map<String, Object>> webContent) {
		scientificName = sppRow.getWsbScientificName();
		groupId = sppRow.getSppGroup().getSgId();
		commonName = sppRow.getWsbCommonName();
		tsn = sppRow.getWsbTsn();

		/*--------------------------------------------------
		 * if the pdf already exists, return it.
		 *-------------------------------------------------*/
		File outputPdf = new File(this.rootPath + "/report/" + sppRow.getSppGroup().getSgGroupName().replaceAll("_", "_") + "\\" + scientificName.replaceAll(" ", "_") + "_tmp.pdf");
		File outputPdfDest = new File(this.rootPath + "/report/" + sppRow.getSppGroup().getSgGroupName().replaceAll("_", "_") + "\\" + scientificName.replaceAll(" ", "_") + ".pdf");

		if (outputPdfDest.exists()) {
			return outputPdfDest.getName();
		}
		// create folders 
		outputPdf.getParentFile().mkdirs();
		
		/*--------------------------------------------------
		 * otherwise generate on the fly
		 *-------------------------------------------------*/
		Document document = null;
		PdfWriter writer = null;
		try {

			Map<String, Object> profiles = SpeciesProfileDAO.getSingleSpeciesProfile(false, scientificName, "");

			/*--------------------------------------------------
			 * Define document pdf margin size
			 *--------------------------------------------------*/
			document = new Document();
			document.setPageSize(PageSize.LETTER); // 612, 792
			document.setMargins(36, 36, 40, 72); // left, right,
													// top, bottom
			// document.setMarginMirroring(false);
			document.addAuthor(styleObj.author);

			/*--------------------------------------------------
			 * step 2: we create a writer that listens to the document to
			 * 1. create header and footer
			 * 2. page numbers
			 *--------------------------------------------------*/

			writer = PdfWriter.getInstance(document, new FileOutputStream(outputPdf));

			SpeciesProfilePDFHeaderFooter pageHeaderFooterEvent = new SpeciesProfilePDFHeaderFooter();
			pageHeaderFooterEvent.setStyleObj(styleObj);
			pageHeaderFooterEvent.setHeaderBannerText(styleObj.headerText);
			pageHeaderFooterEvent.setHeaderImage(styleObj.abmiHeaderImagePath);
			pageHeaderFooterEvent.setHeaderText(styleObj.headerText + ":" + CommonUtil.nvl(commonName, scientificName));

			pageHeaderFooterEvent.setFooterText(styleObj.footerText);
			pageHeaderFooterEvent.setFootImage(styleObj.abmiHeaderImagePath);

			writer.setPageEvent(pageHeaderFooterEvent);
			writer.setViewerPreferences(PdfWriter.PageModeUseOutlines);

			/*--------------------------------------------------
			 * open document
			 *--------------------------------------------------*/
			document.open();

			/*--------------------------------------------------
			 * add first page blue banners  and profile banner( tables with species profile images, distribution maps, and 
			 * species information (names, description and guilds)
			 *--------------------------------------------------*/
			Map<String, Object> introduction = webContent.get(0); // introduction
			addBanner(document);
			addProfileBanner(document, profiles, introduction);

			/*--------------------------------------------------
			 * 	 main content introduction (content from html page) *
			 *--------------------------------------------------*/

			String introductionHtml = "";

			try {
				introductionHtml = (String) introduction.get(description);
			} catch (Exception e) {

			}

			addHeaderParagrph(document, styleObj.mainheader[0], introductionHtml);

			addAllReadMoreParagraph(document, (Map<String, Object>) introduction.get("readMore"));

			/*--------------------------------------------------
			 *  forest header: Habitat & Human Footprint Associations
			 *--------------------------------------------------*/

			Map<String, Object> forestSect = webContent.get(1); //

			addHeaderParagrph(document, styleObj.mainheader[1], (String) profiles.get("WebHabitat"));

			addAllReadMoreParagraph(document, (Map<String, Object>) forestSect.get("readMore"));

			/*--------------------------------------------
			 * first graph, big one
			 --------------------------------------------*/

			try {
				Map<String, Object> bigGraphSect = webContent.get(2); // tree
																		// graph

				// String graphHeaderIcon[] = {};
				String graphPath[] = { GraphService.getGraphPath(sppRow, GraphService.SPECIES_ABUNDANCE_GRAPH, false, false) };
				// String graphHeaderText[] = {
				// styleObj.speciesForestGraphHeader };

				String contentHtml[] = { (String) profiles.get("WebForestedRegionGraphText") };

				addGraphParagrph(document, styleObj.speciesForestGraphHeader, true, null, null, graphPath, contentHtml,
						(Map<String, Object>) bigGraphSect.get("readMore"));
			} catch (Exception e) {
				log.error(e.getMessage());
			}
			/*
			 * -------------------------------------------- second graph, side
			 * by side tree and non-tree graph and their descriptions
			 * --------------------------------------------
			 */

			try {
				Map<String, Object> treeGraphSect = webContent.get(3); // tree
																		// graph

				// graph
				Boolean graphHeaderIcon[] = null;

				String graphPath[] = { GraphService.getGraphPath(sppRow, GraphService.NON_TREED_SITE_GRAPH, null, false),
						GraphService.getGraphPath(sppRow, GraphService.NON_TREED_SITE_GRAPH, null, true), };
				String contentHtml[] = {
						(String) profiles.get("WebNonTreeGraphText") + (String) profiles.get("WebTreeGraphText") };

				String graphHeaderText[] = { styleObj.nonTreedGraphHeader, styleObj.treedGraphHeader };

				addGraphParagrph(document, styleObj.speciesPrairieGraphHeader, true, graphHeaderIcon, graphHeaderText,
						graphPath, contentHtml, (Map<String, Object>) treeGraphSect.get("readMore"));

			} catch (Exception e) {
				log.error(e.getMessage());
			}

			/*--------------------------------------------
			 *  third graph, side by side linear human footprint graphs
			 --------------------------------------------*/
			LineSeparator line = new LineSeparator();
			line.setOffset(-10);

			document.add(line);

			try {
				Map<String, Object> linearGraphSect = webContent.get(4); //

				Boolean graphHeaderIcon[] = { true, false };

				String graphPath[] = {
						GraphService.getGraphPath(sppRow, GraphService.LINEAR_FOOTPRINT_GRAPH, true, null),
						GraphService.getGraphPath(sppRow, GraphService.LINEAR_FOOTPRINT_GRAPH, false, null), };
				String contentHtml[] = { (String) profiles.get("WebFLinearHFGraphText")
						+ (String) profiles.get("WebPLinearHFGraphText") };

				String graphHeaderText[] = { styleObj.linearFPForestGraphHeader, styleObj.linearFPPrairieGraphHeader };

				addGraphParagrph(document, styleObj.linearFPHeader, null, graphHeaderIcon, graphHeaderText, graphPath,
						contentHtml, (Map<String, Object>) linearGraphSect.get("readMore"));

			} catch (Exception e) {
				log.error(e.getMessage());
			}

			/*----------------------------------------------------------------------------------------
			 *  forth graph, side by side, sector effect graphs
			 ----------------------------------------------------------------------------------------*/

			addHeaderParagrph(document, styleObj.mainheader[2], (String) profiles.get("WebHFEffect"));

			try {
				Map<String, Object> hfImpactSect = webContent.get(5); // tree

				Boolean graphHeaderIcon[] = { true, false };
				String graphPath[] = { GraphService.getGraphPath(sppRow, GraphService.SECTOR_EFFECTS_GRAPH, true, null),
						GraphService.getGraphPath(sppRow, GraphService.SECTOR_EFFECTS_GRAPH, false, null), };
				String contentHtml[] = { (String) profiles.get("WebHFForestRegionGraph"),
						(String) profiles.get("WebPrarieRegionGraphText") };

				String graphHeaderText[] = { styleObj.hfEffectForestGraphHeader, styleObj.hfEffectPrairieGraphHeader };

				addGraphParagrph(document, null, null, graphHeaderIcon, graphHeaderText, graphPath, contentHtml,
						(Map<String, Object>) hfImpactSect.get("readMore"));

			} catch (Exception e) {
				log.error(e.getMessage());
			}

			/*----------------------------------------------------------------------------------------
			 *  description
			 *  3 maps current reference and difference			 * 
			 ----------------------------------------------------------------------------------------*/

			addHeaderParagrph(document, styleObj.mainheader[3], (String) profiles.get("WebRange"));

			try {
				Map<String, Object> mapSect = webContent.get(6); // tree

				Map<String, Object> graph = (Map<String, Object>) mapSect.get("graph");
				String externalMapUrl = "http://species.abmi.ca/contents/species/" + gExternalSppFolder[groupId - 1]
						+ "/";
				String sppName = "";
				if (groupId == 1 || groupId == 7) {
					sppName = commonName.replaceAll(" ", "");
				} else {
					sppName = scientificName.replaceAll(" ", "");
				}

				String map1 = externalMapUrl + "/map-rf/" + sppName + ".png";
				String map2 = externalMapUrl + "/map-cr/" + sppName + ".png";
				String map3 = externalMapUrl + "/map-df/" + sppName + ".png";

				String graphPath[] = { map1, map2, map3 };

				String contentHtml[] = { this.getRefMapText(), this.getCurrentMapText(),
						(String) profiles.get("WebDifferenceMapText") };

				String graphHeaderText[] = styleObj.mapHeader;
				addGraphParagrph(document, null, null, null, graphHeaderText, graphPath, contentHtml,
						(Map<String, Object>) mapSect.get("readMore"));

			} catch (Exception e) {
				log.error(e.getMessage());
			}

			/*----------------------------------------------------------------------------------------
			 * Reference sections
			 ----------------------------------------------------------------------------------------*/
			addHeaderParagrph(document, styleObj.mainheader[4], "");

			addHeaderParagrph(document, "Results Coming Soon", "", styleObj.textFont);

			addHeaderParagrph(document, styleObj.mainheader[5], "");

			addHeaderParagrph(document, "Not Available", "", styleObj.textFont);
			addHeaderParagrph(document, styleObj.mainheader[6], "");

			// reference, data source, citation etc. // graph

			addHeaderParagrph(document, styleObj.mainheader[5], "");

			addHeaderParagrph(document, styleObj.referenceSubHeader, (String) profiles.get("WebReference"),
					styleObj.methodHeadingFont);

			Map<String, Object> refSect = (Map<String, Object>) webContent.get(7).get("section"); // tree

			addHeaderParagrph(document, styleObj.dataSourceSubHeader, (String) refSect.get("data-source"),
					styleObj.methodHeadingFont);

			addHeaderParagrph(document, styleObj.creditSubHeader, (String) refSect.get("citation"),
					styleObj.methodHeadingFont);

			document.close();
			writer.close();

			this.addFooter(outputPdf.getAbsolutePath(), outputPdfDest.getAbsolutePath());

			return outputPdfDest.getName();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			if (document != null)
				document.close();

			if (writer != null)
				writer.close();

		}
		return null;
	}

	/**
	 * first page's blue banner
	 * 
	 * @param document
	 */
	void addBanner(Document document) {
		try {
			PdfPTable headerTable = new PdfPTable(2);
			headerTable.getDefaultCell().setBorder(0);
			headerTable.setSpacingAfter(0f);
			headerTable.setSpacingBefore(0f);
			headerTable.getDefaultCell().setPadding(0f);

			headerTable.getDefaultCell().setBackgroundColor(styleObj.blueBGColor);

			float widthCells[] = { 50, 500 };

			headerTable.setWidths(widthCells);
			headerTable.setWidthPercentage(100f);

			Image img = Image.getInstance(styleObj.abmiHeaderImagePath);
			img.scalePercent(50);
			PdfPCell cell = new PdfPCell(img, true);
			cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
			cell.setBackgroundColor(styleObj.greenGraphHeadingColor);
			cell.setBorder(0);

			headerTable.addCell(cell);

			Phrase textHeader = new Phrase(styleObj.headerText, styleObj.headingWhiteFont);
			PdfPCell cell2 = new PdfPCell(textHeader);

			cell2.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			cell2.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
			cell2.setPaddingTop(10);
			cell2.setBackgroundColor(styleObj.blueBGColor);
			cell2.setBorder(0);

			headerTable.addCell(cell2);

			document.add(headerTable);
		} catch (Exception e) {

		}
	}

	/**
	 * 
	 * @param src
	 * @param dest
	 * @throws IOException
	 * @throws DocumentException
	 */
	void addFooter(String src, String dest) {
		PdfReader reader = null;
		PdfStamper stamper = null;
		try {
			reader = new PdfReader(src);

			int n = reader.getNumberOfPages();
			stamper = new PdfStamper(reader, new FileOutputStream(dest));
			PdfContentByte pagecontent;
			for (int i = 0; i < n;) {
				pagecontent = stamper.getOverContent(++i);
				if (i < n) {
					float widthCells[] = { 50, 500, 50 };
					PdfPTable table = new PdfPTable(3);
					table.setTotalWidth(550);
					table.setWidths(widthCells);

					PdfPCell cell = new PdfPCell(new Phrase("Page " + n, styleObj.headerFooterFont));

					cell.setBorder(0);
					cell.setPaddingLeft(20);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);

					Image img = Image.getInstance(styleObj.abmiHeaderImagePath);
					img.scaleToFit(20, 20);
					cell = new PdfPCell(img);

					cell.setBorder(0);
					cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);

					cell = new PdfPCell(new Phrase("www.abmi.ca", styleObj.headerFooterFont));
					cell.setBackgroundColor(styleObj.blueBGColor);
					cell.setBorder(0);
					cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);
					// If you have more than one cell in a row, you need to set
					// the background for all cells. Note that I'm defining a
					// total width for the table (523 is the width of the page
					// minus the margins). The total width is needed because
					// we'll add the table using writeSelectedRows():

					table.writeSelectedRows(0, -1, 36, 34, pagecontent);
				} else { // last page add a table
					PdfPTable table = new PdfPTable(3);
					table.setTotalWidth(523);
					Image img = Image.getInstance(styleObj.abmiHeaderImagePath);
					img.scaleToFit(20, 20);
					PdfPCell cell = new PdfPCell(img);
					cell.setBackgroundColor(styleObj.blueBGColor);
					cell.setBorder(0);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);

					cell = new PdfPCell(new Phrase(styleObj.headerText, styleObj.headerFooterWhiteFont));
					cell.setBackgroundColor(styleObj.blueBGColor);
					cell.setBorder(0);
					cell.setPaddingLeft(5);
					cell.setPaddingTop(5);
					cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);

					cell = new PdfPCell(new Phrase("www.abmi.ca", styleObj.headerFooterWhiteFont));
					cell.setBackgroundColor(styleObj.blueBGColor);
					cell.setBorder(0);
					cell.setPaddingTop(5);
					cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
					table.addCell(cell);
					// If you have more than one cell in a row, you need to set
					// the background for all cells. Note that I'm defining a
					// total width for the table (523 is the width of the page
					// minus the margins). The total width is needed because
					// we'll add the table using writeSelectedRows():

					table.writeSelectedRows(0, -1, 36, 64, pagecontent);
				}

			}

			stamper.close();
			reader.close();

			File srcFile = new File(src);
			srcFile.delete();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (stamper!= null){
				try {
					stamper.close();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
			}
			
			if (reader != null){
				reader.close();
			}
		}

	}

	/**
	 * species profile banner, similar as the web page renderes
	 * 
	 * @param document
	 * @param profileRow
	 * @param webContent
	 */
	void addProfileBanner(Document document, Map<String, Object> profileRow, Map<String, Object> webContent) {
		/* banner graph / text / distribution maps */
		String image = null;

		for (Map<String, String> singleImg : CommonUtil
				.emptyIfNull((List<Map<String, String>>) profileRow.get("WebPhotos"))) {
			if ("mainBanner".equals(singleImg.get("imageType"))) {
				image = (String) singleImg.get("imageLink");
				break;
			}
		}

		String map = this.rootPath + "\\distributionMaps\\" + groupId + "_" + tsn + ".png";

		String CSS = "p { color:#ffffff; }  " + " .orange {font-size:17px; color:#751e11;}";
		PdfPTable tb;

		if (image == null) {
			tb = new PdfPTable(2);

			float widthCells1[] = { 400, 200 };
			try {
				tb.setWidths(widthCells1);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				log.error(e1.getMessage());
			}
		} else {
			tb = new PdfPTable(3);
			Image img;
			try {
				if (image.indexOf("/dam/jcr:") != -1) { // read byte[] from dam
					img = Image.getInstance(
							SpeciesProfileDAO.getSingleSpeciesProfileImageByte(scientificName, "mainBanner"));
				} else {
					img = Image.getInstance(image);
				}
				PdfPCell imgCell = new PdfPCell(img, true);
				imgCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
				imgCell.setBackgroundColor(styleObj.orangeBGColor);
				tb.addCell(imgCell);
				float widthCells1[] = { 200, 400, 200 };
				try {
					tb.setWidths(widthCells1);
				} catch (Exception e1) {

					log.error(e1.getMessage());
				}

			} catch (Exception e1) {

				log.error(e1.getMessage());
				tb = new PdfPTable(2);
				float widthCells1[] = { 400, 200 };
				try {
					tb.setWidths(widthCells1);
				} catch (Exception ex) {

					log.error(e1.getMessage());
				}
			}

		}
		tb.getDefaultCell().setBorder(0);
		tb.setSpacingAfter(0f);
		tb.setSpacingBefore(0f);
		tb.getDefaultCell().setPadding(0f);
		tb.setWidthPercentage(100f);

		tb.getDefaultCell().setBackgroundColor(styleObj.orangeBGColor);
		// add names
		PdfPCell cell = new PdfPCell();
		cell.setBorder(0);
		cell.setPadding(10);
		cell.setBackgroundColor(styleObj.orangeBGColor);
		Paragraph namePar = new Paragraph();
		namePar.add(new Paragraph(commonName, styleObj.bigSpeciesHeadingFont));
		namePar.add(new Paragraph(scientificName, styleObj.smallSpeciesHeadingFont));
		namePar.setSpacingAfter(20);
		cell.addElement(namePar);

		// add descriptions

		try {
			String descriptionHtml = (String) profileRow.get("WebMainDescription");
			if (descriptionHtml != null) {
				for (Element e : XMLWorkerHelper.parseToElementList(descriptionHtml, CSS)) {
					cell.addElement(e);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}

		Paragraph spacePar = new Paragraph("");
		
		spacePar.setSpacingAfter(20);
		cell.addElement(spacePar);

		
		try {
			String details = (String) webContent.get("details");
			if (details != null) {
				for (Element e : XMLWorkerHelper.parseToElementList(details, CSS)) {

					cell.addElement(e);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		tb.addCell(cell);
		// add map
		Image mapImg;
		try {
			mapImg = Image.getInstance(map);
			PdfPCell mapCell = new PdfPCell(mapImg, true);
			mapCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			mapCell.setBackgroundColor(styleObj.orangeBGColor);
			mapCell.setBorder(0);
			tb.addCell(mapCell);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			tb.addCell(new PdfPCell());
		}

		try {
			document.add(tb);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	}

	/**
	 * 
	 * @param doc
	 * @param headerLabel
	 * @param contentHtml
	 */
	void addHeaderParagrph(Document doc, String headerLabel, String contentHtml) {

		addHeaderParagrph(doc, headerLabel, contentHtml, styleObj.headingFont);
	}

	/**
	 * add header with html content below
	 * 
	 * @param doc
	 * @param headerLabel
	 * @param contentHtml
	 * @param font
	 */
	void addHeaderParagrph(Document doc, String headerLabel, String contentHtml, Font font) {

		Paragraph header = new Paragraph();
		header.add(new Chunk(headerLabel, font));
		header.setSpacingAfter(10);
		header.setSpacingBefore(15);

		try {
			doc.add(header);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			log.error(e1.getMessage());
		}

		Paragraph paragraph = new Paragraph();

		paragraph.setSpacingAfter(10);
		String CSS = "";
		try {
			if (contentHtml != null) { // only process when it is not null

				for (Element e : XMLWorkerHelper.parseToElementList(contentHtml, CSS)) {
					paragraph.add(e);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}

		try {
			doc.add(paragraph);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			log.error(e1.getMessage());
		}
	}

	/**
	 * 
	 * @param doc
	 * @param headerLabel
	 * @param headerIcon
	 * @param graphHeaderText
	 * @param graphPath
	 * @param contentHtml
	 * @param font
	 */

	// styleObj.forestIcon, styleObj.prairieIcon

	void addGraphParagrph(Document doc, String headerLabel, Boolean headerIcon, Boolean graphHeaderIcon[],
			String[] graphHeaderText, String[] graphPath, String[] contentHtml, Map<String, Object> readMore) {

		Paragraph header = new Paragraph(10);
		header.setSpacingBefore(15);
		float[] innerWidthCells = { 4, 96 };
		// if header icon provided, display icon and text in a table
		if (headerIcon != null) { // header line
			PdfPCell bigCell = new PdfPCell();
			PdfPTable innerTable = new PdfPTable(2);

			try {
				innerTable.setWidths(innerWidthCells);
			} catch (Exception e) {

				log.error(e.getMessage());
			}
			innerTable.setWidthPercentage(100f);

			Image icon;
			try {
				icon = Image.getInstance(headerIcon ? styleObj.forestIcon : styleObj.prairieIcon);
				icon.setScaleToFitLineWhenOverflow(true);

				PdfPCell iconCell = new PdfPCell(icon, true);
				iconCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
				iconCell.setBorder(0);
				innerTable.addCell(iconCell);
			} catch (Exception e) {

				log.error(e.getMessage());
			}
			PdfPCell cell = new PdfPCell();
			cell.addElement(new Chunk(headerLabel,
					headerIcon ? styleObj.orangeGreenHeadingFont : styleObj.greenGraphHeadingFont));
			cell.setBorder(0);
			innerTable.addCell(cell);
			bigCell.setBorder(0);
			bigCell.addElement(innerTable);
			innerTable.getDefaultCell().setBorder(0);
			try {
				innerTable.addCell(bigCell);
				header.add(innerTable);
			} catch (Exception e) {

				log.error(e.getMessage());
			}
		} else { /* display a line */
			if (headerLabel != null) {
				header.add(new Chunk(headerLabel, styleObj.greenGraphHeadingFont));
			}
		}
		try {
			doc.add(header);

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			log.error(e1.getMessage());
		}

		this.addAllReadMoreParagraph(doc, readMore);
		String CSS = "";
		/* first create tables */

		if (graphPath != null || graphPath.length > 0) {
			int nColumn = graphPath.length;

			PdfPTable table = new PdfPTable(nColumn);
			table.setKeepTogether(true);
			float[] widthCells = new float[nColumn];
			for (int i = 0; i < nColumn; i++) {
				widthCells[i] = 100 / nColumn;
			}
			try {
				table.setWidths(widthCells);
			} catch (Exception e) {

				log.error(e.getMessage());
			}
			table.setWidthPercentage(100f);
			table.getDefaultCell().setBorder(0);

			/* add content to table cells */

			// first row, all headers
			if (graphHeaderIcon != null && graphHeaderIcon.length == nColumn) { // if
																				// there
																				// are
																				// indicators
																				// showing
																				// forest
																				// or
																				// prairie
				for (int i = 0; i < nColumn; i++) {
					PdfPCell bigCell = new PdfPCell();
					PdfPTable innerTable = new PdfPTable(2);

					try {
						innerTable.setWidths(innerWidthCells);
					} catch (Exception e) {

						log.error(e.getMessage());
					}
					innerTable.setWidthPercentage(100f);

					Image icon;
					try {
						icon = Image.getInstance(graphHeaderIcon[i] ? styleObj.forestIcon : styleObj.prairieIcon);
						icon.setScaleToFitLineWhenOverflow(true);
						PdfPCell iconCell = new PdfPCell(icon, true);
						iconCell.setBorder(0);
						iconCell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
						innerTable.addCell(iconCell);
					} catch (Exception e) {

						log.error(e.getMessage());
					}

					PdfPCell cell = new PdfPCell();
					cell.addElement(new Chunk(graphHeaderText[i],
							graphHeaderIcon[i] ? styleObj.orangeGreenHeadingFont : styleObj.greenGraphHeadingFont));
					cell.setBorder(0);
					cell.setVerticalAlignment(PdfPCell.ALIGN_CENTER);

					innerTable.addCell(cell);
					bigCell.addElement(innerTable);
					bigCell.setBorder(0);
					innerTable.getDefaultCell().setBorder(0);
					table.addCell(bigCell);
				}
			} else if (graphHeaderText != null) {
				for (int i = 0; i < nColumn && i < graphHeaderText.length; i++) {
					PdfPCell cell = new PdfPCell();
					cell.addElement(new Chunk(graphHeaderText[i], styleObj.greenGraphHeadingFont));
					cell.setBorder(0);
					table.addCell(cell);
				}
			}

			// second row, all images

			for (int i = 0; i < nColumn; i++) {

				Image graph;
				try {
					if (graphPath[i] != null && graphPath[i].length() > 4) {
						if (graphPath[i].startsWith("http:") || graphPath[i].startsWith("https:")) {
							graph = Image.getInstance(graphPath[i]);
						} else {// add path if located on hard drive
							graph = Image.getInstance(this.rootPath + graphPath[i]);
						}
						PdfPCell graphCell = new PdfPCell(graph, true);
						graphCell.setBorder(0);
						table.addCell(graphCell);
					}
				} catch (Exception e) {

					log.error(e.getMessage());
					PdfPCell graphCell = new PdfPCell();
					graphCell.addElement(new Chunk(""));
					graphCell.setBorder(0);
					table.addCell(graphCell);
				}

			}

			// thrid row, all description
			if (contentHtml != null) {
				if (contentHtml.length == nColumn) { // if there are one
														// description html for
														// each cells, add them
														// in

					for (int i = 0; i < nColumn; i++) {
						PdfPCell cell = new PdfPCell();

						try {
							if (contentHtml != null && contentHtml[i] != null) {
								for (Element e : XMLWorkerHelper.parseToElementList(contentHtml[i], CSS)) {
									cell.addElement(e);
								}
							}
						} catch (Exception e) {

							log.error(e.getMessage());
							cell.addElement(new Chunk(""));
						}

						cell.setBorder(0);
						table.addCell(cell);
					}
				} else if (contentHtml.length == 1) { // if there are only one
														// description together,
														// add them in as one
														// row
					PdfPCell cell = new PdfPCell();

					try {
						if (contentHtml != null && contentHtml[0] != null) {
							for (Element e : XMLWorkerHelper.parseToElementList(contentHtml[0], CSS)) {
								cell.addElement(e);
							}
						}
					} catch (Exception e) {

						log.error(e.getMessage());
					}

					cell.setBorder(0);
					cell.setColspan(nColumn);
					table.addCell(cell);

				} else {
					log.error("the size of html doesn't match the number of graphs");
				}
			}

			// add table in
			try {
				doc.add(table);
			} catch (Exception e1) {

				log.error(e1.getMessage());
			}
		}

	}

	String getRefMapText() {
		String text = "<p>The reference condition shows the predicted relative abundance of the"
				+ CommonUtil.nvl(commonName, scientificName)
				+ " after all human footprint had been backfilled based on native vegetation in the surrounding area.</p>";
		return text;
	}

	String getCurrentMapText() {
		String text = "<p>The current condition is the predicted relative abundance of the "
				+ CommonUtil.nvl(commonName, scientificName)
				+ " taking current human footprint (circa 2012) into account. </p>";

		return text;
	}

	/**
	 * 
	 * @param doc
	 * @param readMore
	 */
	void addAllReadMoreParagraph(Document doc, Map<String, Object> readMore) {
		if (readMore == null) {
			return;
		}

		for (Map.Entry<String, Object> entry : CommonUtil.emptyIfNull(readMore.entrySet())) {
			String key = entry.getKey();
			String value = entry.getValue().toString();

			addHeaderParagrph(doc, key, value, styleObj.methodHeadingFont);
		}
	}

}
