package abmi.model.services.biobrowser.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.swing.Icon;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.MinMaxCategoryRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleInsets;

import abmi.model.entity.biobrowser.graph.WebGraphCategoryData;
import abmi.model.entity.biobrowser.graph.WebGraphCategoryDetail;
import abmi.model.entity.biobrowser.graph.WebGraphData;
import abmi.model.entity.biobrowser.graph.WebGraphLinearData;
import abmi.model.entity.biobrowser.graph.WebGraphSery;
import abmi.model.entity.biobrowser.graph.WebGraphType;
import abmi.model.services.biobrowser.GraphService;
import abmi.model.util.CommonUtil;

import abmi.model.util.chart.ext.ABMICategoryToolTipGenerator;
import abmi.model.util.chart.ext.CategoryTitleAnnotation;
import abmi.model.util.chart.renderer.ABMIMinMaxCategoryRenderer;
import abmi.model.util.chart.renderer.ABMIMultiColorBarRenderer;

/**
 * this class is used to create few detection charts for north and south
 * province the chart itself is a simple multiple color bar with data coming
 * from database table web_graph_category_datas
 * 
 * @author Qiongyan
 *
 */
public class FewDetectionChart {

	@PersistenceContext(unitName = "data")
	EntityManager em;

	String fontName = "Open Sans";

	Paint[] colors;

	/**
	 * get data from database. calculate the maxvalue used for both treed and
	 * non-treed graphs.
	 * 
	 * @param tsn
	 * @param bTreed
	 *            default is non-tree. to get a treed value, need to read a
	 *            multiplier
	 * @return
	 */
	DefaultCategoryDataset getData(int tsn, boolean bNorth) {
		// dataset 0: min max line, 1: bar
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		// get wgt =2 data Non-Treed Sites/Treed Sites

		TypedQuery<WebGraphCategoryData> query = em.createQuery(
				"select  p from WebGraphCategoryData p where p.wgcdTsn =:tsn and  p.webGraphSery.webGraphType.wgtId = :northOrSouthId and p.wgcdMean is not null order by p.webGraphSery.wgsOrder",
				WebGraphCategoryData.class);

		query.setParameter("tsn", tsn);
		query.setParameter("northOrSouthId", bNorth ? 5 : 6); // 5 for north
																// chart and 6
																// for south
																// chart

		List<WebGraphCategoryData> results = query.getResultList();
		if (results == null)
			return null;

		colors = new Paint[results.size()];
		int i = 0;
		for (WebGraphCategoryData row : results) {

			// bar dataset
			dataset.addValue(row.getWgcdMean(), "Mean", row.getWebGraphSery().getWgsLabel());
			colors[i] = Color.decode(row.getWebGraphSery().getWgsColor());
			
//			System.out.println("color:" + colors[i]);
			i++;

		}
		if (dataset.getColumnCount() == 0 && dataset.getRowCount() == 0)
			return null;

		return dataset;

	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	/**
	 * 
	 * @param graphType
	 * @param tsn
	 * @param path
	 * @param bTreed
	 * @return
	 */
	public boolean createChart(WebGraphType graphType, int tsn, String path, Boolean bNorth) {

		// configure plot final ABMIMultiColorBarRenderer renderer = new
		// ABMIMultiColorBarRenderer(colors);
//
//		Paint[] colors = new Paint[graphType.getWebGraphSeries().size()];
//		for (int i = 0; i < colors.length; i++) { // acutally
//													// only two:
//													// one is
//													// soft
//													// liear,
//													// the other
//													// is hard
//													// linear
//			try {
//				colors[i] = Color.decode(graphType.getWebGraphSeries().get(i).getWgsColor());
//			} catch (NullPointerException e) {
//				System.out.println("null " + graphType.getWebGraphSeries().get(i).getWgsColor());
//			}
//		}

		DefaultCategoryDataset dataset = this.getData(tsn, bNorth);

		if (dataset == null) { // no data available
			return false;
		}
		JFreeChart chart = ChartFactory.createLineChart(graphType.getWgtTitle(), graphType.getWgtDomainLabel(),
				graphType.getWgtValueLabel(), dataset, PlotOrientation.VERTICAL, true, true, false);

		chart.setBackgroundPaint(new Color(255, 255, 255, 255)); // set
																	// background
																	// as
																	// transparent
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		plot.setBackgroundPaint(new Color(255, 255, 255, 255));

		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(new Color(100, 100, 100));
		plot.setOutlineVisible(false);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));

		/**
		 * custom bar render to draw different color bars.
		 */
		ABMIMultiColorBarRenderer renderer = new ABMIMultiColorBarRenderer(colors);

		renderer.setBarPainter(new StandardBarPainter());
		//
		renderer.setDrawBarOutline(false);

		renderer.setBaseItemLabelFont(new Font(fontName, Font.PLAIN, 9));
		renderer.setShadowVisible(true);
		renderer.setAutoPopulateSeriesFillPaint(false);
		if (bNorth) {
			renderer.setMaximumBarWidth(0.05); // set bar width (13 categories)
		} else {
			renderer.setMaximumBarWidth(0.10); // set bar width (6 categories)
		}
		
		plot.setRenderer(renderer);
		plot.setDataset(dataset);

		// set y axis label, and line

		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();

		yAxis.setLabelFont(new Font(fontName, Font.BOLD, 15));
		yAxis.setAxisLineStroke(new BasicStroke(1.5f));
		yAxis.setAxisLinePaint(new Color(0, 0, 0));
		yAxis.setUpperBound(1); // this works when range is auto set
		yAxis.setLowerBound(-1);
	
		yAxis.setTickUnit(new NumberTickUnit(0.2));
	
		// configure xAxis

		plot.setRangeAxis(yAxis);
		plot.setRangeZeroBaselineVisible(true);
		
		
		CategoryAxis xAxis = plot.getDomainAxis();
		// xAxis.setLabelFont(new Font(fontName, Font.BOLD, 9));
		xAxis.setAxisLinePaint(new Color(0, 0, 0));
		xAxis.setAxisLineStroke(new BasicStroke(1.5f));
	
		
		for (int i = 0; i < dataset.getColumnCount(); i++) {
			xAxis.setTickLabelPaint(dataset.getColumnKey(i), colors[i]);

		}

		xAxis.setTickLabelFont(new Font(fontName, Font.BOLD, 13));
		xAxis.setMaximumCategoryLabelWidthRatio(1.2f);
		xAxis.setMaximumCategoryLabelLines(2);
	
		plot.setDomainAxis(xAxis);
		plot.setBackgroundAlpha(0.0f);
		// hide default legend.
		chart.removeLegend();

		return GraphService.saveChartAsImage(chart, graphType, path);

	}
}
