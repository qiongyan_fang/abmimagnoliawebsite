package abmi.model.services.biobrowser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.text.pdf.PdfWriter;

import abmi.api.services.speciesprofiles.SpeciesProfileDAO;
import abmi.model.services.ProjectPropertyService;
import abmi.model.services.biobrowser.species.SpeciesProfileContent;
import abmi.model.util.CommonUtil;
import abmi.model.util.ModuleParameters;
import abmi.model.util.pdf.PDFEditor;
import abmi.model.util.pdf.SpeciesProfilePDFHeaderFooter;

/**
 * this class tries to read all content passed from website. * I think it has
 * too much traffic passed through internet. and the makes the post too long.
 * 
 * not to be used.
 * 
 * @author Qiongyan
 *
 */
public class PdfDownloadJSON {
	private static final Logger log = LoggerFactory.getLogger(PdfDownloadJSON.class);
	String rootPath;
	String prefix = "FileDownloadServlet?dir=WEB_GRAPH&filename=";

	PdfDownloadStyles styleObj;

	public PdfDownloadJSON() {

		styleObj = new PdfDownloadStyles();

	}

	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	String description = "description";

	public String createPDF(List<Map<String, Object>> content) {
		Document document = null;
		PdfWriter writer = null;
		try {

			Map<String, Object> banner = this.getContentRow(content, styleObj.keywords[0]); // banner
			Map<String, Object> section = (Map<String, Object>) banner.get("section");
			String commonName = (String) section.get("commonName");
			String scientificName = (String) section.get("scientificName");
			String tsn = (String) section.get("tsn");

			document = new Document();
			document.setPageSize(PageSize.LETTER); // 612, 792
			document.setMargins(36, 36, 40, 72); // left, right,
													// top, bottom
			// document.setMarginMirroring(false);
			document.addAuthor(styleObj.author);

			File outputPdf = new File(this.rootPath + "/" + scientificName + ".pdf");

			// step 2: we create a writer that listens to the
			// document
			writer = PdfWriter.getInstance(document, new FileOutputStream(outputPdf));
			// step 3: we open the document
			SpeciesProfilePDFHeaderFooter pageHeaderFooterEvent = new SpeciesProfilePDFHeaderFooter();

			pageHeaderFooterEvent.setHeaderBannerText(styleObj.headerText);
			pageHeaderFooterEvent.setHeaderImage(styleObj.abmiHeaderImagePath);
			pageHeaderFooterEvent.setHeaderText(styleObj.headerText + ":" + CommonUtil.nvl(commonName, scientificName));

			pageHeaderFooterEvent.setFooterText(styleObj.footerText);
			pageHeaderFooterEvent.setFootImage(styleObj.abmiHeaderImagePath);

			writer.setPageEvent(pageHeaderFooterEvent);
			writer.setViewerPreferences(PdfWriter.PageModeUseOutlines);

			// open the document
			document.open();
			/**
			 * add header
			 */

			addBanner(document);
			addProfileBanner(document, banner);

			/** main content **/

			/* content */
			Map<String, Object> introduction = this.getContentRow(content, styleObj.keywords[1]); // banner
			section = (Map<String, Object>) introduction.get("section");

			String introductionHtml = (String) section.get(description);

			addHeaderParagrph(document, styleObj.mainheader[0], introductionHtml);

			addAllReadMoreParagraph(document, (Map<String, Object>) introduction.get("readMore"));

			/**
			 * forest header
			 */
			Map<String, Object> forestSect = this.getContentRow(content, styleObj.keywords[2]); // banner

			section = (Map<String, Object>) forestSect.get("section");

			addHeaderParagrph(document, styleObj.mainheader[1], (String) section.get(description));

			addAllReadMoreParagraph(document, (Map<String, Object>) forestSect.get("readMore"));

			/**
			 * first graph, big one
			 */

			try {
				Map<String, Object> graph = (Map<String, Object>) forestSect.get("graph");
				String graphHeaderIcon[] = {};
				String graphPath[] = { (String) graph.get("graph1") };
				String graphHeaderText[] = {};

				String contentHtml[] = { (String) graph.get("description1") };

				addGraphParagrph(document, styleObj.speciesForestGraphHeader, styleObj.forestIcon, null, null,
						graphPath, contentHtml);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// second graph

			try {
				Map<String, Object> graphSect = this.getContentRow(content, styleObj.keywords[4]); // first
				// graph
				String graphHeaderIcon[] = null;

				Map<String, Object> graph = (Map<String, Object>) graphSect.get("graph");
				String graphPath[] = { (String) graph.get("graph1"), (String) graph.get("graph2") };
				String contentHtml[] = { (String) graph.get("description1") };

				String graphHeaderText[] = { styleObj.nonTreedGraphHeader, styleObj.treedGraphHeader };

				addGraphParagrph(document, styleObj.speciesPrairieGraphHeader, styleObj.forestIcon, graphHeaderIcon,
						graphHeaderText, graphPath, contentHtml);

			} catch (Exception e) {
				e.printStackTrace();
			}

			// third graph
			LineSeparator line = new LineSeparator();

			document.add(line);

			try {
				Map<String, Object> graphSect = this.getContentRow(content, styleObj.keywords[5]); // graph
				String graphHeaderIcon[] = { styleObj.forestIcon, styleObj.prairieIcon };

				Map<String, Object> graph = (Map<String, Object>) graphSect.get("graph");
				String graphPath[] = { (String) graph.get("graph1"), (String) graph.get("graph2") };
				String contentHtml[] = { (String) graph.get("description1") };

				String graphHeaderText[] = { styleObj.linearFPForestGraphHeader, styleObj.linearFPPrairieGraphHeader };

				addGraphParagrph(document, styleObj.linearFPHeader, null, graphHeaderIcon, graphHeaderText, graphPath,
						contentHtml);

			} catch (Exception e) {
				e.printStackTrace();
			}

			// forth graph
			Map<String, Object> impactSect = this.getContentRow(content, styleObj.keywords[6]); // impact
			section = (Map<String, Object>) impactSect.get("graph"); // graph

			addHeaderParagrph(document, styleObj.mainheader[2], (String) section.get(description));

			addAllReadMoreParagraph(document, (Map<String, Object>) impactSect.get("readMore"));

			try {

				String graphHeaderIcon[] = { styleObj.forestIcon, styleObj.prairieIcon };
				Map<String, Object> graph = (Map<String, Object>) impactSect.get("graph");
				String graphPath[] = { (String) graph.get("graph1"), (String) graph.get("graph2") };
				String contentHtml[] = { (String) graph.get("description1"), (String) graph.get("description2") };

				String graphHeaderText[] = { styleObj.hfEffectForestGraphHeader, styleObj.hfEffectPrairieGraphHeader };

				addGraphParagrph(document, null, null, graphHeaderIcon, graphHeaderText, graphPath, contentHtml);

			} catch (Exception e) {
				e.printStackTrace();
			}

			// 3 map section graph
			Map<String, Object> mapSect = this.getContentRow(content, styleObj.keywords[7]); // impact
																								// graph
			section = (Map<String, Object>) mapSect.get("section");

			addHeaderParagrph(document, styleObj.mainheader[3], (String) section.get(description));

			addAllReadMoreParagraph(document, (Map<String, Object>) mapSect.get("readMore"));

			try {

				String graphHeaderIcon[] = {};
				Map<String, Object> graph = (Map<String, Object>) mapSect.get("graph");
				String graphPath[] = { (String) graph.get("graph1"), (String) graph.get("graph2"),
						(String) graph.get("graph3") };
				String contentHtml[] = { (String) graph.get("description1"), (String) graph.get("description2"),
						(String) graph.get("description3") };

				String graphHeaderText[] = styleObj.mapHeader;
				addGraphParagrph(document, null, null, graphHeaderIcon, graphHeaderText, graphPath, contentHtml);

			} catch (Exception e) {
				e.printStackTrace();
			}

			addHeaderParagrph(document, styleObj.mainheader[4], "");

			addHeaderParagrph(document, "Results Coming Soon", "", styleObj.textFont);

			addHeaderParagrph(document, styleObj.mainheader[5], "");

			addHeaderParagrph(document, "Not Available", "", styleObj.textFont);
			addHeaderParagrph(document, styleObj.mainheader[6], "");

			Map<String, Object> referenceSect = this.getContentRow(content, styleObj.keywords[9]); // impact
																									// graph

			addHeaderParagrph(document, styleObj.mainheader[5], "");

			section = (Map<String, Object>) referenceSect.get("section");

			addHeaderParagrph(document, styleObj.referenceSubHeader, (String) section.get(description),
					styleObj.methodHeadingFont);
			addHeaderParagrph(document, styleObj.dataSourceSubHeader, (String) section.get("data-source"),
					styleObj.methodHeadingFont);

			addHeaderParagrph(document, styleObj.creditSubHeader, (String) section.get("citation"),
					styleObj.methodHeadingFont);

			document.close();
			writer.close();
			return outputPdf.getName();
		} catch (Exception e) {
			e.printStackTrace();
			if (document != null)
				document.close();

			if (writer != null)
				writer.close();

		}
		return null;
	}

	void addBanner(Document document) {
		try {
			PdfPTable headerTable = new PdfPTable(2);
			headerTable.getDefaultCell().setBorder(0);
			headerTable.setSpacingAfter(0f);
			headerTable.setSpacingBefore(0f);
			headerTable.getDefaultCell().setPadding(0f);

			headerTable.getDefaultCell().setBackgroundColor(styleObj.blueBGColor);

			float widthCells[] = { 50, 500 };

			headerTable.setWidths(widthCells);
			headerTable.setWidthPercentage(100f);

			Image img = Image.getInstance(styleObj.abmiHeaderImagePath);
			PdfPCell cell = new PdfPCell(img, true);
			cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			cell.setBackgroundColor(styleObj.greenGraphHeadingColor);
			cell.setBorder(0);

			headerTable.addCell(cell);

			Phrase textHeader = new Phrase(styleObj.headerText, styleObj.headingWhiteFont);
			PdfPCell cell2 = new PdfPCell(textHeader);

			cell2.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			cell2.setBackgroundColor(styleObj.blueBGColor);
			cell2.setBorder(0);

			headerTable.addCell(cell2);

			document.add(headerTable);
		} catch (Exception e) {

		}
	}

	void addProfileBanner(Document document, Map<String, Object> row) {
		/* banner graph / text / distribution maps */
		String image = null;
		Map<String, Object> section = (Map<String, Object>) row.get("section");

		if (section.get("image") != null && ((String) section.get("image")).endsWith(GraphService.imageExtention)) {
			image = (String) section.get("image");
		}

		String map = null;
		if (section.get("map") != null && ((String) section.get("image")).length() > 4)
			map = this.rootPath + section.get("map");

		String commonName = (String) section.get("commonName");
		String scientificName = (String) section.get("scientificName");
		String descriptionHtml = (String) section.get("description");

		String CSS = "p { color:#ffffff; }  " + " .orange {font-size:17px; color:#751e11;}";
		PdfPTable tb;

		if (image == null) {
			tb = new PdfPTable(2);

			float widthCells1[] = { 400, 200 };
			try {
				tb.setWidths(widthCells1);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else {
			tb = new PdfPTable(3);
			Image img;
			try {
				img = Image.getInstance(image);
				PdfPCell imgCell = new PdfPCell(img, true);
				imgCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
				imgCell.setBackgroundColor(styleObj.orangeBGColor);
				tb.addCell(imgCell);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			float widthCells1[] = { 200, 400, 200 };
			try {
				tb.setWidths(widthCells1);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		tb.getDefaultCell().setBorder(0);
		tb.setSpacingAfter(0f);
		tb.setSpacingBefore(0f);
		tb.getDefaultCell().setPadding(0f);
		tb.setWidthPercentage(100f);

		tb.getDefaultCell().setBackgroundColor(styleObj.orangeBGColor);
		// add names
		PdfPCell cell = new PdfPCell();
		cell.setBorder(0);
		cell.setBackgroundColor(styleObj.orangeBGColor);
		Paragraph namePar = new Paragraph();
		namePar.add(new Paragraph(commonName, styleObj.bigSpeciesHeadingFont));
		namePar.add(new Paragraph(scientificName, styleObj.smallSpeciesHeadingFont));
		cell.addElement(namePar);

		// add descriptions

		try {
			for (Element e : XMLWorkerHelper.parseToElementList(descriptionHtml, CSS)) {
				cell.addElement(e);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tb.addCell(cell);
		// add map
		Image mapImg;
		try {
			mapImg = Image.getInstance(map);
			PdfPCell mapCell = new PdfPCell(mapImg, true);
			mapCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			mapCell.setBackgroundColor(styleObj.orangeBGColor);
			tb.addCell(mapCell);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(tb);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void addHeaderParagrph(Document doc, String headerLabel, String contentHtml) {

		addHeaderParagrph(doc, headerLabel, contentHtml, styleObj.headingFont);
	}

	/**
	 * add header with html content below
	 * 
	 * @param doc
	 * @param headerLabel
	 * @param contentHtml
	 * @param font
	 */
	void addHeaderParagrph(Document doc, String headerLabel, String contentHtml, Font font) {

		Paragraph header = new Paragraph();
		header.add(new Chunk(headerLabel, font));
		header.setSpacingAfter(5);
		header.setSpacingBefore(5);

		try {
			doc.add(header);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Paragraph paragraph = new Paragraph();

		paragraph.setSpacingAfter(10);
		String CSS = "";
		try {
			if (contentHtml != null) { // only process when it is not null

				for (Element e : XMLWorkerHelper.parseToElementList(contentHtml, CSS)) {
					paragraph.add(e);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			doc.add(paragraph);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * 
	 * @param doc
	 * @param headerLabel
	 * @param headerIcon
	 * @param graphHeaderText
	 * @param graphPath
	 * @param contentHtml
	 * @param font
	 */

	void addGraphParagrph(Document doc, String headerLabel, String headerIcon, String graphHeaderIcon[],
			String[] graphHeaderText, String[] graphPath, String[] contentHtml) {

		Paragraph header = new Paragraph(10);
		header.setSpacingBefore(5);

		if (headerIcon != null) { // header line
			PdfPCell bigCell = new PdfPCell();
			PdfPTable innerTable = new PdfPTable(2);
			float[] innerWidthCells = { 5, 95 };
			try {
				innerTable.setWidths(innerWidthCells);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			innerTable.setWidthPercentage(100f);

			Image icon;
			try {
				icon = Image.getInstance(headerIcon);
				icon.setScaleToFitLineWhenOverflow(true);
				PdfPCell iconCell = new PdfPCell(icon, true);
				iconCell.setBorder(0);
				innerTable.addCell(iconCell);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell cell = new PdfPCell();
			cell.addElement(new Chunk(headerLabel, styleObj.greenGraphHeadingFont));
			cell.setBorder(0);
			innerTable.addCell(cell);
			bigCell.setBorder(0);
			bigCell.addElement(innerTable);
			innerTable.getDefaultCell().setBorder(0);
			try {
				innerTable.addCell(bigCell);
				header.add(innerTable);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			if (headerLabel != null) {
				header.add(new Chunk(headerLabel, styleObj.greenGraphHeadingFont));
			}
		}
		String CSS = "";
		/* first create tables */

		if (graphPath != null || graphPath.length > 0) {
			int nColumn = graphPath.length;

			PdfPTable table = new PdfPTable(nColumn);
			float[] widthCells = new float[nColumn];
			for (int i = 0; i < nColumn; i++) {
				widthCells[i] = 100 / nColumn;
			}
			try {
				table.setWidths(widthCells);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			table.setWidthPercentage(100f);
			table.getDefaultCell().setBorder(0);

			/* add content to table cells */

			// first row, all headers
			if (graphHeaderIcon != null && graphHeaderIcon.length == nColumn) {
				for (int i = 0; i < nColumn; i++) {
					PdfPCell bigCell = new PdfPCell();
					PdfPTable innerTable = new PdfPTable(2);
					float[] innerWidthCells = { 5, 95 };
					try {
						innerTable.setWidths(innerWidthCells);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					innerTable.setWidthPercentage(100f);

					Image icon;
					try {
						icon = Image.getInstance(graphHeaderIcon[i]);
						icon.setScaleToFitLineWhenOverflow(true);
						PdfPCell iconCell = new PdfPCell(icon, true);
						iconCell.setBorder(0);
						innerTable.addCell(iconCell);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					PdfPCell cell = new PdfPCell();
					cell.addElement(new Chunk(graphHeaderText[i], styleObj.greenGraphHeadingFont));
					cell.setBorder(0);
					innerTable.addCell(cell);
					bigCell.addElement(innerTable);
					bigCell.setBorder(0);
					innerTable.getDefaultCell().setBorder(0);
					table.addCell(bigCell);
				}
			} else if (graphHeaderText != null) {
				for (int i = 0; i < nColumn && i < graphHeaderText.length; i++) {
					PdfPCell cell = new PdfPCell();
					cell.addElement(new Chunk(graphHeaderText[i], styleObj.greenGraphHeadingFont));
					cell.setBorder(0);
					table.addCell(cell);
				}
			}

			// second row, all images

			for (int i = 0; i < nColumn; i++) {

				Image graph;
				try {
					if (graphPath[i] != null && graphPath[i].length() > 4) {
						graph = Image.getInstance(this.rootPath + graphPath[i].substring(this.prefix.length() + 2));
						PdfPCell graphCell = new PdfPCell(graph, true);
						graphCell.setBorder(0);
						table.addCell(graphCell);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					PdfPCell graphCell = new PdfPCell();
					graphCell.addElement(new Chunk(""));
					graphCell.setBorder(0);
					table.addCell(graphCell);
				}

			}

			// thrid row, all description
			if (contentHtml != null) {
				if (contentHtml.length == nColumn) { // if there are one
														// description html for
														// each cells, add them
														// in

					for (int i = 0; i < nColumn; i++) {
						PdfPCell cell = new PdfPCell();

						try {
							if (contentHtml != null && contentHtml[i] != null) {
								for (Element e : XMLWorkerHelper.parseToElementList(contentHtml[i], CSS)) {
									cell.addElement(e);
								}
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							cell.addElement(new Chunk(""));
						}

						cell.setBorder(0);
						table.addCell(cell);
					}
				} else if (contentHtml.length == 1) { // if there are only one
														// description together,
														// add them in as one
														// row
					PdfPCell cell = new PdfPCell();

					try {
						if (contentHtml != null && contentHtml[0] != null) {
							for (Element e : XMLWorkerHelper.parseToElementList(contentHtml[0], CSS)) {
								cell.addElement(e);
							}
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					cell.setBorder(0);
					cell.setColspan(nColumn);
					table.addCell(cell);

				} else {
					log.error("the size of html doesn't match the number of graphs");
				}
			}

			header.add(table);
		}
		try {
			doc.add(header);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * find matching keyword rows
	 * 
	 * @param content
	 * @param keyword
	 * @return
	 */
	Map<String, Object> getContentRow(List<Map<String, Object>> content, String keyword) {
		for (Map<String, Object> row : content) {
			Map<String, Object> section = (Map<String, Object>) row.get("section");
			if (keyword.equals(section.get("keyword"))) {
				return row;
			}
		}

		return null;
	}

	/**
	 * 
	 * @param doc
	 * @param readMore
	 */
	void addAllReadMoreParagraph(Document doc, Map<String, Object> readMore) {
		if (readMore == null) {
			return;
		}

		for (Map.Entry<String, Object> entry : readMore.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue().toString();

			addHeaderParagrph(doc, key, value, styleObj.methodHeadingFont);
		}
	}
	/*
	 * public void createPDF() { try { Document document = new Document();
	 * document.setPageSize(PageSize.LETTER); // 612, 792
	 * document.setMargins(36, 36, 40, 72); // left, right, // top, bottom //
	 * document.setMarginMirroring(false);
	 * document.addAuthor("Alberta Biodiversity Monitoring Insititue");
	 * 
	 * File outputPdf = null;
	 * 
	 * outputPdf = File.createTempFile("LetterTo", ".pdf");
	 * 
	 * // step 2: we create a writer that listens to the // document PdfWriter
	 * writer = PdfWriter.getInstance(document, new
	 * FileOutputStream(outputPdf)); // step 3: we open the document
	 * SpeciesProfilePDFHeaderFooter pageHeaderFooterEvent = new
	 * SpeciesProfilePDFHeaderFooter();
	 * 
	 * pageHeaderFooterEvent.setHeaderBannerText("ABMI Species Profile Series");
	 * pageHeaderFooterEvent.setHeaderImage(styleObj. abmiHeaderImagePath);
	 * pageHeaderFooterEvent.setHeaderText("Species Profiles Header Text");
	 * 
	 * pageHeaderFooterEvent.setFooterText("www.abmi.ca");
	 * pageHeaderFooterEvent.setFootImage(styleObj.abmiHeaderImagePath) ;
	 * 
	 * writer.setPageEvent(pageHeaderFooterEvent);
	 * writer.setViewerPreferences(PdfWriter.PageModeUseOutlines);
	 * 
	 * // open the document document.open(); /** add header
	 *
	 * addBanner(document); addProfileBanner(document);
	 * 
	 * /** main content ** String forestIcon =
	 * "http://dna.abmi.co/assets/Forested-Region.png"; String prairieIcon =
	 * "http://dna.abmi.co/assets/Prairie-Region.png";
	 * 
	 * /* content * String mainheader = "Introduction"; String method =
	 * "Methods"; String sppDescription =
	 * "<p>Pileated Woodpecker is typically associated with mature coniferous, mixedwood, and deciduous forests stands, but will live in younger forests or even urban areas if large, dead trees are present. Nests and roosts in the largest available trees and snags within a forest stand, preferring Trembling Aspen trees if available.</p>"
	 * ;
	 * 
	 * addHeaderParagrph(document, mainheader, "<p>introduction</p>"); String
	 * content =
	 * "<p>Pileated Woodpecker is typically associated with mature coniferous, mixedwood, and deciduous forests stands, but will live in younger forests or even urban areas if large, dead trees are present. Nests and roosts in the largest available trees and snags within a forest stand, preferring Trembling Aspen trees if available.</p>"
	 * ;
	 * 
	 * String habitatHeader = "Habitat & Human Footprint Associations";
	 * 
	 * addHeaderParagrph(document, habitatHeader, content);
	 * 
	 * addHeaderParagrph(document, method, content, styleObj.methodHeadingFont);
	 * 
	 * String speciesForestGraphHeader =
	 * "Species-habitat Associations in the Forested Region";
	 * 
	 * String speciesPrairieGraphHeader =
	 * "Species-habitat Associations in the Prairie Region";
	 * 
	 * try {
	 * 
	 * String graphHeaderIcon[] = {}; String graphPath[] = {
	 * "http://dna.abmi.co/assets/forested-region-graph.png" }; String
	 * graphHeaderText[] = {};
	 * 
	 * String contentHtml[] = {
	 * "<ul><li>Pileated Woodpecker relative abundance does not change as soft linear footprint increases but relative abundance decreases with hard linear footprint in the forested region.  </li>"
	 * +
	 * "<li>PPileated Woodpecker relative abundance decreases with soft linear footprint and hard linear footprint in the prairie region. </li>"
	 * +
	 * "<li>The pairs of points show the change expected from the average habitat with no linear footprint (left point) and when 10% linear"
	 * + " footprint is added (right point).</li></ul>" };
	 * 
	 * addGraphParagrph(document, speciesForestGraphHeader, forestIcon, null,
	 * null, graphPath, contentHtml); } catch (Exception e) { } // second graph
	 * String nonTreedGraphHeader = "Non-Treed Sites in the Prairie Region";
	 * String treedGraphHeader = "Treed Sites in the Prairie Region";
	 * 
	 * try {
	 * 
	 * String graphHeaderIcon[] = null; String graphPath[] = {
	 * "http://dna.abmi.co/assets/Non-Treed-Sites.png",
	 * "http://dna.abmi.co/assets/Treed-Sites.png" }; String graphHeaderText[] =
	 * new String[2];
	 * 
	 * graphHeaderText[0] = nonTreedGraphHeader; graphHeaderText[1] =
	 * treedGraphHeader; String contentHtml[] = {
	 * "<ul><li>Pileated Woodpecker relative abundance increases with forest age in all stand types in the forested region, but is most abundant in old upland spruce stands.</li>"
	 * +
	 * "	<li>Predicted relative abundance is also quite high in cultivated human footprint as well as shrub and wetland vegetation types.</li></ul>"
	 * };
	 * 
	 * addGraphParagrph(document, speciesPrairieGraphHeader, forestIcon,
	 * graphHeaderIcon, graphHeaderText, graphPath, contentHtml);
	 * 
	 * } catch (Exception e) { }
	 * 
	 * // third graph String linearFPHeader =
	 * "Relationship to Linear Footprint";
	 * 
	 * String linearFPForestGraphHeader =
	 * "Relationship to Linear Footprint in the Forest Region"; String
	 * linearFPPrairieGraphHeader =
	 * "Relationship to Linear Footprint in the Prairie Region";
	 * 
	 * LineSeparator line = new LineSeparator();
	 * 
	 * document.add(line);
	 * 
	 * try {
	 * 
	 * String graphHeaderIcon[] = { forestIcon, prairieIcon }; String
	 * graphPath[] = { "http://dna.abmi.co/assets/liner-1.png",
	 * "http://dna.abmi.co/assets/liner-2.png" }; String graphHeaderText[] = {
	 * linearFPForestGraphHeader, linearFPPrairieGraphHeader };
	 * 
	 * String contentHtml[] = {
	 * "<ul><li>Pileated Woodpecker relative abundance does not change as soft linear footprint increases but relative abundance decreases with hard linear footprint in the forested region.  </li>"
	 * +
	 * "<li>PPileated Woodpecker relative abundance decreases with soft linear footprint and hard linear footprint in the prairie region. </li>"
	 * +
	 * "<li>The pairs of points show the change expected from the average habitat with no linear footprint (left point) and when 10% linear footprint is added (right point).</li></ul>"
	 * };
	 * 
	 * addGraphParagrph(document, linearFPHeader, null, graphHeaderIcon,
	 * graphHeaderText, graphPath, contentHtml);
	 * 
	 * } catch (Exception e) { }
	 * 
	 * // forth graph String impactsOfHFHeader = "Impacts of Human Footprint";
	 * String hfEffectForestGraphHeader =
	 * "Human Footprint Effects in the Forested Region"; String
	 * hfEffectPrairieGraphHeader =
	 * "Human Footprint Effects in the Prairie Region";
	 * 
	 * String html =
	 * "<p>Pileated Woodpecker can be found in a wide variety of habitats, including urban areas, as long as there are large dead and dying trees available for nesting and roosting. In the forested region, forestry footprint results in the biggest predicted decrease in Pileated Woodpecker population abundance because it covers the largest area. Whereas, in the prairie region, agriculture covers the largest area resulting in the largest predicted decrease.</p>"
	 * ; addHeaderParagrph(document, impactsOfHFHeader, html);
	 * 
	 * addHeaderParagrph(document, method, html, styleObj.methodHeadingFont);
	 * 
	 * try {
	 * 
	 * String graphHeaderIcon[] = { forestIcon, prairieIcon }; String
	 * graphPath[] = { "http://dna.abmi.co/assets/ForestedRegion-graph.png",
	 * "http://dna.abmi.co/assets/Prairie-Region-graph.png" }; String
	 * graphHeaderText[] = { hfEffectForestGraphHeader,
	 * hfEffectPrairieGraphHeader };
	 * 
	 * String contentHtml[] = { "<ul>" +
	 * "<li>Urban footprint has the strongest strong unit effect on the Pileated Woodpecker but because it occupies a small area, only results in a 0.3% decrease in predicted population size.</li>"
	 * +
	 * "<li>Forestry footprint has a relatively low negative effect per unit area, but because it occupies a largest area in the forested region results in a predicted 3.1% decrease in the Pileated Woodpecker</li>	</ul>"
	 * ,
	 * 
	 * "<ul><li>Forestry and urban footprint have the strongest per unit effect on the Pileated Woodpecker in the prairie region but because each  occupies a small area, results in a -0.5% and -1.3% predicted decrease in population abundance, respectively.</li>"
	 * +
	 * "<li>Agriculture footprint has a relatively low negative effect per unit area, but because it occupies a large area, results in a predicted 28.8% decrease in the Pileated Woodpecker.</li>		</ul>"
	 * };
	 * 
	 * addGraphParagrph(document, null, null, graphHeaderIcon, graphHeaderText,
	 * graphPath, contentHtml);
	 * 
	 * } catch (Exception e) { }
	 * 
	 * String predictedRAHeader = "Predicted Relative Abundance";
	 * 
	 * String refMapHeader = "Reference Conditions"; String currentMapHeader =
	 * "Current Conditions"; String differenceMapHeader =
	 * "Difference Conditions";
	 * 
	 * html =
	 * "<p>Pileated Woodpecker can be found in a wide variety of habitats, including urban areas, as long as there are large dead and dying trees available for nesting and roosting. In the forested region, forestry footprint results in the biggest predicted decrease in Pileated Woodpecker population abundance because it covers the largest area. Whereas, in the prairie region, agriculture covers the largest area resulting in the largest predicted decrease.</p>"
	 * ; addHeaderParagrph(document, predictedRAHeader, html);
	 * 
	 * addHeaderParagrph(document, method, html, styleObj.methodHeadingFont);
	 * 
	 * try {
	 * 
	 * String graphHeaderIcon[] = {}; String graphPath[] = {
	 * "http://dna.abmi.co/assets/Reference-map.png",
	 * "http://dna.abmi.co/assets/Current-map.png",
	 * "http://dna.abmi.co/assets/Difference-map.png" }; String
	 * graphHeaderText[] = { "Reference Map", "Current Map", "Difference Map" };
	 * 
	 * String contentHtml[] = {
	 * "<ul><li>The reference condition shows the predicted relative abundance of the Pileated Woodpecker after all human footprint had been backfilled based on native vegetation in the surrounding area.</li></ul>"
	 * ,
	 * "<ul><li>The reference condition shows the predicted relative abundance of the Pileated Woodpecker after all human footprint had been backfilled based on native vegetation in the surrounding area.</li></ul>"
	 * ,
	 * "<ul><li>The Pileated Woodpecker is predicted to have declined in abundance in parts of its range as indicated by areas in purple.</li></ul>"
	 * };
	 * 
	 * addGraphParagrph(document, null, null, graphHeaderIcon, graphHeaderText,
	 * graphPath, contentHtml);
	 * 
	 * } catch (Exception e) { }
	 * 
	 * String impactClimateHeader = "Impacts of Climate Change";
	 * addHeaderParagrph(document, impactClimateHeader, "");
	 * 
	 * addHeaderParagrph(document, "Results Coming Soon", "",
	 * styleObj.textFont);
	 * 
	 * String otherIssueHeader = "Other Issues";
	 * 
	 * String referenceHeader = "References";
	 * 
	 * addHeaderParagrph(document, otherIssueHeader, "");
	 * 
	 * addHeaderParagrph(document, "Not Available", "", styleObj.textFont);
	 * 
	 * String referenceSubHeader = "References"; html =
	 * "<p>Cornell Lab of Ornithology. 2015. All About Birds: Pileated Woodpecker [Internet]. New York: Cornell University.]. <a href=\"https://www.allaboutbirds.org/guide/Pileated_Woodpecker/id. Accessed November 30, 2015\" target=\"_blank\">https://www.allaboutbirds.org/guide/Pileated_Woodpecker/id. Accessed November 30, 2015</a></p>"
	 * +
	 * "<p>Tree of Life Web Project. 2008. Picidae. Woodpeckers. Version 27. June <a href=\"http://tolweb.org/Picidae/26423/2008.06.27\" target=\"_blank\">http://tolweb.org/Picidae/26423/2008.06.27</a>. Accessed February 12, 2015."
	 * +
	 * "Bonar, R.L. 2000. Availability of Pileated Woodpecker cavities and use by other species. Journal of Wildlife Management 64(1): 52-59.</p>"
	 * +
	 * "<p>Cooke, H.A. and S.J. Hannon. 2012. Nest-site selection by old boreal forest cavity excavators as a basis for structural retention guidelines in spatially-aggregated harvests. Forest Ecology and Management 269(2000): 37-51.</p>"
	 * ;
	 * 
	 * addHeaderParagrph(document, referenceSubHeader, html);
	 * 
	 * String dataSourceSubHeader = "Data Sources"; String creditSubHeader =
	 * "Photo & Credits"; addHeaderParagrph(document, dataSourceSubHeader, html,
	 * styleObj.methodHeadingFont);
	 * 
	 * addHeaderParagrph(document, creditSubHeader, html,
	 * styleObj.methodHeadingFont);
	 * 
	 * document.close(); } catch (Exception e) { e.printStackTrace(); } }
	 */
}
