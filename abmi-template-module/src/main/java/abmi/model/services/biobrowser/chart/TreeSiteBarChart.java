package abmi.model.services.biobrowser.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.swing.Icon;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.MinMaxCategoryRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleInsets;
import org.jfree.util.Log;

import abmi.model.entity.biobrowser.graph.WebGraphCategoryData;
import abmi.model.entity.biobrowser.graph.WebGraphCategoryDetail;
import abmi.model.entity.biobrowser.graph.WebGraphData;
import abmi.model.entity.biobrowser.graph.WebGraphLinearData;
import abmi.model.entity.biobrowser.graph.WebGraphSery;
import abmi.model.entity.biobrowser.graph.WebGraphType;
import abmi.model.services.biobrowser.GraphService;
import abmi.model.util.CommonUtil;

import abmi.model.util.chart.ext.ABMICategoryToolTipGenerator;
import abmi.model.util.chart.ext.CategoryTitleAnnotation;
import abmi.model.util.chart.renderer.ABMIMinMaxCategoryRenderer;
import abmi.model.util.chart.renderer.ABMIMultiColorBarRenderer;

public class TreeSiteBarChart {

	@PersistenceContext(unitName = "data")
	EntityManager em;

	double maxValue = 0;
	double upperBound = 0;
	String fontName = "Open Sans";
	/**
	 * get data from database. calculate the maxvalue used for both treed and
	 * non-treed graphs.
	 * 
	 * @param tsn
	 * @param bTreed
	 *            default is non-tree. to get a treed value, need to read a
	 *            multiplier
	 * @return
	 */
	DefaultCategoryDataset[] getData(int tsn, Boolean bTreed) {
		// dataset 0: min max line, 1: bar
		DefaultCategoryDataset[] dataset = new DefaultCategoryDataset[3];
		dataset[0] = new DefaultCategoryDataset();
		dataset[1] = new DefaultCategoryDataset();
		dataset[2] = new DefaultCategoryDataset();

		// get wgt =2 data Non-Treed Sites/Treed Sites

		TypedQuery<WebGraphCategoryData> query = em.createQuery(
				"select  p from WebGraphCategoryData p where p.wgcdTsn =:tsn and  p.webGraphSery.webGraphType.wgtId  =2 and  p.wgcdIsTreed = :isTreed  order by p.webGraphSery.wgsOrder",
				WebGraphCategoryData.class);

		query.setParameter("tsn", tsn);
		query.setParameter("isTreed", bTreed ? 1 : 0);

		for (WebGraphCategoryData row : CommonUtil.emptyIfNull(query.getResultList())) {
			if (row.getWgcdUpper() > this.maxValue) {
				maxValue = row.getWgcdUpper();
			}

			// bar dataset
			dataset[2].addValue(row.getWgcdMean(), "Mean", row.getWebGraphSery().getWgsLabel());

			// min Max

			dataset[1].addValue(row.getWgcdMean(), "Mean", row.getWebGraphSery().getWgsLabel());
			dataset[1].addValue(row.getWgcdLower(), "Min", row.getWebGraphSery().getWgsLabel());

			dataset[0].addValue(row.getWgcdMean(), "Mean", row.getWebGraphSery().getWgsLabel());

			dataset[0].addValue(row.getWgcdUpper(), "Max", row.getWebGraphSery().getWgsLabel());
		}
		if (dataset[0].getColumnCount() == 0 && dataset[0].getRowCount() == 0)
			return null;

		return dataset;

	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	/**
	 * because the treed and non-treed graph shared same data except with a x
	 * multipler. if we create two graphs separately, they will look similar
	 * except the range axis are different. so we want to create two graphs have
	 * same range axis values. so users can tell the difference.
	 * 
	 * 
	 * @param graphType
	 * @param tsn
	 * @param treedPath
	 * @param nonTreedPath
	 * @return
	 */
	public boolean[] createChart(WebGraphType graphType, int tsn, String treedPath, String nonTreedPath) {

		// configure plot final ABMIMultiColorBarRenderer renderer = new
		// ABMIMultiColorBarRenderer(colors);
		boolean bGraphCreated[] = new boolean[2];
		bGraphCreated[0] = false;
		bGraphCreated[1] = true;

		/* read colors from db */
		Paint[] colors = new Paint[graphType.getWebGraphSeries().size()];
		for (int i = 0; i < colors.length; i++) { // acutally
													// only two:
													// one is
													// soft
													// liear,
													// the other
													// is hard
													// linear
			colors[i] = Color.decode(graphType.getWebGraphSeries().get(i).getWgsColor());
		}
		/* get data from db */
		DefaultCategoryDataset[] treedDataset = this.getData(tsn, true);
		DefaultCategoryDataset[] nonTreeddataset = this.getData(tsn, false);

		/**
		 * I use use same upper bound for both treed and untreed graphs, so they
		 * can be compared side by side. in order to do this, I manually
		 * caculuate the number, with some round ups.
		 */

		this.maxValue *= 1.2;

		if (this.maxValue < 0.005) {
			upperBound = (Math.round(maxValue * 10000 + 0.5)) / 1000.0;
		} else if (this.maxValue < 0.05) {
			upperBound = (Math.round(maxValue * 1000 + 0.5)) / 1000.0;
		} else if (this.maxValue < 0.5) {
			upperBound = (Math.round(maxValue * 100 + 0.5)) / 100.0;
		} else {
			upperBound = (Math.round(maxValue * 10 + 0.5)) / 10.0;
		}

		if (treedDataset == null) { // no data available
			bGraphCreated[0] = false;
		} else {
			bGraphCreated[0] = createPlot(graphType, treedDataset, colors, treedPath);
		}

		if (nonTreeddataset == null) { // no data available
			bGraphCreated[1] = false;
		} else {
			bGraphCreated[1] = createPlot(graphType, nonTreeddataset, colors, nonTreedPath);
		}

		return bGraphCreated;

	}

	boolean createPlot(WebGraphType graphType, DefaultCategoryDataset[] dataset, Paint[] colors, String path) {

		JFreeChart chart = ChartFactory.createLineChart(graphType.getWgtTitle(), graphType.getWgtDomainLabel(),
				graphType.getWgtValueLabel(), dataset[0], PlotOrientation.VERTICAL, true, true, false);

		chart.setBackgroundPaint(new Color(255, 255, 255, 255)); // set
																	// background
																	// as
																	// transparent
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		plot.setBackgroundPaint(new Color(255, 255, 255, 255));

		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(new Color(100, 100, 100));
		plot.setOutlineVisible(false);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));

		/**
		 * custom renderer to draw verticle lines over bar centers, matching
		 * bar's color
		 */
		ABMIMinMaxCategoryRenderer minMaxRenderer = new ABMIMinMaxCategoryRenderer(colors);
		minMaxRenderer.setDrawLines(false);
		minMaxRenderer.setDefaultEntityRadius(0);
		// minMaxRenderer.setGroupPaint(Color.red);
		minMaxRenderer.setMaxIcon(null);
		minMaxRenderer.setMinIcon(null);
		minMaxRenderer.setObjectIcon(null);
		minMaxRenderer.setGroupPaint(null);
		minMaxRenderer.setGroupStroke(new BasicStroke(2f));
		minMaxRenderer.setAutoPopulateSeriesStroke(false);

		plot.setDataset(0, dataset[0]);
		plot.setRenderer(0, minMaxRenderer);

		/**
		 * custom renderer to draw verticle lines over bar centers, using white
		 * colors.
		 */

		ABMIMinMaxCategoryRenderer minMaxRenderer2 = new ABMIMinMaxCategoryRenderer(null);
		minMaxRenderer2.setDrawLines(false);

		// minMaxRenderer.setGroupPaint(Color.red);
		minMaxRenderer2.setMaxIcon(null);
		minMaxRenderer2.setMinIcon(null);
		minMaxRenderer2.setObjectIcon(null);
		minMaxRenderer2.setGroupPaint(new Color(255, 255, 255));
		minMaxRenderer2.setGroupStroke(new BasicStroke(2f));
		minMaxRenderer2.setAutoPopulateSeriesStroke(false);

		plot.setDataset(1, dataset[1]);
		plot.setRenderer(1, minMaxRenderer2);

		/**
		 * custom bar render to draw different color bars.
		 */
		ABMIMultiColorBarRenderer renderer = new ABMIMultiColorBarRenderer(colors);

		renderer.setBarPainter(new StandardBarPainter());
		//
		renderer.setDrawBarOutline(false);

		renderer.setMaximumBarWidth(0.05);
		renderer.setBaseItemLabelFont(new Font("Arial", Font.PLAIN, 9));
		renderer.setShadowVisible(false);
		renderer.setAutoPopulateSeriesFillPaint(false);
		renderer.setMaximumBarWidth(0.2); // set bar width
		plot.setRenderer(2, renderer);
		plot.setDataset(2, dataset[2]);

		// set y axis label, and line

		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();

		yAxis.setLabelFont(new Font(fontName, Font.BOLD, 15));
		yAxis.setAxisLineStroke(new BasicStroke(1.5f));
		yAxis.setAxisLinePaint(new Color(0, 0, 0));
		yAxis.setUpperMargin(0.5); // this works when range is auto set

		Log.debug("upperBound = " + upperBound + " tickUnit = " + (upperBound / 4.0));
		yAxis.setTickUnit(new NumberTickUnit(upperBound / 4.0));
		yAxis.setUpperBound(upperBound);

		// configure xAxis

		plot.setRangeAxis(yAxis);

		CategoryAxis xAxis = plot.getDomainAxis();
		// xAxis.setLabelFont(new Font(fontName, Font.BOLD, 9));
		xAxis.setAxisLinePaint(new Color(0, 0, 0));
		xAxis.setAxisLineStroke(new BasicStroke(1.5f));
		xAxis.setMaximumCategoryLabelLines(1);
		for (int i = 0; i < dataset[2].getColumnCount(); i++) {
			xAxis.setTickLabelPaint(dataset[2].getColumnKey(i), colors[i]);

		}

		xAxis.setTickLabelFont(new Font(fontName, Font.BOLD, 13));
		xAxis.setMaximumCategoryLabelWidthRatio(1.2f);
		xAxis.setMaximumCategoryLabelLines(2);

		plot.setDomainAxis(xAxis);
		plot.setBackgroundAlpha(0.0f);
		// hide default legend.
		chart.removeLegend();

		return GraphService.saveChartAsImage(chart, graphType, path);

	}
}
