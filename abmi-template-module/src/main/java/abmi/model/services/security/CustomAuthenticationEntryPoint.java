package abmi.model.services.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.AuthenticationEntryPoint;

public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {
	private static final Logger log = LoggerFactory
			.getLogger(CustomAuthenticationEntryPoint.class);
	
	// @Override
	public void commence(
			HttpServletRequest request,
			HttpServletResponse response,
			org.springframework.security.core.AuthenticationException authException)
	

			throws IOException, ServletException {
		
		try {

			// response.sendError(401); /* Not authorized*/
			response.setHeader("REQUIRES_AUTH", "1");
			response.setStatus(401);

			// response.sendRedirect("/spring_security_login");
			

			if (request.getUserPrincipal() != null) {
			}
		} catch (Exception exp) {

			log.error("Exception while Creating Status ");
			exp.printStackTrace();

		}
	}
}