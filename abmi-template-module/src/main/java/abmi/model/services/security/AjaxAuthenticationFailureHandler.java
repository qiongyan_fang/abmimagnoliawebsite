package abmi.model.services.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;

import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

/* not used */ 
//@Component("ajaxAuthenticationFailureHandler")
public class AjaxAuthenticationFailureHandler extends
		SimpleUrlAuthenticationFailureHandler {

	public AjaxAuthenticationFailureHandler() {
		
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {

//		HttpSession session = request.getSession();
//		DefaultSavedRequest defaultSavedRequest = (DefaultSavedRequest) session
//				.getAttribute(WebAttributes.SAVED_REQUEST);
		// check if login is originated from ajax call
		
//		failed AjaxAuthenticationFailureHandler
		
		if ("true".equals(request.getHeader("X-Ajax-call"))) {
			try {
				
				response.getWriter().print(exception.getMessage());// return "not-ok" string
				response.getWriter().flush();
				
			} catch (IOException e) {
				// handle exception...
			}
		}
		else{
			super.onAuthenticationFailure(request, response, exception);
		}
	}
}