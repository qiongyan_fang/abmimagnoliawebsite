package abmi.model.services.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import abmi.model.security.entity.User;
import abmi.model.security.repository.UserRepository;

/**
 * A custom {@link UserDetailsService} where user information is retrieved from
 * a JPA repository
 */
@Service
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {
	private static final Logger log = LoggerFactory
			.getLogger(CustomUserDetailsService.class);
	@Autowired
	private UserRepository userRepository;

	@PersistenceContext(unitName = "security")
	private EntityManager em;

	/**
	 * Returns a populated {@link UserDetails} object. The username is first
	 * retrieved from the database and then mapped to a {@link UserDetails}
	 * object.
	 */
	public UserDetails loadUserByUsername(String userid)
			throws UsernameNotFoundException {
		if (userid == null) {
			return null;
		}
		try {

			// abmi.model.security.entity.User domainUser =
			// userRepository.findByUserId(userid);
			User domainUser = this.getUserByUserId(userid);
			if (domainUser == null) {
				throw new RuntimeException("The user name is not registered.");
			}
			boolean enabled = (domainUser.getUserEnabled().intValue() == 1);

			boolean accountNonExpired = true;
			boolean credentialsNonExpired = true;
			boolean accountNonLocked = true;

			log.info(domainUser.getUserName() + " log in");
			return new CustomUserDetails(domainUser.getUserId(), domainUser
					.getUserPassword().toLowerCase(), enabled,
					accountNonExpired, credentialsNonExpired, accountNonLocked,
					domainUser.getGrantedRoles(), domainUser.getUserName(),
					null, null);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Retrieves a collection of {@link GrantedAuthority} based on a numerical
	 * role
	 * 
	 * @param role
	 *            the numerical role
	 * @return a collection of {@link GrantedAuthority
	 */
	public Collection<? extends GrantedAuthority> getAuthorities(Integer role) {
		List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
		return authList;
	}

	/**
	 * Converts a numerical role to an equivalent list of roles
	 * 
	 * @param role
	 *            the numerical role
	 * @return list of roles as as a list of {@link String}
	 */
	public List<String> getRoles(Integer role) {
		List<String> roles = new ArrayList<String>();

		if (role.intValue() == 1) {
			roles.add("ROLE_USER");
			roles.add("ROLE_ADMIN");

		} else if (role.intValue() == 2) {
			roles.add("ROLE_USER");
		}

		return roles;
	}

	/**
	 * Wraps {@link String} roles to {@link SimpleGrantedAuthority} objects
	 * 
	 * @param roles
	 *            {@link String} of roles
	 * @return list of granted authorities
	 */
	public static List<GrantedAuthority> getGrantedAuthorities(
			List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

	User getUserByUserId(String userId) {

		TypedQuery<User> userQuery = em.createQuery(
				"Select p from User p where p.userId=:userId", User.class);

		userQuery.setParameter("userId", userId);
		userQuery.setHint("eclipselink.refresh", "true");
		/** refresh **/

		List<User> userList = userQuery.getResultList();

		if (userList.size() > 0) {
			return userList.get(0);
		}

		return null;

	}
}
