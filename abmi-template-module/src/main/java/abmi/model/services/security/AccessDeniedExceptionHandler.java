package abmi.model.services.security;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

/* not used */
public class AccessDeniedExceptionHandler implements AccessDeniedHandler 
{ 
 
  
  //  @Override 
    public void handle(HttpServletRequest request,
            HttpServletResponse response,
            AccessDeniedException accessDeniedException)
            throws IOException,
                  ServletException { 
         
     	 try {   
     

            response.getWriter().write("{\"status\": false, \"error\": \"Not Authorized\"}"); 
  
           }
         catch (Exception exp) { 
        	 
 
             exp.printStackTrace(); 
  
         } 

         

    } 
 
     
 
} 
