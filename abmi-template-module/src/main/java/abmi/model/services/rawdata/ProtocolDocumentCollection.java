package abmi.model.services.rawdata;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import abmi.model.util.pdf.PDFEditor;

public class ProtocolDocumentCollection {
	private static final Logger log = LoggerFactory
			.getLogger(ProtocolDocumentCollection.class);
	String protocolCode = "";
	List<String> frontDocs, middleDocs, backDocs;

	List<String> methodDocs, metadataDocs;

	File dynamicFrontPage;
	File finalPdf;

	public File getDynamicFrontPage() {
		return dynamicFrontPage;
	}

	public void setDynamicFrontPage(File dynamicFrontPage) {
		this.dynamicFrontPage = dynamicFrontPage;
	}

	public File getFinalPdf() {
		return finalPdf;
	}

	public void setFinalPdf(File finalPdf) {
		this.finalPdf = finalPdf;
	}

	public ProtocolDocumentCollection() {

	}

	public String getProtocolCode() {
		return protocolCode;
	}

	public void setProtocolCode(String protocolCode) {
		this.protocolCode = protocolCode;
	}

	public List<String> getFrontDocs() {
		return frontDocs;
	}

	public void setFrontDocs(List<String> frontDocs) {
		this.frontDocs = frontDocs;
	}

	public List<String> getMiddleDocs() {
		return middleDocs;
	}

	public void setMiddleDocs(List<String> middleDocs) {
		this.middleDocs = middleDocs;
	}

	public List<String> getBackDocs() {
		return backDocs;
	}

	public void setBackDocs(List<String> backDocs) {
		this.backDocs = backDocs;
	}

	public List<String> getMethodDocs() {
		return methodDocs;
	}

	public void setMethodDocs(List<String> methodDocs) {
		this.methodDocs = methodDocs;
	}

	public List<String> getMetadataDocs() {
		return metadataDocs;
	}

	public void setMetadataDocs(List<String> metadataDocs) {
		this.metadataDocs = metadataDocs;
	}

	/**
	 * 
	 * @param docName
	 */
	public void addMetadataDocs(String docName) {
		if (metadataDocs == null) {
			metadataDocs = new ArrayList<String>();
			metadataDocs.add(docName);
			return;
		}

		if (!metadataDocs.contains(docName)) {
			metadataDocs.add(docName);
		}
	}

	/**
	 * 
	 * @param docName
	 */
	public void addFrontDocs(String docName) {
		if (frontDocs == null) {
			frontDocs = new ArrayList<String>();
			frontDocs.add(docName);
			return;
		}

		if (!frontDocs.contains(docName)) {
			frontDocs.add(docName);
		}
	}

	/**
	 * 
	 * @param docName
	 */
	public void addMethodDocs(String docName) {
		if (methodDocs == null) {
			methodDocs = new ArrayList<String>();
			methodDocs.add(docName);
			return;
		}

		if (!methodDocs.contains(docName)) {
			methodDocs.add(docName);
		}
	}

	/**
	 * 
	 * @param docName
	 */
	public void addMiddleDocs(String docName) {
		if (middleDocs == null) {
			middleDocs = new ArrayList<String>();
			middleDocs.add(docName);
			return;
		}

		if (!middleDocs.contains(docName)) {
			middleDocs.add(docName);
		}
	}

	/**
	 * 
	 * @param docName
	 */
	public void addBackDocs(String docName) {
		if (backDocs == null) {
			backDocs = new ArrayList<String>();
			backDocs.add(docName);
			return;
		}

		if (!backDocs.contains(docName)) {
			backDocs.add(docName);
		}
	}

	/**
	 * 
	 * @param outputFile
	 * @param coverFile
	 * @param metadatPath
	 * @param methodPath
	 */
	public void concatenatePfds(File outputFile, File coverFile,
			String metadatPath, String methodPath) {
		/** first gather all pdf in order **/
		ArrayList<File> fileList = new ArrayList<File>();
		// 1. the coverfile
		fileList.add(coverFile);
		// 2. the front file

		for (String fp : this.frontDocs) {
			try {
				File docFp = new File(methodPath, fp);
				if (!fileList.contains(docFp)) {
					fileList.add(docFp);
				}
			} catch (Exception e) {
				log.error("error adding " + fp + "\n" + e.getMessage());
			}
		}
		for (String fp : this.getMethodDocs()) {

			try {
				File docFp = new File(methodPath, fp);
				if (!fileList.contains(docFp)) {
					fileList.add(docFp);
				}
			} catch (Exception e) {
				log.error("error adding " + fp + "\n" + e.getMessage());
			}
		}

		for (String fp : this.middleDocs) {
			try {
				File docFp = new File(methodPath, fp);
				if (!fileList.contains(docFp)) {
					fileList.add(docFp);
				}
			} catch (Exception e) {
				log.error("error adding " + fp + "\n" + e.getMessage());
			}
		}

		for (String fp : this.getMetadataDocs()) {
			try {
				File docFp = new File(metadatPath, fp);
				if (!fileList.contains(docFp)) {
					fileList.add(docFp);
				}
			} catch (Exception e) {
				log.error("error adding " + fp + "\n" + e.getMessage());
			}
		}

		for (String fp : this.backDocs) {
			try {
				File docFp = new File(methodPath, fp);
				if (!fileList.contains(docFp)) {
					fileList.add(docFp);
				}
			} catch (Exception e) {
				log.error("error adding " + fp + "\n" + e.getMessage());
			}
		}
		try {

			PDFEditor.concatenatePDFs(fileList, outputFile);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		this.setFinalPdf(outputFile);
	}
}
