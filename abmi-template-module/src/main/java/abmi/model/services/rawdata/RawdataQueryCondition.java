package abmi.model.services.rawdata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import abmi.model.entity.metadata.GFileDownloadCategory;
import abmi.model.services.ProjectPropertyService;
import abmi.model.services.RegionsService;

/* 
 * conditions appy to current query: i.e. apply to all tables need to be downloaded: protocol locations
 * 
 */

public class RawdataQueryCondition implements Serializable {
	private static final long serialVersionUID = 1L;

	// @PersistenceContext
	// private EntityManager em;

	public List<String> getmRotationNameList() {
		return mRotationNameList;
	}

	ProjectPropertyService projectPropertyService;
	RegionsService regionsService;
	DataCategoryService dataCategoryService;

	public void setRegionsService(RegionsService regionDao) {
		this.regionsService = regionDao;
	}

	String[] coreOffgridLabel = { "ABMI Core Sites", "Offgrid Sites",
			"Both ABMI core and offgrid sites",
			"ABMI sites visited at least once" };

	public void setProjectPropertyService(ProjectPropertyService prjDao) {
		this.projectPropertyService = prjDao;
	}

	boolean bQuerySite = false;

	boolean bAllYears = true, bAllAlberta = true;

	boolean bQueryExactSiteName = false;

	public boolean isbQueryExactSiteName() {
		return bQueryExactSiteName;
	}

	public boolean isbAllYears() {
		return bAllYears;
	}

	public boolean isbAllAlberta() {
		return bAllAlberta;
	}

	List<Integer> mRotationList;
	List<String> mSiteList;
	List<Integer> mYearList;
	List<Integer> mRegionIds;
	List<String> mRegionNameList, mRotationNameList;
	public static final int ABMI_CORE_DATA = 1;
	public static final int ABMI_OFFGRID_DATA = 2;
	public static final int ABMI_CORE_AND_OFFGRID_DATA = 3;
	public static final int ABMI_REVISITED_DATA = 4;
	List<GFileDownloadCategory> mRawDownloadCategoryList,
			mCompiledDownloadCategoryList;
	String mRotationName;

	public List<String> getRequestList(String protocolLocation) {
		if (this.mRawDownloadCategoryList == null)
			return null;

		List<String> resultList = new ArrayList<String>();
		for (GFileDownloadCategory row : this.mRawDownloadCategoryList) {
			if (row.getGFileDownloadCategoryMain().getGfdcmProtocol()
					.equalsIgnoreCase(protocolLocation)) {

				if (this.mOffgridCode == ABMI_REVISITED_DATA) {
					resultList.add("The Raw " + row.getGfdcTitle()
							+ " dataset from revisited sites.");
				} else {
					resultList.add("The Raw " + row.getGfdcTitle()
							+ " dataset from " + this.mRotationName + "."); // return
																			// detail
																			// protocl

					// name: such as site
					// capbility
				}
			}
		}

		for (GFileDownloadCategory row : this.mCompiledDownloadCategoryList) {
			if (row.getGFileDownloadCategoryMain().getGfdcmProtocol()
					.equalsIgnoreCase(protocolLocation)) {
				if (this.mOffgridCode == ABMI_REVISITED_DATA) {
					resultList.add("The Raw " + row.getGfdcTitle()
							+ " dataset from revisited sites.");
				} else {
					resultList.add("The Raw " + row.getGfdcTitle()
							+ " dataset from " + this.mRotationName + "."); // return
																			// detail
																			// protocl

					// name: such as site
					// capbility
				}
			}
		}

		resultList
				.add("The Methodology and Metadata describing above data set(s).");
		return resultList;

	}

	public void setmRawDownloadCategoryList(
			List<GFileDownloadCategory> mRawDownloadCategoryList) {
		this.mRawDownloadCategoryList = mRawDownloadCategoryList;
	}

	public void setmCompiledDownloadCategoryList(
			List<GFileDownloadCategory> mCompiledDownloadCategoryList) {
		this.mCompiledDownloadCategoryList = mCompiledDownloadCategoryList;
	}

	String mTablePrefix = "";
	int mOffgridCode = 1;

	public int getmOffgridCode() {
		return mOffgridCode;
	}

	public void setmOffgridCode(int mOffgridCode) {
		this.mOffgridCode = mOffgridCode;
	}

	String mArchPath;

	List<String> mRequestProtocols;
	List<String> mExtraFiles; // files to be included in zip file
								// individually

	public List<String> getMExtraFiles() {
		if (mExtraFiles != null) {
			return this.mExtraFiles;
		}

		return mExtraFiles;
	}

	public void setmArchPath(String mArchPath) {
		this.mArchPath = mArchPath;
	}

	public boolean isHasPrototype() {
		return hasPrototype;
	}

	public void setHasPrototype(boolean hasPrototype) {
		this.hasPrototype = hasPrototype;
	}

	boolean hasPrototype = true;

	/**
	 * 
	 * @param offgridCode
	 *            1: ingrid, 2, offgrid, 3: all (both in and offgrid)
	 */

	public void setOffgridId(Integer offgridCode) {
		if (offgridCode == null) { // set default as 1 : core data
			offgridCode = 1;
		}
		mOffgridCode = offgridCode;

		mTablePrefix = dataCategoryService
				.getTablePrefixByOffgridCode(mOffgridCode);
		
	}

	/**
	 * 
	 */
	public RawdataQueryCondition() {

		bQuerySite = false;
		mRotationList = new ArrayList<Integer>();
		mSiteList = new ArrayList<String>();
		mYearList = new ArrayList<Integer>();
		mRegionIds = new ArrayList<Integer>();
		mRegionNameList = new ArrayList<String>();
		mRequestProtocols = new ArrayList<String>();
		mTablePrefix = "";
		mOffgridCode = 1;
	}

	public String getmTablePrefix() {
		return mTablePrefix;
	}

	public void setmTablePrefix(String mTablePrefix) {
		this.mTablePrefix = mTablePrefix;
	}

	public String getmArchPath() {
		if (mArchPath == null) {
			mArchPath = projectPropertyService.getPath("ARCHIVE_PATH");
		}
		return mArchPath;
	}

	public boolean isbQuerySite() {
		return bQuerySite;
	}

	public void setbQuerySite(boolean bQuerySite) {
		this.bQuerySite = bQuerySite;
	}

	public List<Integer> getmRotationList() {
		return mRotationList;
	}

	public void setmRotationList(List<Integer> mRotationList) {
		this.mRotationList = mRotationList;

		this.mRotationNameList = dataCategoryService
				.getRotationIdName(mRotationList);

		if (mRotationList == null || mRotationList.size() == 0) {
			this.mRotationName = " all sample years";

		} else {
			String rotationDisplay = mRotationNameList.toString();
			mRotationName = rotationDisplay.substring(1,
					rotationDisplay.length() - 1);
		}

		this.bAllYears = dataCategoryService.isAllRotation(mRotationList);
	}

	public void setDataCategoryService(DataCategoryService dataDao) {
		this.dataCategoryService = dataDao;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getmSiteList() {

		if (this.bQuerySite) {
			if (mSiteList != null && mSiteList.size() > 0) {
				return mSiteList;
			}
		} else {

			if (this.mRegionIds == null || this.mRegionIds.size() == 0) {
				return null;
			}

			mSiteList = new ArrayList<String>();

			// System.out.println("regionService = " + regionsService);
			mSiteList = regionsService.getSiteListByRegionIds(mRegionIds);
		}
		return mSiteList;
	}

	public void setmSiteList(List<String> mSiteList, boolean bQueryExactName) {
		this.mSiteList = mSiteList;
		if (mSiteList != null && mSiteList.size() > 0) {
			bQuerySite = true;
		} else {
			bQuerySite = false;
		}

		this.bQueryExactSiteName = bQueryExactName;
	}

	// public List<Integer> getmYearList() {
	// return mYearList;
	// }
	//
	// public void setmYearList(List<Integer> mYearList) {
	// this.mYearList = mYearList;
	// }

	// /**
	// *
	// * @param regionNames
	// */
	// public void setRegionNames(List<Integer> in_regionIds) {
	// this.mRegionIds = in_regionIds;
	// // bQueryRegion = true;
	// bQuerySite = false;
	//
	// }

	public List<Integer> getmRegionIds() {
		return mRegionIds;
	}

	public void setmRegionIds(List<Integer> mRegionIds) {
		this.mRegionIds = mRegionIds;
		mSiteList = new ArrayList<String>();
		// also find the display region names using the IDs
		mRegionNameList = new ArrayList<String>();
		bQuerySite = false;

		mRegionNameList = regionsService.getRegionNamesByIds(mRegionIds);

		this.bAllAlberta = regionsService.isAllAlberta(mRegionIds);
	}

	// public List<String> getmRegionNameList() {
	//
	// return mRegionNameList;
	// }

	public String getRegionNameDisplay() {
		if (mRegionNameList == null || mRegionNameList.size() == 0)
			return "Alberta";
		String allRegion = mRegionNameList.toString();
		return allRegion.toString().substring(1, allRegion.length() - 1);
	}

	public static String SITEFIELD = "site";
	public static String ROTATIONFIELD = "rotation";

	// void addRequestProtocol(String protocolName) {
	// if (mRequestProtocols == null) {
	// mRequestProtocols = new ArrayList<String>();
	// mRequestProtocols.add(protocolName);
	// return;
	// }
	//
	// if (!mRequestProtocols.contains(protocolName)) {
	// mRequestProtocols.add(protocolName);
	// }
	// }

	public List<String> getmRequestProtocols() {
		return mRequestProtocols;
	}

	/**
	 * 
	 The spatial distribution of the data you requested is:
	 * 
	 * 1. Core site locations systematically distributed across the Alberta
	 * Province in a 1556 site grid, of which �. (fill with correct number) have
	 * been surveyed to date using terrestrial sampling protocols. 2. Off-grid
	 * site locations, especially selected across the province for their unique
	 * or otherwise important environmental characteristics, of which �. (fill
	 * with correct number) have been surveyed to date using terrestrial
	 * sampling protocols.
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param protocolType
	 *            "wetland terrestrial"
	 * @return
	 */
	public List<String> getSpatialInfo(String protocolType) {

		List<String> results = new ArrayList<String>();
		String selectedRegion = getRegionNameDisplay();
		// in grid
		
			
		if (this.mOffgridCode == ABMI_CORE_DATA || this.mOffgridCode == ABMI_CORE_AND_OFFGRID_DATA) {
			String tmp = "Core site locations systematically distributed across the "
					+ selectedRegion;

			HashMap<String, Integer> siteSurvey = (HashMap<String, Integer>) regionsService
					.getSiteInfo(this.getmRegionIds(), this.mRotationNameList,
							ABMI_CORE_DATA, protocolType.toLowerCase());

			tmp += " in a " + siteSurvey.get("total") + " site grid, of which "
					+ siteSurvey.get("sampled")
					+ " been surveyed to date using "
					+ protocolType.toLowerCase() + " sampling protocols.";

			
			results.add(tmp);
		}

		// offgrid
		if (this.mOffgridCode == ABMI_OFFGRID_DATA || this.mOffgridCode == ABMI_CORE_AND_OFFGRID_DATA) {
			String tmp = "Off-grid site locations, especially selected across "
					+ selectedRegion
					+ " for their unique or otherwise important environmental characteristics, of which ";

			HashMap<String, Integer> siteSurvey = (HashMap<String, Integer>) regionsService
					.getSiteInfo(this.getmRegionIds(), this.mRotationNameList,
							ABMI_OFFGRID_DATA, protocolType.toLowerCase());
			tmp += siteSurvey.get("sampled")
					+ " have been surveyed to date using "
					+ protocolType.toLowerCase() + " sampling protocols.";


			results.add(tmp);
		}

		if (this.mOffgridCode == ABMI_REVISITED_DATA) {
			
			String tmp = "Revisited core site locations systematically distributed across the "
					+ selectedRegion;

			HashMap<String, Integer> siteSurvey = (HashMap<String, Integer>) regionsService
					.getSiteInfo(this.getmRegionIds(), this.mRotationNameList,
							ABMI_REVISITED_DATA, protocolType.toLowerCase());

			tmp += " in a " + siteSurvey.get("total") + " site grid, of which "
					+ siteSurvey.get("sampled") + " been resurveyed to date using "
					+ protocolType.toLowerCase() + " sampling protocols.";


			results.add(tmp);
		}
		return results;

	}

	/*
	public List<String> getSpatialInfoOld(String protocolType) {

		List<String> results = new ArrayList<String>();

		String tmp = "Primarily across the " + getRegionNameDisplay();

		HashMap<String, Integer> siteSurvey = (HashMap<String, Integer>) regionsService
				.getSiteInfo(this.getmRegionIds(), this.mRotationNameList,
						this.getmOffgridCode(), protocolType.toLowerCase());

		tmp += " including "
				+ siteSurvey.get("total")
				+ " core ABMI sites, of which "
				+ siteSurvey.get("sampled")
				+ (this.mOffgridCode > 1 ? " (including "
						+ coreOffgridLabel[this.mOffgridCode - 1].toLowerCase()
						+ ")" : "") + " have been surveyed to date using "
				+ protocolType.toLowerCase() + " sampling protocols.";

		// System.out.println("spatial info=" + tmp);
		results.add(tmp);
		return results;

	}
	*/
}
