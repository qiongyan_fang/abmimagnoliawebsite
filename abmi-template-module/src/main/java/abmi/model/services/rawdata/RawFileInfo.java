package abmi.model.services.rawdata;

import java.io.File;

public class RawFileInfo {
	String fileName;
	boolean bToDelete;
	String status;
	
	String title;
	long totalRecord;
	File file;
	String protocolType;
	public String getProtocolType() {
		return protocolType;
	}


	public void setProtocolType(String protocolType) {
		this.protocolType = protocolType;
	}


	public RawFileInfo() {
		totalRecord = 0;
		status = "";
		bToDelete = false;
		title = "";
		file = null;
	}

	public void clear() {
		
		status = null;
		
		title = null;
		file = null;
	}

	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public File getFile() {
		return file;
	}


	public void setFile(File file) {
		this.file = file;
	}


	public boolean isbToDelete() {
		return bToDelete;
	}
	public void setbToDelete(boolean bToDelete) {
		this.bToDelete = bToDelete;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(long totalRecord) {
		this.totalRecord = totalRecord;
	}
	
	public String toString(){
		try{
			
			if (this.getFile() == null || this.getFile().getName() ==null){
				return this.getTitle() + " has " + this.getTotalRecord() + " rows";
			}
			
		return this.getTitle() + " (" + this.getFile().getName() + "-" 
				+ this.getFile().length()/1024 + "KB) has " + this.getTotalRecord() + " rows";
		}
		catch(Exception e){
			return this.getTitle() +  " " + this.getFile() +   " " + this.getTotalRecord();
			
		}
	}
}
