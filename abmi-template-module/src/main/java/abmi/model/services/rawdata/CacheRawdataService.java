package abmi.model.services.rawdata;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import abmi.model.entity.gis.RegionInfo;
import abmi.model.entity.internal.TableSynJob;
import abmi.model.entity.metadata.GCachecsv;
import abmi.model.entity.metadata.GCsvPrefix;
import abmi.model.entity.metadata.GRotation;
import abmi.model.entity.metadata.GViewobject;
import abmi.model.services.ProjectPropertyService;
import abmi.model.services.RegionsService;

/**
 * previous we have archived tables to cache rawdata files. this is redo class
 * in Spring. 1. rotation 2. regions (NR, LUF, WPAC) 3. In grid, offgrid, or
 * both 4. each protocols
 * 
 * saved in a cache table. this only runs on backend. can not skipped.
 * 
 * @author Qiongyan
 * 
 */

@Component
public class CacheRawdataService {
	private static final Logger log = LoggerFactory
			.getLogger(CacheRawdataService.class);
	@PersistenceContext(unitName = "data")
	EntityManager em;

	@Autowired
	ProjectPropertyService projectPropertyService;

	@Autowired
	RegionsService regionsService;

	@Autowired
	DataCategoryService dataCategoryService;
	@Autowired
	DownloadPackageGenerator zipFileDAO;

	@Autowired
	SingleCSVFileCreator csvGenerator;

	// SimpleDateFormat sdfull = new SimpleDateFormat("MMM dd yyyy hh:mm");

	public void setProjectPropertyService(
			ProjectPropertyService projectPropertyService) {
		this.projectPropertyService = projectPropertyService;
	}

	public void setRegionsService(RegionsService regionsService) {
		this.regionsService = regionsService;
	}

	public void setDataCategoryService(DataCategoryService dataCategoryService) {
		this.dataCategoryService = dataCategoryService;
	}

//	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
//	public void generateCSVFilexxxxxx() {
//		/**
//		 * step 1: get all rotations
//		 */
//		
//		
//		List<GCachecsv> newCsvList = new ArrayList<GCachecsv>();
//		for (GRotation rotationRow : dataCategoryService.getTestAllRotationList()) {
//			/**
//			 * step 2: get all regions
//			 */
//			for (RegionInfo regionRow : dataCategoryService
//					.getTestPickedRegionList()) {
//
//				if (regionRow.getRegionLayer().getRlId() != 5)
//					continue;

//				/**
//				 * step 3: get all regions
//				 */
//				for (GCsvPrefix coreOffgridRow : dataCategoryService
//						.getTestAllCoreOffGridList()) {
//
//					RawdataQueryCondition queryCondition = new RawdataQueryCondition();
//					queryCondition
//							.setProjectPropertyService(this.projectPropertyService); // autowired
//																						// not
//					// working on
//					// "new" object
//					queryCondition.setDataCategoryService(dataCategoryService);
//					queryCondition.setRegionsService(regionsService);
//					ArrayList<Integer> regionIDList = new ArrayList<Integer>();
//					regionIDList.add((int) regionRow.getRiId());
//
//					//
//					ArrayList<Integer> rotations = new ArrayList<Integer>();
//					rotations.add(rotationRow.getGrId()); // use
//															// rotation
//															// name
//															// for
//															// query
//					queryCondition.setmRotationList(rotations);
//					queryCondition.setOffgridId(coreOffgridRow.getGcpId());
//					queryCondition.setmRegionIds(regionIDList);
//
//					/**
//					 * step 4: for single protocol
//					 */
//					for (GViewobject tableRow : dataCategoryService
//							.getTestAllTableList()) {
//
//						if (tableRow.getGvTablename().startsWith("C")) {
//							continue;
//						}
//
//						SingleCSVFileCreator csvGenerator = new SingleCSVFileCreator(
//								em, this.projectPropertyService.getRawCsvPath());
//						csvGenerator.setTableId(tableRow.getGvId());
//
//						csvGenerator.setQueryCondition(queryCondition);
//						csvGenerator.getTempFile();
//						RawFileInfo fileInfo = csvGenerator.resultMap;
//						log.info(fileInfo);
//						if (fileInfo.getTotalRecord() > 0) {
//							GCachecsv newCsv = new GCachecsv();
//							newCsv.setGcFilepath(fileInfo.file.getName());
//							newCsv.setGcRotationId(rotationRow.getGrId());
//							newCsv.setGcRiId((int) regionRow.getRiId());
//							newCsv.setGcGcp(coreOffgridRow.getGcpId());
//							newCsv.setGcRecordcount(fileInfo.getTotalRecord());
//							// newCsv.setGcDate(sdfull.format( new Date()));
//							newCsvList.add(newCsv);
//							// works here but not work in saveOneRecord function
//							// saveOneRecord( newCsv);
//
//						} else {
//							log.info("empty file");
//						}
//						// break;
//					}
//					break;
//				}
//				break;
//			}
//			break;
//		}
//
//		for (GCachecsv row : newCsvList) {
//			System.out
//					.println("try to persist outside  " + row.getGcFilepath());
//			em.persist(row);
//
//			// saveOneRecord(row);
//		}
//	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void generateCSVFile2() {
		/**
		 * step 1: get all rotations
		 */
		List<GCachecsv> newCsvList = new ArrayList<GCachecsv>();
		/**
		 * step 4: for single protocol
		 */
		RawdataQueryCondition queryCondition = new RawdataQueryCondition();
		queryCondition.setProjectPropertyService(this.projectPropertyService); // autowired
																				// not
																				// working
																				// on
																				// "new"
																				// object
		queryCondition.setDataCategoryService(this.dataCategoryService);

		queryCondition.setRegionsService(this.regionsService);

		int i = 0;

		for (GViewobject tableRow : dataCategoryService.getTestAllTableList()) {
			i++;
			log.info("---------------------------------\ntableRow id = " + tableRow.getGvTablename());
			
			
			if ("RT_COARSE_WDS".equals(tableRow.getGvTablename()))
				continue;
			
			if (tableRow.getTableSynJobs2() == null || tableRow.getTableSynJobs2().size() == 0 
					|| ((TableSynJob)tableRow.getTableSynJobs2().get(0)).getTsjJobNumber() != 1
					) {
				continue;
			}
			log.info(" process ");
			
			for (GRotation rotationRow : dataCategoryService
					.getTestAllRotationList()) {
//				log.info(" rotationRow.getGrRotationName() =  " + rotationRow.getGrRotationName() );
				// skip prototype 
				if ( "Prototype".equals(rotationRow.getGrRotationName())) {
					continue;
				}
				
				if(rotationRow.getGrRotationName()!= null) {
					continue;
				}
				/**
				 * step 2: get all regions
				 */
				for (RegionInfo regionRow : dataCategoryService
						.getTestPickedRegionList()) {

					log.info("region id = " + regionRow.getRiName());
					
//					if (regionRow.getRiId() != null) // note: only for province
//						continue;
					// log.info("region id = "
					// +regionRow.getRegionLayer().getRlId());
					/**
					 * step 3: get all regions
					 */
					for (GCsvPrefix coreOffgridRow : dataCategoryService
							.getTestAllCoreOffGridList()) {
					
						/* there will be only offgrid for rotation1, not for prototype, not for rotation 2, or 3 etc.*/
						if ( coreOffgridRow.getGcpLabel().indexOf("Off-Grid Data") != -1 /* contains offgrid*/
								&&
								!"Rotation 1".equals(rotationRow.getGrRotationName())) {
							log.info("skip ");
							continue;
						}
						csvGenerator.init();
						csvGenerator.setTableId(tableRow.getGvId());
					

						csvGenerator.setQueryCondition(queryCondition);

						RawFileInfo fileInfo = null;

						try {
							fileInfo = csvGenerator.createSingleCacheRawData(
									tableRow.getGvId(), regionRow.getRiId(),
									rotationRow.getGrId(),
									coreOffgridRow.getGcpId());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						
						
						if (fileInfo == null || fileInfo.file == null || fileInfo.getTotalRecord() == 0) {
							log.info("empty file");
						} else {
							GCachecsv newCsv = new GCachecsv();
							newCsv.setGcFilepath(fileInfo.file.getName());
							newCsv.setGcRotationId(rotationRow.getGrId());
							newCsv.setGcRiId(regionRow.getRiId());
							newCsv.setGcGcp(coreOffgridRow.getGcpId());
							newCsv.setGcRecordcount(fileInfo.getTotalRecord());
							newCsv.setGcGvId(tableRow.getGvId());
							// newCsv.setGcDate(sdfull.format( new Date()));
							newCsvList.add(newCsv);
							log.info("rotation id=" + rotationRow.getGrId() + " regionId = " + regionRow.getRiId() + " offgridId" + coreOffgridRow.getGcpId()
									+ " gvId = " + tableRow.getGvId());
						}
						
					
						// break;
					}// end of offgrid in grid.
					
				}// end of rotation
				
			}// end of regions
			log.info("end of this table");
		}// end of tables

		for (GCachecsv row : newCsvList) {
			
			try {
//				em.persist(row);
			} catch (Exception e) {
				
				e.printStackTrace();
			}

			// saveOneRecord(row);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void testsaveSimple() {
		/**
		 * step 1: get all rotations
		 */

		/**
		 * step 4: for single protocol
		 */
		RawdataQueryCondition queryCondition = new RawdataQueryCondition();
		queryCondition.setProjectPropertyService(this.projectPropertyService); // autowired
																				// not
																				// working
																				// on
																				// "new"
																				// object
		queryCondition.setDataCategoryService(this.dataCategoryService);

		queryCondition.setRegionsService(this.regionsService);
		SingleCSVFileCreator csvGenerator = new SingleCSVFileCreator(em,
				this.projectPropertyService.getRawCsvPath());

		ArrayList<Integer> regionIDList = new ArrayList<Integer>();
		regionIDList.add(1);
		regionIDList.add(2);
		//
		ArrayList<Integer> rotations = new ArrayList<Integer>();
		rotations.add(1);
		rotations.add(2);

		queryCondition.setmRotationList(rotations);
		queryCondition.setOffgridId(3);
		queryCondition.setmRegionIds(regionIDList);
		csvGenerator.setQueryCondition(queryCondition);
		csvGenerator.setTableId(1);
		RawFileInfo fileInfo = null;

		try {
			fileInfo = csvGenerator.getFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		log.info(fileInfo.toString());
		// GCachecsv newCsv = new GCachecsv();
		// if (fileInfo.file == null){
		// newCsv.setGcFilepath(null);
		// }
		// else {
		// newCsv.setGcFilepath(fileInfo.file.getName());
		// }
		// newCsv.setGcRotationId(1);
		// newCsv.setGcRiId(1);
		// newCsv.setGcGcp(1);
		// newCsv.setGcRecordcount(fileInfo.getTotalRecord());
		// newCsv.setGcGvId(200);
		// // newCsv.setGcDate(sdfull.format( new Date()));
		// em.persist(newCsv);
		//

	}

	@Transactional
	// (readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void testsave() {
		/**
		 * step 1: get all rotations
		 */

		/**
		 * step 4: for single protocol
		 */
		RawdataQueryCondition queryCondition = new RawdataQueryCondition();
		queryCondition.setProjectPropertyService(this.projectPropertyService); // autowired
																				// not
																				// working
																				// on
																				// "new"
																				// object
		queryCondition.setDataCategoryService(this.dataCategoryService);

		queryCondition.setRegionsService(this.regionsService);
		// DownloadPackageGenerator downloadPackage = new
		// DownloadPackageGenerator(
		// em);
		ArrayList<Integer> regionIDList = new ArrayList<Integer>();
		regionIDList.add(1);

		//
		ArrayList<Integer> rotations = new ArrayList<Integer>();
		rotations.add(2);

		ArrayList<Integer> rawdata = new ArrayList<Integer>();
		rawdata.add(7);

		queryCondition.setmRotationList(rotations);
		queryCondition.setOffgridId(3);
		queryCondition.setmRegionIds(null);
		queryCondition.setbQuerySite(false);

		try {
			zipFileDAO.setGeneralQuery(queryCondition, rawdata, null);

			File zipFile = zipFileDAO.createZipFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

}
