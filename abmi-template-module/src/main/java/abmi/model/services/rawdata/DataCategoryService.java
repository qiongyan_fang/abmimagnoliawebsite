package abmi.model.services.rawdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import abmi.model.entity.gis.RegionInfo;
import abmi.model.entity.gis.RegionLayer;
import abmi.model.entity.metadata.GCsvPrefix;
import abmi.model.entity.metadata.GFileDownloadCategory;
import abmi.model.entity.metadata.GFileDownloadCategoryMain;
import abmi.model.entity.metadata.GRotation;
import abmi.model.entity.metadata.GViewobject;
import abmi.model.util.CommonUtil;


@Service
@Transactional(readOnly = true)
public class DataCategoryService {

	@PersistenceContext(unitName="data")
	EntityManager em;
	

	
	public DataCategoryService () {
		
	}
	public DataCategoryService (EntityManager em) {
		this.em = em;
	}
	
	/* **************************************************************
	 * 
	 *  for pdf display information 
	 *  
	 * ************************************************************** */
	public List<Integer> getRotationIdList(List<String> names) {
		TypedQuery<Integer> rotationQuery = em.createQuery(
				"Select p.grId from GRotation p where p.grRotationName in :name", Integer.class);
		
		rotationQuery.setParameter("name", names);
		return rotationQuery.getResultList();
	}
	
	public List<String> getRotationIdName(List<Integer> ids) {
		TypedQuery<String> rotationQuery = em.createQuery(
				"Select p.grRotationName from GRotation p where p.grId in :ids", String.class);
		
		rotationQuery.setParameter("ids", ids);
		return rotationQuery.getResultList();
	}
	

	public String getRotationIdName(Integer id) {
		GRotation row = em.find(GRotation.class, id);
		
		
		return row.getGrRotationName();
	}
	
	
	public String getRotationDisplayName(List<Integer> rotationList){
		if (rotationList == null || rotationList.size() == 0){
			return "all sampled years";
		}
		String rotationDisplay = getRotationIdName(rotationList).toString();
		return rotationDisplay.substring(0, rotationDisplay.length() -1);
		
	}
	
	/* **************************************************************
	 * 
	 *  for unit test
	 *  
	 * ************************************************************** */
	public List<GRotation> getTestAllRotationList() {
		TypedQuery<GRotation> rotationQuery = em.createQuery(
				"Select p from GRotation p", GRotation.class);
		
		List<GRotation> allRotation = rotationQuery.getResultList();
		GRotation allRot = new GRotation();
		allRot.setGrId(null);
		allRot.setGrRotationName(null);
		allRotation.add(allRot);
		return allRotation;
	}
	
	/**
	 * use 
	 * @return
	 */
	public List<GCsvPrefix> getTestAllCoreOffGridList() {
		TypedQuery<GCsvPrefix> query = em.createQuery(
				"Select p from GCsvPrefix p", GCsvPrefix.class);
		
		return query.getResultList();
	}
	
	public List<RegionInfo> getTestPickedRegionList() {
		TypedQuery<RegionInfo> query = em.createQuery(
				"Select p from RegionInfo p where p.regionLayer.rlPicked = 1", RegionInfo.class);
		
		List<RegionInfo> allRegion = query.getResultList();
		RegionInfo allRot = new RegionInfo();
		allRot.setRiId(null);
		allRot.setRiName("All Alberta");
		allRegion.add(allRot);
		return allRegion;
		
	}
	
	public List<GViewobject> getTestAllTableList() {
		TypedQuery<GViewobject> query = em.createQuery(
				"Select p from GViewobject p where p.gvProtocolCode in ('Terrestrial', 'Wetland') and p.gvTablename is not null  order by p.gvId", GViewobject.class);
		
		
		return query.getResultList();
	}


	/* **************************************************************
	 * 
	 *  for API 
	 *  
	 * ************************************************************** */
	
	/**
	 * get terrestrial/habitat and detailed variables
	 * @return
	 */
	public List<Map<String,Object>> getAPIAllProtocolVariable() {

		
		List<Map<String,Object>> rawSelects = new ArrayList<Map<String,Object>>();
		TypedQuery<GFileDownloadCategoryMain> query = em.createQuery(
				"select p from GFileDownloadCategoryMain p where p.gfdcmInclude = 1",
				GFileDownloadCategoryMain.class);

		for (GFileDownloadCategoryMain categoryItem : query.getResultList()) {
			
			Map<String,Object> category = new HashMap<String,Object>();
			category.put("protocol", categoryItem.getGfdcmProtocol());
			category.put("name", categoryItem.getGfdcmCategory());
			category.put("note", categoryItem.getGfdcmDescription());
			
			
			List<Map<String,Object>> categoryRow = new ArrayList<Map<String,Object>>();
			for(GFileDownloadCategory row: categoryItem.getGFileDownloadCategories()){
				Map<String,Object> rowDetail  = new HashMap<String,Object>();
				rowDetail.put("name", row.getGfdcTitle());
				rowDetail.put("id", row.getGfdcId());
				rowDetail.put("note", row.getGfdcDescription());
				rowDetail.put("last_year", row.getGfdcLastestYear());
				rowDetail.put("start_year", row.getGfdcStartYear());
				rowDetail.put("next_release", row.getGfdcNextReleaseDate());
				categoryRow.add(rowDetail);
			}
			
			category.put("detail", categoryRow);
			
			rawSelects.add(category);

		}
		
		return rawSelects;
	}
	
	
	/**get gcpCategory = 1 display which are on grid and offgrid
	 * get gcpCategory = 2 display which are mulitple visits
	 * @return
	 */
	public List<Map<String, Object>> getAPICoreOffgridOptions() {
		TypedQuery<GCsvPrefix> coreOffgridQuery = em.createQuery(
				"Select p from GCsvPrefix p where p.gcpCategory is not null", GCsvPrefix.class);
		
		List<Map<String,Object>> selects = new ArrayList<Map<String,Object>>();
		
		for (GCsvPrefix row : coreOffgridQuery.getResultList()) {
			Map<String,Object> coreOffGridOptions = new HashMap<String,Object>();
			coreOffGridOptions.put("id", row.getGcpId());
			coreOffGridOptions.put("name", row.getGcpLabel());
			coreOffGridOptions.put("category", row.getGcpCategory());
			coreOffGridOptions.put("note", row.getGcpComments());
			selects.add(coreOffGridOptions);
		}

		return selects;
	}

	
	/**
	 * 
	 * @return
	 */
	public List<Map<String, Object>> getAPIAllRegion() {
		TypedQuery<RegionLayer> query = em.createQuery(
				"Select p from RegionLayer p where p.rlPicked > 0 order by p.rlOrder", RegionLayer.class);
		
		List<Map<String,Object>> regionSelects = new ArrayList<Map<String,Object>>();
		
		for (RegionLayer regionRow: query.getResultList()){
			Map<String,Object> regionMap = new HashMap<String,Object>();
			regionMap.put("id", regionRow.getRlId());
			regionMap.put("name", regionRow.getRlName());
			regionMap.put("main", regionRow.getRlPicked());
			regionMap.put("coverWholeAlberta", regionRow.getRlWholeAlberta());
			regionMap.put("note", regionRow.getRlDescription());
			
			
			List<Map<String,Object>> subregionRow = new ArrayList<Map<String,Object>>();
			for(RegionInfo row: regionRow.getRegionInfos()){
				Map<String,Object> rowDetail  = new HashMap<String,Object>();
				rowDetail.put("name", row.getRiName());
				rowDetail.put("id", row.getRiId());
				if (row.getRegionParent() != null) { // link to another region, used for nature subregion, the parentid will link to the nature region
					rowDetail.put("parentid", row.getRegionParent().getRiId());
					rowDetail.put("parentname", row.getRegionParent().getRiName());
				}
				subregionRow.add(rowDetail);
			}
			
			regionMap.put("subregion", subregionRow);
			
			regionSelects.add(regionMap);
		}
		
		return regionSelects;
		
	}
	

	/**
	 * 
	 * @return
	 */
	public List<Map<String, Object>> getAPIRotationOptions() {
		List<Map<String,Object>> rotationSelects = new ArrayList<Map<String,Object>>();
		
		TypedQuery<GRotation> rotationQuery = em.createQuery(
				"Select p from GRotation p order by p.grOrder", GRotation.class);
		
		for (GRotation row : rotationQuery.getResultList()) {
			Map<String, Object> rotationRow = new HashMap<String, Object>();
			rotationRow.put("name" , row.getGrRotationName());
			rotationRow.put("years" , "(" + row.getGrStartYear() + " - " + row.getGrEndYear() +")");
			rotationRow.put("id" , row.getGrId());
			rotationRow.put("note" , row.getGrRotationDesc());
			
			rotationSelects.add(rotationRow);
					
		}

		return rotationSelects;
	}
	
	
	/* **************************************************************
	 * 
	 *  for csv file generation
	 *  
	 * ************************************************************** */
	
	public boolean isAllRotation(List<Integer> rotationList){
		
		if (rotationList == null || rotationList.size() == 0 )
			return true;
		
		
		TypedQuery<String> rotationQuery = em.createQuery(
				"Select p.grRotationName from GRotation p where p.grId not in :ids", String.class);
		
		rotationQuery.setParameter("ids", rotationList);
		return rotationQuery.getResultList().size() == 0 ? true:false;
	}
	
	
	/**this used to get the table prefixes for: all data, offgrid data, or core data.
	 * but this is expanded so, sites visited multiple times table prefix is also included.
	 * 
	 * @param offgridId 
	 * @return
	 */
	public String getTablePrefixByOffgridCode(Integer offgridId){
		TypedQuery<String >  query = em.createQuery("select p.gcpTablePrefix from GCsvPrefix p where p.gcpId = :id ", String.class);
		query.setParameter("id", offgridId);
		
		List<String> list= query.getResultList();
		

		for (String prefix:list){
			return CommonUtil.nvl(prefix);
		}
		

		return "";
	}
	
	/**
	 * get database table name for querying
	 * @param tableId
	 * @param offgridId
	 * @return
	 */
	public String getDBTableName(Integer tableId, Integer offgridId){
		GViewobject viewObj = em.find(GViewobject.class, tableId);
		
		GCsvPrefix offgridObj = em.find(GCsvPrefix.class, offgridId);
		
		if (offgridObj.getGcpTablePrefix() == null || "".equals(offgridObj.getGcpTablePrefix().trim()))
			return viewObj.getGvTablename();
		
		return offgridObj.getGcpTablePrefix()+ viewObj.getGvTablename();
	}
	
	/**
	 * 
	 * @param tableId
	 * @return
	 */
	public GViewobject getTableObj(Integer tableId){
		GViewobject viewObj = em.find(GViewobject.class, tableId);
		
		return viewObj;
	}
	
	/**
	 * get table file name 
	 * @param tableId
	 * @param offgridId
	 * @return
	 */
	public String getTableFileName(Integer tableId, Integer offgridId){
		GViewobject viewObj = em.find(GViewobject.class, tableId);
		
		GCsvPrefix offgridObj = em.find(GCsvPrefix.class, offgridId);
		

	
		if (offgridObj.getGcpTablePrefix() == null || "".equals(offgridObj.getGcpTablePrefix().trim()))
			return viewObj.getFileTitle();
		
		return offgridObj.getGcpTablePrefix()  + viewObj.getFileTitle();
	}
	
	
}
