package abmi.model.services.rawdata;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import abmi.model.entity.metadata.GFileDownloadCategory;
import abmi.model.entity.metadata.GProtocolDocument;
import abmi.model.entity.metadata.GViewobject;

import abmi.model.util.CommonUtil;
import abmi.model.util.ZipUtil;
import abmi.model.util.pdf.PDFEditor;

@Service
@Scope("session")
public class DownloadPackageGenerator implements Serializable {
	private static final long serialVersionUID = 1L;
	@PersistenceContext(unitName = "data")
	EntityManager em;
	List<Integer> tableList;
	ArrayList<String> selectedCategories;
	RawdataQueryCondition generalQuery;

	String fTempPath;
	List<ProtocolDocumentCollection> docCollection;

	@Autowired
	SingleCSVFileCreator fileCreator;

	// first get quested raw data and compiled lists
	private static final Logger log = LoggerFactory
			.getLogger(DownloadPackageGenerator.class);
	public DownloadPackageGenerator() {
	}

	public DownloadPackageGenerator(EntityManager em) {
		this.em = em;

	}

	/**
	 * pass in the list of category ids (such as bird, vascular plants, wetland
	 * water chemistry) and return the list of database tables (remove
	 * duplicates)
	 * 
	 * @return
	 * @throws Exception
	 */
	void setSelection(List<Integer> inclRawList, List<Integer> inclCompiledList)
			throws Exception {

		/* the array can't be empty, otherwise, the query will have an error */
		if (inclRawList == null || inclRawList.size() == 0) {
			inclRawList = new ArrayList<Integer>();
			inclRawList.add(new Integer(-9999));
		}

		if (inclCompiledList == null || inclCompiledList.size() == 0) {
			inclCompiledList = new ArrayList<Integer>();
			inclCompiledList.add(new Integer(-9999));
		}

		String rawQueryCategoryStr = "SELECT distinct p.gFileDownloadCategory FROM GRawTableMap p where "
				+ "( p.grtmRawCompiled  ='R'  and p.gFileDownloadCategory.gfdcId in :inclRawList) order by p.gFileDownloadCategory.gfdcOrder";
		TypedQuery<GFileDownloadCategory> rawQueryCategory = em.createQuery(
				rawQueryCategoryStr, GFileDownloadCategory.class);
		rawQueryCategory.setParameter("inclRawList", inclRawList);

		this.generalQuery.setmRawDownloadCategoryList(rawQueryCategory
				.getResultList());
		String compiledQueryCategoryStr = "SELECT distinct p.gFileDownloadCategory FROM GRawTableMap p where "
				+ " ( p.grtmRawCompiled  ='C'  and p.gFileDownloadCategory.gfdcId in :inclCompiledList) order by p.gFileDownloadCategory.gfdcOrder";

		TypedQuery<GFileDownloadCategory> compiledQueryCategory = em
				.createQuery(compiledQueryCategoryStr,
						GFileDownloadCategory.class);

		compiledQueryCategory
				.setParameter("inclCompiledList", inclCompiledList);
		this.generalQuery
				.setmCompiledDownloadCategoryList(compiledQueryCategory
						.getResultList());

		/**/
		String queryTableStr = "SELECT distinct p.gViewobject.gvId FROM GRawTableMap p where ( p.grtmRawCompiled  ='R'  and p.gFileDownloadCategory.gfdcId in :inclRawList) or "
				+ " ( p.grtmRawCompiled  ='C'  and p.gFileDownloadCategory.gfdcId in :inclCompiledList) order by p.gViewobject.gvOrder";

		TypedQuery<Integer> queryTable = em.createQuery(queryTableStr,
				Integer.class);

		queryTable.setParameter("inclRawList", inclRawList);
		queryTable.setParameter("inclCompiledList", inclCompiledList);

		tableList = queryTable.getResultList();

//		for (Integer id : CommonUtil.emptyIfNull(tableList)) {
//			log.debug("id=" + id);
//		}
		if (tableList.size() == 0) {
			tableList.add(-999);
		}

	}

	public void setGeneralQuery(RawdataQueryCondition in_generalQuery,
			List<Integer> inclRawList, List<Integer> inclCompiledList)
			throws Exception {
		this.generalQuery = in_generalQuery;
		this.setSelection(inclRawList, inclCompiledList);
	}

	/**
	 * using all table ids to generate all csv tables. terrestrial wetland files
	 * are done together.
	 * 
	 * @return
	 * @throws Exception
	 */
	// @Transactional
	public File createZipFile() throws Exception {

		try {
			this.fTempPath = this.generalQuery.projectPropertyService
					.getTempPath();
		} catch (Exception e) {
			this.fTempPath = System.getProperty("java.io.tmpdir");
		}

		Map<String, ArrayList<RawFileInfo>> fpList = null;
		fpList = createRawCSVFiles(); // new ArrayList<Map<String, Object>>();
										// //
		

		// pdf for each protocol location
		try {
			docCollection = this.collectPDFFile();

			this.packagePDF(fpList);
		} catch (Exception e) {
		}
		// additional pdf
		List<String> additionalFiles = this.generalQuery.projectPropertyService
				.getAdditionalDownloadFiles();

		// gather all files and zip them
		ArrayList<File> toZipFiles = new ArrayList<File>();
		List<File> toRemoveFiles = new ArrayList<File>();
		// 1. csv files,
	
		Iterator<Entry<String, ArrayList<RawFileInfo>>> it = fpList.entrySet()
				.iterator();
		while (it.hasNext()) {
			Entry<String, ArrayList<RawFileInfo>> pair = (Entry<String, ArrayList<RawFileInfo>>) it
					.next();

			for (RawFileInfo fp : CommonUtil.emptyIfNull(pair.getValue())) {
				if (fp.getFile() != null) {
					if (!toZipFiles.contains((File) fp.getFile())) {
						toZipFiles.add((File) fp.getFile());
					}

					if (fp.isbToDelete() == true) {
						toRemoveFiles.add(fp.getFile());
					}
				}
			}
		}

		// 2. pdf files

	

		for (ProtocolDocumentCollection doc : CommonUtil
				.emptyIfNull(docCollection)) {
			if (!toZipFiles.contains(doc.getFinalPdf())) {
				toZipFiles.add(doc.getFinalPdf());
			}
			toRemoveFiles.add(doc.getDynamicFrontPage());
			toRemoveFiles.add(doc.getFinalPdf());

		}
	
		// 3. additional files
		for (String fpName : CommonUtil.emptyIfNull(additionalFiles)) {
			if (!toZipFiles.contains(new File(
					this.generalQuery.projectPropertyService.getMethodPdfPath()
							+ fpName))) {
				toZipFiles.add(new File(
						this.generalQuery.projectPropertyService
								.getMethodPdfPath() + fpName));
			}
		}
	
		// 4. zip all files up
		File finalZipFile = ZipUtil.createZip(
				this.generalQuery.projectPropertyService, toZipFiles,
				"ABMIData");
		// clear tempoary files

		for (File fp : CommonUtil.emptyIfNull(toRemoveFiles)) {
			if (fp!= null && fp.exists()) {
				fp.delete();
			} else {
				log.error((fp == null ?"file " : fp.getName()) + " doesn't exists!");
			}
		}

		toRemoveFiles.clear();
		toRemoveFiles = null;
		additionalFiles.clear(); 

		toZipFiles.clear();
		docCollection.clear();

		fpList.clear();

		em.clear();
		return finalZipFile;
	}

	/**
	 * pass in the list of category ids (such as bird, vascular plants, wetland
	 * water chemistry) and return the list of database tables (remove
	 * duplicates)
	 * 
	 * @return
	 * @throws Exception
	 */

	public Map<String, ArrayList<RawFileInfo>> createRawCSVFiles() throws Exception {
		Map<String, ArrayList<RawFileInfo>> csvFileMaps = new HashMap<String, ArrayList<RawFileInfo>>();
		fileCreator.setfTempPath(fTempPath);
		fileCreator.setQueryCondition(this.generalQuery);
		for (Integer tableId : CommonUtil.emptyIfNull(tableList)) {
			// ArrayList<RawFileInfo> csvFiles = new ArrayList<RawFileInfo>();
			fileCreator.init();
			fileCreator.setTableId(tableId);
			try {
				RawFileInfo singleFile = fileCreator.getFile();
				if (csvFileMaps.containsKey(singleFile.getProtocolType())) {
					ArrayList<RawFileInfo> ownGroup = csvFileMaps.get(singleFile
							.getProtocolType());
					ownGroup.add(singleFile);
					csvFileMaps.put(singleFile.getProtocolType(), ownGroup);
				} else {
					ArrayList<RawFileInfo> ownGroup = new ArrayList<RawFileInfo>();
					ownGroup.add(singleFile);
					csvFileMaps.put(singleFile.getProtocolType(), ownGroup);
				}
			}catch(Exception err) { // if one file error, catch error and do next
				log.error("error create file " + tableId + "\n " + err.getMessage());
			}

//			
			
			// csvFiles.add(fileCreator.getFile());
		}
		return csvFileMaps;
	}

	/**
	 * PDF files: 1. part 1: will be dynamically created to show user
	 * selections, regions, time range, and results (generated files/rows) etc.
	 * 2. part 2: will be the description metadata files + general protocol pdf
	 * description pdfs. 3. part 2: individual metadata pdfs files;
	 * 
	 * pDf will be grouped by protocols (i.e. terrestrial, wetland, will have
	 * their own pdfs files)
	 * 
	 * @return
	 */
	List<ProtocolDocumentCollection> collectPDFFile() {
		List<String> protocolList = new ArrayList<String>();
		Map<String, List<String>> protocolDocMaps = new HashMap<String, List<String>>();

		String queryStr = "SELECT p FROM GViewobject p where  p.gvId in :tableList"
				+ " order by p.gvOrder";
		TypedQuery<GViewobject> query = em.createQuery(queryStr,
				GViewobject.class);

		query.setParameter("tableList", this.tableList);

		docCollection = new ArrayList<ProtocolDocumentCollection>();
		// get all method doc and metadata doc from view tables
		for (GViewobject row : CommonUtil.emptyIfNull(query.getResultList())) {
			ProtocolDocumentCollection updateRow = null;
			for (ProtocolDocumentCollection protocolRow : CommonUtil
					.emptyIfNull(docCollection)) {
				if (row.getGvProtocolCode().equals(
						protocolRow.getProtocolCode())) {
					updateRow = protocolRow;

				}
			}
			if (updateRow == null) {
				protocolList.add(row.getGvProtocolCode());
				updateRow = new ProtocolDocumentCollection();
				updateRow.setProtocolCode(row.getGvProtocolCode());
				docCollection.add(updateRow);
			}
			updateRow.addMetadataDocs(row.getGvMetadataPath());
			updateRow.addMethodDocs(row.getGvProtocolPath());

		}
		em.clear();

		/*
		 * get other static pdf for each protocol: front pdf, middle and
		 * appendices
		 */
//		log.debug("debug ---------------------------------\n + protcolList="
//						+ protocolList);

		TypedQuery<GProtocolDocument> docQuery = em.createQuery(
				"SELECT p FROM GProtocolDocument p where  p.gpdProtocolName  in :protocolList"
						+ " order by p.gpdCode, p.gpdDocOrder",
				GProtocolDocument.class);

		docQuery.setParameter("protocolList", protocolList);

		for (GProtocolDocument row : CommonUtil.emptyIfNull(docQuery
				.getResultList())) {

			for (ProtocolDocumentCollection protocolRow : docCollection) {

				if (row.getGpdProtocolName().equals(
						protocolRow.getProtocolCode())) {
					if ("FRONT".equals(row.getGpdDocLocation())) {
						protocolRow.addFrontDocs(row.getGpdDocPath());
					}

					if ("MIDDLE".equals(row.getGpdDocLocation())) {
						protocolRow.addMiddleDocs(row.getGpdDocPath());
					}

					if ("BACK".equals(row.getGpdDocLocation())) {
						protocolRow.addBackDocs(row.getGpdDocPath());
					}

				}
			}

		}
		em.clear();
		return docCollection;
	}

	/**
	 * 
	 * @param resultNoteList
	 *            list of files (name, size, no of rows)
	 * @param methodDocList
	 *            methdology pdf documents
	 * @param metaList
	 *            metadata pdf lists all individual fields
	 * @param coverList
	 *            front documents
	 * @param middleList
	 *            middle documents (e.g. explain DNC, VNA, etc)
	 * @param referenceList
	 *            reference and appendix
	 * @param requestList
	 * @param protocolType
	 *            protocol type: terrestrial, wetland, etc.
	 * @throws Exception
	 */
	List<String> packagePDF(Map<String, ArrayList<RawFileInfo>> csvFileMaps)
			throws Exception {

		try {
			List<String> pdfFiles = new ArrayList<String>();
			String xmlFormat = this.generalQuery.projectPropertyService
					.getPath("RAWDATA_DISCRIBER_ XML_TEMPLATE_PATH");
			String headerImage = this.generalQuery.projectPropertyService
					.getPath("RAWDATA_DISCRIBER_HEADER_IMAGE_PATH");
			PDFEditor pdfCreator = new PDFEditor();

			pdfCreator.setBPrototype(this.generalQuery.hasPrototype);

			// for each protocol location i.e terrestrial and wetland
			for (ProtocolDocumentCollection doc : CommonUtil
					.emptyIfNull(docCollection)) {
				// the request protocols (birds, plant will be separated by
				// protocol locations
				
//				ArrayList<RawFileInfo> singleGroup = new ArrayList<RawFileInfo>();

				pdfCreator
						.setDeliverList(csvFileMaps.get(doc.getProtocolCode()));

				pdfCreator.setRequestList(this.generalQuery.getRequestList(doc
						.getProtocolCode()));
				/* find spatial ranges */
				try {
					pdfCreator.setSpatialList(this.generalQuery
							.getSpatialInfo(doc.getProtocolCode()));
				} catch (Exception spatialErr) {
					spatialErr.printStackTrace();
				}

				File tmpPdfCover = pdfCreator.createPDFCover(
						doc.getProtocolCode(), xmlFormat, headerImage,
						this.fTempPath);

				doc.setDynamicFrontPage(tmpPdfCover);
				// get a temporary pdf file
				File tmpPDF = File.createTempFile(doc.getProtocolCode()
						+ "ABMIGuide", ".pdf", new File(this.fTempPath));

				doc.concatenatePfds(tmpPDF, tmpPdfCover,
						this.generalQuery.projectPropertyService
								.getMetadataPdfPath(),
						this.generalQuery.projectPropertyService
								.getMethodPdfPath());

				// // create a new pdf file from the list of pdffiles
				// PDFEditor.concatenatePDFs(pdfList, tmpPDF);
				// // add tmpPDF file for zipping
				// mToBeZippedList.add(tmpPDF);
				// // set tmpPDfCover for deletion
				// mToBeDeleteList.add(tmpPdfCover);
				// mToBeDeleteList.add(tmpPDF);

			}
			return pdfFiles;

		} catch (Exception ioe) {
			// LogUtil.out("Create Raw PDF Cover: " + ioe, "Exception");
			ioe.printStackTrace();
			throw new Exception("Create Raw PDF Cover: " + ioe);
		}

	}
}
