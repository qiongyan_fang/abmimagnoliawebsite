package abmi.model.services.rawdata;

//import abmi.controller.PublicationController;
//import abmi.model.entity.metadata.GArchive;
import abmi.model.entity.metadata.GCachecsv;

//import abmi.model.entity.metadata.GCsvPrefix;
//import abmi.model.entity.metadata.GHeader;
import abmi.model.entity.metadata.GViewobject;
//import abmi.model.services.ProjectPropertyService;
//import abmi.model.services.RegionsService;
//import abmi.model.util.CommonUtil;

import abmi.model.util.ModuleParameters;
import abmi.model.util.CommonUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultSetType;
import org.eclipse.persistence.queries.ScrollableCursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Component
@Scope("session") 
/**
 * 5 types of bean scopes supported :
1.singleton � Return a single bean instance per Spring IoC container (default)
2.prototype � Return a new bean instance each time when requested
3.request � Return a single bean instance per HTTP request. *
4.session � Return a single bean instance per HTTP session. *
5.globalSession � Return a single bean instance per global HTTP session. *


 *we need one for each http session
 *
 */
// must include this. otherwise rawdata controller will be called twice
public class SingleCSVFileCreator implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(unitName = "data")
	EntityManager em;

	private static final Logger log = LoggerFactory
			.getLogger(SingleCSVFileCreator.class);

	// static final String MetadataVO = "MetadataVO1";

	// String mViewObjectName;
	String fTableName;
	FileWriter mFw;
	BufferedWriter mBw;
	// PrintWriter mOut;
	RawdataQueryCondition queryObj;
	GViewobject tableObj;

	RawFileInfo resultMap;

	String fTempPath;

	public SingleCSVFileCreator() {
		resultMap = new RawFileInfo();
	};

	public SingleCSVFileCreator(EntityManager in_em, String tempPath) {
		resultMap = new RawFileInfo();
		//
		 this.em = in_em;
		 log.warn("set em value ");

		this.fTempPath = tempPath;
	}

	

	public void setfTempPath(String fTempPath) {
		this.fTempPath = fTempPath;
	}

	public void init() {
		resultMap = new RawFileInfo();
	}

	

	

	public void setQueryCondition(RawdataQueryCondition in_generalQuery
			) {

		this.queryObj = in_generalQuery;
		
	}

	public void setTableId(Integer viewObjectId) {

		tableObj = em.find(GViewobject.class, viewObjectId);

	}

	/**
	 * Maps: File: bToDelete: Status: Title: totalRecord:
	 * 
	 * @return either temporary files or archive files
	 */
	// @Transactional

	public RawFileInfo getFile() {

		resultMap.setTitle(tableObj.getGvTitle());
		resultMap.setProtocolType(tableObj.getGvProtocolCode());
		// first, find archived file if it is not specified as create tmp file
		// only
		// do not search archive files if users request by sites
		if (queryObj == null || queryObj.bQuerySite == false) { //

			if (this.getCacheFile()) {

				return resultMap;

			}
			log.debug("*** get cache file failed ");
		}

		// otherwise archived file doesn't exist, we need to creat them
		// applied for users query by specific sites not belong to any regions.

		getTempFile();
		log.debug("*** create temporary file " + resultMap);

		// return default tmpFile, as null

		return resultMap;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getCacheFile() {
		long totalRecord = 0;
		ArrayList<File> fileList = new ArrayList<File>();

		ArrayList<File> deleteFileList = new ArrayList<File>();
		// check all archived files link to this viewobject table

		List<Integer> regionIds = queryObj.getmRegionIds();
		if (regionIds == null || regionIds.size() == 0
				|| queryObj.isbAllAlberta()) {
			regionIds = new ArrayList<Integer>();
			regionIds.add(null);
		}

		List<Integer> rotationIds = queryObj.getmRotationList();
		if (rotationIds == null || rotationIds.size() == 0
				|| queryObj.isbAllYears()) {
			rotationIds = new ArrayList<Integer>();
			rotationIds.add(null);
		}

		for (Integer regionRow : regionIds) {
			for (Integer rotationRow : rotationIds) {
				String archFileName = "";
				long currentRecord = -1;
				try {

					TypedQuery<GCachecsv> query = em
							.createQuery(
									"select p from GCachecsv p where   p.gcGvId =:gvId and "
											+ "((:riId is null and p.gcRiId is null) or p.gcRiId = :riId) and "
											+ "((:grId is null and p.gcRotationId is null) or p.gcRotationId =:grId) and p.gcGcp =:coreOffgridId",
									GCachecsv.class);
					query.setParameter("riId", regionRow);
					query.setParameter("grId", rotationRow);
					query.setParameter("gvId", this.tableObj.getGvId());
					query.setParameter("coreOffgridId",
							queryObj.getmOffgridCode());
					GCachecsv row = query.getSingleResult();
					currentRecord = row.getGcRecordcount().longValue();

					archFileName = row.getGcFilepath();

				} catch (Exception err) {
					/** create new file */
					log.warn("failed on getting cache file {}",
							err.getMessage());

				}

				/**
				 * if record # > 0, and file not exist or it is null , create it
				 * now
				 */
				File archFile = new File(queryObj.getmArchPath() + archFileName);

				// the file can not be found, try Re-create a file 

				if (currentRecord == -1
						|| (currentRecord > 0 && (archFileName == null
								|| "".equals(archFileName) || archFile == null || !archFile
									.exists()))) {
					RawFileInfo fileInfo;
					log.debug("cache files can't be found " + archFileName);
					try {
						fileInfo = this.createSingleCacheRawData(
								tableObj.getGvId(), regionRow, rotationRow,
								queryObj.getmOffgridCode());
						totalRecord += fileInfo.getTotalRecord();
						fileList.add(fileInfo.getFile());
						if (fileInfo.bToDelete) {
							deleteFileList.add(fileInfo.getFile()); // don't
																	// remove
																	// cache
																	// files,
																	// only if
																	// failed to
																	// insert
																	// into db
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					fileInfo = null;
				} else if (currentRecord > 0) {
					fileList.add(archFile);
					totalRecord += currentRecord;
				} else {
					log.debug("no cache files");
				}

			}
		}

		if (fileList.size() == 0 && totalRecord > 0) {
			return false;
		}
		if (fileList.size() > 0 && totalRecord == 0) {
			return false;
		}

		/* contact collected files, remove headers except for the first file*/
		if (fileList.size() > 1) {
			try {
				File rawFile = CommonUtil.createTempFile(
						queryObj.getmTablePrefix() + tableObj.getFileTitle(),
						queryObj.projectPropertyService.getTempPath());
				CommonUtil.concatFilesSkipRow(fileList, rawFile);
				log.debug("rawFile = " + rawFile.getName());

				/* remove temporary files */
				try {
					for (File fp : deleteFileList) {

						try {
							fp.delete();
						} catch (Exception err) {
						}
					}
				} catch (Exception err2) {
				}
				resultMap.setFile(rawFile);
				resultMap.setbToDelete(true);
			} catch (Exception ioerr) {
				ioerr.printStackTrace();
				return false;
			}

		} else if (fileList.size() == 1) {
			File newFile = fileList.get(0);
			resultMap.setFile(newFile);
			if (deleteFileList.contains(newFile)) {
				resultMap.setbToDelete(true);
			} else {
				resultMap.setbToDelete(false);
			}
		}
		resultMap.setTotalRecord(totalRecord);
		fileList.clear();
		return true;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public void getTempFile() {
		try {

			File mTmpFile = createRawData();
			// by any reason, this viewobject can't create a file, fail it

			resultMap.setFile(mTmpFile);
			resultMap.setbToDelete(true);

		} catch (Exception e) {
			resultMap.setStatus("Output to File Failed");
			resultMap.setFile(null);
			resultMap.setTotalRecord(0);
			e.printStackTrace();

		}
	}

	/**
	 * create one csv file using the parameters store in QueryObject, different
	 * form create single cache file, this one combines mulitple regions,
	 * rotation info. the fils will not be cached.
	 * 
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
	public File createRawData() throws Exception {

		long start_time = System.currentTimeMillis();

		File rawFile = null;
		try {
			// get ViewObjects
			//

			fTableName = queryObj.getmTablePrefix() + tableObj.getGvTablename();

			String listSql = " select * from csvdownload." + fTableName;
			log.debug(listSql);
			String subSql = "";
			// query by siteList
			if (queryObj.getmSiteList() != null
					&& queryObj.getmSiteList().size() > 0) {

				subSql += " where ";

				/* if only core sites required, just use exact match */
				if (this.queryObj.getmOffgridCode() == 1) {
					subSql += CommonUtil.createSqlFromArrayList(
							RawdataQueryCondition.SITEFIELD,
							queryObj.getmSiteList(), "'");
				} else {
					/*
					 * if offgrid data required, extract the core site name from
					 * offgrid label 'OG-ABMI-###-1'
					 */
					if (!this.queryObj.bQueryExactSiteName) { // query by
																// nearest
																// public
																// location
						subSql += CommonUtil
								.createSqlFromArrayList(
										"case when site like '%-%' then substr(site, instr(site,'-',1,2) +1, instr(site,'-',1,3) - instr(site, '-',1,2) -1) else site end ",
										queryObj.getmSiteList(), "'");
					} else { // query by exact site name
						subSql += CommonUtil.createSqlFromArrayList(
								RawdataQueryCondition.SITEFIELD,
								queryObj.getmSiteList(), "'");
					}
				}
			}

			// set rotation list
			if (queryObj.getmRotationList() != null
					&& queryObj.getmRotationList().size() > 0) {
				if (!subSql.equals("")) {
					subSql += " and ";
				} else {
					subSql += " where ";
				}

				// query by rotation Name
				subSql += CommonUtil.createSqlFromArrayList(
						RawdataQueryCondition.ROTATIONFIELD,
						queryObj.mRotationNameList, "'");
			}

			
			listSql = listSql + " " + subSql;

			
			Query query = em.createNativeQuery(listSql);
			query.setHint("eclipselink.cursor.scrollable", true);
			// query.setHint("eclipselink.read-only", true);

			ScrollableCursor scrollableCursor = (ScrollableCursor) query
					.getSingleResult();

			String filename = queryObj.mTablePrefix
					+ this.tableObj.getFileTitle(); // remove
													// the
													// last
			// VO1 from file
			// names
			// CommonUtil utilDao = new CommonUtil();
			rawFile = CommonUtil.createTempFile(filename, fTempPath);
			/* create file writer handler for storing raw data */
			mFw = new FileWriter(rawFile);
			mBw = new BufferedWriter(mFw);
			// mOut = new PrintWriter(mBw);

			mBw.write(tableObj.getGHeaders().getGhHeader() + "\n");

			int nRecords = 0;
			while (scrollableCursor.hasNext()) {
				Object[] rowValue = (Object[]) scrollableCursor.next();

				String line = "";
				for (Object rowStr : rowValue) {

					try {
						if (rowStr.toString().indexOf(",") != -1)
							line += "\"" + rowStr + "\",";
						else
							line += rowStr + ",";
					} catch (Exception e) {
						line += rowStr + ",";
					}
				}
				line += "\n";
				mBw.write(line);

				if (nRecords % 100 == 0) {
					em.clear();

				}
				nRecords++;

			}

			scrollableCursor.clear();
			scrollableCursor.close();
			em.clear();

			resultMap.setTotalRecord(nRecords);

			long end_time = System.currentTimeMillis();
			if (fTableName.startsWith("RT_Site")
					|| fTableName.startsWith("RT_Breed")) {
				log.debug("ABMI test: time spent on creating " + fTableName
						+ " is " + (end_time - start_time) + " ms");
			}
			return rawFile;

		} catch (Exception e) {

			try {

				// delete unsuccessfully created files
				if (rawFile != null && rawFile.exists()) {
					rawFile.delete();
				}
			} catch (Exception ioe) {
				log.error("fail to remove temp file: " + ioe.getMessage());
			}

			e.printStackTrace();
			throw new Exception("\nerror exception on creating raw File("
					+ fTableName + "):" + e);
		} finally {
			closeFile();
		}

	}

	/**
	 * this method creates a csv file for one region and one rotation. (regionid
	 * and rotationid can be null, i
	 * 
	 * @return
	 * @throws Exception
	 */
	// @Transactional(readOnly = false)
	public void persistRow(GCachecsv row) {

		/*
		 * this is not ideal, but I can't make the transactional work It worked
		 * on testing environment, but in running environment, it gives me a no
		 * transaction is active error.
		 * 
		 * I used the same setting for both environment, but didn't work. so I
		 * end up using the ugly way.
		 */
		try {
			EntityManager tempEM = (EntityManager) Persistence
					.createEntityManagerFactory(
							ModuleParameters.DATA_ENTITY_MANAGER)
					.createEntityManager();
			tempEM.getTransaction().begin();
			System.out.println("row =" + row.getGcId());
			tempEM.persist(row);
			tempEM.getTransaction().commit();
			tempEM.close();
		} catch (Exception ioe) {
			log.error(ioe.getLocalizedMessage());
		}

		// try {
		// em.getTransaction().begin();
		// em.persist(row);
		// log.warn("persist " );
		// em.flush();
		// log.warn("flush " );
		// em.getTransaction().commit();
		// } catch (Exception ioe) {
		// ioe.printStackTrace();
		// }
	}

	// @Transactional(readOnly = false)
	public void persistRowNeedInject(GCachecsv row) {

		try {
			

			em.persist(row);

		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
	}

	// @Transactional
	/**
	 * using injected entity manager will cause memory leak. (em.clear, etc
	 * can't clear memory).
	 * 
	 * @param tableId
	 * @param regionId
	 * @param rotationId
	 * @param offgridCoreId
	 * @return
	 * @throws Exception
	 */

	@Transactional(value = "myTxManager", readOnly = true, propagation = Propagation.REQUIRES_NEW)
	public RawFileInfo createSingleCacheRawData(Integer tableId,
			Integer regionId, Integer rotationId, Integer offgridCoreId)
			throws Exception {

		// EntityManager tempEM = (EntityManager) Persistence
		// .createEntityManagerFactory(
		// ModuleParameters.DATA_ENTITY_MANAGER)
		// .createEntityManager();

		long start_time = System.currentTimeMillis();
		RawFileInfo result = new RawFileInfo();
		File rawFile = null;

		GViewobject viewObj = this.queryObj.dataCategoryService
				.getTableObj(tableId);
		try {
			// get ViewObjects

			// fTableName = queryObj.getmTablePrefix() +
			// tableObj.getGvTablename();

			String listSql = " select * from csvdownload."
					+ this.queryObj.dataCategoryService.getDBTableName(tableId,
							offgridCoreId);
			// LogUtil.debug(listSql);
			String subSql = "";
			// query by siteList
			List<Integer> siteList = this.queryObj.regionsService
					.getCoreSiteListFromPublicRegions(regionId);
		
			
			if (siteList != null && siteList.size() > 0) {

				subSql += " where ";

				/* if only core sites required, just use exact match */
				if (offgridCoreId == 1) {
					subSql += CommonUtil.createSqlFromArrayList(
							RawdataQueryCondition.SITEFIELD, siteList, "'");
				} else {
					/*
					 * if offgrid data required, extract the core site name from
					 * offgrid label 'OG-ABMI-###-1'
					 */

					subSql += CommonUtil
							.createSqlFromArrayList(
									"case when site like '%-%' then substr(site, instr(site,'-',1,2) +1, instr(site,'-',1,3) - instr(site, '-',1,2) -1) else site end ",
									siteList, "'");
				}
			}

			
			// set rotation list
			if (rotationId != null) {
				if (!subSql.equals("")) {
					subSql += " and ";
				} else {
					subSql += " where ";
				}

				// query by rotation Name
				subSql += RawdataQueryCondition.ROTATIONFIELD
						+ "= '"
						+ this.queryObj.dataCategoryService
								.getRotationIdName(rotationId) + "'";
			}

			listSql = listSql + " " + subSql;

			// 
			Query query = em.createNativeQuery(listSql);
			
			query.setHint(QueryHints.RESULT_SET_TYPE, ResultSetType.ForwardOnly);

			query.setHint("eclipselink.cursor.scrollable", true);
			// query.setHint("eclipselink.read-only", true);-- can't be
			// used here

			ScrollableCursor scrollableCursor = (ScrollableCursor) query
					.getSingleResult();

			

			String filename = this.queryObj.dataCategoryService.getTableFileName(
					tableId, offgridCoreId).replaceAll(" ", "")
					+ (rotationId == null ? "AllYear" : "Rotation" + rotationId)
					+ "_"
					+ (regionId == null ? "AB" : "Region" + regionId)
					+ "_"; // remove
			// the
			// last
			// VO1 from file
			// names

			Runtime rt = Runtime.getRuntime();
			long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
			log.debug("memory usage 1: " + usedMB);
			rawFile = CommonUtil.createTempFile(filename,
					this.queryObj.projectPropertyService.getRawCsvPath());
			/* create file writer handler for storing raw data */
			mFw = new FileWriter(rawFile);
			mBw = new BufferedWriter(mFw);
			// mOut = new PrintWriter(mBw, true);

			mBw.write(viewObj.getGHeaders().getGhHeader() + "\n");

			
			int nRecords = 0;
			while (scrollableCursor.hasNext()) {
				Object[] rowValue = (Object[]) scrollableCursor.next();

				String line = "";
				for (Object rowStr : rowValue) {

					try {
						if (rowStr.toString().indexOf(",") != -1)
							line += "\"" + rowStr + "\",";
						else
							line += rowStr + ",";
					} catch (Exception e) {
						line += rowStr + ",";
					}
				}
				line += "\n";
				mBw.write(line);

				if (nRecords % 100 == 0) {
					
					em.clear();

				}
				nRecords++;
				rowValue = null;
				line=null;
			}

			long usedMB2 = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
			log.debug("memory usage 2: " + usedMB2);
			log.debug(listSql + " " + nRecords);

			scrollableCursor.clear();

			scrollableCursor.close();

			em.clear();
			// tempEM.close();

			result.setTotalRecord(nRecords);

			if (nRecords == 0) {

				result.setFile(null);
				return result;
			}
			//
			// for (Object[] singleRow : CommonUtil.emptyIfNull(resultList)) {
			// String line = "";
			// for (Object rowStr : singleRow) {
			//
			// try{
			// if (rowStr.toString().indexOf(",")!= -1)
			// line += "\"" + rowStr + "\",";
			// else
			// line += rowStr + ",";
			// }catch(Exception e){
			// line += rowStr + ",";
			// }
			// }
			// line += "\n";
			//
			// mBw.write(line); // test not to write into files
			//
			//
			// }
			//
			// closeFile();

			long end_time = System.currentTimeMillis();
			log.debug("ABMI test: time spent:" + (end_time - start_time)
					+ " ms");

			result.setFile(rawFile);
			/** clear object **/
			query = null;

			GCachecsv newCsv = new GCachecsv();
			if (result.file == null) {
				newCsv.setGcFilepath(null);
			} else {
				newCsv.setGcFilepath(result.file.getName());
			}
			newCsv.setGcRecordcount(result.getTotalRecord());

			newCsv.setGcRotationId(rotationId);
			newCsv.setGcRiId(regionId);
			newCsv.setGcGcp(offgridCoreId);
			newCsv.setGcGvId(tableId);

			try {
				persistRow(newCsv);

			} catch (Exception e) {
				log.error("error on inserting a new record in cache table "
						+ e.getMessage());
				result.setbToDelete(true);
			}
			newCsv = null;
			return result;

		} catch (Exception e) {

			// delete unsuccessfully created files
			if (rawFile != null && rawFile.exists()) {
				rawFile.delete();
			}

			e.printStackTrace();
			throw new Exception("\nerror exception on creating raw File("
					+ fTableName + "):" + e);
		} finally {
			em.clear();
			try {
				closeFile();
			} catch (IOException ioe) {
				throw new Exception("close file error Exception "
						+ ioe.getMessage());
			}

		}

	}

	/**
	 * close file handlers
	 * 
	 * @throws IOException
	 */
	private void closeFile() throws IOException {
		try {
			// finish writing

			//
			// if (mOut != null) {
			// mOut.flush();
			//
			// mOut.close();
			// }
			if (mBw != null) {

				mBw.close();
			}

			if (mFw != null) {

				mFw.close();
			}
			//

			// mOut = null;
			mBw = null;
			mFw = null;
		} catch (IOException e) {
			e.printStackTrace();
			throw new IOException("error close rawdata file " + e.getMessage());
		}
	}

}
