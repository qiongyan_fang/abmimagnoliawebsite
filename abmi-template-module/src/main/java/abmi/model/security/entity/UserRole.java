package abmi.model.security.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the USER_ROLES database table.
 * 
 */
@Entity
@Table(schema="LABDATA",name="USER_ROLES")
@SequenceGenerator(schema="LABDATA", name="UR_SEQ", initialValue=100, allocationSize=1)
public class UserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="UR_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UR_SEQ")
	private long urId;

	@Column(name="ROLE_TYPE")
	private String roleType;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="US_ID")
	private User user;

	public UserRole() {
	}

	public long getUrId() {
		return this.urId;
	}

	public void setUrId(long urId) {
		this.urId = urId;
	}

	public String getRoleType() {
		return this.roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}