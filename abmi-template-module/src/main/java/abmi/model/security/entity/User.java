package abmi.model.security.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;




import java.util.ArrayList;

import java.util.List;


/**
 * The persistent class for the USERS database table.
 * 
 */
@Entity
@Table(schema="LABDATA",name="USERS")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UR_SEQ")
	private Long Id;

	
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	
	@Column(name="USER_ID",unique=true)
	
	private String userId;

	@Column(name="USER_ENABLED")
	private Integer userEnabled;

	@Column(name="USER_NAME")
	private String userName;

	@Column(name="USER_PASSWORD")
	private String userPassword;

	//bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy="user")
//	@JoinColumn(name="ID", insertable=true, updatable=true)
   //@MapKeyColumn(name="UR_ID") 
	private List<UserRole> userRoles;

	@Column(name="USER_GROUP")
	private String userGroup;
	public String getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}

	public User() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getUserEnabled() {
		return this.userEnabled;
	}

	public void setUserEnabled(Integer userEnabled) {
		this.userEnabled = userEnabled;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public List<UserRole> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	
	/*Custom*/
	public List<GrantedAuthority> getGrantedRoles(){
		List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
		for (UserRole role:this.userRoles ){
			roles.add(new SimpleGrantedAuthority(role.getRoleType()));
		}
		
		return roles;
	}
	
	
	public boolean hasRole (String roleType){
			if (roleType==null){
				return false;
			}
			
			for (UserRole role:getUserRoles()){
				if (roleType.equalsIgnoreCase(role.getRoleType())){
					return true;
				}
			}
			
			return false;
	}
	
	public String getuserRoleString(){
		String roleString=",";
		for (UserRole role:this.getUserRoles()){
			roleString += role.getRoleType() +",";
		}
		return roleString.substring(1);
	}
	
	
	
}