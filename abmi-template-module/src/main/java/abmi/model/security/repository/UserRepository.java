package abmi.model.security.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;


import abmi.model.security.entity.User;


public interface UserRepository extends JpaRepository<User, Long> {
	@Transactional(value = "mySecurityTxManager", readOnly = true) 
	@Query("SELECT p from User p where p.userId=:userId")
	User findByUserId(String userId); /* match name of userName cases need to be matched*/
}
