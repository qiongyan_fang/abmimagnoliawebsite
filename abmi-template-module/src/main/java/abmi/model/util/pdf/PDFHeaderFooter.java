package abmi.model.util.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Demonstrates the use of templates to add Watermarks and Pagenumbers.
 */
public class PDFHeaderFooter extends PdfPageEventHelper {
	public PDFHeaderFooter() {
	}

	/** An Image that goes in the header. */
	public Image headerImage, footImage;

	/** The headertable. */
	public PdfPTable table;

	/** The Graphic state */
	public PdfGState gstate;

	/** A template that will hold the total number of pages. */
	public PdfTemplate tpl;

	/** The font that will be used. */
	public BaseFont helv;

	public String fHeaderImgs = "ABMILetterHeaderOrg.jpg";
	public String fFootImgs = null;
	int imageScale = 26;

	/**
	 * Generates a document with a header containing Page x of y and with a
	 * Watermark on every page.
	 * 
	 * @param args
	 *            no arguments needed
	 */

	/**
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onOpenDocument(PdfWriter writer, Document document) {
		try {

			// initialization of the header table

			headerImage = Image.getInstance(fHeaderImgs);
			headerImage.setAlignment(Image.ALIGN_MIDDLE);
			headerImage.scalePercent((int) (50000f / headerImage.getWidth()));

			// foot image
			if (fFootImgs != null) {
				footImage = Image.getInstance(fFootImgs);
				footImage.setAlignment(Image.ALIGN_MIDDLE);
				footImage.scalePercent(this.imageScale);
			} else {
				footImage = null;
			}
			//
			// table = new PdfPTable(1);
			// table.setWidthPercentage(100);
			// table.getDefaultCell().setBorderWidth(1);
			// table.addCell(new Phrase(new Chunk(headerImage, 0, 0)));
			tpl = writer.getDirectContent().createTemplate(100, 100);
			tpl.setBoundingBox(new Rectangle(-20, -20, 100, 100));

			helv = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI,
					false);
		} catch (Exception e) {
			e.printStackTrace();
			// throw new ExceptionConverter(e);
		}
	}

	/**
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onEndPage(PdfWriter writer, Document document) {
	
		try {

			// int left =
			// (int)(0.5 * (document.getPageSize().getWidth() -
			// headerImage.getWidth() *
			// 0.25));
			// int top =
			// (int)(document.getPageSize().getHeight() - 20 -
			// headerImage.getHeight() *
			// 0.25);

			int left = (int) (0.5 * (document.getPageSize().getWidth() - 500));
			int top = (int) (document.getPageSize().getHeight() - 115);
			headerImage.setAbsolutePosition(left, top);
			document.add(headerImage);
			// cb.addImage( headerImage);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if (footImage != null) {
				int left = 0;
				// (int)(0.5 * (document.getPageSize().getWidth() -600));
				int top = 0;
				// (int)( footImage.getHeight()*this.imageScale*0.01);
				footImage.setAbsolutePosition(left, top);
				document.add(footImage);
			}
			// cb.addImage( headerImage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	

		return;
		/*
		 * the page number disabled for 2009 // compose the footer String text =
		 * "Page " + writer.getPageNumber(); // + " of "; float textSize =
		 * helv.getWidthPoint(text, 12); float textBase = document.bottom() -
		 * 20; cb.beginText(); cb.setFontAndSize(helv, 10);
		 * 
		 * // for odd pagenumbers, show the footer at the left if
		 * ((writer.getPageNumber() & 1) == 1) {
		 * cb.setTextMatrix(document.left(), textBase); cb.showText(text);
		 * cb.endText(); cb.addTemplate(tpl, document.left() + textSize,
		 * textBase); } // for even numbers, show the footer at the right else {
		 * float adjust = helv.getWidthPoint("0", 12);
		 * cb.setTextMatrix(document.right() - textSize - adjust, textBase);
		 * cb.showText(text); cb.endText(); cb.addTemplate(tpl, document.right()
		 * - adjust, textBase); }
		 * 
		 * cb.restoreState();
		 */

	}

	/**
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onStartPage(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onStartPage(PdfWriter writer, Document document) {
		// don't use this, otherwise, you will get an error on begintext and
		// endtext
		// PdfContentByte cb = writer.getDirectContent();
		// cb.saveState();

	}

	/**
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onCloseDocument(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onCloseDocument(PdfWriter writer, Document document) {
		// tpl.beginText();
		// tpl.setFontAndSize(helv, 12);
		// tpl.setTextMatrix(0, 0);
		// tpl.showText("" + (writer.getPageNumber() - 1));
		// tpl.endText();
		if ((writer.getPageNumber() & 1) == 1) {
			// document.newPage();
		}
	}

	public void setImageScale(int imageScale) {
		this.imageScale = imageScale;
	}

	public void setFHeaderImgs(String fHeaderImgs) {
		this.fHeaderImgs = fHeaderImgs;
	}

	public void setFFootImgs(String fFootImgs) {
		this.fFootImgs = fFootImgs;
	}

}
