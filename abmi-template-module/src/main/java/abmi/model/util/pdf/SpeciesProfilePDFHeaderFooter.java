package abmi.model.util.pdf;


import abmi.model.services.biobrowser.PdfDownloadStyles;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Demonstrates the use of templates to add Watermarks and Pagenumbers.
 */
public class SpeciesProfilePDFHeaderFooter extends PdfPageEventHelper {
	
	PdfDownloadStyles styleObj;
	
	public PdfDownloadStyles getStyleObj() {
		return styleObj;
	}

	public void setStyleObj(PdfDownloadStyles styleObj) {
		this.styleObj = styleObj;
	}

	public SpeciesProfilePDFHeaderFooter() {
	}

	/** An Image that goes in the header. */
	public String headerImage, footImage;


	
	public String headerText ="", headerBannerText="";
	public void setHeaderImage(String headerImage) {
		this.headerImage = headerImage;
	}

	public void setFootImage(String footImage) {
		this.footImage = footImage;
	}

	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}

	public void setHeaderBannerText(String headerBannerText) {
		this.headerBannerText = headerBannerText;
	}

	public void setFooterText(String footerText) {
		this.footerText = footerText;
	}

	public String footerText ="";
	

	/**
	 * Generates a document with a header containing Page x of y and with a
	 * Watermark on every page.
	 * 
	 * @param args
	 *            no arguments needed
	 */

	/**
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onOpenDocument(PdfWriter writer, Document document) {
		
	}

	/**
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onEndPage(PdfWriter writer, Document document) {
		if (writer.getPageNumber() == 1) {
			

		}
		else {
			PdfContentByte cb = writer.getDirectContent();
			
			Phrase header = new Phrase(this.headerText, styleObj.headerFooterFont);
			try{
			ColumnText.showTextAligned(cb, Element.ALIGN_LEFT, header,
					30, 760, 0);
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		/*
		try {
			if (footImage != null) {
				int left = 550;
				// (int)(0.5 * (document.getPageSize().getWidth() -600));
				int top = 40;
				// (int)( footImage.getHeight()*this.imageScale*0.01);
				Image img = Image.getInstance(this.footImage);
				img.setAbsolutePosition(left, top);
				document.add(img);
			}
			Phrase footerText = new Phrase(this.footerText, styleObj.headerFooterFont);
			ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, footerText,
					600, 30, 0);
			
			// cb.addImage( headerImage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/

		return;
		/*
		 * the page number disabled for 2009 // compose the footer String text =
		 * "Page " + writer.getPageNumber(); // + " of "; float textSize =
		 * helv.getWidthPoint(text, 12); float textBase = document.bottom() -
		 * 20; cb.beginText(); cb.setFontAndSize(helv, 10);
		 * 
		 * // for odd pagenumbers, show the footer at the left if
		 * ((writer.getPageNumber() & 1) == 1) {
		 * cb.setTextMatrix(document.left(), textBase); cb.showText(text);
		 * cb.endText(); cb.addTemplate(tpl, document.left() + textSize,
		 * textBase); } // for even numbers, show the footer at the right else {
		 * float adjust = helv.getWidthPoint("0", 12);
		 * cb.setTextMatrix(document.right() - textSize - adjust, textBase);
		 * cb.showText(text); cb.endText(); cb.addTemplate(tpl, document.right()
		 * - adjust, textBase); }
		 * 
		 * cb.restoreState();
		 */

	}

	/**
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onStartPage(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onStartPage(PdfWriter writer, Document document) {
		// don't use this, otherwise, you will get an error on begintext and
		// endtext
		// PdfContentByte cb = writer.getDirectContent();
		// cb.saveState();

	}

	/**
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onCloseDocument(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onCloseDocument(PdfWriter writer, Document document) {
		// tpl.beginText();
		// tpl.setFontAndSize(helv, 12);
		// tpl.setTextMatrix(0, 0);
		// tpl.showText("" + (writer.getPageNumber() - 1));
		// tpl.endText();
		if ((writer.getPageNumber() & 1) == 1) {
			// document.newPage();
		}
	}

	
	
}
