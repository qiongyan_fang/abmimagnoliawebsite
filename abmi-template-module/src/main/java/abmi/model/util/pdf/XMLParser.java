package abmi.model.util.pdf;

import java.io.*;
import java.util.Vector;
import javax.xml.parsers.*;
import org.xml.sax.*;

import org.w3c.dom.*;

public class XMLParser {
	String result = new String();

	String filename = new String();
	// Constructor..
	// 1. File Name of the File To Be Parsed
	Vector vResult = new Vector();
	String tmpResult[] = new String[2];
	boolean bInit = true;

	public XMLParser(String filename) {
		this.filename = filename;
	}

	public Vector parser() {

		// Step 1: create a DocumentBuilderFactory and configure it
		DocumentBuilderFactory documentFactory;
		documentFactory = DocumentBuilderFactory.newInstance();

		// Optional: set various configuration options
		documentFactory.setValidating(false);
		documentFactory.setIgnoringComments(true);
		documentFactory.setIgnoringElementContentWhitespace(true);
		documentFactory.setNamespaceAware(false);
		// The opposite of creating entity ref nodes is expanding them inline
		documentFactory.setExpandEntityReferences(true);

		// At this point the DocumentBuilderFactory instance can be saved
		// and reused to create any number of DocumentBuilder instances
		// with the same configuration options.

		// Step 2:create a DocumentBuilder that satisfies the constraints
		// specified by the DocumentBuilderFactory
		DocumentBuilder myDocumentBuilder = null;
		try {
			myDocumentBuilder = documentFactory.newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			System.err.println(pce);
			System.exit(1);
		}
		// Sets the Error Handler..
		myDocumentBuilder.setErrorHandler(new MyErrorHandler());

		// Step 3: parse the input file
		Document parsedDocument = null;
		try {
			parsedDocument = myDocumentBuilder.parse(new File(filename));
		} catch (SAXException se) {
			System.err.println(se.getMessage());
			System.exit(1);
		} catch (IOException ioe) {
			System.err.println(ioe);
			System.exit(1);
		}
		// Store the DOM tree in a String Object
		storeResult(parsedDocument);
		if (tmpResult != null && tmpResult[1] != null)
			vResult.add(tmpResult);
		return (vResult);
		// System.out.println(result);
		// for (int i=0; i < vResult.size(); i++){
		// tmpResult = (String[]) vResult.get(i);
		// System.out.println(tmpResult[0] + ":" + tmpResult[1]);
		// }
	}

	// The function is responsible for Traversing the DOM Tree and
	// storing the result in the Object of the Class String
	void storeResult(Node node) {
		String val = node.getNodeValue();

		int nodeType = node.getNodeType();
		if (nodeType == 1 && tmpResult != null && tmpResult[1] != null) {
			vResult.add(tmpResult);
			tmpResult = new String[2];

		}

		if (val == null) {
			result += "Name : " + node.getNodeName();
			if (nodeType == 1)
				tmpResult[0] = node.getNodeName();
		} else if (val.trim().equals("")) {
			// Simple Ignore
		} else if (nodeType == 3) {
			result += "Name : " + node.getNodeName();
			result += "\tValue :" + val;
			tmpResult[1] = val;
		}

		for (Node child = node.getFirstChild(); child != null; child = child
				.getNextSibling())
			storeResult(child);

	}

	String returnResult() {
		// This function is responsible for returning the result to
		// the calling program.
		return result;
	}

	// Error Handler Implementation.

	private static class MyErrorHandler implements ErrorHandler {
		/**
		 * Returns a string describing parse exception details
		 */
		private String getParseExceptionInfo(SAXParseException spe) {
			String systemid = spe.getSystemId();
			if (systemid == null) {
				systemid = "null";
			}
			String info = "URI=" + systemid + " Line=" + spe.getLineNumber()
					+ ": " + spe.getMessage();
			return info;
		}

		// The following methods are standard SAX ErrorHandler methods.
		// See SAX documentation for more info.

		public void warning(SAXParseException spe) throws SAXException {
			System.out.println("Warning: " + getParseExceptionInfo(spe));
		}

		public void error(SAXParseException spe) throws SAXException {
			String message = "Error: " + getParseExceptionInfo(spe);
			throw new SAXException(message);
		}

		public void fatalError(SAXParseException spe) throws SAXException {
			String message = "Fatal Error: " + getParseExceptionInfo(spe);
			throw new SAXException(message);
		}
	}

}
