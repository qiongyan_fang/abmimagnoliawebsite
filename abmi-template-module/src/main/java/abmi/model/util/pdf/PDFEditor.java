package abmi.model.util.pdf;

import abmi.model.services.rawdata.RawFileInfo;
import abmi.model.util.CommonUtil;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.List;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PRAcroForm;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfOutline;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.SimpleBookmark;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PDFEditor {
	private static final Logger log = LoggerFactory.getLogger(CommonUtil.class);
	static String Title = "Alberta Biodiversity Monitoring Institue Overview Of your Data Request";
	static String dateSection = "Time and Type of Data Requested";
	static String spatialTitle = "Spatial";

	static int linespacing = 14;

	public PDFEditor() {
	}

	java.util.List<String> requestList, spatialList;
	java.util.List<RawFileInfo> deliverList;
	boolean bPrototype;
	String[] prototypeAttention = {
			" Ten sites in the data you requested were sampled in 4 consecutive years. "
					+ "Normally, the ABMI surveys sites once every 5 years. "
					+ "The inclusion of 4 consecutive years of data from 10 sites may cause "
					+ "confusion during data analysis. ",
			"Sites surveyed during 2003-2006 (inclusive) were selected by ABMI to ensure "
					+ "that the protocols worked appropriately across a gradient of ecosystem types "
					+ "and human disturbances. Thus, the data you requested may not be entirely representative "
					+ "of landscapes from which the data was collected (i.e., not a random sample of ABMI's data points)." };

	/**
	 * Creates a document with outlines.
	 * 
	 * @param args
	 *            no arguments needed
	 */
	public File createPDFCover(String protocolType, String xmlPath,
			String headerImage, String tempPath) throws Exception {
		XMLParser xmlParser = new XMLParser(xmlPath);
		// step 1: creation of a document-object
		Document document = new Document();
		document.setPageSize(PageSize.LETTER);
		document.setMargins(65, 65, 125, 72); // left, right, top, bottom
		// document.setMarginMirroring(false);
		document.addAuthor("Alberta Biodiversity Monitoring Insititue");

		// System.out.println("temp=" + tempPath);
		File outputPdf = null;
		try {
			outputPdf = File.createTempFile("RawPDFCover", ".pdf", new File(
					tempPath));

			PDFHeaderFooter headerEvent = new PDFHeaderFooter();
			headerEvent.setFFootImgs(null);
			headerEvent.setFHeaderImgs(headerImage);
			headerEvent.setImageScale(80);

			// step 2: we create a writer that listens to the document
			PdfWriter writer = PdfWriter.getInstance(document,
					new FileOutputStream(outputPdf));

			writer.setPageEvent(headerEvent);
			// step 3: we open the document
			writer.setViewerPreferences(PdfWriter.PageModeUseOutlines); // add
																		// bookmarks
			// writer.setViewerPreferences(PdfWriter.PageLayoutSinglePage|PdfWriter.PageModeUseNone);
			// //no bookmarks

			// step 4: we add content to the document
			// we define some fonts
			Font titleFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 22,
					Font.BOLD, new BaseColor(17, 90, 179));
			Font sectionFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 13,
					Font.BOLD, new BaseColor(0, 0, 0));
			Font subsectionFont = FontFactory.getFont(FontFactory.TIMES_ROMAN,
					12, Font.NORMAL, new BaseColor(0, 0, 0));
			Font subsectionFontItalic = FontFactory.getFont(
					FontFactory.TIMES_ROMAN, 12, Font.ITALIC, new BaseColor(0,
							0, 0));
			Font subsectionFontBold = FontFactory.getFont(
					FontFactory.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0,
							0));
			Font subsectionFontBoldItalic = FontFactory.getFont(
					FontFactory.TIMES_ROMAN, 12, Font.BOLDITALIC,
					new BaseColor(0, 0, 0));
			// open the document
			document.open();
			// read content from xml files

			Vector vResult = xmlParser.parser();
			Paragraph gPar = new Paragraph(linespacing);
			boolean bFirstTitle = true;
			for (int i = 0; i < vResult.size(); i++) {
				String[] tmpResult = (String[]) vResult.get(i);
				String para = tmpResult[1];
				if (para.indexOf("_InputDate_") != -1) {
					SimpleDateFormat dateformat = new SimpleDateFormat(
							"EEEE, MMMM d, yyyy 'at' HH:mm:ss zzzz");

					String date = dateformat.format(new Date());
					para = para.replaceAll("_InputDate_", date);
				}

				if (para.indexOf("_ProtocolType_") != -1) {

					para = para.replaceAll("_ProtocolType_", protocolType);
				}

				if (tmpResult[0].equalsIgnoreCase("title")) {

					Paragraph title = new Paragraph(para, titleFont);
					title.setAlignment(Element.ALIGN_CENTER);
					document.add(title);
					// }

				} else if (tmpResult[0].equalsIgnoreCase("section-title")) {
					// para = para.replaceAll(protocolType,
					// protocolType.substring(0,1).toUpperCase() +
					// protocolType.substring(1, protocolType.length()));

					if (gPar.size() > 0) {
						document.add(gPar);
						gPar = new Paragraph(linespacing);

					}

					if (tmpResult[1] != null && tmpResult[1].length() > 0) {
						Chunk headerChunk = new Chunk(para, sectionFont);

						if (bFirstTitle) {
							bFirstTitle = false;
							headerChunk.setLocalDestination(protocolType);
							new PdfOutline(writer.getDirectContent()
									.getRootOutline(), PdfAction.gotoLocalPage(
									protocolType, false), protocolType
									+ " Data Request Overview");

						}
						Paragraph section_title = new Paragraph();
						section_title.add(headerChunk);
						section_title.setSpacingBefore(linespacing);
						section_title.setSpacingAfter(0);
						document.add(section_title);
					}

				} else if (tmpResult[0].equalsIgnoreCase("i")) {
					gPar.add(new Chunk(para, subsectionFontItalic));
				} else if (tmpResult[0].equalsIgnoreCase("b")) {
					gPar.add(new Chunk(para, subsectionFontBold));
				} else if (tmpResult[0].equalsIgnoreCase("bi")) {
					gPar.add(new Chunk(para, subsectionFontBoldItalic));
				} else if (tmpResult[0].equalsIgnoreCase("sub-content")) {
					if (gPar.size() > 0) {
						document.add(gPar);
						gPar = new Paragraph(linespacing);
						gPar.setSpacingBefore(-0.5f * linespacing);
					}

					gPar.add(new Chunk(para, subsectionFont));
					gPar.setIndentationLeft(30);
				} else if (tmpResult[0].equalsIgnoreCase("content")) {

					gPar.add(new Chunk(para, subsectionFont)); // linespacing is
																// the spaces
																// between lines

					// gPar.setSpacingBefore(-1*linespacing);
					gPar.setSpacingAfter(0);
					// gPar.setFirstLineIndent(linespacing);
					// document.add(content);
				} else if (tmpResult[0].equalsIgnoreCase("requestType")) {
					if (gPar.size() > 0) {
						document.add(gPar);
						gPar = new Paragraph(linespacing);
					}
//					System.out.println("requestList" + requestList);
					document.add(createListFromArryList(requestList,
							subsectionFont));
				} else if (tmpResult[0].equalsIgnoreCase("spatialType")) {
					if (gPar.size() > 0) {
						document.add(gPar);
						gPar = new Paragraph(linespacing);
					}
//					System.out.println("spatialList" + spatialList);
					document.add(createListFromArryList(spatialList,
							subsectionFont));
				} else if (tmpResult[0].equalsIgnoreCase("deliverFile")) {
					if (gPar.size() > 0) {
						document.add(gPar);
						gPar = new Paragraph(linespacing);
					}
//					System.out.println("deliverList" + deliverList);

					document.add(this.createListForRawFiles(deliverList,
							subsectionFont));
				} else if (tmpResult[0].equalsIgnoreCase("prototypeAttention")
						&& bPrototype) {
					if (gPar.size() > 0) {
						document.add(gPar);
						gPar = new Paragraph(linespacing);
					}
					if (!"".equals(prototypeAttention)) {
						Paragraph section_title = new Paragraph("Attention",
								sectionFont);

						section_title.setSpacingBefore(linespacing);
						section_title.setSpacingAfter(0);
						document.add(section_title);
						document.add(createListFromString(prototypeAttention,
								subsectionFont));
					}
				}
			}

			if (gPar.size() > 0)
				document.add(gPar);
			// document.add(chap);
		} catch (IOException ioe) {
			log.error(ioe.getMessage());
			throw ioe;

		} catch (Exception de) {
			de.printStackTrace();
		}

		// step 5: we close the document
		document.close();

		return outputPdf;
	}

	/**
	 * 
	 * @param requestList
	 * @param font
	 * @return
	 */
	List createListFromString(String[] requestList, Font font) {
		List list = new List(true, 20);
		for (int i = 0; requestList != null && i < requestList.length; i++) {
			ListItem item = new ListItem(linespacing, requestList[i], font);
			list.add(item);
		}

		// list.setIndentationLeft(30);
		return list;
	}

	/**
	 * 
	 * @param requestList
	 * @param font
	 * @return
	 */
	List createListFromArryList(java.util.List<String> requestList, Font font) {
		List list = new List(true, 20);
		for (int i = 0; requestList != null && i < requestList.size(); i++) {
			ListItem item = new ListItem(linespacing,
					(String) requestList.get(i), font);
			list.add(item);
		}

		// list.setIndentationLeft(30);
		return list;
	}

	List createListForRawFiles(java.util.List<RawFileInfo> fileList, Font font) {
		List list = new List(true, 20);
		if (fileList == null) {
			ListItem item = new ListItem(linespacing, "No file was created.",
					font);
			list.add(item);

			return list;
		}

		for (RawFileInfo row : fileList) {
			ListItem item = new ListItem(linespacing, row.toString(), font);
			list.add(item);
		}

		// list.setIndentationLeft(30);
		return list;
	}

	public void setRequestList(java.util.List<String> requestList) {
		this.requestList = requestList;
	}

	public void setSpatialList(java.util.List<String> spatialList) {
		this.spatialList = spatialList;
//		System.out.println("spatialList" + this.spatialList);
	}

	/**
	 * version 2 itext
	 * 
	 * @param sourceFiles
	 * @param outFile
	 */
	/*
	 * public static void concatenatePDFs(ArrayList<File> sourceFiles, File
	 * outFile) {
	 * 
	 * try { int pageOffset = 0; ArrayList<List> master = new ArrayList<List>();
	 * int f = 0; String tmpPdf = outFile.getPath() + ".tmp"; Document document
	 * = null; PdfCopy writer = null; while (f < sourceFiles.size()) { //
	 * LogUtil.debug(sourceFiles.get(f)); // we create a reader for a certain
	 * document try {
	 * 
	 * PdfReader reader = new
	 * PdfReader(((File)sourceFiles.get(f)).getAbsolutePath());
	 * reader.consolidateNamedDestinations(); // we retrieve the total number of
	 * pages int n = reader.getNumberOfPages(); java.util.List bookmarks =
	 * SimpleBookmark.getBookmark(reader); if (bookmarks != null) { if
	 * (pageOffset != 0) SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset,
	 * null); master.addAll(bookmarks); } pageOffset += n;
	 * 
	 * if (f == 0) { // step 1: creation of a document-object document = new
	 * Document(reader.getPageSizeWithRotation(1)); // step 2: we create a
	 * writer that listens to the document writer = new PdfCopy(document, new
	 * FileOutputStream(tmpPdf));
	 * writer.setViewerPreferences(PdfWriter.PageModeUseOutlines); //
	 * writer.setViewerPreferences
	 * (PdfWriter.PageLayoutSinglePage|PdfWriter.PageModeUseNone); //no
	 * bookmarks // writer.setPageEvent(new ABMIPageHeaderBottom()); // step 3:
	 * we open the document document.open(); } // step 4: we add content
	 * PdfImportedPage page; for (int i = 0; i < n; ) { ++i; page =
	 * writer.getImportedPage(reader, i); writer.addPage(page);
	 * 
	 * } PRAcroForm form = reader.getAcroForm(); if (form != null)
	 * writer.copyAcroForm(reader); } catch (Exception pdfE) {
	 * log.error(pdfE.getMessage()); } f++; } if (!master.isEmpty())
	 * writer.setOutlines(master);
	 * 
	 * 
	 * // step 5: we close the document document.close(); writer.close();
	 * addWatermarkToPdf(outFile, tmpPdf); } catch (Exception e) {
	 * e.printStackTrace(); } }
	 */
	
	/**merge pdfs
	 * 
	 * @param sourceFiles
	 * @param outFilee
	 * @throws IOException
	 * @throws DocumentException
	 */
	static public void concatenatePDFs(ArrayList<File> sourceFiles, File outFilee)
			throws IOException, DocumentException {

		Document document = new Document();
		PdfCopy copy = new PdfCopy(document, new FileOutputStream(outFilee));

		document.open();

		ArrayList<PdfReader> readers = new ArrayList<PdfReader>();
		for (File file : sourceFiles) {
			try{
			PdfReader reader = new PdfReader(file.getAbsolutePath());
			copy.addDocument(reader);
			readers.add(reader);
			}catch(Exception e){
				log.error("failed to concatenate PDF:" + file.getAbsolutePath() +"\n"+ e.getMessage());
			}

		}
		document.close();

		for (PdfReader reader : readers) {

			reader.close();

		}

	}

	public void setDeliverList(java.util.List<RawFileInfo> deliverList) {
		this.deliverList = deliverList;

	}

	/**
	 * Reads the pages of an existing PDF file, adds pagenumbers and a
	 * watermark.
	 * 
	 * @param args
	 *            no arguments needed
	 */
	public static void addWatermarkToPdf(File stampedFile, String orgFileName) {

		try {
			// we create a reader for a certain document
			PdfReader reader = new PdfReader(orgFileName);
			int n = reader.getNumberOfPages();
			// we create a stamper that will copy the document to a new file
			PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(
					stampedFile));
			// adding some metadata
			HashMap<String, String> moreInfo = new HashMap<String, String>();
			moreInfo.put("Author", "Alberta Biodiversity Monitoring Institue");
			stamp.setMoreInfo(moreInfo);
			// adding content to each page
			int i = 0;
			PdfContentByte under;
			PdfContentByte over;

			BaseFont bf = BaseFont.createFont(BaseFont.TIMES_ROMAN,
					BaseFont.WINANSI, BaseFont.EMBEDDED);

			while (i < n) {
				i++;

				over = stamp.getOverContent(i);

				float right = over.getPdfWriter().getPageSize().getWidth();
				// over.beginText();
				// over.setFontAndSize(bf, 8);
				//
				// String text =
				// " Alberta Biodiversity Monitoring Institue                           WWW.ABMI.CA";
				//
				// over.setTextMatrix(30, 30);
				// over.showText(text);
				//
				// // over.setFontAndSize(bf, 12);
				// // over.showTextAligned(Element.ALIGN_LEFT, "DUPLICATE", 230,
				// 430, 45);
				// over.endText();

				over.beginText();
				over.setFontAndSize(bf, 8);

				over.setTextMatrix(right - 30, 30);
				over.showText(String.valueOf(i));
				over.endText();
			}
			// closing PdfStamper will generate the new PDF file
			reader.close();
			stamp.close();

			bf = null;
			File orgFp = new File(orgFileName);
			orgFp.delete();
		} catch (Exception de) {
			de.printStackTrace();
		}
	}

	public void setBPrototype(boolean bPrototype) {
		this.bPrototype = bPrototype;
	}

	public boolean isBPrototype() {
		return bPrototype;
	}
}
