package abmi.model.util;
/**
 * @author qfang
 *
 *<br> handling temp files, registration and delete them
 *
 */



import java.io.File;
import java.io.Serializable;

import java.util.Date;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileDeleter implements HttpSessionBindingListener, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory
			.getLogger(FileDeleter.class);
    /** The chart names. */
    private ArrayList<File> filePointer = new ArrayList<File>();
    /**
     * Blank constructor.
     */
    public FileDeleter() {
    }

    /**
     * 
     * @return the number of files registered
     */
    public synchronized int getFileNum() {
        return filePointer.size();
    }

    /**
     * display all files in the list
     *
     */
    public synchronized void displayFiles() {
        
        for (int i = 0; i < filePointer.size(); i++) {
            log.debug(i + ":" + filePointer.get(i));
        }

    }

    /**
     * Add a chart to be deleted when the session expires
     *
     * @param filename  the name of the chart in the temporary directory to be deleted.
     */
    public void addFile(File filename) {
    	filePointer.add(filename);
    }

    /**
     * Checks to see if a chart is in the list of charts to be deleted
     *
     * @param filename  the name of the chart in the temporary directory.
     * @return a boolean value indicating whether the chart is present in the list.
     */
    public boolean isFileAvailable(File filename) {
        return (this.filePointer.contains(filename));
    }

    /**
     * Binding this object to the session has no additional effects.
     *
     * @param event  the session bind event.
     */
    public void valueBound(HttpSessionBindingEvent event) {
        HttpSession s= event.getSession();
        
        return;
    }

    /**
     * When this object is unbound from the session (including upon session
     * expiry) the files that have been added to the ArrayList are iterated
     * and deleted.
     *
     * @param event  the session unbind event.
     */
    public synchronized void valueUnbound(HttpSessionBindingEvent event) {
        
      
        for (File fp:filePointer){
        	 if (fp.exists()) {
                 fp.delete();
             }
        }
      
        return;

    }

}


