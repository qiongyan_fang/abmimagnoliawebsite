package abmi.model.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.text.SimpleDateFormat;

import java.util.Date;

import javax.activation.MimetypesFileTypeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Joan Fang send files to httpresponse
 */

public class WebContentSender {
	private static final Logger log = LoggerFactory
			.getLogger(WebContentSender.class);
	/**
	 * Binary streams the specified file to the HTTP response in 1KB chunks
	 * 
	 * @param file
	 *            The file to be streamed.
	 * @param response
	 *            The HTTP response object.
	 * 
	 * @throws IOException
	 *             if there is an I/O problem.
	 * @throws FileNotFoundException
	 *             if the file is not found.
	 */
	public static void sendTempFile(File file, HttpServletResponse response)
			throws IOException, FileNotFoundException {
		sendTempFile(file, response, "");
	}

	/**
	 * 
	 * @param file
	 * @param response
	 * @param attachType
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void sendTempFile(File file, HttpServletResponse response,
			String attachType) throws IOException, FileNotFoundException {

		String fileName = file.getName();

		MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();

		// only by file name
		String mimeType = mimeTypesMap.getContentType(fileName);

		if (attachType.equalsIgnoreCase("attachment"))
			response.setHeader("content-disposition", "attachment; filename="
					+ file.getName());

		if (attachType.equalsIgnoreCase("inline"))
			response.setHeader("content-disposition", "inline; filename="
					+ file.getName());

		WebContentSender.sendTempFileWithMIME(file, response, mimeType);
	}

	/**
	 * Binary streams the specified file to the HTTP response in 1KB chunks
	 * 
	 * @param file
	 *            The file to be streamed.
	 * @param response
	 *            The HTTP response object.
	 * @param mimeType
	 *            The mime type of the file, null allowed.
	 * 
	 * @throws IOException
	 *             if there is an I/O problem.
	 * @throws FileNotFoundException
	 *             if the file is not found.
	 */
	public static void sendTempFileWithMIME(File file,
			HttpServletResponse response, String mimeType) throws IOException,
			FileNotFoundException {

		if (file.exists()) {
			BufferedInputStream bis = new BufferedInputStream(
					new FileInputStream(file));

			// Set HTTP headers
			if (mimeType != null) {
				response.setHeader("Content-Type", mimeType);
			}
			response.setHeader("Content-Length", String.valueOf(file.length()));
			SimpleDateFormat sdf = new SimpleDateFormat(
					"EEE, dd MMM yyyy HH:mm:ss z");
			response.setHeader("Last-Modified",
					sdf.format(new Date(file.lastModified())));
			BufferedOutputStream bos = new BufferedOutputStream(
					response.getOutputStream());
			byte[] input = new byte[1024];
			boolean eof = false;
			while (!eof) {
				int length = bis.read(input);
				if (length == -1) {
					eof = true;
				} else {
					bos.write(input, 0, length);
				}
			}
			bos.flush();
			bis.close();
			bos.close();

		} else {
			throw new FileNotFoundException(file.getAbsolutePath());
		}
		return;
	}

	/**
	 * Adds a ChartDeleter object to the session object with the name
	 * JFreeChart_Deleter if there is not already one bound to the session and
	 * adds the filename to the list of charts to be deleted.
	 * 
	 * @param tempFile
	 *            the file to be deleted.
	 * @param session
	 *            the HTTP session of the client.
	 */
	static public void registerFileForDeletion(File tempFile,
			HttpSession session) {

		// Add chart to deletion list in session
		if (session != null) {
			FileDeleter fileDeleter = (FileDeleter) session
					.getAttribute("Raw_Deleter");
			if (fileDeleter == null) {
				fileDeleter = new FileDeleter();
				session.setAttribute("Raw_Deleter", fileDeleter);
			}
			// debug
			// fileDeleter.displayFiles();
			fileDeleter.addFile(tempFile);
			// debug
			// fileDeleter.displayFiles();
		} else {
			log.warn("Session is null - " + tempFile +" will not be deleted");
		}

	}

	/**
	 * Binary streams the specified file in the temporary directory to the HTTP
	 * response in 1KB chunks
	 * 
	 * @param filename
	 *            The name of the file in the temporary directory.
	 * @param response
	 *            The HTTP response object.
	 * @throws IOException
	 *             if there is an I/O problem.
	 * @throws FileNotFoundException
	 *             if the file is not found.
	 */
	public static void sendTempFile(String filename,
			HttpServletResponse response) throws IOException,
			FileNotFoundException {

		File file = new File(System.getProperty("java.io.tmpdir"), filename);
		WebContentSender.sendTempFile(file, response);
	}

	/**
	 * 
	 * @param file
	 * @param response
	 */
	public static void sendStreamData(File file, HttpServletResponse response) {
		try {
			ServletOutputStream os = response.getOutputStream();
			FileInputStream is = new FileInputStream(file);
			writeInputStreamToOutputStream(is, os);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	/**
	 * 
	 * @param in
	 * @param out
	 * @throws IOException
	 */
	public static void writeInputStreamToOutputStream(InputStream in,
			OutputStream out) throws IOException {
		byte[] buffer = new byte[8192];
		int bytesRead = 0;
		while ((bytesRead = in.read(buffer, 0, 8192)) != -1) {
			out.write(buffer, 0, bytesRead);
		}
	}

	/**
	 * Perform a search/replace operation on a String There are String methods
	 * to do this since (JDK 1.4)
	 * 
	 * @param inputString
	 *            the String to have the search/replace operation.
	 * @param searchString
	 *            the search String.
	 * @param replaceString
	 *            the replace String.
	 * 
	 * @return the String with the replacements made.
	 */
	public static String searchReplace(String inputString, String searchString,
			String replaceString) {

		int i = inputString.indexOf(searchString);
		if (i == -1) {
			return inputString;
		}

		String r = "";
		r += inputString.substring(0, i) + replaceString;
		if (i + searchString.length() < inputString.length()) {
			r += searchReplace(
					inputString.substring(i + searchString.length()),
					searchString, replaceString);
		}

		return r;
	}
}
