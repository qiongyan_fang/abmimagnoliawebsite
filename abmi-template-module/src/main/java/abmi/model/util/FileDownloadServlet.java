package abmi.model.util;

/**
 * @author qfang
 *<br>Created on Nov 15, 2004
 * <br>Utitly class to locate a temp file in temp fold givent the file's name
 *
 */

import java.io.File;
import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import abmi.model.services.ProjectPropertyService;

/**
 * Servlet used for streaming charts to the client browser from the temporary
 * directory. You need to add this servlet and mapping to your deployment
 * descriptor (web.xml) in order to get it to work. The syntax is as follows:
 * <xmp> <servlet> <servlet-name>RawDataDisplay</servlet-name>
 * <servlet-class>.../mPath/.RawDataDisplay</servlet-class> </servlet>
 * <servlet-mapping> <servlet-name>RawDataDisplay</servlet-name>
 * <url-pattern>/servlet/RawDataDisplay</url-pattern> </servlet-mapping> </xmp>
 * 
 * @author Richard Atkinson
 */
public class FileDownloadServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// @Autowired
	// ProjectPropertyService prjDao;
	//
	// @PersistenceContext private EntityManager em;
	/**
	 * Default constructor.
	 */
	public FileDownloadServlet() {
		super();
	}

	/**
	 * Init method.
	 * 
	 * @throws ServletException
	 *             never.
	 */
	public void init(ServletConfig config) {
		try {
			super.init(config);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
				config.getServletContext());

	}

	/**
	 * 
	 * @param filename
	 * @param file
	 * @param request
	 * @param response
	 * @throws ServletException
	 */
	private void getTempFile(String filename, File file,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		// Check that the graph being served was created by the current user
		// or that it begins with "public"
		HttpSession session = request.getSession();
		String sessionName = request.getParameter("session");
		if (sessionName == null)
			sessionName = "Raw_Deleter";

		// System.out.println("Session Name is " + sessionName);
		boolean isFileInUserList = false;
		FileDeleter fileDeleter = (FileDeleter) session
				.getAttribute(sessionName);

		if (fileDeleter != null) {
			isFileInUserList = fileDeleter.isFileAvailable(file);
		}

		boolean isFilePublic = false;
		if (filename.length() >= 6) {
			if (filename.substring(0, 6).equals("public")) {
				isFilePublic = true;
			}
		}
		String attachType = request.getParameter("attachment");
		if (attachType == null) {
			attachType = "attachment";
		}

		if (isFileInUserList || isFilePublic) {
			// Serve it up
			try {
				WebContentSender.sendTempFile(file, response, attachType);

			} catch (Exception e) {
				throw new ServletException("Data file not found");

			}
		} else {
			throw new ServletException("Data file not found");
		}

		return;
	}

	/**
	 * 
	 * @param file
	 * @param response
	 * @throws ServletException
	 */
	private void getDiskFile(File file, HttpServletResponse response)
			throws ServletException {

		try {

			WebContentSender.sendTempFile(file, response, "attachment");

		} catch (Exception e) {
			throw new ServletException("Data file not found");

		}

		return;

	}

	/**
	 * Service method.
	 * 
	 * @param request
	 *            the request.
	 * @param response
	 *            the response.
	 * 
	 * @throws ServletException
	 *             ??.
	 * @throws IOException
	 *             ??.
	 */
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		boolean bTempFile = true;
		String filename = request.getParameter("filename");

		if (filename == null) {
			throw new ServletException("Parameter 'filename' must be supplied");
		}

		String dirname = request.getParameter("dir");
		String path = "";

		if (dirname == null) {
			path = System.getProperty("java.io.tmpdir");
		} else {

			EntityManager em = (EntityManager) Persistence
					.createEntityManagerFactory(
							ModuleParameters.DATA_ENTITY_MANAGER)
					.createEntityManager();
			ProjectPropertyService prjDao = new ProjectPropertyService();
			prjDao.setEm(em);
		

			bTempFile = false;
			String dirPathName = dirname.toUpperCase() + "_PATH";

			path = prjDao.getPath(dirPathName);
		}

		// LogUtil.debug("servlet path = " + path);
		// Replace ".." with ""
		// This is to prevent access to the rest of the file system
		filename = WebContentSender.searchReplace(filename, "..", "");

		// Check the file exists
		File file = new File(path, filename);
		if (!file.exists()) {
			throw new ServletException("File '" + path + "/" + filename
					+ "' does not exist");
		}

		if (bTempFile) {
			getTempFile(filename, file, request, response);
		} else {
			getDiskFile(file, response);
		}
	}
}
