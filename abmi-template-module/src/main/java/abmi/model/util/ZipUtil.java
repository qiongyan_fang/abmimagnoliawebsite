package abmi.model.util;

import abmi.model.services.ProjectPropertyService;

import java.util.ArrayList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 
 * @author qfang <br>
 *         compress a number of files into a zip file.
 * 
 */
public class ZipUtil {

	/**
	 * 
	 * @param filesNameToZip
	 *            the list of files to be zipped, do not include "." at ends of
	 *            file name
	 * @param zipFileName
	 *            the out put zip file without extension name "zip"
	 * @param session
	 *            http session
	 * @return the zipped file name
	 */
	public static String createZip(String[] filesNameToZip, String zipFileName,
			String path) {

		byte[] buffer = new byte[18024];
		ArrayList<File> zipped = new ArrayList<File>();
		try {
			ProjectPropertyService prjDAO = new ProjectPropertyService();
			File zipFile = File.createTempFile(zipFileName, ".zip", new File(
					prjDAO.getTempPath()));

			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(
					zipFile));

			// Set the compression ratio
			out.setLevel(Deflater.DEFAULT_COMPRESSION);

			// iterate through the array of files, adding each to the zip file
			for (int i = 0; i < filesNameToZip.length; i++) {

				// do nothing when empty file input
				if (filesNameToZip[i] == null || filesNameToZip[i].equals(""))
					continue;
				// System.out.println(i + "\tout of " + filesToZip.length +"\t"
				// + filesToZip[i]);
				// Associate a file input stream for the current file
				File file = new File(path, filesNameToZip[i]);
				if (zipped.contains(file))
					continue;

				zipped.add(file);
				FileInputStream in = new FileInputStream(file);

				// Add ZIP entry to output stream.
				out.putNextEntry(new ZipEntry(filesNameToZip[i]));

				// Transfer bytes from the current file to the ZIP file
				// out.write(buffer, 0, in.read(buffer));

				int len;
				while ((len = in.read(buffer)) > 0) {
					out.write(buffer, 0, len);
				}

				// Close the current entry
				out.closeEntry();

				// Close the current file input stream
				in.close();

			}
			// Close the ZipOutPutStream
			out.close();
			zipped.clear();
			buffer = null;
			return zipFile.getName();
		} catch (IllegalArgumentException iae) {
			iae.printStackTrace();
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return "";

	}

	/*
	 * 
	 * @param filesToZip the list of files to be zipped, do not include "." at
	 * ends of file name
	 * 
	 * @param zipFileName the out put zip file without extension name "zip"
	 * 
	 * @param session http session
	 * 
	 * @return the zipped file name
	 */

	public static File createZip(ProjectPropertyService prjDAO,
			ArrayList<File> filesToZip, String zipFileName) {

		byte[] buffer = new byte[18024];
		ArrayList<File> zipped = new ArrayList<File>();
		try {

			File zipFile =

			File.createTempFile(zipFileName, ".zip",
					new File(prjDAO.getTempPath()));

			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(
					zipFile));

			// Set the compression ratio
			out.setLevel(Deflater.DEFAULT_COMPRESSION);

			// iterate through the array of files, adding each to the zip file
			for (int i = 0; i < filesToZip.size(); i++) {

				// do nothing when empty file input
				if (filesToZip.get(i) == null)
					continue;
				File file = (File) filesToZip.get(i);

				if (zipped.contains(file) || !file.exists())
					continue;

				zipped.add(file);
				// Associate a file input stream for the current file

				FileInputStream in = new FileInputStream(file);

				// Add ZIP entry to output stream.
				out.putNextEntry(new ZipEntry(file.getName()));

				// Transfer bytes from the current file to the ZIP file
				// out.write(buffer, 0, in.read(buffer));

				int len;
				while ((len = in.read(buffer)) > 0) {
					out.write(buffer, 0, len);
				}

				// Close the current entry
				out.closeEntry();

				// Close the current file input stream
				in.close();

			}
			// Close the ZipOutPutStream
			out.close();

			zipped.clear();
			buffer = null;
			return zipFile;
		} catch (IllegalArgumentException iae) {
			iae.printStackTrace();
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return null;

	}

	public static File createZip(ArrayList<File> filesToZip,
			String zipFileName, String path) {

		byte[] buffer = new byte[18024];
		ArrayList<File> zipped = new ArrayList<File>();
		try {
			File zipFile =

			File.createTempFile(zipFileName, ".zip", new File(path));

			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(
					zipFile));

			// Set the compression ratio
			out.setLevel(Deflater.DEFAULT_COMPRESSION);

			// iterate through the array of files, adding each to the zip file
			for (int i = 0; i < filesToZip.size(); i++) {

				// do nothing when empty file input
				if (filesToZip.get(i) == null)
					continue;
				File file = (File) filesToZip.get(i);

				if (zipped.contains(file))
					continue;

				zipped.add(file);
				// Associate a file input stream for the current file

				FileInputStream in = new FileInputStream(file);

				// Add ZIP entry to output stream.
				out.putNextEntry(new ZipEntry(file.getName()));

				// Transfer bytes from the current file to the ZIP file
				// out.write(buffer, 0, in.read(buffer));

				int len;
				while ((len = in.read(buffer)) > 0) {
					out.write(buffer, 0, len);
				}

				// Close the current entry
				out.closeEntry();

				// Close the current file input stream
				in.close();

			}
			// Close the ZipOutPutStream
			out.close();
			zipped.clear();
			buffer = null;

			return zipFile;
		} catch (IllegalArgumentException iae) {
			iae.printStackTrace();
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return null;

	}
}
