package abmi.model.util;

import abmi.model.services.ProjectPropertyService;

import java.io.File;

import java.io.FileInputStream;

import java.io.FileOutputStream;


import java.nio.channels.FileChannel;



/**
 * @author qfang
 * <br>this function creates temporary directory, temporary files, and register them 
 * <br>for later deletion.
*
 */
public class TempFileUtil {

	/**
	 * Creates the temporary directory if it does not exist.
	 * Throws a RuntimeException if the temporary directory is null.
	 * Uses the database setting for temporary fiel or system property java.io.tmpdir as the temporary directory.
	 * Sounds like a strange thing to do but my temporary directory was not created
	 * on my default Tomcat 4.0.3 installation.  Could save some questions on the
	 * forum if it is created when not present.
	 */
	public static void createTempDir() {
		ProjectPropertyService prjDAO = new ProjectPropertyService();
	   String tempDirName  = prjDAO.getTempPath();
	
	    //  Create the temporary directory if it doesn't exist
	     createDir(tempDirName);
	}
        
     /**
     * 
     * @param tempDirName
     */
    public static void createTempDir(String tempDirName) {
         //  Create the temporary directory if it doesn't exist
       createDir(tempDirName);
    }
    /**
     * 
     * @param dirName
     */
    public static void createDir(String dirName) {
         //  Create the temporary directory if it doesn't exist
        File tempDir = new File(dirName);
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
    }
        /**
     * 
     * @param in
     * @param out
     * @throws Exception
     */
    public static void moveFile(String in, String out) throws Exception {
           FileChannel sourceChannel = new
                    FileInputStream(new File(in)).getChannel();
           FileChannel destinationChannel = new
                    FileOutputStream(new File(out)).getChannel();
           sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
           // or
           //  destinationChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
           sourceChannel.close();
           destinationChannel.close();
           }
    
        
}
