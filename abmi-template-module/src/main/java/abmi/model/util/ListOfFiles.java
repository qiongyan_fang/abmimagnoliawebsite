package abmi.model.util;

import java.util.*;
import java.io.*;

public class ListOfFiles implements Enumeration {

    private List<File> listOfFiles;
    private int current = 0;

    public ListOfFiles(List<File> listOfFiles) {
        this.listOfFiles = listOfFiles;
    }

    public boolean hasMoreElements() {
        if (current < listOfFiles.size())
            return true;
        else
            return false;
    }

    public Object nextElement() {
        InputStream in = null;

        if (!hasMoreElements())
            throw new NoSuchElementException("No more files.");
        else {
            File nextElement = (File) listOfFiles.get(current);
            current++;
            try {
                in = new FileInputStream(nextElement);
            } catch (FileNotFoundException e) {
                System.err.println("ListOfFiles: Can't open " + nextElement);
            }
        }
        return in;
    }
}

