package abmi.model.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;    
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class Mail {
    public Mail() {
    }
    
    public static void sendMail(String recipient, String sender, String senderName, String subject,String content) throws MessagingException{
        Properties props = new Properties();
        InputStream    input = Mail.class.getClassLoader().getResourceAsStream("mail.properties");
        String smtphost="smtp.srv.ualberta.ca";
		// load a properties file
        	try {
				props.load(input);
				smtphost = props.getProperty("mailhost");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	// get the property value and print it out
        	
	

	
        props.put("mail.smtp.host", smtphost);
        Session session = Session.getDefaultInstance(props, null);
        // create a message
        Message msg = new MimeMessage(session);
        // set the from and to address
        InternetAddress addressFrom = new InternetAddress(sender);
        try {
			addressFrom.setPersonal(senderName);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        msg.setFrom(addressFrom);
        InternetAddress[] addressTo = new InternetAddress[1];
        addressTo[0] = new InternetAddress(recipient);
        msg.setRecipients(Message.RecipientType.TO, addressTo);
        // Optional : You can also set your custom headers in the Email if you Want
        //msg.addHeader("MyHeaderName", "myHeaderValue");
        // Setting the Subject and Content Type
        msg.setSubject(subject);
        msg.setContent(content, "text/plain");
        Transport.send(msg);
    }
}
