package abmi.model.util;

import java.text.SimpleDateFormat;

public class ModuleParameters {

	final static public String GLOSSARY_WORKSPACE = "glossarys";
	final static public String GLOSSARY_APPS = "glossarys";
	final static public String DATA_ENTITY_MANAGER = "data";
	final static public String SECURITY_ENTITY_MANAGER = "security";
	
	static public SimpleDateFormat sdfmonthyear = new SimpleDateFormat("MMMM yyyy");
	static public SimpleDateFormat sdfull = new SimpleDateFormat("MMMM d, yyyy");
	
	
}
