package abmi.model.util.chart.ext;

import java.util.ArrayList;

import org.jfree.chart.labels.AbstractXYItemLabelGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.data.xy.XYDataset;

public class ABMIXYToolTipGenerator  extends AbstractXYItemLabelGenerator  implements XYToolTipGenerator {
      
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<String> fLabels;

    public ABMIXYToolTipGenerator(ArrayList<String> labels) {
        this.fLabels = labels;
    }

    public String generateToolTip(XYDataset dataset,  int series, int item) {
        return (String)fLabels.get(item);
//        return generateLabelString(dataset, series, item);
    }
}
