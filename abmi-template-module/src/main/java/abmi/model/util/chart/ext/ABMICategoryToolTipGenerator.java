package abmi.model.util.chart.ext;

import java.util.ArrayList;

import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.data.category.CategoryDataset;

public  class ABMICategoryToolTipGenerator extends StandardCategoryToolTipGenerator {
        /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		ArrayList<String> fLabels;

        public ABMICategoryToolTipGenerator(ArrayList<String> labels) {
            this.fLabels = labels;
        }

        public String generateToolTip(CategoryDataset dataset, int row, 
                                      int column) {
            return (String)fLabels.get(column);
        }
    }