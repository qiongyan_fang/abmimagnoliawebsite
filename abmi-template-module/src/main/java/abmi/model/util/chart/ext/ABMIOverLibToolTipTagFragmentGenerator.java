package abmi.model.util.chart.ext;

import org.jfree.chart.imagemap.ImageMapUtilities;
import org.jfree.chart.imagemap.ToolTipTagFragmentGenerator;

public class ABMIOverLibToolTipTagFragmentGenerator 
    
    implements ToolTipTagFragmentGenerator {

    /**
    * Creates a new instance.
    */
     public ABMIOverLibToolTipTagFragmentGenerator() {
     
        super();
    }

    /**
    * Generates a tooltip string to go in an HTML image map.
    *
    * @param toolTipText  the tooltip text.
    *
    * @return The formatted HTML area tag attribute(s).
    */
    public String generateToolTipFragment(String toolTipText) {
    return " onMouseOver=\"return overlib('"
            + ImageMapUtilities.htmlEscape(toolTipText)
            + "', this);\" onMouseOut=\"return nd(this);\"";
    }
}
