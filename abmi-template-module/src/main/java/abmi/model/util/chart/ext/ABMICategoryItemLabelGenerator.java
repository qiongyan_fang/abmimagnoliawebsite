package abmi.model.util.chart.ext;

import java.util.ArrayList;

import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.data.category.CategoryDataset;

public class ABMICategoryItemLabelGenerator extends
		StandardCategoryItemLabelGenerator {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<String> fLabels;

	public  ABMICategoryItemLabelGenerator(ArrayList<String> labels) {
		this.fLabels = labels;
	}

	public String generateLabel(CategoryDataset dataset, int row, int column) {
		return (String) fLabels.get(column);
	}
}