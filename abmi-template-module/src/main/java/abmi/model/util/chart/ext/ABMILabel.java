package abmi.model.util.chart.ext;
import java.awt.Font;
import java.awt.Paint;

public class ABMILabel {
	

	
	/** 
	 * An arbitrary piece of text which can be placed anywhere in the
	 * chart area. The coordinates are given in pixels with (0,0) at
	 * the top-left of the chart. Note that the plot area overwrites any
	 * notes so if you need text inside the plot, use an Annotation
	 * instead.
	 */
	

	    /** The text to be printed in the note */
	    public String text;

	    /** The font to be used for the note */
	    public Font font;
	    
	    /** The paint (ie, color) to be used for the note */
	    public Paint paint;
	    
	    /** The x-coordinate of the note (in pixels) */
	    public int xCoord;

	    /** The y-coordinate of the note (in pixels) */
	    public int yCoord;

	    /**
	     * Constructs a Note - A note is an arbitrary piece of text which
	     * can be placed anywhere in the chart area.
	     */
	    public ABMILabel (String text, Font font, Paint paint, int xCoord, int yCoord) {
	        this.text = text;
	        this.font = font;
	        this.paint = paint;
	        this.xCoord = xCoord;
	        this.yCoord = yCoord;
	    }
	}
