package abmi.model.util.chart.renderer;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRendererState;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.ui.RectangleEdge;
import org.jfree.util.ShapeUtilities;

public class ABMILineAndShapeCategoryRenderer extends BarRenderer {
	private double offsetX;

	public double getOffsetX() {
		return offsetX;
	}

	public void setOffsetX(double offsetX) {
		this.offsetX = offsetX;
	}

	@Override
	public void drawItem(Graphics2D g2, CategoryItemRendererState state,
			Rectangle2D dataArea, CategoryPlot plot, CategoryAxis domainAxis,
			ValueAxis rangeAxis, CategoryDataset dataset, int row, int column,
			int pass) {

		int visibleRow = state.getVisibleSeriesIndex(row);
		if (visibleRow < 0) {
			return;
		}

		PlotOrientation orientation = plot.getOrientation();
		double rectX = 0.0, rectX0 = 0.0;
		double rectY = 0.0, rectY0 = 0.0;

		RectangleEdge rangeAxisLocation = plot.getRangeAxisEdge();

		// Y0
		Number value1 = dataset.getValue(row, column);
		if (value1 == null) {
			return;
		}
		double java2dValue1 = rangeAxis.valueToJava2D(value1.doubleValue(),
				dataArea, rangeAxisLocation);

		
 		
		// previous Y
		 if (column != 0) { // if it is not the first column, then line to the first column
			 
             Number previousValue = dataset.getValue(row, column - 1);
             if (previousValue != null) {
                 // previous data point...
                 double previous = previousValue.doubleValue();
                 double java2dValue0 = rangeAxis.valueToJava2D(previous,
         				dataArea, rangeAxisLocation);
                 
                 
             	// BAR WIDTH
         		double rectWidth = state.getBarWidth();

         		// BAR HEIGHT
         		double rectHeight = Math.abs(java2dValue1 - java2dValue0);

         		RectangleEdge barBase = RectangleEdge.LEFT;
         		Line2D line = null;
         		if (orientation == PlotOrientation.HORIZONTAL) { // not tested on horizontal
         			// BAR Y
         			rectX = java2dValue1;
         			rectY = calculateBarW0(getPlot(), orientation, dataArea,
         					domainAxis, state, visibleRow, column);
         			
         			rectY0 = calculateBarW0(getPlot(), orientation, dataArea,
         					domainAxis, state, visibleRow-1, column-1);
         			
         			rectHeight = state.getBarWidth();
         			rectWidth = Math.abs(java2dValue1 - java2dValue0);
         			barBase = RectangleEdge.LEFT;
         			
         			
         			rectY = java2dValue1;
         			rectY0 = java2dValue0;
         			barBase = RectangleEdge.BOTTOM;
         			
         			line = new Line2D.Double(  rectY,rectX + (0.5 + this.offsetX)
             				* rectWidth, rectY0, rectX0 + (0.5 + this.offsetX) * rectWidth
             				);
         			
         			g2.setStroke(this.getBaseStroke());
             		g2.setPaint(this.getItemPaint(row, column));
             		g2.draw(line);
             		
         		} else if (orientation == PlotOrientation.VERTICAL) {
         			// BAR X
         			rectX = calculateBarW0(getPlot(), orientation, dataArea,
         					domainAxis, state, visibleRow, column);
         			
         			
         			rectX0 = calculateBarW0(getPlot(), orientation, dataArea,
         					domainAxis, state, visibleRow-1, column-1);
         			
         			
         			rectY = java2dValue1;
         			rectY0 = java2dValue0;
         			barBase = RectangleEdge.BOTTOM;
         			
         			line = new Line2D.Double( rectX + (0.5 + this.offsetX)
             				* rectWidth, rectY, rectX0 + (0.5 + this.offsetX) * rectWidth,
             				rectY0);
         			
         			g2.setStroke(this.getBaseStroke());
             		g2.setPaint(this.getItemPaint(row, column));
             		g2.draw(line);
         		}
         		// Rectangle2D bar = new Rectangle2D.Double(rectX, rectY, rectWidth,
         		// rectHeight);
         	
         		

         		CategoryItemLabelGenerator generator = getItemLabelGenerator(row,
         				column);
         		if (generator != null && isItemLabelVisible(row, column)) {

         			drawItemLabel(g2, orientation, dataset, row, column, rectY, rectX,
         					false);
         		}

         		// add an item entity, if this information is being collected
         		EntityCollection entities = state.getEntityCollection();
         		if (entities != null) {
         			addItemEntity(entities, dataset, row, column, line);
         		}
                 
                 
             }
		 }
                 
		
	

	}

}
