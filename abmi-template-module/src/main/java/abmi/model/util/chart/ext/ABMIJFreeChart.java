package abmi.model.util.chart.ext;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.entity.JFreeChartEntity;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.title.Title;
import org.jfree.text.TextBlock;
import org.jfree.text.TextLine;
import org.jfree.ui.Align;
import org.jfree.ui.Size2D;

public class ABMIJFreeChart extends JFreeChart {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List notes;

	public ABMIJFreeChart(Plot plot) {
		super(plot);
		// TODO Auto-generated constructor stub
		
	}

    public ABMIJFreeChart(String title, Font titleFont, Plot plot,
            boolean createLegend) {
    	super(title,titleFont, plot, createLegend);
    	this.notes = new ArrayList();
    }
	@Override
	public void draw(Graphics2D g2, Rectangle2D chartArea, Point2D anchor,
			ChartRenderingInfo info) {

		   notifyListeners(new ChartProgressEvent(this, this,
	                ChartProgressEvent.DRAWING_STARTED, 0));
	        
	        EntityCollection entities = null;
	        // record the chart area, if info is requested...
	        if (info != null) {
	            info.clear();
	            info.setChartArea(chartArea);
	            entities = info.getEntityCollection();
	        }
	        if (entities != null) {
	            entities.add(new JFreeChartEntity((Rectangle2D) chartArea.clone(),
	                    this));
	        }

	        // ensure no drawing occurs outside chart area...
	        Shape savedClip = g2.getClip();
	        g2.clip(chartArea);

	        g2.addRenderingHints(super.getRenderingHints());

	        // draw the chart background...
	        if (super.getBackgroundPaint() != null) {
	            g2.setPaint(super.getBackgroundPaint());
	            g2.fill(chartArea);
	        }

	        if (super.getBackgroundImage() != null) {
	            Composite originalComposite = g2.getComposite();
	            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
	                    super.getBackgroundImageAlpha()));
	            Rectangle2D dest = new Rectangle2D.Double(0.0, 0.0,
	            		super.getBackgroundImage().getWidth(null),
	            		super.getBackgroundImage().getHeight(null));
	            Align.align(dest, chartArea, super.getBackgroundImageAlignment());
	            g2.drawImage(super.getBackgroundImage(), (int) dest.getX(),
	                    (int) dest.getY(), (int) dest.getWidth(),
	                    (int) dest.getHeight(), null);
	            g2.setComposite(originalComposite);
	        }

	        if (isBorderVisible()) {
	            Paint paint = getBorderPaint();
	            Stroke stroke = getBorderStroke();
	            if (paint != null && stroke != null) {
	                Rectangle2D borderArea = new Rectangle2D.Double(
	                        chartArea.getX(), chartArea.getY(),
	                        chartArea.getWidth() - 1.0, chartArea.getHeight()
	                        - 1.0);
	                g2.setPaint(paint);
	                g2.setStroke(stroke);
	                g2.draw(borderArea);
	            }
	        }

	        // draw the title and subtitles...
	        Rectangle2D nonTitleArea = new Rectangle2D.Double();
	        nonTitleArea.setRect(chartArea);
	        super.getPadding().trim(nonTitleArea);

	        if (super.getTitle() != null && super.getTitle().isVisible()) {
	            EntityCollection e = drawTitle(super.getTitle(), g2, nonTitleArea,
	                    (entities != null));
	            if (e != null && entities != null) {
	                entities.addAll(e);
	            }
	        }

	        Iterator iterator = super.getSubtitles().iterator();
	        while (iterator.hasNext()) {
	            Title currentTitle = (Title) iterator.next();
	            if (currentTitle.isVisible()) {
	                EntityCollection e = drawTitle(currentTitle, g2, nonTitleArea,
	                        (entities != null));
	                if (e != null && entities != null) {
	                    entities.addAll(e);
	                }
	            }
	        }

	        Rectangle2D plotArea = nonTitleArea;

	        // draw the plot (axes and data visualisation)
	        PlotRenderingInfo plotInfo = null;
	        if (info != null) {
	            plotInfo = info.getPlotInfo();
	        }
	        super.getPlot().draw(g2, plotArea, anchor, null, plotInfo);

	        g2.setClip(savedClip);
	        
	        
	    	Iterator noteIterator = this.notes.iterator();
			while (noteIterator.hasNext()) {
				ABMILabel currentNote = (ABMILabel) noteIterator.next();
				drawNote(currentNote, g2, nonTitleArea);
			}
			
	        notifyListeners(new ChartProgressEvent(this, this,
	                ChartProgressEvent.DRAWING_FINISHED, 100));
		
		
	
	}

	public void drawNote(ABMILabel note, Graphics2D g2, Rectangle2D area) {
		TextBlock tb = new TextBlock();
		tb.addLine(new TextLine(note.text, note.font, note.paint));
		Size2D size = tb.calculateDimensions(g2);
		
		tb.draw(g2, (float)(note.xCoord), (float)( note.yCoord), null);
		
		
	}

	public void addNote(ABMILabel note) {
		if (note != null) {
			this.notes.add(note);
		}
	}
}
