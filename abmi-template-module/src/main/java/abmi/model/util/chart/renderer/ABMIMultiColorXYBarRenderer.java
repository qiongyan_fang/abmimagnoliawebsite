package abmi.model.util.chart.renderer;

import java.awt.Color;
import java.awt.Paint;
import java.util.ArrayList;

import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;

import org.jfree.chart.plot.DrawingSupplier;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.ui.TextAnchor;

public class ABMIMultiColorXYBarRenderer extends XYBarRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** The colors. */
	private Paint[] colors;
	private double[] values;

	/**
	 * Creates a new renderer.
	 * 
	 * @param colors
	 *            the colors.
	 */
	public ABMIMultiColorXYBarRenderer(final Paint[] colors) {
		
		this.colors = colors;
		
	}
	
	public ABMIMultiColorXYBarRenderer(final ArrayList<String> colorCodes) {
		this.colors = new Paint[colorCodes.size()];
		for (int i= 0; i < colors.length; i++) {
			colors[i] = Color.decode(colorCodes.get(i));	
		}
	}

	public ABMIMultiColorXYBarRenderer(final double[] values) {
		this.values = values;
	}

	/**
	 * Returns the paint for an item. Overrides the default behaviour inherited
	 * from AbstractSeriesRenderer.
	 * 
	 * @param row
	 *            the series.
	 * @param column
	 *            the category.
	 * 
	 * @return The item color.
	 */
	final ItemLabelPosition p = new ItemLabelPosition(
			ItemLabelAnchor.OUTSIDE6, TextAnchor.BOTTOM_CENTER,
			TextAnchor.CENTER, 0);

	final ItemLabelPosition pOut = new ItemLabelPosition(
			ItemLabelAnchor.OUTSIDE6, TextAnchor.CENTER_LEFT,
			TextAnchor.CENTER_LEFT, -Math.PI / 2.0);

	public Paint getItemPaint(final int row, final int column) {
		// if (this.values[column % this.colors.length] == 0){
		// return new Color(237, 240, 250);
		// }
		// else {
		// return new Color(237, 240, 250);
		// }
		return this.colors[column % this.colors.length];
	}
	
	@Override
    public Paint getSeriesPaint(int series) {

		  return this.colors[series];
      

    }

	/**
	 * 
	 * @param row
	 * @param column
	 * @return
	 */
//	public ItemLabelPosition getPositiveItemLabelPosition(int row, int column) {
//		//
//		// if (this.values[column % this.colors.length] == 0){
//		//
//		// }
//		// else if (this.values[column % this.colors.length] == 0){
//		// }
//		// else{
//		//
//		// }
//		if (this.colors[column % this.colors.length].equals(new Color(237, 240,
//				250))) {
//			return pOut;
//		} else {
//			return p;
//
//		}
//	}
}