package abmi.model.util.chart.ext;

/* ===========================================================
 * JFreeChart : a free chart library for the Java(tm) platform
 * ===========================================================
 *
 * (C) Copyright 2000-2013, by Object Refinery Limited and Contributors.
 *
 * Project Info:  http://www.jfree.org/jfreechart/index.html
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * [Oracle and Java are registered trademarks of Oracle and/or its affiliates. 
 * Other names may be trademarks of their respective owners.]
 *
 * ----------------------
 * XYTitleAnnotation.java
 * ----------------------
 * (C) Copyright 2007-2011, by Object Refinery Limited and Contributors.
 *
 * Original Author:  David Gilbert (for Object Refinery Limited);
 * Contributor(s):   Andrew Mickish;
 *                   Peter Kolb (patch 2809117);
 *
 * Changes:
 * --------
 * 02-Feb-2007 : Version 1 (DG);
 * 30-Apr-2007 : Fixed equals() method (DG);
 * 26-Feb-2008 : Fixed NullPointerException when drawing chart with a null
 *               ChartRenderingInfo - see patch 1901599 by Andrew Mickish (DG);
 * 03-Sep-2008 : Moved from experimental to main (DG);
 * 24-Jun-2009 : Fire change events (see patch 2809117 by PK) (DG);
 *
 */

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;

import org.jfree.chart.HashUtilities;
import org.jfree.chart.annotations.AbstractAnnotation;
import org.jfree.chart.annotations.AbstractXYAnnotation;
import org.jfree.chart.annotations.CategoryAnnotation;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.block.BlockParams;
import org.jfree.chart.block.EntityBlockResult;
import org.jfree.chart.block.RectangleConstraint;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.entity.XYAnnotationEntity;
import org.jfree.chart.event.AnnotationChangeEvent;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.Title;
import org.jfree.chart.util.ParamChecks;
import org.jfree.chart.util.XYCoordinateType;
import org.jfree.data.Range;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.Size2D;
import org.jfree.util.ObjectUtilities;
import org.jfree.util.PublicCloneable;

/**
 * 
 * @author Qiongyan
 * 
 */
public class CategoryTitleAnnotation

extends AbstractAnnotation implements CategoryAnnotation, Cloneable, PublicCloneable, Serializable {

	/** For serialization. */
	private static final long serialVersionUID = -4364694501921559958L;

	/** The maximum width. */
	private double maxWidth;

	/** The maximum height. */
	private double maxHeight;

	/** The title. */
	private Title title;

	private Rectangle2D position;

	 String toolTipText;
     String URL;
     
     public CategoryTitleAnnotation(Title title ) {
    	 super();
    	 this.title = title;
     }
	public Rectangle2D getPosition() {
		return position;
	}

	public void setPosition(Rectangle2D position) {
		this.position = position;
	}

	/**
	 * The title anchor point.
	 */
	private RectangleAnchor anchor;

	/**
	 * Creates a new annotation to be displayed at the specified (x, y)
	 * location.
	 * 
	 * @param x
	 *            the x-coordinate (in data space).
	 * @param y
	 *            the y-coordinate (in data space).
	 * @param title
	 *            the title (<code>null</code> not permitted).
	 */

	/**
	 * Returns the title for the annotation.
	 * 
	 * @return The title.
	 */
	public Title getTitle() {
		return this.title;
	}

	/**
	 * Returns the title anchor for the annotation.
	 * 
	 * @return The title anchor.
	 */
	public RectangleAnchor getTitleAnchor() {
		return this.anchor;
	}

	/**
	 * Returns the maximum width.
	 * 
	 * @return The maximum width.
	 */
	public double getMaxWidth() {
		return this.maxWidth;
	}

	/**
	 * Sets the maximum width and sends an {@link AnnotationChangeEvent} to all
	 * registered listeners.
	 * 
	 * @param max
	 *            the maximum width (0.0 or less means no maximum).
	 */
	public void setMaxWidth(double max) {
		this.maxWidth = max;
		fireAnnotationChanged();
	}

	/**
	 * Returns the maximum height.
	 * 
	 * @return The maximum height.
	 */
	public double getMaxHeight() {
		return this.maxHeight;
	}

	/**
	 * Sets the maximum height and sends an {@link AnnotationChangeEvent} to all
	 * registered listeners.
	 * 
	 * @param max
	 *            the maximum height.
	 */
	public void setMaxHeight(double max) {
		this.maxHeight = max;
		fireAnnotationChanged();
	}

	

	public String getToolTipText() {
		return toolTipText;
	}

	public void setToolTipText(String toolTipText) {
		this.toolTipText = toolTipText;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}

	public RectangleAnchor getAnchor() {
		return anchor;
	}

	public void setAnchor(RectangleAnchor anchor) {
		this.anchor = anchor;
	}

	public void setTitle(Title title) {
		this.title = title;
	}
	
	 /**
     * A utility method for adding an {@link XYAnnotationEntity} to
     * a {@link PlotRenderingInfo} instance.
     *
     * @param info  the plot rendering info (<code>null</code> permitted).
     * @param hotspot  the hotspot area.
     * @param rendererIndex  the renderer index.
     * @param toolTipText  the tool tip text.
     * @param URLText  the URL text.
     */
    protected void addEntity(PlotRenderingInfo info,
                             Shape hotspot, int rendererIndex,
                             String toolTipText, String URLText) {
        if (info == null) {
            return;
        }
        EntityCollection entities = info.getOwner().getEntityCollection();
        if (entities == null) {
            return;
        }
        XYAnnotationEntity entity = new XYAnnotationEntity(hotspot,
                rendererIndex, toolTipText, URLText);
        entities.add(entity);
    }

	/**
	 * Tests this object for equality with an arbitrary object.
	 * 
	 * @param obj
	 *            the object (<code>null</code> permitted).
	 * 
	 * @return A boolean.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof CategoryTitleAnnotation)) {
			return false;
		}
		CategoryTitleAnnotation that = (CategoryTitleAnnotation) obj;
		
		if (this.maxWidth != that.maxWidth) {
			return false;
		}
		if (this.maxHeight != that.maxHeight) {
			return false;
		}
		if (!ObjectUtilities.equal(this.title, that.title)) {
			return false;
		}
		if (!this.anchor.equals(that.anchor)) {
			return false;
		}
		return super.equals(obj);
	}

	/**
	 * Returns a hash code for this object.
	 * 
	 * @return A hash code.
	 */
	@Override
	public int hashCode() {
		int result = 193;
		result = HashUtilities.hashCode(result, this.anchor);
		
		result = HashUtilities.hashCode(result, this.maxWidth);
		result = HashUtilities.hashCode(result, this.maxHeight);
		result = HashUtilities.hashCode(result, this.title);
		return result;
	}

	/**
	 * Returns a clone of the annotation.
	 * 
	 * @return A clone.
	 * 
	 * @throws CloneNotSupportedException
	 *             if the annotation can't be cloned.
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	/**
	 * only works for horizontal 
	 */
	@Override
	public void draw(Graphics2D g2, CategoryPlot plot, Rectangle2D dataArea,
			CategoryAxis domainAxis, ValueAxis rangeAxis) {
		// TODO Auto-generated method stub
		
			PlotOrientation orientation = plot.getOrientation();
			AxisLocation domainAxisLocation = plot.getDomainAxisLocation();
			AxisLocation rangeAxisLocation = plot.getRangeAxisLocation();
			RectangleEdge domainEdge = Plot.resolveDomainAxisLocation(
					domainAxisLocation, orientation);
			RectangleEdge rangeEdge = Plot.resolveRangeAxisLocation(
					rangeAxisLocation, orientation);
			
			double anchorX, anchorY;

			double maxW = dataArea.getWidth();
			double maxH = dataArea.getHeight();
			if (this.maxWidth > 0.0) {
				maxW = maxW * this.maxWidth;
			}
			if (this.maxHeight > 0.0) {
				maxH = maxH * this.maxHeight;
			}

			float xx = (float)dataArea.getWidth();
			float yy = (float)dataArea.getHeight();
			

			RectangleConstraint rc = new RectangleConstraint(new Range(0, maxW),
					new Range(0, maxH));

			Size2D size = this.title.arrange(g2, rc);
			Rectangle2D titleRect = new Rectangle2D.Double(0, 0, size.width,
					size.height);
			Point2D anchorPoint = RectangleAnchor.coordinates(titleRect,
					this.anchor);
			xx = xx - (float) anchorPoint.getX();
			yy = yy - (float) anchorPoint.getY();
			titleRect.setRect(dataArea.getX()+xx -titleRect.getWidth(), dataArea.getY() + yy- titleRect.getHeight(), titleRect.getWidth(), titleRect.getHeight());
			BlockParams p = new BlockParams();
			
			Object result = this.title.draw(g2, titleRect, p);
			
		}

}
