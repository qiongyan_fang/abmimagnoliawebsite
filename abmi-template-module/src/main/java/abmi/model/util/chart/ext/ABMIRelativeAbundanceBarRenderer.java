package abmi.model.util.chart.ext;

import java.awt.Color;
import java.awt.Paint;

import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.ui.TextAnchor;

/**
 * this is a custom bar render, which change the bar renderer's color for each category.
 * by default same category across different series will have same colors.
 * but for ABMI website Species relative abundance bar, it wants same series have same colors.
 * i.e. all ages at spruces have one color, and all ages for pine have one colors.
 * @author Qiongyan
 *
 */
public class ABMIRelativeAbundanceBarRenderer extends BarRenderer {

	        /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
			/** The colors. */
	        private Paint[] colors;
	        private double[] values;

	        /**
	         * Creates a new renderer.
	         *
	         * @param colors  the colors.
	         */
	        public ABMIRelativeAbundanceBarRenderer(final Paint[] colors) {
	            this.colors = colors;
	        }
	        public ABMIRelativeAbundanceBarRenderer(final double[] values) {
	            this.values = values;
	        }

	        /**
	         * Returns the paint for an item.  Overrides the default behaviour inherited from
	         * AbstractSeriesRenderer.
	         *
	         * @param row  the series.
	         * @param column  the category.
	         *
	         * @return The item color.
	         */
	         final ItemLabelPosition p = 
	             new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, 
	                                     TextAnchor.BOTTOM_CENTER, 
	                                     TextAnchor.CENTER,   0);

	         final ItemLabelPosition pOut = 
	             new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, 
	                                   TextAnchor.CENTER_LEFT, 
	                                   TextAnchor.CENTER_LEFT, -Math.PI / 2.0);

	        public Paint getItemPaint(final int row, final int column) {
//	            if (this.values[column % this.colors.length] == 0){
//	                return new Color(237, 240, 250);                
//	            }
//	            else {
//	                return new Color(237, 240, 250);
//	            }
	            return this.colors[column % this.colors.length];
	        }
	        
	        
	        /**
	         * 
	         * @param row
	         * @param column
	         * @return
	         */
	        public ItemLabelPosition getPositiveItemLabelPosition(int row,
	                                                              int column){

	              if ( this.colors[column % this.colors.length].equals(new Color(237, 240, 250)) ){
	                  return pOut;
	              }
	              else{
	                return p;
	                  
	              }
	        }
}
