package abmi.model.util.chart.renderer;


import java.awt.Color;
import java.awt.Paint;
import java.io.Serializable;
import java.util.ArrayList;

import org.jfree.chart.renderer.category.BarRenderer;

public class ABMIMultiColorBarRenderer extends BarRenderer implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** The colors. */
	private Paint[] colors;

	/**
	 * Creates a new renderer.
	 * 
	 * @param colors
	 *            the colors.
	 */
	public ABMIMultiColorBarRenderer(final Paint[] colors) {
		this.colors = colors;
	}
	
	public ABMIMultiColorBarRenderer(final String[] colorCodes) {
		this.colors = new Paint[colorCodes.length];
		for (int i= 0; i < colors.length; i++) {
			colors[i] = Color.decode(colorCodes[i]);	
		}
	}

	public ABMIMultiColorBarRenderer(final ArrayList<String> colorCodes) {
		this.colors = new Paint[colorCodes.size()];
		for (int i= 0; i < colors.length; i++) {
			colors[i] = Color.decode(colorCodes.get(i));	
		}
	}

	/**
	 * Returns the paint for an item. Overrides the default behaviour inherited
	 * from AbstractSeriesRenderer.
	 * 
	 * @param row
	 *            the series.
	 * @param column
	 *            the category.
	 * 
	 * @return The item color.
	 */

	public Paint getItemPaint(final int row, final int column) {
		// if (this.values[column % this.colors.length] == 0){
		// return new Color(237, 240, 250);
		// }
		// else {
		// return new Color(237, 240, 250);
		// }
		return this.colors[column % this.colors.length];
	}



}
