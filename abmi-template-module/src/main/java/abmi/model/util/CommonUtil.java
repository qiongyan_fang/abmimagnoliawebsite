package abmi.model.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;

public class CommonUtil {
	private static final Logger log = LoggerFactory
			.getLogger(CommonUtil.class);
	public CommonUtil() {
	}

	/**
	 * 
	 * @param in
	 * @param out
	 * @throws IOException
	 */
	public static void writeInputStreamToOutStream(InputStream in,
			OutputStream out) throws IOException {
		byte[] buffer = new byte[8192];
		int charsRead = 0;
		while ((charsRead = in.read(buffer)) != -1) {
			out.write(buffer, 0, charsRead);
		}
	}

	public static void writeInputStreamToWriter(InputStream in, Writer out)
			throws IOException {
		InputStreamReader isr = new InputStreamReader(in);
		char[] buffer = new char[8192];
		int charsRead = 0;
		while ((charsRead = isr.read(buffer, 0, 8192)) != -1) {
			out.write(buffer, 0, charsRead);
		}
	}

	/**
	 * 
	 * @param clob
	 * @param filePath
	 * @return
	 */
	public static boolean createFileFromString(String filePath, String text,
			String fileName) throws IOException {

		try {
			TempFileUtil.createDir(filePath);
			FileWriter fw = new FileWriter(filePath + fileName);
			FileCopyUtils.copy(text, fw);

			return true;
		} catch (IOException ioe) {
			throw ioe;
		}

	}


	/**
	 * 
	 * @param fieldName
	 * @param fieldValues
	 * @param enclosedBy
	 * @return
	 */
	public static String createSqlFromArrayList(String fieldName,
			List fieldValues, String enclosedBy) {
		if (fieldValues == null || fieldValues.size() == 0)
			return "";

		String listSql = "";
		listSql += fieldName + " in (";
		int j = 0;
		for (int i = 0; i < fieldValues.size(); i++) {
			if (fieldValues.get(i) == null)
				continue;

			if (j > 0)
				listSql += ",";

			if (enclosedBy.equals("'")) {
				String encodedValue = String.valueOf(fieldValues.get(i))
						.replace("'", "''");
				listSql += enclosedBy + encodedValue + enclosedBy;
			} else {
				listSql += enclosedBy + fieldValues.get(i) + enclosedBy;
			}
			j++;
			if (j >= 1000) {
				listSql += ") OR " + fieldName + " in (";
				j = 0;
			}
		}
		listSql += ")";

		return "(" + listSql + ")";
	}

	/**
	 * 
	 * @param fieldValues
	 * @param enclosedBy
	 * @return
	 */
	public static String createSqlFromArrayListWithoutFieldName(
			List fieldValues, String enclosedBy) {
		if (fieldValues == null || fieldValues.size() == 0)
			return "";

		String listSql = "";
		listSql += "(";
		for (int i = 0; i < fieldValues.size(); i++) {
			if (fieldValues.get(i) == null)
				continue;
			if (i > 0)
				listSql += ",";

			if (enclosedBy.equals("'")) {
				String encodedValue = ((String) fieldValues.get(i)).replace(
						"'", "''");
				listSql += enclosedBy + encodedValue + enclosedBy;
			} else {
				listSql += enclosedBy + fieldValues.get(i) + enclosedBy;
			}

		}
		listSql += ")";

		return listSql;
	}

//	public static String[] createSiteStringFromArrayList(List fieldValues) {
//		if (fieldValues == null || fieldValues.size() == 0)
//			return null;
//
//		String[] listSql = new String[2];
//		int strInt = 0;
//		int siteInd = 0;
//		listSql[strInt] = new String("");
//		for (int i = 0; i < fieldValues.size(); i++) {
//			if (fieldValues.get(i) == null)
//				continue;
//			if (siteInd > 0)
//				listSql[strInt] += ",";
//
//			String tmpRow = String.valueOf(fieldValues.get(i));
//			if (listSql[strInt].length() + tmpRow.length() >= 3999) {
//				strInt++;
//				siteInd = 0;
//				listSql[strInt] = new String("");
//			}
//			siteInd++;
//			listSql[strInt] += tmpRow;
//
//		}
//
//		return listSql;
//	}
//
//
//	public static String createSqlFromArray(String fieldName,
//			String[] fieldValues, String enclosedBy) {
//		if (fieldValues == null || fieldValues.length == 0)
//			return "";
//
//		String listSql = "";
//		listSql += fieldName + " in (";
//		int j = 0;
//		for (int i = 0; i < fieldValues.length; i++) {
//			if (j > 0)
//				listSql += ",";
//			if (enclosedBy.equals("'")) {
//				String encodedValue = ((String) fieldValues[i]).replace("'",
//						"''");
//				listSql += enclosedBy + encodedValue + enclosedBy;
//			} else {
//				listSql += enclosedBy + fieldValues[i] + enclosedBy;
//			}
//
//			j++;
//			if (j >= 1000) {
//				listSql += ") OR " + fieldName + " in (";
//				j = 0;
//			}
//		}
//		listSql += ")";
//
//		return listSql;
//	}
//
//	public static String createSqlFromArrayWithoutFiledName(
//			String[] fieldValues, String enclosedBy) {
//		if (fieldValues == null || fieldValues.length == 0)
//			return "";
//
//		String listSql = "";
//		listSql += "(";
//		for (int i = 0; i < fieldValues.length; i++) {
//			if (i > 0)
//				listSql += ",";
//
//			String encodedValue;
//
//			if (enclosedBy.equals("'"))
//				encodedValue = fieldValues[i].replace("'", "''");
//			else
//				encodedValue = fieldValues[i];
//			listSql += enclosedBy + encodedValue + enclosedBy;
//
//		}
//		listSql += ")";
//
//		return listSql;
//	}
//
//	public static void concatFiles(List<File> fileList, File outputFile)
//			throws IOException {
//		ListOfFiles mylist = new ListOfFiles(fileList);
//
//		FileWriter fw = new FileWriter(outputFile);
//		BufferedWriter bw = new BufferedWriter(fw);
//		PrintWriter out = new PrintWriter(bw);
//		SequenceInputStream s = new SequenceInputStream(mylist);
//
//		int c;
//		while ((c = s.read()) != -1) {
//			out.write(c);
//
//		}
//
//		s.close();
//		out.close();
//		bw.close();
//		fw.close();
//	}

	public static void concatFilesSkipRow(List<File> fileList, File outputFile)
			throws IOException {

		FileWriter fw = new FileWriter(outputFile);
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter out = new PrintWriter(bw);
		for (int i = 0; fileList != null && i < fileList.size(); i++) {
			File fp = fileList.get(i);
			try {
				System.out.println(fp.getAbsolutePath() + " " + fp.exists());
				if (fp == null || !fp.exists()) {
					continue;
				}
				BufferedReader s = new BufferedReader(new FileReader(fp));

				String c;
				if (i > 0) { // skip headers
					s.readLine();
				}
				while ((c = s.readLine()) != null) {
					out.println(c);

				}

				s.close();
			} catch (Exception e) {
				System.out.println("skip one file");
			}
		}
		out.close();
		bw.close();
		fw.close();
	}

	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static File createTempFile(String fileName, String path)
			throws Exception {
		try {
			File tempFile;

			log.debug("filename = " + fileName + " path=" + path);
			try {
				TempFileUtil.createTempDir(path);
			} catch (Exception e) {
			}
			tempFile = // at least 3 characters needed for a file prefix
			File.createTempFile(fileName, ".csv", new File(path));

			return tempFile;
		} catch (Exception e) {
			log.error("file exception " + e  + " fileName="+fileName + " path=" + path);
			throw e;
		}
	}

	public void removeFile(String pathStr, String fileName) {
		try {
			if (fileName == null)
				return;

			File fp = new File(pathStr + fileName);
			if (fp.exists()) {
				fp.delete();
				log.debug(pathStr + fileName + " is removed successfully ");
			} else
				log.warn("Report doesn't exist:" + pathStr + fileName + "\n");
		} catch (Exception ioe) {
			log.error("Error on delete Report:" + fileName + "\n");
			log.error(ioe.getMessage());
		}

	}

	public static <T> Iterable<T> emptyIfNull(Iterable<T> iterable) {
		return iterable == null ? Collections.<T> emptyList() : iterable;
	}
	
	/**
	 * 
	 * @param inStr
	 * @return
	 */
	public static String nvl(Object inStr){
		return  (inStr == null?"":String.valueOf(inStr));
	}

	/**
	 * 
	 * @param inStr
	 * @param defaultStr
	 * @return
	 */
	public static String nvl(Object inStr, String defaultStr){
		return  (inStr == null?defaultStr:String.valueOf(inStr));
	}

	
	public static String getServerPath(HttpServletRequest request) {

		// request.getRequestURL()=http://localhost:8081/.ajax/getSingleSeciesProfile
		// request.getRequestURI()=/.ajax/getSingleSeciesProfile

		StringBuffer url = request.getRequestURL();
		String uri = request.getRequestURI();
		return url.substring(0, url.length() - uri.length())
				+ request.getContextPath();
	}
}
