package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the G_CSV_PREFIXS database table.
 * 
 */
@Entity
@Table(schema="METADATA",name="G_CSV_PREFIXS")
public class GCsvPrefix implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GCP_ID")
	private Integer gcpId;

	@Column(name="GCP_LABEL")
	private String gcpLabel;

	@Column(name="GCP_TABLE_PREFIX")
	private String gcpTablePrefix;

	@Column(name="GCP_CATEGORY")
	private Integer gcpCategory;

	@Column(name="GCP_COMMENTS")
	private String gcpComments;
	
	public String getGcpComments() {
		return gcpComments;
	}

	
	public Integer getGcpCategory() {
		return gcpCategory;
	}

	public void setGcpCategory(Integer gcpCategory) {
		this.gcpCategory = gcpCategory;
	}

	public GCsvPrefix() {
	}

	public Integer getGcpId() {
		return this.gcpId;
	}

	public void setGcpId(Integer gcpId) {
		this.gcpId = gcpId;
	}

	public String getGcpLabel() {
		return this.gcpLabel;
	}

	public void setGcpLabel(String gcpLabel) {
		this.gcpLabel = gcpLabel;
	}

	public String getGcpTablePrefix() {
		return this.gcpTablePrefix;
	}

	public void setGcpTablePrefix(String gcpTablePrefix) {
		this.gcpTablePrefix = gcpTablePrefix;
	}

}