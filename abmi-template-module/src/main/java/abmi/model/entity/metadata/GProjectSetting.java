package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the G_PROJECT_SETTINGS database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="G_PROJECT_SETTINGS")
public class GProjectSetting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GPS_NAME")
	private String gpsName;

	@Column(name="GPS_VALUE")
	private String gpsValue;

	public GProjectSetting() {
	}

	public String getGpsName() {
		return this.gpsName;
	}

	public void setGpsName(String gpsName) {
		this.gpsName = gpsName;
	}

	public String getGpsValue() {
		return this.gpsValue;
	}

	public void setGpsValue(String gpsValue) {
		this.gpsValue = gpsValue;
	}

}