package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the G_DOWNLOAD_FILES database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="G_DOWNLOAD_FILES")
public class GAdditionalDownloadFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	@Column(name="GDF_FILE_BLOB")
	private byte[] gdfFileBlob;

	@Column(name="GDF_FILE_NAME")
	private String gdfFileName;

	@Id
	@Column(name="GDF_ID")
	private long gdfId;

	@Column(name="GDF_ORDER")
	private BigDecimal gdfOrder;

	@Column(name="GDF_PROTOCOL_TYPE")
	private String gdfProtocolType;

	@Column(name="GDF_SIZE")
	private BigDecimal gdfSize;

	@Column(name="GDF_TITLE")
	private String gdfTitle;

	public GAdditionalDownloadFile() {
	}

	public byte[] getGdfFileBlob() {
		return this.gdfFileBlob;
	}

	public void setGdfFileBlob(byte[] gdfFileBlob) {
		this.gdfFileBlob = gdfFileBlob;
	}

	public String getGdfFileName() {
		return this.gdfFileName;
	}

	public void setGdfFileName(String gdfFileName) {
		this.gdfFileName = gdfFileName;
	}

	public long getGdfId() {
		return this.gdfId;
	}

	public void setGdfId(long gdfId) {
		this.gdfId = gdfId;
	}

	public BigDecimal getGdfOrder() {
		return this.gdfOrder;
	}

	public void setGdfOrder(BigDecimal gdfOrder) {
		this.gdfOrder = gdfOrder;
	}

	public String getGdfProtocolType() {
		return this.gdfProtocolType;
	}

	public void setGdfProtocolType(String gdfProtocolType) {
		this.gdfProtocolType = gdfProtocolType;
	}

	public BigDecimal getGdfSize() {
		return this.gdfSize;
	}

	public void setGdfSize(BigDecimal gdfSize) {
		this.gdfSize = gdfSize;
	}

	public String getGdfTitle() {
		return this.gdfTitle;
	}

	public void setGdfTitle(String gdfTitle) {
		this.gdfTitle = gdfTitle;
	}

}