package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the G_FILE_DOWNLOAD_CATEGORY_MAINS database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="G_FILE_DOWNLOAD_CATEGORY_MAINS")
public class GFileDownloadCategoryMain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GFDCM_ID")
	private Integer gfdcmId;

	@Column(name="GFDCM_CATEGORY")
	private String gfdcmCategory;

	@Column(name="GFDCM_HEIGHT")
	private Integer gfdcmHeight;

	@Column(name="GFDCM_ORDER")
	private Integer gfdcmOrder;

	@Column(name="GFDCM_PROTOCOL")
	private String gfdcmProtocol;
	
	@Column(name="GFDCM_INCLUDE")
	private Integer gfdcmInclude;
	
	@Column(name="GFDCM_DESCRIPTION")
	private String gfdcmDescription;

	public String getGfdcmDescription() {
		return gfdcmDescription;
	}

	
	public Integer getGfdcmInclude() {
		return gfdcmInclude;
	}

	//bi-directional many-to-one association to GFileDownloadCategory
	@OneToMany(mappedBy="gFileDownloadCategoryMain")
	@OrderBy (value="gfdcOrder")
	private List<GFileDownloadCategory> gFileDownloadCategories;

	public GFileDownloadCategoryMain() {
	}

	public Integer getGfdcmId() {
		return this.gfdcmId;
	}

	public void setGfdcmId(Integer gfdcmId) {
		this.gfdcmId = gfdcmId;
	}

	public String getGfdcmCategory() {
		return this.gfdcmCategory;
	}

	public void setGfdcmCategory(String gfdcmCategory) {
		this.gfdcmCategory = gfdcmCategory;
	}

	public Integer getGfdcmHeight() {
		return this.gfdcmHeight;
	}

	public void setGfdcmHeight(Integer gfdcmHeight) {
		this.gfdcmHeight = gfdcmHeight;
	}

	public Integer getGfdcmOrder() {
		return this.gfdcmOrder;
	}

	public void setGfdcmOrder(Integer gfdcmOrder) {
		this.gfdcmOrder = gfdcmOrder;
	}

	public String getGfdcmProtocol() {
		return this.gfdcmProtocol;
	}

	public void setGfdcmProtocol(String gfdcmProtocol) {
		this.gfdcmProtocol = gfdcmProtocol;
	}

	public List<GFileDownloadCategory> getGFileDownloadCategories() {
		return this.gFileDownloadCategories;
	}

	public void setGFileDownloadCategories(List<GFileDownloadCategory> GFileDownloadCategories) {
		this.gFileDownloadCategories = GFileDownloadCategories;
	}

}