package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the G_FILE_DOWNLOAD_CATEGORIES database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="G_FILE_DOWNLOAD_CATEGORIES")
public class GFileDownloadCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GFDC_ID")
	private Integer gfdcId;

	@Column(name="GFDC_ORDER")
	private Integer gfdcOrder;

	@Column(name="GFDC_TITLE")
	private String gfdcTitle;
	
	@Column(name="GFDC_DESCRIPTION")
	private String gfdcDescription;

	public String getGfdcDescription() {
		return gfdcDescription;
	}
	
	@Column(name="GFDC_NEXT_RELEASE_DATE")
	private String gfdcNextReleaseDate;

	@Column(name="GFDC_LATEST_YEAR")
	private int gfdcLastestYear;
	
	@Column(name="GFDC_START_YEAR")
	private int gfdcStartYear;
	 
	


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getGfdcNextReleaseDate() {
		return gfdcNextReleaseDate;
	}

	public int getGfdcLastestYear() {
		return gfdcLastestYear;
	}

	public int getGfdcStartYear() {
		return gfdcStartYear;
	}

	public GFileDownloadCategoryMain getgFileDownloadCategoryMain() {
		return gFileDownloadCategoryMain;
	}

	//bi-directional many-to-one association to GFileDownloadCategoryMain
	@ManyToOne
	@JoinColumn(name="GFDC_GFDCM_ID")
	private GFileDownloadCategoryMain gFileDownloadCategoryMain;

	public GFileDownloadCategory() {
	}

	public Integer getGfdcId() {
		return this.gfdcId;
	}

	public void setGfdcId(Integer gfdcId) {
		this.gfdcId = gfdcId;
	}

	public Integer getGfdcOrder() {
		return this.gfdcOrder;
	}

	public void setGfdcOrder(Integer gfdcOrder) {
		this.gfdcOrder = gfdcOrder;
	}

	public String getGfdcTitle() {
		return this.gfdcTitle;
	}

	public void setGfdcTitle(String gfdcTitle) {
		this.gfdcTitle = gfdcTitle;
	}

	public GFileDownloadCategoryMain getGFileDownloadCategoryMain() {
		return this.gFileDownloadCategoryMain;
	}

	public void setGFileDownloadCategoryMain(GFileDownloadCategoryMain gFileDownloadCategoryMain) {
		this.gFileDownloadCategoryMain = gFileDownloadCategoryMain;
	}

}