package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the G_CACHECSV database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="G_CACHECSV")
@NamedQuery(name="GCachecsv.findAll", query="SELECT g FROM GCachecsv g")
@SequenceGenerator(schema="METADATA", name="GC_SEQ", initialValue=50, allocationSize=1)

public class GCachecsv implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GC_ID")
	
	// need to add schema name in there
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GC_SEQ")
	private long gcId;

	@Column(name="GC_DATE")
	private String gcDate;

	@Column(name="GC_FILEPATH")
	private String gcFilepath;

	@Column(name="GC_GCP")
	private Integer gcGcp;

	@Column(name="GC_RECORDCOUNT")
	private Long gcRecordcount;

	@Column(name="GC_RI_ID")
	private Integer gcRiId;

	@Column(name="GC_ROTATION_ID")
	private Integer gcRotationId;

	
	@Column(name="GC_GV_ID")
	private Integer gcGvId;

	
	public Integer getGcGvId() {
		return gcGvId;
	}

	public void setGcGvId(Integer gcGvId) {
		this.gcGvId = gcGvId;
	}

	public GCachecsv() {
	}

	public long getGcId() {
		return this.gcId;
	}

	public void setGcId(long gcId) {
		this.gcId = gcId;
	}

	public String getGcDate() {
		return this.gcDate;
	}

	public void setGcDate(String gcDate) {
		this.gcDate = gcDate;
	}

	public String getGcFilepath() {
		return this.gcFilepath;
	}

	public void setGcFilepath(String gcFilepath) {
		this.gcFilepath = gcFilepath;
	}

	public Integer getGcGcp() {
		return gcGcp;
	}

	public void setGcGcp(Integer gcGcp) {
		this.gcGcp = gcGcp;
	}

	public Long getGcRecordcount() {
		return gcRecordcount;
	}

	public void setGcRecordcount(Long gcRecordcount) {
		this.gcRecordcount = gcRecordcount;
	}

	public Integer getGcRiId() {
		return gcRiId;
	}

	public void setGcRiId(Integer gcRiId) {
		this.gcRiId = gcRiId;
	}

	public Integer getGcRotationId() {
		return gcRotationId;
	}

	public void setGcRotationId(Integer gcRotationId) {
		this.gcRotationId = gcRotationId;
	}

	

	
}