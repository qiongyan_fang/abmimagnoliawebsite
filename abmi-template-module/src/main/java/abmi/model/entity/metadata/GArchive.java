package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the G_ARCHIVES database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="G_ARCHIVES")
public class GArchive implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GA_ID")
	private long gaId;

	@Lob
	@Column(name="GA_BFILE")
	private byte[] gaBfile;

	@Column(name="GA_CREATE_DATE")
	private String gaCreateDate;

	@Lob
	@Column(name="GA_FILE")
	private String gaFile;

	@Column(name="GA_FILE_PATH")
	private String gaFilePath;

	@Column(name="GA_GCP_ID")
	private Integer gaGcpId;


	@Column(name="GA_RECORD_NUMBER")
	private Long gaRecordNumber;

	@Column(name="GA_REGION")
	private String gaRegion;

	@Column(name="GA_ROTATION")
	private String gaRotation;

	@Column(name="GA_TABLE_NAME")
	private String gaTableName;

	@Column(name="GA_TITLE")
	private String gaTitle;

	@Column(name="GA_VERSION")
	private String gaVersion;


	@ManyToOne
	@JoinColumn(name="GA_GV_ID")
	GViewobject gViewobject;
	
	public GViewobject getgViewobject() {
		return gViewobject;
	}

	public GArchive() {
	}

	public long getGaId() {
		return this.gaId;
	}

	public void setGaId(long gaId) {
		this.gaId = gaId;
	}

	public byte[] getGaBfile() {
		return this.gaBfile;
	}

	public void setGaBfile(byte[] gaBfile) {
		this.gaBfile = gaBfile;
	}

	public String getGaCreateDate() {
		return this.gaCreateDate;
	}

	public void setGaCreateDate(String gaCreateDate) {
		this.gaCreateDate = gaCreateDate;
	}

	public String getGaFile() {
		return this.gaFile;
	}

	public void setGaFile(String gaFile) {
		this.gaFile = gaFile;
	}

	public String getGaFilePath() {
		return this.gaFilePath;
	}

	public void setGaFilePath(String gaFilePath) {
		this.gaFilePath = gaFilePath;
	}

	public Integer getGaGcpId() {
		return this.gaGcpId;
	}

	public void setGaGcoId(Integer gaGcpId) {
		this.gaGcpId = gaGcpId;
	}





	public Long getGaRecordNumber() {
		return this.gaRecordNumber;
	}

	public void setGaRecordNumber(Long gaRecordNumber) {
		this.gaRecordNumber = gaRecordNumber;
	}

	public String getGaRegion() {
		return this.gaRegion;
	}

	public void setGaRegion(String gaRegion) {
		this.gaRegion = gaRegion;
	}

	public String getGaRotation() {
		return this.gaRotation;
	}

	public void setGaRotation(String gaRotation) {
		this.gaRotation = gaRotation;
	}

	public String getGaTableName() {
		return this.gaTableName;
	}

	public void setGaTableName(String gaTableName) {
		this.gaTableName = gaTableName;
	}

	public String getGaTitle() {
		return this.gaTitle;
	}

	public void setGaTitle(String gaTitle) {
		this.gaTitle = gaTitle;
	}

	public String getGaVersion() {
		return this.gaVersion;
	}

	public void setGaVersion(String gaVersion) {
		this.gaVersion = gaVersion;
	}





}