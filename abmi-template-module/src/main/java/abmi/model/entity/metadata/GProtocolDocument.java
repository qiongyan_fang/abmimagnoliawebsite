package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the G_PROTOCOL_DOCUMENTS database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="G_PROTOCOL_DOCUMENTS")
public class GProtocolDocument implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="GPD_CODE")
	private String gpdCode;

	@Column(name="GPD_COMMENTS")
	private String gpdComments;

	@Lob
	@Column(name="GPD_DOC_BLOB")
	private byte[] gpdDocBlob;

	@Column(name="GPD_DOC_LOCATION")
	private String gpdDocLocation;

	@Column(name="GPD_DOC_ORDER")
	private BigDecimal gpdDocOrder;

	@Column(name="GPD_DOC_PATH")
	private String gpdDocPath;

	@Id
	@Column(name="GPD_ID")
	private Integer gpdId;

	@Column(name="GPD_PROTOCOL_NAME")
	private String gpdProtocolName;

	public GProtocolDocument() {
	}

	public String getGpdCode() {
		return this.gpdCode;
	}

	public void setGpdCode(String gpdCode) {
		this.gpdCode = gpdCode;
	}

	public String getGpdComments() {
		return this.gpdComments;
	}

	public void setGpdComments(String gpdComments) {
		this.gpdComments = gpdComments;
	}

	public byte[] getGpdDocBlob() {
		return this.gpdDocBlob;
	}

	public void setGpdDocBlob(byte[] gpdDocBlob) {
		this.gpdDocBlob = gpdDocBlob;
	}

	public String getGpdDocLocation() {
		return this.gpdDocLocation;
	}

	public void setGpdDocLocation(String gpdDocLocation) {
		this.gpdDocLocation = gpdDocLocation;
	}

	public BigDecimal getGpdDocOrder() {
		return this.gpdDocOrder;
	}

	public void setGpdDocOrder(BigDecimal gpdDocOrder) {
		this.gpdDocOrder = gpdDocOrder;
	}

	public String getGpdDocPath() {
		return this.gpdDocPath;
	}

	public void setGpdDocPath(String gpdDocPath) {
		this.gpdDocPath = gpdDocPath;
	}

	public Integer getGpdId() {
		return this.gpdId;
	}

	public void setGpdId(Integer gpdId) {
		this.gpdId = gpdId;
	}

	public String getGpdProtocolName() {
		return this.gpdProtocolName;
	}

	public void setGpdProtocolName(String gpdProtocolName) {
		this.gpdProtocolName = gpdProtocolName;
	}

}