package abmi.model.entity.metadata;

import java.io.Serializable;

import javax.persistence.*;

import abmi.model.entity.internal.TableSynJob;

import java.util.List;


/**
 * The persistent class for the G_VIEWOBJECTS database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="G_VIEWOBJECTS")
public class GViewobject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GV_ID")
	private Integer gvId;


	@Column(name="GV_METADATA_PATH")
	private String gvMetadataPath;

	@Column(name="GV_OFFGRID_VIEWNAME")
	private String gvOffgridViewname;

	@Column(name="GV_ORDER")
	private Integer gvOrder;

	

	@Column(name="GV_PROTOCOL_CODE")
	private String gvProtocolCode;

	@Column(name="GV_PROTOCOL_PATH")
	private String gvProtocolPath;

	@Column(name="GV_SORT_COLUMNS")
	private String gvSortColumns;

	@Column(name="GV_TABLENAME")
	private String gvTablename;

	@Column(name="GV_TITLE")
	private String gvTitle;

	@Column(name="GV_VIEWNAME")
	private String gvViewname;

	//bi-directional many-to-one association to GHeader
	@OneToMany(mappedBy="gViewobject",  targetEntity=GHeader.class)
	private List<GHeader> GHeaders;

	//bi-directional many-to-one association to GHeader
	@OneToMany(mappedBy="gViewobjectJob",  targetEntity=TableSynJob.class)
	private List<TableSynJob> tableSynJobs2;
	
	public List<TableSynJob> getTableSynJobs2() {
		return tableSynJobs2;
	}


	

	@OneToMany(mappedBy="gViewobject", targetEntity=GArchive.class)
	List<GArchive> gArchives;
	
	public List<GArchive> getGArchives() {
		return gArchives;
	}


	public GViewobject() {
	}

	public Integer getGvId() {
		return this.gvId;
	}

	

	
	public String getGvMetadataPath() {
		return this.gvMetadataPath;
	}

	public void setGvMetadataPath(String gvMetadataPath) {
		this.gvMetadataPath = gvMetadataPath;
	}

	public String getGvOffgridViewname() {
		return this.gvOffgridViewname;
	}

	public void setGvOffgridViewname(String gvOffgridViewname) {
		this.gvOffgridViewname = gvOffgridViewname;
	}

	public Integer getGvOrder() {
		return this.gvOrder;
	}

	public void setGvOrder(Integer gvOrder) {
		this.gvOrder = gvOrder;
	}

	

	public String getGvProtocolCode() {
		return this.gvProtocolCode;
	}

	public void setGvProtocolCode(String gvProtocolCode) {
		this.gvProtocolCode = gvProtocolCode;
	}

	public String getGvProtocolPath() {
		return this.gvProtocolPath;
	}

	public void setGvProtocolPath(String gvProtocolPath) {
		this.gvProtocolPath = gvProtocolPath;
	}

	public String getGvSortColumns() {
		return this.gvSortColumns;
	}

	public void setGvSortColumns(String gvSortColumns) {
		this.gvSortColumns = gvSortColumns;
	}

	public String getGvTablename() {
		return this.gvTablename;
	}

	public void setGvTablename(String gvTablename) {
		this.gvTablename = gvTablename;
	}

	public String getGvTitle() {
		return this.gvTitle;
	}

	public void setGvTitle(String gvTitle) {
		this.gvTitle = gvTitle;
	}

	public String getGvViewname() {
		return this.gvViewname;
	}

	public void setGvViewname(String gvViewname) {
		this.gvViewname = gvViewname;
	}

	public GHeader getGHeaders() {
		if (this.GHeaders.size() > 0){
			return this.GHeaders.get(0);
		}
		else 
			return null;
	}

	public String getFileTitle(){
		String fileTitle = getGvTitle();
		fileTitle = fileTitle.replaceAll(" ", "_");
		fileTitle = fileTitle.replaceAll("'s", "");
		fileTitle = fileTitle.replaceAll("-", "");
		fileTitle = fileTitle.replaceAll("\\(", "_");
		fileTitle = fileTitle.replaceAll("\\)", "_");
		fileTitle = fileTitle.replaceAll(",", "_");
		fileTitle = fileTitle.replaceAll("RawData", "");
		fileTitle = fileTitle.replaceAll("CompiledData", "");
	
		return fileTitle+"_";
	}


}