package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the G_HEADERS database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="G_HEADERS")
public class GHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="GH_HEADER")
	private String ghHeader;

	@Id
	@Column(name="GH_ID")
	private Integer ghId;
	//bi-directional many-to-one association to GViewobject
	
	@ManyToOne
	@JoinColumn(name="GH_GV_ID")
	private GViewobject gViewobject;

	public GHeader() {
	}

	public String getGhHeader() {
		return this.ghHeader;
	}

	public void setGhHeader(String ghHeader) {
		this.ghHeader = ghHeader;
	}

	public GViewobject getGViewobject() {
		return this.gViewobject;
	}

	public void setGViewobject(GViewobject GViewobject) {
		this.gViewobject = GViewobject;
	}

}