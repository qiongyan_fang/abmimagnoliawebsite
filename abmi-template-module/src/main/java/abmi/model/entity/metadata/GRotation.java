package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the G_ROTATIONS database table.
 * 
 */
@Entity
@Table(schema="RAWDATA", name="G_ROTATIONS")
public class GRotation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GR_ID")
	private Integer grId;
	
	@Column(name="GR_ORDER")
	private Integer grOrder;
	
	public Integer getGrOrder() {
		return grOrder;
	}

	public Integer getGrId() {
		return grId;
	}

	public void setGrId(Integer grId) {
		this.grId = grId;
	}

	@Column(name="GR_ROTATION_NAME")
	private String grRotationName;

	@Column(name="GR_END_YEAR")
	private BigDecimal grEndYear;

	@Column(name="GR_ROTATION_DESC")
	private String grRotationDesc;

	@Column(name="GR_START_YEAR")
	private BigDecimal grStartYear;

	public GRotation() {
	}

	public String getGrRotationName() {
		return this.grRotationName;
	}

	public void setGrRotationName(String grRotationName) {
		this.grRotationName = grRotationName;
	}

	public BigDecimal getGrEndYear() {
		return this.grEndYear;
	}

	public void setGrEndYear(BigDecimal grEndYear) {
		this.grEndYear = grEndYear;
	}

	public String getGrRotationDesc() {
		return this.grRotationDesc;
	}

	public void setGrRotationDesc(String grRotationDesc) {
		this.grRotationDesc = grRotationDesc;
	}

	public BigDecimal getGrStartYear() {
		return this.grStartYear;
	}

	public void setGrStartYear(BigDecimal grStartYear) {
		this.grStartYear = grStartYear;
	}

}