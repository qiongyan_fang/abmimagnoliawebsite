package abmi.model.entity.metadata;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the G_RAW_TABLE_MAPS database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="G_RAW_TABLE_MAPS")
public class GRawTableMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GRTM_ID")
	private long grtmId;

	@ManyToOne
	@JoinColumn(name="GRTM_GFDC_ID")
	private GFileDownloadCategory gFileDownloadCategory;

	

	public GViewobject getgViewobject() {
		return gViewobject;
	}

	public void setgViewobject(GViewobject gViewobject) {
		this.gViewobject = gViewobject;
	}

	@ManyToOne
	@JoinColumn(name="GRTM_GV_ID")
	private GViewobject gViewobject;

	public GFileDownloadCategory getgFileDownloadCategory() {
		return gFileDownloadCategory;
	}

	public void setgFileDownloadCategory(GFileDownloadCategory gFileDownloadCategory) {
		this.gFileDownloadCategory = gFileDownloadCategory;
	}

	@Column(name="GRTM_RAW_COMPILED")
	private String grtmRawCompiled;

	public GRawTableMap() {
	}

	public long getGrtmId() {
		return this.grtmId;
	}

	public void setGrtmId(long grtmId) {
		this.grtmId = grtmId;
	}

	

	public String getGrtmRawCompiled() {
		return this.grtmRawCompiled;
	}

	public void setGrtmRawCompiled(String grtmRawCompiled) {
		this.grtmRawCompiled = grtmRawCompiled;
	}

}