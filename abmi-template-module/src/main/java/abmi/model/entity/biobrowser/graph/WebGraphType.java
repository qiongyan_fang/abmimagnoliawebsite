package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the WEB_GRAPH_TYPES database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER",name="WEB_GRAPH_TYPES")
public class WebGraphType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGT_ID")
	private long wgtId;

	@Column(name="WGT_BACKGROUND_COLOR")
	private String wgtBackgroundColor;

	@Column(name="WGT_COMMENTS")
	private String wgtComments;

	@Column(name="WGT_DOMAIN_LABEL")
	private String wgtDomainLabel;

	@Column(name="WGT_HEIGHT")
	private Integer wgtHeight;

	@Column(name="WGT_NAME")
	private String wgtName;

	@Column(name="WGT_TITLE")
	private String wgtTitle;

	@Column(name="WGT_VALUE_LABEL")
	private String wgtValueLabel;

	@Column(name="WGT_WIDTH")
	private Integer wgtWidth;

	//bi-directional many-to-one association to WebGraphSery
	@OneToMany(mappedBy="webGraphType")
	private List<WebGraphSery> webGraphSeries;

	public WebGraphType() {
	}

	public long getWgtId() {
		return this.wgtId;
	}

	public void setWgtId(long wgtId) {
		this.wgtId = wgtId;
	}

	public String getWgtBackgroundColor() {
		return this.wgtBackgroundColor;
	}

	public void setWgtBackgroundColor(String wgtBackgroundColor) {
		this.wgtBackgroundColor = wgtBackgroundColor;
	}

	public String getWgtComments() {
		return this.wgtComments;
	}

	public void setWgtComments(String wgtComments) {
		this.wgtComments = wgtComments;
	}

	public String getWgtDomainLabel() {
		return this.wgtDomainLabel;
	}

	public void setWgtDomainLabel(String wgtDomainLabel) {
		this.wgtDomainLabel = wgtDomainLabel;
	}

	public Integer getWgtHeight() {
		return this.wgtHeight;
	}

	public void setWgtHeight(Integer wgtHeight) {
		this.wgtHeight = wgtHeight;
	}

	public String getWgtName() {
		return this.wgtName;
	}

	public void setWgtName(String wgtName) {
		this.wgtName = wgtName;
	}

	public String getWgtTitle() {
		return this.wgtTitle;
	}

	public void setWgtTitle(String wgtTitle) {
		this.wgtTitle = wgtTitle;
	}

	public String getWgtValueLabel() {
		return this.wgtValueLabel;
	}

	public void setWgtValueLabel(String wgtValueLabel) {
		this.wgtValueLabel = wgtValueLabel;
	}

	public Integer getWgtWidth() {
		return this.wgtWidth;
	}

	public void setWgtWidth(Integer wgtWidth) {
		this.wgtWidth = wgtWidth;
	}

	public List<WebGraphSery> getWebGraphSeries() {
		return this.webGraphSeries;
	}

	public void setWebGraphSeries(List<WebGraphSery> webGraphSeries) {
		this.webGraphSeries = webGraphSeries;
	}

	public WebGraphSery addWebGraphSery(WebGraphSery webGraphSery) {
		getWebGraphSeries().add(webGraphSery);
		webGraphSery.setWebGraphType(this);

		return webGraphSery;
	}

	public WebGraphSery removeWebGraphSery(WebGraphSery webGraphSery) {
		getWebGraphSeries().remove(webGraphSery);
		webGraphSery.setWebGraphType(null);

		return webGraphSery;
	}

}