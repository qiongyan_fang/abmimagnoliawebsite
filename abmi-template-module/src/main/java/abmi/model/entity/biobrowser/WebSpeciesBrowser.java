package abmi.model.entity.biobrowser;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the WEB_SPECIES_BROWSERS database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="WEB_SPECIES_BROWSERS")
public class WebSpeciesBrowser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WSB_ID")
	private long wsbId;

	@Column(name="WSB_COMMON_NAME")
	private String wsbCommonName;

	@Column(name="WSB_TSN")
	private Integer wsbTsn;
	
	public Integer getWsbTsn() {
		return wsbTsn;
	}

	public void setWsbTsn(Integer wsbTsn) {
		this.wsbTsn = wsbTsn;
	}

//	@Column(name="WSB_GROUP_NAME")
//	private String wsbGroupName;

	@Column(name="WSB_GUID")
	private BigDecimal wsbGuid;

	@Column(name="WSB_KINGDOM_ID")
	private BigDecimal wsbKingdomId;

	@Column(name="WSB_PARENT_TSN")
	private BigDecimal wsbParentTsn;

	@Column(name="WSB_RANK_ID")
	private BigDecimal wsbRankId;

	@Column(name="WSB_RANK_NAME")
	private String wsbRankName;

	@Column(name="WSB_SCIENTIFIC_NAME")
	private String wsbScientificName;

	@Column(name="WSB_TO_INCLUDED")
	private String wsbToIncluded;

	//bi-directional many-to-one association to WebSpeciesDetection
	@OneToMany(mappedBy="webSpeciesBrowser")
	private List<WebSpeciesDetection> webSpeciesDetections;

	//bi-directional many-to-one association to SppLocationType
	@ManyToOne
	@JoinColumn(name="WSB_LOCATION")
	private SppLocationType sppLocationType;

	//bi-directional many-to-one association to SppSummaryType
	@ManyToOne
	@JoinColumn(name="WSB_SUMMARY_TYPE")
	private SppSummaryType sppSummaryType;

	
	@ManyToOne
	@JoinColumn(name="WSB_SG_ID")
	private SppGroup sppGroup;
	
	public SppGroup getSppGroup() {
		return sppGroup;
	}

	public void setSppGroup(SppGroup sppGroup) {
		this.sppGroup = sppGroup;
	}

	public WebSpeciesBrowser() {
	}

	public long getWsbId() {
		return this.wsbId;
	}

	public void setWsbId(long wsbId) {
		this.wsbId = wsbId;
	}

	public String getWsbCommonName() {
		return this.wsbCommonName;
	}

	public void setWsbCommonName(String wsbCommonName) {
		this.wsbCommonName = wsbCommonName;
	}

//	public String getWsbGroupName() {
//		return this.wsbGroupName;
//	}
//
//	public void setWsbGroupName(String wsbGroupName) {
//		this.wsbGroupName = wsbGroupName;
//	}

	public BigDecimal getWsbGuid() {
		return this.wsbGuid;
	}

	public void setWsbGuid(BigDecimal wsbGuid) {
		this.wsbGuid = wsbGuid;
	}

	public BigDecimal getWsbKingdomId() {
		return this.wsbKingdomId;
	}

	public void setWsbKingdomId(BigDecimal wsbKingdomId) {
		this.wsbKingdomId = wsbKingdomId;
	}

	public BigDecimal getWsbParentTsn() {
		return this.wsbParentTsn;
	}

	public void setWsbParentTsn(BigDecimal wsbParentTsn) {
		this.wsbParentTsn = wsbParentTsn;
	}

	public BigDecimal getWsbRankId() {
		return this.wsbRankId;
	}

	public void setWsbRankId(BigDecimal wsbRankId) {
		this.wsbRankId = wsbRankId;
	}

	public String getWsbRankName() {
		return this.wsbRankName;
	}

	public void setWsbRankName(String wsbRankName) {
		this.wsbRankName = wsbRankName;
	}

	public String getWsbScientificName() {
		return this.wsbScientificName;
	}

	public void setWsbScientificName(String wsbScientificName) {
		this.wsbScientificName = wsbScientificName;
	}

	public String getWsbToIncluded() {
		return this.wsbToIncluded;
	}

	public void setWsbToIncluded(String wsbToIncluded) {
		this.wsbToIncluded = wsbToIncluded;
	}

	public List<WebSpeciesDetection> getWebSpeciesDetections() {
		return this.webSpeciesDetections;
	}

	public void setWebSpeciesDetections(List<WebSpeciesDetection> webSpeciesDetections) {
		this.webSpeciesDetections = webSpeciesDetections;
	}

	public WebSpeciesDetection addWebSpeciesDetection(WebSpeciesDetection webSpeciesDetection) {
		getWebSpeciesDetections().add(webSpeciesDetection);
		webSpeciesDetection.setWebSpeciesBrowser(this);

		return webSpeciesDetection;
	}

	public WebSpeciesDetection removeWebSpeciesDetection(WebSpeciesDetection webSpeciesDetection) {
		getWebSpeciesDetections().remove(webSpeciesDetection);
		webSpeciesDetection.setWebSpeciesBrowser(null);

		return webSpeciesDetection;
	}

	public SppLocationType getSppLocationType() {
		return this.sppLocationType;
	}

	public void setSppLocationType(SppLocationType sppLocationType) {
		this.sppLocationType = sppLocationType;
	}

	public SppSummaryType getSppSummaryType() {
		return this.sppSummaryType;
	}

	public void setSppSummaryType(SppSummaryType sppSummaryType) {
		this.sppSummaryType = sppSummaryType;
	}

}