package abmi.model.entity.biobrowser;

import java.io.Serializable;
import javax.persistence.*;



/**
 * The persistent class for the WEB_SPECIES_DETECTIONS database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="WEB_SPECIES_DETECTIONS")
public class WebSpeciesDetection implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WSD_ID")
	private long wsdId;

	@Column(name="WSD_SITE")
	private Integer wsdSite;

	//bi-directional many-to-one association to WebSpeciesBrowser
	@ManyToOne
	@JoinColumn(name="WSD_TSN", referencedColumnName="WSB_TSN")
	private WebSpeciesBrowser webSpeciesBrowser;

	public WebSpeciesDetection() {
	}

	public long getWsdId() {
		return this.wsdId;
	}

	public void setWsdId(long wsdId) {
		this.wsdId = wsdId;
	}

	public Integer getWsdSite() {
		return this.wsdSite;
	}

	public void setWsdSite(Integer wsdSite) {
		this.wsdSite = wsdSite;
	}

	public WebSpeciesBrowser getWebSpeciesBrowser() {
		return this.webSpeciesBrowser;
	}

	public void setWebSpeciesBrowser(WebSpeciesBrowser webSpeciesBrowser) {
		this.webSpeciesBrowser = webSpeciesBrowser;
	}

}