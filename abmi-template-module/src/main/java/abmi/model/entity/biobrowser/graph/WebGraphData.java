package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the WEB_GRAPH_DATAS database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER",name="WEB_GRAPH_DATAS")
public class WebGraphData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGD_ID")
	private long wgdId;

	@Column(name="WGD_IS_MAIN")
	private Integer wgdIsMain;

	@Column(name="WGD_LOWER")
	private Double wgdLower;

	@Column(name="WGD_MEAN", insertable=false, updatable=false)
	private Double wgdMean;

	@Column(name="WGD_TREE_SIZE")
	private Integer wgdTreeSize;

	@Column(name="WGD_TSN")
	private Integer wgdTsn;

	@Column(name="WGD_UPPER")
	private Double wgdUpper;

	//bi-directional many-to-one association to WebGraphSery
	@ManyToOne
	@JoinColumn(name="WGD_WGS_ID")
	private WebGraphSery webGraphSery;

	public WebGraphData() {
	}

	public long getWgdId() {
		return this.wgdId;
	}

	public void setWgdId(long wgdId) {
		this.wgdId = wgdId;
	}

	public Integer getWgdIsMain() {
		return this.wgdIsMain;
	}



	public Double getWgdLower() {
		return this.wgdLower;
	}

	public void setWgdLower(Double wgdLower) {
		this.wgdLower = wgdLower;
	}

	public Double getWgdMean() {
		return this.wgdMean;
	}

	public void setWgdMean(Double wgdMean) {
		this.wgdMean = wgdMean;
	}

	public Integer getWgdTreeSize() {
		return this.wgdTreeSize;
	}

	public void setWgdTreeSize(Integer wgdTreeSize) {
		this.wgdTreeSize = wgdTreeSize;
	}

	public Integer getWgdTsn() {
		return this.wgdTsn;
	}

	public void setWgdTsn(Integer wgdTsn) {
		this.wgdTsn = wgdTsn;
	}

	public Double getWgdUpper() {
		return this.wgdUpper;
	}

	public void setWgdUpper(Double wgdUpper) {
		this.wgdUpper = wgdUpper;
	}

	public WebGraphSery getWebGraphSery() {
		return this.webGraphSery;
	}

	public void setWebGraphSery(WebGraphSery webGraphSery) {
		this.webGraphSery = webGraphSery;
	}

}