package abmi.model.entity.biobrowser;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the SPP_SUMMARY_TYPE database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="SPP_SUMMARY_TYPE")
public class SppSummaryType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SST_CODE")
	private Integer sstCode;

	@Column(name="SST_DISPLAY")
	private String sstDisplay;

	//bi-directional many-to-one association to WebSpeciesBrowser
	@OneToMany(mappedBy="sppSummaryType")
	private List<WebSpeciesBrowser> webSpeciesBrowsers;

	public SppSummaryType() {
	}

	public Integer getSstCode() {
		return this.sstCode;
	}

	

	public String getSstDisplay() {
		return this.sstDisplay;
	}

	public void setSstDisplay(String sstDisplay) {
		this.sstDisplay = sstDisplay;
	}

	public List<WebSpeciesBrowser> getWebSpeciesBrowsers() {
		return this.webSpeciesBrowsers;
	}

	public void setWebSpeciesBrowsers(List<WebSpeciesBrowser> webSpeciesBrowsers) {
		this.webSpeciesBrowsers = webSpeciesBrowsers;
	}

	public WebSpeciesBrowser addWebSpeciesBrowser(WebSpeciesBrowser webSpeciesBrowser) {
		getWebSpeciesBrowsers().add(webSpeciesBrowser);
		webSpeciesBrowser.setSppSummaryType(this);

		return webSpeciesBrowser;
	}

	public WebSpeciesBrowser removeWebSpeciesBrowser(WebSpeciesBrowser webSpeciesBrowser) {
		getWebSpeciesBrowsers().remove(webSpeciesBrowser);
		webSpeciesBrowser.setSppSummaryType(null);

		return webSpeciesBrowser;
	}

}