package abmi.model.entity.biobrowser;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the WEB_SITE_VISITED database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="WEB_SITE_VISITED")
public class WebSiteVisited implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WSV_ID")
	private long wsvId;

	@Column(name="WSV_SITE")
	private Integer wsvSite;

	//bi-directional many-to-one association to SppGroup
	@ManyToOne
	@JoinColumn(name="WSV_SG_ID")
	private SppGroup sppGroup;

	public WebSiteVisited() {
	}

	public long getWsvId() {
		return this.wsvId;
	}

	public void setWsvId(long wsvId) {
		this.wsvId = wsvId;
	}

	public Integer getWsvSite() {
		return this.wsvSite;
	}

	public void setWsvSite(Integer wsvSite) {
		this.wsvSite = wsvSite;
	}

	public SppGroup getSppGroup() {
		return this.sppGroup;
	}

	public void setSppGroup(SppGroup sppGroup) {
		this.sppGroup = sppGroup;
	}

}