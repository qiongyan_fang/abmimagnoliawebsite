package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the WEB_GRAPH_LINEAR_DATAS database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER",name="WEB_GRAPH_LINEAR_DATAS")
public class WebGraphLinearData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGLD_ID")
	private long wgldId;

	@Column(name="WGLD_HARD")
	private Double wgldHard;

	@Column(name="WGLD_IS_NORTH")
	private Integer wgldIsNorth;

	@Column(name="WGLD_SOFT")
	private Double wgldSoft;

	@Column(name="WGLD_START")
	private Double wgldStart;

	@Column(name="WGLD_TSN")
	private Integer wgldTsn;

	public WebGraphLinearData() {
	}

	public long getWgldId() {
		return this.wgldId;
	}

	public void setWgldId(long wgldId) {
		this.wgldId = wgldId;
	}

	public Double getWgldHard() {
		return this.wgldHard;
	}

	public void setWgldHard(Double wgldHard) {
		this.wgldHard = wgldHard;
	}

	public Integer getWgldIsNorth() {
		return this.wgldIsNorth;
	}

	public void setWgldIsnorth(Integer wgldIsnorth) {
		this.wgldIsNorth = wgldIsnorth;
	}

	public Double getWgldSoft() {
		return this.wgldSoft;
	}

	public void setWgldSoft(Double wgldSoft) {
		this.wgldSoft = wgldSoft;
	}

	public Double getWgldStart() {
		return this.wgldStart;
	}

	public void setWgldStart(Double wgldStart) {
		this.wgldStart = wgldStart;
	}

	public Integer getWgldTsn() {
		return this.wgldTsn;
	}

	public void setWgldTsn(Integer wgldTsn) {
		this.wgldTsn = wgldTsn;
	}

}