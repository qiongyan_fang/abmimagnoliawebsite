package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the WEB_GRAPH_SERIES database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER",name="WEB_GRAPH_SERIES")
public class WebGraphSery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGS_ID")
	private long wgsId;

	@Column(name="WGS_COLOR")
	private String wgsColor;

	@Column(name="WGS_COMMENTS")
	private String wgsComments;

	@Column(name="WGS_LABEL")
	private String wgsLabel;

	@Column(name="WGS_ORDER")
	private Integer wgsOrder;

	//bi-directional many-to-one association to WebGraphData
	@OneToMany(mappedBy="webGraphSery")
	private List<WebGraphData> webGraphDatas;

	//bi-directional many-to-one association to WebGraphSectorArea
	@OneToMany(mappedBy="webGraphSery")
	private List<WebGraphSectorArea> webGraphSectorAreas;

	

	//bi-directional many-to-one association to WebGraphCategory
	@ManyToOne
	@JoinColumn(name="WGS_WGC_ID")
	private WebGraphCategory webGraphCategory;

	//bi-directional many-to-one association to WebGraphType
	@ManyToOne
	@JoinColumn(name="WGS_WGT_ID")
	private WebGraphType webGraphType;

	public WebGraphSery() {
	}

	public long getWgsId() {
		return this.wgsId;
	}

	public void setWgsId(long wgsId) {
		this.wgsId = wgsId;
	}

	public String getWgsColor() {
		return this.wgsColor.trim();
	}

	public void setWgsColor(String wgsColor) {
		this.wgsColor = wgsColor;
	}

	public String getWgsComments() {
		return this.wgsComments;
	}

	public void setWgsComments(String wgsComments) {
		this.wgsComments = wgsComments;
	}

	public String getWgsLabel() {
		return this.wgsLabel;
	}

	public void setWgsLabel(String wgsLabel) {
		this.wgsLabel = wgsLabel;
	}

	public Integer getWgsOrder() {
		return this.wgsOrder;
	}

	public void setWgsOrder(Integer wgsOrder) {
		this.wgsOrder = wgsOrder;
	}

	public List<WebGraphData> getWebGraphDatas() {
		return this.webGraphDatas;
	}

	public void setWebGraphDatas(List<WebGraphData> webGraphDatas) {
		this.webGraphDatas = webGraphDatas;
	}

	public WebGraphData addWebGraphData(WebGraphData webGraphData) {
		getWebGraphDatas().add(webGraphData);
		webGraphData.setWebGraphSery(this);

		return webGraphData;
	}

	public WebGraphData removeWebGraphData(WebGraphData webGraphData) {
		getWebGraphDatas().remove(webGraphData);
		webGraphData.setWebGraphSery(null);

		return webGraphData;
	}

	public List<WebGraphSectorArea> getWebGraphSectorAreas() {
		return this.webGraphSectorAreas;
	}

	public void setWebGraphSectorAreas(List<WebGraphSectorArea> webGraphSectorAreas) {
		this.webGraphSectorAreas = webGraphSectorAreas;
	}

	public WebGraphSectorArea addWebGraphSectorArea(WebGraphSectorArea webGraphSectorArea) {
		getWebGraphSectorAreas().add(webGraphSectorArea);
		webGraphSectorArea.setWebGraphSery(this);

		return webGraphSectorArea;
	}

	public WebGraphSectorArea removeWebGraphSectorArea(WebGraphSectorArea webGraphSectorArea) {
		getWebGraphSectorAreas().remove(webGraphSectorArea);
		webGraphSectorArea.setWebGraphSery(null);

		return webGraphSectorArea;
	}

	
	

	public WebGraphCategory getWebGraphCategory() {
		return this.webGraphCategory;
	}

	public void setWebGraphCategory(WebGraphCategory webGraphCategory) {
		this.webGraphCategory = webGraphCategory;
	}

	public WebGraphType getWebGraphType() {
		return this.webGraphType;
	}

	public void setWebGraphType(WebGraphType webGraphType) {
		this.webGraphType = webGraphType;
	}

}