package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the WEB_GRAPH_AVAILABLITY database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="WEB_GRAPH_AVAILABLITY")
@NamedQuery(name="WebGraphAvailablity.findAll", query="SELECT w FROM WebGraphAvailablity w")
public class WebGraphAvailablity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGA_ID")
	private long wgaId;

	@Column(name="WGA_OLD_FOREST_BIRD")
	private Integer wgaOldForestBird;

	@Column(name="WGA_TSN")
	private Integer wgaTsn;

	@Column(name="WGA_COMMENTS")
	private String wgaComments;

	//bi-directional many-to-one association to WebGraphModelType
	@ManyToOne
	@JoinColumn(name="WGA_PRAIRIE_MODEL")
	private WebGraphModelType webPrairieModel;

	//bi-directional many-to-one association to WebGraphModelType
	@ManyToOne
	@JoinColumn(name="WGA_FOREST_MODEL")
	private WebGraphModelType webForestModel;

	public WebGraphAvailablity() {
	}

	public long getWgaId() {
		return this.wgaId;
	}

	public void setWgaId(long wgaId) {
		this.wgaId = wgaId;
	}

	public Integer getWgaOldForestBird() {
		return this.wgaOldForestBird;
	}

	public void setWgaOldForestBird(Integer wgaOldForestBird) {
		this.wgaOldForestBird = wgaOldForestBird;
	}

	public Integer getWgaTsn() {
		return this.wgaTsn;
	}

	public void setWgaTsn(Integer wgaTsn) {
		this.wgaTsn = wgaTsn;
	}

	public String getWgsComments() {
		return this.wgaComments;
	}

	public void setWgsComments(String wgaComments) {
		this.wgaComments = wgaComments;
	}

	public WebGraphModelType getWebPrairieModel() {
		return webPrairieModel;
	}

	public void setWebPrairieModel(WebGraphModelType webPrairieModel) {
		this.webPrairieModel = webPrairieModel;
	}

	public WebGraphModelType getWebForestModel() {
		return webForestModel;
	}

	public void setWebForestModel(WebGraphModelType webForestModel) {
		this.webForestModel = webForestModel;
	}

	
}