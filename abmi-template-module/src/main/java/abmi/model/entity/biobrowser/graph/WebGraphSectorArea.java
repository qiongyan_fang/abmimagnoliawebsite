package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the WEB_GRAPH_SECTOR_AREAS database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER",name="WEB_GRAPH_SECTOR_AREAS")
public class WebGraphSectorArea implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGSA_ID")
	private long wgsaId;

	@Column(name="WGSA_AREA")
	private Double wgsaArea;

	@Column(name="WGSA_IS_NORTH")
	private Integer wgsaIsNorth;

	//bi-directional many-to-one association to WebGraphSery
	@ManyToOne
	@JoinColumn(name="WGSA_WGS_ID")
	private WebGraphSery webGraphSery;

	public WebGraphSectorArea() {
	}

	public long getWgsaId() {
		return this.wgsaId;
	}

	public void setWgsaId(long wgsaId) {
		this.wgsaId = wgsaId;
	}

	public Double getWgsaArea() {
		return this.wgsaArea;
	}

	public void setWgsaArea(Double wgsaArea) {
		this.wgsaArea = wgsaArea;
	}

	public Integer getWgsaIsNorth() {
		return this.wgsaIsNorth;
	}

	public void setWgsaIsNorth(Integer wgsaIsNorth) {
		this.wgsaIsNorth = wgsaIsNorth;
	}

	public WebGraphSery getWebGraphSery() {
		return this.webGraphSery;
	}

	public void setWebGraphSery(WebGraphSery webGraphSery) {
		this.webGraphSery = webGraphSery;
	}

}