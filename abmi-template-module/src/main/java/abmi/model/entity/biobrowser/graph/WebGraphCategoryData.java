package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the WEB_GRAPH_CATEGORY_DATAS database table. table
 * holds data for graphs with similar category values: such as treed non-treed
 * bars, and spp with few detection on north or south data
 */
@Entity
@Table(schema = "BIOBROWSER", name = "WEB_GRAPH_CATEGORY_DATAS")
@NamedQuery(name = "WebGraphCategoryData.findAll", query = "SELECT w FROM WebGraphCategoryData w")
public class WebGraphCategoryData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "WGCD_ID")
	private long wgcdId;

	@Column(name = "WGCD_IS_TREED")
	private Integer wgcdIsTreed;

	@Column(name = "WGCD_LOWER")
	private Double wgcdLower;

	@Column(name = "WGCD_MEAN")
	private Double wgcdMean;

	@Column(name = "WGCD_TSN")
	private Integer wgcdTsn;

	@Column(name = "WGCD_UPPER")
	private Double wgcdUpper;

	// bi-directional many-to-one association to WebGraphSery
	@ManyToOne
	@JoinColumn(name = "WGCD_WGS_ID")
	private WebGraphSery webGraphSery;

	public WebGraphCategoryData() {
	}

	public long getWgcdId() {
		return this.wgcdId;
	}

	public void setWgcdId(long wgcdId) {
		this.wgcdId = wgcdId;
	}

	public Integer getWgcdIsTreed() {
		return this.wgcdIsTreed;
	}

	public void setWgcdIsTreed(Integer wgcdIsTreed) {
		this.wgcdIsTreed = wgcdIsTreed;
	}

	public Double getWgcdLower() {
		return this.wgcdLower;
	}

	public void setWgcdLower(Double wgcdLower) {
		this.wgcdLower = wgcdLower;
	}

	public Double getWgcdMean() {
		return this.wgcdMean;
	}

	public void setWgcdMean(Double wgcdMean) {
		this.wgcdMean = wgcdMean;
	}

	public Integer getWgcdTsn() {
		return this.wgcdTsn;
	}

	public void setWgcdTsn(Integer wgcdTsn) {
		this.wgcdTsn = wgcdTsn;
	}

	public Double getWgcdUpper() {
		return this.wgcdUpper;
	}

	public void setWgcdUpper(Double wgcdUpper) {
		this.wgcdUpper = wgcdUpper;
	}

	public WebGraphSery getWebGraphSery() {
		return webGraphSery;
	}

	public void setWebGraphSery(WebGraphSery webGraphSery) {
		this.webGraphSery = webGraphSery;
	}

}