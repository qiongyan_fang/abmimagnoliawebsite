package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the WEB_GRAPH_CATEGORY_DETAILS database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER",name="WEB_GRAPH_CATEGORY_DETAILS")
public class WebGraphCategoryDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGCD_ID")
	private long wgcdId;

	@Column(name="WGC_COMMENTS")
	private String wgcComments;

	@Column(name="WGC_LABEL")
	private String wgcLabel;

	//bi-directional many-to-one association to WebGraphCategory
	@ManyToOne
	@JoinColumn(name="WGC_WGC_ID")
	private WebGraphCategory webGraphCategory;

	public WebGraphCategoryDetail() {
	}

	public long getWgcdId() {
		return this.wgcdId;
	}

	public void setWgcdId(long wgcdId) {
		this.wgcdId = wgcdId;
	}

	public String getWgcComments() {
		return this.wgcComments;
	}

	public void setWgcComments(String wgcComments) {
		this.wgcComments = wgcComments;
	}

	public String getWgcLabel() {
		return this.wgcLabel;
	}

	public void setWgcLabel(String wgcLabel) {
		this.wgcLabel = wgcLabel;
	}

	public WebGraphCategory getWebGraphCategory() {
		return this.webGraphCategory;
	}

	public void setWebGraphCategory(WebGraphCategory webGraphCategory) {
		this.webGraphCategory = webGraphCategory;
	}

}