package abmi.model.entity.biobrowser;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the SPP_DETECTION_COUNTS database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="SPP_DETECTION_COUNTS")
@NamedQuery(name="SppDetectionCount.findAll", query="SELECT s FROM SppDetectionCount s")
public class SppDetectionCount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SPP_GROUP_ID")
	private Long sppGroupId;

	@Column(name="SPP_COUNT")
	private Long sppCount;

	@Column(name="SPP_GROUP")
	private String sppGroup;

	public SppDetectionCount() {
	}

	public Long getSppGroupId() {
		return this.sppGroupId;
	}

	public void setSppGroupId(Long sppGroupId) {
		this.sppGroupId = sppGroupId;
	}

	public Long getSppCount() {
		return this.sppCount;
	}

	public void setSppCount(Long sppCount) {
		this.sppCount = sppCount;
	}

	public String getSppGroup() {
		return this.sppGroup;
	}

	public void setSppGroup(String sppGroup) {
		this.sppGroup = sppGroup;
	}

}