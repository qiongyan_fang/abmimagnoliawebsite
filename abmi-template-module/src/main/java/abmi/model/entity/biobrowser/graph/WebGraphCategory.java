package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the WEB_GRAPH_CATEGORIES database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER",name="WEB_GRAPH_CATEGORIES")
public class WebGraphCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGC_ID")
	private long wgcId;

	@Column(name="WGC_COMMENTS")
	private String wgcComments;

	@Column(name="WGC_LABEL")
	private String wgcLabel;

	//bi-directional many-to-one association to WebGraphCategoryDetail
	@OneToMany(mappedBy="webGraphCategory")
	private List<WebGraphCategoryDetail> webGraphCategoryDetails;

	//bi-directional many-to-one association to WebGraphSery
	@OneToMany(mappedBy="webGraphCategory")
	private List<WebGraphSery> webGraphSeries;

	public WebGraphCategory() {
	}

	public long getWgcId() {
		return this.wgcId;
	}

	public void setWgcId(long wgcId) {
		this.wgcId = wgcId;
	}

	public String getWgcComments() {
		return this.wgcComments;
	}

	public void setWgcComments(String wgcComments) {
		this.wgcComments = wgcComments;
	}

	public String getWgcLabel() {
		return this.wgcLabel;
	}

	public void setWgcLabel(String wgcLabel) {
		this.wgcLabel = wgcLabel;
	}

	public List<WebGraphCategoryDetail> getWebGraphCategoryDetails() {
		return this.webGraphCategoryDetails;
	}

	public void setWebGraphCategoryDetails(List<WebGraphCategoryDetail> webGraphCategoryDetails) {
		this.webGraphCategoryDetails = webGraphCategoryDetails;
	}

	public WebGraphCategoryDetail addWebGraphCategoryDetail(WebGraphCategoryDetail webGraphCategoryDetail) {
		getWebGraphCategoryDetails().add(webGraphCategoryDetail);
		webGraphCategoryDetail.setWebGraphCategory(this);

		return webGraphCategoryDetail;
	}

	public WebGraphCategoryDetail removeWebGraphCategoryDetail(WebGraphCategoryDetail webGraphCategoryDetail) {
		getWebGraphCategoryDetails().remove(webGraphCategoryDetail);
		webGraphCategoryDetail.setWebGraphCategory(null);

		return webGraphCategoryDetail;
	}

	public List<WebGraphSery> getWebGraphSeries() {
		return this.webGraphSeries;
	}

	public void setWebGraphSeries(List<WebGraphSery> webGraphSeries) {
		this.webGraphSeries = webGraphSeries;
	}

	public WebGraphSery addWebGraphSery(WebGraphSery webGraphSery) {
		getWebGraphSeries().add(webGraphSery);
		webGraphSery.setWebGraphCategory(this);

		return webGraphSery;
	}

	public WebGraphSery removeWebGraphSery(WebGraphSery webGraphSery) {
		getWebGraphSeries().remove(webGraphSery);
		webGraphSery.setWebGraphCategory(null);

		return webGraphSery;
	}

}