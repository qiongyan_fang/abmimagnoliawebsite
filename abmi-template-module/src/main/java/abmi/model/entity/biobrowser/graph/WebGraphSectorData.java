package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the WEB_GRAPH_SECTOR_DATA database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER",name="WEB_GRAPH_SECTOR_DATA")
public class WebGraphSectorData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGSD_ID")
	private long wgsdId;

	
	@Column(name="WGSD_TSN")
	private Integer wgsdTsn;

	@Column(name="WGSD_UNITEFFECT")
	private Double wgsdUniteffect;

	//bi-directional many-to-one association to WebGraphSery
	@ManyToOne
	@JoinColumn(name="WGSD_WGSA_ID")
	private WebGraphSectorArea webGraphSectorArea;

	public WebGraphSectorData() {
	}

	public long getWgsdId() {
		return this.wgsdId;
	}

	public void setWgsdId(long wgsdId) {
		this.wgsdId = wgsdId;
	}



	public Integer getWgsdTsn() {
		return this.wgsdTsn;
	}

	public void setWgsdTsn(Integer wgsdTsn) {
		this.wgsdTsn = wgsdTsn;
	}

	public Double getWgsdUniteffect() {
		return this.wgsdUniteffect;
	}

	public void setWgsdUniteffect(Double wgsdUniteffect) {
		this.wgsdUniteffect = wgsdUniteffect;
	}

	
	public WebGraphSectorArea getWebGraphSectorArea() {
		return webGraphSectorArea;
	}

	

}