package abmi.model.entity.biobrowser.graph;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the WEB_GRAPH_MODEL_TYPES database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="WEB_GRAPH_MODEL_TYPES")
@NamedQuery(name="WebGraphModelType.findAll", query="SELECT w FROM WebGraphModelType w")
public class WebGraphModelType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGMT_ID")
	private long wgmtId;

	@Column(name="WGMT_CODE")
	private String wgmtCode;

	@Column(name="WGMT_NOTES")
	private String wgmtNotes;

	//bi-directional many-to-one association to WebGraphAvailablity
	@OneToMany(mappedBy="webForestModel")
	private List<WebGraphAvailablity> webForestModel;

	//bi-directional many-to-one association to WebGraphAvailablity
	@OneToMany(mappedBy="webPrairieModel")
	private List<WebGraphAvailablity> webPrairieModel;

	public WebGraphModelType() {
	}

	public long getWgmtId() {
		return this.wgmtId;
	}

	public void setWgmtId(long wgmtId) {
		this.wgmtId = wgmtId;
	}

	public String getWgmtCode() {
		return this.wgmtCode;
	}

	public void setWgmtCode(String wgmtCode) {
		this.wgmtCode = wgmtCode;
	}

	public String getWgmtNotes() {
		return this.wgmtNotes;
	}

	public void setWgmtNotes(String wgmtNotes) {
		this.wgmtNotes = wgmtNotes;
	}

	

}