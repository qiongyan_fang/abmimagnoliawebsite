package abmi.model.entity.biobrowser;

import java.io.Serializable;
import javax.persistence.*;



/**
 * The persistent class for the WEB_SPECIES_VIEW_V database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="WEB_SPECIES_VIEW_V")
@NamedQuery(name="WebSpeciesViewV.findAll", query="SELECT w FROM WebSpeciesViewV w")
public class WebSpeciesViewV implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WGA_ID")
	private Integer wgaId;

	
	@Column(name="WGA_SPP_GROUP_ID")
	private Integer wgaSppGroupId;

	@Column(name="WSB_TSN")
	private Integer wsbTsn;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getWsbTsn() {
		return wsbTsn;
	}

	public Integer getWgaMaps() {
		return wgaMaps;
	}

	@Column(name="WGA_COMMENTS")
	private String wgaComments;

	@Column(name="WGA_FOREST_MODEL")
	private Integer wgaForestModel;

	@Column(name="WGA_PRAIRIE_MODEL")
	private Integer wgaPrairieModel;

	@Column(name="WSB_COMMON_NAME")
	private String wsbCommonName;

	@Column(name="WSB_SCIENTIFIC_NAME")
	private String wsbScientificName;

	@Column(name="WGA_MAPS")
	private Integer wgaMaps;

	
	
	@ManyToOne
	@JoinColumn(name="WSB_SG_ID")
	private SppGroup sppGroup;
	

	
	@Column(name="SUMMARY_TYPE")
	private String summaryType;
	

	public String getSummaryType() {
		return summaryType;
	}

	public WebSpeciesViewV() {
	}

	public Integer getWgaId() {
		return this.wgaId;
	}

	public void setWgaId(Integer wgaId) {
		this.wgaId = wgaId;
	}

	
	public Integer getWgaSppGroupId() {
		return this.wgaSppGroupId;
	}

	public void setWgaSppGroupId(Integer wgaSppGroupId) {
		this.wgaSppGroupId = wgaSppGroupId;
	}


	public String getWgaComments() {
		return wgaComments;
	}

	public Integer getWgaForestModel() {
		return wgaForestModel;
	}

	public Integer getWgaPrairieModel() {
		return wgaPrairieModel;
	}

	public String getWsbCommonName() {
		return this.wsbCommonName;
	}

	public void setWsbCommonName(String wsbCommonName) {
		this.wsbCommonName = wsbCommonName;
	}

	public String getWsbScientificName() {
		return this.wsbScientificName;
	}

	public void setWsbScientificName(String wsbScientificName) {
		this.wsbScientificName = wsbScientificName;
	}


	public SppGroup getSppGroup() {
		return sppGroup;
	}

	

}