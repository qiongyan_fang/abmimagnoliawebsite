package abmi.model.entity.biobrowser;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the SPP_GROUPS database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="SPP_GROUPS")
public class SppGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SG_ID")
	private Integer sgId;

	@Column(name="SG_GROUP_NAME")
	private String sgGroupName;

	//bi-directional many-to-one association to WebSiteVisited
	@OneToMany(mappedBy="sppGroup")
	private List<WebSiteVisited> webSiteVisiteds;
	
	
	//bi-directional many-to-one association to WebSpeciesBrowser
	@OneToMany(mappedBy="sppGroup")
	private List<WebSpeciesBrowser> webSpeciesBrowser;

	public SppGroup() {
	}

	public Integer getSgId() {
		return this.sgId;
	}

	
	public String getSgGroupName() {
		return this.sgGroupName;
	}

	public void setSgGroupName(String sgGroupName) {
		this.sgGroupName = sgGroupName;
	}

	public List<WebSiteVisited> getWebSiteVisiteds() {
		return this.webSiteVisiteds;
	}

	public void setWebSiteVisiteds(List<WebSiteVisited> webSiteVisiteds) {
		this.webSiteVisiteds = webSiteVisiteds;
	}

	public WebSiteVisited addWebSiteVisited(WebSiteVisited webSiteVisited) {
		getWebSiteVisiteds().add(webSiteVisited);
		webSiteVisited.setSppGroup(this);

		return webSiteVisited;
	}

	public WebSiteVisited removeWebSiteVisited(WebSiteVisited webSiteVisited) {
		getWebSiteVisiteds().remove(webSiteVisited);
		webSiteVisited.setSppGroup(null);

		return webSiteVisited;
	}

}