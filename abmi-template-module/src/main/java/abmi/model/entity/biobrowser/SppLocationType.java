package abmi.model.entity.biobrowser;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the SPP_LOCATION_TYPES database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="SPP_LOCATION_TYPES")
public class SppLocationType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SLT_CODE")
	private Integer sltCode;

	@Column(name="SLT_DISPLAY")
	private String sltDisplay;

	//bi-directional many-to-one association to WebSpeciesBrowser
	@OneToMany(mappedBy="sppLocationType")
	private List<WebSpeciesBrowser> webSpeciesBrowsers;

	public SppLocationType() {
	}

	public Integer getSltCode() {
		return this.sltCode;
	}

	
	public String getSltDisplay() {
		return this.sltDisplay;
	}

	public void setSltDisplay(String sltDisplay) {
		this.sltDisplay = sltDisplay;
	}

	public List<WebSpeciesBrowser> getWebSpeciesBrowsers() {
		return this.webSpeciesBrowsers;
	}

	public void setWebSpeciesBrowsers(List<WebSpeciesBrowser> webSpeciesBrowsers) {
		this.webSpeciesBrowsers = webSpeciesBrowsers;
	}

	public WebSpeciesBrowser addWebSpeciesBrowser(WebSpeciesBrowser webSpeciesBrowser) {
		getWebSpeciesBrowsers().add(webSpeciesBrowser);
		webSpeciesBrowser.setSppLocationType(this);

		return webSpeciesBrowser;
	}

	public WebSpeciesBrowser removeWebSpeciesBrowser(WebSpeciesBrowser webSpeciesBrowser) {
		getWebSpeciesBrowsers().remove(webSpeciesBrowser);
		webSpeciesBrowser.setSppLocationType(null);

		return webSpeciesBrowser;
	}

}