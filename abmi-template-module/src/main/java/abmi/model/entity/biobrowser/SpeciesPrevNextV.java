package abmi.model.entity.biobrowser;

import java.io.Serializable;
import javax.persistence.*;



/**
 * The persistent class for the SPECIES_PREV_NEXT_V database table.
 * 
 */
@Entity
@Table(schema="BIOBROWSER", name="SPECIES_PREV_NEXT_V")
@NamedQuery(name="SpeciesPrevNextV.findAll", query="SELECT s FROM SpeciesPrevNextV s")
public class SpeciesPrevNextV implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="NEXT_TSN")
	private Integer nextTsn;

	@Column(name="PREVIOUS_TSN")
	private Integer previousTsn;

	@Column(name="ROW_ASC")
	private Integer rowAsc;

	@Column(name="ROW_DESC")
	private Integer rowDesc;

	@Id
	@Column(name="WGA_ID")
	private Integer wgaId;

	@Column(name="WGA_SPP_GROUP_ID")
	private Integer wgaSppGroupId;

	@Column(name="WSB_TSN")
	private Integer wsbTsn;

	@Column(name="WSB_COMMON_NAME")
	private String wsbCommonName;

	@Column(name="WSB_SCIENTIFIC_NAME")
	private String wsbScientificName;

	public SpeciesPrevNextV() {
	}

	public Integer getNextTsn() {
		return this.nextTsn;
	}

	public void setNextTsn(Integer nextTsn) {
		this.nextTsn = nextTsn;
	}

	public Integer getPreviousTsn() {
		return this.previousTsn;
	}

	public void setPreviousTsn(Integer previousTsn) {
		this.previousTsn = previousTsn;
	}

	public Integer getRowAsc() {
		return this.rowAsc;
	}

	public void setRowAsc(Integer rowAsc) {
		this.rowAsc = rowAsc;
	}

	public Integer getRowDesc() {
		return this.rowDesc;
	}

	public void setRowDesc(Integer rowDesc) {
		this.rowDesc = rowDesc;
	}

	public Integer getWgaId() {
		return this.wgaId;
	}

	public void setWgaId(Integer wgaId) {
		this.wgaId = wgaId;
	}

	public Integer getWgaSppGroupId() {
		return this.wgaSppGroupId;
	}

	public void setWgaSppGroupId(Integer wgaSppGroupId) {
		this.wgaSppGroupId = wgaSppGroupId;
	}

	
	public String getWsbCommonName() {
		return this.wsbCommonName;
	}

	public void setWsbCommonName(String wsbCommonName) {
		this.wsbCommonName = wsbCommonName;
	}

	public String getWsbScientificName() {
		return this.wsbScientificName;
	}

	public void setWsbScientificName(String wsbScientificName) {
		this.wsbScientificName = wsbScientificName;
	}

}