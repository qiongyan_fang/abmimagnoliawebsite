package abmi.model.entity.internal;

import java.io.Serializable;
import javax.persistence.*;

import abmi.model.entity.metadata.GViewobject;




/**
 * The persistent class for the TABLE_SYN_JOBS database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="TABLE_SYN_JOBS")
public class TableSynJob implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="TSJ_ID")
	private long tsjId;

	@Column(name="TSJ_COMMENTS")
	private String tsjComments;

	
	@ManyToOne
	@JoinColumn(name="TSJ_GV_ID")
	private GViewobject gViewobjectJob;
	
	
	@Column(name="TSJ_JOB_NUMBER")
	private Integer tsjJobNumber;

	public TableSynJob() {
	}

	public long getTsjId() {
		return this.tsjId;
	}

	public void setTsjId(long tsjId) {
		this.tsjId = tsjId;
	}

	public String getTsjComments() {
		return this.tsjComments;
	}

	public void setTsjComments(String tsjComments) {
		this.tsjComments = tsjComments;
	}



	
	public Integer getTsjJobNumber() {
		return this.tsjJobNumber;
	}

	public void setTsjJobNumber(Integer tsjJobNumber) {
		this.tsjJobNumber = tsjJobNumber;
	}

}