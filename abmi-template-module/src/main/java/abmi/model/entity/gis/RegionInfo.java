package abmi.model.entity.gis;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the REGION_INFOS database table.
 * 
 */
@Entity
@Table(schema="GIS", name="REGION_INFOS")
@NamedQuery(name="RegionInfo.findAll", query="SELECT r FROM RegionInfo r")
public class RegionInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RI_ID")
	private Integer riId;

	@Column(name="RI_AREA")
	private BigDecimal riArea;

	@Column(name="RI_ICON")
	private String riIcon;

	@Column(name="RI_NAME")
	private String riName;

//	@Column(name="RI_PARENT_ID")
//	private BigDecimal riParentId;
	
	@ManyToOne
	@JoinColumn(name="RI_PARENT_ID")
	private RegionInfo regionParent;

	@Column(name="RI_PROTECTED_AREA")
	private BigDecimal riProtectedArea;

	@Column(name="RI_PROTECTED_PERCENT")
	private BigDecimal riProtectedPercent;

	@Column(name="RI_SITE_NO")
	private BigDecimal riSiteNo;

	//bi-directional many-to-one association to RegionLayer
	@ManyToOne
	@JoinColumn(name="RI_RL_ID")
	private RegionLayer regionLayer;

	//bi-directional many-to-one association to RegionSiteMap
	@OneToMany(mappedBy="regionInfo")
	private List<RegionSiteMap> regionSiteMaps;

	public RegionInfo() {
	}

	public Integer getRiId() {
		return this.riId;
	}

	public void setRiId(Integer riId) {
		this.riId = riId;
	}

	public BigDecimal getRiArea() {
		return this.riArea;
	}

	public void setRiArea(BigDecimal riArea) {
		this.riArea = riArea;
	}

	public String getRiIcon() {
		return this.riIcon;
	}

	public void setRiIcon(String riIcon) {
		this.riIcon = riIcon;
	}

	public String getRiName() {
		return this.riName;
	}

	public void setRiName(String riName) {
		this.riName = riName;
	}

//	public BigDecimal getRiParentId() {
//		return this.riParentId;
//	}
//
//	public void setRiParentId(BigDecimal riParentId) {
//		this.riParentId = riParentId;
//	}

	public BigDecimal getRiProtectedArea() {
		return this.riProtectedArea;
	}

	public RegionInfo getRegionParent() {
		return regionParent;
	}

	public void setRegionParent(RegionInfo regionParent) {
		this.regionParent = regionParent;
	}

	public void setRiProtectedArea(BigDecimal riProtectedArea) {
		this.riProtectedArea = riProtectedArea;
	}

	public BigDecimal getRiProtectedPercent() {
		return this.riProtectedPercent;
	}

	public void setRiProtectedPercent(BigDecimal riProtectedPercent) {
		this.riProtectedPercent = riProtectedPercent;
	}

	public BigDecimal getRiSiteNo() {
		return this.riSiteNo;
	}

	public void setRiSiteNo(BigDecimal riSiteNo) {
		this.riSiteNo = riSiteNo;
	}

	public RegionLayer getRegionLayer() {
		return this.regionLayer;
	}

	public void setRegionLayer(RegionLayer regionLayer) {
		this.regionLayer = regionLayer;
	}

	public List<RegionSiteMap> getRegionSiteMaps() {
		return this.regionSiteMaps;
	}

	public void setRegionSiteMaps(List<RegionSiteMap> regionSiteMaps) {
		this.regionSiteMaps = regionSiteMaps;
	}

	public RegionSiteMap addRegionSiteMap(RegionSiteMap regionSiteMap) {
		getRegionSiteMaps().add(regionSiteMap);
		regionSiteMap.setRegionInfo(this);

		return regionSiteMap;
	}

	public RegionSiteMap removeRegionSiteMap(RegionSiteMap regionSiteMap) {
		getRegionSiteMaps().remove(regionSiteMap);
		regionSiteMap.setRegionInfo(null);

		return regionSiteMap;
	}

}