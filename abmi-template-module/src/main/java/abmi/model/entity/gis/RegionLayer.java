package abmi.model.entity.gis;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the REGION_LAYERS database table.
 * 
 */
@Entity
@Table(name="REGION_LAYERS")
@NamedQuery(name="RegionLayer.findAll", query="SELECT r FROM RegionLayer r")
public class RegionLayer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RL_ID")
	private long rlId;

	@Column(name="RL_ICON")
	private String rlIcon;

	@Column(name="RL_PICKED")
	private Integer rlPicked;

	@Column(name="RL_WHOLE_ALBERTA")
	private Integer rlWholeAlberta;
	
	@Column(name="RL_DESCRIPTION")
	private String rlDescription;
	
	public String getRlDescription() {
		return rlDescription;
	}

	public void setRlDescription(String rlDescription) {
		this.rlDescription = rlDescription;
	}

	public Integer getRlWholeAlberta() {
		return rlWholeAlberta;
	}

	public void setRlWholeAlberta(Integer rlWholeAlberta) {
		this.rlWholeAlberta = rlWholeAlberta;
	}

	public Integer getRlPicked() {
		return rlPicked;
	}

	public void setRlPicked(Integer rlPicked) {
		this.rlPicked = rlPicked;
	}

	@Column(name="RL_LAYERID")
	private String rlLayerid;

	@Column(name="RL_LEVEL")
	private BigDecimal rlLevel;

	@Column(name="RL_NAME")
	private String rlName;

	@Column(name="RL_ORDER")
	private BigDecimal rlOrder;

	@Column(name="Z_RL_PARENT_ID")
	private BigDecimal zRlParentId;

	//bi-directional many-to-one association to RegionInfo
	@OneToMany(mappedBy="regionLayer")
	private List<RegionInfo> regionInfos;

	public RegionLayer() {
	}

	public long getRlId() {
		return this.rlId;
	}

	public void setRlId(long rlId) {
		this.rlId = rlId;
	}

	public String getRlIcon() {
		return this.rlIcon;
	}

	public void setRlIcon(String rlIcon) {
		this.rlIcon = rlIcon;
	}

	public String getRlLayerid() {
		return this.rlLayerid;
	}

	public void setRlLayerid(String rlLayerid) {
		this.rlLayerid = rlLayerid;
	}

	public BigDecimal getRlLevel() {
		return this.rlLevel;
	}

	public void setRlLevel(BigDecimal rlLevel) {
		this.rlLevel = rlLevel;
	}

	public String getRlName() {
		return this.rlName;
	}

	public void setRlName(String rlName) {
		this.rlName = rlName;
	}

	public BigDecimal getRlOrder() {
		return this.rlOrder;
	}

	public void setRlOrder(BigDecimal rlOrder) {
		this.rlOrder = rlOrder;
	}

	public BigDecimal getZRlParentId() {
		return this.zRlParentId;
	}

	public void setZRlParentId(BigDecimal zRlParentId) {
		this.zRlParentId = zRlParentId;
	}

	public List<RegionInfo> getRegionInfos() {
		return this.regionInfos;
	}

	public void setRegionInfos(List<RegionInfo> regionInfos) {
		this.regionInfos = regionInfos;
	}

	public RegionInfo addRegionInfo(RegionInfo regionInfo) {
		getRegionInfos().add(regionInfo);
		regionInfo.setRegionLayer(this);

		return regionInfo;
	}

	public RegionInfo removeRegionInfo(RegionInfo regionInfo) {
		getRegionInfos().remove(regionInfo);
		regionInfo.setRegionLayer(null);

		return regionInfo;
	}

}