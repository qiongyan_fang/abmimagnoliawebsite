package abmi.model.entity.gis;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the REGION_SITE_MAPS database table.
 * 
 */
@Entity
@Table(name="REGION_SITE_MAPS")
@NamedQuery(name="RegionSiteMap.findAll", query="SELECT r FROM RegionSiteMap r")
public class RegionSiteMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RSM_ID")
	private long rsmId;

	@Column(name="RSM_SITE_ID")
	private Integer rsmSiteId;

	//bi-directional many-to-one association to RegionInfo
	@ManyToOne
	@JoinColumn(name="RSM_RI_ID")
	private RegionInfo regionInfo;

	public RegionSiteMap() {
	}

	public long getRsmId() {
		return this.rsmId;
	}

	public void setRsmId(long rsmId) {
		this.rsmId = rsmId;
	}

	public Integer getRsmSiteId() {
		return this.rsmSiteId;
	}

	public void setRsmSiteId(Integer rsmSiteId) {
		this.rsmSiteId = rsmSiteId;
	}

	public RegionInfo getRegionInfo() {
		return this.regionInfo;
	}

	public void setRegionInfo(RegionInfo regionInfo) {
		this.regionInfo = regionInfo;
	}

}