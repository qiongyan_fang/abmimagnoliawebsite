package abmi.model.entity.guild;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the SPECIES_GROUPS database table.
 * 
 */
@Entity
@Table(schema="WORKBENCH", name="SPECIES_GROUPS")
@NamedQuery(name="SpeciesGroup.findAll", query="SELECT s FROM SpeciesGroup s")
public class SpeciesGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SG_ID")
	private long sgId;

	@Column(name="SG_NAME")
	private String sgName;

	//bi-directional many-to-one association to SppGuildDefinitionGroup
	@OneToMany(mappedBy="speciesGroup")
	private List<SppGuildDefinitionGroup> sppGuildDefinitionGroups;

	public SpeciesGroup() {
	}

	public long getSgId() {
		return this.sgId;
	}

	public void setSgId(long sgId) {
		this.sgId = sgId;
	}

	public String getSgName() {
		return this.sgName;
	}

	public void setSgName(String sgName) {
		this.sgName = sgName;
	}

	public List<SppGuildDefinitionGroup> getSppGuildDefinitionGroups() {
		return this.sppGuildDefinitionGroups;
	}

	public void setSppGuildDefinitionGroups(List<SppGuildDefinitionGroup> sppGuildDefinitionGroups) {
		this.sppGuildDefinitionGroups = sppGuildDefinitionGroups;
	}

	public SppGuildDefinitionGroup addSppGuildDefinitionGroup(SppGuildDefinitionGroup sppGuildDefinitionGroup) {
		getSppGuildDefinitionGroups().add(sppGuildDefinitionGroup);
		sppGuildDefinitionGroup.setSpeciesGroup(this);

		return sppGuildDefinitionGroup;
	}

	public SppGuildDefinitionGroup removeSppGuildDefinitionGroup(SppGuildDefinitionGroup sppGuildDefinitionGroup) {
		getSppGuildDefinitionGroups().remove(sppGuildDefinitionGroup);
		sppGuildDefinitionGroup.setSpeciesGroup(null);

		return sppGuildDefinitionGroup;
	}

}