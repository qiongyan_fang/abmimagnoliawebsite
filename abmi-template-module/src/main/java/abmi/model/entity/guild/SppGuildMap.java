package abmi.model.entity.guild;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the SPP_GUILD_MAPS database table.
 * 
 */
@Entity
@Table(schema="WORKBENCH", name="SPP_GUILD_MAPS")
@NamedQuery(name="SppGuildMap.findAll", query="SELECT s FROM SppGuildMap s")
public class SppGuildMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SGM_ID")
	private long sgmId;

	@Column(name="SGM_CACHED_VALUE")
	private String sgmCachedValue;

	@Column(name="SGM_SCIENTIFIC_NAME_ORG")
	private String sgmScientificNameOrg;

	@Column(name="SGM_TSN_ORG")
	private Integer sgmTsnOrg;

	//bi-directional many-to-one association to SpeciesGuildDefinition
	@ManyToOne
	@JoinColumn(name="SGM_SGD_ID")
	private SpeciesGuildDefinition speciesGuildDefinition;

	//bi-directional many-to-one association to SppGuildValue
	@OneToMany(mappedBy="sppGuildMap")
	private List<SppGuildValue> sppGuildValues;

	//bi-directional many-to-one association to AbmiTaxonomicUnit
	@ManyToOne
	@JoinColumn(name="SGM_GUID")
	private AbmiTaxonomicUnit abmiTaxonomicUnit;

	public SppGuildMap() {
	}

	public long getSgmId() {
		return this.sgmId;
	}

	public void setSgmId(long sgmId) {
		this.sgmId = sgmId;
	}

	public String getSgmCachedValue() {
		return this.sgmCachedValue;
	}

	public void setSgmCachedValue(String sgmCachedValue) {
		this.sgmCachedValue = sgmCachedValue;
	}

	public String getSgmScientificNameOrg() {
		return this.sgmScientificNameOrg;
	}

	public void setSgmScientificNameOrg(String sgmScientificNameOrg) {
		this.sgmScientificNameOrg = sgmScientificNameOrg;
	}

	public Integer getSgmTsnOrg() {
		return this.sgmTsnOrg;
	}

	public void setSgmTsnOrg(Integer sgmTsnOrg) {
		this.sgmTsnOrg = sgmTsnOrg;
	}

	public SpeciesGuildDefinition getSpeciesGuildDefinition() {
		return this.speciesGuildDefinition;
	}

	public void setSpeciesGuildDefinition(SpeciesGuildDefinition speciesGuildDefinition) {
		this.speciesGuildDefinition = speciesGuildDefinition;
	}

	public List<SppGuildValue> getSppGuildValues() {
		return this.sppGuildValues;
	}

	public void setSppGuildValues(List<SppGuildValue> sppGuildValues) {
		this.sppGuildValues = sppGuildValues;
	}

	public SppGuildValue addSppGuildValue(SppGuildValue sppGuildValue) {
		getSppGuildValues().add(sppGuildValue);
		sppGuildValue.setSppGuildMap(this);

		return sppGuildValue;
	}

	public SppGuildValue removeSppGuildValue(SppGuildValue sppGuildValue) {
		getSppGuildValues().remove(sppGuildValue);
		sppGuildValue.setSppGuildMap(null);

		return sppGuildValue;
	}

	public AbmiTaxonomicUnit getAbmiTaxonomicUnit() {
		return this.abmiTaxonomicUnit;
	}

	public void setAbmiTaxonomicUnit(AbmiTaxonomicUnit abmiTaxonomicUnit) {
		this.abmiTaxonomicUnit = abmiTaxonomicUnit;
	}

}