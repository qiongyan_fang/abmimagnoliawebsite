package abmi.model.entity.guild;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the SPP_GUILD_VALUES database table.
 * 
 */
@Entity
@Table(schema="WORKBENCH", name="SPP_GUILD_VALUES")
@NamedQuery(name="SppGuildValue.findAll", query="SELECT s FROM SppGuildValue s")
public class SppGuildValue implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SGV_ID")
	private long sgvId;

	@Column(name="SGV_VALUE")
	private String sgvValue;

	//bi-directional many-to-one association to SppGuildDefinitionOption
	@ManyToOne
	@JoinColumn(name="SGV_SGDO_ID")
	private SppGuildDefinitionOption sppGuildDefinitionOption;

	//bi-directional many-to-one association to SppGuildMap
	@ManyToOne
	@JoinColumn(name="SGV_SGM_ID")
	private SppGuildMap sppGuildMap;

	public SppGuildValue() {
	}

	public long getSgvId() {
		return this.sgvId;
	}

	public void setSgvId(long sgvId) {
		this.sgvId = sgvId;
	}

	public String getSgvValue() {
		return this.sgvValue;
	}

	public void setSgvValue(String sgvValue) {
		this.sgvValue = sgvValue;
	}

	public SppGuildDefinitionOption getSppGuildDefinitionOption() {
		return this.sppGuildDefinitionOption;
	}

	public void setSppGuildDefinitionOption(SppGuildDefinitionOption sppGuildDefinitionOption) {
		this.sppGuildDefinitionOption = sppGuildDefinitionOption;
	}

	public SppGuildMap getSppGuildMap() {
		return this.sppGuildMap;
	}

	public void setSppGuildMap(SppGuildMap sppGuildMap) {
		this.sppGuildMap = sppGuildMap;
	}

}