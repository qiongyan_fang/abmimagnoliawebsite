package abmi.model.entity.guild;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the SPECIES_GUILD_DEFINITIONS database table.
 * 
 */
@Entity
@Table(schema="WORKBENCH", name="SPECIES_GUILD_DEFINITIONS")
@NamedQuery(name="SpeciesGuildDefinition.findAll", query="SELECT s FROM SpeciesGuildDefinition s")
public class SpeciesGuildDefinition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SGD_ID")
	private long sgdId;

	@Column(name="SGD_DESCRIPTION")
	private String sgdDescription;

	@Column(name="SGD_EDITABLE")
	private Integer sgdEditable;

	@Column(name="SGD_MULTIPLE")
	private Integer sgdMultiple;

	@Column(name="SGD_NAME")
	private String sgdName;

	//bi-directional many-to-one association to SppGuildDefinitionGroup
	@OneToMany(mappedBy="speciesGuildDefinition")
	private List<SppGuildDefinitionGroup> sppGuildDefinitionGroups;

	//bi-directional many-to-one association to SppGuildDefinitionOption
	@OneToMany(mappedBy="speciesGuildDefinition")
	private List<SppGuildDefinitionOption> sppGuildDefinitionOptions;

	//bi-directional many-to-one association to SppGuildMap
	@OneToMany(mappedBy="speciesGuildDefinition")
	private List<SppGuildMap> sppGuildMaps;

	public SpeciesGuildDefinition() {
	}

	public long getSgdId() {
		return this.sgdId;
	}

	public void setSgdId(long sgdId) {
		this.sgdId = sgdId;
	}

	public String getSgdDescription() {
		return this.sgdDescription;
	}

	public void setSgdDescription(String sgdDescription) {
		this.sgdDescription = sgdDescription;
	}

	public Integer getSgdEditable() {
		return this.sgdEditable;
	}

	public void setSgdEditable(Integer sgdEditable) {
		this.sgdEditable = sgdEditable;
	}

	public Integer getSgdMultiple() {
		return this.sgdMultiple;
	}

	public void setSgdMultiple(Integer sgdMultiple) {
		this.sgdMultiple = sgdMultiple;
	}

	public String getSgdName() {
		return this.sgdName;
	}

	public void setSgdName(String sgdName) {
		this.sgdName = sgdName;
	}

	public List<SppGuildDefinitionGroup> getSppGuildDefinitionGroups() {
		return this.sppGuildDefinitionGroups;
	}

	public void setSppGuildDefinitionGroups(List<SppGuildDefinitionGroup> sppGuildDefinitionGroups) {
		this.sppGuildDefinitionGroups = sppGuildDefinitionGroups;
	}

	public SppGuildDefinitionGroup addSppGuildDefinitionGroup(SppGuildDefinitionGroup sppGuildDefinitionGroup) {
		getSppGuildDefinitionGroups().add(sppGuildDefinitionGroup);
		sppGuildDefinitionGroup.setSpeciesGuildDefinition(this);

		return sppGuildDefinitionGroup;
	}

	public SppGuildDefinitionGroup removeSppGuildDefinitionGroup(SppGuildDefinitionGroup sppGuildDefinitionGroup) {
		getSppGuildDefinitionGroups().remove(sppGuildDefinitionGroup);
		sppGuildDefinitionGroup.setSpeciesGuildDefinition(null);

		return sppGuildDefinitionGroup;
	}

	public List<SppGuildDefinitionOption> getSppGuildDefinitionOptions() {
		return this.sppGuildDefinitionOptions;
	}

	public void setSppGuildDefinitionOptions(List<SppGuildDefinitionOption> sppGuildDefinitionOptions) {
		this.sppGuildDefinitionOptions = sppGuildDefinitionOptions;
	}

	public SppGuildDefinitionOption addSppGuildDefinitionOption(SppGuildDefinitionOption sppGuildDefinitionOption) {
		getSppGuildDefinitionOptions().add(sppGuildDefinitionOption);
		sppGuildDefinitionOption.setSpeciesGuildDefinition(this);

		return sppGuildDefinitionOption;
	}

	public SppGuildDefinitionOption removeSppGuildDefinitionOption(SppGuildDefinitionOption sppGuildDefinitionOption) {
		getSppGuildDefinitionOptions().remove(sppGuildDefinitionOption);
		sppGuildDefinitionOption.setSpeciesGuildDefinition(null);

		return sppGuildDefinitionOption;
	}

	public List<SppGuildMap> getSppGuildMaps() {
		return this.sppGuildMaps;
	}

	public void setSppGuildMaps(List<SppGuildMap> sppGuildMaps) {
		this.sppGuildMaps = sppGuildMaps;
	}

	public SppGuildMap addSppGuildMap(SppGuildMap sppGuildMap) {
		getSppGuildMaps().add(sppGuildMap);
		sppGuildMap.setSpeciesGuildDefinition(this);

		return sppGuildMap;
	}

	public SppGuildMap removeSppGuildMap(SppGuildMap sppGuildMap) {
		getSppGuildMaps().remove(sppGuildMap);
		sppGuildMap.setSpeciesGuildDefinition(null);

		return sppGuildMap;
	}

}