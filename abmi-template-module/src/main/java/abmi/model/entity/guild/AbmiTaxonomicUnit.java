package abmi.model.entity.guild;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ABMI_TAXONOMIC_UNITS database table.
 * 
 */
@Entity
@Table(schema="WORKBENCH", name="ABMI_TAXONOMIC_UNITS")
@NamedQuery(name="AbmiTaxonomicUnit.findAll", query="SELECT a FROM AbmiTaxonomicUnit a")
public class AbmiTaxonomicUnit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ATU_GUID")
	private long atuGuid;

	@Column(name="ATU_ABMI_STATUS")
	private String atuAbmiStatus;

	@Column(name="ATU_AUTHOR_NAME")
	private String atuAuthorName;

	@Column(name="ATU_COMMON_NAME")
	private String atuCommonName;

	@Column(name="ATU_ITIS_STATUS")
	private String atuItisStatus;

	@Column(name="ATU_KINGDOM_ID")
	private Integer atuKingdomId;

	@Column(name="ATU_NS_CODE")
	private String atuNsCode;

	@Column(name="ATU_PARENT_TSNID")
	private Integer atuParentTsnid;

	@Column(name="ATU_RANK_ID")
	private Integer atuRankId;

	@Column(name="ATU_SCIENTIFIC_NAME")
	private String atuScientificName;

	@Column(name="ATU_SOURCE")
	private String atuSource;

	@Column(name="ATU_TSNID")
	private Integer atuTsnid;

	//bi-directional many-to-one association to SppGuildMap
	@OneToMany(mappedBy="abmiTaxonomicUnit")
	private List<SppGuildMap> sppGuildMaps;

	public AbmiTaxonomicUnit() {
	}

	public long getAtuGuid() {
		return this.atuGuid;
	}

	public void setAtuGuid(long atuGuid) {
		this.atuGuid = atuGuid;
	}

	public String getAtuAbmiStatus() {
		return this.atuAbmiStatus;
	}

	public void setAtuAbmiStatus(String atuAbmiStatus) {
		this.atuAbmiStatus = atuAbmiStatus;
	}

	public String getAtuAuthorName() {
		return this.atuAuthorName;
	}

	public void setAtuAuthorName(String atuAuthorName) {
		this.atuAuthorName = atuAuthorName;
	}

	public String getAtuCommonName() {
		return this.atuCommonName;
	}

	public void setAtuCommonName(String atuCommonName) {
		this.atuCommonName = atuCommonName;
	}

	public String getAtuItisStatus() {
		return this.atuItisStatus;
	}

	public void setAtuItisStatus(String atuItisStatus) {
		this.atuItisStatus = atuItisStatus;
	}

	public Integer getAtuKingdomId() {
		return this.atuKingdomId;
	}

	public void setAtuKingdomId(Integer atuKingdomId) {
		this.atuKingdomId = atuKingdomId;
	}

	public String getAtuNsCode() {
		return this.atuNsCode;
	}

	public void setAtuNsCode(String atuNsCode) {
		this.atuNsCode = atuNsCode;
	}

	public Integer getAtuParentTsnid() {
		return this.atuParentTsnid;
	}

	public void setAtuParentTsnid(Integer atuParentTsnid) {
		this.atuParentTsnid = atuParentTsnid;
	}

	public Integer getAtuRankId() {
		return this.atuRankId;
	}

	public void setAtuRankId(Integer atuRankId) {
		this.atuRankId = atuRankId;
	}

	public String getAtuScientificName() {
		return this.atuScientificName;
	}

	public void setAtuScientificName(String atuScientificName) {
		this.atuScientificName = atuScientificName;
	}

	public String getAtuSource() {
		return this.atuSource;
	}

	public void setAtuSource(String atuSource) {
		this.atuSource = atuSource;
	}

	public Integer getAtuTsnid() {
		return this.atuTsnid;
	}

	public void setAtuTsnid(Integer atuTsnid) {
		this.atuTsnid = atuTsnid;
	}

	public List<SppGuildMap> getSppGuildMaps() {
		return this.sppGuildMaps;
	}

	public void setSppGuildMaps(List<SppGuildMap> sppGuildMaps) {
		this.sppGuildMaps = sppGuildMaps;
	}

	public SppGuildMap addSppGuildMap(SppGuildMap sppGuildMap) {
		getSppGuildMaps().add(sppGuildMap);
		sppGuildMap.setAbmiTaxonomicUnit(this);

		return sppGuildMap;
	}

	public SppGuildMap removeSppGuildMap(SppGuildMap sppGuildMap) {
		getSppGuildMaps().remove(sppGuildMap);
		sppGuildMap.setAbmiTaxonomicUnit(null);

		return sppGuildMap;
	}

}