package abmi.model.entity.guild;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the SPP_GUILD_DEFINITION_GROUPS database table.
 * 
 */
@Entity
@Table(schema="WORKBENCH", name="SPP_GUILD_DEFINITION_GROUPS")
@NamedQuery(name="SppGuildDefinitionGroup.findAll", query="SELECT s FROM SppGuildDefinitionGroup s")
public class SppGuildDefinitionGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SGDG_ID")
	private long sgdgId;

	//bi-directional many-to-one association to SpeciesGroup
	@ManyToOne
	@JoinColumn(name="SGDG_SG_ID")
	private SpeciesGroup speciesGroup;

	//bi-directional many-to-one association to SpeciesGuildDefinition
	@ManyToOne
	@JoinColumn(name="SGDG_SGD_ID")
	private SpeciesGuildDefinition speciesGuildDefinition;

	public SppGuildDefinitionGroup() {
	}

	public long getSgdgId() {
		return this.sgdgId;
	}

	public void setSgdgId(long sgdgId) {
		this.sgdgId = sgdgId;
	}

	public SpeciesGroup getSpeciesGroup() {
		return this.speciesGroup;
	}

	public void setSpeciesGroup(SpeciesGroup speciesGroup) {
		this.speciesGroup = speciesGroup;
	}

	public SpeciesGuildDefinition getSpeciesGuildDefinition() {
		return this.speciesGuildDefinition;
	}

	public void setSpeciesGuildDefinition(SpeciesGuildDefinition speciesGuildDefinition) {
		this.speciesGuildDefinition = speciesGuildDefinition;
	}

}