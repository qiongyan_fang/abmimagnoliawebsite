package abmi.model.entity.guild;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the SPP_GUILD_DEFINITION_OPTIONS database table.
 * 
 */
@Entity
@Table(schema="WORKBENCH", name="SPP_GUILD_DEFINITION_OPTIONS")
@NamedQuery(name="SppGuildDefinitionOption.findAll", query="SELECT s FROM SppGuildDefinitionOption s")
public class SppGuildDefinitionOption implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SGDO_ID")
	private long sgdoId;

	@Column(name="SGDO_DESCRIPTION")
	private String sgdoDescription;

	@Column(name="SGDO_OPTIONS")
	private String sgdoOptions;

	//bi-directional many-to-one association to SpeciesGuildDefinition
	@ManyToOne
	@JoinColumn(name="SGDO_SGD_ID")
	private SpeciesGuildDefinition speciesGuildDefinition;

	//bi-directional many-to-one association to SppGuildValue
	@OneToMany(mappedBy="sppGuildDefinitionOption")
	private List<SppGuildValue> sppGuildValues;

	public SppGuildDefinitionOption() {
	}

	public long getSgdoId() {
		return this.sgdoId;
	}

	public void setSgdoId(long sgdoId) {
		this.sgdoId = sgdoId;
	}

	public String getSgdoDescription() {
		return this.sgdoDescription;
	}

	public void setSgdoDescription(String sgdoDescription) {
		this.sgdoDescription = sgdoDescription;
	}

	public String getSgdoOptions() {
		return this.sgdoOptions;
	}

	public void setSgdoOptions(String sgdoOptions) {
		this.sgdoOptions = sgdoOptions;
	}

	public SpeciesGuildDefinition getSpeciesGuildDefinition() {
		return this.speciesGuildDefinition;
	}

	public void setSpeciesGuildDefinition(SpeciesGuildDefinition speciesGuildDefinition) {
		this.speciesGuildDefinition = speciesGuildDefinition;
	}

	public List<SppGuildValue> getSppGuildValues() {
		return this.sppGuildValues;
	}

	public void setSppGuildValues(List<SppGuildValue> sppGuildValues) {
		this.sppGuildValues = sppGuildValues;
	}

	public SppGuildValue addSppGuildValue(SppGuildValue sppGuildValue) {
		getSppGuildValues().add(sppGuildValue);
		sppGuildValue.setSppGuildDefinitionOption(this);

		return sppGuildValue;
	}

	public SppGuildValue removeSppGuildValue(SppGuildValue sppGuildValue) {
		getSppGuildValues().remove(sppGuildValue);
		sppGuildValue.setSppGuildDefinitionOption(null);

		return sppGuildValue;
	}

}