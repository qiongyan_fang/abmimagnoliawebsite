package abmi.module.internal.template.component;

import javax.jcr.Node;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.objectfactory.Components;
import info.magnolia.templating.functions.TemplatingFunctions;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Internal Publication Administrator", id = "abmiModule:pages/internal_document_admin")
public class PublicationBackgroundComponent {

	@RequestMapping("/internal_document_admin")
	public String render() {
		if (Components.getComponent(TemplatingFunctions.class)
				.isAuthorInstance()) {
			return "internal_pages/internal_admin.jsp";
		}
		return null;

	}

	@Available
	public boolean isAvailable(Node websiteNode) {

		
		// Replace this with logic for your specific use case
		return TemplateUtil.checkAvailability(websiteNode, "/internal");

	}

}
