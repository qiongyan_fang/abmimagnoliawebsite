package abmi.module.template.dataanalytics.component.home;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.MainIconButtonComponent;


@Controller
@Template(id="abmiModule:components/da_home_topIntroduction", title="IntroductionText")
public class IntroductionButtonComponent {

	
	 @Area(value="greenButtonArea", maxComponents=3)
	 @Controller
	 @AvailableComponentClasses({MainIconButtonComponent.class})
	 
	 public static class GreenButtonArea {

	     @RequestMapping("/da_mainTemplate/greenButtonArea")
	     public String render() {
	         return "areas/generalarea.jsp";
	     }
	 }
	 
	  @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
		
	    	 tab.fields(
	    			
	    			    cfg.fields.text("header").label("Top Header"),
	    			    cfg.fields.text("description").label("Description").rows(5)
	    			  
	         );
	    }
	 
	  @RequestMapping("/da_home_topIntroduction")
	   public String render() {
	       return "data_analystics_portal/components/home_top_introduction.jsp";
	   }
	 
}
