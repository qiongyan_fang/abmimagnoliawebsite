package abmi.module.template.dataanalytics.page;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;

import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.dataanalytics.component.home.DATwoColumnComponent;
import abmi.module.template.dataanalytics.component.home.IntroductionButtonComponent;
import abmi.module.template.dataanalytics.component.home.LearnMoreRowComponent;
import abmi.module.template.dataanalytics.component.home.WhatsNewListComponent;
import abmi.module.template.util.TemplateUtil;

/**
 * Template with two columns, a main content area and a right side column.
 */
@Controller
@Template(title = "DA Home Page", id = "abmiModule:pages/da_home")
public class DataAnalysticsHomePage {
	 @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	
		 
	    	 tab.fields(
	    			  cfg.fields.text("keyword").label("keywords").description("The keywords about this page")
	               
	                
	         );
	    }

 @RequestMapping("/da_mainTemplate")
 public String render(Node page, ModelMap model) throws RepositoryException {


 	

     return "data_analystics_portal/pages/da_main_page.jsp";
 }



 @Available
	public boolean isAvailable(Node websiteNode) {

		
		return TemplateUtil.checkAvailability(websiteNode);

	}
 
 @Controller
 @Area(value = "megaMenuArea", title = "Top Navigation Bar")   
 @AvailableComponentClasses({MegaMenuAreaComponent.class})
 @Inherits(components = ComponentInheritanceMode.ALL)
 public static class topNavigationBarArea {

     @RequestMapping("/da_mainTemplate/topBar")
     public String render() {
         return "areas/generalarea.jsp";
     }
 }
 
 
 
 /**
  * 3 green button area area.
  */
	@Area(value = "introductionTopArea", maxComponents=1)
 @Controller
 @AvailableComponentClasses({IntroductionButtonComponent.class})
 
 public static class IntroductionTopArea {

     @RequestMapping("/da_mainTemplate/introductionTopArea")
     public String render() {
         return "areas/generalarea.jsp";
     }
 }
 

 /**
  * the learn more button under project sponsors
  */
 @Area(value="bottomLearnMoreRowArea", maxComponents=1)
 @Controller
 @AvailableComponentClasses({LearnMoreRowComponent.class})
 
 public static class BottomLearnMoreButtonArea {

     @RequestMapping("/da_mainTemplate/bottomLearnMoreRowArea")
     public String render() {
         return "areas/generalarea.jsp";
     }
 }
 
 /* rows with two columns layout, users to add 4 boxes*/
 @Controller
 @Area(value = "rowArea", title = "Row Area")
 @Inherits
 @AvailableComponentClasses({DATwoColumnComponent.class})

 public static class RowArea {

     @RequestMapping("/da_mainTemplate/rowarea")
     public String render() {
         return "areas/main_rowarea.jsp";
     }
 }
 
 
 /* rows with two columns layout, users to add news hightlight etc.*/
 @Controller
 @Area(value = "whatnewlistArea", title = "What's New Area")
 @Inherits
 @AvailableComponentClasses({WhatsNewListComponent.class})

 public static class WhatsNewArea {

     @RequestMapping("/da_mainTemplate/whatnewlistArea")
     public String render() {
         return "areas/generalarea.jsp";
     }
 }
 
 
 @Controller
 @Area(value = "footer", title = "Footer Menu", maxComponents=2)   
 @AvailableComponentClasses({FooterAreaComponent.class})
 @Inherits(components = ComponentInheritanceMode.ALL)
 public static class FooterMenuArea {

     @RequestMapping("/da_mainTemplate/footermenu")
     public String render() {
         return "areas/generalarea.jsp";
     }
 }
 
 @Controller
 @Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents=1)   
 @AvailableComponentClasses({FinePrintFooterComponent.class})
 @Inherits(components = ComponentInheritanceMode.ALL)
 public static class FineFooterMenuArea {

     @RequestMapping("/da_mainTemplate/finefootermenu")
     public String render() {
         return "areas/generalarea.jsp";
     }
 }
}
