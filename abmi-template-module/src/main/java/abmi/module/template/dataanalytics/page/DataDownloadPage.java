package abmi.module.template.dataanalytics.page;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.*;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioSpeciesBannerComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioSpeciesBannerMultipleComponent;
import abmi.module.template.dataanalytics.component.biobrowser.ImageTextBannerComponent;
import abmi.module.template.dataanalytics.component.download.DARawDataDownloadComponent;
import abmi.module.template.dataanalytics.component.download.DownloadPageCommonComponent;
import abmi.module.template.dataanalytics.component.download.ProductRightMenuComponent;
import abmi.module.template.dataanalytics.component.home.BannerComponent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Data Analytics Download Page", id = "abmiModule:pages/da_datadownload")
public class DataDownloadPage {

	@RequestMapping("/da_datadownload")
	public String render(Node page, ModelMap model) throws RepositoryException {

		// model.put("currentLink", MgnlContext.getAggregationState()
		// .getCurrentContentNode().getPath());
		return "data_analystics_portal/pages/da_download_page.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(cfg.fields.text("title").label("Title"),
				cfg.fields.checkbox("hideRightSideMenu").buttonLabel("").label("wide no side"),
				cfg.fields.checkbox("hideSearch").buttonLabel("").label("hide Search box"),
				cfg.fields.text("keyword").label("keywords").description("The keywords about this page"),
				cfg.fields.checkbox("hideInNavigation").buttonLabel("").label("Hide in navigation")
						.description("Check this box to hide this page in navigation"));
	}

	@Available
	public boolean isAvailable(Node websiteNode) {

		return TemplateUtil.checkAvailability(websiteNode);

	}

	@Controller
	@Area(value = "bannerArea", title = "Banner Area")
	@Inherits
	@AvailableComponentClasses({ BannerComponent.class, ImageTextBannerComponent.class })
	public static class BannerArea {

		@RequestMapping("/da_datadownload/bannerArea")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses({ TextContent.class, DownloadPageCommonComponent.class, MainContent.class,
			DARawDataDownloadComponent.class })
	public static class LeftColumnArea {

		@RequestMapping("/da_datadownload/leftColumn")
		public String render(Node content) {

			return "areas/pagetwocolumn/leftcolumn_area.jsp";

		}

	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ RichTextComponent.class, ProductRightMenuComponent.class })
	public static class ReftColumnArea {

		@RequestMapping("/da_datadownload/rightColumn")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class RightWidgetColumnArea {

		@RequestMapping("/da_datadownload/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		// @TabFactory("Content")
		// public void contentTab(UiConfig cfg, TabBuilder tab) {
		// Map<String, String> widgetCategories = new LinkedHashMap<String,
		// String>();
		// widgetCategories.put("Download List", "list");
		// widgetCategories.put("Connect Icons", "connect");
		//
		// tab.fields(cfg.fields.text("header").label("Title"),
		// cfg.fields.select("classname").label("Widget
		// Type").options(widgetCategories.keySet()));
		//
		// }
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/da_datadownload/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/da_datadownload/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
}
