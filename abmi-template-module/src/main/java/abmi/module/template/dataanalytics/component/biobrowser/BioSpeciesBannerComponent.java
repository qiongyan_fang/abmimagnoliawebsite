package abmi.module.template.dataanalytics.component.biobrowser;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.dataanalytics.component.home.BannerComponent;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

@Controller
@Template(title = "DA Biobrowser Species Banner", id = "abmiModule:components/da_biobrowsersppbanner")
@BannerComponent
public class BioSpeciesBannerComponent {
	@RequestMapping("/da_biobrowsersppbanner")
	public String render() {

		return "data_analystics_portal/components/biobrowser_species_banner.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

		cfg.fields.link("backgroundImageUrl").label("Background Image").appName("assets")
				.targetWorkspace("dam"),

		cfg.fields.text("header").label("Top Header"),
				cfg.fields.text("header2").label("Description"), cfg.fields
						.text("description").label("Description")

		);
	}
}
