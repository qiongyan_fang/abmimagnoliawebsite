package abmi.module.template.dataanalytics.component.download;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "DA List of Single products", id = "abmiModule:components/da_listofproducts")
@TemplateDescription("DA List of Single products")
@DownloadPageCommonComponent
public class L2ProductOverviewBlockComponent {

	@RequestMapping("/da/productoviewpagelistbox")
	public String render(ModelMap model, Node content)
			throws RepositoryException {

		String path = PropertyUtil.getString(content, "rootpath", null);
		if (path != null) {
			

			ArrayList<Map<String, Object>> subPageList = new ArrayList<Map<String, Object>>();

			for (Node node : NodeUtil.getNodes(
					content.getSession().getNode(path), NodeTypes.Page.NAME)) {
				if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {

					Map<String, Object> rowMap = new HashMap<String, Object>();
					rowMap.put("path", node.getPath());

					rowMap.put("title",
							PropertyUtil.getString(node, "title", ""));
					rowMap.put("details",
							this.getDownloadComponent(node.getPath()));

					subPageList.add(rowMap);

				}
			}

			model.put("products", subPageList);
		}

		return "data_analystics_portal/components/product_single_overview.jsp";
	}

	static long getCountDownloadComponent(String rootPath) {

		try {
			Session session = MgnlContext.getJCRSession("website");
			String countQueryString = "select * from [mgnl:component] as t where ISDESCENDANTNODE(["
					+ rootPath
					+ "]) and [mgnl:template] = 'abmiModule:components/da_singleproductversion'";

			QueryManager manager = session.getWorkspace().getQueryManager();

			String queryString = countQueryString;

			javax.jcr.query.Query query = manager.createQuery(queryString,
					javax.jcr.query.Query.JCR_SQL2);
			QueryResult result = query.execute();

			NodeIterator resultsIter = result.getNodes();

			return (resultsIter == null ? 0 : resultsIter.getSize());
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	Map<String, Object> getDownloadComponent(String rootPath) {

		try {
			Session session = MgnlContext.getJCRSession("website");
			String countQueryString = "select * from [mgnl:component] as t where ISDESCENDANTNODE(["
					+ rootPath
					+ "]) and [mgnl:template] = 'abmiModule:components/da_singleproduct'";

			QueryManager manager = session.getWorkspace().getQueryManager();

			String queryString = countQueryString;

			javax.jcr.query.Query query = manager.createQuery(queryString,
					javax.jcr.query.Query.JCR_SQL2);
			QueryResult result = query.execute();

			NodeIterator resultsIter = result.getNodes();
			Map<String, Object> resultMap = new HashMap<String, Object>();
			if (resultsIter != null && resultsIter.hasNext()) { // only get
																// first one

				Node node = (Node) resultsIter.next();
				resultMap.put("customTitle",
						PropertyUtil.getString(node, "customHeader", ""));
				// resultMap.put("productCount",
				// SingleProductOverviewComponent.getCountDownloadComponent(node.getParent().getPath()));
				resultMap.put("boxDescription",
						PropertyUtil.getString(node, "boxDescription", ""));
				resultMap.put("boxImage",
						PropertyUtil.getString(node, "boxImage", ""));
			}

			return resultMap;
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				cfg.fields.text("header").label("Header (if diff from title)"),
				cfg.fields.richText("description").label("Description"),

				cfg.fields.staticField("1").label("")
						.value("-----------Overview Box Info--------------")
						.readOnly(),
				cfg.fields.text("boxDescription").label("Box Description").rows(5),
				cfg.fields.link("boxImage").label("Box Image")
						.appName("assets").targetWorkspace("dam"),
				cfg.fields.staticField("2").label("")
						.value("-----------List of products--------------")
						.readOnly(),
				cfg.fields.text("rootpath").label("Root Path"),
				cfg.fields.link("placeholderimg").label("Default Image")
						.appName("assets").targetWorkspace("dam"),
				cfg.fields
						.text("descriptioncount")
						.label("Description characters")
						.defaultValue("300")
						.description(
								"how many characters can a description have?")

		);
	}

}
