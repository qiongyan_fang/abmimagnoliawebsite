package abmi.module.template.dataanalytics.component.home;

import java.util.LinkedHashMap;
import java.util.Map;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;



@Controller
@Template(title = "DA Home Quick Link Box", id = "abmiModule:components/da_homequicklink")
@TemplateDescription("DA Home Quick Link Box")
public class QuickLinkBoxComponent {
	  @RequestMapping("/da_homequicklink")
	    public String render() {
	    
	        return "data_analystics_portal/components/home_quicklink.jsp";
	    }
	  
	
	  @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
			Map<String, String> positionCategories = new LinkedHashMap<String, String>();
			positionCategories.put("left", "left");
			positionCategories.put("right", "right");
			positionCategories.put("background", "background");
			
	    	 tab.fields(
	    			 cfg.fields.select("backgroundColor").label("Background Color").options(TemplateUtil.getBaseColor()),
	    			    cfg.fields.link("iconUrl").label("Icon Image").appName("assets").targetWorkspace("dam"),
	    			
	    			    cfg.fields.text("header").label("Top Header"),
	    			    cfg.fields.text("description").label("Description").rows(5),
	    			    cfg.fields.text("embed").label("Embed Code").description("embed code to show a submit box").rows(5),
	    			    cfg.fields.link("buttonUrl").label("Button Url").appName("pages").targetWorkspace("website").description("select a page, or enter external page link starts with http"),
	    			    cfg.fields.text("buttonText").label("Button Text").defaultValue("Learn More"),
	    			    cfg.fields.link("imageUrl").label(" Image").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.select("imagePosition").label("Image Position").options(positionCategories.keySet())
	    			  
	         );
	    }
}
