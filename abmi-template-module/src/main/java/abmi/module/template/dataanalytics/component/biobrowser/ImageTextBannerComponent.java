package abmi.module.template.dataanalytics.component.biobrowser;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.dataanalytics.component.home.BannerComponent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "DA Image Text Banner", id = "abmiModule:component/da_imagetextbanner")
@TemplateDescription("DA Image Text Banner")
@BannerComponent
public class ImageTextBannerComponent {

	@RequestMapping("/da_imagetextbanner")
	public String render() {

		return "data_analystics_portal/components/product_image_text_banner.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),

				cfg.fields.link("backgroundImageUrl").label("spp icon").appName("assets").targetWorkspace("dam")
						.description("select icons optional"),

				cfg.fields.select("backgroundColor").options(TemplateUtil.getDABackgroundColors())
						.label("Select background Color"),

				cfg.fields.text("header").label("Top Header"), cfg.fields.text("header2").label("Header 2"),
				cfg.fields.text("description").label("Description").rows(5),

				cfg.fields.text("reportPath").label("Report Path").description(
						"path to pdf reports in web server, same location as other species reports. e.g /report/Birds/Botaurus_lentiginosus.pdf")

		);
	}
}
