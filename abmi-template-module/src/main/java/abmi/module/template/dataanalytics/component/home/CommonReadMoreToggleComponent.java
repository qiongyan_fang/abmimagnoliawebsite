package abmi.module.template.dataanalytics.component.home;

import javax.jcr.Node;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.FAQAreaComponent;
import abmi.module.template.component.MainContent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextContent;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

@Controller
@Template(title = "DA Common Toggle Read More Box", id = "abmiModule:components/da_commonreadmore")
@MainContent
@TextContent
@TemplateDescription("Toggle Read More")
public class CommonReadMoreToggleComponent {
	  @RequestMapping("/da_commonreadmore")
	    public String render() {
	    
	        return "data_analystics_portal/components/home_readmore_toggle.jsp";
	    }
	  
	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

			
				cfg.fields.text("headerText").label("Header(black)")
						.description("Header text (left side)"),
				cfg.fields.richText("description").label("Description")
			
		);
	}
	
	@Area("collapsible_area") // Habitat & Human Footprint Associations
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, FAQAreaComponent.class })
	public static class basicIntroductionArea {

		@RequestMapping("/da_commonreadmore/collapsible_area")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}
}
