package abmi.module.template.dataanalytics.component.download;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "DA Single Download Information", id = "abmiModule:components/da_ftpdownloadlink")
@TemplateDescription("DA Single Download Information")
public class L5ProductDownloadDetailComponent {
	@RequestMapping("/da_ftpdownloadlink")
	public String render() {

		return "data_analystics_portal/components/product_ftpdownloadlink.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				cfg.fields.text("productname").label("Name Version"),
//				cfg.fields.text("fileextension").label("File Extension"),
				cfg.fields.text("size").label("File Size")
						.description("file size in kb,Mb, Gb, Tb"),
				cfg.fields.link("url").label("Link Url").appName("pages")
						.targetWorkspace("website"),
				cfg.fields.select("fileicon").label("File Icon")
						.options(TemplateUtil.getDownloadFileType()).description("no link will be display if ftp is chosen")

		);
	}
}
