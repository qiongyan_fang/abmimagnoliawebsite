package abmi.module.template.dataanalytics.component.biobrowser;

import javax.jcr.Node;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.CompositeFieldBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueSubChildrenNodePropertiesTransformer;
import info.magnolia.ui.framework.config.UiConfig;

// this banner allow mulitple inputs: bird, plant etc.
@Controller
@Template(title = "DA Biobrowser Mulitple Species Banner", id = "abmiModule:components/da_biobrowsersppbannermulitple")
public class BioSpeciesBannerMultipleComponent {
	@RequestMapping("/da_biobrowsersppbannermulitple")
	public String render(Node content, ModelMap model) {
		
		model.put("banner", TemplateUtil.getMultiField(content, "banner"));
        
		return "data_analystics_portal/components/biobrowser_species_banner_multiple.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		 tab.fields(
		cfg.fields
				.multi("banner")
				.label("Banner Detail")
				.field(new CompositeFieldBuilder("headertextcompo")
						.fields(cfg.fields.select("speciesGroup")
								.label("Species Group")
								.options(TemplateUtil.getSpeciesGroup()))
						.fields(cfg.fields.text("header").label("Header")
								.rows(1))
						.fields(cfg.fields.text("header2").label("Header 2").description("Species detection message"))
						.fields(cfg.fields.checkbox("includeCounter").label("include Counter").buttonLabel("use Counter in header2"))
						.fields(cfg.fields.richText("description").label(
								"Description"))
						.fields(cfg.fields.link("imageurl").label("Image")
								.appName("assets").targetWorkspace("dam")))
				.transformerClass(
						MultiValueSubChildrenNodePropertiesTransformer.class)
						);

	}
}
