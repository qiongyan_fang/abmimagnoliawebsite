package abmi.module.template.dataanalytics.component.biobrowser;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.TextContent;
import abmi.module.template.dataanalytics.component.home.CommonReadMoreToggleComponent;

@Controller
@Template(title = "DA Species Section", id = "abmiModule:components/da_species_Section")
@TemplateDescription("DA Species Section")

// detailed species information: graphs, text, maps,etc. used in species profile
// pages.
public class BioSpeciesHorizontalSectionComponent {

	@RequestMapping("/da_species_Section")
	public String render() {

		return "data_analystics_portal/components/species_section.jsp";
	}

	@Area("textArea") // Habitat & Human Footprint Associations
	@Controller
	@AvailableComponentClasses({TextContent.class})
	public static class textArea {

		@RequestMapping("/da_species_section/textArea")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}
	
	
	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				cfg.fields.text("header").label("Top Header")
				);
	}
	
}
