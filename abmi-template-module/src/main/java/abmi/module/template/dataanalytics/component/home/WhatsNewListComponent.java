package abmi.module.template.dataanalytics.component.home;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "DA What's New List", id = "abmiModule:components/da_whatsnewlist")
@TemplateDescription("DA What's New List")
public class WhatsNewListComponent {
	@RequestMapping("/da/da_whatsnewlist")
	public String render(ModelMap model, Node content)
			throws RepositoryException {

		String path = content.getProperty("listpath").getString();
		int descriptionCount = 300;
		try {
			descriptionCount = Integer.parseInt(PropertyUtil.getString(content,
					"descriptioncount", "300"));
		} catch (Exception e) {
		}

		int maxItemCount = 4;
		try {
			maxItemCount = Integer.parseInt(PropertyUtil.getString(content,
					"count", "4"));
		} catch (Exception e) {
		}

		model.put("newlist",
				this.getNewList(path, maxItemCount, descriptionCount));
		return "data_analystics_portal/components/home_whatsnew.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				cfg.fields.text("header").label("Top Header"),

				cfg.fields.text("buttonText").label("Button Text")
						.defaultValue("Learn More"),
				cfg.fields.link("listpath").label("List Path")
						.description("What's New Page path").appName("pages")
						.targetWorkspace("website"),
				cfg.fields
						.text("count")
						.label("No. of News")
						.description(
								"the number of 'what's new' items to be shown"),

				cfg.fields
						.text("descriptioncount")
						.label("Description characters")
						.defaultValue("300")
						.description(
								"how many characters can a description have?")

		);
	}

	/**
	 * 
	 * @param path
	 *            the what's new page link
	 * @param maxCount
	 *            how many items to display
	 * @param descriptionCount
	 *            how many characters can each description text has
	 * @return
	 */
	List<Map<String, String>> getNewList(String path, long maxCount,
			int descriptionCount) {
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		try {
			Session session = MgnlContext.getJCRSession("website");
			String countQueryString = "select * from [mgnl:component] as t where ISDESCENDANTNODE(["
					+ path
					+ "]) and [mgnl:template] = 'abmiModule:components/da_whatsnewitem'";

			QueryManager manager = session.getWorkspace().getQueryManager();

			String queryString = countQueryString;

			// System.out.println(queryString);
			javax.jcr.query.Query query = manager.createQuery(queryString,
					javax.jcr.query.Query.JCR_SQL2);
			query.setLimit(maxCount);
			QueryResult result = query.execute();

			NodeIterator resultsIter = result.getNodes();

			while (resultsIter != null && resultsIter.hasNext()) {
				Node node = (Node) resultsIter.next();
				Map<String, String> singleMap = new HashMap<String, String>();
				singleMap.put("title",
						PropertyUtil.getString(node, "header", ""));
				singleMap.put("date", PropertyUtil.getString(node, "date", ""));
				singleMap.put("description", TemplateUtil.getCharByNumber(
						PropertyUtil.getString(node, "description", ""),
						descriptionCount));

				resultList.add(singleMap);
			}

			return resultList;
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resultList;
	}
}
