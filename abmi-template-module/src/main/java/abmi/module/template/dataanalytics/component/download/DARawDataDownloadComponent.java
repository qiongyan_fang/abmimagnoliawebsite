package abmi.module.template.dataanalytics.component.download;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.TextContent;

@Controller
@Template(title = "Species Habitat Download Component", id = "abmiModule:component/da_rawdownload")
@TemplateDescription("DA Species Habitat Download")
@DownloadPageCommonComponent
public class DARawDataDownloadComponent {

	@RequestMapping("/darawdatadownloadcomponent")
	public String render(Node page, ModelMap model) throws RepositoryException {

		model.put("currentLink", MgnlContext.getAggregationState().getHandle()
				);
		return "data_analystics_portal/components/species_habitat_download.jsp";
	}

	@Area("dadownloadheader")
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	public static class HeaderArea {

		@RequestMapping("/darawdatadownloadcomponent/header")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("dadownloaddescription")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class })
	public static class descriptionArea {

		@RequestMapping("/darawdatadownloadcomponent/description")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("dadownload_table_content")
	@Controller
	@AvailableComponentClasses({TextContent.class })
	public static class tableDescriptionArea {

		@RequestMapping("/darawdatadownloadcomponent/dadownload_table_content")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
	
	@Area("dadownload_table_title")
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	public static class tableTitleArea {

		@RequestMapping("/darawdatadownloadcomponent/dadownload_table_title")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
}