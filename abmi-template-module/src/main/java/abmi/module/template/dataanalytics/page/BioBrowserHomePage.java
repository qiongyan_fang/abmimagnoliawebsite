package abmi.module.template.dataanalytics.page;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;

import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.PlainTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.TextContent;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioHomeBannerComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioHomeBottomHighlighComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioHomeGuildAreaComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioHomeLearnMoreComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioHomeSpeciesBlockComponent;
import abmi.module.template.util.TemplateUtil;

/**
 * Template with two columns, a main content area and a right side column.
 */
@Controller
@Template(title = "DA Biobrowser Page", id = "abmiModule:pages/da_biobrowser_home")
public class BioBrowserHomePage {
	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				cfg.fields.link("listUrl").label("Species List Url")
						.appName("pages").targetWorkspace("website")
						.description("path to the species list page"),
				cfg.fields.link("detailUrl").label("Species Detail Url")
						.appName("pages").targetWorkspace("website")
						.description("path to the species list page"),
				cfg.fields.text("keyword").label("keywords")
						.description("The keywords about this page")

		);
	}

	@RequestMapping("/da_biobrowser_home")
	public String render(Node page, ModelMap model) throws RepositoryException {

		return "data_analystics_portal/pages/da_biobrowser_home_page.jsp";
	}

	@Available
	public boolean isAvailable(Node websiteNode) {

		return TemplateUtil.checkAvailability(websiteNode);

	}

	/* rows with two columns layout, users to add news hightlight etc. */
	@Controller
	@Area(value = "bannerArea", title = "Banner Area")
	@Inherits
	@AvailableComponentClasses({ BioHomeBannerComponent.class })
	public static class BannerArea {

		@RequestMapping("/da_mainBiobrowserTemplate/bannerArea")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "speciesTextArea", title = "Species Count Text Area", maxComponents = 1)
	@Inherits
	@AvailableComponentClasses({ TextContent.class })
	public static class SpeciesTextArea {

		@RequestMapping("/da_mainBiobrowserTemplate/speciesTextArea")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	/* each block has to be put here one by one because of the random layout */
	@Controller
	@Area(value = "speciesBlockArea1", title = "Species Block", maxComponents = 1)
	@Inherits
	@AvailableComponentClasses({ BioHomeSpeciesBlockComponent.class })
	public static class SpeciesBlockArea1 {

		@RequestMapping("/da_mainBiobrowserTemplate/speciesTextArea1")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "speciesBlockArea2", title = "Species Block", maxComponents = 1)
	@Inherits
	@AvailableComponentClasses({ BioHomeSpeciesBlockComponent.class })
	public static class SpeciesBlockArea2 {

		@RequestMapping("/da_mainBiobrowserTemplate/speciesTextArea2")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "speciesBlockArea3", title = "Species Block", maxComponents = 1)
	@Inherits
	@AvailableComponentClasses({ BioHomeSpeciesBlockComponent.class })
	public static class SpeciesBlockArea3 {

		@RequestMapping("/da_mainBiobrowserTemplate/speciesTextArea3")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "speciesBlockArea4", title = "Species Block", maxComponents = 1)
	@Inherits
	@AvailableComponentClasses({ BioHomeSpeciesBlockComponent.class })
	public static class SpeciesBlockArea4 {

		@RequestMapping("/da_mainBiobrowserTemplate/speciesTextArea4")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "speciesBlockArea5", title = "Species Block", maxComponents = 1)
	@Inherits
	@AvailableComponentClasses({ BioHomeSpeciesBlockComponent.class })
	public static class SpeciesBlockArea5 {

		@RequestMapping("/da_mainBiobrowserTemplate/speciesTextArea5")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "speciesBlockArea6", title = "Species Block", maxComponents = 1)
	@Inherits
	@AvailableComponentClasses({ BioHomeSpeciesBlockComponent.class })
	public static class SpeciesBlockArea6 {

		@RequestMapping("/da_mainBiobrowserTemplate/speciesTextArea6")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "speciesBlockArea7", title = "Species Block", maxComponents = 1)
	@Inherits
	@AvailableComponentClasses({ BioHomeSpeciesBlockComponent.class })
	public static class SpeciesBlockArea7 {

		@RequestMapping("/da_mainBiobrowserTemplate/speciesTextArea7")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "guildArea", title = "Choose a Guild", maxComponents = 1)
	@Inherits
	@AvailableComponentClasses({ PlainTextComponent.class,
			BioHomeGuildAreaComponent.class })
	public static class GuildArea {

		@RequestMapping("/da_mainBiobrowserTemplate/guildTextArea")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "learnMoreArea", title = "Learn More Area", maxComponents = 2)
	@Inherits
	@AvailableComponentClasses({ BioHomeLearnMoreComponent.class })
	public static class LearnMoreArea {

		@RequestMapping("/da_mainBiobrowserTemplate/learnMoreArea")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "highlightArea", title = "High Light Area", maxComponents = 4)
	@Inherits
	@AvailableComponentClasses({ BioHomeBottomHighlighComponent.class })
	public static class HighlightArea {

		@RequestMapping("/da_mainBiobrowserTemplate/highlightArea")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/da_mainBiobrowserTemplate/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/da_mainBiobrowserTemplate/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
}
