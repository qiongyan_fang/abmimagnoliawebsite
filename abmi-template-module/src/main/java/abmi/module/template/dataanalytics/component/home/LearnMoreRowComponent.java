package abmi.module.template.dataanalytics.component.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

@Controller
@Template(title = "DA Home Learn More", id = "abmiModule:components/da_homelearnmore")
@TemplateDescription("DA List of products")
public class LearnMoreRowComponent {
	  @RequestMapping("/da_homelearnmore")
	    public String render() {
	    
	        return "data_analystics_portal/components/home_learnmore.jsp";
	    }
	  
	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				cfg.fields.link("imageUrl").label("Image")
						.description("Image path").appName("assets")
						.targetWorkspace("dms"),
				cfg.fields.text("headerText").label("Header(black)")
						.description("Header text (left side)"),
				cfg.fields.text("headerTextOrange").label("Header(orange)")
						.description("Header text (right side)"),
				cfg.fields.richText("description").label("Description"),
				cfg.fields
						.link("buttonUrl")
						.label("Button Url")
						.appName("pages")
						.targetWorkspace("website")
						.description(
								"path to the page, use http:// for external links"),
				cfg.fields.text("buttonText").label("Button Text")

		);
	}
}
