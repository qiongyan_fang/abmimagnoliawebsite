package abmi.module.template.dataanalytics.component.biobrowser;

import javax.jcr.Node;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.OptionBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

// a set of of image images at biobrowser species home page
@Controller
@Template(title = "DA Home Biobrowser species", id = "abmiModule:components/da_biobrowserhomespecies")
@TemplateDescription("DA Home Biobrowser species")
public class BioHomeSpeciesBlockComponent {
	@RequestMapping("/da_biobrowserhomespecies")
	public String render(Node content,ModelMap model) {

		
		model.put("listUrl", PropertyUtil.getString(MgnlContext.getAggregationState().getMainContentNode(), "listUrl", ""));
		model.put("detailUrl", PropertyUtil.getString(MgnlContext.getAggregationState().getMainContentNode(), "detailUrl", ""));
		
		return "data_analystics_portal/components/biobrowser_home_speciesblock.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				cfg.fields.text("headerText").label("Header")
						.description("Header text (left side)"),
				cfg.fields.text("headerText2").label("Header second"),
				cfg.fields.link("backgroundImageUrl").label(" Image")
						.appName("assets").targetWorkspace("dam"),
				// cfg.fields
				// .link("buttonUrl")
				// .label("Button Url")
				// .appName("pages")
				// .targetWorkspace("website")
				// .description(
				// "path to the page, use http:// for external links"),

						
				    	
				cfg.fields.select("speciesGroup").options(
					TemplateUtil.getSpeciesGroup()).label("Species Group"),
				cfg.fields.text("buttonText").label("Button Text")
						.defaultValue("Learn More")

		);
	}
}
