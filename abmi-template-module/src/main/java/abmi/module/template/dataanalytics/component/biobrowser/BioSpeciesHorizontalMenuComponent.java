package abmi.module.template.dataanalytics.component.biobrowser;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.dataanalytics.component.home.CommonReadMoreToggleComponent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "DA Species Menus", id = "abmiModule:components/da_species_menu")
@TemplateDescription("DA Species Menus")
// detailed species information: graphs, text, maps,etc. used in species profile
// pages.
public class BioSpeciesHorizontalMenuComponent {

	@RequestMapping("/da_species_menu")
	public String render() {

		return "data_analystics_portal/components/species_multi_menu.jsp";
	}

	@Area("menuArea")

	@Controller
	@AvailableComponentClasses({ BioSpeciesHorizontalSectionComponent.class })
	public static class introductionArea {

		@RequestMapping("/da_species_menu/singleMenu")
		public String render(Node content) {

			return "data_analystics_portal/areas/species_multi_menu_area.jsp";
		}
		
		
		@TabFactory("Content")
		public void contentTab(UiConfig cfg, TabBuilder tab) {

			tab.fields(

					cfg.fields.text("speciesText").label("Image Text")
							.description("text for image"),

					cfg.fields.link("imageUrl").label(" Image").appName("assets")
							.targetWorkspace("dam"));

		}

	}

	
}
