package abmi.module.template.dataanalytics.component.biobrowser;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

@Controller
@Template(title = "DA Guild Item", id = "abmiModule:components/da_biobrowserhomeguilditem")
public class BioHomeGuildItemComponent {
	@RequestMapping("/da_biobrowserhomeguilditem")
	public String render() {

		return "data_analystics_portal/components/biobrowser_home_guild_item.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		/*
		 * <a href="#" class="species-item"> <div class="speciesImg-block"> <img
		 * src="assets/Bryophytes-sm.jpg" alt="Bryophytes"/> </div> <span>Guild
		 * Title #9</span> </a>
		 */
		tab.fields(cfg.fields.link("imageUrl").label("Image").appName("assets")
				.targetWorkspace("dam"),

		cfg.fields.text("guildTitle").label("Guild Caption/Title"),
		cfg.fields.link("url").label("Page Path").appName("pages")
				.targetWorkspace("website")
//		cfg.fields.text("url").label("url").description("where the guild clicking leads to")
		);

	}
}
