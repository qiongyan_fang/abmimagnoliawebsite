package abmi.module.template.dataanalytics.component.download;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.objectfactory.Components;
import info.magnolia.templating.functions.TemplatingFunctions;
import info.magnolia.ui.form.config.OptionBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.ClearStyleComponent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.TextContent;
import abmi.module.template.dataanalytics.component.home.CommonReadMoreToggleComponent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "DA Single Product Download", id = "abmiModule:components/da_singleproduct")
@TemplateDescription("DA Single Product Download")
@DownloadPageCommonComponent
public class L3ProductDescriptionComponent {

	@RequestMapping("/da/da_singleproduct")
	public String render(ModelMap model, Node content)
			throws RepositoryException {

		// get page's page
		String currentPath = MgnlContext.getAggregationState().getHandle();

		if (Components.getComponent(TemplatingFunctions.class)
				.isAuthorInstance()) {

			long count = L2ProductOverviewBlockComponent
					.getCountDownloadComponent(currentPath);
			PropertyUtil.setProperty(content, "productCount", count);
		}

//		model.put("currentPath", currentPath);?
		return "data_analystics_portal/components/product_download_header.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		

	 
		tab.fields(
				cfg.fields.text("customHeader").label(
						"Header(default is title)"),

				cfg.fields.link("image").label("Default Image")
						.appName("assets").targetWorkspace("dam"),
				cfg.fields.text("description").label("Description").rows(5),
			    cfg.fields.text("embed").label("Embedded html").rows(5),
				cfg.fields.staticField("0")
				.value("-----------Optional download instruction --------------").label("").readOnly(),
				cfg.fields.checkbox("bHideDownload").buttonLabel("").label("check to hide instructions"),
				cfg.fields.richText("downloadDescription").label(
						"Download Instruction").required(false),
				cfg.fields.select("backgroundColor").options(TemplateUtil.getDABackgroundColors()
								)
								.label("Select background Color"),
				cfg.fields.select("fileicon").label("File Icon")
						.options(TemplateUtil.getDownloadFileType()),

				cfg.fields.staticField("1")
						.value("-----------Overview Box Info used in parent pages--------------").label("")
						.readOnly(),
				cfg.fields.text("boxDescription").label("Box Description").rows(5),
				cfg.fields.link("boxImage").label("Box Image")
						.appName("assets").targetWorkspace("dam")

		);
	}

	@Area("Header")
	@Controller
	@AvailableComponentClasses({ ClearStyleComponent.class })
	public static class H4TitleArea {

		@RequestMapping("/da_singleproduct/h4title")
		public String render(Node content) {

			return "areas/pagetwocolumn/leftcolumn_area.jsp";

		}

	}

	@Area("Description")
	@Controller
	@AvailableComponentClasses({ TextContent.class , ProductMapGalleryViewerComponent.class})
	public static class RichTextArea {

		@RequestMapping("/da_singleproduct/richtext")
		public String render(Node content) {

			return "areas/pagetwocolumn/leftcolumn_area.jsp";

		}

	}

	@Area("downloadproducts")
	@Controller
	@AvailableComponentClasses({L4ProductVersionComponent.class, L4ProductSpeciesGroupBarComponent.class})
	public static class DownloadProductArea {

		@RequestMapping("/da_singleproduct/downloadproducts")
		public String render(Node content) {

			return "areas/pagetwocolumn/leftcolumn_area.jsp";

		}

	}
}
