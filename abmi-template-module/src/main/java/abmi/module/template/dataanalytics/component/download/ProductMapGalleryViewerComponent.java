package abmi.module.template.dataanalytics.component.download;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.CompositeFieldBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueSubChildrenNodePropertiesTransformer;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.MainContent;
import abmi.module.template.util.TemplateUtil;

/* map gallery viewer, which lists a slider of images, and can be open up and navigate left and right*/
@Controller
@Template(title = "DA Image Gallery Viewer", id = "abmiModule:components/da_mapslider")
@TemplateDescription("DA Image Gallery Viewer")
@DownloadPageCommonComponent
@MainContent
public class ProductMapGalleryViewerComponent {
	@RequestMapping("/da_mapslider")
	public String render(Node content, ModelMap model) {

		model.put("maps", TemplateUtil.getMultiField(content, "maps"));
		return "data_analystics_portal/components/product_map_gallery_viewer.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(cfg.fields.multi("maps").label("image Details")
				.description("leave Image blank if it is same as the icon")
				.field(new CompositeFieldBuilder("headertextcompo")
						.fields(cfg.fields.link("iconUrl").label("thumbnail").description("small image")
								.appName("assets").targetWorkspace("dam"))
						.fields(cfg.fields.text("title").label("Image Caption").rows(1))
						.fields(cfg.fields.link("imageUrl").label("Image (optional)")
								.description("large image, leave it as blank if it is same as the icon")
								.appName("assets").targetWorkspace("dam")))
				.transformerClass(MultiValueSubChildrenNodePropertiesTransformer.class)

		);
	}
}
