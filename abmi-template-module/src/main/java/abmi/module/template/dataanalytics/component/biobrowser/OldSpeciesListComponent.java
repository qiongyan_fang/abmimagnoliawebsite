package abmi.module.template.dataanalytics.component.biobrowser;

import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.dataanalytics.component.download.DownloadPageCommonComponent;

// a search box for searching species ; 
// hmm, not sure if we need this after we used datatable plugin
@Controller
@Template(title = "DA Species List", id = "abmiModule:components/da_specieslist")
@TemplateDescription("DA Species List")
@DownloadPageCommonComponent
public class OldSpeciesListComponent {

	 @RequestMapping("/da_specieslist")
	    public String render() {
	    
	        return "data_analystics_portal/components/species_list.jsp";
	    }

	   
}
