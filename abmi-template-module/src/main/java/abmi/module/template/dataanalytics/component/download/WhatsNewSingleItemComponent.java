package abmi.module.template.dataanalytics.component.download;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "DA Single What's New", id = "abmiModule:components/da_whatsnewitem")
@TemplateDescription("DA Single What's New")
@DownloadPageCommonComponent
public class WhatsNewSingleItemComponent {
	 @RequestMapping("/da_whatsnewitem")
	    public String render() {
	    
	        return "data_analystics_portal/components/whatsnewitem.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	

			
	    	tab.fields(
	    		    
	                 cfg.fields.text("header").label("Header Text"),
	                 cfg.fields.text("date").label("Date"),
	                 cfg.fields.richText("description").label("description"),
	                 cfg.fields.link("url").label("Link Url").appName("pages").targetWorkspace("website")
	               
	         );
	    }
}
