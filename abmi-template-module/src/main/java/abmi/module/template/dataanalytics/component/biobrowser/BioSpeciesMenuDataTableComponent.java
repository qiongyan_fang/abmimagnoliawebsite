package abmi.module.template.dataanalytics.component.biobrowser;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.CompositeFieldBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueSubChildrenNodePropertiesTransformer;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

// data table showing menu and 
// a list of species with names, summary types, location implemented in datatable
@Controller
@Template(title = "DA Biobrowser Species List with Menu", id = "abmiModule:components/da_biobrowsersppgroup")

public class BioSpeciesMenuDataTableComponent {
	  @RequestMapping("/da_biobrowsersppgroup")
	  public String render(Node content, ModelMap model) {
			model.put("speciesIcons", TemplateUtil.getMultiField(content, "speciesIcons"));
			model.put("speciesGroups", TemplateUtil.getSpeciesGroupStringArray());
	        return "data_analystics_portal/components/biobrowser_species_group_navigation.jsp";
	    }
	  
	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				cfg.fields
				.multi("speciesIcons")
				.label("Species icons").description("leave species icons empty if use default ones")
				
				.field(new CompositeFieldBuilder("iconcompo")
						.fields(cfg.fields.select("speciesGroup")
								.label("Species Group")
								.options(TemplateUtil.getSpeciesGroup()))
						.fields(cfg.fields.link("imageurl").label("Icon Image")
								.appName("assets").targetWorkspace("dam")))
				.transformerClass(
						MultiValueSubChildrenNodePropertiesTransformer.class)
						,
		
				cfg.fields.link("speciesDetailUrl").label("Species Detail Link").targetWorkspace("website").appName("pages")
		);
	}
}