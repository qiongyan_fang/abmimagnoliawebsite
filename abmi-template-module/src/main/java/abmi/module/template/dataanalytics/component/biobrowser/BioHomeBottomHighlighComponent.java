package abmi.module.template.dataanalytics.component.biobrowser;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

@Controller
@Template(title = "DA Biobrowser Home Highlight", id = "abmiModule:components/da_biobrowserbottomhighlight")

public class BioHomeBottomHighlighComponent {
	  @RequestMapping("/da_biobrowserbottomhighlight")
	    public String render() {
	    
	        return "data_analystics_portal/components/biobrowser_bottom_highlight.jsp";
	    }
	  
	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				  cfg.fields.link("iconUrl").label("Icon Image").appName("assets").targetWorkspace("dam"),
	    			
  			    cfg.fields.text("header").label("Top Header"),
  			    cfg.fields.text("description").label("Description").rows(5),
  			    cfg.fields.link("buttonUrl").label("Button Url").appName("pages").targetWorkspace("website").description("select a page, or enter external page link starts with http"),
  			    cfg.fields.text("buttonText").label("Button Text").defaultValue("Learn More"),
  			    cfg.fields.link("backgroundImageUrl").label(" Image").appName("assets").targetWorkspace("dam")
  			   
		);
	}
}
