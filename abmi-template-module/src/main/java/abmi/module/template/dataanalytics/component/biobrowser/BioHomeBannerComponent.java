package abmi.module.template.dataanalytics.component.biobrowser;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.dataanalytics.component.home.BannerComponent;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

@Controller
@Template(title = "DA Biobrowser Home Banner", id = "abmiModule:components/da_biobrowserhomebanner")
@BannerComponent
public class BioHomeBannerComponent {
	  @RequestMapping("/da_biobrowserhomebanner")
	    public String render() {
	    
	        return "data_analystics_portal/components/biobrowser_home_banner.jsp";
	    }
	  
	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				  cfg.fields.link("iconUrl").label("Icon Image").appName("assets").targetWorkspace("dam"),
	    			
  			    cfg.fields.text("header").label("Top Header"),
  			    cfg.fields.text("description").label("Description").rows(5),
//  			    cfg.fields.link("buttonUrl").label("Button Url").appName("pages").targetWorkspace("website").description("select a page, or enter external page link starts with http"),
//  			    cfg.fields.text("buttonText").label("Button Text").defaultValue("Learn More"),
  			    cfg.fields.text("embed").label("Embedded html").rows(5),
  			    cfg.fields.link("backgroundImageUrl").label(" Image").appName("assets").targetWorkspace("dam")
  			   
		);
	}
}
