package abmi.module.template.dataanalytics.component.home;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@Template(id="abmiModule:components/da_da_towColumn", title="Two column layout")
public class DATwoColumnComponent {

   /**
    * Left column.
    */
	@Area(value="left", maxComponents=1)
   @Controller
   @AvailableComponentClasses(QuickLinkBoxComponent.class)
   public static class LeftArea {

       @RequestMapping("/da_towColumn/left")
       public String render() {
           return "areas/generalarea.jsp";
       }
   }

   /**
    * Right column.
    */
   @Area(value="right", maxComponents=1)
   
   @Controller
   @AvailableComponentClasses(QuickLinkBoxComponent.class)
   public static class RightArea {

       @RequestMapping("/da_towColumn/right")
       public String render() {
           return "areas/generalarea.jsp";
       }
   }

   @RequestMapping("/da_towColumn")
   public String render() {
       return "data_analystics_portal/components/home_twocolumn.jsp";
   }
}
