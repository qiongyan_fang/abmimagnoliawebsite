package abmi.module.template.dataanalytics.component.biobrowser;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;

import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

@Controller
@Template(title = "DA Biobrowser Home Guild Slider", id = "abmiModule:components/da_biobrowserhomeguildslider")
public class BioHomeGuildAreaComponent {
	@RequestMapping("/da_biobrowserhomeguildslider")
	public String render() {

		return "data_analystics_portal/components/biobrowser_home_guild_area.jsp";
	}

	/**
	 * Left column.
	 */
	@Area("single_guild_area")
	@Controller
	@AvailableComponentClasses({ BioHomeGuildItemComponent.class })
	public static class BioHomeGuildItemComponentlArea {

		@RequestMapping("/sa_biohomesingleguild/area")
		public String render() {
			return "areas/generalarea.jsp";
		}

	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(cfg.fields.text("guildTitle").label("Guild Caption/Title"));
	}
}
