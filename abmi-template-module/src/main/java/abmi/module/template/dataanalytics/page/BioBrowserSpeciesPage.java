package abmi.module.template.dataanalytics.page;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.*;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioSpeciesBannerComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioSpeciesBannerMultipleComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioSpeciesDetailComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioSpeciesMenuDataTableComponent;
import abmi.module.template.dataanalytics.component.biobrowser.BioSpeciesHorizontalMenuComponent;
import abmi.module.template.dataanalytics.component.biobrowser.ImageTextBannerComponent;
import abmi.module.template.dataanalytics.component.home.BannerComponent;
import abmi.module.template.dataanalytics.component.home.CommonReadMoreToggleComponent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "DA Biobrowser Species Page", id = "abmiModule:pages/da_biobrowserspecies")
public class BioBrowserSpeciesPage {

	@RequestMapping("/da_biobrowserspecies")
	public String render(Node page, ModelMap model) throws RepositoryException {

//		model.put("currentLink", MgnlContext.getAggregationState()
//				.getCurrentContentNode().getPath());
		return "data_analystics_portal/pages/da_biobrowser_species_page.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(
				cfg.fields.text("title").label("Title"),
				cfg.fields.checkbox("narrowcontent").buttonLabel("")
						.label("check to use narrow left column"),
				cfg.fields.text("keyword").label("keywords")
						.description("The keywords about this page"),
				cfg.fields
						.checkbox("hideInNavigation")
						.buttonLabel("")
						.label("Hide in navigation")
						.description(
								"Check this box to hide this page in navigation"));
	}

	@Available
	public boolean isAvailable(Node websiteNode) {

		return TemplateUtil.checkAvailability(websiteNode);

	}

	
	 /* rows with two columns layout, users to add news hightlight etc.*/
	 @Controller
	 @Area(value = "bannerArea", title = "Banner Area")
	 @Inherits
	 @AvailableComponentClasses({BannerComponent.class, ImageTextBannerComponent.class})

	 public static class BannerArea {

	     @RequestMapping("/da_biobrowserspecies/bannerArea")
	     public String render() {
	         return "areas/generalarea.jsp";
	     }
	 }
	 
	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses({TextContent.class,
			BioSpeciesMenuDataTableComponent.class, BioSpeciesDetailComponent.class, BioSpeciesHorizontalMenuComponent.class })
	public static class CenterColumnArea {

		@RequestMapping("/da_biobrowserspecies/leftColumn")
		public String render(Node content) {

			return "areas/pagetwocolumn/leftcolumn_area.jsp";

		}

	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/da_biobrowserspecies/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/da_biobrowserspecies/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
}
