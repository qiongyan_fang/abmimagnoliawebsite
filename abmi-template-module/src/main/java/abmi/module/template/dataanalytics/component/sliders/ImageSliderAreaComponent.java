package abmi.module.template.dataanalytics.component.sliders;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.MainContent;

@Controller
@Template(title = "Big Image Slider Area", id = "abmiModule:components/imagesliderarea")
@TemplateDescription("Big Image Slider Area")
@MainContent
public class ImageSliderAreaComponent {

	@Area("SingleImages")
	@Controller
	@AvailableComponentClasses(ImageSliderSingleComponent.class)
	public static class LeftArea {

		@RequestMapping("/imagesliderarea/singleImage")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@RequestMapping("/imagesliderarea")
	public String render() {
		return "data_analystics_portal/components/image_slider_component.jsp";
	}
	
	  @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
	              
	                 cfg.fields.checkbox("onRight").label("Display Text on right").buttonLabel("")
	         );
	    }
}
