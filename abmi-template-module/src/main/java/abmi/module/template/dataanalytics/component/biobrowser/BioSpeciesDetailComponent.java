package abmi.module.template.dataanalytics.component.biobrowser;

import java.io.File;

import java.util.Map;

import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.objectfactory.Components;
import info.magnolia.templating.functions.TemplatingFunctions;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import abmi.api.services.speciesprofiles.SpeciesProfileDAO;

import abmi.model.entity.biobrowser.WebSpeciesViewV;
import abmi.model.entity.biobrowser.graph.WebGraphAvailablity;
import abmi.model.services.biobrowser.BioBrowserService;
import abmi.model.services.biobrowser.GraphService;
import abmi.model.services.guilds.GuildServices;
import abmi.model.util.CommonUtil;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.dataanalytics.component.home.CommonReadMoreToggleComponent;

/**
 * 
 * @author Qiongyan
 *
 */
@Controller
@Template(title = "DA Species Details", id = "abmiModule:components/da_species_detail")
@TemplateDescription("DA Species Profile")

// detailed species information: graphs, text, maps,etc. used in species profile
// pages.
public class BioSpeciesDetailComponent {

	@Autowired
	BioBrowserService bioBrowserService;
	@Autowired
	GraphService graphService;

	@RequestMapping("/da_speciesprofile")
	public String render(@RequestParam(value = "sname", required = false) String sname,
			@RequestParam(value = "tsn", required = false) Integer tsn, Node content, ModelMap model,
			HttpServletResponse response, HttpServletRequest request) {

		/*---------------------------------------------
		 * get species information when in preview mode
		 * and when tsn is not empty 
		 *---------------------------------------------*/
		String scientificName = "";
		if (tsn != null && !Components.getComponent(TemplatingFunctions.class).isEditMode()) {

			String rootPath = graphService.getRootPath();
			try {
				WebSpeciesViewV sppRow = bioBrowserService.getSppMixedRow(tsn);
				model.put("groupId", sppRow.getWgaSppGroupId());
				model.put("groupName", sppRow.getSppGroup().getSgGroupName().replace(" ", "_"));
				scientificName = sppRow.getWsbScientificName();
				model.put("ScientificName", scientificName);
				model.put("CommonName", sppRow.getWsbCommonName());
				model.put("tsn", sppRow.getWsbTsn());
				model.put("hasMap", sppRow.getWgaMaps());
				try {
				
					String pathStr = "/report/" + sppRow.getSppGroup().getSgGroupName().replaceAll(" ", "_") + "\\"
							+ scientificName.replaceAll(" ", "_") + ".pdf";
					File outputPdfDest = new File(rootPath + pathStr);

					if (outputPdfDest.exists()) {
						model.put("report", pathStr);
					}
				} catch (Exception e3) {

				}

			} catch (Exception e) {
				e.printStackTrace();
				scientificName = sname;

			}
			/* first get species profile data from profile repository */
			if (scientificName != null && !"".equals(scientificName)) {
				try {
					Map<String, Object> profiles = SpeciesProfileDAO.getSingleSpeciesProfile(false, scientificName,
							request.getContextPath()); // CommonUtil.getServerPath(request));
				/*	ArrayList<Map<String, String>> images = (ArrayList<Map<String, String>>) profiles.get("WebPhotos");
					for (Map<String, String> row : CommonUtil.emptyIfNull(images)) {
						profiles.put(row.get("imageType"), row.get("imageLink"));
					}
				 */
					
					
					if (new File(rootPath + "/profiles/" + scientificName.replace(" ", "-") + "-large.jpg").exists()){
						profiles.put("mainBanner", "/profiles/" + scientificName.replace(" ", "-") + "-large.jpg");
					}
					
					if (new File(rootPath + "/profiles/" + scientificName.replace(" ", "-") + "-small.jpg").exists()){
						profiles.put("miniCircle", "/profiles/" + scientificName.replace(" ", "-") + "-small.jpg");
					}
					
					model.put("profile", profiles);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			/*
			 * -----------------------------------------------------------------
			 * ------------------ get data available information i.e forest
			 * prairie is FULL (1) /FEW (2) /NONE (3)
			 * -----------------------------------------------------------------
			 * -----------------
			 */
			try {
//				WebSpeciesViewV rowStatus = bioBrowserService.getSppRow(tsn);

				WebGraphAvailablity graphStatus = bioBrowserService.getSppGraphStatus(tsn);
				model.put("forestModel", graphStatus.getWebForestModel().getWgmtId());
				model.put("prairieModel", graphStatus.getWebPrairieModel().getWgmtId());
				model.put("modelComments", graphStatus.getWgsComments());
			} catch (Exception e) {
				model.put("forestModel", 3); // set it as NONE
				model.put("prairieModel", 3); // set it as NONE

			}

			/*
			 * -----------------------------------------------------------------
			 * ------------------ get graph data
			 * -----------------------------------------------------------------
			 * -----------------
			 */
			model.put("graphs", graphService.getAllGraphs(tsn, false));
		}
		return "data_analystics_portal/components/species_detail.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				cfg.fields.link("imageUrl").label("Default Image").appName("assets").targetWorkspace("dam"),
				cfg.fields.link("iconUrl").label("Default icon").appName("assets").targetWorkspace("dam")

		);
	}

	@Area("introduction") // Habitat & Human Footprint Associations
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, CommonReadMoreToggleComponent.class })
	public static class introductionArea {

		@RequestMapping("/da_species_detail/introduction")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	
	@Area("introduction_basic") // Habitat & Human Footprint Associations
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, CommonReadMoreToggleComponent.class })
	public static class basicIntroductionArea {

		@RequestMapping("/da_species_detail/introduction_basic")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}
	@Area("method_fdfa") // north side few detection
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, CommonReadMoreToggleComponent.class })
	public static class FewDetectionNorthhArea {

		@RequestMapping("/da_species_detail/method_fafa")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("method_fdpa") // north side few detection
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, CommonReadMoreToggleComponent.class })
	public static class FewDetectionSouthArea {

		@RequestMapping("/da_species_detail/method_fdpa")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("method_hhfa") // Habitat & Human Footprint Associations
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, CommonReadMoreToggleComponent.class })
	public static class Method1Area {

		@RequestMapping("/da_species_detail/method_hhfa")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("method_ihf") // Impacts of Human Footprint
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, CommonReadMoreToggleComponent.class })
	public static class Method2Area {

		@RequestMapping("/da_species_detail/method_ihf")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("method_pra") // Predicted Relative Abundance
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, CommonReadMoreToggleComponent.class })
	public static class Method3Area {

		@RequestMapping("/da_species_detail/method_pra")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("method_icc") // Impacts of Climate Change
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, CommonReadMoreToggleComponent.class })
	public static class Method4Area {

		@RequestMapping("/da_species_detail/method_icc")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("method_fd") // with few detection
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, CommonReadMoreToggleComponent.class })
	public static class Method5Area {

		@RequestMapping("/da_species_detail/method_fd")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("method_graph1") // with few detection
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class, CommonReadMoreToggleComponent.class })
	public static class methodGraph1Area {

		@RequestMapping("/da_species_detail/method_graph1")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("method_graph2") // with few detection
	@Controller
	@AvailableComponentClasses({ CommonReadMoreToggleComponent.class, RichTextComponent.class })
	public static class methodGraph2Area {

		@RequestMapping("/da_species_detail/method_graph2")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}
}
