package abmi.module.template.dataanalytics.component.download;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "DA Species link Bars", id = "abmiModule:components/da_singleproductspecies")
@TemplateDescription("DA Species link Bars")

public class L4ProductSpeciesGroupBarComponent {
	@RequestMapping("/da_singleproductspecies")
	public String render() {

		return "data_analystics_portal/components/product_singleproduct_species.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				cfg.fields.text("productname").label("Product Name"),
				cfg.fields.link("image").label("Default Image").appName("assets").targetWorkspace("dam").description("circle icons"),
				cfg.fields.link("url").label("Url").appName("pages").targetWorkspace("website").description("internal or external link")

		);
	}

	@Area("links")
	@Controller
	@AvailableComponentClasses(L5ProductDownloadDetailComponent.class)
	public static class linkArea {

		@RequestMapping("/da_singleproductspecies/links")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

}
