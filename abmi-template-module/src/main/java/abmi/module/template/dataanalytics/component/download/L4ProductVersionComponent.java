package abmi.module.template.dataanalytics.component.download;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@Template(title = "DA Single Product", id = "abmiModule:components/da_singleproductversion")
@TemplateDescription("DA Single Product")

public class L4ProductVersionComponent {
	 @RequestMapping("/da_singleproductversion")
	    public String render() {
	    
	        return "data_analystics_portal/components/product_singleproduct.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	    	tab.fields(
	    		    
	                 cfg.fields.text("productname").label("Version Name"),
	                 cfg.fields.text("lastupdated").label("Updated Date")
	            
	         );
	    }
	    
	    @Area("links")
	    @Controller
	    @AvailableComponentClasses(L5ProductDownloadDetailComponent.class)
	    public static class linkArea {

	        @RequestMapping("/da_singleproduct/links")
	        public String render() {
	            return "areas/generalarea.jsp";
	        }
	    }
	   
}
