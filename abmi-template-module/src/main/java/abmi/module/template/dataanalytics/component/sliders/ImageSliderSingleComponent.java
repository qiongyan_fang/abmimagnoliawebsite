
package abmi.module.template.dataanalytics.component.sliders;


import javax.jcr.Node;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Single Image/TExt", id = "abmiModule:components/imageslidersingleimage")


public class ImageSliderSingleComponent {
	 @RequestMapping("/imageslidersingleimage")
	    public String render(Node content) {
			
		 return "data_analystics_portal/components/image_slider_single_component.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	    	tab.fields(
	    		    cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),
	    		    cfg.fields.link("url").label("link").appName("pages").targetWorkspace("website"),
	                 cfg.fields.text("header").label("header").rows(5),
	                 cfg.fields.richText("description").label("description")	                 
	                 
	         );
	    }
}
