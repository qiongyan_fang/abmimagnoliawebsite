package abmi.module.template.rawdata;


import javax.jcr.Node;
import javax.jcr.RepositoryException;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.*;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.util.TemplateUtil;


@Controller
@Template(title = "Raw Data Download Page", id = "abmiModule:pages/rawdatadownloadpage")

public class  RawDataPageTemplate {

	@RequestMapping("/rawdatadownloadpageTemplate")
	public String render(Node page, ModelMap model) throws RepositoryException {

		// Map<String, String> navigation = new LinkedHashMap<String, String>();
		// for (Node node :
		// NodeUtil.getNodes(page.getSession().getNode("/home"),
		// NodeTypes.Page.NAME)) {
		// if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {
		// navigation.put(node.getPath(), PropertyUtil.getString(node, "title",
		// ""));
		// System.out.println("level 1: " + node.getPath() + " "+
		// PropertyUtil.getString(node, "title", "")) ;
		// for (Node subNode: NodeUtil.getNodes(node)){
		// System.out.println(PropertyUtil.getString(subNode, "headerline", "")
		// + "  " +PropertyUtil.getBoolean(subNode, "hideInNavigation", false));
		// }
		// }
		// }
		// model.put("navigation", navigation);
		//
//		model.put("currentLink", MgnlContext.getAggregationState().getCurrentContentNode().getPath());
		return "pages/rawdatadownloadpages.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(cfg.fields.text("title").label("Title"),
//				cfg.fields.checkbox("narrowcontent").buttonLabel("").label("check to use narrow left column"),
				
				cfg.fields
				.checkbox("rawdatadownloadpage").buttonLabel("").label("Raw Data Download"),
				cfg.fields
				.checkbox("hideInNavigation").buttonLabel("").label("Hide in navigation")
				.description("Check this box to hide this page in navigation"));
	}

	// header images
    @Area(value="jumbotron", maxComponents=1)
    @Controller
    @Inherits(components = ComponentInheritanceMode.FILTERED)
    @AvailableComponentClasses({JumbotronComponent.class})
    
    public static class JumbotronArea {

        @RequestMapping("/rawdatadownloadpageTemplate/jumbotron")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }

    @Controller
    @Area(value = "megaMenuArea", title = "Top Navigation Bar")   
    @AvailableComponentClasses({MegaMenuAreaComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class topNavigationBarArea {

        @RequestMapping("/rawdatadownloadpageTemplate/topBar")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
	@Area(value="titleArea", maxComponents=1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class})
	public static class titleArea {

		@RequestMapping("/rawdatadownloadpageTemplate/title")
		public String render() {
			
			return "areas/title_area.jsp";
		}
	}
	
	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses({ TextComponent.class, RawPageNavigation.class })
	  @Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class LeftColumnArea {

		@RequestMapping("/rawdatadownloadpageTemplate/leftColumn")
		public String render(Node content) {
			
				return "areas/generalarea.jsp";	
			
			
		}
		
	
		
	}

	
	
	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses( {MainContent.class, RawDataContentComponent.class}
	 )
	public static class ReftColumnArea {

		@RequestMapping("/rawdatadownloadpageTemplate/rightColumn")
		public String render() {
			return "areas/generalarea.jsp";	
		}
	}


    @Controller
    @Area(value = "footer", title = "Footer Menu", maxComponents=2)   
    @AvailableComponentClasses({FooterAreaComponent.class})
 @Inherits(components = ComponentInheritanceMode.ALL)
    public static class FooterMenuArea {

        @RequestMapping("/rawdatadownloadpageTemplate/footermenu")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "finePrintFooter", title = "Fine Print Footer Links" , maxComponents=1)     
    @AvailableComponentClasses({FinePrintFooterComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class FineFooterMenuArea {

        @RequestMapping("/rawdatadownloadpageTemplate/finefootermenu")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Available
	public boolean isAvailable(Node websiteNode) {

		
		return TemplateUtil.checkAvailability(websiteNode);

	}	
	
}
