package abmi.module.template.rawdata;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Template;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Raw Data Download Component", id = "abmiModule:component/rawdatadownloadcomponent")

public class RawDataContentComponent {
	@RequestMapping("/rawdatadownloadcomponent")
	public String render(Node page, ModelMap model) throws RepositoryException {


		return "components/rawdatadownload_main.jsp";
	}
}
