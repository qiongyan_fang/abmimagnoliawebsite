package abmi.module.template.rawdata;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;


import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Raw Data Navigation", id = "abmiModule:components/rawpagelist")
@TemplateDescription("Raw Data Navigation")
@RawPageNavigation
public class RawDataLeftMenuComponent {

	@RequestMapping("/rawpagelistbox")
	public String render(ModelMap model, Node content)
			throws RepositoryException {
		// try{
		// MgnlContext.getWebContext().getResponse().setHeader("Cache-Control",
		// "no-cache");
		// }catch(Exception e){
		// System.out.println("rawpagelistComponent disable cache failed:" +
		// e.getMessage());
		// }

		String path = content.getProperty("rootpath").getString();
		ArrayList<Map<String, Object>> rawpagelist = new ArrayList<Map<String, Object>>();

		for (Node node : NodeUtil.getNodes(content.getSession().getNode(path),
				NodeTypes.Page.NAME)) {
			if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {
				// System.out.println(content.getPath() + " == ? " +
				// node.getPath());

				Map<String, Object> childRawpagelist = new LinkedHashMap<String, Object>();
				childRawpagelist.put("path", node.getPath());
				childRawpagelist.put("title",
						PropertyUtil.getString(node, "title", ""));
				childRawpagelist.put("classname",
						node.getName());

				ArrayList<Map<String, Object>> subrawpagelist = new ArrayList<Map<String, Object>>();
				for (Node subnode : NodeUtil.getNodes(node,
						NodeTypes.Page.NAME)) {
					if (!PropertyUtil.getBoolean(subnode, "hideInNavigation",
							false)) {
						Map<String, Object> childRawpagelist2 = new LinkedHashMap<String, Object>();
						childRawpagelist2.put("path", subnode.getPath());
						childRawpagelist2.put("title",
								PropertyUtil.getString(subnode, "title", ""));
						childRawpagelist2.put("classname",
								node.getName());

						subrawpagelist.add(childRawpagelist2);
					}
					
				}
				
				childRawpagelist.put("pages",subrawpagelist);

				rawpagelist.add(childRawpagelist);
			}

		}

		model.put("subpage", rawpagelist);

		return "components/rawdata_subpagelist.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(

		cfg.fields.link("rootpath").label("Link Url").appName("pages")
				.targetWorkspace("website"),
		// cfg.fields.text("header").label("Header Text"),
		// cfg.fields.checkbox("includeroot").buttonLabel("include the root page in the navigation").label("Include the root page?"),
		// cfg.fields.text("roottext").label("Text of the root page (default:title)"),
		//
				cfg.fields.checkbox("inheritable").buttonLabel("")
						.label("Inheritable").description("Show in Subpages?")

		// ,
		// cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),
		// cfg.fields.text("headerline1").label("Top Header line Text"),
		// cfg.fields.text("headerline2").label("Middle Header line Text"),
		// cfg.fields.text("content").label("Content Text")
		);
	}
}
