package abmi.module.template.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import info.magnolia.cms.util.QueryUtil;

import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.jcr.util.SessionUtil;
import info.magnolia.ui.form.config.OptionBuilder;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.UnsupportedRepositoryOperationException;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import abmi.model.util.ModuleParameters;

public class TemplateUtil {
	private static final Logger log = LoggerFactory.getLogger(TemplateUtil.class);

	public static boolean checkAvailability(Node websiteNode) {
		try {
			if (websiteNode.getPath().startsWith("/home")) {
				return true;
			} else {
				return false;
			}
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public static boolean checkAvailability(Node websiteNode, String path) {
		try {
			if (websiteNode.getPath().startsWith(path) || websiteNode.getPath().startsWith("/temp")
					|| websiteNode.getPath().startsWith("/")) {
				return true;
			} else {
				return false;
			}
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 
	 * @param content
	 * @param templateName
	 * @return 0 for the first one, -1 not found
	 */
	public static int getNodePosition(final Node content, final String templateName) {
		if (content != null) {

			Node parent;
			try {
				parent = content.getParent();
				if (parent != null) {
					final NodeIterator it = parent.getNodes(); // NodeTypes.Area,
																// NodeTypes.Component
					int position = -1;

					while (it.hasNext()) {

						final Node node = (Node) it.next();
						if (NodeTypes.Renderable.getTemplate(node).equals(templateName)) {
							position++;

							if (StringUtils.equals(node.getIdentifier(), content.getIdentifier())) {

								return position;
							}
						}

					}
				}
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	/**
	 * 
	 * @param content
	 * @return
	 */
	public static String getFirstChildPagePath(final Node content) {

		if (content != null) {

			try {

				final NodeIterator it = content.getNodes(); // NodeTypes.Area,
															// NodeTypes.Component

				while (it.hasNext()) {

					final Node node = (Node) it.next();
					if (node.isNodeType(NodeTypes.Page.NAME)) {
						return node.getPath();
					}
				}

			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
		return "/home.html";
	}

	public static List<String> getBaseColor() {
		ArrayList<String> colors = new ArrayList<String>();
		colors.add("transparent");
		colors.add("  ");
		colors.add("green");
		colors.add("cyan");
		colors.add("orange");
		colors.add("blue");
		colors.add("red");
		colors.add("grey");

		return colors;
	}

	public static List<String> getSocialMedia() {
		ArrayList<String> colors = new ArrayList<String>();
		colors.add("facebook");
		colors.add("twitter");
		colors.add("vimeo");

		return colors;
	}

	/**
	 * 
	 * @param description
	 * @param charInt
	 * @return
	 */
	public static String getCharByNumber(String description, int charInt) {
		if (description.length() > charInt) {
			int firstIndexOf = description.indexOf(" ", charInt - 10);
			String returnStr = description.substring(0, (firstIndexOf < 0 ? charInt : firstIndexOf));

			return returnStr + "...";
		} else {
			return description;
		}
	}

	/**
	 * convert multiField into arrays
	 * 
	 * @param content
	 * @param fieldName
	 * @return
	 */
	public static ArrayList<Map<String, String>> getMultiField(Node content, String fieldName) {
		ArrayList<Map<String, String>> textList = new ArrayList<Map<String, String>>();
		try {
			for (Node node : NodeUtil.getNodes(content.getNode(fieldName))) {
				Map<String, String> newMap = new HashMap<String, String>();
				PropertyIterator it = node.getProperties();
				while (it.hasNext()) {
					Property property = it.nextProperty();
					if (property.getName().indexOf(":") == -1) {
						newMap.put(property.getName(), PropertyUtil.getString(node, property.getName()));

					}
				}

				textList.add(newMap);
			}

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return textList;
	}

	/**
	 * used for rest, have abosolute path to assets
	 * 
	 * @param content
	 * @param fieldName
	 * @return
	 */
	public static ArrayList<Map<String, String>> getMultiFieldREST(Node content, String fieldName, String serverPath) {
		ArrayList<Map<String, String>> textList = new ArrayList<Map<String, String>>();
		try {
			for (Node node : NodeUtil.getNodes(content.getNode(fieldName))) {
				Map<String, String> newMap = new HashMap<String, String>();
				PropertyIterator it = node.getProperties();
				while (it.hasNext()) {
					Property property = it.nextProperty();
					if (property.getName().indexOf(":") == -1) {
						newMap.put(property.getName(), PropertyUtil.getString(node, property.getName()));

						String path = PropertyUtil.getString(node, property.getName(), "");

						if (property.getName().endsWith("Link") && !path.equals("")) { // if
																						// it
																						// is
																						// a
							// link,
							// then get
							// asset
							// node

							newMap.put(property.getName(), TemplateUtil.getAssetPath(serverPath, path));

						}

					}
				}

				textList.add(newMap);
			}

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return textList;
	}

	/**
	 * convert multiField into arrays
	 * 
	 * @param content
	 * @param fieldName
	 * @return
	 */
	public static ArrayList<String> getMultiFieldValues(Node content, String fieldName) {

		ArrayList<String> valueList = new ArrayList<String>();
		try {
			ArrayList<String> textList = new ArrayList<String>();
			PropertyIterator it = content.getNode(fieldName).getProperties();
			while (it.hasNext()) {
				Property property = it.nextProperty();
				if (property.getName().indexOf(":") == -1) {
					// textList.add(property.getString());
					textList.add(property.getName());
				}
			}

			Object[] tmpArray = textList.toArray();
			Arrays.sort(tmpArray);

			for (Object path : tmpArray) {
				valueList.add(content.getNode(fieldName).getProperty(path.toString()).getString());
			}
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		return valueList;
	}

	/**
	 * JCR values stored as JSON, split the values and put them into list
	 * 
	 * @param content
	 * @param fieldName
	 * @return
	 */
	public static ArrayList<String> getMultiFieldJSONValues(Node content, String fieldName) {
		ArrayList<String> textList;
		try {

			String value = PropertyUtil.getString(content, fieldName, null);
			if (value == null)
				return null;

			String[] valueList = value.split(",");
			textList = new ArrayList<String>(10);
			for (int i = 0; valueList != null && i < valueList.length; i++) {
				if (valueList[i] != null && !"".equals(valueList[i])) {
					textList.add(valueList[i]);
				}
			}

			if (textList.size() > 0)
				return textList;
			else
				return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static HashMap<String, String> getMultiFieldJSONValues(Node content, String fieldName,
			HashMap<String, String> op) {
		ArrayList<String> textList;
		try {

			String value = PropertyUtil.getString(content, fieldName, null);
			if (value == null)
				return null;

			String[] valueList = value.split(",");
			HashMap<String, String> keySet = new HashMap<String, String>();
			for (int i = 0; valueList != null && i < valueList.length; i++) {
				if (valueList[i] != null && !"".equals(valueList[i])) {

					keySet.put(valueList[i], op.get(valueList[i]));

				}
			}
			return keySet;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static ArrayList<Map<String, String>> getSupplementalFieldValues(Node content, String fieldName) {
		ArrayList<Map<String, String>> textList = new ArrayList<Map<String, String>>();
		try {

			PropertyIterator it = content.getNode(fieldName).getProperties();
			while (it.hasNext()) {
				Property property = it.nextProperty();
				if (property.getName().indexOf(":") == -1) {
					String pageLink = property.getString();
					try {
						Node pageNode = content.getSession().getNode(pageLink);
						Map<String, String> docMap = new HashMap<String, String>();
						docMap.put("title", PropertyUtil.getString(pageNode, "title", ""));
						String link = PropertyUtil.getString(pageNode, "viewlink", "");
						docMap.put("viewlink", link);

						Map<String, String> fileProperties = TemplateUtil.getAssetsProperty(link);
						if (fileProperties != null) {
							docMap.put("extension", fileProperties.get("extension"));
							docMap.put("size", fileProperties.get("size"));
						}
						textList.add(docMap);
					} catch (Exception emptyErr) {
						log.warn("error getting sup link {}", emptyErr.getMessage());
					}

				}
			}

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		return textList;
	}

	public static Map<String, String> getDepartmentOptions() {

		Map<String, String> department = new HashMap<String, String>();
		department.put("General Questions", "General Questions");
		department.put("Media Inquiries", "Media Inquiries");
		/*
		 * department.put("Application Centre", "Application Centre");
		 * department.put("Information Centre", "Information Centre");
		 * department.put("Geospatial Centre", "Geospatial Centre");
		 * department.put("Monitoring Centre", "Monitoring Centre");
		 * department.put("Processing Centre", "Processing Centre");
		 * department.put("Science Centre", "Science Centre");
		 */

		return department;
	}

	public static Map<String, String> getABMIDepartmentOptions() {

		Map<String, String> department = new HashMap<String, String>();

		department.put("Application Centre", "Application Centre");
		department.put("Information Centre", "Information Centre");
		department.put("Geospatial Centre", "Geospatial Centre");
		department.put("Monitoring Centre", "Monitoring Centre");
		department.put("Processing Centre", "Processing Centre");
		department.put("Science Centre", "Science Centre");

		return department;
	}

	public static ArrayList<String> getBoxWidth() {
		ArrayList<String> boxSizeList = new ArrayList<String>();
		boxSizeList.add("wide");
		boxSizeList.add("medium");
		boxSizeList.add("narrow");

		return boxSizeList;
	}

	public static ArrayList<String> getPublicationSubjectArea1() {
		ArrayList<String> boxSizeList = new ArrayList<String>();
		boxSizeList.add("aquatic");
		boxSizeList.add("biodiversity intactness");
		boxSizeList.add("climate change");
		boxSizeList.add("cumulative effects");
		boxSizeList.add("ecosystem services");
		boxSizeList.add("habitat");
		boxSizeList.add("human footprint");
		boxSizeList.add("mapping");
		boxSizeList.add("monitoring");
		boxSizeList.add("oil sands");
		boxSizeList.add("rare species");
		boxSizeList.add("reclamation");
		boxSizeList.add("species");
		boxSizeList.add("stakeholder engagement");
		boxSizeList.add("terrestrial");
		return boxSizeList;

	}

	/*
	 * - Status of Biodiversity Reports - Monitoring Reports - Applied Research
	 * Reports - Protocols - Data Analysis Reports - Annual Reports - Outreach
	 * and Education Documents - Peer-reviewed Publications - About the ABMI
	 * Documents
	 */
	public static ArrayList<String> getPublicationDocumetTypeArea1() {
		ArrayList<String> boxSizeList = new ArrayList<String>();
		boxSizeList.add("About the ABMI");
		boxSizeList.add("Annual Reports");
		boxSizeList.add("Applied Research Reports");
		boxSizeList.add("Data Analysis Reports");
		boxSizeList.add("Monitoring Reports");
		boxSizeList.add("Outreach & Education");
		boxSizeList.add("Peer-reviewed Publications");
		boxSizeList.add("Protocols");
		boxSizeList.add("Status of Biodiversity Reports");

		return boxSizeList;

	}

	public static Map<String, List<String>> getPublicationTagsOrg() {
		Properties prop = new Properties();
		InputStream input = null;
		HashMap<String, List<String>> result = new HashMap<String, List<String>>();
		try {
			input = TemplateUtil.class.getClassLoader().getResourceAsStream("web.properties");
			// load a properties file
			prop.load(input);

			// get the property value and print it out
			String documentType = prop.getProperty("document.type");
			String[] documentList = documentType.split(",");

			String subject = prop.getProperty("document.subject");
			String[] subjectList = subject.split(",");

			result.put("documentType", Arrays.asList(documentList));
			result.put("subject", Arrays.asList(subjectList));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return result;

	}

	public static LinkedHashMap<String, Object> getPublicationTags(boolean includeOption) {

		Properties prop = new Properties();
		InputStream input = null;
		LinkedHashMap<String, Object> result = new LinkedHashMap<String, Object>();
		try {
			input = TemplateUtil.class.getClassLoader().getResourceAsStream("new-web.properties");
			// load a properties file
			prop.load(input);

			// get the property value and print it out
			String documentType = prop.getProperty("document.subtype");
			String[] documentList = documentType.split(",");

			String subject = prop.getProperty("document.subject");
			String[] subjectList = subject.split(",");

			OptionBuilder[] typeOptions = new OptionBuilder[documentList.length];
			Map<String, String> typeMap = new HashMap<String, String>();
			for (int i = 0; i < documentList.length; i++) {

				typeOptions[i] = new OptionBuilder();
				String[] pair = documentList[i].split("=");
				typeOptions[i].value(pair[0]);
				typeOptions[i].label(pair[1]);

				typeMap.put(pair[0], pair[1]);

			}

			OptionBuilder[] subjectOptions = new OptionBuilder[subjectList.length];
			Map<String, String> subjectMap = new HashMap<String, String>();
			for (int i = 0; i < subjectList.length; i++) {

				subjectOptions[i] = new OptionBuilder();
				String[] pair = subjectList[i].split("=");
				subjectOptions[i].value(pair[0]);
				subjectOptions[i].label(pair[1]);

				subjectMap.put(pair[0], pair[1]);
			}

			String documentMainType = prop.getProperty("document.type");
			String[] documentMainList = documentMainType.split(",");
			Map<String, String> mainTypeMap = new HashMap<String, String>();
			for (int i = 0; i < documentMainList.length; i++) {

				String[] pair = documentMainList[i].split("=");

				mainTypeMap.put(pair[0], pair[1]);
			}

			if (includeOption) {
				result.put("documentType", typeOptions);
				result.put("subject", subjectOptions);
			}
			result.put("documentMap", typeMap);
			result.put("subjectMap", subjectMap);
			result.put("mainTypeMap", mainTypeMap);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return result;

	}

	public static Map<String, String> getPostition() {
		Map<String, String> positionCategories = new LinkedHashMap<String, String>();
		positionCategories.put("left", "left");
		positionCategories.put("right", "right");

		return positionCategories;
	}

	public static Map<String, String> getWidth() {
		Map<String, String> positionCategories = new LinkedHashMap<String, String>();
		positionCategories.put("full", "full");
		positionCategories.put("half", "half");

		return positionCategories;
	}

	public static ArrayList<String> getAllGlossaryItems(String path) {

		String query = "select * from [mgnl:component] " + " where ISDESCENDANTNODE([" + path
				+ "]) and [mgnl:template] = 'abmiModule:components/glossaryitem' " + " order by name ";

		try {
			// NodeIterator results = QueryUtil.search("website", query,
			// "JCR-SQL2", "mgnl:component");

			NodeIterator results = QueryUtil.search("website", query);

			ArrayList<String> options = new ArrayList<String>();

			while (results.hasNext()) {
				Node node = (Node) results.next();

				options.add(PropertyUtil.getString(node, "name"));

			}

			// Session session = MgnlContext.getJCRSession("website");
			//
			// QueryManager manager = session.getWorkspace().getQueryManager();
			// javax.jcr.query.Query query2 = manager.createQuery(query,
			// javax.jcr.query.Query.JCR_SQL2);
			// QueryResult result = query2.execute();
			// System.out.println("source mgnlcontext " + query + "=>" +
			// result.getNodes().getSize());
			//
			// NodeIterator results3 =
			// NodeUtil.filterParentNodeType(query2.execute().getNodes(),
			// MgnlNodeType.NT_COMPONENT);
			// System.out.println("filtered " + results3.getSize());
			//

			return options;
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static String getOneGlossaryItem(String termname) {

		String query = "select * from [mgnl:component] where [mgnl:template] = 'abmiModule:components/glossaryitem' and "
				+ "		name ='" + termname + "'";

		try {

			NodeIterator results = QueryUtil.search("website", query);

			if (results.hasNext()) {
				Node node = (Node) results.next();
				return PropertyUtil.getString(node, "detail", "NA");
			} else {
				return "glossary name not found";
			}

		} catch (LoginException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return "error";
	}

	/**
	 * 
	 * @param termname
	 * @return
	 */

	public static String getOneItemFromGlossaryWorkSpace(String termname) {

		String query = "select * from [mgnl:glossary] where " + "		glossaryName ='" + termname + "'";

		try {
			log.warn("query = {}", query);
			NodeIterator results = QueryUtil.search(ModuleParameters.GLOSSARY_WORKSPACE, query);

			if (results.hasNext()) {
				Node node = (Node) results.next();
				log.warn("node = {}", node);
				return PropertyUtil.getString(node, "definition", "");
			} else {
				log.warn("no result ");
				return "glossary name not found";
			}

		} catch (LoginException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return "error";
	}

	static public String toJSON1(Object result) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			// System.out.println(mapper.writeValueAsString(result));
			return mapper.writeValueAsString(result);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block

		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block

		} catch (IOException e) {
			// TODO Auto-generated catch block

		}

		return "{error:'can't convert to JSON}";
	}

	/**
	 * try to get assets's property, file name size etc.
	 * 
	 * @param pathName
	 * @return
	 */
	public static Map<String, String> getAssetsProperty(String pathName) {

		if (true) {
			return null;
		}
		int slashLoc = pathName.lastIndexOf("/");
		String path = "/xxxx$$$";
		String name = "xxx";
		if (slashLoc != -1) {
			path = pathName.substring(0, slashLoc);
			name = pathName.substring(slashLoc + 1);
		} else {
			return null;
		}

		String query = "select * from [mgnl:resource] where " + "		ISDESCENDANTNODE(['" + pathName + "']) "; // and
																													// upper(name)
																													// ='"
																													// +
																													// (name==null?"
																													// ":name.toUpperCase())
																													// +
																													// "'";

		try {
			// log.warn("query = {}", query);
			NodeIterator results = QueryUtil.search("dam", query);

			if (results.hasNext()) {
				Map<String, String> resultMap = new HashMap<String, String>();
				Node node = (Node) results.next();
				// log.warn("node = {}", node);
				long fileSize = PropertyUtil.getLong(node, "size", 0L);

				// log.warn("node = {} filesize={}", node, fileSize );

				// for (Node subNode: NodeUtil.getNodes(node)){
				// log.warn("subNode = {} fielsize={}", subNode,
				// PropertyUtil.getLong(subNode,"size", -0L) );
				// }

				String fileSizeStr;
				if (fileSize < 1024) {
					fileSizeStr = "1K";
				} else if (fileSize < 148576) { /* < 1M */
					fileSizeStr = String.format("%.0f", ((double) fileSize) / 1024) + "K";
				} else { /* < 1M */
					fileSizeStr = String.format("%.0f", ((double) fileSize) / 148576) + "M";
				}
				String extName = PropertyUtil.getString(node, "extension", "");
				resultMap.put("size", fileSizeStr);
				resultMap.put("extension", extName);
				return resultMap;

			} else {
				log.warn("{} can't be found ", pathName);

			}
			return null;

		} catch (LoginException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param session
	 * @param localRootPage
	 * @param versionGroup
	 * @return
	 */
	public static boolean updateSingleVersion(Session session, String localRootPage, String versionGroup) {
		QueryManager manager2;
		try {
			manager2 = session.getWorkspace().getQueryManager();

			String queryString2 = "select * from [mgnl:page] as t where ISDESCENDANTNODE([" + localRootPage
					+ "]) and versiongrouping ='" + versionGroup + "' order by publishdate desc";
			javax.jcr.query.Query query2 = manager2.createQuery(queryString2, javax.jcr.query.Query.JCR_SQL2);

			QueryResult result2 = query2.execute();

			NodeIterator nodeIter2 = result2.getNodes();
			int index = 0;
			long totalCount = nodeIter2.getSize();
			while (nodeIter2 != null && nodeIter2.hasNext()) {

				Node node2 = (Node) nodeIter2.next();

				node2.setProperty("versioncount", totalCount);
				if (index == 0) { // show first (newest, but hide older ones)
					node2.setProperty("hideInNavigation", false);
					node2.setProperty("isoldversion", false);

				} else {
					node2.setProperty("hideInNavigation", true);
					node2.setProperty("isoldversion", true);
				}
				index++;
			}

			session.save();
			return true;

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * 
	 * @param session
	 * @param localRootPage
	 * @param versionGroup
	 * @return
	 */
	public static List<Map<String, Object>> getAllVersion(Session session, String localRootPage, String versionGroup) {

		QueryManager manager2;
		try {
			manager2 = session.getWorkspace().getQueryManager();

			String queryString2 = "select * from [mgnl:page] as t where ISDESCENDANTNODE([" + localRootPage
					+ "]) and versiongrouping ='" + versionGroup + "' order by publishdate desc";
			javax.jcr.query.Query query2 = manager2.createQuery(queryString2, javax.jcr.query.Query.JCR_SQL2);

			QueryResult result2 = query2.execute();

			NodeIterator nodeIter2 = result2.getNodes();

			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

			while (nodeIter2 != null && nodeIter2.hasNext()) {

				Node node2 = (Node) nodeIter2.next();
				Map<String, Object> pageInfo = new HashMap<String, Object>();
				pageInfo.put("title", PropertyUtil.getString(node2, "title", ""));
				pageInfo.put("path", node2.getPath());

				pageInfo.put("version", PropertyUtil.getString(node2, "version", ""));

				pageInfo.put("isoldversion", PropertyUtil.getBoolean(node2, "isoldversion", false));

				resultList.add(pageInfo);

			}

			return resultList;

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static String getAssetPath(String serverPath, String path) {
		if (path == null || "".equals(path)) {
			return "";
		}

		/* getNode(String workspace, String path) */
		Node subnode = SessionUtil.getNode("dam", path);

		if (subnode == null) {
			return "";
		}
		try {
			return serverPath + "/dam/jcr:" + subnode.getIdentifier() + "/" + subnode.getName();
		} catch (UnsupportedRepositoryOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "";
	}

	public static <T> Iterable<T> emptyIfNull(Iterable<T> iterable) {
		return iterable == null ? Collections.<T>emptyList() : iterable;
	}

	public static ArrayList<String> getDownloadFileType() {
		ArrayList<String> extension = new ArrayList<String>();
		extension.add("none");
		extension.add("ftp");
		extension.add("zip");
		extension.add("pdf");
		extension.add("excel");
		extension.add("word");
		extension.add("map");
		extension.add("image");
		extension.add("other");
		

		return extension;
	}

	public static OptionBuilder[] getSpeciesGroup() {

		OptionBuilder[] extension = {

				new OptionBuilder().value("1").label("Birds"), 
				
				new OptionBuilder().value("2").label("Vascular Plants"),
				new OptionBuilder().value("3").label("Soil Mites"), new OptionBuilder().value("4").label("Lichens"),
				new OptionBuilder().value("5").label("Bryophytes"),
				new OptionBuilder().value("6").label("Aquatic Invertabrates") ,
				new OptionBuilder().value("7").label("Mammals")};

		return extension;
	}

	public static String[] getSpeciesGroupStringArray() {

		String[] groups = {

				"Birds", "Vascular Plants", "Soil Mites", "Lichens", "Bryophytes", "Aquatic Invertabrates","Mammals" };

		return groups;
	}

	public static OptionBuilder[] getDABackgroundColors() {

		OptionBuilder[] backgroundColors = { new OptionBuilder().value("salmon").label("salmon red"),
				new OptionBuilder().value("brown").label("brown"), new OptionBuilder().value("red").label("red"),
				new OptionBuilder().value("brown-grey").label("brown grey"),
				new OptionBuilder().value("cyan-blue").label("cyan blue"),
				new OptionBuilder().value("orange").label("orange"), new OptionBuilder().value("green").label("green"),
				new OptionBuilder().value("dark-blue").label("dark blue"),
				new OptionBuilder().value("cyan").label("cyan")

		};
		return backgroundColors;
	}
}
