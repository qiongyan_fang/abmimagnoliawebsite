package abmi.module.template.util;

import info.magnolia.jcr.util.PropertyUtil;

import java.util.Calendar;
import java.util.Comparator;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.UnsupportedRepositoryOperationException;

public class NodeSort implements Comparable<NodeSort> {

	Calendar calendar ;
    String path;

    public NodeSort(Node node) {
    	 calendar = PropertyUtil.getDate(node, "publishdate");
    	 try {
			path = node.getPath();
		} catch (UnsupportedRepositoryOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	public String getPath(){
		return path;
	}



    public static class Comparators {

        public static Comparator<NodeSort> TIME = new Comparator<NodeSort>() {
            @Override
            public int compare(NodeSort o1, NodeSort o2) {
                return o2.calendar.compareTo(o1.calendar); // sort from newest to oldest
            }
        };
       
    }


	@Override
	public int compareTo(NodeSort o) {
		 return Comparators.TIME.compare(this, o);
	}
}