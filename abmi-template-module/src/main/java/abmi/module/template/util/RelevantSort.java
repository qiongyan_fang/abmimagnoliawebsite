package abmi.module.template.util;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;


public class RelevantSort extends HashMap implements Comparable<RelevantSort> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Integer matchHit  ;
    

    public RelevantSort(Map<String, Object> node) {
    	try {
    	matchHit = Integer.parseInt(node.get("matchHit").toString());
    	}catch(Exception e) {
    		matchHit = 0;
    	}
    	
    }

	


    public static class Comparators {

        public static Comparator<RelevantSort> TIME = new Comparator<RelevantSort>() {
            @Override
            public int compare(RelevantSort o1, RelevantSort o2) {
                return o2.matchHit.compareTo(o1.matchHit); // sort from newest to oldest
            }
        };
       
    }


	@Override
	public int compareTo(RelevantSort o) {
		 return Comparators.TIME.compare(this, o);
	}
}