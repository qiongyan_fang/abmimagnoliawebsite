package abmi.module.template.component;


import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.model.util.ModuleParameters;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "All Jobs Box", id = "abmiModule:components/alljobsbox")
@TemplateDescription("All Jobs Box")
@FullWidthContent
public class JobOverviewComponent {
	@RequestMapping("/joboverviewlistbox")
	public String render(ModelMap model, Node content)
			throws RepositoryException {

		String path = content.getProperty("rootpath").getString();
		if (path == null || "".equals(path.trim())) {
			path = "/home/careers";
		}

		ArrayList<Map<String, String>> jobList = new ArrayList<Map<String, String>>();

		for (Node node : NodeUtil.getNodes(content.getSession().getNode(path),
				NodeTypes.Page.NAME)) {
			if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)

			) {
				LinkedHashMap<String, String> singleJob = new LinkedHashMap<String, String>();

				singleJob.put("position",
						PropertyUtil.getString(node, "position", ""));

				try {
					Calendar calendar = PropertyUtil.getDate(node, "deadline");

					String closedate = ModuleParameters.sdfull.format(calendar
							.getTime());

					singleJob.put("deadline", closedate);

					String status = "Open";
					if (new Date().getTime() > calendar.getTime().getTime()) {
						status = "Closed";
					}

					singleJob.put("status", status);
				} catch (Exception e) {

				}
				try {

					Calendar postcalendar = PropertyUtil.getDate(node,
							"postdate");

					String postdate = ModuleParameters.sdfull.format(postcalendar
							.getTime());

					singleJob.put("postdate", postdate);
				} catch (Exception err) {
				}

				singleJob.put("description", TemplateUtil.getCharByNumber(
						PropertyUtil.getString(node, "description", ""), 150));
				singleJob.put("applylink",
						PropertyUtil.getString(node, "applylink", ""));
				singleJob.put("jobpostupload",
						PropertyUtil.getString(node, "jobpostupload", ""));

				singleJob.put("path", node.getPath());

				jobList.add(singleJob);
			}
		}

		model.put("jobs", jobList);
		return "components/alljobs.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(

		cfg.fields.link("rootpath").label("Link Url").appName("pages")
				.targetWorkspace("website")

		);
	}
}
