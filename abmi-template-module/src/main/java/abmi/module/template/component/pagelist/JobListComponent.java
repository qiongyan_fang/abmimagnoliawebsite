package abmi.module.template.component.pagelist;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;


import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Job List Box", id = "abmiModule:components/joblist")
@TemplateDescription("Job List")
//@PageNavigation
public class JobListComponent {

	
	 @RequestMapping("/joblistbox")
	  public String render(ModelMap model, Node content) throws RepositoryException {
		 		
		 
		        String path = content.getProperty("rootpath").getString();
		        Map<String, String> jobList = new LinkedHashMap<String, String>();
		        for (Node node : NodeUtil.getNodes(content.getSession().getNode(path), NodeTypes.Page.NAME)) {
		            if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {

		            	jobList.put(node.getPath(), PropertyUtil.getString(node, "title", ""));
		            	if (content.getPath().startsWith(node.getPath())){
		            		model.put("currentpage", node.getPath());
		            	}
		             }
		        }
		        
		        model.put("jobpage", jobList);
		      
	        return "components/joblist.jsp";
	    }

	 @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
	    			 cfg.fields.link("rootpath").label("Link Url").appName("pages").targetWorkspace("website"),
	    			 cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
//	    			 ,
//	                 cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),
//	                 cfg.fields.text("headerline1").label("Top Header line Text"),
//	                 cfg.fields.text("headerline2").label("Middle Header line Text"),
//	                 cfg.fields.text("content").label("Content Text")
	         );
	    }
}
