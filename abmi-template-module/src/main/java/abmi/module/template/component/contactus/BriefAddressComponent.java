package abmi.module.template.component.contactus;



import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

@Template(title = "Centre Addresses", id = "abmiModule:components/multipleaddress")
@TemplateDescription("Centre Addresses")
//@MainContent
public class BriefAddressComponent {

	  @RequestMapping("/multipleaddress")
	    public String render(Node content, ModelMap model ) {
	    

			  return "components/address_multipleblock.jsp";
			  
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {

	    	
		 
	    	 tab.fields(
	    			 cfg.fields.text("centername").label("Center Name"),
	    			 cfg.fields.text("centernamenote").label("Center Name Notation").defaultValue("University of Alberta"),
	                 cfg.fields.text("mailingaddress").label("Complete Mailing Address"),
	                 cfg.fields.text("streetaddress").label("Complete Street Address"),
	                 cfg.fields.text("phone").label("Phone"),
	                 cfg.fields.text("fax").label("Fax"),
	                 cfg.fields.text("email").label("email")
	         );
	    }
}
