package abmi.module.template.component.rightsidewidget;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@RightWidgetComponent
@Controller
@Template(title = "Reference/Download Widget Area ", id = "abmiModule:components/downloadwidgetarea")

public class DownloadWidgetArea {

	  @RequestMapping("/downloadwidgetarea")
	    public String render() {
	    
	        return "components/downloadwidget_area.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                 cfg.fields.text("header").label("Header Text").defaultValue("Downloads"),
	                 cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
	         );
	    }
	    
	    
	    @Area("DownloadsArea")
	    @Controller
	    @AvailableComponentClasses({DownloadListComponent.class, ReferenceListComponent.class})
	    public static class SocialMediaArea {

	        @RequestMapping("/downloadwidgetarea/area")
	        public String render() {
	        	return "areas/generalarea.jsp";
	        }
	    }
	  
}
