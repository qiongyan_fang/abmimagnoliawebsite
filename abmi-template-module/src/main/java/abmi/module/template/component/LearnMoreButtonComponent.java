package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;



import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@Template(title = "Learn More Button", id = "abmiModule:components/learnmorebutton")
@TemplateDescription("Learn More Button")
@MainContent
public class LearnMoreButtonComponent {
	 @RequestMapping("/learnmorebutton")
	    public String render() {
	    
	        return "components/learnmorebutton.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	   	  
	    	tab.fields(
	                
	                 cfg.fields.link("url").label("Link Url").appName("pages").targetWorkspace("website"),
	                 cfg.fields.text("buttontext").label("Button Text").defaultValue("Learn More")
	                 
	                 
	         );
	    }
}
