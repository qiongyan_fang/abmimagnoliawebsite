package abmi.module.template.component.rightsidewidget;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RightWidgetComponent
@Template(title = "link to categories of reports", id = "abmiModule:components/detailedreportlink")
@TemplateDescription("link to categories of reports")

public class SelectedPublicationLinkComponent {

	  @RequestMapping("/detailedreportlink")
	    public String render() {
	    
	        return "components/rightwidget_selectedpublication.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
	    			 cfg.fields.link("imageUrl").label("image link").appName("assets").targetWorkspace("dam").description("Image icon").defaultValue(""),
	                 cfg.fields.link("url").label("page link").appName("pages").targetWorkspace("website").description(" use http:// for external document or copy path here").required(),
	                 cfg.fields.text("headerText").label("header text"),
	                 cfg.fields.text("text").label("description text").rows(3),
	                 cfg.fields.text("buttonText").label("Button text").defaultValue("View"),
	                 cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
	         );
	    }
}

