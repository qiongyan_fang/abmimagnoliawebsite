package abmi.module.template.component.tab;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;


@Controller
@Template(title = "JQuery Tab Detail", id = "abmiModule:components/JQuerytab")
@TemplateDescription("JQuery Tab Detail")

public class JQueryTabComponent {

    @RequestMapping("/JQueryTab")
    public String render() {
        return "components/JQuerytabcomponent.jsp";
    }


    
    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
        tab.fields(
        		cfg.fields.text("header").label("Tab Header").required(),
                cfg.fields.richText("text").label("Text body"),
                cfg.fields.link("imagepath").label("Image").appName("assets").targetWorkspace("dam"),
                cfg.fields.select("imagePosition").label("Image Position").options(TemplateUtil.getPostition().keySet())
                
        );
   
        
        
    }
}