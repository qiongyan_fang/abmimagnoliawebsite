package abmi.module.template.component.box;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

@Template(title = "Project (Image Button) Box", id = "abmiModule:components/projectintrobox")
@TemplateDescription("Spotlight Box")

public class ProjectImageBox {

	  @RequestMapping("/projectintrobox")
	    public String render(Node content, ModelMap model ) {
	    
//		  int posistion = TemplateUtil.getNodePosition(content, NodeTypes.Component.NAME, "abmiModule:components/projectintrobox");
//		  
//		  try{
//			  PropertyUtil.setProperty(content, "myindex", posistion+"");
//		  }
//		  catch(Exception e){}
		  
		  return "components/projectintrobox.jsp";
		  
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	
		 
	    	 tab.fields(
	    			 cfg.fields.link("imageUrl").label("Top Image").appName("assets").targetWorkspace("dam"),
	                 cfg.fields.link("logoUrl").label("Project logo Image").appName("assets").targetWorkspace("dam"),
	                 cfg.fields.text("header").label("Header line"),
	                 cfg.fields.text("description").label("Content Text").rows(5),
	                 cfg.fields.text("buttonText").label("button Text").defaultValue("VISIT WEBSITE"),
	                 cfg.fields.link("externalUrl").label("Website Link Url").appName("pages").targetWorkspace("website")
	                
	         );
	    }
}
