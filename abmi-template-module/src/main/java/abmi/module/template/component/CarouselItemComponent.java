package abmi.module.template.component;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

	@Controller
	@Template(id="abmiModule:components/carouselItem", title="Individual Carousel Item")
	public class CarouselItemComponent {

	 
	     @RequestMapping("/carousel/items")
	        public String render(ModelMap model, Node content) throws RepositoryException {
	    	 
	    	 content.setProperty("imageposition", PropertyUtil.getString(content.getParent(), "imageposition"));
	    	 content.setProperty("itemIndex", TemplateUtil.getNodePosition(content,  "abmiModule:components/carouselItem"));
	            return "components/carouselItem_component.jsp";
	        }
	 
	    
	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	    			// cfg.fields.text("id").label("An Unique Div Id (required only on first item)"),
	                 cfg.fields.link("url").label("internal or external Link Url").appName("pages").targetWorkspace("website"),
	                 cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),
	                 cfg.fields.text("headerline").label("Top Header line Text"),
	                 cfg.fields.text("date").label("Date"),
	                 cfg.fields.text("subheaderline").label("sub header"),
	                 cfg.fields.text("content").label("Content Text").rows(5)
	         );
	    }
}
