package abmi.module.template.component.pagelist;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;


import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Sub Page (with child pages)", id = "abmiModule:components/subpage2levellist")
@TemplateDescription("sub page List")
@PageNavigation
public class SubPageListTwoLevelComponent {

	@RequestMapping("/subpage2levellistbox")
	public String render(ModelMap model, Node content)
			throws RepositoryException {

		String rootPath = content.getProperty("rootpath").getString();
		String currentPath = MgnlContext.getAggregationState().getHandle();
		
		ArrayList<Map<String, Object>> subPageList = new ArrayList<Map<String, Object>>();
		boolean bIncludeRoot = true;
		try {
			bIncludeRoot = PropertyUtil.getBoolean(content, "includeroot",
					false);

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (bIncludeRoot) {
			Node rootNode = content.getSession().getNode(rootPath);
			String title = "";
			try {
				title = content.getProperty("roottext").getString();
			} catch (Exception e) {
				title = PropertyUtil.getString(rootNode, "title", "");
			}

			Map<String, Object> rowMap = new HashMap<String, Object>();
			rowMap.put("path", rootPath);
			
			rowMap.put("title", title);
			subPageList.add(rowMap);

		}
		
		String currentParentPath = "/";
		try{
			currentParentPath = MgnlContext.getAggregationState().getMainContentNode().getParent().getPath();
		}catch(Exception e){
			
		}
		

		if (false ) { // && isSubSubPage) { // only display subpages here blocked never do this
			
//			currentParentPath = MgnlContext.getAggregationState().getMainContentNode().getParent().getPath();
			
//			boolean isSubSubPage = false;
//			try{
//			isSubSubPage = MgnlContext.getAggregationState().getMainContentNode().getParent().getParent().getPath().equals(rootPath);
//			}catch(Exception e){
//				// when there is no parent parent keep it as false
//			}
//			System.out.println("currentpath" + currentPath + " parent ="
//					+ currentParentPath + " isSubSubPage?" + isSubSubPage);
			model.put("menu_header", PropertyUtil.getString(content.getSession().getNode(currentParentPath),"title"));
			Map<String, Object> rowMap = new HashMap<String, Object>();
			rowMap.put("path", currentParentPath);
			rowMap.put("title", "All");
			subPageList.add(rowMap);
			for (Node node : NodeUtil.getNodes(
					content.getSession().getNode(currentParentPath),
					NodeTypes.Page.NAME)) {
				if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {
					Map<String, Object> rowMap1 = new HashMap<String, Object>();
					rowMap1.put("path", node.getPath());
					rowMap1.put("title",
							PropertyUtil.getString(node, "title", ""));
					subPageList.add(rowMap1);
				}
			}


		} else {
			model.put("menu_header", PropertyUtil.getString(content, "title", PropertyUtil.getString(content.getSession().getNode(rootPath),"title")));
			for (Node node : NodeUtil.getNodes(
					content.getSession().getNode(rootPath),
					NodeTypes.Page.NAME)) {
				if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {

					if (node.getPath().equals(currentPath)  // if level 1 path is current path, display its childrens
							|| node.getPath().equals(currentParentPath) //or if level 2 path's parent node is

					) { // first if it is current page (or it is current pages'
						// parent page) , or it is the current page's parent
						Map<String, Object> rowMap = new HashMap<String, Object>();
						rowMap.put("path", node.getPath());
						rowMap.put("title",
								PropertyUtil.getString(node, "title", ""));

						ArrayList<Map<String, Object>> subsubPageList = new ArrayList<Map<String, Object>>();

						for (Node childNode : NodeUtil.getNodes(node,
								NodeTypes.Page.NAME)) {// check if it has any
														// childrens, if so add
														// childrens node
							if (!PropertyUtil.getBoolean(node,
									"hideInNavigation", false)) {
								Map<String, Object> subsubPageRow = new HashMap<String, Object>();
								subsubPageRow.put("path", childNode.getPath());
								subsubPageRow.put("title", PropertyUtil
										.getString(childNode, "title", ""));
								subsubPageList.add(subsubPageRow);
							}
						}

						if (subsubPageList.size() > 0) {

							rowMap.put("childrenpage", subsubPageList);
						}

						subPageList.add(rowMap);

					} else {

						Map<String, Object> rowMap = new HashMap<String, Object>();
						rowMap.put("path", node.getPath());
						rowMap.put("title",
								PropertyUtil.getString(node, "title", ""));
						subPageList.add(rowMap);
					}

				}
			}
		}

		model.put("subpage", subPageList);
		model.put("currentParentPath", currentParentPath);


		return "components/subpage2levellist.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(

				cfg.fields.link("rootpath").label("Link Url").appName("pages")
						.targetWorkspace("website"),
				cfg.fields.text("header").label("Header Text")
						.defaultValue("All Publications"),
				cfg.fields.checkbox("includeroot")
						.buttonLabel("include the root page in the navigation")
						.label("Include the root page?"),
				cfg.fields.text("roottext").label(
						"Text of the root page (default:title)"),

				cfg.fields.checkbox("inheritable").buttonLabel("")
						.label("Inheritable").description("Show in Subpages?")

		// ,
		// cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),
		// cfg.fields.text("headerline1").label("Top Header line Text"),
		// cfg.fields.text("headerline2").label("Middle Header line Text"),
		// cfg.fields.text("content").label("Content Text")
		);
	}
}
