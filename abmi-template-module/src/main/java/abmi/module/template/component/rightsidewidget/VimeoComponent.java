/**
 * This file Copyright (c) 2013 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This file is dual-licensed under both the Magnolia
 * Network Agreement and the GNU General Public License.
 * You may elect to use one or the other of these licenses.
 *
 * This file is distributed in the hope that it will be
 * useful, but AS-IS and WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE, or NONINFRINGEMENT.
 * Redistribution, except as permitted by whichever of the GPL
 * or MNA you select, is prohibited.
 *
 * 1. For the GPL license (GPL), you can redistribute and/or
 * modify this file under the terms of the GNU General
 * Public License, Version 3, as published by the Free Software
 * Foundation.  You should have received a copy of the GNU
 * General Public License, Version 3 along with this program;
 * if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * 2. For the Magnolia Network Agreement (MNA), this file
 * and the accompanying materials are made available under the
 * terms of the MNA which accompanies this distribution, and
 * is available at http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package abmi.module.template.component.rightsidewidget;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.MainContent;

/**
 * Component hosting youtube videos.
 */
@Controller
@Template(title="Vimeo video", id="abmiModule:components/vimeo")
@TemplateDescription("Embeds a Vimeo page")
@RightWidgetComponent
@MainContent
public class VimeoComponent {

    @RequestMapping("/vimeo")
    public String render(Node node, ModelMap model) throws RepositoryException {
        model.put("videoId", node.getProperty("videoId").getString());
        return "components/vimeocomponent.jsp";
    }

    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
        tab.fields(
                cfg.fields.text("videoId").label("Video id").description("Vimeo Video Id"),
//                cfg.fields.text("videolabel").label("Video label (any)").description("any name used for html to pop up a dialog box").required(true),
                cfg.fields.link("imageurl").label("Image url (optional)").appName("assets").targetWorkspace("dam"),
                
                cfg.fields.text("videotext").label("Video Title"),
                cfg.fields.checkbox("useimagesize").label("use image size").buttonLabel("").description("check to show actual image size, uncheck to resize image to outside Div size"),
                cfg.fields.checkbox("showgreenbox").label("text in green box").buttonLabel("").description("show text in green box, otherwise, it shows as a text line"),
//                cfg.fields.select("boxwidth").options(TemplateUtil.getBoxWidth()).label("Boxes width").defaultValue("narow").description("narrow on show on right side"),
                cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
        );
    }
}
