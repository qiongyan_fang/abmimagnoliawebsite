package abmi.module.template.component.glossary;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.rightsidewidget.RightWidgetComponent;


@Controller
@Template(title = "Glossary Area", id = "abmiModule:components/glossaryareacite")
@TemplateDescription("Glossary Area")
@RightWidgetComponent
public class GlossaryCitationAreaComponent {

	@Area("GlossaryArea")
	@Controller
	@AvailableComponentClasses({GlossaryCitationComponent.class, GlossaryCitationFromAppsComponent.class})
	public static class LeftArea {

		@RequestMapping("/glossaryareacite/component")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
	  @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	    	tab.fields(
	    		     cfg.fields.text("header").label("Header Text").defaultValue("Glossary"),
	    		     cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
	                
	         );
	    }
	

	@RequestMapping("/glossaryareacite")
	public String render() {
		return "components/glossaryareacitationcomponent.jsp";
	}
}
