package abmi.module.template.component.contactus;



import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.FullWidthContent;
import abmi.module.template.component.MainContent;

@Controller

@Template(title = "Staff Contact Area", id = "abmiModule:components/staffcontactareageneral")
@TemplateDescription("Staff Contact")
@FullWidthContent
@MainContent
public class StaffDirectoryGenearlAreaComponent {

	  @RequestMapping("/staffcontactareageneral")
	    public String render(Node content, ModelMap model ) {
	    

			  return "components/address_staffcontactgeneralarea.jsp";
			  
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	    			
	                 cfg.fields.text("header").label("Header")
	         );
	    }
	    
	    @Area(value="centerarea")
		@Controller
		@AvailableComponentClasses({StaffDirectoryAreaComponent.class
			})
		public static class StaffArea {

			@RequestMapping("/staffcontactgeneralarea/staffcenter")
			public String render() {
				 return "areas/generalarea.jsp";
			}
		}
}
