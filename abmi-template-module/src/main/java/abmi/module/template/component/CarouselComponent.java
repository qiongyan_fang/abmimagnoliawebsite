package abmi.module.template.component;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

	@Controller
	@Template(id="abmiModule:components/carousel", title="General Carousel")
	@TemplateDescription("General Carousel")
	@HightLightBoxComponent
	public class CarouselComponent {

	    /**
	     * Left column.
	     */
	    @Area("carousel_area")
	    @Controller
	    @AvailableComponentClasses({CarouselItemComponent.class})
	    public static class CarouselArea {

	        @RequestMapping("/carousel/area")
	        public String render() {
	            return "areas/carousel_area.jsp";
	        }
	        
	        @TabFactory("Content")
		    public void contentTab(UiConfig cfg, TabBuilder tab) {
			 Map<String, String> positionCategories = new LinkedHashMap<String, String>();
			 positionCategories.put("left", "left");
			 positionCategories.put("right", "right");
	   	  
		    	 tab.fields(
//		    			 cfg.fields.text("id").label("an Unique Id").defaultValue("carousel-news").required(true),
		    			 cfg.fields.select("imageposition").label("News Image Position").
		    			 options(positionCategories.keySet()).description("Image Position Left or Right")
		    			 );
		    }
	    }
	    
	    @RequestMapping("/carousel_component")
	    public String render(ModelMap model, Node content) throws RepositoryException {
	    
	     
	    
	        return "components/carousel_component.jsp";
	    }

	    
	   
	}

