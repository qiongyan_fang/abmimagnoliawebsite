package abmi.module.template.component.contactus;


import java.util.ArrayList;
import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.FullWidthContent;

@Controller

@Template(title = "Detailed Address Block", id = "abmiModule:components/detailedaddressblock")
@TemplateDescription("Detailed Address Block")
@FullWidthContent
public class DetailAddressComponent {

	  @RequestMapping("/detailedaddressblock")
	    public String render(Node content, ModelMap model ) {
	    

			  return "components/address_detailedblock.jsp";
			  
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	ArrayList<String> addressTypeList = new ArrayList<String>();
	    	addressTypeList.add("Office Address");
	    	addressTypeList.add("Mailing Address");
	    	addressTypeList.add("Street Address");	
	    	
		 
	    	 tab.fields(
	    			 cfg.fields.select("type").options(addressTypeList).label("Address Type").defaultValue("Office Address").readOnly(false),
	    			 cfg.fields.text("addressname").label("Address Name").defaultValue("ABMI Headerquarters"),
	                   cfg.fields.text("function").label("Address for (general inquires or ..)").defaultValue("General Inquiries"),
	                 cfg.fields.text("address").label("Street (using < br > for line breaks)"),
	                 cfg.fields.text("city").label("City").defaultValue("Edmonton"),
	                 cfg.fields.text("province").label("Province").defaultValue("AB"),
	                 cfg.fields.text("country").label("Country").defaultValue("Canada"),
	                 cfg.fields.text("postcode").label("Postcode"),
	                 cfg.fields.text("phone").label("Phone"),
	                 cfg.fields.text("fax").label("Fax"),
	                 cfg.fields.text("email").label("Email"),
	                 cfg.fields.text("lat").label("Latitude"),
	                 cfg.fields.text("longitude").label("Longitude")
	                 
	         );
	    }
}
