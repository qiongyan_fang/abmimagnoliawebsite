package abmi.module.template.component.rightsidewidget;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller

@Template(title = "Download Link", id = "abmiModule:components/downloadlink")
@TemplateDescription("Link")

public class DownloadListComponent {

	  @RequestMapping("/downloadlink")
	    public String render() {
	    
	        return "components/downloadlink.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
	                 
	                 cfg.fields.link("url").label("document link").appName("assets").targetWorkspace("dam"),
	                 cfg.fields.text("text").label("link text"),
	                 cfg.fields.text("externalurl").label("Document Url").description(" use http:// for external document"),
	               
	                 cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
	         );
	    }
}
