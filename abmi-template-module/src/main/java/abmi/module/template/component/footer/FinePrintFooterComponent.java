package abmi.module.template.component.footer;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;


import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;
import org.springframework.ui.ModelMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * component for add footer (text and links)
 * @author Qiongyan
 *
 */
@Controller
@Template(title = "Fine Print Footer", id = "abmiModule:components/fineprintfooter")
@TemplateDescription("Fine Print Footer")

public class FinePrintFooterComponent {
	  @RequestMapping("/fineprintfooter")
	    public String render(Node content, ModelMap model) {
		   String path = PropertyUtil.getString(content, "rootpath", "/home/footer");
	        List<Map<String, String>> subPageList = new ArrayList<Map<String, String>>();
	       	   try {
				for (Node node : NodeUtil.getNodes(content.getSession().getNode(path), NodeTypes.Page.NAME)) {
				    if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {

				    	LinkedHashMap<String, String> row = new LinkedHashMap<String, String> ();
				    	row.put("path", node.getPath());
				    	row.put("title", PropertyUtil.getString(node, "title", ""));	
				    	subPageList.add(row);
				     }
				}
			} catch (PathNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        model.put("menu", subPageList);
	        
	        return "components/fineprintfootercomponent.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	        tab.fields(
	               cfg.fields.text("headertext").label("").defaultValue("Alberta Biodiversity Monitoring Institute &copy;2014&nbsp;&nbsp;All Rights Reserved&nbsp;&nbsp;"),
	               cfg.fields.link("rootpath").label("Link Url").appName("pages").targetWorkspace("website").defaultValue("/home/footer")
	        		
	        );
	    }
}
