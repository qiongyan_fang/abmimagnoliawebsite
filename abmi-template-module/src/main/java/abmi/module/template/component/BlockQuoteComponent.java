package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;



	@Controller
	@Template(title = "Block Quote", id = "abmiModule:components/blockquote")
	@TemplateDescription("Block Quote")
	@MainContent
	public class BlockQuoteComponent {

	    @RequestMapping("/blockquote")
	    public String render() {
	        return "components/blockquote.jsp";
	    }


	    
	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	        tab.fields(
	        		

	                cfg.fields.richText("text").label("Text body")
	                
	        );
	   
	        
	        
	    }
	}

