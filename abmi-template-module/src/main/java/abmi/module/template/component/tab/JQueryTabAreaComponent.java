package abmi.module.template.component.tab;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.MainContent;

@Controller
@Template(title = "JQueryTab  Area", id = "abmiModule:components/JQueryTabarea")
@TemplateDescription("JQueryTab  Area")
@MainContent
public class JQueryTabAreaComponent {

	@Area(value="JQueryTabArea")
	@Controller
	@AvailableComponentClasses(JQueryTabComponent.class)
	 
	public static class FooterArea {
		@RequestMapping("/JQueryTabarea/component")
		public String render() {
			return "areas/JQuerytabarea.jsp";
		}
	}

	
	@RequestMapping("/JQueryTabArea")
	public String render() {
		return "components/JQuerytabareacomponent.jsp";
	}
}
