
package abmi.module.template.component.contactus;



import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.FullWidthContent;
import abmi.module.template.component.MainContent;

@Controller

@Template(title = "Question? Please Contact", id = "abmiModule:components/questioncontact")
@TemplateDescription("Contact Component")
@MainContent
@FullWidthContent

public class QuestionContactAreaComponent {

	  @RequestMapping("/questioncontact")
	    public String render(Node content, ModelMap model ) {
	    

			  return "components/address_questioncontactarea.jsp";
			  
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	    			 cfg.fields.text("headertext").label("Header Text").defaultValue("More questions? Feel free to contact:")
//	    			 ,
//	                 cfg.fields.text("staffname").label("Staff Name"),
//	                 cfg.fields.text("stafftitle").label("Staff Title"),
//	                 cfg.fields.text("phone").label("Phone"),
//	                 cfg.fields.text("cell").label("Cell Phone"),
//	                 cfg.fields.text("email").label("email")
	         );
	    }
	    
	    @Area("contact_area")
		@Controller
		@AvailableComponentClasses({StaffDirectoryComponent.class, QuestionContactUsComponent.class})
		public static class LeftArea {

			@RequestMapping("/questioncontact/area")
			public String render() {
				return "areas/generalarea.jsp";
			}
		}

}
