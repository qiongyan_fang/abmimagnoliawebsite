package abmi.module.template.component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;


import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@MainContent
@Controller
@Template(id="abmiModule:components/teammember", title="list of all team members")
public class TeamMemberComponent {
	
	private static final Logger log = LoggerFactory
			.getLogger(TeamMemberComponent.class);
	
	  @RequestMapping("/teammembercomponent")
	    public String render(Node content, ModelMap model) throws RepositoryException {
	    	
			
	        String path = null;
	        
	        try{
	        	path = content.getProperty("rootpath").getString();
	        }catch(Exception e){
	        	log.info("can't get rootpath from DB use default one. " + e.getMessage());
	        	path = "/home/aboutus/boardmembers";
	        }
	        
	        if (path==null || "".equals(path.trim())){
	        	path = "/home/aboutus/boardmembers";
	        }
	        
	       
	      
	        ArrayList< Map<String, String>> memberList = new   ArrayList<Map<String, String>>();
	      
	        for (Node node : NodeUtil.getNodes(content.getSession().getNode(path), NodeTypes.Page.NAME)) {
	            if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)
	            		
	            		) {
	            	LinkedHashMap<String, String> singleMember = new LinkedHashMap<String, String>();
	            	
	            	singleMember.put("photo", PropertyUtil.getString(node, "photo", ""));
	            	singleMember.put("personname", PropertyUtil.getString(node, "personname", PropertyUtil.getString(node, "title", "")));
	            	singleMember.put("position", PropertyUtil.getString(node, "position", ""));
	            	singleMember.put("otherposition", PropertyUtil.getString(node, "otherposition", ""));   
	            	singleMember.put("path", node.getPath());
	            	
	            	memberList.add(singleMember);
	            }
	        }
	        
	        model.put("members", memberList);
	    	
	        return "components/allteammember.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	        tab.fields(
//	                cfg.fields.text("title").label("Title"),
	                cfg.fields.link("rootpath").label("Link Url").appName("pages").targetWorkspace("website")
	                //cfg.fields.checkbox("hideInNavigation").label("Hide in navigation").description("Check this box to hide this page in navigation")
	        );
	    }
	    
}
