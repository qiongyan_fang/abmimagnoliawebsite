package abmi.module.template.component;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "JumboTron (Image Banner)", id = "abmiModule:components/jumbotron")
@TemplateDescription("Big JumboTron video/image")

public class JumbotronComponent {

	  @RequestMapping("/jumbotron")
	    public String render(Node content) {
//		  boolean bMainPage = false;
//		  try{
//			  bMainPage = content.getProperty("ismainpage").getBoolean();
//		  }
//		  catch(Exception e){}
//		  
//		  if (bMainPage)
//	        return "components/jumbotron.jsp";
//		  else
			
			  return "components/smalljumbotron.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
	    			    cfg.fields.link("image").label("Image").appName("assets").targetWorkspace("dam"),
	    			 /*   cfg.fields.link("videomp4").label("Video Mp4").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.link("videoogg").label("Video OGG").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.link("videowebm").label("Video WBM").appName("assets").targetWorkspace("dam"),*/
	    			    cfg.fields.text("header").label("Overlay Header"),
	    			    cfg.fields.text("subheader").label("Overlay Sub-Header"),
//	    			    cfg.fields.checkbox("ismainpage").buttonLabel("on home page?").label("Main Page?"),
	    			    cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
	         );
	    }
}
