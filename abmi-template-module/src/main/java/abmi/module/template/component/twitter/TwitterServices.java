package abmi.module.template.component.twitter;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterServices {
	private static final Logger log = LoggerFactory
			.getLogger(TwitterServices.class);
	static Status getTimeline() {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = TwitterServices.class.getClassLoader().getResourceAsStream("twitter4j.properties");
				// load a properties file
			prop.load(input);

			// get the property value and print it out
			String key = prop.getProperty("oauth.consumerKey");
			String pass = prop.getProperty("oauth.consumerSecret");

			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(key);
			builder.setOAuthConsumerSecret(pass);
			builder.setOAuthAccessToken(prop.getProperty("oauth.accessToken"));
			builder.setOAuthAccessTokenSecret(prop.getProperty("oauth.accessTokenSecret"));
			
			Configuration configuration = builder.build();
			TwitterFactory factory = new TwitterFactory(configuration);
			Twitter twitter = factory.getInstance();

			// Twitter twitter = new TwitterFactory().getInstance();


			Paging page = new Paging();
			page.setCount(20);
			List<Status> statuses = twitter.getUserTimeline(page); //getHomeTimeline();
		

			return statuses.get(0);
		} catch (TwitterException te) {
			te.printStackTrace();
			log.error("Failed to get timeline: {} ", te.getMessage());

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	/*
	static void getToken() {

		try {
			Twitter twitter = new TwitterFactory().getInstance();
			try {
				// get request token.
				// this will throw IllegalStateException if access token is
				// already available
				RequestToken requestToken = twitter.getOAuthRequestToken();
//				System.out.println("Got request token.");
//				System.out.println("Request token: " + requestToken.getToken());
//				System.out.println("Request token secret: "
//						+ requestToken.getTokenSecret());
				AccessToken accessToken = null;
				BufferedReader br = new BufferedReader(new InputStreamReader(
						System.in));
				while (null == accessToken) {
					System.out.println("Open the following URL and grant access to your account:");
					System.out.println(requestToken.getAuthorizationURL());
					System.out.println("Enter the PIN(if available) and hit enter after you granted access.[PIN]:");
					String pin = br.readLine();
					try {
						if (pin.length() > 0) {
							accessToken = twitter.getOAuthAccessToken(
									requestToken, pin);
						} else {
							accessToken = twitter
									.getOAuthAccessToken(requestToken);
						}
					} catch (TwitterException te) {
						if (401 == te.getStatusCode()) {
							log.error("Unable to get the access token.");
						} else {
							te.printStackTrace();
						}
					}
				}
//				System.out.println("Got access token.");
//				System.out.println("Access token: " + accessToken.getToken());
//				System.out.println("Access token secret: "
//						+ accessToken.getTokenSecret());
			} catch (IllegalStateException ie) {
				// access token is already available, or consumer key/secret is
				// not set.
				if (!twitter.getAuthorization().isEnabled()) {
					log.error("OAuth consumer key/secret is not set.");
					System.exit(-1);
				}
			}
			Status status = twitter.updateStatus("Unknown");
			log.info("Successfully updated the status to ["
					+ status.getText() + "].");
			System.exit(0);
		} catch (TwitterException te) {
			te.printStackTrace();
			log.error("Failed to get timeline: " + te.getMessage());
			System.exit(-1);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			log.error("Failed to read the system input.");
			System.exit(-1);
		}
	}
	*/

}
