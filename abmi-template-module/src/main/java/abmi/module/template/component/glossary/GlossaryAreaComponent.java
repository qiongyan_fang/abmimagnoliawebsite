package abmi.module.template.component.glossary;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.MainContent;



@Controller
@Template(title = "Glossary Item Area", id = "abmiModule:components/glossaryarea")
@TemplateDescription("Glossary Area")
@MainContent
public class GlossaryAreaComponent {

	@Area("GlossaryArea")
	@Controller
	@AvailableComponentClasses(GlossaryItemComponent.class)
	public static class LeftArea {

		@RequestMapping("/glossaryarea/component")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
	  @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	    	tab.fields(
	    		     cfg.fields.text("name").label("Index Letter")
//	                 cfg.fields.text("indexletter").label("Index Letter").description("Leading Letter of Glossaries (A, B, C)")
	                
	         );
	    }
	

	@RequestMapping("/glossaryarea")
	public String render() {
		return "components/glossaryareacomponent.jsp";
	}
}
