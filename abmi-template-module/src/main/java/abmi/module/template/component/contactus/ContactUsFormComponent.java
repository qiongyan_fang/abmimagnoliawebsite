package abmi.module.template.component.contactus;

import java.util.ArrayList;

import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.model.util.Mail;
import abmi.module.template.component.MainContent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Contact Form", id = "abmiModule:components/contactForm")
@TemplateDescription("A contact form where visitors can get in contact with ABMI by filling in a form")
@MainContent
public class ContactUsFormComponent {
	
	
	@RequestMapping("/contact")
	public String render(@ModelAttribute ContactForm contactForm,
			BindingResult result, Node content,ModelMap model, HttpServletRequest request) {

		if ("POST".equals(request.getMethod())) {
			
			new ContactFormValidator().validate(contactForm, result);
			if (result.hasErrors()) {
				return "components/contactForm.jsp";
			}

			try {
				
						
				Mail.sendMail(contactForm.getReceiverEmail(), contactForm.getSenderEmail(), contactForm.getSenderName(), 
						contactForm.getDepartment() + "--" + contactForm.getSubject(), contactForm.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "components/contactFormSubmitted.jsp?error="+e.getMessage();
			} 
				
			
			return "components/contactFormSubmitted.jsp";
		}
		
		contactForm.setReceiverEmail(PropertyUtil.getString(content, "sendto", "abmiinfo@ualberta.ca"));
		contactForm.setDepartment(PropertyUtil.getString(content, "department", "NONE"));
		model.addAttribute("contactForm", contactForm);
		model.addAttribute("departmentList", TemplateUtil.getDepartmentOptions());
		return "components/contactForm.jsp";
	}

	
	   @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	ArrayList<String> addressTypeList = new ArrayList<String>();
	    	addressTypeList.add("Office Address");
	    	addressTypeList.add("Mailing Address");	
	    	
		 
	    	 tab.fields(
	    			 
	                 cfg.fields.text("sendto").label("Send to Email Address").defaultValue("abmiinfo@ualberta.ca"),
	                 cfg.fields.text("department").label("Default Department").defaultValue("NONE"),
	                 cfg.fields.richText("message").label("Message after sumbisson")
	                
	         );
	    }

}
