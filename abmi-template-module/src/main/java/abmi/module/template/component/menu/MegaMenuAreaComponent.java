package abmi.module.template.component.menu;


import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Mega Menu Area", id = "abmiModule:components/megamenuarea")
public class MegaMenuAreaComponent{
	@Area("SubMenuArea")
	@Controller
	@AvailableComponentClasses(SubMenuComponent.class)
	public static class LeftArea {

		@RequestMapping("/megamenurea/singlemenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@RequestMapping("/megamenurea")
	public String render() {
		return "components/megamenuarea.jsp";
	}
	
	
	  @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
//	    			    cfg.fields.link("image").label("Image").appName("assets").targetWorkspace("dam"),
	    			 /*   cfg.fields.link("videomp4").label("Video Mp4").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.link("videoogg").label("Video OGG").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.link("videowebm").label("Video WBM").appName("assets").targetWorkspace("dam"),*/
	    				cfg.fields.text("menutitle").label("Description"),
	    				cfg.fields.text("menuwidth").label("Width in pixels"),
	    				cfg.fields.text("menushift").label("Shift dropdown to left in pixels").defaultValue("300"),
	    				
	    				cfg.fields.link("imageurl").label("Image").appName("assets").targetWorkspace("dam")
	    			
								
					
	    			 
	    			   
	         );
	    }
}
