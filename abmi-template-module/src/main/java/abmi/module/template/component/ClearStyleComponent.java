package abmi.module.template.component;

import java.util.ArrayList;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.rightsidewidget.RightWidgetComponent;


@Controller
@Template(title = "Different Headers / Clear Style", id = "abmiModule:components/headercomponent")
@TemplateDescription("Headers / Clear Style")
@MainContent
@RightWidgetComponent
@TextContent
public class ClearStyleComponent<FckEditorDialog> {

    @RequestMapping("/headerclear")
    public String render() {
        return "components/headers.jsp";
    }


    
    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
    	ArrayList<String> boxSizeList = new ArrayList<String>();
    	
    	boxSizeList.add("h1");
    	boxSizeList.add("h2");
    	boxSizeList.add("h3");
    	boxSizeList.add("h4");
    	boxSizeList.add("h5");
    	boxSizeList.add("h6");
    	boxSizeList.add("header 4");
    	boxSizeList.add("header 5");    	
    	boxSizeList.add("start at new line");
    	boxSizeList.add("horizontal line");
    	
    	ArrayList<String> colorList = new ArrayList<String>();
    	colorList.add("");
    	colorList.add("default");
    	colorList.add("blue");
    	
        tab.fields(
        		
//                cfg.fields.text("heading").label("Heading"),
                cfg.fields.text("text").label("header text"),
                cfg.fields.select("type").label("Type").options(boxSizeList),
                cfg.fields.select("color").label("Color - naturelynx").options(colorList).defaultValue("default"),
                cfg.fields.text("padding").label("padding left pixel").defaultValue("0"),
                cfg.fields.text("paddingtop").label("top spacing in pixel").defaultValue("0"),
                cfg.fields.text("paddingbottom").label("bottom spacing pixel").defaultValue("0")
                
        );
   
        
        
    }
}