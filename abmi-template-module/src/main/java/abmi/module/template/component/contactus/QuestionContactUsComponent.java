

package abmi.module.template.component.contactus;



import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

@Template(title = "Detailed Contact", id = "abmiModule:components/staffcontactdetail")
@TemplateDescription("Detailed Contact")

public class QuestionContactUsComponent {

	  @RequestMapping("/staffcontactdetail")
	    public String render(Node content, ModelMap model ) {
	    

			  return "components/address_questioncontact.jsp";
			  
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	    			
	                 cfg.fields.text("staffname").label("Staff Name"),
	                 cfg.fields.text("stafftitle").label("Staff Title"),
	                 cfg.fields.text("address").label("Address"),
	                 cfg.fields.text("phone").label("Phone"),
	                 cfg.fields.text("cell").label("Cell Phone"),
	                 cfg.fields.text("email").label("email")
	         );
	    }
}
