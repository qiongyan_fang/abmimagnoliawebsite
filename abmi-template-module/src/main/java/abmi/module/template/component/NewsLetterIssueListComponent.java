package abmi.module.template.component;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@Template(title = "Past Newsletter Issues", id = "abmiModule:components/newsletterComponent")
@TemplateDescription("Past Newsletter Issues Component")
@MainContent
public class NewsLetterIssueListComponent {

	
	 @RequestMapping("/newsletterComponent")
	 public String render(ModelMap model, Node content) throws RepositoryException {
		        return "components/newsletterissues.jsp";
	    }

	
	   
}
