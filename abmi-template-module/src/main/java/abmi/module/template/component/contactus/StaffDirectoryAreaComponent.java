package abmi.module.template.component.contactus;



import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

@Template(title = "Center  Area", id = "abmiModule:components/staffcontactarea")
@TemplateDescription("Center Area")
//@FullWidthContent
//@MainContent
public class StaffDirectoryAreaComponent {

	  @RequestMapping("/staffcontactarea")
	    public String render(Node content, ModelMap model ) {
	    

			  return "components/address_staffcontactarea.jsp";
			  
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	    			
	                 cfg.fields.text("centername").label("Center Name"),
	                 cfg.fields.text("minheight").label("Max Rows").description("the maximum rows an address block has.")
	         );
	    }
	    
	    @Area(value="staffarea")
		@Controller
		@AvailableComponentClasses({StaffDirectoryComponent.class
			})
		public static class StaffArea {

			@RequestMapping("/staffcontactarea/staffcenter")
			public String render() {
				 return "areas/generalarea.jsp";
			}
		}
}
