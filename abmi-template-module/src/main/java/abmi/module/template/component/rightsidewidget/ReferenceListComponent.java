package abmi.module.template.component.rightsidewidget;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller

@Template(title = "Reference Link", id = "abmiModule:components/referencelink")
@TemplateDescription("Link")

public class ReferenceListComponent {

	  @RequestMapping("/referencelink")
	    public String render() {
	    
	        return "components/download_referencelink.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
	                 
	                 cfg.fields.link("url").label("page link").appName("pages").targetWorkspace("website").description(" use http:// for external document").required(),
	                 cfg.fields.text("text").label("link text").required(),
	                 cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
	         );
	    }
}
