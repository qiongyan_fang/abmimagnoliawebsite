package abmi.module.template.component.menu;



import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Sub Menu Area", id = "abmiModule:components/submenurea")


public class SubMenuAreaComponent{
	@Area("menuBlockArea")
	@Controller
	@AvailableComponentClasses(SubMenuComponent.class)
	public static class LeftArea {

		@RequestMapping("/submenurea/submenurea")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@RequestMapping("/submenurea")
	public String render() {
		return "components/megamenusubarea.jsp";
	}
	
	
}
