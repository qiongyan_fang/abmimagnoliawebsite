package abmi.module.template.component;


import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Event Box", id = "abmiModule:components/eventmorebox")
@TemplateDescription("Event Box")
@HightLightBoxComponent
public class EventBoxComponent {
	 @RequestMapping("/eventbox")
	    public String render() {
	    
	        return "components/eventbox.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	    	tab.fields(
	    		     cfg.fields.link("url").label("Link Url").appName("pages").targetWorkspace("website"),
	                 cfg.fields.text("headerline").label("Top Header line Text"),
	                 cfg.fields.text("date").label("Event Date"),
	                 cfg.fields.text("location").label("Event Location"),
	                 cfg.fields.text("content").label("Content Text").rows(5)
	         );
	    }
}
