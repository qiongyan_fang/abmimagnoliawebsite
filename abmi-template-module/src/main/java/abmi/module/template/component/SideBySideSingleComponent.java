package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Inside Area", id = "abmiModule:components/sidebysidesinglediv")

public class SideBySideSingleComponent {

	@Area("sidebysideareadiv")
	@Controller
	@AvailableComponentClasses(MainContent.class)
	public static class LeftArea {

		@RequestMapping("/sidebysidesinglediv/singlediv")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@RequestMapping("/sidebysidesinglediv")
	public String render() {
		return "components/sidebysideareasinglecomponent.jsp";
	}
	
	

}