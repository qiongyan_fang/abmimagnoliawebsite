package abmi.module.template.component.box;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.FullWidthContent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Resource Download Box", id = "abmiModule:components/resourcedownloadbox")
@TemplateDescription("Resource Download Box at bottom row only")
@FullWidthContent
public class ResourceViewDownloadBox {

	@RequestMapping("/resourcedownloadbox")
	public String render(Node content, ModelMap model) {

		// int posistion = TemplateUtil.getNodePosition(content,
		// NodeTypes.Component.NAME, "abmiModule:components/projectintrobox");
		//
		// try{
		// PropertyUtil.setProperty(content, "myindex", posistion+"");
		// }
		// catch(Exception e){}

		return "components/resourcedownloadbox.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
			
				cfg.fields.link("imageUrl").label("Top Image")
						.appName("assets").targetWorkspace("dam"),
				cfg.fields
						.text("header").label("Header line"),
				cfg.fields.text("description").label("Content Text").rows(5),
				cfg.fields.link("viewUrl").label("view Url (optional)")
								.appName("assets").targetWorkspace("dam"),
				cfg.fields.link("downloadUrl").label("download Link Url (optional)")
						.appName("assets").targetWorkspace("dam"),
				cfg.fields.select("backgroundcolor").label("Background Color").options(TemplateUtil.getBaseColor()));
	}
}
