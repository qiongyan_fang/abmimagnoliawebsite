package abmi.module.template.component.twitter;

import javax.jcr.Node;

import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import twitter4j.Status;



import abmi.model.util.CommonUtil;
import abmi.module.template.component.HightLightBoxComponent;

@HightLightBoxComponent
@Controller
@Template(title = "Twitter", id = "abmiModule:components/twitter")
@TemplateDescription("Simple Twitter block")
public class TwitterComponent {
	private static final Logger log = LoggerFactory
			.getLogger(TwitterComponent.class);

	@RequestMapping("/twitterBlock")
	public String render(ModelMap model, Node content) {
		try {
			MgnlContext.getWebContext().getResponse()
					.setHeader("Cache-Control", "no-cache");
		} catch (Exception e) {
			log.error("Twitter component disable cache failed:"
					+ e.getMessage());
		}
		Status lastStatus = TwitterServices.getTimeline();

		String twitterText = "";
		String url = "";
		try {
			twitterText = CommonUtil.nvl(lastStatus.getText());
			if (lastStatus.getURLEntities().length > 0)
				url = (lastStatus.getURLEntities())[0].getURL();
		} catch (Exception e) {
			log.error("error getting twitter text {}", e.getMessage());
		}

		model.put("twitterItem", twitterText);
		model.put("twitterUrl", url);

		return "components/twitterblock.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(cfg.fields.link("twittericon").label("Twitter Icon")
				.appName("assets").targetWorkspace("dam"),
				cfg.fields.text("twittername").label("Twitter Name")
						.defaultValue("@ABBiodiversity"),
				cfg.fields.richText("body").label("Twitter Manually Entered"));
	}
}
