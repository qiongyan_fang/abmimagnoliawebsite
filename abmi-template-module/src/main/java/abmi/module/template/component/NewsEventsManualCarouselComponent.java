package abmi.module.template.component;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;


import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueChildNodeTransformer;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "News Events Manual Carousel Box", id = "abmiModule:components/manualNewsEventCarousel")
@TemplateDescription("News Events Carousel")
@HightLightBoxComponent
public class NewsEventsManualCarouselComponent {

	
	 @RequestMapping("/manualNewsEventCarousel")
	  public String render(ModelMap model, Node content) throws RepositoryException {
		 		
		 
		        
		        	
		        
		        
		        int nCharacter= 140; // default is 140
		        try {
		        	nCharacter	= Integer.parseInt( content.getProperty("charactercount").getString()); 
		      
		        }catch(Exception err){
		        	
		        }
		        
		        
		        int nTitleCharacter= 140; // default is 140
		        try {
		        	nTitleCharacter	= Integer.parseInt( content.getProperty("titlecount").getString()); 
		      
		        }catch(Exception err){
		        	
		        }
		        
		        
		     
		        ArrayList<Map<String, String>> newsCarousel =	new ArrayList<Map<String, String>>();
		        
				
				ArrayList<String> pageList = TemplateUtil.getMultiFieldValues(content, "carousellist");
				
		     System.out.println("pageList = " + pageList);
		        for (String pagePath :pageList) {
		        	
//		        	System.out.println("processing node " + node);
		        	Node node = content.getSession().getNode(pagePath);
		            if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)
		            		
		            		) {
		            	
		            	Map<String, String>  newsDetails = new LinkedHashMap<String, String>();
		            	newsDetails.put("link", node.getPath());
		            	newsDetails.put("title", TemplateUtil.getCharByNumber(PropertyUtil.getString(node, "title", ""), nTitleCharacter));
		            	newsDetails.put("description", TemplateUtil.getCharByNumber(PropertyUtil.getString(node, "description", ""),nCharacter));
		            	newsDetails.put("smallimage", PropertyUtil.getString(node, "newsImageUrl"));
		            	newsDetails.put("largeimage", PropertyUtil.getString(node, "imageUrl", ""));
		            	
		            	String templateName = NodeTypes.Renderable.getTemplate(node);
		            	boolean bShowEvent = templateName.contains("event");
		            	if (bShowEvent
		            			){ // Event
		            			Calendar calendar = PropertyUtil.getDate(node, "eventstartdate");
		            		
		            		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
		            		String date = sdf.format(calendar.getTime());
		            		
			            	newsDetails.put("date", date);
			            	newsDetails.put("location", PropertyUtil.getString(node, "eventlocation", ""));
			          
			            	
		            	}
		            	else if (!bShowEvent) {
			            	Calendar calendar = PropertyUtil.getDate(node, "publishdate");
		            		
		            		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
		            		String date = "";
		            		try{
		            			date =	sdf.format(calendar.getTime());
		            		}catch(Exception err){
		            			
		            		}
		            		
			            	newsDetails.put("date", date);
			            	
		            	}
		            	
		            	
		            	newsCarousel.add(newsDetails);
		            	
		            }
		        }
		        
		   
		        model.put("newsItem", newsCarousel);
		      
	        return "components/news_carousel_manual_component.jsp";
	    }

	 @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
		 Map<String, String> positionCategories = new LinkedHashMap<String, String>();
		 positionCategories.put("left", "left");
		 positionCategories.put("right", "right");
   	  
	    	 tab.fields(
	    			// cfg.fields.text("id").label("Name (no space- Optional)").defaultValue("carousel-news"),
//	    			 cfg.fields.link("rootpath").label("News Path").appName("pages").targetWorkspace("website").defaultValue("/home/news"),
	    			 cfg.fields.select("imageposition").label("News Image Position").options(positionCategories.keySet()),
//	    			 cfg.fields.text("newscount").label("Number of News/Events").defaultValue("4"),
	    			 cfg.fields.text("charactercount").label("Number of characters").defaultValue("140"),
	    			 cfg.fields.text("titlecount").label("Number of title characters").defaultValue("60"),
	    				cfg.fields
						.multi("carousellist")
						.label("Pages to be included")
						.field(cfg.fields
										.link("pagelink")
										.label("News/Events Page")
										.appName(
												"pages")
										.targetWorkspace(
												"website"))
						.transformerClass(
								MultiValueChildNodeTransformer.class)
	         );
	    }
}
