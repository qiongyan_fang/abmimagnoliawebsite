package abmi.module.template.component;

import java.util.ArrayList;
import java.util.Map;

import javax.jcr.Node;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.CompositeFieldBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueSubChildrenNodePropertiesTransformer;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "JumboTron (Image Banner)", id = "abmiModule:components/homejumbotron")
@TemplateDescription("Big JumboTron video/image")

public class MainJumbotronComponent {

	  @RequestMapping("/mainjumbotron")
	    public String render(Node content, ModelMap model) {

		  ArrayList<Map<String, String>> headerText = TemplateUtil.getMultiField(content, "headertext");
		  	
		  	int videoNum = (int) Math.round(Math.random()*headerText.size())%headerText.size();
		  	model.put("headertext", headerText.get(videoNum));
		  	model.put("videonum", videoNum + 1); // as video number starts from 0
	        return "components/jumbotron.jsp";
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
//	    			    cfg.fields.link("image").label("Image").appName("assets").targetWorkspace("dam"),
	    			 /*   cfg.fields.link("videomp4").label("Video Mp4").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.link("videoogg").label("Video OGG").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.link("videowebm").label("Video WBM").appName("assets").targetWorkspace("dam"),*/
	    				cfg.fields.multi("headertext").label("Header Text").field(new CompositeFieldBuilder("headertextcompo")
						.fields(cfg.fields.text("header").label("Main Header Text")).
						 fields(cfg.fields.text("subheader").label("SubHeader Text"))
								).transformerClass(MultiValueSubChildrenNodePropertiesTransformer.class),
					
	    			 
	    			    cfg.fields.checkbox("inheritable").buttonLabel("").label("Show in Subpages?")
	         );
	    }
}
