
package abmi.module.template.component.rightsidewidget;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RightWidgetComponent
@Controller
@Template(title = "Social Media Area ", id = "abmiModule:components/socialmediaiconarea")
@TemplateDescription("Social Media Icons Area")

public class SocialMediaAreaComponent {

	  @RequestMapping("/socialmediaiconarea")
	    public String render() {
	    
	        return "components/socialmediaicon_area.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                 cfg.fields.text("header").label("Header Text"),
	                 cfg.fields.checkbox("inheritable").buttonLabel("").label("Inheritable").description("Show in Subpages?")
	         );
	    }
	    
	    
	    @Area("socialMediaIconsArea")
	    @Controller
	    @AvailableComponentClasses(SocialMediaComponent.class)
	    public static class SocialMediaArea {

	        @RequestMapping("/socialmediaiconarea/icons")
	        public String render() {
	        	return "areas/generalarea.jsp";
	        }
	    }
	  
}
