package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@HightLightBoxComponent
@Template(title = "Spotlight Box", id = "abmiModule:components/spotlightbox")
@TemplateDescription("Spotlight Box")

public class SpotlightComponent {

	  @RequestMapping("/spotlightbox")
	    public String render() {
	    
	        return "components/spotlightbox.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
	                 cfg.fields.link("url").label("Link Url").appName("pages").targetWorkspace("website").description("url for the whole box. leave it blank if you want different urls for headers, and contents"),
	                 cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),
	                 cfg.fields.text("headerline1").label("Top Header line Text"),
	                 cfg.fields.link("headerline1url").label("Header Url").appName("pages").targetWorkspace("website").description("Optional url"),
	                 cfg.fields.text("headerline2").label("Middle Header line Text"),
	                 cfg.fields.link("headerline2url").label("Link Url").appName("pages").targetWorkspace("website").description("Optional url"),
	                 cfg.fields.text("content").label("Content Text").rows(5),
	                 cfg.fields.link("contenturl").label("Link Url").appName("pages").targetWorkspace("website").description("Optional url")
	         );
	    }
}
