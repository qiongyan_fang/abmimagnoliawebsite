package abmi.module.template.component.security;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@Template(title = "Rich Text", id = "abmiModule:components/richtext")
@TemplateDescription("Rich text block")

public class LoginBlockComponent {



    @RequestMapping("/userinformation")
    public String render() {
        return "components/loginblock.jsp";
    }

    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
        tab.fields(
        		   cfg.fields.link("loginlink").label("Login Link").appName("pages").targetWorkspace("website"),
        		   cfg.fields.link("logoutlink").label("Logout Link").appName("pages").targetWorkspace("website")
        );
    }
}