package abmi.module.template.component.contactus;



import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.MainContent;

@Controller

@Template(title = "Center Brief Address", id = "abmiModule:components/centrebriefaddressarea")
@TemplateDescription("Center Brief Address")
//@FullWidthContent
@MainContent
public class BriefAddressAreaComponent {

	  @RequestMapping("/centrebriefaddressarea")
	    public String render(Node content, ModelMap model ) {
	    

			  return "components/address_multipleblock_area.jsp";
			  
		
	    }

	   
	    
	    @Area(value="centrebriefaddressarea")
		@Controller
		@AvailableComponentClasses({BriefAddressComponent.class
			})
		public static class StaffArea {

			@RequestMapping("/centrebriefaddressarea/centerblock")
			public String render() {
				 return "areas/generalarea.jsp";
			}
		}
}
