
package abmi.module.template.component.rightsidewidget;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;


@Controller
@Template(title = "Social Media Icons", id = "abmiModule:components/socialmediaicon")
@TemplateDescription("Social Media Icons")

public class SocialMediaComponent {

	  @RequestMapping("/socialmediaicon")
	    public String render() {
	    
	        return "components/socialmediaicon.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                 cfg.fields.select("type").options(TemplateUtil.getSocialMedia()).label("Select Social Media")
	         );
	    }
}
