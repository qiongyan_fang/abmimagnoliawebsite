package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "FAQ Item Area", id = "abmiModule:components/faqboxarea")
@TemplateDescription("FAQ Items")
@MainContent
public class FAQAreaComponent {

	@Area("FAQArea")
	@Controller
	@AvailableComponentClasses(FAQStyleBlockComponent.class)
	public static class LeftArea {

		@RequestMapping("/faqboxarea/singlefaq")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@RequestMapping("/faqboxarea")
	public String render() {
		return "components/faqboxareacomponent.jsp";
	}
}
