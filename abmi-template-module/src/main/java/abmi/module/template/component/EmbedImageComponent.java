package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;


@Controller
@Template(title = "Zoomable Image with Caption", id = "abmiModule:components/embedimage")
@TemplateDescription("Zoomable Image with Caption")
@MainContent
@FullWidthContent
public class EmbedImageComponent {

	  @RequestMapping("/embedImageComponent")
	    public String render() {
	    
	        return "components/imagewithcaption.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	    			 	cfg.fields.text("boxwdith").label("Box width in pixel").description("leave it as blank for image above text, set pixel for embedded in text"),
	    			
	    			    cfg.fields.link("largeimageUrl").label("large Image").appName("assets").targetWorkspace("dam"),
		                cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam").required(),
		                cfg.fields.select("imageposition").label("Image Position").description("empty for image above text, left or right embedded in text").options(TemplateUtil.getPostition().keySet()).readOnly(false),
		                cfg.fields.text("caption").label("Image Caption"),
		                cfg.fields.checkbox("zoomable").label("Image Zoomable?").buttonLabel("").description("force to zoomable use same image when no large images provided ")
	         );
	    }
}
