package abmi.module.template.component.glossary;


import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Include Glossary Items", id = "abmiModule:components/citeglossaryitem")
@TemplateDescription("Include Glossary Items")

public class GlossaryCitationComponent {
	@RequestMapping("/citeglossaryitem")
    public String render(Node content, ModelMap map) {
	 
		//	map.put("description", TemplateUtil.getOneGlossaryItem(PropertyUtil.getString(content, "glossary", null)));
	 return "components/glossaryiteminuse.jsp";
    }

    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
    	 
    	 tab.fields( 
    	 cfg.fields.select("glossary").label("Glossary Name").options(TemplateUtil.getAllGlossaryItems("/home/glossary")).readOnly(false)
		         );
		
    }
}
