package abmi.module.template.component;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;


import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "News Events Carousel Box", id = "abmiModule:components/autoNewsEventCarousel")
@TemplateDescription("News Events Carousel")
@HightLightBoxComponent
public class NewsCarouselComponent {

	@RequestMapping("/newsCarouselbox")
	public String render(ModelMap model, Node content)
			throws RepositoryException {

		String path = content.getProperty("rootpath").getString();
		if (path == null || "".equals(path.trim())) {
			path = "/home/news";
		}

		int newsCount = 4; // default is 4
		try {
			newsCount = Integer.parseInt(content.getProperty("newscount")
					.getString());

		} catch (Exception err) {

		}

		int nCharacter = 140; // default is 140
		try {
			nCharacter = Integer.parseInt(content.getProperty("charactercount")
					.getString());

		} catch (Exception err) {

		}

		int nTitleCharacter = 140; // default is 140
		try {
			nTitleCharacter = Integer.parseInt(content
					.getProperty("titlecount").getString());

		} catch (Exception err) {

		}

		boolean bShowEvent = false;
		try {
			bShowEvent = content.getProperty("showEvent") == null ? false
					: content.getProperty("showEvent").getBoolean();
		} catch (Exception err) {
			// System.out.println("error getting showEvent property" +
			// err.getMessage());
		}

		ArrayList<Map<String, String>> newsCarousel = new ArrayList<Map<String, String>>();

		List<Node> subPages = NodeUtil.asList(NodeUtil.getNodes(content
				.getSession().getNode(path), NodeTypes.Page.NAME));

		for (int i = 0; i < newsCount; i++) {
			if (subPages.size() - 1 - i < 0) { // don't do anything if more
												// pages requested than the
												// actual page collection.
				break;
			}

			Node node = subPages.get(subPages.size() - 1 - i);

			if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)

			) {

				Map<String, String> newsDetails = new LinkedHashMap<String, String>();
				newsDetails.put("link", node.getPath());
				newsDetails.put("title", TemplateUtil.getCharByNumber(
						PropertyUtil.getString(node, "title", ""),
						nTitleCharacter));
				newsDetails.put("description", TemplateUtil.getCharByNumber(
						PropertyUtil.getString(node, "description", ""),
						nCharacter));
				if (bShowEvent) { // Event
					Calendar calendar = PropertyUtil.getDate(node,
							"eventstartdate");

					SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
					String date = sdf.format(calendar.getTime());

					newsDetails.put("date", date);
					newsDetails.put("location",
							PropertyUtil.getString(node, "eventlocation", ""));

				

				} else if (!bShowEvent) {
					Calendar calendar = PropertyUtil.getDate(node,
							"publishdate");

					SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
					String date = "";
					try {
						date = sdf.format(calendar.getTime());
					} catch (Exception err) {

					}

					newsDetails.put("date", date);
					newsDetails.put("smallimage",
							PropertyUtil.getString(node, "newsImageUrl"));
					newsDetails.put("largeimage",
							PropertyUtil.getString(node, "imageUrl", ""));
					// newsDetails.put("description",
					// TemplateUtil.getCharByNumber(PropertyUtil.getString(node,
					// "description", ""),nCharacter));

				}

				newsCarousel.add(newsDetails);

			}
		}

		model.put("isEventNews", bShowEvent ? "event" : "news");
		model.put("newsItem", newsCarousel);

		return "components/news_carousel_component.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		Map<String, String> positionCategories = new LinkedHashMap<String, String>();
		positionCategories.put("left", "left");
		positionCategories.put("right", "right");

		tab.fields(
				// cfg.fields.text("id").label("Name (no space- Optional)").defaultValue("carousel-news"),
				cfg.fields.link("rootpath").label("News Path").appName("pages")
						.targetWorkspace("website").defaultValue("/home/news"),
				cfg.fields.select("imageposition").label("News Image Position")
						.options(positionCategories.keySet()),
				cfg.fields.text("newscount").label("Number of News/Events")
						.defaultValue("4"),
				cfg.fields.text("charactercount").label("Number of characters")
						.defaultValue("140"),
				cfg.fields.text("titlecount")
						.label("Number of title characters").defaultValue("60"),
				cfg.fields.checkbox("showEvent").buttonLabel("")
						.label("show Event")
						.description("Check this box to show Events")

		//
		// ,
		// cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),
		// cfg.fields.text("headerline1").label("Top Header line Text"),
		// cfg.fields.text("headerline2").label("Middle Header line Text"),
		// cfg.fields.text("content").label("Content Text")
		);
	}
}
