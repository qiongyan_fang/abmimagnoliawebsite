package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@Template(title = "Link Buttons Area", id = "abmiModule:components/newseventbuttons")
@MainContent
@FullWidthContent
public class NewsEventsButtonComponent {
	
	 @Area(value="Links")
		@Controller
		@AvailableComponentClasses({PlainHyperlinkComponent.class})
		 
		public static class LinkArea {
			@RequestMapping("/newseventbuttons/component")
			public String render() {
				return "areas/generalarea.jsp";
			}
		}

		
		@RequestMapping("/newseventbuttons")
		public String render() {
			return "components/newsbuttonareacomponent.jsp";
		}
}
