package abmi.module.template.component.footer;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Footer Menu Area", id = "abmiModule:components/footermenuarea")
@TemplateDescription("Footer Menu  Area")
public class FooterAreaComponent {

	@Area(value="FooterMenuArea",  maxComponents=2)
	@Controller
	@AvailableComponentClasses(FooterComponent.class)
	 
	public static class FooterArea {
		@RequestMapping("/footermenuarea/component")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	
	@RequestMapping("/footermenuarea")
	public String render() {
		return "components/footermenuareacomponent.jsp";
	}
}
