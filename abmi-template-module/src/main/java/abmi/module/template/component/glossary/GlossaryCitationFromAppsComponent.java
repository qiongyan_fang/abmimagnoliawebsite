package abmi.module.template.component.glossary;


import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.model.util.ModuleParameters;

@Controller
@Template(title = "Include Glossary Item From Glossary Apps", id = "abmiModule:components/citeglossaryitemfromapps")
@TemplateDescription("Include Glossary Items From Glossary Apps")

public class GlossaryCitationFromAppsComponent {
	private static final Logger log = LoggerFactory
			.getLogger(GlossaryCitationFromAppsComponent.class);
	
	@RequestMapping("/citeglossaryitemfromapps")
    public String render(Node content, ModelMap model) {
		String nodePath = PropertyUtil.getString(content, "glossarypath", null);
		Session session;
		
		
		try {
		if (nodePath!= null){
			session = MgnlContext.getJCRSession(ModuleParameters.GLOSSARY_WORKSPACE);
			Node c = session.getNode(nodePath);
			String glossaryName = PropertyUtil.getString(c, "glossaryName");
			model.put("glossaryName", glossaryName);
			
			
		}
			
		} catch (PathNotFoundException e) {
			// TODO Auto-generated catch block
			log.error("path not found " + e.getMessage());
			
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			log.error("repsotiry exception " + e.getMessage());
		}
	 
	 return "components/glossaryiteminusefromapps.jsp";
    }

    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
    	 
    	 tab.fields( 
    	 cfg.fields.link("glossarypath").appName(ModuleParameters.GLOSSARY_APPS).targetWorkspace(ModuleParameters.GLOSSARY_WORKSPACE) );
		
    }
}
