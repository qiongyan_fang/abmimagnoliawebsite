
package abmi.module.template.component;


import javax.jcr.Node;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "FAQ Single Item", id = "abmiModule:components/faqbox")
@TemplateDescription("FAQ Single Item")

public class FAQStyleBlockComponent {
	 @RequestMapping("/faqbox")
	    public String render(Node content) {
		 int index = TemplateUtil.getNodePosition(content,  "abmiModule:components/faqbox");
		 try {
			content.setProperty("componentindex", "index" + index);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		 
		 return "components/faqbox.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	    	tab.fields(
	    		     cfg.fields.link("url").label("Link Url").appName("pages").targetWorkspace("website"),
	                 cfg.fields.text("question").label("Question").rows(5),
	                 cfg.fields.richText("answer").label("Answer"),
	                 cfg.fields.hidden("componentindex")
	                 
	         );
	    }
}
