package abmi.module.template.component.menu;


import javax.jcr.Node;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.CompositeFieldBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueSubChildrenNodePropertiesTransformer;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Menu", id = "abmiModule:components/megamenudetail")
@TemplateDescription("Big JumboTron video/image")

public class SubMenuComponent {

	  @RequestMapping("/megamenudetail")
	    public String render(Node content, ModelMap model) {
		 
		  
		  	model.put("menu", TemplateUtil.getMultiField(content, "menu"));
	        return "components/megamenu_single.jsp";
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
//	    			    cfg.fields.link("image").label("Image").appName("assets").targetWorkspace("dam"),
	    			 /*   cfg.fields.link("videomp4").label("Video Mp4").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.link("videoogg").label("Video OGG").appName("assets").targetWorkspace("dam"),
	    			    cfg.fields.link("videowebm").label("Video WBM").appName("assets").targetWorkspace("dam"),*/
	    				cfg.fields.multi("menu").label("Menu Detail").field(new CompositeFieldBuilder("headertextcompo")
						.fields(cfg.fields.text("description").label("Description").rows(3)).
						fields(cfg.fields.text("titletext").label("optional title")).
						 fields(cfg.fields.link("rootpath").label("Link Url").appName("pages").targetWorkspace("website")).
						 fields(cfg.fields.link("imageurl").label("Image").appName("assets").targetWorkspace("dam"))
								).transformerClass(MultiValueSubChildrenNodePropertiesTransformer.class)
					
	    			 
	    			   
	         );
	    }
}
