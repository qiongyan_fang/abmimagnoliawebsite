package abmi.module.template.component.box;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller

@Template(title = " Image Button Custom Height Box", id = "abmiModule:components/customheightimagebox")

public class CustomHeightImageBox {

	  @RequestMapping("/customheightimagebox")
	    public String render(Node content, ModelMap model ) {
	    
//		  int posistion = TemplateUtil.getNodePosition(content, NodeTypes.Component.NAME, "abmiModule:components/projectintrobox");
//		  
//		  try{
//			  PropertyUtil.setProperty(content, "myindex", posistion+"");
//		  }
//		  catch(Exception e){}
		  
//		  
//		  	System.out.println("background color:" + PropertyUtil.getString(content, "backgroundcolor",""));
//			  String boxPerRow = PropertyUtil.getString(content, "boxnum","narrow");
//			  int width = 4;
//			  if(boxPerRow.equals("medium")){
//				  width = 5;
//			  }
//			  else   if(boxPerRow.equals("wide")){
//				  width = 6;
//			  }
//			  model.put("width", width );
			  return "components/imageboxcustomheight.jsp";
			  
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    
		 
	    	 tab.fields(
	    			 cfg.fields.select("boxnum").options(TemplateUtil.getBoxWidth()).label("Boxes width"),
	                 cfg.fields.link("imageUrl").label("Top Image").appName("assets").targetWorkspace("dam"),
	                 cfg.fields.link("logoUrl").label("Project logo Image").appName("assets").targetWorkspace("dam"),
	                 cfg.fields.text("header").label("Header line"),
	                 cfg.fields.text("description").label("Content Text").rows(5),
	                 cfg.fields.text("descriptionrow").label("Minimum Description row numbers"),
	                 cfg.fields.text("buttonText").label("button Text").defaultValue("VISIT WEBSITE"),
	                 cfg.fields.link("externalUrl").label("Website Link Url").appName("pages").targetWorkspace("website"),
	                 cfg.fields.select("backgroundcolor").label("Background Color").options(TemplateUtil.getBaseColor()).readOnly(false)
	         );
	    }
}
