
package abmi.module.template.component.rightsidewidget;

import java.util.ArrayList;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@Template(title = "Project Toggle Box", id = "abmiModule:components/projecttogglebox")
@TemplateDescription("Project Toggle Box")
@RightWidgetComponent
public class ProjectToggleBoxComponent {

	  @RequestMapping("/projecttogglebox")
	    public String render() {
	    
	        return "components/project_toggle_box.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	ArrayList <String> headerColor = new ArrayList <String>(); 
	    	headerColor.add("black");
	    	headerColor.add("green");
	    	 tab.fields(
	                 cfg.fields.text("header").label("Header"), 
	                 cfg.fields.select("headercolor").label("Header Color").options(headerColor),
	                 cfg.fields.checkbox("bold").label("bold header").buttonLabel("click to use bold header"),
	                 cfg.fields.richText("description").label("Description")
	                 
	         );
	    }
}
