package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Green Icon Button", id = "abmiModule:components/greeniconbutton")
@TemplateDescription("Green Icon Button")
public class MainIconButtonComponent {
	@RequestMapping("/greeniconbutton")
	public String render() {

		return "components/greeniconbutton.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				cfg.fields.link("url").label("Link Url").appName("pages")
						.targetWorkspace("website"),
				cfg.fields.link("imageurl").label("icon").appName("assets")
						.targetWorkspace("dam"),
				cfg.fields.text("buttontext").label("Button Text"),
				cfg.fields.checkbox("last").label("is this last one?")
						.buttonLabel("is this the last button ?")
						.defaultValue("false"), 
						cfg.fields.checkbox("oneLine")
						.buttonLabel("").label("vertical centered?")
						.defaultValue("true")

		);
	}
}
