package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Side by Side Area", id = "abmiModule:components/sidebysidearea")
@MainContent
@FullWidthContent
public class SideBySideAreaComponent {

	@Area("sidebysidearea")
	@Controller
	@AvailableComponentClasses(SideBySideSingleComponent.class)
	public static class LeftArea {

		@RequestMapping("/sidebysidearea/singlediv")
		public String render() {
			return "areas/sidebysidearea.jsp";
		}
		
		 @TabFactory("Content")
		    public void contentTab(UiConfig cfg, TabBuilder tab) {
		        tab.fields(
		                cfg.fields.text("areacount").label("Area Per Row").defaultValue("2").required()
		        );
		   
		 }

	}

	@RequestMapping("/sidebysidearea")
	public String render() {
		return "components/sidebysideareacomponent.jsp";
	}
	
	
}