package abmi.module.template.component.footer;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.CompositeFieldBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueSubChildrenNodePropertiesTransformer;
import info.magnolia.ui.framework.config.UiConfig;
import org.springframework.ui.ModelMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

/**
 * component for add footer (text and links)
 * @author Qiongyan
 *
 */
@Controller
@Template(title = "Footer", id = "abmiModule:components/footer")
@TemplateDescription("Footer")

public class FooterComponent {
	  @RequestMapping("/footer")
	    public String render(Node content, ModelMap model) {
			model.put("menu", TemplateUtil.getMultiField(content, "menu"));
	        
	        return "components/footercomponent.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	        tab.fields(
	                cfg.fields.text("header").label("Heading"),
	                cfg.fields.multi("menu").label("Menu Detail").field(new CompositeFieldBuilder("headertextcompo")
					.fields(cfg.fields.text("linktext").label("Option Link Text").rows(1)).
					 fields(cfg.fields.link("rootpath").label("Link Url").appName("pages").targetWorkspace("website"))
					
							).transformerClass(MultiValueSubChildrenNodePropertiesTransformer.class),
	                cfg.fields.checkbox("inheritable").defaultValue("true").buttonLabel("").label("Inheritable").description("Show in Subpages?")
	        );
	    }
}
