package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@Template(title = "Image", id = "abmiModule:components/image")
@TemplateDescription("Image")
@MainContent
@FullWidthContent
public class ImageComponent {

	  @RequestMapping("/imageComponent")
	    public String render() {
	    
	        return "components/image.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	    			 	cfg.fields.text("url").label("Image Url").description("image link"),
		                cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam")
		               
		                
	         );
	    }
}
