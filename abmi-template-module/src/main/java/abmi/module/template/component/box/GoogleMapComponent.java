package abmi.module.template.component.box;




import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;



import abmi.module.template.component.FullWidthContent;
import abmi.module.template.component.MainContent;
import abmi.module.template.util.TemplateUtil;

@Controller

@Template(title = "Google Map Box", id = "abmiModule:components/googlebox")
@TemplateDescription("Google Map Box")
@MainContent
@FullWidthContent
public class GoogleMapComponent {

	  @RequestMapping("/googlebox")
	    public String render(Node content, ModelMap model ) {
	    
//		  int posistion = TemplateUtil.getNodePosition(content, NodeTypes.Component.NAME, "abmiModule:components/projectintrobox");
//		  
//		  try{
//			  PropertyUtil.setProperty(content, "myindex", posistion+"");
//		  }
//		  catch(Exception e){}
		  
		  return "components/googlemap.jsp";
		  
	    }
	  
	  @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
//	    			 cfg.fields.text("url").label("Google Link").defaultValue("https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1185.8156470039667!2d-113.5250812630893!3d53.5286414596274!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x53a022208c7bbe49%3A0x31ac2f85e0ff8b70!2sCentennial+Centre+for+Interdisciplinary+Science%2C+University+of+Alberta%2C+Edmonton%2C+AB+T6G+2E9!5e0!3m2!1sen!2sca!4v1412099538877"),
//	    			 cfg.fields.text("width").label("Width (default 100%)").defaultValue("100%"),
//	    			 cfg.fields.text("height").label("Height (actual pixel)").defaultValue("380"));
//	    	 
	    			 cfg.fields.text("lat").label("Latitude"),
	    			 cfg.fields.text("longitude").label("Longitude"),
	    			 cfg.fields.text("mapname").label("Map Name"),
	    			 cfg.fields.select("boxwidth").options(TemplateUtil.getBoxWidth()).label("Boxes width").defaultValue("wide"));
	  }
}