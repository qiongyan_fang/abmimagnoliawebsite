package abmi.module.template.component;


import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.util.TemplateUtil;


@Controller
@Template(title = "Rich Text", id = "abmiModule:components/richtext")
@TemplateDescription("Rich text block")
@MainContent
@FullWidthContent
@RightWidgetComponent
@TextContent
public class RichTextComponent<FckEditorDialog> {

    @RequestMapping("/richtext")
    public String render() {
        return "components/richtext.jsp";
    }


    
    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
        tab.fields(
        		
//                cfg.fields.text("heading").label("Heading"),
                cfg.fields.richText("body").images(true).label("Text body"),
                cfg.fields.select("backgroundcolor").label("Background Color").options(TemplateUtil.getBaseColor()).readOnly(false),
                cfg.fields.checkbox("nopadding").label("no padding?").buttonLabel("").description("use padding and color for quote").defaultValue("false"),
                cfg.fields.checkbox("clearfloat").label("remove floating?").buttonLabel("").description("used float inside rich text, and clear it at bottom").defaultValue("false")
                
        );
   
        
        
    }
}

