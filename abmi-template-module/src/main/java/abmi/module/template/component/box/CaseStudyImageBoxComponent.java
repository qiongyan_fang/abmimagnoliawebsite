
package abmi.module.template.component.box;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.MainContent;
import abmi.module.template.util.TemplateUtil;

@Controller

@Template(title = "Case Study Box", id = "abmiModule:components/casestudybox")
@TemplateDescription("Case Study Box")
@MainContent

public class CaseStudyImageBoxComponent {

	  @RequestMapping("/casestudybox")
	    public String render(Node content, ModelMap model ) {
	    

			  return "components/casestudyimagebox.jsp";
			  
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
		 
	    	 tab.fields(
	    			 
	                 cfg.fields.link("imageUrl").label("Top Image").appName("assets").targetWorkspace("dam"),
	              
	                 cfg.fields.text("header").label("Header line"),
	                 cfg.fields.text("description").label("Content Text").rows(5),
	                 cfg.fields.text("author").label("Author"),
	                 cfg.fields.date("publishdate").label("Publish Date"),
	                 cfg.fields.date("industry").label("Industry Type"),                
	                 
	                 cfg.fields.select("backgroundcolor").label("Background Color").options(TemplateUtil.getBaseColor()).readOnly(false)
	         );
	    }
}
