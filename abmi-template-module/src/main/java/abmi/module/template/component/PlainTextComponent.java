package abmi.module.template.component;


import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.rightsidewidget.RightWidgetComponent;


@Controller
@Template(title = "Plain Text", id = "abmiModule:components/plaintext")
@TemplateDescription("Plain text block")
@MainContent
@FullWidthContent
@RightWidgetComponent
@TextContent
public class PlainTextComponent<FckEditorDialog> {

    @RequestMapping("/plaintext")
    public String render() {
        return "components/plaintext.jsp";
    }


    
    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
        tab.fields(
        		
//                cfg.fields.text("heading").label("Heading"),
                cfg.fields.text("plaintext").label("Text body (no format, or script").rows(6)
             
                
        );
   
        
        
    }
}

