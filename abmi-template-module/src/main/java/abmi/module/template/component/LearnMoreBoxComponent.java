package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Learn More Box", id = "abmiModule:components/learnmorebox")
@TemplateDescription("Learn More Box")
@HightLightBoxComponent
@RightWidgetComponent
public class LearnMoreBoxComponent {
	 @RequestMapping("/learnmorebox")
	    public String render() {
	    
	        return "components/learnmorebox.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	
	   	  
	    	tab.fields(
	                
	                 cfg.fields.link("url").label("Link Url").appName("pages").targetWorkspace("website"),
	                 cfg.fields.link("imageUrl").label("Image").appName("assets").targetWorkspace("dam"),
	                 cfg.fields.text("headerline").label("Top Header line Text"),
	                 cfg.fields.select("imageposition").label("Image Position").options(TemplateUtil.getPostition().keySet()),
	                 cfg.fields.text("content").label("Content Text").rows(5)
	         );
	    }
}
