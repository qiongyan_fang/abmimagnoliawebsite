package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Common  Button", id = "abmiModule:components/commonbutton")
@TemplateDescription("Common Button")
//@MainContent
//@FullWidthContent
public class CommonButtonComponent {
	@RequestMapping("/commonbutton")
	public String render() {

		return "components/commonbutton.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				cfg.fields.text("url").label("Link Url"),
				cfg.fields.text("buttontext").label("Button Text"),
				cfg.fields.select("color").label("Button Color").options(TemplateUtil.getBaseColor())

		);
	}
}
