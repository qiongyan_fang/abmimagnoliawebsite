package abmi.module.template.component;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Plain Url", id = "abmiModule:components/plainUrl")
@TemplateDescription("Simple text block")


public class PlainHyperlinkComponent {

	  @RequestMapping("/plainlink")
	    public String render() {
	    
	        return "components/plain_url.jsp";
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	                
	                 cfg.fields.link("url").label("Link Url").appName("pages").targetWorkspace("website"),
	                 cfg.fields.text("text").label("link text").description("required for external link. will use page titles if empty for internal links"),
	                 cfg.fields.text("externalurl").label("External Url")
	         );
	    }
}
