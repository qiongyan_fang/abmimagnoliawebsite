package abmi.module.template.component.glossary;

import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Template(title = "Glossary Items", id = "abmiModule:components/glossaryitem")
@TemplateDescription("Glossary Items")

public class GlossaryItemComponent {
	@RequestMapping("/glossaryitem")
    public String render(Node content) {
	 
	 return "components/glossaryitem.jsp";
    }

    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
    	
    	tab.fields(
    			 cfg.fields.text("name").label("Glossary Name for query"),
//                 cfg.fields.text("acronyms").label("Glossary Name"),
                 cfg.fields.richText("detail").label("Glossary Detail")
                 
                 
         );
    }
}
