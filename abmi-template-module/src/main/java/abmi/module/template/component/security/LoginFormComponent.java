package abmi.module.template.component.security;

import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@Template(title = "Login Form", id = "blossomSampleModule:components/loginform")
@TemplateDescription("Login Form")

public class LoginFormComponent {

	 @RequestMapping("/loginform")
	    public String render() {
	        return "components/loginform.jsp";
	    }
	 
	
}
