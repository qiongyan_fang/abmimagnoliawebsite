package abmi.module.template.component.box;


import javax.jcr.Node;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.FullWidthContent;

@Controller

@Template(title = "Image Box Area", id = "abmiModule:components/imageboxarea")
@TemplateDescription("Image Box Area")
@FullWidthContent
public class ImageBoxAreaComponent {

	  @RequestMapping("/imageboxarea")
	    public String render(Node content, ModelMap model ) {
	    

			  return "components/imageboxarea.jsp";
			  
		
	    }

	    @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	    	 tab.fields(
	    			
	                 cfg.fields.text("header").label("Header Text")
	         );
	    }
	    
	    @Area(value="imageboxarea")
		@Controller
		@AvailableComponentClasses({ImageBox.class,ResourceViewDownloadBox.class
			})
		public static class ImageBoxArea {

			@RequestMapping("/imageboxarea/area")
			public String render() {
				 return "areas/colorbox_area.jsp";
			}
		}
}
