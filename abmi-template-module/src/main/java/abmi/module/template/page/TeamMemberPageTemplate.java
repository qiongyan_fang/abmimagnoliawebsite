package abmi.module.template.page;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.JumbotronComponent;

import abmi.module.template.component.MainContent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.util.TemplateUtil;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;

import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

/**
 * this is a page for single team members
 * * I would use a
 * component instead of a page if I need to do it again.
 * 
 * @author Qiongyan
 * 
 */
@Controller
@Template(title = "Team member page", id = "abmiModule:pages/teammember")
public class TeamMemberPageTemplate {
	@Available
	public boolean isAvailable(Node websiteNode) {

		return TemplateUtil.checkAvailability(websiteNode);

	}

	@RequestMapping("/teammember/teammember")
	public String render(Node page, ModelMap model) throws RepositoryException {
		// model.put("currentLink",
		// MgnlContext.getAggregationState().getCurrentContentNode().getPath());
		return "pages/teammember.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(
				cfg.fields.text("position").label("ABMI Position"),
				cfg.fields.text("personname").label("Name"),
				cfg.fields.text("otherposition").label("Other Positions"),
				cfg.fields.link("photo").label("Photo").appName("assets")
						.targetWorkspace("dam"),
				cfg.fields
						.checkbox("hideInNavigation")
						.buttonLabel("")
						.label("Hide in navigation")
						.description(
								"Check this box to hide this page in navigation"),
				cfg.fields.text("keyword").label("keywords")
						.description("The keywords about this page"));
	}

	/**
	 * jumbotron area.
	 */
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/teammember/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Bar")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/teammember/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ TextComponent.class })
	public static class titleArea {

		@RequestMapping("/teammember/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses({ MainContent.class })
	public static class LeftColumnArea {

		@RequestMapping("/teammember/leftColumn")
		public String render() {
			return "areas/pagetwocolumn/leftcolumn_area.jsp";
		}
	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ RichTextComponent.class, PageNavigation.class })
	public static class RightColumnArea {

		@RequestMapping("/teammember/rightColumn")
		public String render() {
			return "areas/pagetwocolumn/rightcolumn_area.jsp";
		}

		@TabFactory("Content")
		public void contentTab(UiConfig cfg, TabBuilder tab) {
			tab.fields(cfg.fields.text("header").label("Header Text")
					.defaultValue("")

			);
		}

	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class RightWidgetColumnArea {

		@RequestMapping("/teammember/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		// @TabFactory("Content")
		// public void contentTab(UiConfig cfg, TabBuilder tab) {
		// Map<String, String> widgetCategories = new LinkedHashMap<String,
		// String>();
		// widgetCategories.put("Download List", "list");
		// widgetCategories.put("Connect Icons", "connect");
		//
		// tab.fields(cfg.fields.text("header").label("Title"),
		// cfg.fields.select("classname").label("Widget Type").options(widgetCategories.keySet()));
		//
		// }
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/teammember/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/teammember/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

}
