package abmi.module.template.page.publication;

import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.page.newsevents.ArchiveListComponent;


@Controller
//@Template(title = "News Page ??", id = "abmiModule:pages/newspost")
@RequestMapping("/newspageTemplate")
public class ZNewsPageTemplate {
   

    @RequestMapping("newspageTemplate")
    public String render(@RequestParam(value="id", required=false) Integer id, Node content,ModelMap model)throws RepositoryException {

//    public String render(Node page, ModelMap model) throws RepositoryException {

//        Map<String, String> navigation = new LinkedHashMap<String, String>();
//        for (Node node : NodeUtil.getNodes(page.getSession().getNode("/home"), NodeTypes.Page.NAME)) {
//            if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {
//                navigation.put(node.getPath(), PropertyUtil.getString(node, "title", ""));
//                System.out.println("level 1: " + PropertyUtil.getString(node, "title", "")) ;  
//              for (Node subNode:  NodeUtil.getNodes(node)){
//            	 System.out.println(PropertyUtil.getString(subNode, "title", "") + "  " +PropertyUtil.getBoolean(subNode, "hideInNavigation", false));
//              }
//            }
//        }
//        model.put("navigation", navigation);
//
    	model.put("currentLink", MgnlContext.getAggregationState().getCurrentContentNode().getPath());
    	
    
    	return "pages/newspage.jsp";
    }

    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
    
           
        tab.fields(
              
                cfg.fields.text("newstitle").label("Title").required(true),
                cfg.fields.date("publishdate").label("Date").required(true),
                cfg.fields.text("author").label("Author for News"),
                cfg.fields.text("location").label("Location for event:"),
                cfg.fields.link("imageUrl").label("News Image").appName("assets").targetWorkspace("dam"),
                cfg.fields.text("newsabstract").label("Abstract/Description").required(true),                
                cfg.fields.checkbox("hideInNavigation").buttonLabel("").label("Hide in Menu"), //.description("Check this box to hide this page in navigation"),
                cfg.fields.checkbox("isEvent").buttonLabel("").label("Is a Event").description("Check this box to make it a event"),
                cfg.fields.checkbox("archive").buttonLabel("").label("Achive News").description("Check this box to Archive"),
                cfg.fields.text("keyword").label("keywords").description("The keywords about this page")
        );
    }
    
    
    @Area(value="jumbotron", maxComponents=1)
    @Controller
    @AvailableComponentClasses({JumbotronComponent.class})
    
    public static class JumbotronArea {

        @RequestMapping("/newspageTemplate/teammemberjumbotron")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
	@Area(value="titleArea", maxComponents=1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class})
	public static class titleArea {

		@RequestMapping("/newspageTemplate/title")
		public String render() {
			
			return "areas/title_area.jsp";
		}
	}
	
    @Area("newsLeftColumnArea")
    @Controller
    @AvailableComponentClasses({TextComponent.class, RichTextComponent.class})
    public static class LeftColumnArea {

        @RequestMapping("/newspageTemplate/leftColumn")
        public String render() {
            return "areas/pagetwocolumn/leftcolumn_area.jsp";
        }
    }
    
    
    @Area("rightColumnArea")
    @Controller
    @Inherits(components = ComponentInheritanceMode.ALL)
    @AvailableComponentClasses({TextComponent.class, ArchiveListComponent.class})
    public static class ReftColumnArea {

        @RequestMapping("/newspageTemplate/rightColumn")
        public String render() {
            return "areas/pagetwocolumn/rightcolumn_area.jsp";
        }
    }
}