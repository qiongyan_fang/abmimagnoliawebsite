package abmi.module.template.page.newsevents;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.DialogFactory;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.dialog.config.DialogBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.AccessDeniedException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.MainContent;
import abmi.module.template.component.NewsEventsButtonComponent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;

@Controller
@Template(title = "Single Event Page", id = "abmiModule:pages/eventpage", dialog = "event-properties")
public class EventsPageTemplate {

	@DialogFactory("event-properties")
	public void frontPageProperties(UiConfig cfg, DialogBuilder dialog) {
		dialog.form()
				.tabs(cfg.forms
						.tab("Properties1")
						.label("General Property")
						.fields(

						cfg.fields.text("imagetitle").label("Abriged Title"),
								cfg.fields.text("title").label("Full Title"),

								cfg.fields.text("author").label("Author"),
								cfg.fields.link("imageUrl")
										.label("Project Image")
										.appName("assets")
										.targetWorkspace("dam"),
								// cfg.fields.text("image").label("Image"),
								cfg.fields.date("publishdate").label(
										"Publish Date"),
								cfg.fields.text("description")
										.label("Brief Description").rows(5),

								cfg.fields.link("newsImageUrl")
										.label("News Teaser Image")
										.appName("assets")
										.targetWorkspace("dam")
										.description(" News teaser image"),

								cfg.fields
										.checkbox("showparent")
										.buttonLabel(
												" use parent path in navigation menu")
										.label("use parent path in navigation menun"),

								cfg.fields.checkbox("hideInNavigation")
										.buttonLabel("")
										.label("Hide in navigation")),

						cfg.forms
								.tab("event")
								.label("For Events Only")
								.fields(
								// cfg.fields.checkbox("isevent")
								// .label("Is this a Event?")
								// .defaultValue("false").buttonLabel(""),
								cfg.fields.date("eventstartdate").label(
										"Event Start Date"),
										cfg.fields.date("eventenddate").label(
												"Event End Date"),
										cfg.fields.text("eventstarttime")
												.label("Event Start Time"),
										cfg.fields.text("eventendtime").label(
												"Event End Time"),
										cfg.fields.text("eventlocation").label(
												"Event Location")),
						cfg.forms
								.tab("Metadata")
								.label("Page Keyword")
								.fields(cfg.fields
										.text("keyword")
										.label("keywords")
										.description(
												"The keywords about this page"))

				);
	}

	@RequestMapping("/eventpage")
	public String render(

	Node content, ModelMap model) throws RepositoryException {

		if (PropertyUtil.getBoolean(content, "showparent", true)) {
			model.put("currentLink", MgnlContext.getAggregationState()
					.getCurrentContentNode().getParent().getPath());
		} else {
			model.put("currentLink", MgnlContext.getAggregationState()
					.getCurrentContentNode().getPath());
		}
		model.put("uri",
				PropertyUtil.getString(content, "rootpath", "/home/events"));
		// MgnlContext.getAggregationState().getCurrentContentNode().getParent().getHandle());

		return "pages/singleeventpage.jsp";

	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/eventpage/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Menu")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/eventpage/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	// header images
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/eventpage/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses({ MainContent.class,
			NewsEventsButtonComponent.class })
	public static class LeftColumnArea {

		@RequestMapping("/eventpage/leftColumn")
		public String render() {
			return "areas/pagetwocolumn/leftcolumn_area.jsp";
		}
	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.ALL)
	@AvailableComponentClasses({ TextComponent.class, PageNavigation.class })
	public static class WidgettColumnArea {

		@RequestMapping("/eventpage/rightColumnArea")
		public String render() {
			return "areas/pagetwocolumn/rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class RightWidgetColumnArea {

		@RequestMapping("/eventpage/rightColumnWidget")
		public String render(Node content, ModelMap model) {
			try {
				model.put("currentLink", content.getParent().getPath());
			} catch (AccessDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ItemNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		// @TabFactory("Content")
		// public void contentTab(UiConfig cfg, TabBuilder tab) {
		// Map<String, String> widgetCategories = new LinkedHashMap<String,
		// String>();
		// widgetCategories.put("Download List", "list");
		// widgetCategories.put("Connect Icons", "connect");
		//
		// tab.fields(cfg.fields.text("header").label("Title"),
		// cfg.fields.select("classname").label("Widget Type")
		// .options(widgetCategories.keySet()));
		//
		// }
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/eventpage/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/eventpage/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
}
