package abmi.module.template.page;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.OptionBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.*;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;

import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Two Column Page", id = "abmiModule:pages/pagetwocolumn")
public class PageTwoColumnTemplate {

	@RequestMapping("/pageTwoColumnTemplate")
	public String render(Node page, ModelMap model) throws RepositoryException {

		model.put("currentLink", MgnlContext.getAggregationState()
				.getMainContentNode().getPath());
		return "pages/pagetwocolumns.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		
	  	OptionBuilder[] colorBanner = {
	  			new OptionBuilder().value("").label("default"),
	  			new OptionBuilder().value("blue-background").label("blue")
	  	    	
	  	    	};
	  	
		tab.fields(
				cfg.fields.text("title").label("Title"),
				cfg.fields.checkbox("narrowcontent").buttonLabel("")
						.label("check to use narrow left column"),
						
				cfg.fields.select("bannerColor").options(colorBanner
						)
						.label("Select Banner Color"),
						
				cfg.fields.text("keyword").label("keywords")
						.description("The keywords about this page"),
				cfg.fields
						.checkbox("hideInNavigation")
						.buttonLabel("")
						.label("Hide in navigation")
						.description(
								"Check this box to hide this page in navigation"));
	}

	@Available
	public boolean isAvailable(Node websiteNode) {

		return TemplateUtil.checkAvailability(websiteNode);

	}

	// header images
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/pageTwoColumnTemplate/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Bar")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/pageTwoColumnTemplate/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/pageTwoColumnTemplate/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses({ TextComponent.class, RichTextComponent.class,
			MainContent.class })
	public static class LeftColumnArea {

		@RequestMapping("/pageTwoColumnTemplate/leftColumn")
		public String render(Node content) {

			return "areas/pagetwocolumn/leftcolumn_area.jsp";

		}

	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ RichTextComponent.class, PageNavigation.class })
	public static class ReftColumnArea {

		@RequestMapping("/pageTwoColumnTemplate/rightColumn")
		public String render() {
			return "areas/pagetwocolumn/rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class RightWidgetColumnArea {

		@RequestMapping("/pageTwoColumnTemplate/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		// @TabFactory("Content")
		// public void contentTab(UiConfig cfg, TabBuilder tab) {
		// Map<String, String> widgetCategories = new LinkedHashMap<String,
		// String>();
		// widgetCategories.put("Download List", "list");
		// widgetCategories.put("Connect Icons", "connect");
		//
		// tab.fields(cfg.fields.text("header").label("Title"),
		// cfg.fields.select("classname").label("Widget Type").options(widgetCategories.keySet()));
		//
		// }
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/pageTwoColumnTemplate/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/pageTwoColumnTemplate/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
}
