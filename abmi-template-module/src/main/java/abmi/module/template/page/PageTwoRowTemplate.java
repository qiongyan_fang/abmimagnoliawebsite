package abmi.module.template.page;


import javax.jcr.Node;
import javax.jcr.RepositoryException;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.DialogFactory;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.dialog.config.DialogBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.*;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;

import abmi.module.template.component.rightsidewidget.RightWidgetComponent;

import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "Two Rows Page", id = "abmiModule:pages/pagetworow", dialog = "frontpage-properties")
public class PageTwoRowTemplate {

	@DialogFactory("frontpage-properties")
	public void frontPageProperties(UiConfig cfg, DialogBuilder dialog) {
		dialog.form()
				.tabs(cfg.forms
						.tab("Properties")
						.label("Basic Page Information")
						.fields(cfg.fields.text("title").label("Title"),
								// cfg.fields.checkbox("narrowcontent").buttonLabel("").label("check to use narrow left column"),
								cfg.fields
										.checkbox("hideInNavigation")
										.label("Hide in navigation")
										.description(
												"Check this box to hide this page in navigation")),
						cfg.forms
								.tab("metadata")
								.label("Metadata")
								.fields(cfg.fields
										.text("metadata")
										.label("keywords")
										.description(
												"The keywords about this page")));
	}

	@RequestMapping("/pageTwoRowTemplate")
	public String render(Node page, ModelMap model) throws RepositoryException {

		model.put("currentLink", MgnlContext.getAggregationState()
				.getMainContentNode().getPath());
		return "pages/pagetworows.jsp";
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		tab.fields(
				cfg.fields.text("title").label("Title"),
				cfg.fields.text("keyword").label("keywords")
						.description("The keywords about this page"),
				cfg.fields
						.checkbox("hideInNavigation")
						.label("Hide in navigation")
						.description(
								"Check this box to hide this page in navigation"));
	}

	@Available
	public boolean isAvailable(Node websiteNode) {

		return TemplateUtil.checkAvailability(websiteNode);

	}

	// header images
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/pageTwoRowTemplate/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/pageTwoRowTemplate/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Bar")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/pageTwoRowTemplate/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses({ TextComponent.class, RichTextComponent.class,
			MainContent.class })
	public static class LeftColumnArea {

		@RequestMapping("/pageTwoRowTemplate/leftColumn")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

		// @TabFactory("Content")
		// public void contentTab(UiConfig cfg, TabBuilder tab) {
		// Map<String, String> categories = new LinkedHashMap<String, String>();
		// categories.put("All Projects", "project");
		// categories.put("Others", "connect");
		//
		// tab.fields(
		// cfg.fields.select("areaname").label("Choose an Area Type").options(categories.keySet()));
		//
		// }

	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ RichTextComponent.class, PageNavigation.class })
	public static class ReftColumnArea {

		@RequestMapping("/pageTwoRowTemplate/rightColumn")
		public String render() {
			return "areas/pagetwocolumn/rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class RightWidgetColumnArea {

		@RequestMapping("/pageTwoRowTemplate/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		// @TabFactory("Content")
		// public void contentTab(UiConfig cfg, TabBuilder tab) {
		// Map<String, String> widgetCategories = new LinkedHashMap<String,
		// String>();
		// widgetCategories.put("Download List", "list");
		// widgetCategories.put("Connect Icons", "connect");
		//
		// tab.fields(cfg.fields.text("header").label("Title"),
		// cfg.fields.select("classname").label("Widget Type").options(widgetCategories.keySet()));
		//
		// }
	}

	@Area(value = "bottomHeaderArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	public static class BottomHeaderArea {

		@RequestMapping("/pageTwoRowTemplate/bottomRowHeader")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("bottomRowBoxArea")
	@Controller
	@AvailableComponentClasses({ FullWidthContent.class, MainContent.class })
	public static class BottomRowBoxArea {

		@RequestMapping("/pageTwoRowTemplate/bottomRowBox")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/pageTwoRowTemplate/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/pageTwoRowTemplate/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
}
