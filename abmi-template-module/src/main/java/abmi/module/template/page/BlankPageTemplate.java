package abmi.module.template.page;

import java.io.IOException;

import java.util.LinkedHashMap;
import java.util.Map;

import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.MainContent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.dataanalytics.component.download.ProductRightMenuComponent;
import abmi.module.template.util.TemplateUtil;

/**
 * use this blank page, if you just want a root page, but with no content, when this blank page is clicked, it
 * will be redirect to the first child page.
 * 
 * @author Qiongyan
 *
 */
@Controller
@Template(title = "A Blank Page", id = "abmiModule:pages/blank" ,  dialog = "frontpage-properties")
public class BlankPageTemplate {

    @RequestMapping("/blankTemplate")
    public String render(Node page, ModelMap model,HttpServletResponse response) throws RepositoryException {

    	
    	 if (MgnlContext.getAggregationState().isPreviewMode()
//    			 && !Components.getComponent(ServerConfiguration.class).isAdmin() // public instance only
    			 
    			){
    		
				try {
					response.sendRedirect(TemplateUtil.getFirstChildPagePath(page)+ ".html");  
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
    	 }

        return "pages/pagetwocolumns.jsp";
    }
    
    @Available
  	public boolean isAvailable(Node websiteNode) {

  		
  		return TemplateUtil.checkAvailability(websiteNode);

  	}
    @Area(value="jumbotron", maxComponents=1)
    @Controller
    @Inherits(components = ComponentInheritanceMode.FILTERED)
    @AvailableComponentClasses({JumbotronComponent.class})
    
    public static class JumbotronArea {

        @RequestMapping("/blankTemplate/jumbotron")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }

    @Controller
    @Area(value = "megaMenuArea", title = "Top Navigation Bar")   
    @AvailableComponentClasses({MegaMenuAreaComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class topNavigationBarArea {

        @RequestMapping("/blankTemplate/topBar")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
	@Area(value="titleArea", maxComponents=1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class}) 
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/blankTemplate/title")
		public String render() {
			
			return "areas/title_area.jsp";
		}
	}
	
	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses({ PageNavigation.class,
			 MainContent.class })
	public static class LeftColumnArea {

		@RequestMapping("/blankTemplate/leftColumn")
		public String render(Node content) {
			
				return "areas/pagetwocolumn/leftcolumn_area.jsp";	
			
			
		}
		
	
		
	}

	
	
	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({  RichTextComponent.class,  PageNavigation.class,
		ProductRightMenuComponent.class
	 })
	public static class ReftColumnArea {

		@RequestMapping("/blankTemplate/rightColumn")
		public String render() {
			return "areas/pagetwocolumn/rightcolumn_area.jsp";
		}
	}

	
	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
		RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class RightWidgetColumnArea {

		@RequestMapping("/blankTemplate/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}
		
		@TabFactory("Content")
		public void contentTab(UiConfig cfg, TabBuilder tab) {
			 Map<String, String> widgetCategories = new LinkedHashMap<String, String>();
			 widgetCategories.put("Download List", "list");
			 widgetCategories.put("Connect Icons", "connect");
			 
			 tab.fields(cfg.fields.text("header").label("Title"), 
					   cfg.fields.select("classname").label("Widget Type").options(widgetCategories.keySet()));
					
		}
	}
    

	 @Controller
	    @Area(value = "footer", title = "Footer Menu", maxComponents=2)   
	    @AvailableComponentClasses({FooterAreaComponent.class})
	 @Inherits(components = ComponentInheritanceMode.ALL)
	    public static class FooterMenuArea {

	        @RequestMapping("/blankTemplate/footermenu")
	        public String render() {
	            return "areas/generalarea.jsp";
	        }
	    }
	    
	    @Controller
	    @Area(value = "finePrintFooter", title = "Fine Print Footer Links" , maxComponents=1)     
	    @AvailableComponentClasses({FinePrintFooterComponent.class})
	    @Inherits(components = ComponentInheritanceMode.ALL)
	    public static class FineFooterMenuArea {

	        @RequestMapping("/blankTemplate/finefootermenu")
	        public String render() {
	            return "areas/generalarea.jsp";
	        }
	    }
	
}
