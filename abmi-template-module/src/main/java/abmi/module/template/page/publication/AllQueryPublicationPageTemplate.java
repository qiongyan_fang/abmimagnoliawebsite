package abmi.module.template.page.publication;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.jackrabbit.value.ValueFactoryImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;

import abmi.model.util.ModuleParameters;
import abmi.model.util.CommonUtil;
import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.box.ProjectImageBox;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.util.NodeSort;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "All Publication Overview (Query) Template", id = "abmiModule:pages/allpublicationquery")
public class AllQueryPublicationPageTemplate {
	// static public SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
	// static SimpleDateFormat sdfull = new SimpleDateFormat("MMMM dd, yyyy");
	long totalCount = 0;

	@RequestMapping("/allpublicationquery")
	public String render(
			@RequestParam(value = "page", required = false) Integer pageNumber,
			@RequestParam(value = "mode", required = false) String listMode,
			@RequestParam(value = "time", required = false) String time,
			@RequestParam(value = "documenttype", required = false) String documentType,
			@RequestParam(value = "subject", required = false) String subject,
			@RequestParam(value = "keyword", required = false) String keyword,
			Node content, ModelMap model) throws RepositoryException {

		MgnlContext.getWebContext().getResponse()
				.setHeader("Cache-Control", "no-cache");

//		long start_time = System.nanoTime();
		ArrayList<NodeSort> publicationList = new ArrayList<NodeSort>();
		ArrayList<Map<String, Object>> selectedList = new ArrayList<Map<String, Object>>();

		if (pageNumber == null) {
			pageNumber = 1;
		}

		if (listMode == null
				|| (!"list".equalsIgnoreCase(listMode) && (!"calendar"
						.equalsIgnoreCase(listMode)))) {
			listMode = "detail";
		}

		int itemPerPage = 3;
		try {
			itemPerPage = Integer.parseInt(PropertyUtil.getString(content,
					listMode + "count", "3"));
		} catch (Exception e) {
		}

		int titleCount = 200000;
		int descriptionCount = 300000;
		try {
			titleCount = Integer.parseInt(PropertyUtil.getString(content,
					"titlecount", "200"));
		} catch (Exception e) {
		}

		try {
			descriptionCount = Integer.parseInt(PropertyUtil.getString(content,
					"descriptioncount", "300"));
		} catch (Exception e) {
		}

		int startCount = itemPerPage * (pageNumber - 1) + 1;
		int endCount = startCount + itemPerPage - 1;
		try {
			String path = PropertyUtil.getString(content, "rootpath",
					MgnlContext.getAggregationState().getCurrentContentNode()
							.getPath());

			NodeIterator nodeIter = null;

			ArrayList<String> allQueryNames = new ArrayList<String>(5);
			ArrayList<String> allQueryValues = new ArrayList<String>(5);
			if (keyword != null) {
				allQueryNames.add("dockeyword");
				allQueryValues.add(keyword);
			}
			if (subject != null) {
				allQueryNames.add("docsubject");
				allQueryValues.add(subject);

			}
			if (documentType != null) {
				allQueryNames.add("documenttype");
				allQueryValues.add(documentType);

			}
			if (time != null) {
				allQueryNames.add("displaydate");
				allQueryValues.add(time);

			}
			nodeIter = this.QueryPage(path, allQueryNames, allQueryValues,
					startCount, itemPerPage);

			int count = 1;
			ArrayList<String> subjectList = new ArrayList<String>();
			ArrayList<String> documentTypeList = new ArrayList<String>();
			ArrayList<Date> archiveNewsList = new ArrayList<Date>();

			// String month =
			// AllPublicationPageTemplate.sdf.format(calendar.getTime());
			int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			;
			boolean bCollectKeywords = !PropertyUtil.getBoolean(content,
					"isshowall", true);
			while (nodeIter != null && nodeIter.hasNext()
					&& (bCollectKeywords || count <= endCount)) {

				Node node = (Node) nodeIter.next();
				if (count >= startCount && count <= endCount) {
					selectedList.add(getPublicationInfo(path, node, listMode,
							descriptionCount, titleCount));
				}
				if (bCollectKeywords) {
					for (String row : CommonUtil.emptyIfNull(TemplateUtil
							.getMultiFieldJSONValues(node, "documenttype"))) {
						if (!documentTypeList.contains(row)) {

							documentTypeList.add(row);
						}
					}

					for (String row : CommonUtil.emptyIfNull(TemplateUtil
							.getMultiFieldJSONValues(node, "docsubject"))) {
						if (!subjectList.contains(row.trim())) {

							subjectList.add(row);
						}
					}

					Calendar calendar = PropertyUtil.getDate(node,
							"publishdate");
					int year = calendar.get(Calendar.YEAR);

				

					if (year == currentYear) {
						calendar.set(Calendar.DAY_OF_MONTH, 1);
					} else {
						
						calendar.set(Calendar.MONTH, 1);
						calendar.set(Calendar.DAY_OF_MONTH, 1);
					}

					Date curDate = calendar.getTime();

					if (!archiveNewsList.contains(curDate)) {

						archiveNewsList.add(curDate);
					}
				}

				count++;

			}
	

			if (bCollectKeywords) {
				Collections.sort(subjectList);
				Collections.sort(documentTypeList);

				model.put("subjectList", subjectList);
				model.put("documentType", documentTypeList);

				Collections.sort(archiveNewsList);
				ArrayList<String> archiveList = new ArrayList<String>();

				SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

				for (int i = archiveNewsList.size() - 1; i >= 0; i--) {
					Date curDate = archiveNewsList.get(i);

					String year = yearFormat.format(curDate);
					// System.out.println("year" + year + " " +
					// sdf.format(curDate));
					if (year.equals(currentYear + "")) {

						String monthYear = ModuleParameters.sdfmonthyear.format(curDate);
						if (!archiveList.contains(monthYear))
							archiveList.add(monthYear);
					} else {
						if (!archiveList.contains(year)) {
							archiveList.add(year);
						}
					}

				}

				model.put("archiveNews", archiveList);
			}

		} catch (Exception repError) {
			repError.printStackTrace();
		}

		model.put("currentLink", MgnlContext.getAggregationState()
				.getCurrentContentNode().getPath());

		model.put("page", pageNumber);
		model.put("totalPage",
				(int) (Math.ceil((double) totalCount / (double) itemPerPage)));
		model.put("mode", listMode);

		model.put("publicationList", selectedList);

//		long end_time = System.nanoTime();
//		double difference = (end_time - start_time) / 1e6;

		return "pages/allpublicationpage.jsp";
	}

	/*
	 * SELECT parent.* FROM [mgnl:page] AS parent INNER JOIN [mgnl:contentNode]
	 * AS child ON ISCHILDNODE(child,parent) WHERE child.documenttype = 'test
	 * value'
	 * 
	 * select * from [mgnl:page] as t where
	 * ISDESCENDANTNODE([/home/publications/]) and (hideInNavigation is null or
	 * hideInNavigation = false)
	 */

	NodeIterator QueryPage(String rootPath, ArrayList<String> allQueryName,
			ArrayList<String> allQueryValue, int startCount, int itemPerPage) {

		try {
			Session session = MgnlContext.getJCRSession("website");
			Calendar cal = Calendar.getInstance();
			String todayStr = ValueFactoryImpl.getInstance().createValue(cal)
					.getString();

			String countQueryString = "select * from [mgnl:page] as t where  (hideInNavigation is null or hideInNavigation = false) and "
					+ " publishdate <= cast('" + todayStr + "' as date) and " + /*
																				 * after
																				 * publishdate
																				 */
					"ISDESCENDANTNODE([" + rootPath + "])";

			for (int i = 0; allQueryName != null && i < allQueryName.size(); i++) {
				String queryName = allQueryName.get(i);
				String queryValue = allQueryValue.get(i);

				if (queryName != null) {
					if (queryName.equalsIgnoreCase("displaydate")) {
						String newQueryValue = queryValue;
						if (queryValue.indexOf(" ") != -1) {
							newQueryValue = queryValue.replace(" ", "%");
						} else
							newQueryValue = "%" + queryValue;
						countQueryString += " and (" + queryName + " like '"
								+ newQueryValue + "')";
					} else {
						countQueryString += " and (" + queryName + " like '%"
								+ queryValue + ",%'" + " or " + queryName
								+ " like '%" + queryValue + "')";
					}
				}
			}

			QueryManager manager = session.getWorkspace().getQueryManager();
			javax.jcr.query.Query countQuery = manager.createQuery(
					countQueryString, javax.jcr.query.Query.JCR_SQL2);
			QueryResult countResult = countQuery.execute();
			totalCount = countResult.getNodes().getSize();
			// System.out.println("count returns:"
			// + totalCount);

			String queryString = countQueryString += " order by publishdate desc, title";

//			System.out.println(queryString);
			javax.jcr.query.Query query = manager.createQuery(queryString,
					javax.jcr.query.Query.JCR_SQL2);

			// query2.setOffset(startCount); not work
			// query2.setLimit(itemPerPage);
			QueryResult result = query.execute();

			NodeIterator resultsIter = result.getNodes();
			// System.out.println("position = " + resultsIter.getPosition() +
			// " has next?" + resultsIter.hasNext());
			// while (resultsIter != null && resultsIter.hasNext()) {
			// Node node = (Node) resultsIter.next();
			//
			// System.out.println("node = " + node.getPath());
			// }
			//
			
			/*
			javax.jcr.query.Query queryTest = manager.createQuery(queryString,
					javax.jcr.query.Query.JCR_SQL2);
			((QueryImpl) queryTest).setOffset(startCount);
			((QueryImpl) queryTest).setLimit(itemPerPage);
			QueryResult result3 = queryTest.execute();

			NodeIterator resultsIter3 = result3.getNodes();
			System.out.println("position = " + resultsIter3.getPosition()
					+ " has next?" + resultsIter3.hasNext());
					
					*/
			// while (resultsIter3 != null && resultsIter3.hasNext()) {
			// Node node = (Node) resultsIter3.next();
			//
			// System.out.println("node = " + node.getPath());
			// }

			return resultsIter;
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/*
	void loopNodes(Node node, String templateName, String listMode,
			ArrayList<NodeSort> publicationList, String time,
			String documentType, String subject, String keyword)
			throws RepositoryException {
		for (Node subNode : NodeUtil.getNodes(node, NodeTypes.Page.NAME)) {
			if (!PropertyUtil.getBoolean(subNode, "hideInNavigation", false)
			// && !PropertyUtil.getBoolean(subNode, "isevent", false)
			) {

				if (MetaDataUtil.getTemplate(subNode).endsWith(templateName)) {
					try {

						Calendar calendar = PropertyUtil.getDate(subNode,
								"publishdate");

						// only display the items that has a publish date
						// earlier than today
						if (Calendar.getInstance().compareTo(calendar) >= 0) {

							int year = calendar.get(Calendar.YEAR);

							String month = ModuleConfig.sdfmonthyear.format(calendar
									.getTime());

							if (time != null && time.length() > 0) { // if query
																		// by
																		// time
								if (!time.equals(month)
										&& !time.equals("" + year)) {
									continue; // no match ? skip
								}
							}

							if (documentType != null
									&& documentType.length() > 0) { // if query
																	// by
																	// documenttype
								if (!TemplateUtil.getMultiFieldJSONValues(
										subNode, "documenttype").contains(
										documentType)) {
									continue; // no match ? skip
								}
							}

							if (subject != null && subject.length() > 0) { // if
																			// query
																			// by
																			// subject
								if (!TemplateUtil.getMultiFieldJSONValues(
										subNode, "docsubject")
										.contains(subject)) {
									continue; // no match ? skip
								}
							}

							if (keyword != null && keyword.length() > 0) { // if
																			// query
																			// by
																			// keyword
								if (!TemplateUtil.getMultiFieldJSONValues(
										subNode, "dockeyword")
										.contains(keyword)) {
									continue; // no match ? skip
								}
							}

							// all matched? add in
							publicationList.add(new NodeSort(subNode));
							// System.out.println("matched path =" +
							// subNode.getPath());
							/*
							 * if (time == null || time.equals("") ||
							 * (time.equals(month) || time.equals("" + year))) {
							 * String curTags = PropertyUtil.getString( subNode,
							 * "subject", ""); System.out.println("subject =" +
							 * TemplateUtil.getMultiFieldValues(subNode,
							 * "subject")); if (keyword == null ||
							 * keyword.equals("") || (curTags != null && ("," +
							 * curTags.replaceAll(" ", "") .toLowerCase() + ",")
							 * .indexOf("," + keyword.replaceAll( " ", "")
							 * .toLowerCase() + ",") != -1)) {
							 * publicationList.add(new NodeSort(subNode)); }
							 * 
							 * }
							 *
						}
						// System.out.println("totalCount =" + totalCount);
					} catch (Exception e) {
					}
				} // end of if itis a valid page

				loopNodes(subNode, templateName, listMode, publicationList,
						time, documentType, subject, keyword);

			}
		}

		// System.out.println("publicationList=" + publicationList.size());
	}
	
	*/

	/**
	 * 
	 * @param subNode
	 * @param mode
	 *            detail: image, author, date, links, title, abstract, subtitle
	 *            ?, tags list: title, links
	 * @return
	 */

	public static LinkedHashMap<String, Object> getPublicationInfo(
			String rootPath, Node subNode, String mode, int descriptionCount,
			int titleCount) {
		LinkedHashMap<String, Object> singleProject = new LinkedHashMap<String, Object>();

		singleProject.put("doctitle", TemplateUtil.getCharByNumber(
				PropertyUtil.getString(subNode, "title", ""), titleCount));
		
		singleProject.put("author",
				PropertyUtil.getString(subNode, "author", ""));

		try{
		singleProject.put("displaydate",
				PropertyUtil.getString(subNode, "displaydate", ModuleParameters.sdfull.format(PropertyUtil.getDate(subNode, "publishdate").getTime())));
		}
		catch(Exception e) {e.printStackTrace();}
		
		singleProject.put("versioncount",
				PropertyUtil.getLong(subNode, "versioncount", 0L));
		
		 singleProject.put("suppdocslink", TemplateUtil.getSupplementalFieldValues(subNode,
				  "supplementalreportslink"));
		 
		 String doi = PropertyUtil.getString(subNode, "doi", "");
			if ("".equals(doi)) {
				String link = PropertyUtil.getString(subNode, "viewlink", "");
				singleProject.put("viewlink", link);

				Map<String, String> fileProperties = TemplateUtil
						.getAssetsProperty(link);
				if (fileProperties != null) {
					singleProject.put("extension",
							fileProperties.get("extension"));
					singleProject.put("size", fileProperties.get("size"));
				}
			} else {
				singleProject.put("doi", doi);
			}
		 
		
			String flipbookLink = PropertyUtil.getString(subNode, "flipbooklink", "");
			String flipbookLinkText = PropertyUtil.getString(subNode, "flipbooklinktext", "");
			singleProject.put("flipbookLink", flipbookLink);
			singleProject.put("flipbookLinkText", flipbookLinkText);
			
			// extra information for detailed publication views
		if ("detail".equals(mode)) {
			singleProject.put("imagetitle",
					PropertyUtil.getString(subNode, "imagetitle", ""));

		

			String coverImageUrl = PropertyUtil.getString(subNode,
					"coverImageUrl", "");
			singleProject.put("coverImageUrl", coverImageUrl);

			singleProject.put("imageUrl",
					PropertyUtil.getString(subNode, "imageUrl", ""));

			singleProject.put("description", TemplateUtil.getCharByNumber(
					PropertyUtil.getString(subNode, "description", ""),
					descriptionCount));

			

			singleProject.put("hasMultiversion",
					PropertyUtil.getBoolean(subNode, "ismultiversion", false));

		
			
		
			/*
			 * ArrayList<String> documentType = TemplateUtil
			 * .getMultiFieldJSONValues(subNode, "documenttype");
			 * singleProject.put("documenttype", documentType);
			 * 
			 * ArrayList<String> docSubject = TemplateUtil
			 * .getMultiFieldJSONValues(subNode, "docsubject");
			 * singleProject.put("docsubject", docSubject); ArrayList<String>
			 * dockeyword = TemplateUtil .getMultiFieldJSONValues(subNode,
			 * "dockeyword"); singleProject.put("dockeyword", dockeyword);
			 * 
			 * ArrayList<String[]> docs = new ArrayList<String[]>(); try { for
			 * (Node docNode : NodeUtil.getNodes(subNode
			 * .getNode("supplementalreports"))) { String[] supDoc = new
			 * String[3]; supDoc[0] =
			 * docNode.getProperty("subdoctitle").getString(); supDoc[1] =
			 * docNode.getProperty("subdocviewlink") .getString();
			 * 
			 * docs.add(supDoc); }
			 * 
			 * singleProject.put("suppdocs", docs); } catch (Exception e) { //
			 * System.out.println("loop " + e.getMessage()); }
			 * 
			 * singleProject.put("suppdocslink", TemplateUtil
			 * .getSupplementalFieldValues(rootPath, subNode,
			 * "supplementalreportslink"));
			 */
		} 
//		else if ("list".equals(mode)) {
//			singleProject.put("doctitle",
//					PropertyUtil.getString(subNode, "title", ""));
//
//			singleProject.put("viewlink",
//					PropertyUtil.getString(subNode, "viewlink", ""));
//
//		}

		try {
			singleProject.put("parentNodeName",
					PropertyUtil.getString(subNode.getParent(), "name", ""));
		} catch (Exception e) {

		}

		try {
			singleProject.put("path", subNode.getPath());
		} catch (Exception e) {

		}

		return singleProject;
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				cfg.fields.text("rootpath").label("Root Path"),

				cfg.fields.text("detailcount")
						.label("Number of detailed Items").defaultValue("10")
						.description("Per Page"),
				cfg.fields.text("listcount").label("Number of list items")
						.defaultValue("12"),
				cfg.fields.link("placeholderimg").label("Default Image")
						.appName("assets").targetWorkspace("dam"),
				cfg.fields
						.text("titlecount")
						.label("Publication Title characters")
						.defaultValue("200")
						.description(
								"how many characters can a publication title have"),

				cfg.fields
						.text("descriptioncount")
						.label("Publication Description characters")
						.defaultValue("300")
						.description(
								"how many characters can a publication description have?"),

				cfg.fields
						.checkbox("isshowall")
						.buttonLabel("")
						.label("Show All Categories")
						.description(
								"Show All Categories even with no matching documents? also need to configure on right side widget")
						.defaultValue("false"),
				cfg.fields.checkbox("hideInNavigation").buttonLabel("")
						.label("Hide in navigation"),
				cfg.fields.text("keyword").label("keywords")
						.description("The keywords about this page"));

	}

	// header images
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/allpublicationquery/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/allpublicationquery/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Menu")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/allpublicationquery/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("topLeftColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class })
	public static class TopLeftColumnArea {

		@RequestMapping("/allpublicationquery/leftColumn1")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("bottomLeftColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class })
	public static class DownLeftColumnArea {

		@RequestMapping("/allpublicationquery/leftColumn2")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("projectBoxArea")
	@Controller
	@AvailableComponentClasses({ ProjectImageBox.class })
	public static class ProjectArea {

		@RequestMapping("/allpublicationquery/leftColumnPrjBox")
		public String render(Node content) {

			return "areas/allproject_area.jsp";
		}

	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ RichTextComponent.class, PageNavigation.class })
	public static class LeftColumnArea {

		@RequestMapping("/allpublicationquery/rightColumn")
		public String render() {
			return "areas/publication_rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class RightWidgetColumnArea {

		@RequestMapping("/allpublicationquery/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		// @TabFactory("Content")
		// public void contentTab(UiConfig cfg, TabBuilder tab) {
		// Map<String, String> widgetCategories = new LinkedHashMap<String,
		// String>();
		// widgetCategories.put("Download List", "list");
		// widgetCategories.put("Connect Icons", "connect");
		//
		// tab.fields(cfg.fields.text("header").label("Title"),
		// cfg.fields.select("classname").label("Widget Type")
		// .options(widgetCategories.keySet()));
		//
		// }
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/allpublicationquery/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/allpublicationquery/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
	
    @Available
	public boolean isAvailable(Node websiteNode) {

		
		return TemplateUtil.checkAvailability(websiteNode);

	}
}
