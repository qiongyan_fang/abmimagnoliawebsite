package abmi.module.template.page.publication;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.objectfactory.Components;
import info.magnolia.templating.functions.TemplatingFunctions;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueChildNodeTransformer;
import info.magnolia.ui.framework.config.UiConfig;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.jackrabbit.value.ValueFactoryImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import abmi.model.util.ModuleParameters;
import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.box.ProjectImageBox;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "All Publication Overview (Ajax) Template", id = "abmiModule:pages/newpublicationquery")
public class NewQueryPublicationPageTemplate {
	// static public SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
	// static SimpleDateFormat sdfull = new SimpleDateFormat("MMMM dd, yyyy");
	long totalCount = 0;

	@RequestMapping("/newpublicationquery")
	public String render(Node content, ModelMap model)
			throws RepositoryException {

		MgnlContext.getWebContext().getResponse()
				.setHeader("Cache-Control", "no-cache");

		long start_time = System.nanoTime();

		String path = PropertyUtil.getString(content, "rootpath", MgnlContext
				.getAggregationState().getCurrentContentNode().getPath());

		model.put("currentLink", MgnlContext.getAggregationState()
				.getCurrentContentNode().getPath());

		if (Components.getComponent(TemplatingFunctions.class)
				.isAuthorInstance() &&
				Components.getComponent(TemplatingFunctions.class)
				.isEditMode()
				) {
			QueryManager manager = content.getSession().getWorkspace()
					.getQueryManager();

			String queryString = "select * from [mgnl:page] as t where ISDESCENDANTNODE(["
					+ path + "]) and isFeature = true";
			javax.jcr.query.Query query = manager.createQuery(queryString,
					javax.jcr.query.Query.JCR_SQL2);

			QueryResult result = query.execute();

			NodeIterator nodeIter = result.getNodes();
			while (nodeIter != null && nodeIter.hasNext()) {

				Node node = (Node) nodeIter.next();
				PropertyUtil.setProperty(
						content.getSession().getNode(node.getPath()),
						"isFeatured", false);
			}
			model.put("featured",
					TemplateUtil.getMultiFieldValues(content, "featured"));
			ArrayList<String> featuredPath = TemplateUtil.getMultiFieldValues(
					content.getSession().getNode(path), "featured");
			for (String itemPath : TemplateUtil.emptyIfNull(featuredPath)) {
				PropertyUtil.setProperty(
						content.getSession().getNode(itemPath), "isFeatured",
						true);

			}
			content.getSession().save();
			System.out.println("updated featured");  
		}
		return "pages/allpublicationpage.jsp";
	}

	/*
	 * SELECT parent.* FROM [mgnl:page] AS parent INNER JOIN [mgnl:contentNode]
	 * AS child ON ISCHILDNODE(child,parent) WHERE child.documenttype = 'test
	 * value'
	 * 
	 * select * from [mgnl:page] as t where
	 * ISDESCENDANTNODE([/home/publications/]) and (hideInNavigation is null or
	 * hideInNavigation = false)
	 */

	NodeIterator QueryPage(String rootPath, ArrayList<String> allQueryName,
			ArrayList<String> allQueryValue, int startCount, int itemPerPage) {

		try {
			Session session = MgnlContext.getJCRSession("website");
			Calendar cal = Calendar.getInstance();
			String todayStr = ValueFactoryImpl.getInstance().createValue(cal)
					.getString();

			String countQueryString = "select * from [mgnl:page] as t where  (hideInNavigation is null or hideInNavigation = false) and "
					+ " publishdate <= cast('" + todayStr + "' as date) and " + /*
																				 * after
																				 * publishdate
																				 */
					"ISDESCENDANTNODE([" + rootPath + "])";

			for (int i = 0; allQueryName != null && i < allQueryName.size(); i++) {
				String queryName = allQueryName.get(i);
				String queryValue = allQueryValue.get(i);

				if (queryName != null) {
					if (queryName.equalsIgnoreCase("displaydate")) {
						String newQueryValue = queryValue;
						if (queryValue.indexOf(" ") != -1) {
							newQueryValue = queryValue.replace(" ", "%");
						} else
							newQueryValue = "%" + queryValue;
						countQueryString += " and (" + queryName + " like '"
								+ newQueryValue + "')";
					} else {
						countQueryString += " and (" + queryName + " like '%"
								+ queryValue + ",%'" + " or " + queryName
								+ " like '%" + queryValue + "')";
					}
				}
			}

			QueryManager manager = session.getWorkspace().getQueryManager();
			javax.jcr.query.Query countQuery = manager.createQuery(
					countQueryString, javax.jcr.query.Query.JCR_SQL2);
			QueryResult countResult = countQuery.execute();
			totalCount = countResult.getNodes().getSize();
			// System.out.println("count returns:"
			// + totalCount);

			String queryString = countQueryString += " order by publishdate desc, title";

			// System.out.println(queryString);
			javax.jcr.query.Query query = manager.createQuery(queryString,
					javax.jcr.query.Query.JCR_SQL2);

			// query2.setOffset(startCount); not work
			// query2.setLimit(itemPerPage);
			QueryResult result = query.execute();

			NodeIterator resultsIter = result.getNodes();
			// System.out.println("position = " + resultsIter.getPosition() +
			// " has next?" + resultsIter.hasNext());
			// while (resultsIter != null && resultsIter.hasNext()) {
			// Node node = (Node) resultsIter.next();
			//
			// System.out.println("node = " + node.getPath());
			// }
			//

			/*
			 * javax.jcr.query.Query queryTest =
			 * manager.createQuery(queryString, javax.jcr.query.Query.JCR_SQL2);
			 * ((QueryImpl) queryTest).setOffset(startCount); ((QueryImpl)
			 * queryTest).setLimit(itemPerPage); QueryResult result3 =
			 * queryTest.execute();
			 * 
			 * NodeIterator resultsIter3 = result3.getNodes();
			 * System.out.println("position = " + resultsIter3.getPosition() +
			 * " has next?" + resultsIter3.hasNext());
			 */
			// while (resultsIter3 != null && resultsIter3.hasNext()) {
			// Node node = (Node) resultsIter3.next();
			//
			// System.out.println("node = " + node.getPath());
			// }

			return resultsIter;
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/*
	 * void loopNodes(Node node, String templateName, String listMode,
	 * ArrayList<NodeSort> publicationList, String time, String documentType,
	 * String subject, String keyword) throws RepositoryException { for (Node
	 * subNode : NodeUtil.getNodes(node, NodeTypes.Page.NAME)) { if
	 * (!PropertyUtil.getBoolean(subNode, "hideInNavigation", false) // &&
	 * !PropertyUtil.getBoolean(subNode, "isevent", false) ) {
	 * 
	 * if (MetaDataUtil.getTemplate(subNode).endsWith(templateName)) { try {
	 * 
	 * Calendar calendar = PropertyUtil.getDate(subNode, "publishdate");
	 * 
	 * // only display the items that has a publish date // earlier than today
	 * if (Calendar.getInstance().compareTo(calendar) >= 0) {
	 * 
	 * int year = calendar.get(Calendar.YEAR);
	 * 
	 * String month = ModuleConfig.sdfmonthyear.format(calendar .getTime());
	 * 
	 * if (time != null && time.length() > 0) { // if query // by // time if
	 * (!time.equals(month) && !time.equals("" + year)) { continue; // no match
	 * ? skip } }
	 * 
	 * if (documentType != null && documentType.length() > 0) { // if query //
	 * by // documenttype if (!TemplateUtil.getMultiFieldJSONValues( subNode,
	 * "documenttype").contains( documentType)) { continue; // no match ? skip }
	 * }
	 * 
	 * if (subject != null && subject.length() > 0) { // if // query // by //
	 * subject if (!TemplateUtil.getMultiFieldJSONValues( subNode, "docsubject")
	 * .contains(subject)) { continue; // no match ? skip } }
	 * 
	 * if (keyword != null && keyword.length() > 0) { // if // query // by //
	 * keyword if (!TemplateUtil.getMultiFieldJSONValues( subNode, "dockeyword")
	 * .contains(keyword)) { continue; // no match ? skip } }
	 * 
	 * // all matched? add in publicationList.add(new NodeSort(subNode)); //
	 * System.out.println("matched path =" + // subNode.getPath()); /* if (time
	 * == null || time.equals("") || (time.equals(month) || time.equals("" +
	 * year))) { String curTags = PropertyUtil.getString( subNode, "subject",
	 * ""); System.out.println("subject =" +
	 * TemplateUtil.getMultiFieldValues(subNode, "subject")); if (keyword ==
	 * null || keyword.equals("") || (curTags != null && ("," +
	 * curTags.replaceAll(" ", "") .toLowerCase() + ",") .indexOf("," +
	 * keyword.replaceAll( " ", "") .toLowerCase() + ",") != -1)) {
	 * publicationList.add(new NodeSort(subNode)); }
	 * 
	 * }
	 * 
	 * } // System.out.println("totalCount =" + totalCount); } catch (Exception
	 * e) { } } // end of if itis a valid page
	 * 
	 * loopNodes(subNode, templateName, listMode, publicationList, time,
	 * documentType, subject, keyword);
	 * 
	 * } }
	 * 
	 * // System.out.println("publicationList=" + publicationList.size()); }
	 */

	/**
	 * 
	 * @param subNode
	 * @param mode
	 *            detail: image, author, date, links, title, abstract, subtitle
	 *            ?, tags list: title, links
	 * @return
	 */

	public static LinkedHashMap<String, Object> getPublicationInfo(
			String rootPath, Node subNode, String mode, int descriptionCount,
			int titleCount) {
		LinkedHashMap<String, Object> singleProject = new LinkedHashMap<String, Object>();

		singleProject.put("doctitle", TemplateUtil.getCharByNumber(
				PropertyUtil.getString(subNode, "title", ""), titleCount));

		singleProject.put("author",
				PropertyUtil.getString(subNode, "author", ""));

		try {
			singleProject.put("displaydate", PropertyUtil.getString(
					subNode,
					"displaydate",
					ModuleParameters.sdfull.format(PropertyUtil.getDate(
							subNode, "publishdate").getTime())));
		} catch (Exception e) {
			e.printStackTrace();
		}

		singleProject.put("versioncount",
				PropertyUtil.getLong(subNode, "versioncount", 0L));

		singleProject
				.put("suppdocslink", TemplateUtil.getSupplementalFieldValues(
						subNode, "supplementalreportslink"));

		String doi = PropertyUtil.getString(subNode, "doi", "");
		if ("".equals(doi)) {
			String link = PropertyUtil.getString(subNode, "viewlink", "");
			singleProject.put("viewlink", link);

			Map<String, String> fileProperties = TemplateUtil
					.getAssetsProperty(link);
			if (fileProperties != null) {
				singleProject.put("extension", fileProperties.get("extension"));
				singleProject.put("size", fileProperties.get("size"));
			}
		} else {
			singleProject.put("doi", doi);
		}

		String flipbookLink = PropertyUtil.getString(subNode, "flipbooklink",
				"");
		String flipbookLinkText = PropertyUtil.getString(subNode,
				"flipbooklinktext", "");
		singleProject.put("flipbookLink", flipbookLink);
		singleProject.put("flipbookLinkText", flipbookLinkText);

		// extra information for detailed publication views
		if ("detail".equals(mode)) {
			singleProject.put("imagetitle",
					PropertyUtil.getString(subNode, "imagetitle", ""));

			String coverImageUrl = PropertyUtil.getString(subNode,
					"coverImageUrl", "");
			singleProject.put("coverImageUrl", coverImageUrl);

			singleProject.put("imageUrl",
					PropertyUtil.getString(subNode, "imageUrl", ""));

			singleProject.put("description", TemplateUtil.getCharByNumber(
					PropertyUtil.getString(subNode, "description", ""),
					descriptionCount));

			singleProject.put("hasMultiversion",
					PropertyUtil.getBoolean(subNode, "ismultiversion", false));

			/*
			 * ArrayList<String> documentType = TemplateUtil
			 * .getMultiFieldJSONValues(subNode, "documenttype");
			 * singleProject.put("documenttype", documentType);
			 * 
			 * ArrayList<String> docSubject = TemplateUtil
			 * .getMultiFieldJSONValues(subNode, "docsubject");
			 * singleProject.put("docsubject", docSubject); ArrayList<String>
			 * dockeyword = TemplateUtil .getMultiFieldJSONValues(subNode,
			 * "dockeyword"); singleProject.put("dockeyword", dockeyword);
			 * 
			 * ArrayList<String[]> docs = new ArrayList<String[]>(); try { for
			 * (Node docNode : NodeUtil.getNodes(subNode
			 * .getNode("supplementalreports"))) { String[] supDoc = new
			 * String[3]; supDoc[0] =
			 * docNode.getProperty("subdoctitle").getString(); supDoc[1] =
			 * docNode.getProperty("subdocviewlink") .getString();
			 * 
			 * docs.add(supDoc); }
			 * 
			 * singleProject.put("suppdocs", docs); } catch (Exception e) { //
			 * System.out.println("loop " + e.getMessage()); }
			 * 
			 * singleProject.put("suppdocslink", TemplateUtil
			 * .getSupplementalFieldValues(rootPath, subNode,
			 * "supplementalreportslink"));
			 */
		}
		// else if ("list".equals(mode)) {
		// singleProject.put("doctitle",
		// PropertyUtil.getString(subNode, "title", ""));
		//
		// singleProject.put("viewlink",
		// PropertyUtil.getString(subNode, "viewlink", ""));
		//
		// }

		try {
			singleProject.put("parentNodeName",
					PropertyUtil.getString(subNode.getParent(), "name", ""));
		} catch (Exception e) {

		}

		try {
			singleProject.put("path", subNode.getPath());
		} catch (Exception e) {

		}

		return singleProject;
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				cfg.fields.text("rootpath").label("Root Path"),

				cfg.fields.text("detailcount")
						.label("Number of detailed Items").defaultValue("10")
						.description("Per Page"),
				cfg.fields.text("listcount").label("Number of list items")
						.defaultValue("12"),
				cfg.fields.link("placeholderimg").label("Default Image")
						.appName("assets").targetWorkspace("dam"),
				cfg.fields
						.text("titlecount")
						.label("Publication Title characters")
						.defaultValue("200")
						.description(
								"how many characters can a publication title have"),

				cfg.fields
						.text("descriptioncount")
						.label("Publication Description characters")
						.defaultValue("300")
						.description(
								"how many characters can a publication description have?"),

				/*
				 * cfg.fields .checkbox("isshowall") .buttonLabel("")
				 * .label("Show All Categories") .description(
				 * "Show All Categories even with no matching documents? also need to configure on right side widget"
				 * ) .defaultValue("false"),
				 */

				cfg.fields
						.multi("featured")
						.label("featured publications")
						.field(cfg.fields.link("docpage")
								.label("Document Page").appName("pages")
								.targetWorkspace("website"))
						.transformerClass(MultiValueChildNodeTransformer.class),
				cfg.fields.checkbox("hideInNavigation").buttonLabel("")
						.label("Hide in navigation"),
				cfg.fields.text("keyword").label("keywords")
						.description("The keywords about this page"));

	}

	// header images
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/newpublicationquery/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/newpublicationquery/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Menu")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/newpublicationquery/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("topLeftColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class })
	public static class TopLeftColumnArea {

		@RequestMapping("/newpublicationquery/leftColumn1")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("bottomLeftColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class })
	public static class DownLeftColumnArea {

		@RequestMapping("/newpublicationquery/leftColumn2")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("projectBoxArea")
	@Controller
	@AvailableComponentClasses({ ProjectImageBox.class })
	public static class ProjectArea {

		@RequestMapping("/newpublicationquery/leftColumnPrjBox")
		public String render(Node content) {

			return "areas/allproject_area.jsp";
		}

	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ RichTextComponent.class, PageNavigation.class })
	public static class LeftColumnArea {

		@RequestMapping("/newpublicationquery/rightColumn")
		public String render() {
			return "areas/publication_rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class RightWidgetColumnArea {

		@RequestMapping("/newpublicationquery/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		// @TabFactory("Content")
		// public void contentTab(UiConfig cfg, TabBuilder tab) {
		// Map<String, String> widgetCategories = new LinkedHashMap<String,
		// String>();
		// widgetCategories.put("Download List", "list");
		// widgetCategories.put("Connect Icons", "connect");
		//
		// tab.fields(cfg.fields.text("header").label("Title"),
		// cfg.fields.select("classname").label("Widget Type")
		// .options(widgetCategories.keySet()));
		//
		// }
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/newpublicationquery/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/newpublicationquery/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Available
	public boolean isAvailable(Node websiteNode) {

		return TemplateUtil.checkAvailability(websiteNode);

	}
}
