package abmi.module.template.page;



import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import abmi.module.template.component.JumbotronComponent;

import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.SubPageListComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

/**
 * Template with two columns, a main content area and a right side column.
 */
@Controller
//@Template(title = "Team member overview Templage", id = "abmiModule:pages/allteammember")
public class ZAllTeamMemberPageTemplate {


    @RequestMapping("/allteammember")
    public String render(Node content, ModelMap model) throws RepositoryException {
    	
		
        String path = null;
        
        try{
        	content.getProperty("rootpath").getString();
        }catch(Exception e){
//        	System.out.println("rootpath= " + e.getMessage());
        	path = "/home/about-us1/boardmembers";
        }
        
        if (path==null || "".equals(path.trim())){
        	path = "/home/aboutus/boardmembers";
        }
        
       
      
        ArrayList< Map<String, String>> memberList = new   ArrayList<Map<String, String>>();
      
        for (Node node : NodeUtil.getNodes(content.getSession().getNode(path), NodeTypes.Page.NAME)) {
            if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)
            		
            		) {
            	LinkedHashMap<String, String> singleMember = new LinkedHashMap<String, String>();
            	
            	singleMember.put("photo", PropertyUtil.getString(node, "photo", ""));
            	singleMember.put("personname", PropertyUtil.getString(node, "personname", PropertyUtil.getString(node, "title", "")));
            	singleMember.put("position", PropertyUtil.getString(node, "position", ""));
            	singleMember.put("otherposition", PropertyUtil.getString(node, "otherposition", ""));   
            	singleMember.put("path", node.getPath());
            	
            	memberList.add(singleMember);
            }
        }
        
        model.put("members", memberList);
    	model.put("currentLink", MgnlContext.getAggregationState().getCurrentContentNode().getPath());
        return "pages/allteammember.jsp";
    }

    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
        tab.fields(
                cfg.fields.text("title").label("Title"),
                cfg.fields.link("rootpath").label("Link Url").appName("pages").targetWorkspace("website"),
                cfg.fields.checkbox("hideInNavigation").label("Hide in navigation").description("Check this box to hide this page in navigation")
        );
    }
    
    
    /**
     * jumbotron area.
     */
    @Area(value="jumbotron", maxComponents=1)
    @Controller
    @AvailableComponentClasses({JumbotronComponent.class})
    
    public static class JumbotronArea {

        @RequestMapping("/allteammember/teammemberjumbotron")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "megaMenuArea", title = "Top Navigation Menu")   
    @AvailableComponentClasses({MegaMenuAreaComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class topNavigationBarArea {

        @RequestMapping("/allteammember/topBar")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    

	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses({  RichTextComponent.class
			 })
	public static class LeftColumnArea {

		@RequestMapping("/allteammember/leftColumn")
		public String render() {
			return "areas/pagetwocolumn/leftcolumn_area.jsp";
		}
	}

	@Area("memberRightColumnArea")
	@Controller
	 
	@AvailableComponentClasses({ RichTextComponent.class, SubPageListComponent.class
			})
	public static class RightColumnArea {

		@RequestMapping("/allteammember/rightColumn")
		public String render() {
			return "areas/subpagelist_area.jsp";
		}
		
		 @TabFactory("Content")
		    public void contentTab(UiConfig cfg, TabBuilder tab) {
		        tab.fields(
		                cfg.fields.text("header").label("Header Text").defaultValue("Board of Directors")	                
		              
		        );
	}
	}
	
	@Area("memberWidgetArea")
	@Controller
	 
	@AvailableComponentClasses({ TextComponent.class, abmi.module.template.component.pagelist.SubPageListComponent.class,
		RightWidgetComponent.class})
	public static class WidgettColumnArea {

		@RequestMapping("/allteammember/rightColumnWidget")
		public String render() {
			return "areas/subpagelist_area.jsp";
		}
		
		 @TabFactory("Content")
		    public void contentTab(UiConfig cfg, TabBuilder tab) {
		        tab.fields(
		                cfg.fields.text("header").label("Header Text").defaultValue("Downloads")	                
		              
		        );
	}
	}
    
    }
