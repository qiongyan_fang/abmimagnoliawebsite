package abmi.module.template.page;

import java.util.LinkedHashMap;
import java.util.Map;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.MainContent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.util.TemplateUtil;


@Controller
@Template(title = "Job Page", id = "abmiModule:pages/jobpost")
public class JobPageTemplate {

   

    @RequestMapping("/jobpageTemplate")
    public String render(@RequestParam(value="id", required=false) Integer id, Node content,ModelMap model)throws RepositoryException {


    	if (!PropertyUtil.getBoolean(content, "showparent", true)){
    		model.put("currentLink", MgnlContext.getAggregationState().getMainContentNode().getPath());
    	}
    	else{
    		model.put("currentLink", MgnlContext.getAggregationState().getMainContentNode().getParent().getPath());
    	}
    	
    	return "pages/jobpage.jsp";
    }

    @Available
	public boolean isAvailable(Node websiteNode) {

		
		return TemplateUtil.checkAvailability(websiteNode);

	}
    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab) {
    	
           
        tab.fields(
                
                cfg.fields.text("position").label("Job Position"),
                cfg.fields.date("deadline").label("Deadline"),
                
                cfg.fields.date("postdate").label("Posted on"),
//                cfg.fields.select("status").label("HR status").options(statusCategories.keySet()),
                cfg.fields.text("url").label("External Job Url"),
                cfg.fields.link("jobPost").label("Job Post Local Link").appName("assets").targetWorkspace("dam"),
                cfg.fields.text("description").label("Brief Description").rows(5), 
                cfg.fields.pageLink("applylink").label("Apply Link"),
                cfg.fields.basicUpload("jobpostupload").label("Job Post"),
                cfg.fields.checkbox("showparent").buttonLabel("").label("show parent page title in navigation (default show page title)"),
                cfg.fields.checkbox("hideInNavigation").buttonLabel("").label("Hide in navigation").description("Check this box to hide this page in navigation"),
                cfg.fields.text("keyword").label("keywords").description("The keywords about this page")
        );
    }
    
    /**
     * jumbotron area.
     */
    @Area(value="jumbotron", maxComponents=1)
    @Controller
    @Inherits(components = ComponentInheritanceMode.FILTERED)
    @AvailableComponentClasses({JumbotronComponent.class})
    
    public static class JumbotronArea {

        @RequestMapping("/jobPageTemplate/jumbotron")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "megaMenuArea", title = "Top Navigation Bar")   
    @AvailableComponentClasses({MegaMenuAreaComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class topNavigationBarArea {

        @RequestMapping("/jobPageTemplate/topBar")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
	@Area(value="titleArea", maxComponents=1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class})
	 @Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/jobPageTemplate/title")
		public String render() {
			
			return "areas/title_area.jsp";
		}
	}
    @Area("jobLeftColumnArea")
    @Controller
    @AvailableComponentClasses(MainContent.class)
    public static class LeftColumnArea {

        @RequestMapping("/jobPageTemplate/leftColumn")
        public String render() {
            return "areas/pagetwocolumn/leftcolumn_area.jsp";
        }
    }
    
    
    @Area("rightColumnArea")
    @Controller
	 @Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ TextComponent.class, PageNavigation.class,
		RightWidgetComponent.class})
	public static class WidgettColumnArea {

		@RequestMapping("/jobPageTemplate/rightColumnArea")
		public String render() {
			return "areas/pagetwocolumn/rightcolumn_area.jsp";
		}
	}
    
    
    @Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
		RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class RightWidgetColumnArea {

		@RequestMapping("/jobPageTemplate/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}
		
		@TabFactory("Content")
		public void contentTab(UiConfig cfg, TabBuilder tab) {
			 Map<String, String> widgetCategories = new LinkedHashMap<String, String>();
			 widgetCategories.put("Download List", "list");
			 widgetCategories.put("Connect Icons", "connect");
			 
			 tab.fields(cfg.fields.text("header").label("Title"), 
					   cfg.fields.select("classname").label("Widget Type").options(widgetCategories.keySet()));
					
		}
	}
    
    @Controller
    @Area(value = "footer", title = "Footer Menu", maxComponents=2)   
    @AvailableComponentClasses({FooterAreaComponent.class})
 @Inherits(components = ComponentInheritanceMode.ALL)
    public static class FooterMenuArea {

        @RequestMapping("/jobPageTemplate/footermenu")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "finePrintFooter", title = "Fine Print Footer Links" , maxComponents=1)     
    @AvailableComponentClasses({FinePrintFooterComponent.class})
    @Inherits(components = ComponentInheritanceMode.ALL)
    public static class FineFooterMenuArea {

        @RequestMapping("/jobPageTemplate/finefootermenu")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
}