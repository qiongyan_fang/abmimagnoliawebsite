package abmi.module.template.page;

import java.util.LinkedHashMap;
import java.util.Map;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.DialogFactory;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.dialog.config.DialogBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.MainContent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.util.TemplateUtil;

/**
 * * THis is the template for single project page. it can be changed into a
 * component
 * 
 * @author Qiongyan
 * 
 */
@Controller
@Template(title = "Single Project Page", id = "abmiModule:pages/projectpage", dialog = "project-properties")
public class ProjectPageTemplate {

	@RequestMapping("/projectpage")
	public String render(
			@RequestParam(value = "id", required = false) Integer id,
			Node content, ModelMap model) throws RepositoryException {

		if (!PropertyUtil.getBoolean(content, "showparent", false)) {
			model.put("currentLink", MgnlContext.getAggregationState()
					.getMainContentNode().getPath());
		} else {
			model.put("currentLink", MgnlContext.getAggregationState()
					.getMainContentNode().getParent().getPath());
		}
		return "pages/singleprojectpage.jsp";
	}

	@DialogFactory("project-properties")
	public void frontPageProperties(UiConfig cfg, DialogBuilder dialog) {
		dialog.form()
				.tabs(cfg.forms
						.tab("Properties1")
						.label("General Property")
						.fields(cfg.fields.text("projectName").label(
								"Project Name"),
								cfg.fields.text("projecttitle").label(
										"short title"),
								cfg.fields.link("logoUrl").label("Logo Image")
										.appName("assets")
										.targetWorkspace("dam"),
								cfg.fields.link("imageUrl")
										.label("Project Image")
										.appName("assets")
										.targetWorkspace("dam"),

								cfg.fields
										.checkbox("showparent")
										.buttonLabel("")
										.label("parent link")
										.description(
												"show parent page title in navigation (default show page title)"),
								cfg.fields.text("description")
										.label("Brief Description").rows(5),
								cfg.fields.checkbox("hideInNavigation")
										.buttonLabel("")
										.label("Hide in navigation"),
								cfg.fields
										.text("keyword")
										.label("keywords")
										.description(
												"The keywords about this page")),
						cfg.forms
								.tab("Properties1")
								.label("External Website")
								.fields(cfg.fields
										.text("externalurl")
										.label("Website")
										.description(
												"enter external website address, and this page won't be shown in abmi website")));
	}

	// @TabFactory("Content")
	// public void contentTab(UiConfig cfg, TabBuilder tab) {
	//
	// tab.fields(
	//
	// cfg.fields.text("projectName").label("Project Name"),
	// cfg.fields.text("projecttitle").label("short title"),
	// cfg.fields.link("imageUrl").label("Project Image").appName("assets").targetWorkspace("dam"),
	// cfg.fields.link("logoUrl").label("Logo Image").appName("assets").targetWorkspace("dam"),
	// cfg.fields.text("externUlr").label("Website").description("enter external website address, and this page won't be shown in abmi website"),
	//
	// cfg.fields.checkbox("showparent").buttonLabel("").label("parent link").description("show parent page title in navigation (default show page title)"),
	// cfg.fields.text("description").label("Brief Description").rows(5),
	// cfg.fields.checkbox("hideInNavigation").buttonLabel("").label("Hide in navigation"),
	// cfg.fields.text("keyword").label("keywords").description("The keywords about this page")
	// );
	// }

	@Available
	public boolean isAvailable(Node websiteNode) {

		return TemplateUtil.checkAvailability(websiteNode);

	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/projectpage/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	/**
	 * jumbotron area.
	 */
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/projectpage/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Bar")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/projectpage/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses(MainContent.class)
	public static class LeftColumnArea {

		@RequestMapping("/projectpage/leftColumn")
		public String render() {
			return "areas/pagetwocolumn/leftcolumn_area.jsp";
		}
	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ TextComponent.class, PageNavigation.class,
			RightWidgetComponent.class })
	public static class WidgettColumnArea {

		@RequestMapping("/projectpage/rightColumnArea")
		public String render() {
			return "areas/pagetwocolumn/rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class RightWidgetColumnArea {

		@RequestMapping("/projectpage/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		@TabFactory("Content")
		public void contentTab(UiConfig cfg, TabBuilder tab) {
			Map<String, String> widgetCategories = new LinkedHashMap<String, String>();
			widgetCategories.put("Download List", "list");
			widgetCategories.put("Connect Icons", "connect");

			tab.fields(cfg.fields.text("header").label("Title"),
					cfg.fields.select("classname").label("Widget Type")
							.options(widgetCategories.keySet()));

		}
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/projectpage/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/projectpage/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

}
