package abmi.module.template.page.publication;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;



import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.model.util.ModuleParameters;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.page.newsevents.ArchiveListComponent;
import abmi.module.template.util.TemplateUtil;

import java.util.Collections;

@Controller
@Template(title = "Subject List Box", id = "abmiModule:components/subjectlist")
@TemplateDescription("Subject List")
@PageNavigation
public class PublicationSubjectListComponent {

	@RequestMapping("/subjectlist")
	public String render(ModelMap model, Node content)
			throws RepositoryException {
		// document type
		String path = content.getProperty("rootpath").getString();
		Map<String, String> subPageList = new LinkedHashMap<String, String>();

		for (Node node : NodeUtil.getNodes(content.getSession().getNode(path),
				NodeTypes.Page.NAME)) {
			if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)) {

				subPageList.put(node.getPath(),
						PropertyUtil.getString(node, "title", ""));
			}
		}

		model.put("subpage", subPageList);

		// String currentPath = MgnlContext.getAggregationState().getHandle();

		String templateName = "publicationpage";
		// Map<String, String> newsList = new LinkedHashMap<String, String>();

		Node pageNode = content.getSession().getNode(path);

		ArrayList<Date> archiveNewsList = new ArrayList<Date>();

		if (PropertyUtil.getBoolean(content, "isshowall", true)) {
			Map<String, List<String>> publicationMeta = TemplateUtil
					.getPublicationTagsOrg();
			List<String> subjectAreaList = publicationMeta.get("subject");
			Collections.sort(subjectAreaList);
			model.put("subjectList", subjectAreaList);
			List<String> documentTypeList = publicationMeta.get("documentType");
			Collections.sort(documentTypeList);
			model.put("documentType", documentTypeList);

			ArchiveListComponent.loopNodes(pageNode, templateName,
					archiveNewsList);

			

			Collections.sort(archiveNewsList);
			ArrayList<String> archiveList = new ArrayList<String>();
			int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

			for (int i = archiveNewsList.size() - 1; i >= 0; i--) {
				Date curDate = archiveNewsList.get(i);

				String year = yearFormat.format(curDate);
				
				if (year.equals(currentYear + "")) {

					String monthYear = ModuleParameters.sdfmonthyear
							.format(curDate);
					if (!archiveList.contains(monthYear))
						archiveList.add(monthYear);
				} else {
					if (!archiveList.contains(year)) {
						archiveList.add(year);
					}
				}

			}

			model.put("archiveNews", archiveList);
		}
		return "components/subjectlist.jsp";
	}

	void loopNodes(Node node, String templateName,
			ArrayList<String> documentType, ArrayList<String> subjectList,
			ArrayList<Date> archiveNewsList, boolean bArchiveOnly)
			throws RepositoryException {
		for (Node subNode : NodeUtil.getNodes(node, NodeTypes.Page.NAME)) {
			if (!PropertyUtil.getBoolean(subNode, "hideInNavigation", false)

			) {

				String subTemplateName = NodeTypes.Renderable.getTemplate(subNode);
				
				if (subTemplateName != null
						&& subTemplateName.endsWith(templateName)) {
					try {

						/*
						 * String allTags = PropertyUtil.getString(subNode,
						 * "subject", ""); String [] tagList =
						 * allTags.split(","); for (int i = 0; tagList != null
						 * && i < tagList.length; i++){ if (tagList[i] == null
						 * || "".equals(tagList[i].trim())){ continue; } if
						 * (!subjectList.contains(tagList[i].trim())){
						 * 
						 * subjectList.add(tagList[i].trim()); } }
						 */
						if (!bArchiveOnly) {

							for (String row : TemplateUtil
									.getMultiFieldJSONValues(subNode,
											"documenttype")) {
								if (!documentType.contains(row)) {

									documentType.add(row);
								}
							}

							for (String row : TemplateUtil
									.getMultiFieldJSONValues(subNode,
											"docsubject")) {
								if (!subjectList.contains(row.trim())) {

									subjectList.add(row);
								}
							}
						}
						// get archive month, year (for current year) or just
						// older years

					} catch (Exception e) {
					}
				} // end of if itis a valid page

				loopNodes(subNode, templateName, documentType, subjectList,
						archiveNewsList, bArchiveOnly);

			}
		}

	
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(

				cfg.fields.link("rootpath").label("Publication Path")
						.appName("pages").targetWorkspace("website")
						.defaultValue("/home/publications"),
				cfg.fields
						.checkbox("isshowall")
						.buttonLabel("")
						.label("Show All Categories")
						.description(
								"Show All Categories even with no matching documents?")
						.defaultValue("false"),

				cfg.fields.text("generalheader").label("Header Text")
						.defaultValue("Documet Type"),
				cfg.fields.text("documenttypeheader")
						.label("document type Header")
						.defaultValue("Documet Type"),
				cfg.fields.text("subjectheader").label("Subject Header")
						.defaultValue("Subject Area"),
				cfg.fields.text("archiveheader").label("Archive Header")
						.defaultValue("Archive"),
				cfg.fields.checkbox("inheritable").buttonLabel("")
						.label("Inheritable").description("Show in Subpages?")
						.defaultValue("true")

		);
	}
}
