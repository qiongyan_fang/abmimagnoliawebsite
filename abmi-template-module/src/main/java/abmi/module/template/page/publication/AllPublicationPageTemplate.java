package abmi.module.template.page.publication;

import info.magnolia.context.MgnlContext;

import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;

import abmi.model.util.ModuleParameters;
import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.box.ProjectImageBox;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.util.NodeSort;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "(discontinued) All Publication Overview Template", id = "abmiModule:pages/allpublication")
public class AllPublicationPageTemplate {

	@RequestMapping("/allpublication")
	public String render(
			@RequestParam(value = "page", required = false) Integer pageNumber,
			@RequestParam(value = "mode", required = false) String listMode,
			@RequestParam(value = "time", required = false) String time,
			@RequestParam(value = "documenttype", required = false) String documentType,
			@RequestParam(value = "subject", required = false) String subject,
			@RequestParam(value = "keyword", required = false) String keyword,
			Node content, ModelMap model) throws RepositoryException {
		int totalCount = 0;
		MgnlContext.getWebContext().getResponse()
				.setHeader("Cache-Control", "no-cache");

		long start_time = System.nanoTime();
		ArrayList<NodeSort> publicationList = new ArrayList<NodeSort>();
		ArrayList<Map<String, Object>> selectedList = new ArrayList<Map<String, Object>>();

		if (pageNumber == null) {
			pageNumber = 1;
		}

		if (listMode == null) {
			listMode = "detail";
		}

		String pageType = "publication";
		try {
			pageType = content.getProperty("pagetype").getString()
					.toLowerCase();
		} catch (Exception e) {

		}

		int itemPerPage = 3;
		try {
			itemPerPage = Integer.parseInt(PropertyUtil.getString(content,
					listMode + "count", "3"));
		} catch (Exception e) {
		}

		int titleCount = 200000;
		int descriptionCount = 300000;
		if (pageType.equals("publication")) {
			try {
				titleCount = Integer.parseInt(PropertyUtil.getString(content,
						"titlecount", "200"));
			} catch (Exception e) {
			}

			try {
				titleCount = Integer.parseInt(PropertyUtil.getString(content,
						"descriptioncount", "300"));
			} catch (Exception e) {
			}
		}

		int startCount = itemPerPage * (pageNumber - 1) + 1;

		try {
			String path = MgnlContext.getAggregationState().getCurrentContentNode()
					.getPath();

			int endNum = startCount + itemPerPage - 1;

			// first collection all items (publications)
			loopNodes(content.getSession().getNode(path), "page", listMode,
					publicationList, time, documentType, subject, keyword);

			// then sort the items by time
			Collections.sort(publicationList, NodeSort.Comparators.TIME);

//			long end_time1 = System.nanoTime();
//			double difference1 = (end_time1 - start_time) / 1e6;

			totalCount = publicationList.size();

			// then selected the paging items

			for (int index = startCount - 1; index <= endNum - 1
					&& index < publicationList.size(); index++) {
				String nodePath = publicationList.get(index).getPath();

				selectedList.add(AllQueryPublicationPageTemplate
						.getPublicationInfo(path,
								content.getSession().getNode(nodePath),
								listMode, descriptionCount, titleCount));
			}

		} catch (Exception repError) {
			repError.printStackTrace();
		}

		model.put("currentLink", MgnlContext.getAggregationState()
				.getCurrentContentNode().getPath());

		model.put("page", pageNumber);
		model.put("totalPage",
				(int) (Math.ceil((double) totalCount / (double) itemPerPage)));
		model.put("mode", listMode);

		model.put("publicationList", selectedList);

//		long end_time = System.nanoTime();
//		double difference = (end_time - start_time) / 1e6;
	

		return "pages/all" + pageType + "page.jsp";
	}

	void loopNodes(Node node, String templateName, String listMode,
			ArrayList<NodeSort> publicationList, String time,
			String documentType, String subject, String keyword)
			throws RepositoryException {
		for (Node subNode : NodeUtil.getNodes(node, NodeTypes.Page.NAME)) {
			if (!PropertyUtil.getBoolean(subNode, "hideInNavigation", false)
			// && !PropertyUtil.getBoolean(subNode, "isevent", false)
			) {

				if (NodeTypes.Renderable.getTemplate(subNode).endsWith(templateName)) {
					try {

						Calendar calendar = PropertyUtil.getDate(subNode,
								"publishdate");

						// only display the items that has a publish date
						// earlier than today
						if (Calendar.getInstance().compareTo(calendar) >= 0) {

							int year = calendar.get(Calendar.YEAR);

							String month = ModuleParameters.sdfmonthyear
									.format(calendar.getTime());

							if (time != null && time.length() > 0) { // if query
																		// by
																		// time
								if (!time.equals(month)
										&& !time.equals("" + year)) {
									continue; // no match ? skip
								}
							}

							if (documentType != null
									&& documentType.length() > 0) { // if query
																	// by
																	// documenttype
								if (!TemplateUtil.getMultiFieldJSONValues(
										subNode, "documenttype").contains(
										documentType)) {
									continue; // no match ? skip
								}
							}

							if (subject != null && subject.length() > 0) { // if
																			// query
																			// by
																			// subject
								if (!TemplateUtil.getMultiFieldJSONValues(
										subNode, "docsubject")
										.contains(subject)) {
									continue; // no match ? skip
								}
							}

							if (keyword != null && keyword.length() > 0) { // if
																			// query
																			// by
																			// keyword
								if (!TemplateUtil.getMultiFieldJSONValues(
										subNode, "dockeyword")
										.contains(keyword)) {
									continue; // no match ? skip
								}
							}

							// all matched? add in
							publicationList.add(new NodeSort(subNode));
							// System.out.println("matched path =" +
							// subNode.getPath());
							/*
							 * if (time == null || time.equals("") ||
							 * (time.equals(month) || time.equals("" + year))) {
							 * String curTags = PropertyUtil.getString( subNode,
							 * "subject", ""); System.out.println("subject =" +
							 * TemplateUtil.getMultiFieldValues(subNode,
							 * "subject")); if (keyword == null ||
							 * keyword.equals("") || (curTags != null && ("," +
							 * curTags.replaceAll(" ", "") .toLowerCase() + ",")
							 * .indexOf("," + keyword.replaceAll( " ", "")
							 * .toLowerCase() + ",") != -1)) {
							 * publicationList.add(new NodeSort(subNode)); }
							 * 
							 * }
							 */
						}
						// System.out.println("totalCount =" + totalCount);
					} catch (Exception e) {
					}
				} // end of if itis a valid page

				loopNodes(subNode, templateName, listMode, publicationList,
						time, documentType, subject, keyword);

			}
		}

		// System.out.println("publicationList=" + publicationList.size());
	}

	// /**
	// *
	// * @param subNode
	// * @param mode
	// * detail: image, author, date, links, title, abstract, subtitle
	// * ?, tags list: title, links
	// * @return
	// */
	// LinkedHashMap<String, Object> getProjectInfo(Node subNode, String mode,
	// String publicationNewsStr, int descriptionCount, int titleCount) {
	// LinkedHashMap<String, Object> singleProject = new LinkedHashMap<String,
	// Object>();
	//
	// if ("detail".equals(mode)) {
	// singleProject.put("imagetitle",
	// PropertyUtil.getString(subNode, "imagetitle", ""));
	// singleProject.put("doctitle",
	// TemplateUtil.getCharByNumber(PropertyUtil.getString(subNode, "title",
	// ""), titleCount));
	//
	// singleProject.put("subtitle",
	// PropertyUtil.getString(subNode, "subtitle", ""));
	//
	// String tags = PropertyUtil.getString(subNode, "tags", null);
	// if (tags != null) {
	// singleProject.put("tags", tags.split(","));
	// }
	//
	// singleProject.put("imageUrl",
	// PropertyUtil.getString(subNode, "imageUrl", ""));
	//
	// singleProject.put("coverImageUrl",
	// PropertyUtil.getString(subNode, "coverImageUrl", ""));
	//
	// singleProject.put("author",
	// PropertyUtil.getString(subNode, "author", ""));
	//
	// // get an archive time list
	//
	// String dateStr = "";
	// try {
	// Calendar calendar = PropertyUtil
	// .getDate(subNode, "publishdate");
	// dateStr = sdfull.format(calendar.getTime());
	// } catch (Exception e) {
	//
	// }
	// singleProject.put("publishdate", dateStr);
	//
	// singleProject.put("author",
	// PropertyUtil.getString(subNode, "author", ""));
	//
	// singleProject.put("description",
	// TemplateUtil.getCharByNumber(PropertyUtil.getString(subNode,
	// "description", ""), descriptionCount));
	//
	// /*singleProject.put("downloadlink",
	// PropertyUtil.getString(subNode, "downloadlink", ""));*/
	//
	// singleProject.put("viewlink",
	// PropertyUtil.getString(subNode, "viewlink", ""));
	//
	// singleProject.put("documenttype",
	// TemplateUtil.getMultiFieldValues(subNode, "documenttype"));
	//
	// singleProject.put("subject",
	// TemplateUtil.getMultiFieldValues(subNode, "subject"));
	//
	// singleProject.put("keyword",
	// TemplateUtil.getMultiFieldValues(subNode, "keyword"));
	//
	//
	// if ("publication".equalsIgnoreCase(publicationNewsStr)) {
	// ArrayList<String[]> docs = new ArrayList<String[]>();
	// try {
	// for (Node docNode : NodeUtil.getNodes(subNode
	// .getNode("supplementalreports"))) {
	// String[] supDoc = new String[3];
	// supDoc[0] = docNode.getProperty("subdoctitle")
	// .getString();
	// supDoc[1] = docNode.getProperty("subdocviewlink")
	// .getString();
	// /*supDoc[2] = docNode.getProperty("subdocdownloadlink")
	// .getString();*/
	//
	// docs.add(supDoc);
	// }
	//
	// singleProject.put("suppdocs", docs);
	// } catch (Exception e) {
	// System.out.println("loop " + e.getMessage());
	// }
	//
	//
	//
	// singleProject.put("suppdocslink",
	// TemplateUtil.getSupplementalFieldValues(subNode,
	// "supplementalreportslink"));
	//
	// }
	//
	// else if ("event".equalsIgnoreCase(publicationNewsStr)) {
	//
	// Calendar startDate = PropertyUtil.getDate(subNode, "eventstartdate",
	// null);
	// singleProject.put("eventstartdate", startDate
	// );
	// singleProject.put("eventenddate",
	// PropertyUtil.getDate(subNode, "eventenddate", startDate));
	// singleProject.put("eventstarttime",
	// PropertyUtil.getString(subNode, "eventstarttime", ""));
	// singleProject.put("eventendtime",
	// PropertyUtil.getString(subNode, "eventendtime", ""));
	// singleProject.put("eventlocation",
	// PropertyUtil.getString(subNode, "eventlocation", ""));
	//
	// }
	//
	// } else if ("list".equals(mode)){
	// singleProject.put("doctitle",
	// PropertyUtil.getString(subNode, "title", ""));
	//
	// singleProject.put("downloadlink",
	// PropertyUtil.getString(subNode, "downloadlink", ""));
	//
	// singleProject.put("viewlink",
	// PropertyUtil.getString(subNode, "viewlink", ""));
	//
	// }
	// else if ("calendar".equals(mode)){
	// singleProject.put("doctitle",
	// PropertyUtil.getString(subNode, "title", ""));
	//
	// Calendar startDate = PropertyUtil.getDate(subNode, "eventstartdate",
	// null);
	// singleProject.put("eventstartdate", startDate
	// );
	// singleProject.put("eventenddate",
	// PropertyUtil.getDate(subNode, "eventenddate", startDate));
	// singleProject.put("eventstarttime",
	// PropertyUtil.getString(subNode, "eventstarttime", ""));
	// singleProject.put("eventendtime",
	// PropertyUtil.getString(subNode, "eventendtime", ""));
	// singleProject.put("eventlocation",
	// PropertyUtil.getString(subNode, "eventlocation", ""));
	// }
	//
	// try {
	// singleProject.put("parentNodeName",
	// PropertyUtil.getString(subNode.getParent(), "name", ""));
	// } catch (Exception e) {
	//
	// }
	//
	//
	// try {
	// singleProject.put("path", subNode.getPath());
	// } catch (Exception e) {
	//
	// }
	//
	//
	// return singleProject;
	// }

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		ArrayList<String> typeList = new ArrayList<String>();
		typeList.add("News");
		typeList.add("Publication");
		typeList.add("Event");
		tab.fields(
				cfg.fields.select("pagetype").label("Page Type")
						.options(typeList),

				cfg.fields.text("detailcount")
						.label("Number of detailed Items").defaultValue("3")
						.description("Per Page"),
				cfg.fields.text("listcount").label("Number of list items")
						.defaultValue("12"),
				cfg.fields
						.text("titlecount")
						.label("Publication Title characters")
						.defaultValue("200")
						.description(
								"how many characters can a publication title have"),

				cfg.fields
						.text("descriptioncount")
						.label("Publication Description characters")
						.defaultValue("300")
						.description(
								"how many characters can a publication description have?"),

				cfg.fields.checkbox("hideInNavigation").buttonLabel("")
						.label("Hide in navigation"),
				cfg.fields.text("keyword").label("keywords")
						.description("The keywords about this page"));

	}

	// header images
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/allpublication/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/allpublication/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Menu")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/allpublication/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("topLeftColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class })
	public static class TopLeftColumnArea {

		@RequestMapping("/allpublication/leftColumn1")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("bottomLeftColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class })
	public static class DownLeftColumnArea {

		@RequestMapping("/allpublication/leftColumn2")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("projectBoxArea")
	@Controller
	@AvailableComponentClasses({ ProjectImageBox.class })
	public static class ProjectArea {

		@RequestMapping("/allpublication/leftColumnPrjBox")
		public String render(Node content) {

			return "areas/allproject_area.jsp";
		}

	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ RichTextComponent.class, PageNavigation.class })
	public static class ReftColumnArea {

		@RequestMapping("/allpublication/rightColumn")
		public String render() {
			return "areas/publication_rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class RightWidgetColumnArea {

		@RequestMapping("/allpublication/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		// @TabFactory("Content")
		// public void contentTab(UiConfig cfg, TabBuilder tab) {
		// Map<String, String> widgetCategories = new LinkedHashMap<String,
		// String>();
		// widgetCategories.put("Download List", "list");
		// widgetCategories.put("Connect Icons", "connect");
		//
		// tab.fields(cfg.fields.text("header").label("Title"),
		// cfg.fields.select("classname").label("Widget Type")
		// .options(widgetCategories.keySet()));
		//
		// }
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/allpublication/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/allpublication/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Available
	public boolean isAvailable(Node websiteNode) {

		return TemplateUtil.checkAvailability(websiteNode);

	}
}
