package abmi.module.template.page.newsevents;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;


import info.magnolia.context.MgnlContext;

import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import abmi.model.util.ModuleParameters;
import abmi.module.template.component.pagelist.PageNavigation;
import java.util.Collections;

@Controller
@Template(title = "News/Publication Archive List Box", id = "abmiModule:components/archivelist")
@TemplateDescription("News/Publication Archive List")
@PageNavigation
public class ArchiveListComponent {

	@RequestMapping("/archivelistbox")
	public String render(ModelMap model, Node content)
			throws RepositoryException {

	
		String path = MgnlContext.getAggregationState().getHandle();

		String templateName = "page";
//		Map<String, String> newsList = new LinkedHashMap<String, String>();
		ArrayList<Date> archiveNewsList = new ArrayList<Date>();
		Node pageNode = content.getSession().getNode(path);
		 
		loopNodes(pageNode, templateName, archiveNewsList);
	
		Collections.sort(archiveNewsList);
		ArrayList<String> archiveList = new ArrayList<String>();
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

		for (int i = archiveNewsList.size() - 1; i >= 0; i--) {
			Date curDate = archiveNewsList.get(i);

			String year = yearFormat.format(curDate);

			if (year.equals(currentYear + "")) {

				String monthYear = ModuleParameters.sdfmonthyear.format(curDate);
				if (!archiveList.contains(monthYear))
					archiveList.add(monthYear);
			} else {
				if (!archiveList.contains(year)) {
					archiveList.add(year);
				}
			}

		}

		model.put("archiveNews", archiveList);
		return "components/archiveslist.jsp";
	}

	public static void loopNodes(Node node, String templateName,
			ArrayList<Date> archiveNewsList) throws RepositoryException {
		for (Node subNode : NodeUtil.getNodes(node, NodeTypes.Page.NAME)) {
			if (!PropertyUtil.getBoolean(subNode, "hideInNavigation", false)

			) {

				
				if (NodeTypes.Renderable.getTemplate(subNode).endsWith(templateName)) {
					try {
						Calendar calendar = PropertyUtil.getDate(subNode,
								"publishdate");

						if (Calendar.getInstance().compareTo(calendar) >= 0) { // only fetch time that is in the past

							int year = calendar.get(Calendar.YEAR);

							// String month =
							// AllPublicationPageTemplate.sdf.format(calendar.getTime());
							int currentYear = Calendar.getInstance().get(
									Calendar.YEAR);
							;

							

							if (year == currentYear) {
								calendar.set(Calendar.DAY_OF_MONTH, 1);
							} else {
								
								calendar.set(Calendar.MONTH, 1);
								calendar.set(Calendar.DAY_OF_MONTH, 1);
							}

							Date curDate = calendar.getTime();

							if (!archiveNewsList.contains(curDate)) {

								archiveNewsList.add(curDate);
							}

						}
						else {

						}

						// if (currentYear == year){
						// if (!archiveNewsList.contains(month + " " + year)){
						// archiveNewsList.add(month + " " + year);
						// }
						//
						// }
						// else{
						// if (!archiveNewsList.contains(year+"")){
						// archiveNewsList.add(year+"");
						// }
						// }
					} catch (Exception e) {
					}
				} // end of if itis a valid page

				loopNodes(subNode, templateName, archiveNewsList);

			}
		}

		
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		Map<String, String> positionCategories = new LinkedHashMap<String, String>();
		positionCategories.put("Publication", "Publication");
		positionCategories.put("News", "News");

		tab.fields(

				// cfg.fields.link("rootpath").label("News Path").appName("pages").targetWorkspace("website").defaultValue("/home/what-we-do/news"),
				// cfg.fields.select("pagetype").label("Page Type").options(positionCategories.keySet()).required(),
				cfg.fields.text("archiveheader").label("Archive Header")
						.defaultValue("Archive"),
				cfg.fields.checkbox("inheritable").buttonLabel("")
						.label("Inheritable").description("Show in Subpages?")

		);
	}
}
