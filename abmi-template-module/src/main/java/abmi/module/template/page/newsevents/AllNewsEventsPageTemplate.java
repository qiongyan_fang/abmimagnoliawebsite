package abmi.module.template.page.newsevents;

import info.magnolia.context.MgnlContext;

import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.AccessDeniedException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;

import abmi.model.util.ModuleParameters;
import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.MainContent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;

import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;

import abmi.module.template.util.NodeSort;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "All News/Events Overview Template", id = "abmiModule:pages/allnewsevents")
public class AllNewsEventsPageTemplate {
	private static final Logger log = LoggerFactory
			.getLogger(AllNewsEventsPageTemplate.class);

	@RequestMapping("/allnewsevents")
	public String render(
			@RequestParam(value = "page", required = false) Integer pageNumber,
			@RequestParam(value = "mode", required = false) String listMode,
			@RequestParam(value = "time", required = false) String time,
			@RequestParam(value = "documenttype", required = false) String documentType,
			@RequestParam(value = "subject", required = false) String subject,
			@RequestParam(value = "keyword", required = false) String keyword,
			Node content, ModelMap model) throws RepositoryException {
		int totalCount = 0;
		MgnlContext.getWebContext().getResponse()
				.setHeader("Cache-Control", "no-cache");

//		long start_time = System.nanoTime();
		ArrayList<NodeSort> publicationList = new ArrayList<NodeSort>();
		ArrayList<Map<String, Object>> selectedList = new ArrayList<Map<String, Object>>();

		if (pageNumber == null) {
			pageNumber = 1;
		}

		if (listMode == null || "".equals("listMode")) {
			listMode = "detail";
		}

		String pageType = "news";
		try {
			pageType = content.getProperty("pagetype").getString()
					.toLowerCase();
		} catch (Exception e) {

		}

		int itemPerPage = 3;
		try {
			itemPerPage = Integer.parseInt(PropertyUtil.getString(content,
					listMode + "count", "3"));
		} catch (Exception e) {
		}

		int titleCount = 200000;
		int descriptionCount = 300000;

		try {
			titleCount = Integer.parseInt(PropertyUtil.getString(content,
					"titlecount", "200"));
		} catch (Exception e) {
		}

		try {
			descriptionCount = Integer.parseInt(PropertyUtil.getString(content,
					"descriptioncount", "300"));
		} catch (Exception e) {
		}

		int startCount = itemPerPage * (pageNumber - 1) + 1;

		try {
			String path = MgnlContext.getAggregationState().getCurrentContentNode().getPath()
					;

			int endNum = startCount + itemPerPage - 1;

			// first collection all items (publications)
			loopNodes(content.getSession().getNode(path), "page", listMode,
					publicationList, time);

			// then sort the items by time
			Collections.sort(publicationList, NodeSort.Comparators.TIME);

//			long end_time1 = System.nanoTime();
//			double difference1 = (end_time1 - start_time) / 1e6;

			totalCount = publicationList.size();

			// then selected the paging items
			if (!"calendar".equals(listMode)) {

				for (int index = startCount - 1; index <= endNum - 1
						&& index < publicationList.size(); index++) {
					String nodePath = publicationList.get(index).getPath();
					// System.out.println(index + "added path =" + nodePath);
					selectedList.add(getProjectInfo(content.getSession()
							.getNode(nodePath), listMode, pageType,
							descriptionCount, titleCount));
				}
			} else {
				for (int index = 0; index < publicationList.size(); index++) {
					String nodePath = publicationList.get(index).getPath();
					selectedList.add(getProjectInfo(content.getSession()
							.getNode(nodePath), listMode, pageType,
							descriptionCount, titleCount));
				}
			}

		} catch (Exception repError) {
			repError.printStackTrace();
		}

		model.put("currentLink", MgnlContext.getAggregationState()
				.getCurrentContentNode().getPath());

		model.put("page", pageNumber);
		model.put("totalPage",
				(int) (Math.ceil((double) totalCount / (double) itemPerPage)));
		model.put("mode", listMode);

		model.put("publicationList", selectedList);

//		long end_time = System.nanoTime();
//		double difference = (end_time - start_time) / 1e6;
	

		return "pages/all" + pageType + "page.jsp";
	}

	void loopNodes(Node node, String templateName, String listMode,
			ArrayList<NodeSort> publicationList, String time)
			throws RepositoryException {
		for (Node subNode : NodeUtil.getNodes(node, NodeTypes.Page.NAME)) {
			if (!PropertyUtil.getBoolean(subNode, "hideInNavigation", false)
			// && !PropertyUtil.getBoolean(subNode, "isevent", false)
			) {

				if (NodeTypes.Renderable.getTemplate(subNode).endsWith(templateName)) {
					try {

						Calendar calendar = PropertyUtil.getDate(subNode,
								"publishdate");

						// only display the items that has a publish date
						// earlier than today
						if (Calendar.getInstance().compareTo(calendar) >= 0) {

							int year = calendar.get(Calendar.YEAR);

							String month = ModuleParameters.sdfmonthyear
									.format(calendar.getTime());

							if (time != null && time.length() > 0) { // if query
																		// by
																		// time
								if (!time.equals(month)
										&& !time.equals("" + year)) {
									continue; // no match ? skip
								}
							}
						}

						// all matched? add in
						publicationList.add(new NodeSort(subNode));

					} catch (Exception e) {
					}

				} // end of if itis a valid page

				loopNodes(subNode, templateName, listMode, publicationList,
						time);

			}
		}

		// System.out.println("publicationList=" + publicationList.size());
	}

	public static LinkedHashMap<String, Object> getProjectInfo(Node subNode,
			String mode, String publicationNewsStr, int descriptionCount,
			int titleCount) {
		LinkedHashMap<String, Object> singleProject = new LinkedHashMap<String, Object>();

		Calendar calendar = PropertyUtil.getDate(subNode, "publishdate", null);

		singleProject.put("publishdate", calendar);
		// get an archive time list
		// log.warn("calendar {}", calendar);
		try {

			String timeStr = ModuleParameters.sdfull.format(calendar.getTime());

			singleProject.put("publishdateStr", timeStr);

			// log.warn("publishdate {}" , calendar, timeStr);
		} catch (Exception err) {
			log.warn("error convert publishdate {}", err.getMessage());
		}

		if ("detail".equals(mode)) {
			singleProject.put("imagetitle",
					PropertyUtil.getString(subNode, "imagetitle", ""));
			singleProject.put("doctitle", TemplateUtil.getCharByNumber(
					PropertyUtil.getString(subNode, "title", ""), titleCount));

			singleProject.put("imageUrl",
					PropertyUtil.getString(subNode, "imageUrl", ""));

			singleProject.put("coverImageUrl",
					PropertyUtil.getString(subNode, "coverImageUrl", ""));

			singleProject.put("author",
					PropertyUtil.getString(subNode, "author", ""));

			singleProject.put("description", TemplateUtil.getCharByNumber(
					PropertyUtil.getString(subNode, "description", ""),
					descriptionCount));

			singleProject.put("viewlink",
					PropertyUtil.getString(subNode, "viewlink", ""));

			if ("event".equalsIgnoreCase(publicationNewsStr)) {

				Calendar startDate = PropertyUtil.getDate(subNode,
						"eventstartdate", null);
				singleProject.put("eventstartdate", startDate);
				singleProject.put("eventenddate", PropertyUtil.getDate(subNode,
						"eventenddate", startDate));
				singleProject.put("eventstarttime",
						PropertyUtil.getString(subNode, "eventstarttime", ""));
				singleProject.put("eventendtime",
						PropertyUtil.getString(subNode, "eventendtime", ""));
				singleProject.put("eventlocation",
						PropertyUtil.getString(subNode, "eventlocation", ""));

			}

		} else if ("list".equals(mode)) {
			singleProject.put("doctitle",
					PropertyUtil.getString(subNode, "title", ""));
			singleProject.put("author",
					PropertyUtil.getString(subNode, "author", ""));

			singleProject.put("viewlink",
					PropertyUtil.getString(subNode, "viewlink", ""));

			if ("event".equalsIgnoreCase(publicationNewsStr)) {

				Calendar startDate = PropertyUtil.getDate(subNode,
						"eventstartdate", null);
				singleProject.put("eventstartdate", startDate);
				singleProject.put("eventenddate", PropertyUtil.getDate(subNode,
						"eventenddate", startDate));
				singleProject.put("eventstarttime",
						PropertyUtil.getString(subNode, "eventstarttime", ""));
				singleProject.put("eventendtime",
						PropertyUtil.getString(subNode, "eventendtime", ""));
				singleProject.put("eventlocation",
						PropertyUtil.getString(subNode, "eventlocation", ""));

			}

		} else if ("calendar".equals(mode)) {
			singleProject.put("doctitle",
					PropertyUtil.getString(subNode, "title", ""));

			Calendar startDate = PropertyUtil.getDate(subNode,
					"eventstartdate", null);
			singleProject.put("eventstartdate", startDate);
			singleProject.put("eventenddate",
					PropertyUtil.getDate(subNode, "eventenddate", startDate));
			singleProject.put("eventstarttime",
					PropertyUtil.getString(subNode, "eventstarttime", ""));
			singleProject.put("eventendtime",
					PropertyUtil.getString(subNode, "eventendtime", ""));
			singleProject.put("eventlocation",
					PropertyUtil.getString(subNode, "eventlocation", ""));
		}

		try {
			singleProject.put("parentNodeName",
					PropertyUtil.getString(subNode.getParent(), "name", ""));
		} catch (Exception e) {

		}

		try {
			singleProject.put("path", subNode.getPath());
		} catch (Exception e) {

		}

		return singleProject;
	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {
		ArrayList<String> typeList = new ArrayList<String>();
		typeList.add("News");

		typeList.add("Event");
		tab.fields(
				cfg.fields.select("pagetype").label("Page Type")
						.options(typeList),

				cfg.fields.text("detailcount")
						.label("Number of detailed Items").defaultValue("3")
						.description("Per Page"),
				cfg.fields.text("listcount").label("Number of list items")
						.defaultValue("12"),
				cfg.fields
						.text("titlecount")
						.label("Title characters")
						.defaultValue("200")
						.description(
								"how many characters can a publication title have"),

				cfg.fields
						.text("descriptioncount")
						.label("Description characters")
						.defaultValue("300")
						.description(
								"how many characters can a publication description have?"),

				cfg.fields.link("placeholderimg").label("Default Image")
						.appName("assets").targetWorkspace("dam")
						.description(" (external url is ok, include http)"),

				cfg.fields.checkbox("hideInNavigation").buttonLabel("")
						.label("Hide in navigation"),
				cfg.fields.text("keyword").label("keywords")
						.description("The keywords about this page"));

	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Menu")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/allnewsevents/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	// header images
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/allnewsevents/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/allnewsevents/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses(MainContent.class)
	public static class LeftColumnArea {

		@RequestMapping("/allnewsevents/leftColumn")
		public String render() {
			return "areas/pagetwocolumn/leftcolumn_area.jsp";
		}
	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.ALL)
	@AvailableComponentClasses({ TextComponent.class, PageNavigation.class })
	public static class WidgettColumnArea {

		@RequestMapping("/allnewsevents/rightColumnArea")
		public String render() {
			return "areas/pagetwocolumn/rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class RightWidgetColumnArea {

		@RequestMapping("/allnewsevents/rightColumnWidget")
		public String render(Node content, ModelMap model) {
			try {
				model.put("currentLink", content.getParent().getPath());
			} catch (AccessDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ItemNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "areas/pagetwocolumn/widget_area.jsp";
		}

	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/allnewsevents/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/allnewsevents/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

}
