package abmi.module.template.page.publication;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import info.magnolia.cms.util.RequestDispatchUtil;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.DialogFactory;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.objectfactory.Components;
import info.magnolia.templating.functions.TemplatingFunctions;
import info.magnolia.ui.dialog.config.DialogBuilder;
import info.magnolia.ui.form.config.OptionBuilder;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.form.field.transformer.multi.MultiValueChildNodeTransformer;
import info.magnolia.ui.form.field.transformer.multi.MultiValueJSONTransformer;
import info.magnolia.ui.framework.config.UiConfig;

import javax.jcr.AccessDeniedException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import abmi.model.util.ModuleParameters;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;

import abmi.module.template.component.pagelist.PageNavigation;

import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.MainContent;
import abmi.module.template.component.NewsEventsButtonComponent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.util.TemplateUtil;

@Controller
@Template(title = "(discontinued) New Single Publication Page", id = "abmiModule:pages/newpublicationpageNew", dialog = "publication-properties-new")
public class NewPublicationPageTemplate {
	Map<String, Object> publicationMeta = TemplateUtil
			.getPublicationTags(true);
	/*
	 * 
	 * Version Version Grouping (numbers are NOT related to the Unique File #)
	 * Supplementary Grouping (number is referencing the parent file's unique
	 * file number) Description Author(s) Publish Date (joan's sort) Display
	 * Date Document Type 1 Document Type 2 (optional) Subject Area 1 Subject
	 * Area 2 Subject Area 3 Keyword 1 Keyword 2 Keyword 3 Keyword 4 Keyword 5
	 * For Display Image Concatenated File Name File Extension Name
	 * Multi-Versions Centre Contact Person Abstract Journal Volume Issue Page
	 * Range Number of Pages doi*
	 */
	@DialogFactory("publication-properties-new")
	public void frontPageProperties(UiConfig cfg, DialogBuilder dialog) {

	
		dialog.form()
				.tabs(cfg.forms
						.tab("Properties1")
						.label("General Property")
						.fields(cfg.fields.text("fileid")
								.label("Unique File Number").required(),
								cfg.fields.text("legacynumber").label(
										"Legacy Number"),
								cfg.fields.text("imagetitle").label(
										"Abriged Title"),
								cfg.fields.text("title").label("Full Title"),
								cfg.fields
										.text("version")
										.label("Version label: such as time, version no. etc"),
								cfg.fields
										.text("versiongrouping")
										.label("Version Grouping")
										.label("a number assigned to a group of multi-version documents"),

								cfg.fields
										.checkbox("isoldversion")
										.label("Is Old Version (Optional)?")
										.buttonLabel("checked for old versions")
										.description(
												"Optional: will generate automatically if leave it empty.")
										.readOnly(),
								cfg.fields
										.text("versioncount")
										.label("total documents in group")
										.description(
												"Optional: total documents in this group including all versions.")
										.readOnly(),

								cfg.fields.text("author").label("Author(s)")
										.required(),

								cfg.fields.link("coverImageUrl")
										.label("Small Thumbnail Image")
										.appName("assets")
										.targetWorkspace("dam"),

								cfg.fields.link("imageUrl")
										.label("Large Image").appName("assets")
										.targetWorkspace("dam"),
								// cfg.fields.text("image").label("Image"),
								cfg.fields.date("publishdate")
										.label("Publish Date").required(),
								cfg.fields
										.text("displaydate")
										.label("Display Date (Optional)")
										.description(
												"only enter this field, when the publish Date is not accurate."),
								cfg.fields.text("description")
										.label("Brief Description").rows(5),
								//
								//
								//
								// cfg.fields.checkbox("showparent")
								// .buttonLabel(" use parent path in navigation menu")
								// .label("use parent path in navigation menun"),

								cfg.fields.checkbox("hideInNavigation")
										.buttonLabel("")
										.label("Hide in navigation")),
						cfg.forms
								.tab("Properties2")
								.label("Publication Property")
								.fields(

								cfg.fields.text("pagesnumber")
										.label("Number of Pages").required(),
										cfg.fields
												.multi("documenttype")
												.label("Document Types")
												.field(cfg.fields
														.select("documenttypes")
														.options(
																(OptionBuilder[])publicationMeta
																		.get("documentType")))
												.transformerClass(
														MultiValueJSONTransformer.class)
												.required(),
										// MultiValueChildNodeTransformer.class),
										cfg.fields
												.multi("docsubject")
												.label("Subjects")
												.field(cfg.fields
														.select("subject")
														.options(
																(OptionBuilder[])publicationMeta
																		.get("subject")))
												.transformerClass(
														MultiValueJSONTransformer.class)
												.required(),
										// MultiValueChildNodeTransformer.class),

										cfg.fields
												.multi("dockeyword")
												.label("Keywords")
												.field(cfg.fields
														.text("keywordtext"))
												.transformerClass(
														MultiValueJSONTransformer.class),
										// MultiValueChildNodeTransformer.class),

										cfg.fields
												.select("center")
												.label("ABMI Center")
												.options(
														TemplateUtil
																.getABMIDepartmentOptions()
																.keySet())
												.readOnly(false),

										cfg.fields.text("contactperson").label(
												"Contact Person"),
										cfg.fields.text("fileextetion")
												.label("File Extension")
												.description("PDF, XLS, etc"),
										cfg.fields
												.link("viewlink")
												.label("Concatenated File Name")
												.description(
														"complete file name: including path and extension name")
												.appName("assets")
												.targetWorkspace("dam"),
										cfg.fields.text("abstract")
												.label("Abstract").rows(4),

										cfg.fields.text("doi").label("DOI"),

										cfg.fields
												.multi("supplementalreportslink")
												.label("Supplemental Reports")
												.field(cfg.fields
														.link("docpage")
														.label("Document Page")
														.appName("pages")
														.targetWorkspace(
																"website"))
												.transformerClass(
														MultiValueChildNodeTransformer.class)

								),
						cfg.forms
								.tab("Properties2")
								.label("Publication Property")
								.fields(cfg.fields
										.text("flipbooklink")
										.label("flipbook link")
										.description(
												"flipbook (or similar app) link: external starts with http; internal follow same path format as the flip link"),
										cfg.fields
												.text("flipbooklinktext")
												.label("flipbook link text")
												.description(
														"text for link to flipbook (or similar app)"),
										cfg.fields.link("previewflipimg")
												.label("Flipbook image")
												.appName("assets")
												.targetWorkspace("dam"),
										cfg.fields.text("image").label(
												"Image (not sure)"),
										cfg.fields.text("orgimagename").label(
												"Image Orig. File Name"),
										cfg.fields.text("doidisplay").label(
												"For Display (not sure)"),
										cfg.fields
												.link("rootpath")
												.appName("pages")
												.targetWorkspace("website")
												.label("publication overview path")
												.defaultValue(
														"/home/publications")
												.description(
														"optional publication overview page path."),
										cfg.fields.checkbox("ismultiversions")
												.label("Multi-Versions")
												.buttonLabel(""),
										cfg.fields.date("webuploaddate").label(
												"Web Upload Date")

								),
						cfg.forms
								.tab("Properties2")
								.label("Publication Property")
								.fields(cfg.fields.text("journal").label(
										"Journal"),
										cfg.fields.text("volume").label(
												"Volume"),
										cfg.fields.text("issue").label("Issue"),

										cfg.fields.text("pagesrange").label(
												"Page Range")),

						// cfg.forms
						// .tab("event")
						// .label("For Events Only")
						// .fields(cfg.fields.checkbox("isevent")
						// .label("Is this a Event?")
						// .defaultValue("false").buttonLabel(""),
						// cfg.fields.date("eventstartdate")
						// .label("Event Start Date"),
						// cfg.fields.date("eventenddate").label(
						// "Event End Date"),
						// cfg.fields.text("eventstarttime")
						// .label("Event Start Time"),
						// cfg.fields.text("eventendtime").label(
						// "Event End Time"),
						// cfg.fields.text("eventlocation").label(
						// "Event Location")),
						cfg.forms
								.tab("Metadata")
								.label("Page Keyword")
								.fields(cfg.fields
										.text("keyword")
										.label("keywords")
										.description(
												"The keywords about this page"))

				);
	}

	@RequestMapping("/publicationpageNew")
	public String render(
			@RequestParam(value = "id", required = false) Integer id,
			Node content, ModelMap model, HttpServletResponse response,
			HttpServletRequest request) throws RepositoryException {

		if (PropertyUtil.getBoolean(content, "showparent", true)) {
			model.put("currentLink", MgnlContext.getAggregationState()
					.getCurrentContentNode().getParent().getPath());
		} else {
			model.put("currentLink", MgnlContext.getAggregationState()
					.getCurrentContentNode().getPath());
		}

		String path = PropertyUtil.getString(content, "rootpath",
				"/home/publications");

		String displayDate = PropertyUtil.getString(content, "displaydate",
				null);
		if (displayDate == null) {
			Calendar publishDate = PropertyUtil.getDate(content, "publishdate",
					null);
			if (publishDate != null) {
				content.setProperty("displaydate",
						ModuleParameters.sdfull.format(publishDate.getTime()));
				content.getSession().save();
			}
		}
		model.put("content_documenttype",
				TemplateUtil.getMultiFieldJSONValues(content, "documenttype", (HashMap<String,String>)publicationMeta
				.get("documentMap")));

		model.put("content_subject",
				TemplateUtil.getMultiFieldJSONValues(content, "docsubject",(HashMap<String,String>)publicationMeta
						.get("subjectMap")));

		model.put("content_keyword",
				TemplateUtil.getMultiFieldJSONValues(content, "dockeyword"));
		model.put("content_supplementalreportslink", TemplateUtil
				.getSupplementalFieldValues(content, "supplementalreportslink"));

		String doi = PropertyUtil.getString(content, "doi", "");
		if (!"".equals(doi)) {
			String link = PropertyUtil.getString(content, "viewlink", "");

			Map<String, String> fileProperties = TemplateUtil
					.getAssetsProperty(link);
			if (fileProperties != null) {
				model.put("extension", fileProperties.get("extension"));
				model.put("size", fileProperties.get("size"));
			}

		}

		/**************************************************************************************
		 ************************************************************************************** 
		 * If the current document is part of a version group, but doesn't have
		 * total count of version document, generated dynmically. (Only on the
		 * author site).
		 * 
		 **************************************************************************************/
		String versionGroup = PropertyUtil.getString(content,
				"versiongrouping", null);
		if (versionGroup != null) {
			Session session = MgnlContext.getJCRSession("website");

			if (Components.getComponent(TemplatingFunctions.class)
					.isAuthorInstance()
					&& PropertyUtil.getLong(content, "versioncount", null) == null) {

				TemplateUtil.updateSingleVersion(session, path, versionGroup);
			}

			model.put("totalVersionCount",
					PropertyUtil.getLong(content, "versioncount", null));
			model.put("allVersions",
					TemplateUtil.getAllVersion(session, path, versionGroup));

		}

		/**************************************************************************************
		 **************************************************************************************
		 **************************************************************************************/

		/*
		 * also find the list of document that are different versions current
		 * ones. order by years desc
		 */
		// don't display it if publish date not arrived, and in public instance
		// */
		Calendar calendar = PropertyUtil.getDate(content, "publishdate");
		try {

			if (Calendar.getInstance().compareTo(calendar) < 0) { // in future

				if (Components.getComponent(TemplatingFunctions.class)
						.isAuthorInstance()) {
					model.put("futureRelease", true);

				} else {
					System.out.println("redirect to error message");
					RequestDispatchUtil.dispatch(
							"forward:/home/errorPages/404.html?releaseLater",
							request, response);

				}
			}
		} catch (Exception e) {
		}

		model.put("mode", request.getParameter("mode"));
		// System.out.println("debug redirect to single publication page");
		return "pages/singlepublicationpageNew.jsp";

	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/publicationpageNew/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Menu")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/publicationpageNew/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	// header images
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/publicationpageNew/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses({ MainContent.class,
			NewsEventsButtonComponent.class })
	public static class LeftColumnArea {

		@RequestMapping("/publicationpageNew/leftColumn")
		public String render() {
			return "areas/pagetwocolumn/leftcolumn_area.jsp";
		}
	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ TextComponent.class, PageNavigation.class })
	public static class WidgettColumnArea {

		@RequestMapping("/publicationpageNew/rightColumnArea")
		public String render() {
			return "areas/publication_rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class RightWidgetColumnArea {

		@RequestMapping("/publicationpageNew/rightColumnWidget")
		public String render(Node content, ModelMap model) {
			try {
				model.put("currentLink", content.getParent().getPath());
			} catch (AccessDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ItemNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		@TabFactory("Content")
		public void contentTab(UiConfig cfg, TabBuilder tab) {
			Map<String, String> widgetCategories = new LinkedHashMap<String, String>();
			widgetCategories.put("Download List", "list");
			widgetCategories.put("Connect Icons", "connect");

			tab.fields(cfg.fields.text("header").label("Title"),
					cfg.fields.select("classname").label("Widget Type")
							.options(widgetCategories.keySet()));

		}

	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/publicationpageNew/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/publicationpageNew/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
	
    @Available
	public boolean isAvailable(Node websiteNode) {

		
		return TemplateUtil.checkAvailability(websiteNode);

	}
}
