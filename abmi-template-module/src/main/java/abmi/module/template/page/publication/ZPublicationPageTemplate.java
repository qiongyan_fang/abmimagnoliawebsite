package abmi.module.template.page.publication;

import org.springframework.stereotype.Controller;

@Controller
//@Template(title = "Single News/Event/Publication Page", id = "abmiModule:pages/publicationpage", dialog = "publication-properties")
public class ZPublicationPageTemplate {
/*
	@DialogFactory("publication-properties")
	public void frontPageProperties(UiConfig cfg, DialogBuilder dialog) {
		dialog.form().tabs(
				cfg.forms
						.tab("Properties1")
						.label("General Property")
						.fields(
								
								cfg.fields.text("imagetitle").label("Abriged Title"),
								cfg.fields.text("title").label("Full Title"),						
						
								cfg.fields.text("author").label("Author"),
								cfg.fields.link("imageUrl")
										.label("Project Image")
										.appName("assets")
										.targetWorkspace("dam"),
								cfg.fields.date("publishdate").label("Publish Date"),
								cfg.fields.text("description").label(
												"Brief Description").rows(5),
								
								cfg.fields.link("newsImageUrl")
												.label("News Teaser Image")
												.appName("assets")
												.targetWorkspace("dam").description(" News teaser image"),	
								cfg.fields.checkbox("showparent").buttonLabel("").label("show parent page title in navigation (default show page title)"),
												 
									cfg.fields.checkbox("hideInNavigation")
												.buttonLabel("")
												.label("Hide in navigation")												
										),
						cfg.forms
										.tab("Properties2")
										.label("Publication Property")
										.fields(					
												cfg.fields.link("coverImageUrl")
												.label("Cover Image")
												.appName("assets")
												.targetWorkspace("dam").description("Report Cover Images"),	
							
								
										cfg.fields.text("tags").label("Tags (separated by ,)"),
								cfg.fields.multi("tags2").label("Tags").field(cfg.fields.select("tagselect").readOnly(false)).transformerClass(MultiValueChildrenNodeTransformer.class),
										
										cfg.fields.text("subtitle").label(
												"Category Title"),
									
								cfg.fields.link("downloadlink")
										.label("Project Download File")
										.appName("assets")
										.targetWorkspace("dam"),
								cfg.fields.link("viewlink")
										.label("Project View File")
										.appName("assets")
										.targetWorkspace("dam"),
								cfg.fields.multi("supplementalreports").label("Supplemental Reports").field(new CompositeFieldBuilder("subdoc")
								.fields(cfg.fields.text("subdoctitle").label("doc title")).
										fields(cfg.fields.link("subdocdownloadlink").label("download link").appName("assets").targetWorkspace("dam") ) . 
										fields(cfg.fields.link("subdocviewlink").label("view link").appName("assets").targetWorkspace("dam") )
										).transformerClass(MultiValueSubChildrenNodePropertiesTransformer.class)
										 
										),
		  cfg.forms
										.tab("event")
										.label("For Events Only")
										.fields(cfg.fields.checkbox("isevent").label("Is this a Event?").defaultValue("false").buttonLabel(""),
												cfg.fields.date("eventstartdate").label("Event Start Date"),
												cfg.fields.date("eventenddate").label("Event End Date"),
												cfg.fields.text("eventstarttime").label("Event Start Time"),
												cfg.fields.text("eventendtime").label("Event End Time"),
												cfg.fields.text("eventlocation").label("Event Location")
												),
				cfg.forms
						.tab("Metadata")
						.label("Page Keyword")
						.fields(cfg.fields.text("keyword").label("keywords")
								.description("The keywords about this page"))

		);
	}

	@RequestMapping("/publicationpage")
	public String render(
			@RequestParam(value = "id", required = false) Integer id,
			Node content, ModelMap model) throws RepositoryException {

		
	
			if (PropertyUtil.getBoolean(content, "showparent", true)){
	    		model.put("currentLink", MgnlContext.getAggregationState().getCurrentContentNode().getParent().getHandle());
	    	}
	    	else{
	    		model.put("currentLink", MgnlContext.getAggregationState().getCurrentContentNode().getPath());
	    	}
//			model.put("currentLink", MgnlContext.getAggregationState().getCurrentContentNode().getParent().getHandle());
	

		if (PropertyUtil.getBoolean(content, "isevent", false)){
			return "pages/singleeventpage.jsp";
		}
		else				
			return "pages/singlepublicationpage.jsp";
	}

	

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/publicationpage/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}
	
	 @Controller
	    @Area(value = "megaMenuArea", title = "Top Navigation Menu")   
	    @AvailableComponentClasses({MegaMenuAreaComponent.class})
	    @Inherits(components = ComponentInheritanceMode.ALL)
	    public static class topNavigationBarArea {

	        @RequestMapping("/publicationpage/topBar")
	        public String render() {
	            return "areas/generalarea.jsp";
	        }
	    }

		// header images
		@Area(value = "jumbotron", maxComponents = 1)
		@Controller
		@Inherits(components = ComponentInheritanceMode.FILTERED)
		@AvailableComponentClasses({ JumbotronComponent.class })
		public static class JumbotronArea {

			@RequestMapping("/publicationpage/jumbotron")
			public String render() {
				return "areas/generalarea.jsp";
			}
		}
	 
	 @Area("leftColumnArea")
	@Controller
	@AvailableComponentClasses(MainContent.class )
	public static class LeftColumnArea {

		@RequestMapping("/publicationpage/leftColumn")
		public String render() {
			return "areas/pagetwocolumn/leftcolumn_area.jsp";
		}
	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.ALL)
	@AvailableComponentClasses({ TextComponent.class,
			SubPageListComponent.class})
	public static class WidgettColumnArea {

		@RequestMapping("/publicationpage/rightColumnArea")
		public String render() {
			return "areas/publication_rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class RightWidgetColumnArea {

		@RequestMapping("/publicationpage/rightColumnWidget")
		public String render(Node content,  ModelMap model) {
			try {
				model.put("currentLink", content.getParent().getPath());
			} catch (AccessDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ItemNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		@TabFactory("Content")
		public void contentTab(UiConfig cfg, TabBuilder tab) {
			Map<String, String> widgetCategories = new LinkedHashMap<String, String>();
			widgetCategories.put("Download List", "list");
			widgetCategories.put("Connect Icons", "connect");

			tab.fields(cfg.fields.text("header").label("Title"),
					cfg.fields.select("classname").label("Widget Type")
							.options(widgetCategories.keySet()));

		}
	}
	*/
}
