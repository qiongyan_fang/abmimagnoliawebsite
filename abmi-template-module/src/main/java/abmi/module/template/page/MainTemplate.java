/**
 * This file Copyright (c) 2010-2013 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This file is dual-licensed under both the Magnolia
 * Network Agreement and the GNU General Public License.
 * You may elect to use one or the other of these licenses.
 *
 * This file is distributed in the hope that it will be
 * useful, but AS-IS and WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE, or NONINFRINGEMENT.
 * Redistribution, except as permitted by whichever of the GPL
 * or MNA you select, is prohibited.
 *
 * 1. For the GPL license (GPL), you can redistribute and/or
 * modify this file under the terms of the GNU General
 * Public License, Version 3, as published by the Free Software
 * Foundation.  You should have received a copy of the GNU
 * General Public License, Version 3 along with this program;
 * if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * 2. For the Magnolia Network Agreement (MNA), this file
 * and the accompanying materials are made available under the
 * terms of the MNA which accompanies this distribution, and
 * is available at http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package abmi.module.template.page;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import javax.jcr.Node;
import javax.jcr.RepositoryException;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;


import abmi.module.template.component.ImageComponent;
import abmi.module.template.component.MainIconButtonComponent;
import abmi.module.template.component.MainJumbotronComponent;
import abmi.module.template.component.TwoColumnComponent;
import abmi.module.template.component.LearnMoreButtonComponent;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;


import info.magnolia.rendering.template.TemplateDefinition;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

/**
 * Home page with videos, mega menus etc.
 */
@Controller
@Template(title = "ABMI Home Page", id = "abmiModule:pages/main")
public class MainTemplate {

	 @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	
		 
	    	 tab.fields(
	    			  cfg.fields.text("keyword").label("keywords").description("The keywords about this page")
	               
	                
	         );
	    }

    @RequestMapping("/mainTemplate")
    public String render(Node page, ModelMap model) throws RepositoryException {


    	

        return "pages/main.jsp";
    }


  
    @Available
	public boolean isAvailable(Node websiteNode, TemplateDefinition temp) {

		
    	try {

    		
			if (websiteNode.getPath().equals("/") || 
					websiteNode.getPath().equals("/temp")
					|| websiteNode.getDepth() < 2) {
				return true;
			} else {
				return false;
			}
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		}

	}
    
    @Controller
    @Area(value = "megaMenuArea", title = "Top Navigation Bar")   
    @AvailableComponentClasses({MegaMenuAreaComponent.class})
    public static class topNavigationBarArea {

        @RequestMapping("/mainTemplate/topBar")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    /**
     * jumbotron area.
     */
    @Area(value="jumbotron", maxComponents=1)
    @Controller
    @AvailableComponentClasses({MainJumbotronComponent.class})
    
    public static class JumbotronArea {

        @RequestMapping("/mainTemplate/jumbotron")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    
    /**
     * 3 green button area area.
     */
    @Area(value="greenButtonArea", maxComponents=3)
    @Controller
    @AvailableComponentClasses({MainIconButtonComponent.class})
    
    public static class GreenButtonArea {

        @RequestMapping("/mainTemplate/greenButtonArea")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
   
    /**
     * the learn more button under project sponsors
     */
    @Area(value="bottomLearnMoreButtonArea", maxComponents=1)
    @Controller
    @AvailableComponentClasses({LearnMoreButtonComponent.class})
    
    public static class BottomLearnMoreButtonArea {

        @RequestMapping("/mainTemplate/bottomLearnMoreButtonArea")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    /* rows with two columns layout, users to add news hightlight etc.*/
    @Controller
    @Area(value = "rowArea", title = "Row Area")
    @Inherits
    @AvailableComponentClasses({TwoColumnComponent.class})
  
    public static class RowArea {

        @RequestMapping("/mainTemplate/rowarea")
        public String render() {
            return "areas/main_rowarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "partnerIconArea", title = "Partner Icon Area")
    @Inherits
    @AvailableComponentClasses({ImageComponent.class})
  
    public static class PartnerIconArea {

        @RequestMapping("/mainTemplate/partnericonarea")
        public String render() {
            return "areas/main_partnericon_area.jsp";
        }
    }

    
    @Controller
    @Area(value = "leftSponsorIconArea", title = "ABMI Sponsor Icon Area")
    @Inherits
    @AvailableComponentClasses({ImageComponent.class})
  
    public static class LeftSponserArea {

        @RequestMapping("/mainTemplate/leftsponsoriconarea")
   public String render(Node content, ModelMap model) {
        
        	
        	try {
        		List<Integer> list = new ArrayList<Integer>();
				for (int i=0; content.getNodes()!= null && i < content.getNodes().getSize(); i++){
					
					list.add(i);
				}
				Collections.shuffle(list);

				 model.put("order", list);
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        	  return "areas/main_sponsoricon_area.jsp";
    }
    }
    
    @Controller
    @Area(value = "rightSponsorIconArea", title = "Partner Sponsor Icon Area")
    @Inherits
    @AvailableComponentClasses({ImageComponent.class})
  
    public static class RightSponserArea {

        @RequestMapping("/mainTemplate/rightsponsoriconarea")
        public String render(Node content, ModelMap model) {
        
        	
        	try {
        		List<Integer> list = new ArrayList<Integer>();
				for (int i=0; content.getNodes()!= null && i < content.getNodes().getSize(); i++){
					
					list.add(i);
				}
				Collections.shuffle(list);

				 model.put("order", list);
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return "areas/main_sponsoricon_area.jsp";
        }
    }

    @Controller
    @Area(value = "footer", title = "Footer Menu", maxComponents=2)   
    @AvailableComponentClasses({FooterAreaComponent.class})
    public static class FooterMenuArea {

        @RequestMapping("/mainTemplate/footermenu")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
    
    @Controller
    @Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents=1)   
    @AvailableComponentClasses({FinePrintFooterComponent.class})
    public static class FineFooterMenuArea {

        @RequestMapping("/mainTemplate/finefootermenu")
        public String render() {
            return "areas/generalarea.jsp";
        }
    }
}
