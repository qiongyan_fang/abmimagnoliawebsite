package abmi.module.template.page;

import info.magnolia.context.MgnlContext;

import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.ComponentInheritanceMode;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import abmi.module.template.component.JumbotronComponent;
import abmi.module.template.component.RichTextComponent;
import abmi.module.template.component.TextComponent;
import abmi.module.template.component.box.ProjectImageBox;
import abmi.module.template.component.footer.FinePrintFooterComponent;
import abmi.module.template.component.footer.FooterAreaComponent;
import abmi.module.template.component.menu.MegaMenuAreaComponent;
import abmi.module.template.component.pagelist.PageNavigation;
import abmi.module.template.component.rightsidewidget.RightWidgetComponent;
import abmi.module.template.util.TemplateUtil;

/**
 * this is a page for list all projects under the given folder * I would use a
 * component instead of a page if I need to do it again.
 * 
 * @author Qiongyan
 * 
 */

@Controller
@Template(title = "All Projects Overview Template", id = "abmiModule:pages/allprojects")
public class AllProjectPageTemplate {
	@RequestMapping("/allprojects")
	public String render(Node content, ModelMap model)
			throws RepositoryException {
		ArrayList<Map<String, String>> projectList = new ArrayList<Map<String, String>>();
		try {
			String path = MgnlContext.getAggregationState().getHandle();

			// set default numbers of characters.
			int characterInt = 150;
			try {
				characterInt = (int) PropertyUtil.getLong(content,
						"characternumber", 150L).longValue();
			} catch (Exception e) {
			}

			for (Node node : NodeUtil.getNodes(
					content.getSession().getNode(path), NodeTypes.Page.NAME)) {
				if (!PropertyUtil.getBoolean(node, "hideInNavigation", false)

				) {

				
					if (NodeTypes.Renderable.getTemplate(node).endsWith("/projectpage")) {
						projectList.add(getProjectInfo(node, characterInt));
					}

					for (Node subNode : NodeUtil.getNodes(node,
							NodeTypes.Page.NAME)) {
				
						if (!PropertyUtil.getBoolean(subNode,
								"hideInNavigation", false)

						) {
							
							if (NodeTypes.Renderable.getTemplate(subNode).endsWith(
									"/projectpage")) {
								projectList.add(getProjectInfo(subNode,
										characterInt));
							}
						}
					}
				}
			}
		} catch (Exception repError) {
			repError.printStackTrace();
		}
		
		model.put("boxWidth",
				PropertyUtil.getString(content, "boxnum", "medium"));
		model.put("projects", projectList);
		
		return "pages/allprojectpage.jsp";
	}

	@Available
	public boolean isAvailable(Node websiteNode) {

		return TemplateUtil.checkAvailability(websiteNode);

	}

	@TabFactory("Content")
	public void contentTab(UiConfig cfg, TabBuilder tab) {

		tab.fields(
				cfg.fields
						.text("characternumber")
						.label("Description characters number")
						.defaultValue("150")
						.description(
								"how many characters to display in image boxes"),
				cfg.fields.select("boxnum").options(TemplateUtil.getBoxWidth())
						.label("Boxes width"),
				cfg.fields.text("keyword").label("keywords")
						.description("The keywords about this page")

		);
	}

	LinkedHashMap<String, String> getProjectInfo(Node subNode, int charInt) {
		LinkedHashMap<String, String> singleProject = new LinkedHashMap<String, String>();

		singleProject.put("projectname",
				PropertyUtil.getString(subNode, "projectname", ""));

		singleProject.put("imageUrl",
				PropertyUtil.getString(subNode, "imageUrl", ""));
		singleProject.put("logoUrl",
				PropertyUtil.getString(subNode, "logoUrl", ""));
		String description = PropertyUtil.getString(subNode, "description", "");

		singleProject.put("description",
				TemplateUtil.getCharByNumber(description, charInt));

		singleProject.put("externalurl",
				PropertyUtil.getString(subNode, "externalurl", ""));

		singleProject.put("projecttitle",
				PropertyUtil.getString(subNode, "projecttitle", ""));
		try {
			singleProject.put("parentNodeName",
					PropertyUtil.getString(subNode.getParent(), "name", ""));
		} catch (Exception e) {

		}
		try {
			singleProject.put("path", subNode.getPath());
		} catch (Exception e) {

		}

		
		return singleProject;
	}

	// repeated areas copied from other pages.
	// header images
	@Area(value = "jumbotron", maxComponents = 1)
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ JumbotronComponent.class })
	public static class JumbotronArea {

		@RequestMapping("/allprojects/jumbotron")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area(value = "titleArea", maxComponents = 1)
	@Controller
	@AvailableComponentClasses({ TextComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class titleArea {

		@RequestMapping("/allprojects/title")
		public String render() {

			return "areas/title_area.jsp";
		}
	}

	@Controller
	@Area(value = "megaMenuArea", title = "Top Navigation Menu")
	@AvailableComponentClasses({ MegaMenuAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class topNavigationBarArea {

		@RequestMapping("/allprojects/topBar")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Area("topLeftColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class })
	public static class TopLeftColumnArea {

		@RequestMapping("/allprojects/leftColumn1")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("bottomLeftColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class })
	public static class DownLeftColumnArea {

		@RequestMapping("/allprojects/leftColumn2")
		public String render(Node content) {

			return "areas/generalarea.jsp";
		}

	}

	@Area("projectBoxArea")
	@Controller
	@AvailableComponentClasses({ ProjectImageBox.class })
	public static class ProjectArea {

		@RequestMapping("/allprojects/leftColumnPrjBox")
		public String render(Node content) {

			return "areas/allproject_area.jsp";
		}

		@TabFactory("Content")
		public void contentTab(UiConfig cfg, TabBuilder tab) {

			tab.fields(

			cfg.fields
					.text("buttontext")
					.label("Button Text")
					.defaultValue("")
					.description(
							"If you don't want to use View Project or View Website, please specify your button text here.")

			);
		}

	}

	@Area("rightColumnArea")
	@Controller
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	@AvailableComponentClasses({ RichTextComponent.class, PageNavigation.class })
	public static class ReftColumnArea {

		@RequestMapping("/allprojects/rightColumn")
		public String render() {
			return "areas/pagetwocolumn/rightcolumn_area.jsp";
		}
	}

	@Area("rightWidgetColumnArea")
	@Controller
	@AvailableComponentClasses({ RichTextComponent.class,
			RightWidgetComponent.class })
	@Inherits(components = ComponentInheritanceMode.FILTERED)
	public static class RightWidgetColumnArea {

		@RequestMapping("/allprojects/rightColumnWidget")
		public String render() {
			return "areas/pagetwocolumn/widget_area.jsp";
		}

		@TabFactory("Content")
		public void contentTab(UiConfig cfg, TabBuilder tab) {
			Map<String, String> widgetCategories = new LinkedHashMap<String, String>();
			widgetCategories.put("Download List", "list");
			widgetCategories.put("Connect Icons", "connect");

			tab.fields(cfg.fields.text("header").label("Title"),
					cfg.fields.select("classname").label("Widget Type")
							.options(widgetCategories.keySet()));

		}
	}

	@Controller
	@Area(value = "footer", title = "Footer Menu", maxComponents = 2)
	@AvailableComponentClasses({ FooterAreaComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FooterMenuArea {

		@RequestMapping("/allprojects/footermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}

	@Controller
	@Area(value = "finePrintFooter", title = "Fine Print Footer Links", maxComponents = 1)
	@AvailableComponentClasses({ FinePrintFooterComponent.class })
	@Inherits(components = ComponentInheritanceMode.ALL)
	public static class FineFooterMenuArea {

		@RequestMapping("/allprojects/finefootermenu")
		public String render() {
			return "areas/generalarea.jsp";
		}
	}
}
