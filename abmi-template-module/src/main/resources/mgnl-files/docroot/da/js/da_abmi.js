//

// DataTables initialisation

//
var speciesTable;
$(document)
		.ready(
				function() {
					var ServletPath = "/FileDownloadServlet?dir=WEB_GRAPH&filename=";

					var gIsInitLoad = true; // update species count at initial
					// load only
					var rootPath = "/.ajax/bioBrowser/"
					var searchAutoCompleteUrl = rootPath
							+ "getAutoCompeleteSpecies";
					var speciesListUrl = rootPath + "getSpeciesList";
					var gVascularPlantId = 2;

					var columnImage = 0;
					var columnSummary = 1;
					var columnCommonName = 2;
					var columnScientificName = 3;
					if ($("#data-block").length > 0) {

						var dataDef = [ {
							"data" : "image",
							"orderable" : false,
							"searchable" : false
						}, {
							"data" : "summary_type",
							"searchable" : false
						}, {
							"data" : "common_name",
							"searchable" : true
						}, {
							"data" : "scientific_name",
							"searchable" : true
						} ,
						
						{
							"data" : "tsn",
							"visible" : false,
							"searchable" : false
						}];

					

						speciesTable = $('#species-data')
								.on(
										'preXhr.dt',
										function(e, settings, data) { /*
																		 * change
																		 * data
																		 * format,
																		 * to
																		 * reduce
																		 * high
																		 * dimensional
																		 * arrays
																		 * to
																		 * lower
																		 * ones,
																		 * to
																		 * match
																		 * model
																		 * type
																		 */

											for (var i = 0; i < data.columns.length; i++) {
												column = data.columns[i];
												column.searchRegex = column.search.regex;
												column.searchValue = column.search.value;
												delete (column.search);
											}

										})
								.on(
										'xhr.dt',
										function(e, settings, data) { /*
																		 * add
																		 * species
																		 * count
																		 */

											if (gIsInitLoad) {
											/*
											 * $("#sppCount") .text( "(" +
											 * data.recordsTotal
											 * .toLocaleString() + ")");
											 */
												gIsInitLoad = false;
											}

										})

								.DataTable(
										{
											processing : true,
											
											serverSide : true,
											scroller : {

												loadingIndicator : true

											},
											scrollY : 200,
											search : false,
											deferRender : true, // render node
											// only when
											// needed
											paging : true,
											pageLength : 50,
											language : {

												infoEmpty : "No species profiles available for this filter selection; please select another filter option.",
											    zeroRecords:"No species profiles available for this filter selection; please select another filter option.",
												info : "Showing _START_ to _END_ of _TOTAL_ species"

											},
											ajax : speciesListUrl + "?groupId="
													+ gSpeciesGroupId,
											columns : dataDef,

											bAutoWidth:false,
											
											"createdRow" : function(row, data,
													index) {
												// Bold the grade for all 'A'
												// grade browsers
											/*
											 * if (data.summary_type == "Full") {
											 * $('td:eq(1)', row)
											 * .addClass("full");
											 *  } else if (data.summary_type ==
											 * "Forested") { $('td:eq(1)', row)
											 * .addClass( "forested");
											 *  } else if (data.summary_type ==
											 * "Prairie") { $('td:eq(1)', row)
											 * .addClass("prairie");
											 *  } else { $('td:eq(1)', row)
											 * .addClass("basic");
											 *  }
											 */

												$('td:eq(1)', row)
												.html('<img src="/docroot/da/assets/map-' + data.summary_type + '.png" alt=""  title="' + data.summary_type + '" />');
												
											
// var img = $('td:eq(0)', row).text();

												/**
												 * change column 1 from text to
												 * image
												 */
												$('td:eq(0)', row).addClass(
														"datatableimg");

												if (data.image) {
													$('td:eq(0)', row)
															.html(
																	'<div class="pagetitleimg"><img alt="" src="' +
																	ServletPath + data.image
																			+ '"></div>');
												} else { // use default if no
													// image is
													// provided.
													$('td:eq(0)', row)
															.html(
																	'<div class="pagetitleimg"><img alt="" src="/docroot/da/assets/species-list-standard.jpg"></div>');
												}

												// add link and pointer
												$(row).css("cursor", "pointer");

												$('td:eq(3)', row).addClass(
														"scientific-name");

												$('td:eq(1)', row).addClass(
														"summary_col");

												$('td:eq(0)', row).addClass(
														"img_col");

												$('td:eq(3), td:eq(2)', row)
														.addClass("name_col");

												$(row)
														.unbind()
														.bind(
																"click",
																function() {
																	window.open(gDetailedUrlParam+ "?tsn="+ data.tsn);

																});

											}

										});

						/* hide header, but somethinge is there */
						$('#species-data').on(
								'draw.dt',
								function() {
									$('.dataTables_scrollBody thead tr')
											.addClass('hidden');

								});

						$("#searchSpeciesBtn").bind(
								"click",
								function() {
									$("#dataBody").empty();

									speciesTable.columns(2).search(
											$("#searchSpecies").val()).draw();

								});

						/*--------------------------------------------------------------
						 * search Summary values
						 * --------------------------------------------------------------*/

						$(".summary-col .filtering span").click(function(event) {
							searchByCellValue(event, this, columnSummary);

						});

						/*--------------------------------------------------------------
						 * search common name values
						 * --------------------------------------------------------------*/
						$(".common-name-col .filtering span").click(
								function(event) {
									searchByCellValue(event, this,
											columnCommonName);

								});
						/*--------------------------------------------------------------
						 * search scientific name values
						 * --------------------------------------------------------------*/
						$(".scientific-name-col .filtering span").click(
								function(event) {
									searchByCellValue(event, this,
											columnScientificName);

								});

						$("#searchSpecies").autocomplete({
							source : function(request, response) {

								$.ajax({

									url : searchAutoCompleteUrl,

									dataType : "json",

									data : {

										mask : request.term,
										groupId : gSpeciesGroupId

									},

									success : function(data) {

										response(data);

									}

								});

							},

							minLength : 2
						// do autocomplete when at least minLength Characters
						// are entered.

						}); // end of autocomplete

					} // end of check data block

					function searchByCellValue(event, cellObj, cellIndex) {
						event.preventDefault();
						event.stopPropagation();
						if ($(cellObj).hasClass("active")) {
							return;
						}
						$(cellObj).siblings().removeClass("active");
						$(cellObj).addClass("active");
						if ($(cellObj).hasClass("all-selected")) {
							speciesTable.columns(cellIndex).search("").draw();
							return;
						}
						var searchVal = $(cellObj).text();
						speciesTable.columns(cellIndex).search(searchVal)
								.draw();

					}
				}); // end of ready function
