var rootPath = "/.ajax/bioBrowser/"
var searchAutoCompleteUrl = rootPath + "geAutoCompeleteSpecies";
var speciesListUrl = rootPath + "getSpeciesList";


//

// DataTables initialisation

//

if ($("#data-block").length > 0){
    var fullCount = 0;
    var basicCount = 0;
    var dataTable;
    
$(document).ready(function() {


	$.get( speciesListUrl , {groupName:gSpeciesGroup},
				displayDataTable);
		
	

	$("#searchSpecies").autocomplete({
		source : function(request, response) {

			$.ajax({

				url : searchAutoCompleteUrl,

				dataType : "json",

				data : {

					mask : request.term

				},

				success : function(data) {

					response(data);

				}

			});

		},

		minLength : 2

	});

});

/*
 * 	"data" : "image",
		"orderable" : false,
		"searchable" : false
	}, {
		"data" : "summary_type",
		"searchable" : false
	}, {
		"data" : "common_name",
		"searchable" : true
	}, {
		"data" : "scientific_name",
		"searchable" : true
	} ];
	
	  <tr>
					        <td width="12%"><div class="pagetitleimg"><img src="assets/birds-sm.jpg" alt=""></div></a></td>
				            <td><a href="#" class="basic">Basic</a></td>
				            <td><a href="#">Bird Common Name1</a></td>
				            <td><a href="#">Scientific Name5</a></td>
				        </tr>
 */
function displayDataTable(data){

	$.each(data.data, function(index, val) {
		if (val.protocol_location) {
	
			 var htmlStr = '<tr class="element-item ' + val.protocol_location + '" data-category="' + val.protocol_location + '">';
			 htmlStr += '<td width="12%"><a href="#"><div class="pagetitleimg"><img src="' + val.image + '" alt=""></div></a></td>';
			 htmlStr += '<td><a href="#" class="'+ val.summary_type.toLowerCase()+ '">'+ val.summary_type + '</a></td>';
			 htmlStr += '<td><a href="#">' + val.common_name + '</a></td>';
			 htmlStr += '<td><a href="#">' + val.scientific_name + '</a></td>';
			 htmlStr += '<td><a href="#">' + val.protocol_location + '</a></td>';
			 htmlStr +='</tr>';
		}		
		else{
			 var htmlStr = '<tr>';
			 htmlStr += '<td width="12%"><a href="#"><div class="pagetitleimg"><img src="' + val.image + '" alt=""></div></a></td>';
			 htmlStr += '<td><a href="#" class="'+ val.summary_type.toLowerCase()+ '">'+ val.summary_type+ '</a></td>';
			 htmlStr += '<td><a href="#">' + val.common_name + '</a></td>';
			 htmlStr += '<td><a href="#">' + val.scientific_name + '</a></td>';
			
			 htmlStr +='</tr>';
		}
		$("#data").append(htmlStr);
	});
	
	
	dataTable= $('#data').dataTable({
		 scrollY:        '80vh',

	        scrollCollapse: true,

	        paging:         false,
		searching: false,
		"info": false,
	    "aaSorting": [[0, "desc"]]
	});
	
	
	$("#searchSpeciesBtn").bind("click", function(event,dataTable) {
		$("#dataBody").empty();

		dataTable.search($("#searchSpecies").val()).draw();
	});

	$("#terrestrial-filter").bind("click", function(event,dataTable) {
	

		$("#data").dataTable().search("terrestrial").draw();
	});
	$("#aquatic-filter").bind("click", function(event,dataTable) {
	

		dataTable.search("aquatic").draw();
	});

	$("#all-filter").bind("click", function(event,dataTable) {
		

		dataTable.search("").draw();
	});
}

}

