/* -----------------------------------------------------------------------


----------------------------------------------------------------------- */
var rawdataDownloadBtn = "#final-data-download";
var loadingDiv = "#download-msg";
$(document).ready(function() {

	if ($("#species-habitat-download-component").length) {
		initRawdataOptions();
	}
});

function initRawdataOptions() {
	/* use ajax get menu and display them */

	var ajax1 = SendAjax("/.ajax/rawdata/getDataCategory", null,
			addDataCategory);
	var ajax2 = SendAjax("/.ajax/rawdata/getRegion", null, addRegion);
	var ajax3 = SendAjax("/.ajax/rawdata/getRotation", null, addRotation);
	var ajax4 = SendAjax("/.ajax/rawdata/getOffgridOption", null,
			addOffgridOption);

	$.when(ajax1, ajax2, ajax3, ajax4).done(initMenuHandler);

}
/*
 * ----------------------------------------------- track ajax so we know when
 * they are all finished -------------------------------------------------
 */
function SendAjax(url, param, callback, failCallback) {
	var def = $.Deferred();
	$.ajax({
		type : "GET",

		url : url,
		data : param,
		cache : false,
		dataType : "json"
	}).done(function(data) {
		callback(data);
		def.resolve();
	}).fail(function(data) {

		failCallback(data);
		def.resolve();
	});

	return def.promise();
}

/*-------------------------------------------------
 * attach actions 
 */
function initMenuHandler() {

	if ($(".tree").length > 0) {
		$('.tree').checktree();
	}

	$(".filter-sec .toggle-bar").unbind().on(
			'click',
			function(event) {
				$(this).toggleClass("open");
				$(this).parents(".selection-sec").children(".choose-filters")
						.slideToggle();
			});

	$(".has-child .toggle-bar").unbind().on('click', function(event) {
		$(this).toggleClass("open");
		$(this).parent(".has-child").children(".sub-selection").toggle();
	});

	$(".clear-all").unbind().on(
			'click',
			function(event) {
				$(this).parents(".selection-sec").find(
						".choose-filters input[type=checkbox]").removeAttr(
						'checked').change();
			});
	$(".clear-selected").unbind().on(
			'click',
			function(event) {
				$(this).parents(".select-cat").find(
						".select-from input[type=checkbox]").removeAttr(
						'checked').change();
			});
	// collapse region
	$("#regiondetail").find(".sub-selection").toggle();

	// when offgrid selected, prototype, vice versa
	// $("#rotation1").on("change", function(){ // rotation 1 is prototype
	// childLi.prop('checked', c.is(':checked'));
	// });
	// when repeat visits selected, disable rotation, vice versa
	/*---------------------------------------------------------------------------------
	 *  depends on user selection, show or hide it related options.
	 *  e.g. when choose survery period, hide repeated survey options. 
	 ---------------------------------------------------------------------*/

	/*
	 * $("#SurveyPeriod").parent().find(":input").on( "change", function() { //
	 * rotation 1 is prototype if ($(this).is(':checked')) {
	 * 
	 * $("#Repeat-Surveys").parent().find(":input").prop( 'checked',
	 * false).change(); } });
	 * 
	 * $("#datatype").parent().find(":input").on( "change", function() { //
	 * rotation 1 is prototype if ($(this).is(':checked')) {
	 * 
	 * $("#Repeat-Surveys").parent().find(":input").prop( 'checked',
	 * false).change();
	 * 
	 * $("#data-type2-summary").hide(); $("#rotation-summary").show();
	 * $("#data-type1-summary").show(); } });
	 * 
	 * $("#Repeat-Surveys").parent().find(":input").on( "change", function() { //
	 * rotation // 1 is // prototype if ($(this).is(':checked')) {
	 * 
	 * $("#SurveyPeriod").parent().find(":input").prop('checked',
	 * false).change(); $("#datatype").parent().find(":input").prop('checked',
	 * false).change();
	 * 
	 * $("#rotation-summary").hide(); $("#data-type1-summary").hide();
	 * $("#data-type2-summary").show(); }
	 * 
	 * });
	 */

	/*-----------------------------------------------------
	 * 
	 * when terrestrial selected at step 3, select all terrestrial at step 4,
	// same for wetland
	-------------------------------------------------------------------*/
	$("#step3-filter :input[type='checkbox']").on(
			"change",
			function() { // rotation 1 is prototype
				var id = $(this).attr('id').toLowerCase();
				if (id) {
					$("#step4-filter ." + id + " :input").prop('checked',
							$(this).is(':checked')).change();
					// toggle
					if ($(this).is(':checked')) {
						$("#step4-filter ." + id).show();
					} else {
						$("#step4-filter ." + id).hide();
					}
				}

			});

	/*-------------------------------------------------------------------------------------------------  
	 *  control over multiple regions, only one regions can be selected 
	 *  *-----------------------------------------------------*/
	$("#regiondetail :input[type='checkbox']").on(
			"change",
			function() {
				// if one region selected, unselect all other regions at
				// different region categories (nature region, WPAC, LUF, etc).
				if ($(this).is(':checked')) {
					$(this).parents(".level1list").siblings().find(":input")
							.prop("checked", false);

				}
			});

	/*------------------------------------------------------------------------------------------------- 
	 *  for all the changes made in input checkbox, update the summary table 
	 *  ------------------------------------------------------------------------------------------------- */
	$("#species-habitat-download-component :input[type='checkbox']").on(
			"change",
			function() {
				// first check if it is a tree

				var inputName = $(this).prop('name');

				/*---------------------------------------------------------------------------------
				 *  depends on user selection, show or hide it related options.
				 *  e.g. when choose survery period, hide repeated survey options. 
				 ---------------------------------------------------------------------*/

				if ("rotation" == inputName) {

					if ($('input[name=' + inputName + ']').is(':checked')) {

						$("#Repeat-Surveys").parent().find(":input").prop(
								'checked', false);
					}

				} else if ("data-type1" == inputName) {

					if ($('input[name=' + inputName + ']').is(':checked')) {

						$("#Repeat-Surveys").parent().find(":input").prop(
								'checked', false);

						$("#data-type2-summary").hide();
						$("#rotation-summary").show();
						$("#data-type1-summary").show();
					}
				} else if ("data-type2" == inputName) {

					if ($('input[name=' + inputName + ']').is(':checked')) {

						$("#SurveyPeriod").parent().find(":input").prop(
								'checked', false);
						$("#datatype").parent().find(":input").prop('checked',
								false).change();

						$("#rotation-summary").hide();
						$("#data-type1-summary").hide();
						$("#data-type2-summary").show();
					}
				}

				updateOptions(inputName, inputName + "-summary");

			});

	// download
	// $(rawdataDownloadBtn).unbind();
	// $(rawdataDownloadBtn).click(downloadAction);

	/*--------------------------------------------------------------
	 * bind action to download buttons
	 --------------------------------------------------------------*/
	$(rawdataDownloadBtn).click(function(e) {
		if (bDownloading) {
			return;
		}
		bDownloading = true;
		e.preventDefault();
		$(this).progressTimed(1, downloadAction);
	});

	/*--------------------------------------------------------------
	 * 
	 * default download everything
	 * 
	 * --------------------------------------------------------------*/
	$("#first .direct-download").click(
			function() {

				/* download all make selections*/

				$('#progressbar li').addClass('visited-step');
				/* set default values to download all */
				$("#SurveyPeriod").prop('checked', true).change();
				$("#datatype").prop('checked', true).change();

				$("input.level1region").prop('checked', false).change();
				$("#region").prop('checked', true).change(); // select
				// alberta
				// region
				$("#Terrestrial").prop('checked', true).change();
				$("#Wetland").prop('checked', true).change();

				// show tabs
				$(this).parents(".selection-sec").siblings("#fifth").fadeIn(
						'slow');
				$(this).parents(".selection-sec").css({
					'display' : 'none'
				});
				
				// Adding Class selected-step To Show Steps Forward;
				$('#progressbar li:last-child').addClass('selected-step');
				$('#progressbar li:last-child').prevAll().addClass(
						'selected-step selectedSec');
			});

	/*----------------------------------------------------------------------------------------------------------------------------
	 * 
	 * direct download at step 3, i.e. download all terrestrial/wetland data
	 * 
	 ----------------------------------------------------------------------------------------------------------------------------*/
	$("#third .direct-download").click(
			function() {
				$(this).parents(".selection-sec").siblings("#fifth").fadeIn(
						'slow');
				$(this).parents(".selection-sec").css({
					'display' : 'none'
				});
				// Adding Class selected-step To Show Steps Forward;
				$('#progressbar li:last-child').addClass('selected-step');
				$('#progressbar li:last-child').prevAll().addClass(
						'selected-step selectedSec');
				$('#progressbar li').addClass('visited-step');
				// move to step 5

			});

	/*----------------------------------------------------------------------------------------------------------------------------
	 * 
	 * set default selection
	 * 
	 ----------------------------------------------------------------------------------------------------------------------------*/

	$("#SurveyPeriod").prop('checked', true).change();
	$("#datatype").prop('checked', true).change();

	/*
	 * -------------------------------------------------- init popover (hover)
	 * events
	 * 
	 * ---------------------------------------------------
	 */
	$('[data-toggle="popover"]').popover({
		html : true
	});

	$(".alert-box button").click(function() {
		$(this).parent(".alert-box").hide();
	});
	/*------------------------------
	 *  hide all warning message 
	 *  ----------------------*/
	$(".alert-area").hide();

	/*------------------------------------------------------------
	 *  bind next buttons with validation
	 *  ------------------------------------------------------------ */
	$(".next_btn").bind("click", function() {
		var id = $(this).attr("id");
		if (validateNext(id)) {
			$(this).parents(".selection-sec").next().fadeIn('slow');
			$(this).parents(".selection-sec").css({
				'display' : 'none'
			});
			// Adding Class selected-step To Show Steps Forward;
			$('.selected-step').next().addClass('selected-step');
			$('.selected-step').prev().addClass('selectedSec');
		}
	});
	
	$(".restart").bind("click", function(){ // same as clicking step 1.
		$("#first").fadeIn('slow');
		$("#second, #third, #fourth, #fifth").css({
		'display': 'none'
		});
		// Removing Class Active To Show Steps Backward;
		$('.step2, .step3, .step3, .step4, .step5').removeClass('selected-step');
		$('#progressbar li').removeClass('selectedSec');
		$(".restart").hide();
		$(rawdataDownloadBtn).show();
		$(loadingDiv).empty();
		
	});
}

/*--------------------------------------------------------------------
 * when user click next, check if they select enough options
 ------------------------------------------------------*/
function validateNext(id) {
	$(".alert-area").hide();
	var message;
	var bValid = true;
	if (id == "step2-btn-next") {

		var dataCategory2 = $("#data-type-detail2 input:checked").length > 0;
		if (!dataCategory2) { // if not select repeated visits, then need to
			// check if rotation and data type are selected
			var dateCateogry = $("#data-type-detail1 input:checked").length > 0;
			var rotation = $("#survey-periond-detail input:checked").length > 0;

			if (!(dateCateogry && rotation)) {

				$("#site-rotation-checkbox .alert-area").show();
				bValid = false;
			}

		}

		var regionCheck = $("#regiondetail input:checked").length > 0;
		if (!regionCheck) { // if not select repeated visits, then need to
			// check if rotation and data type are selected
			$("#region-checkbox .alert-area").show();
			bValid = false;

		}

	} else if (id == "step3-btn-next" || id == "step3-direct-download") {

		var selection = $("#step3-filter input:checked").length > 0;

		if (!selection) { // if not select repeated visits, then need to
			// check if rotation and data type are selected
			$("#third .alert-area").show();
			bValid = false;

		}

	} else if (id == "step4-btn-next") {

		var selection = $("#step4-filter input:checked").length > 0;

		if (!selection) { // if not select repeated visits, then need to
			// check if rotation and data type are selected
			$("#fourth .alert-area").show();
			bValid = false;

		}
		
		$(rawdataDownloadBtn).show();

	}

	return bValid;
};

/*-----------------------------------------------------------------
 *  get option values, and update summary table on step 5
 -----------------------------------------------------------------*/
var updateOptions = function(thisName, thisID) {
	// console.log('Array: '+ thisName );

	var vals = '';

	$('input[name=' + thisName + ']').each(
			function() {

				if ($(this).prop('checked')
						&& !$(this).hasClass("excludeFromSummary")) {

					// console.log( $(this).val() );
					if (thisName == "region") {
						vals += "<li class='" + $(this).attr("class") + "'>"
								+ $(this).next().text() + "</li>";
					} else {
						vals += "<li>" + $(this).next().text() + "</li>";
					}

				}
			});

	// console.log( 'to id '+ thisID + '' + vals );
	if (vals.length == 0) {
		vals = '<li>none selected</li>';
	}
	$('#data-summary-details #' + thisID + " ul").html(vals);
//	console.log(vals);
}

/*--------------------------------------------------------
 * add rotation list into hmtl * 
 --------------------------------------------------------*/
function addRotation(data) {
	var contentDiv = $("#survey-periond-detail");
	for (var i = 0; i < data.length; i++) {
		// region and name

		var row = data[i];
		var innerHtml = "<li > <input type='checkbox'  name='rotation' id='rotation"
				+ row.id
				+ "' value='rotation"
				+ row.id
				+ "'>"
				+ "<label class='data-hover' data-toggle='popover' data-trigger='hover' data-placement='right' data-content='"
				+ row.note
				+ "' for='rotation"
				+ row.id
				+ "' >"
				+ row.name
				+ "</label>";

		innerHtml += "</li> ";

		$(contentDiv).append(innerHtml);
	}

}

/*******************************************************************************
 * add offgrid option list into hmtl * **********************************
 */

function addOffgridOption(data) {
	for (var i = 0; i < data.length; i++) {
		// region and name

		var row = data[i];
		var contentDiv = $("#data-type-detail" + row.category);

		var innerHtml = "<li > <input type='checkbox'  name='data-type"
				+ row.category
				+ "' id='data-type"
				+ row.id
				+ "' value='data-type"
				+ row.id
				+ "' >"
				+ "<label class='data-hover' data-toggle='popover' data-trigger='hover' data-placement='right' data-content='"
				+ row.note + "' for='data-type" + row.id + "' >" + row.name
				+ "</label>";

		innerHtml += "</li> ";

		$(contentDiv).append(innerHtml);
	}

}

/*
 * add wetland terrestrial variables
 */
function addDataCategory(data) {
	// alert("add data!");
	for (var i = 0; i < data.length; i++) {
		// protocol and name
		var currentProtocol = data[i].protocol.toLowerCase();
		var speciesHabitatName = data[i].name.toLowerCase();
		var currentProtocolDiv = "#step4-filter ." + currentProtocol;
		if ($(currentProtocolDiv).length == 0) {// need to create first if it is
			// not wetland or terrestrial

			// copy terrestrial
			$("#step4-filter .terrestrial").clone().appendTo(
					$(".choose-filters"));

			$("#step4-filter .terrestrial:last").removeClass("terrestrial")
					.addClass(currentProtocol);
			/* add to step 3 in case we add more category later */
			var innerHtml = "<li><input  id='"
					+ currentProtocol
					+ "' type='checkbox' name='check' value='"
					+ currentProtocol
					+ "' /><label class='data-hover' data-toggle='popover' data-trigger='hover' data-placement='right' data-content='"
					+ data[i].note + "' for='" + currentProtocol + "'>'"
					+ currentProtocol + "'</label></li>";

			$("#step3-filter ul.tree").append(innerHtml);
		}

		// already exist then add data category

		for (var j = 0; j < data[i].detail.length; j++) {
			var subData = data[i].detail[j];
			var innerHtml = "<li> <input type='checkbox' id='category"
					+ subData.id + "' name='" + currentProtocol + "-"
					+ speciesHabitatName + "' value='category" + subData.id
					+ "' > <label for='category" + subData.id + "' >"
					/* terrestrial-species */

					+ subData.name + "</label> " + "</li>";

			// append below the liTitle <species habitat>
			$(
					currentProtocolDiv + " ." + speciesHabitatName
							+ " ul.sub-selection").append(innerHtml);

		}

	}

}

/*-------------------------------------------------------
 * add regions
 * 
 * @param data
 * 
 -------------------------------------------------------*/
function addRegion(data) {

	var regionDiv = "#regiondetail";

	for (var i = 0; i < data.length; i++) {
		// region and name

		var row = data[i];
		if (row.main == 1) { // only include the region if the main flag ==1,
			// e.g. nature subregion not included in main
			// category, it will be added to nature region
			var innerHtml = '<li class="level1list has-child">'
					+ '<span class="toggle-bar"></span>'
					+ '<input  class="level1region '
					+ (row.coverWholeAlberta == 1 ? "" : "partOfAlberta")
					+ '" id="mainregion' + row.id
					+ '" type="checkbox" name="region" value="region' + row.id
					+ '" />' + '<label for="mainregion' + row.id + '">'
					+ row.name + '</label>'; // level 1 region name: e.g.
			// natural region

			if (row.subregion.length) {
				innerHtml += ' <ul class="sub-selection"></ul> ';
			}

			$(regionDiv).append(innerHtml);
		}

		// already exist then add detailed region names
		for (var j = 0; j < row.subregion.length; j++) {
			var subData = row.subregion[j];

			// add subregion first.

			if (subData.parentid) {
				// add ul under main region if not exists
				if ($("#region" + subData.parentid).parent().find(
						".sub-selection").length == 0) {
					$(" #region" + subData.parentid).parent().prepend(
							'<span class="toggle-bar open"></span>');
					$(" #region" + subData.parentid).parent().append(
							'<ul class="sub-selection">');
				}

				var innerHtmlLevel3 = "<li class='level3list' > <input class='level3region' type='checkbox' value='region"
						+ subData.id

						+ "' name='region'  id='region"
						+ subData.id
						+ "'>"
						+ "  <label for='region"
						+ subData.id
						+ "'>"
						+ subData.name + "</li>"; // level
				// 2
				// region
				// name:
				// e.g.
				// boreal

				$("#region" + subData.parentid).parent().find(".sub-selection")
						.first().append(innerHtmlLevel3);
				$("#region" + subData.parentid).parent().addClass("has-child");

			} else if ($(" #region" + subData.id).length == 0) {

				var innerHtmlLevel2 = "<li  class='level2list'> <input class='level2region' type='checkbox' value='region"
						+ subData.id
						+ "' name='region'  id='region"
						+ subData.id
						+ "'>"
						+ "  <label for='region"
						+ subData.id + "'>" + subData.name + "</li>"; // level
				// 2
				// region
				// name:
				// e.g.
				// boreal

				$("#mainregion" + row.id).parent().find(".sub-selection")
						.first().append(innerHtmlLevel2);

			}

		}

	}

}

var bDownloading = false;
function downloadAction() {
	console.log(new Date().getTime() + ":generate zip");
	// get parameters
	// $(rawdataDownloadBtn).hide();

	$(loadingDiv).text("Your Data Download Is In Progress!");
	var params = "";
	var dataStr = getCheckBoxResult("rawdata", "terrestrial-species",
			"category")
			+ getCheckBoxResult("rawdata", "terrestrial-habitat", "category")
			+ getCheckBoxResult("rawdata", "wetland-habitat", "category")
			+ getCheckBoxResult("rawdata", "wetland-species", "category");
	if (dataStr.length < 5) {
		alert("Please select at least one species or habitat data.");
		$(rawdataDownloadBtn).show();
		$(loadingDiv).empty();
		return;
	}
	params = dataStr
			+ getRegionCheckBoxResult("#region-checkbox", "region", "region",
					"region", 1) + getCheckBoxResult("rotation")
			+ getDataTypeCheckBoxResult("offgridId", "data-type2", "data-type")
			+ getDataTypeCheckBoxResult("offgridId", "data-type1", "data-type") // not
			// a
			// array
			// it is
			// a
			// single
			// value
			+ ";jsessionid=" + new Date().getTime();

	// send request
	$.ajax({
		type : "GET",

		url : "/.ajax/rawdata/getRawdataFile",
		data : params,

		async : false,
		dataType : "json"
	}).done(function(data) {
		getRawdata(data);
		bDownloading = false;
		
	

	}).fail(function(data) {
		console.log("failed");
		bDownloading = false;
	});

	// $.ajax("/.ajax/rawdata/getRawdataFile", params,
	// getRawdata, function(){$(rawdataDownloadBtn).show();
	// $(loadingDiv).text("Download Failed");
	// });
	// download files
}

// when download successful}

function getRawdata(data) {
	var htmlStr = '<a id="download-rawdata" class="download-button" href="/FileDownloadServlet?filename='
			+ data.file
			+ '">Raw Data File <em>(zip format '
			+ data.filesize
			+ ')</em></a>';

	// $(rawdataDownloadBtn).show();
	$(loadingDiv).html(htmlStr);
	window.open("/FileDownloadServlet?filename=" + data.file, "_blank");
	$(rawdataDownloadBtn).hide()
	$(rawdataDownloadBtn).text("Download Selected Data");
	$(".restart").css({"display":"inline-block"});
	
	/*
	 * $("#download-rawdata").unbind(); $("#download-rawdata").click(
	 * function(event) { // when someone clicks these links var href =
	 * $(this).attr("href"); var target = $(this).attr("target");
	 * 
	 * event.preventDefault(); // don't open the link yet var queryStr = ""; var
	 * divs = [ "terrestrial-species", "terrestrial-habitat", "wetland-species",
	 * "wetland-habitat", "async-region", "async-years" ]; $.each(divs,
	 * function(index, row) { if ($("#" + row + " .noselect").length == 0) {
	 * queryStr += $("#" + row).text() + ";"; } }); ga('send', 'event', 'link',
	 * 'download', "Raw Data:" + queryStr + "]");
	 * 
	 * var thisEvent = $(this).attr("onclick"); setTimeout(function() { // now
	 * wait 300 milliseconds eval(thisEvent); // and continue with the onclick
	 * event window.open(href, (!target ? "_self" : target)); // and // open //
	 * the link // as usual }, 300); });
	 */
}

function getCheckBoxResult(param, name, valuePrefix) {
	var selected = "";

	var divname = (name ? name : param);
	var prefix = (valuePrefix ? valuePrefix : divname)
	$("#species-habitat-download-component input[name=" + divname + "]:checked")
			.each(function() {
				if (!$(this).hasClass("excludeFromSummary")) {
					var idStr = $(this).val();
					var idInt = idStr.substring(prefix.length, idStr.length);
					selected += param + "=" + idInt + "&";
				}
			});
	return selected;
}

/*
 * $("input:checkbox:not(:checked)") for regions, if all subregions are
 * selected, then don't include any subregion, directly use parent id.
 */
function getRegionCheckBoxResult(parentObj, param, name, valuePrefix,
		regionIndex) {
	var selected = "";
	console.log("check" + regionIndex + " under " + parentObj);
	var divname = (name ? name : param);
	var prefix = (valuePrefix ? valuePrefix : divname)
	var className = "level" + regionIndex + "region";
	$(parentObj)
			.find("input." + className + "[name=" + divname + "]:checked")
			.each(
					function() {
						if (!$(this).hasClass("excludeFromSummary")) {
							// first check if it has any children
							if ($(this).hasClass("partOfAlberta")
									|| ($(this).siblings("ul.sub-selection")
											.find("input:checkbox").length > 0 && $(
											this)
											.siblings("ul.sub-selection")
											.find(
													"input:checkbox:not(:checked)").length > 0)) { // some
								// are
								// uncheck
								selected += getRegionCheckBoxResult($(this)
										.siblings("ul.sub-selection"), param,
										name, valuePrefix, regionIndex + 1);

							} else if (regionIndex > 1) { /*
															 * if it is level 1
															 * region, and all
															 * children
															 * subregions are
															 * checked, then
															 * don't record it,
															 * as it meant for
															 * whole alberta ,
															 * unless there are
															 * some regions that
															 * don't cover the
															 * whole alberta,
															 * e.g. oilsand
															 */
								var idStr = $(this).val();

								var idInt = idStr.substring(prefix.length,
										idStr.length);
								selected += param + "=" + idInt + "&";
							}
						}
					});
	return selected;
}
/**
 * data type is not one value not an array. if users choose 1 and 2, we will
 * combine them to value 3.
 * 
 * @param param
 * @param name
 * @param valuePrefix
 * @returns {String}
 */
function getDataTypeCheckBoxResult(param, name, valuePrefix) {
	var selected = "";
	var sumId = 0;
	var divname = (name ? name : param);
	var prefix = (valuePrefix ? valuePrefix : divname)
	$("#species-habitat-download-component input[name=" + divname + "]:checked")
			.each(
					function() {
						if (!$(this).hasClass("excludeFromSummary")) {
							var idStr = $(this).val();
							var idInt = parseInt(idStr.substring(prefix.length,
									idStr.length));
							sumId += idInt
						}
					});
	if (sumId > 0) {
		return param + "=" + sumId + "&";
	} else
		return "";
}