/*document.write('<script type="text/javascript" src="js/jquery.dataTables.js"></script>');
document.write('<script type="text/javascript" src="js/jquery.bxslider.js"></script>');
document.write('<script type="text/javascript" src="js/waypoints.min.js"></script>');
document.write('<script type="text/javascript" src="js/jquery.counterup.min.js"></script>');
document.write('<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>');
 */
$(window).load(function() {
	$('#data').dataTable({
		paging : false,
		searching : false,
		"info" : false,
		"aaSorting" : [ [ 0, "desc" ] ]
	});

});// ]]>

/* Print button Function */
function myPrintFunction() {
	window.print();
}

/* SP Menu scroll and active */
/*
 * var sections = $('.sec-title') , nav = $('nav') , nav_height =
 * nav.outerHeight();
 * 
 * $(window).on('scroll', function () { var cur_pos = $(this).scrollTop();
 * 
 * sections.each(function() { var top = $(this).offset().top - nav_height,
 * bottom = top + $(this).outerHeight();
 * 
 * if (cur_pos >= top && cur_pos <= bottom) {
 * nav.find('a').parent("li").removeClass('active');
 * sections.removeClass('active');
 * 
 * $(this).parent("li").addClass('active');
 * nav.find('a[href="#'+$(this).attr('id')+'"]').parent("li").addClass('active'); }
 * }); });
 * 
 * nav.find('a').on('click', function () { var $el = $(this) , id =
 * $el.attr('href');
 * 
 * $('html, body').animate({ scrollTop: $(id).offset().top - nav_height }, 500);
 * 
 * return false; });
 * 
 */

jQuery(document)
		.ready(
				function($) {

					$('[data-toggle="popover"]').popover({
						html : true
					});

					$(".alert-box button").click(function() {
						$(this).parent(".alert-box").hide();
					});
//
					$(".summary .static-sorting").click(function() {

						$(this).toggleClass("open");
						// $(this).parent(".sort-title").next(".filtering").toggleClass("open");
					});
					$(".static-sorting").click(
							function() {
								if ($(this).hasClass("sorting_asc")) {
									$(this).removeClass("sorting_asc")
											.addClass("sorting_desc");
								} else {
									$(this).removeClass("sorting_desc")
											.addClass("sorting_asc");
								}

							});

					$(".selectionLink").click(
							function() {
								$(".select-list").toggle();
								$(".select-list").toggleClass("open");
								$(".select-download .downloaddata-btn").click(
										function() {
											if ($(".select-list").hasClass(
													"open")) {
												$(".select-list").hide();
												$(".select-list").removeClass(
														"open");
											}
										});

								/*
								 * if ($(window).width() > 767) { // Single Data
								 * Page set height for image block var
								 * h_single_data =
								 * $(".single-data").outerHeight()
								 * $('.single-data-img').css('height',
								 * h_single_data); }
								 */
							});

					/* Species page */
					$(".togglelink h4, .togglelink span").click(function() {
						$(this).next(".togglesec").toggle();
						$(this).toggleClass("open");
					});
					if ($(window).width() > 1025) {
						// Set equal Height

						var ar = $.makeArray()
						$('.species-banner-height').each(function() {
							ar.push($(this).outerHeight());
						});
						$('.species-banner-height').css('height',
								Math.max.apply(Math, ar));

					} else {

						var seth = $(".s-profilepic").height();
						$(".map-portal-sec").css('height', seth + "px");
					}
					$(window).resize(function() {
						if ($(window).width() < 1025) {
							var imgsecH = $(".s-profilepic").height();
							$(".map-portal-sec").css('height', imgsecH + "px");

						}

					});

					
					/*---------------------------------------------------------*/
			/*		$("#final-data-download").click(
							function() {
								$("#download-status").text(
										"Your Data Download in Progress!");

							});

					$("#progressbar li.step2").click(
							function() { // Function Runs On next
								// number Button Click
								if ($(".step1").hasClass("selected-step")
										&& !$(this).hasClass("selected-step")) {
									$("#second").fadeIn('slow');
									$("#first").css({
										'display' : 'none'
									});
									// Adding Class selected-step To Show Steps
									// Forward;
									$('.step2').addClass('selected-step');
									$('.step1').addClass('selectedSec');
								}
							});

					$("#progressbar li.step3").click(
							function() {
								if ($(".step2").hasClass("selected-step")
										&& !$(this).hasClass("selected-step")) {

									if (validateNext("step2-btn-next")) {
										$("#third").fadeIn('slow');
										$("#second").css({
											'display' : 'none'
										});
										$('.step3').addClass('selected-step');
										$('.step2').addClass('selectedSec');
									}
								}

							});
					$("#progressbar li.step4").click(
							function() {
								if ($(".step3").hasClass("selected-step")
										&& !$(this).hasClass("selected-step")) {
									if (validateNext("step3-btn-next")) {
										$("#fourth").fadeIn('slow');
										$("#third").css({
											'display' : 'none'
										});
										// Adding Class selected-step To Show
										// Steps Forward;
										$('.step4').addClass('selected-step');
										$('.step3').addClass('selectedSec');
									}
								}
							});
					$("#progressbar li.step5").click(
							function() {
								if ($(".step4").hasClass("selected-step")
										&& !$(this).hasClass("selected-step")) {
									if (validateNext("step4-btn-next")) {
										$("#fifth").fadeIn('slow');
										$("#fourth").css({
											'display' : 'none'
										});
										// Adding Class selected-step To Show
										// Steps Forward;
										$('.step5').addClass('selected-step');
										$('.step4').addClass('selectedSec');
									}
								}
							});

					$("#progressbar li")
							.click(
									function() {
										if ($(this).hasClass("selectedSec")) {

											if ($(this).hasClass("step1")) {
												$("#first").fadeIn('slow');
												$(
														"#second, #third, #fourth, #fifth")
														.css({
															'display' : 'none'
														});
												// Removing Class Active To Show
												// Steps Backward;
												$(
														'.step2, .step3, .step3, .step4, .step5')
														.removeClass(
																'selected-step');
												$('#progressbar li')
														.removeClass(
																'selectedSec');

											} else if ($(this)
													.hasClass("step2")) {
												$("#second").fadeIn('slow');
												$(
														"#first, #third, #fourth, #fifth")
														.css({
															'display' : 'none'
														});
												// Removing Class Active To Show
												// Steps Backward;
												$('.step3, .step4, .step5')
														.removeClass(
																'selected-step');
												$(
														'.step2, .step3, .step3, .step4, .step5')
														.removeClass(
																'selectedSec');

											} else if ($(this)
													.hasClass("step3")) {
												$("#third").fadeIn('slow');
												$(
														"#first, #second, #fourth, #fifth")
														.css({
															'display' : 'none'
														});
												// Removing Class Active To Show
												// Steps Backward;
												$('.step4, .step5')
														.removeClass(
																'selected-step');
												$('.step3, .step4, .step5')
														.removeClass(
																'selectedSec');

											} else if ($(this)
													.hasClass("step4")) {
												$("#fourth").fadeIn('slow');
												$(
														"#first, #second, #third, #fifth")
														.css({
															'display' : 'none'
														});
												// Removing Class Active To Show
												// Steps Backward;
												$('.step5').removeClass(
														'selected-step');
												$('.step4, .step5')
														.removeClass(
																'selectedSec');
											}
										}
									}); */
					/*
					 * $("#first .direct-download").click(function() {
					 * $(this).parents(".selection-sec").siblings("#fifth").fadeIn('slow');
					 * $(this).parents(".selection-sec").css({ 'display': 'none'
					 * }); // Adding Class selected-step To Show Steps Forward;
					 * $('#progressbar
					 * li:last-child').addClass('selected-step');
					 * $('#progressbar
					 * li:last-child').prevAll().addClass('selected-step
					 * selectedSec'); });
					 * 
					 * $("#third .direct-download").click(function() {
					 * $(this).parents(".selection-sec").siblings("#fifth").fadeIn('slow');
					 * $(this).parents(".selection-sec").css({ 'display': 'none'
					 * }); // Adding Class selected-step To Show Steps Forward;
					 * $('#progressbar
					 * li:last-child').addClass('selected-step');
					 * $('#progressbar
					 * li:last-child').prevAll().addClass('selected-step
					 * selectedSec'); });
					 */
					/*
					 * $(".next_btn").click(function() { // Function Runs On
					 * NEXT Button Click
					 * $(this).parents(".selection-sec").next().fadeIn('slow');
					 * $(this).parents(".selection-sec").css({ 'display': 'none'
					 * }); // Adding Class selected-step To Show Steps Forward;
					 * $('.selected-step').next().addClass('selected-step');
					 * $('.selected-step').prev().addClass('selectedSec');
					 * 
					 * });
					 */
				/*	$(".pre_btn").click(
							function() { // Function Runs On PREVIOUS Button
								// Click
								$(this).parents(".selection-sec").prev()
										.fadeIn('slow');
								$(this).parents(".selection-sec").css({
									'display' : 'none'
								});
								// Removing Class Active To Show Steps Backward;
								$('.selected-step:last').removeClass(
										'selected-step');
								$('.selected-step:last').removeClass(
										'selectedSec');
							});

					$(".btn-sec input").click(function() {
						var target = $("#top-pos");
						$('body,html').animate({
							'scrollTop' : target.offset().top
						}, 400);
					});

					// Selection script
					$(".filter-sec .toggle-bar").on(
							'click',
							function(event) {
								$(this).toggleClass("open");
								$(this).parents(".selection-sec").children(
										".choose-filters").slideToggle();
							});

					$(".has-child .toggle-bar").on(
							'click',
							function(event) {
								$(this).toggleClass("open");
								$(this).parent(".has-child").children(
										".sub-selection").toggle();
							});

					$(".clear-all").on(
							'click',
							function(event) {
								$(this).parents(".selection-sec").find(
										".choose-filters input[type=checkbox]")
										.removeAttr('checked');
							});
					$(".clear-selected").on(
							'click',
							function(event) {
								$(this).parents(".select-cat").find(
										".select-from input[type=checkbox]")
										.removeAttr('checked');
							}); */

					$('.subNav')
							.each(
									function() {
										var secondaryNav = $('.subNav'),
										// secondaryNavTopPosition =
										// $('.browser-banner').offset().top +
										// $('.browser-banner').height() -
										// $('.data-nav').height();
										// secondaryNavTopPosition =
										// $('.sp-main').offset().top +
										// $('.sp-main').height()
										// - $('.data-nav').height();
										// secondaryNavTopPosition =
										// $('.sp-main-top').offset().top +
										// $('.sp-main-top').height() -
										// $('.data-nav').height(); // joan
										// changed it

										secondaryNavTopPosition = $(
												'.sp-main-top').outerHeight();

										$(window)
												.on(
														'scroll',
														function() {

															// on desktop - fix
															// secondary
															// navigation on
															// scrolling
															if ($(window)
																	.scrollTop() >= secondaryNavTopPosition) {
																// fix secondary
																// navigation
																secondaryNav
																		.addClass('is-fixed');

															} else {
																secondaryNav
																		.removeClass('is-fixed');
															}

														});
										secondaryNav
												.find('ul a')
												.on(
														'click',
														function(event) {
															/*
															 * event.preventDefault();
															 * var target=
															 * $(this.hash);
															 * $('body,html').animate({
															 * 'scrollTop':
															 * target.offset().top -
															 * secondaryNav.height() },
															 * 400 );
															 */
															// on mobile - close
															// secondary
															// navigation
															$(
																	'.subNav .collapse')
																	.removeClass(
																			'in');
															$(
																	'.subNav .navbar-toggle')
																	.addClass(
																			'collapsed');

														});
									});

					var nav_height = $(".subNav").outerHeight();

					$(function() {
						$('#subnavbar a[href*=#]:not([href=#])')
								.click(
										function() {

											if ($(".subNav").hasClass(
													"is-fixed")) {
												if (location.pathname.replace(
														/^\//, '') == this.pathname
														.replace(/^\//, '')
														&& location.hostname == this.hostname) {
													var target = $(this.hash);
													target = target.length ? target
															: $('[name='
																	+ this.hash
																			.slice(1)
																	+ ']');
													if (target.length) {
														$('html,body')
																.animate(
																		{
																			scrollTop : target
																					.offset().top
																					- nav_height
																		}, 1000);
														return false;
													}
												}
											} else {
												if (location.pathname.replace(
														/^\//, '') == this.pathname
														.replace(/^\//, '')
														&& location.hostname == this.hostname) {
													var target = $(this.hash);
													target = target.length ? target
															: $('[name='
																	+ this.hash
																			.slice(1)
																	+ ']');
													if (target.length) {
														$('html,body')
																.animate(
																		{
																			scrollTop : target
																					.offset().top
																					- nav_height
																					- 100
																		}, 1000);
														return false;
													}
												}

											}
										});
					});

					$('#subnavbar li a').click(function() {
						$("li").removeClass("active");
						$(this).parent("li").addClass("active");
					});

					$("#backtoHome").popover({
						html : true,
						placement : 'bottom'
					});

					/* getting viewport width */
					var responsive_viewport = $(window).width();
					var ar = $.makeArray()

					// Set equal Height
					if (responsive_viewport > 992) {

						// Set equal Height
						$('.set-height').each(
								function() {
									ar
											.push($(this).outerHeight()
													+ $(".quickbox-link")
															.outerHeight());
								});
						$('.set-height')
								.css('height', Math.max.apply(Math, ar));

						// For Biodiversity Browser page
						$('.stat-analysis-collaborations h2').each(function() {
							ar.push($(this).outerHeight());
						});
						$('.stat-analysis-collaborations h2').css('height',
								Math.max.apply(Math, ar));

						// Single Data Page set height for image block
						var h_single_data = $(".single-data").outerHeight()
						$('.single-data-img').css('height', h_single_data);

					} /* end larger than 992px */

					if (responsive_viewport > 767) {
						// For Data Download Overview page
						$('.overview-link .caption').each(function() {
							ar.push($(this).outerHeight());
						});
						$('.overview-link .caption').css('height',
								Math.max.apply(Math, ar));

						// Single Data Page set height for image block
						/*
						 * var h_single_data = $(".single-data").outerHeight()
						 * $('.single-data-img').css('height', h_single_data);
						 */

					} /* end larger than 767px */

					/*
					 * counter for Species Monitored
					 * 
					 * don't run it, run it after ajax calls
					 * $('.counter').counterUp({ delay: 50, time: 1000 });
					 */
					/* Bx Slider for News post */
					$('.bxslider').bxSlider({
						minSlides : 1,
						maxSlides : 1,
						nextSelector : '#slider-next',
						nextText : '',
						pager : false
					});

					$('.mapslider').bxSlider({
						minSlides : 3,
						maxSlides : 4,
						slideWidth : 150,
						slideMargin : 10,
						pager : false,
						preloadImages : 'all',
						captions : true,
						infiniteLoop : false
					});

					/* Guild Slider */

					if (responsive_viewport < 767) {
						$('.guild-slider').bxSlider({
							minSlides : 1,
							maxSlides : 1,
							moveSlides : 1,
							slideMargin : 10,
							pager : false
						});

					} else {
						$('.guild-slider').bxSlider({
							slideWidth : 160,
							minSlides : 1,
							maxSlides : 6,
							moveSlides : 1,
							slideMargin : 10,
							pager : false
						});
					}
					/* List View/ Grid View */
					$(".sidemenuListview").click(function() {
						$(".sidemenuListview").addClass("active");
						$(".sidemenuGridview").removeClass("active");
						$(".data-post").addClass("list-view");
						$(".data-post").removeClass("grid-view");

					});
					$(".sidemenuGridview").click(function() {
						$(".sidemenuGridview").addClass("active");
						$(".sidemenuListview").removeClass("active");
						$(".data-post").addClass("grid-view");
						$(".data-post").removeClass("list-view");
					});

					/* Toggle databy Year */
					$(".yearData").hide();
					$(".databy-year h4").click(function() {
						$(this).toggleClass("open");
						$(this).next(".yearData").slideToggle();
					});

					/* Select Species Group */
					$(".species-groupList").hide();
					$(".slectGroup").click(function() {
						$(this).toggleClass("open");
						$(".species-groupList").slideToggle();
					});

					// init Isotope
					var $container = $('.isotope').isotope(
							{
								itemSelector : '.element-item',
								layoutMode : 'fitRows',
								getSortData : {
									name : '.name',
									symbol : '.symbol',
									number : '.number parseInt',
									category : '[data-category]',
									weight : function(itemElem) {
										var weight = $(itemElem)
												.find('.weight').text();
										return parseFloat(weight.replace(
												/[\(\)]/g, ''));
									}
								}
							});

					// filter functions
					var filterFns = {
						// show if number is greater than 50
						numberGreaterThan50 : function() {
							var number = $(this).find('.number').text();
							return parseInt(number, 10) > 50;
						},
						// show if name ends with -ium
						ium : function() {
							var name = $(this).find('.name').text();
							return name.match(/ium$/);
						}
					};

					// bind filter button click
					$('#filters').on('click', 'button', function() {
						var filterValue = $(this).attr('data-filter');
						// use filterFn if matches value
						filterValue = filterFns[filterValue] || filterValue;
						$container.isotope({
							filter : filterValue
						});
					});

					// bind sort button click
					$('#sorts').on('click', 'button', function() {
						var sortByValue = $(this).attr('data-sort-by');
						$container.isotope({
							sortBy : sortByValue
						});
					});

					// change is-checked class on buttons
					$('.button-group').each(
							function(i, buttonGroup) {
								var $buttonGroup = $(buttonGroup);
								$buttonGroup.on('click', 'button', function() {
									$buttonGroup.find('.is-checked')
											.removeClass('is-checked');
									$(this).addClass('is-checked');
								});
							});

					/* Navigarion Position on Scroll */
					/*
					 * var $j = jQuery;
					 * 
					 * window.navi = { fixed: false, visible: true, lastST: 0,
					 * lastSTup: 0 }; window.placeholder_mobile = false;
					 * 
					 * window.navi.elm = jQuery(".data-nav");
					 * 
					 * //--------------------------------------------
					 * 
					 * jQuery(window).on("scroll", function(event) {
					 * 
					 * var st = jQuery(this).scrollTop(); if (st > 350 &&
					 * !window.navi.fixed) { window.navi.fixed = true;
					 * window.navi.elm.addClass("fixed"); } else if (st < 10 &&
					 * window.navi.fixed) { window.navi.fixed = false;
					 * window.navi.visible = true;
					 * window.navi.elm.removeClass("fixed invisible
					 * visible").css("position", "inherit");
					 * window.setTimeout('window.navi.elm.css("position", ""); ',
					 * 1); // css position wegen safari in ios7 } var test =
					 * true; if (st > 350 && window.navi.visible && (st -
					 * window.navi.lastST > 0)) {
					 * 
					 * window.navi.visible = false; test = false;
					 * window.setTimeout('window.navi.elm.addClass("invisible").removeClass("visible"); ',
					 * 1); } if (!window.navi.visible && (st -
					 * window.navi.lastSTup < -100) && test) {
					 * window.navi.visible = true;
					 * window.navi.elm.addClass("visible").removeClass("invisible"); }
					 * if (window.navi.visible && (st - window.navi.lastST > 0) &&
					 * $(".navbar-collapse.collapse").hasClass("in")) {
					 * window.navi.visible = true;
					 * window.navi.elm.addClass("visible").removeClass("invisible"); }
					 * 
					 * if (st - window.navi.lastST > 0) window.navi.lastSTup =
					 * st; window.navi.lastST = st;
					 * 
					 * });//On Scroll
					 */

				});// Document read

$(document).ready(function() {

	// Convert all the links with the progress-button class to
	// actual buttons with progress meters.
	// You need to call this function once the page is loaded.
	// If you add buttons later, you will need to call the function only for
	// them.

	$('.progress-button').progressInitialize();

	// the progress animations

	/*
	 * $('#final-data-download').click(function(e){ e.preventDefault();
	 * $(this).progressTimed(2); });
	 */
});
/*
 * // The progress meter functionality is available as a series of plugins. //
 * You can put this code in a separate file if you want to keep things tidy.
 * 
 * (function($){ // Creating a number of jQuery plugins that you can use to //
 * initialize and control the progress meters.
 * 
 * $.fn.progressInitialize = function(){ // This function creates the necessary
 * markup for the progress meter // and sets up a few event listeners. // Loop
 * through all the buttons:
 * 
 * return this.each(function(){
 * 
 * var button = $(this), progress = 0; // Extract the data attributes into the
 * options object. // If they are missing, they will receive default values.
 * 
 * var options = $.extend({ type:'background-horizontal', loading: 'Loading..',
 * finished: 'Done!' }, button.data()); // Add the data attributes if they are
 * missing from the element. // They are used by our CSS code to show the
 * messages button.attr({'data-loading': options.loading, 'data-finished':
 * options.finished}); // Add the needed markup for the progress bar to the
 * button var bar = $('<span class="tz-bar ' + options.type +
 * '">').appendTo(button); // The progress event tells the button to update the
 * progress bar button.on('progress', function(e, val, absolute, finish){
 * 
 * if(!button.hasClass('in-progress')){ // This is the first progress event for
 * the button (or the // first after it has finished in a previous run).
 * Re-initialize // the progress and remove some classes that may be left.
 * 
 * bar.show(); progress = 0;
 * button.removeClass('finished').addClass('in-progress') } // val, absolute and
 * finish are event data passed by the progressIncrement // and progressSet
 * methods that you can see near the end of this file.
 * 
 * if(absolute){ progress = val; } else{ progress += val; }
 * 
 * if(progress >= 100){ progress = 100; }
 * 
 * if(finish){
 * 
 * button.removeClass('in-progress').addClass('finished');
 * $("#download-status").text("Your Data Download is Complete.");
 * bar.delay(500).fadeOut(function(){ // Trigger the custom progress-finish
 * event button.trigger('progress-finish'); setProgress(0); }); }
 * 
 * setProgress(progress); });
 * 
 * function setProgress(percentage){
 * bar.filter('.background-horizontal,.background-bar').width(percentage+'%');
 * bar.filter('.background-vertical').height(percentage+'%'); }
 * 
 * }); }; // progressStart simulates activity on the progress meter. Call it
 * first, // if the progress is going to take a long time to finish.
 * 
 * $.fn.progressStart = function(){
 * 
 * var button = this.first(), last_progress = new Date().getTime();
 * 
 * if(button.hasClass('in-progress')){ // Don't start it a second time! return
 * this; }
 * 
 * button.on('progress', function(){ last_progress = new Date().getTime(); }); //
 * Every half a second check whether the progress // has been incremented in the
 * last two seconds
 * 
 * var interval = window.setInterval(function(){
 * 
 * if( new Date().getTime() > 2000+last_progress){ // There has been no activity
 * for two seconds. Increment the progress // bar a little bit to show that
 * something is happening
 * 
 * button.progressIncrement(5); } }, 500);
 * 
 * button.on('progress-finish',function(){ window.clearInterval(interval); });
 * 
 * return button.progressIncrement(10); };
 * 
 * $.fn.progressFinish = function(){ return this.first().progressSet(100); };
 * 
 * $.fn.progressIncrement = function(val){
 * 
 * val = val || 10;
 * 
 * var button = this.first();
 * 
 * button.trigger('progress',[val])
 * 
 * return this; };
 * 
 * $.fn.progressSet = function(val){ val = val || 10;
 * 
 * var finish = false; if(val >= 100){ finish = true; }
 * 
 * return this.first().trigger('progress',[val, true, finish]); }; // This
 * function creates a progress meter that // finishes in a specified amount of
 * time.
 * 
 * $.fn.progressTimed = function(seconds, cb){
 * 
 * var button = this.first(), bar = button.find('.tz-bar');
 * 
 * if(button.is('.in-progress')){ return this; } // Set a transition declaration
 * for the duration of the meter. // CSS will do the job of animating the
 * progress bar for us.
 * 
 * bar.css('transition', seconds+'s linear'); button.progressSet(99);
 * 
 * window.setTimeout(function(){ bar.css('transition','');
 * button.progressFinish();
 * 
 * if($.isFunction(cb)){ cb(); } }, seconds*1000); };
 * 
 * })(jQuery);
 */