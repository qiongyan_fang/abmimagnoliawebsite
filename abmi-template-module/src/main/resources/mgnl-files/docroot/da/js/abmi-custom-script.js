

document.write('<script type="text/javascript" src="/docroot/da/js/dataTables.scroller.min.js"></script>');
document.write('<script type="text/javascript" src="/docroot/da/js/da_abmi.js"></script>');
document.write('<script type="text/javascript" src="/docroot/da/js/jquery-checktree.js"></script>');
document.write('<script type="text/javascript" src="/docroot/da/js/rawdata_download.js"></script>');
document.write('<script type="text/javascript" src="/docroot/da/js/abmi-species-profile.js"></script>');




var gSpeciesGroupMap = [
	// 1:
		"Birds",
	
	// 2:
		"Vascular Plants",
	// 3:
		"Soil Mites",
	// 4:
		"Lichens",
	// 5:
		"Bryophytes",
	// 6:
		"Aquatic Invertabrates",
		// 7:
			"Mammals"
];



$(document).ready(function() {
	
	$('.large_image_slider').bxSlider({
		
	});

	
	
	if($("#sppGroupName").length){
		$("#sppGroupName").text(gSpeciesGroupMap[gSpeciesGroupId-1]); // set species group name
	}
	
	
	// set active link
	var currentLink = $(location).attr("pathname");  
	$.each($(".data-nav-links .datalink"), function (index, obj){
		var link = $(obj).attr("href");
		link = link.replace(".html","");
		if (currentLink.indexOf(link) != -1){
			$(obj).addClass("active");
		}
		else{
			$(obj).removeClass("active");
		}
		
	});
	
	$("#feedback").popover("show");
	
	$("#zoomit").elevateZoom({ zoomType	: "inner", cursor: "crosshair" });

	//--------------------------------------------
	// FancyBox
	//--------------------------------------------
	$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		width		: '70%',
		height		: '70%',
		openEffect  : 'fade',
		closeEffect : 'fade',
		helpers : {
			media : {}
		}
	});
	
	$(".facybox").fancybox({
	    helpers : {
		        title: {
		            type: 'inside',
		            position: 'top'
		        }
		    },
	    nextEffect: 'fade',
	    prevEffect: 'fade',
	    afterShow   : addLinks
	});
	
	function addLinks() {
		    var list = $("#links");
		    
		    if (!list.length) {    
		        list = $('<ul id="links">');
		    
		        for (var i = 0; i < this.group.length; i++) {
		            $('<li data-index="' + i + '"><label></label></li>').click(function() { $.fancybox.jumpto( $(this).data('index'));}).appendTo( list );
		        }
		        
		        list.appendTo( '.fancybox-wrap' );
		    }
		
		    list.find('li').removeClass('active').eq( this.index ).addClass('active');
		}
		
		function removeLinks() {
		    $("#links").remove();    
		}
		
		
	$('.open-modalwindow, .zoom-graph').click(function(e) {
	    var el, id = $(this).data('open-id');
	    if(id){
	        el = $('.facybox[rel=' + id + ']:eq(0)');
	        e.preventDefault();
	        el.click();
	    }
	}); 
	
	if ($("#searchSpeciesHome").length) {
		var rootPath = "/.ajax/bioBrowser/";
			var searchAutoCompleteUrl = rootPath
			+ "getAutoCompeleteSpecies";
		
				$("#searchSpeciesHome").autocomplete({
				
					source : function(request, response) {
			
						$.ajax({
			
							url : searchAutoCompleteUrl,
			
							dataType : "json",
			
							data : {
			
								mask : request.term,
								groupId : null
			
							},
			
							success : function(data) {
			
								response(data);
			
							}
			
						});
			
					},
			
					minLength : 2
				// do autocomplete when at least minLength Characters
				// are entered.
			
				}); // end of autocomplete
				
/*--------------------------------------------------------------------------------
 *
 * add search html into the page, so it will search (auto-complete) species, and redirect to 
 * species detail pages after cliking search button.
 * ------------------------------------------------------------------------------------------------------------------------*
 * 
 * <div class="bio-browser-search">
	<form action="#" method="get" class="search-form"><fieldset>
	<div class="search-group"><input type="text" name="s" id="searchSpeciesHome" placeholder="Quick Species Search" value="" class="form-control"> <span class="search-group-btn"><button type="submit" class="search-btn" id="searchSpeciesBtn"><span class="search-group-btn"><span>Search</span></span></button></span></div></fieldset></form>
	</div>
 ------------------------------------------------------------------------------------------------------------------------*/
				/*
				$('#q').keyup(function(e) {
					if (e.keyCode == 13) {
						openSearch();
					}
				});
				*/
				
				$("#searchSpeciesBtn").bind("click", function(){
					var sppName = $("#searchSpeciesHome").val();
					if ((typeof gSpeciesDetailLink == 'undefined') || undefined === gSpeciesDetailLink  || !gSpeciesDetailLink){
							gSpeciesDetailLink = "/home/data-analytics/biobrowser-home/species-profile";
					}
					$.ajax({
						
						url : rootPath + "getSpeciesIds",
		
						dataType : "json",
		
						data : {
							speciesName : sppName
							},
		
						success : function(data) {
							window.location =gSpeciesDetailLink + "?tsn=" + data.tsn;
						}
		
					}); // end of ajax
				}); // end of bind click
	} // end of #searchSppBtn 
	
	
	/*----------------------------------------------------------------------------------
	 * species count in biobrowser home and species list page
	*---------------------------------------------------------------------------------- */
	if ($("#spp-count").length){
		var rootPath = "/.ajax/bioBrowser/"; 
		var params ={};
		
		if (typeof gSpeciesGroupId !== "undefined") {
			params["groupId"] = gSpeciesGroupId;
		}
		
	
		$.ajax({
			
			url : rootPath + "getSpeciesCounter",

			dataType : "json",

			data :  params,

			success : function(data) {
				
				$.each(data, function(key, value){
					$("#spp-" + key ).html(value.toLocaleString());	
				});
				
				 $('#spp-count').counterUp({
				        delay: 50,
				        time: 1000
				    });			 
				 
			}

		}); // end of ajax
		
	  
	} // end of spp count
	
	
	/*------------------------------------------
	 * fancy box: used for map gallery style viewer
	*------------------------------------------ */
	  
	   
			$(".fancybox-thumb").fancybox({
				prevEffect	: 'none',
				nextEffect	: 'none',
				loop       : false,
				helpers	: {
					title	: {
						type: 'inside'
					},
					thumbs	: {
						width	: 100,
						height	: 100,
					}
					
				}
			});	 // end of fancybox
		
		

			$(".breadcrumb  .popover .popover-title").click(function(){
				
				$("#feedback").popover("hide");
			});
	
  });
  


