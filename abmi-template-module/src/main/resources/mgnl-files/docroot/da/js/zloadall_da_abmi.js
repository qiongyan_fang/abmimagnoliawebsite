var rootPath = "/.ajax/bioBrowser/"
var searchAutoCompleteUrl = rootPath + "geAutoCompeleteSpecies";
var speciesListUrl = rootPath + "getSpeciesList";


//

// DataTables initialisation

//
var dataTable;

$(document).ready(function() {
	if ($("#data-block").length > 0){
	    var fullCount = 0;
	    var basicCount = 0;
	var dataDef = [ {
		"data" : "image",
		"orderable" : false,
		"searchable" : false
	}, {
		"data" : "summary_type",
		"searchable" : false
	}, {
		"data" : "common_name",
		"searchable" : true
	}, {
		"data" : "scientific_name",
		"searchable" : true
	} ];

	if (gSpeciesGroup == "Vascular Plants") {
		dataDef.push({
			"data" : "protocol_location",
			"searchable" : true
		});
	}

	dataTable = $('#data').DataTable({
		deferRender : true,
		scrollY : 1000,
		scrollCollapse : true,
		scroller : true,
		initComplete : function() {
			var api = this.api();
//			api.scroller().scrollToRow(1);
			
			api.data().each( function (d) { /* after intital loading, count the number of summary types */
				if (d.summary_type == "Full") {
					
					fullCount++;
				} else {
					
					basicCount ++;
				}
			} );
			  $("#fullCount").html( fullCount); 
			  $("#basicCount").html( basicCount);
		 
			  
		},
		ajax : speciesListUrl + "?groupName=" + gSpeciesGroup
		/*
		 * "ajax": { //$.fn.dataTable.pipeline( data: function(data) {
		 * planify(data); }, url: speciesListUrl+"?groupName="+gSpeciesGroup }
		 */,
		columns : dataDef,
		
		"createdRow" : function(row, data, index) {
			// Bold the grade for all 'A' grade browsers
			if (data.summary_type == "Full") {
				$('td:eq(1)', row).addClass("full");
				
			} else {
				$('td:eq(1)', row).addClass("basic");
				
			}
			var img = $('td:eq(0)', row).text();
			$('td:eq(0)', row).html('<div class="pagetitleimg"><img alt="" src="/docroot/da/assets/birds-sm.jpg"></div>');
			$('td:eq(0)', row).addClass("datatableimg");
		}
			
			
		
	});

	$('#data').on( 'draw.dt', function () {
		  $('.dataTables_scrollBody thead tr').addClass('hidden');
		
		
		
		 });
	
	$("#searchSpeciesBtn").bind("click", function() {
		$("#dataBody").empty();

		dataTable.search($("#searchSpecies").val()).draw();
	});

	$("#terrestrial-filter").bind("click", function() {
		$("#dataBody").empty();

		dataTable.search("terrestrial").draw();
	});
	$("#aquatic-filter").bind("click", function() {
		$("#dataBody").empty();

		dataTable.search("aquatic").draw();
	});

	$("#all-filter").bind("click", function() {
		$("#dataBody").empty();

		dataTable.search("").draw();
	});

	$("#searchSpecies").autocomplete({
		source : function(request, response) {

			$.ajax({

				url : searchAutoCompleteUrl,

				dataType : "json",

				data : {

					mask : request.term,
					group: gSpeciesGroup

				},

				success : function(data) {

					response(data);

				}

			});

		},

		minLength : 2

	});

	}
});
