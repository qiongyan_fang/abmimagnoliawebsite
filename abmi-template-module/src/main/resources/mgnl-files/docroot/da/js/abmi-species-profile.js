$(document)
		.ready(
				function() {

					if ((typeof gTsn == "undefined") || !gTsn) // don't do
																// anything if
																// no tsn
																// provided.
						return;
					var gExternalSppFolder = [ "birds", "vplants", "mites",
							"lichens", "mosses", "", "mammals" ]; // species
					// website
					// folder
					// names

					var htmlPath = window.location.pathname;
					var root = ""; // "http://abmi-wp.ccis.ualberta.ca:8080";
					var speciesDetailsUrl = root
							+ "/.ajax/speciesProfile/getSingleSpeciesProfile";
					var speciesGraphsUrl = root
							+ "/.ajax/bioBrowser/geSpeciesGraphs";

					// var distributionMapUrl = root
					// + "/.ajax/bioBrowser/geSpeciesDetection";
					
					// click mini circle to go to the top.
					$(".scrollTop").click(function(){
						 $('body').animate({
						        scrollTop: 0
						    }, 200);
						
					}); 

					if ($("#species-profile-details").length) {
						// $(".auto").hide();
						// initSpeciesDetails();
						// getGraphs();
						getMaps();
						getGuilds();
						getPreviousNextTsn();

						// initDistributionMap();
					}

					/*
					 * send request to get species profiles from profile
					 * workspace
					 */

					function initSpeciesDetails() {

						// read species profiles, and display information
						$.ajax({
							type : "GET",

							url : speciesDetailsUrl,
							data : {
								forApps : false,
								scientificName : gScientificName
							},
							dataType : "json"
						}).done(function(data) {
							_initSpeciesDetailsHtml(data);
						});
					} // end if initSppDetails

					// read graphs and embed them in html
					function getGraphs() {
						var imageUrl = "/FileDownloadServlet?dir=WEB_GRAPH&filename=";
						$
								.ajax({
									type : "GET",
									url : speciesGraphsUrl,
									data : {
										tsn : gTsn
									},
									dataType : "json"
								})
								.done(
										function(data) {
											$
													.each(
															data,
															function(key,
																	element) {
																if (element != null
																		&& element != "null") {
																	$("#" + key)
																			.attr(
																					"src",
																					imageUrl
																							+ element.path);
																	$("#" + key)
																			.show();

																	if (element.large_path) {
																		$(
																				"."
																						+ key
																						+ "_large")
																				.attr(
																						"src",
																						imageUrl
																								+ element.large_path);
																		$(
																				"."
																						+ key
																						+ "_large")
																				.attr(
																						"href",
																						imageUrl
																								+ element.large_path);
																	}

																}
															});
										});

					} // end of getting graph functions
					function getGuilds() {

						var params = {};

						if (typeof gTsn !== "undefined") {
							params["tsn"] = gTsn;
						} else {
							return;
						}

						params["name"] = "Native Status";
						params["guilds"] = [ 'Native' ];

						getSingleGuild(params);
						params["name"] = "Conversation Status";
						params["guilds"] = [ 'Wildlife Act', 'AB ESCC - 2010',
								'AEP', 'SARA', 'COSEWIC', 'SRANK' ];

						getSingleGuild(params);
						generatePDF();
					} // end of getting guild value function

					function getSingleGuild(params) {

						$.ajax({

							url : "/.ajax/bioBrowser/geSpeciesGuilds",
							dataType : "json",
							data : params,
							async : false, // run in orders
							success : function(data) {
								var htmlStr = "";
								$.each(data, function(mapKey, mapRow) {
									$.each(mapRow, function(key, val) {
										if (val && val != "-") {
											if (htmlStr) {
												htmlStr += " | ";
											}

											htmlStr += " " + key + " - <span>"
													+ val + "</span>"
										}
									});

									$(".species-details").prepend(
											"<p>" + mapKey + ": " + htmlStr
													+ "</p>");

								});
							}

						});

					}

					/*
					 * -------------------------------------------------------------------
					 * -------------------------------------------------------------------
					 */
					function getPreviousNextTsn() {

						var params = {};
						params[ "tsn" ] = gTsn;
						$.ajax({

									url : "/.ajax/bioBrowser/getPreviousNextTsn",
									dataType : "json",
									data : params,
									async : false, // run in orders
									success : function(data) {
										var htmlStr = "";
										if (data.previous) {
											
											$(".sp-prev").attr("href",  htmlPath+ '?tsn='+ data.previous);
											$(".sp-prev").attr("data-content", "<p>Previous Species Profile<br><Strong> " + data.previousCommon + "</strong> <br><i>" + data.previousScientific +"</i></p>");
											$(".sp-prev").show();
											$(".sp-prev").popover({
												html : true
											});

										}

										if (data.next) {

											var html = '<a class="downloaddata-btn" href='
													+ htmlPath
													+ '?tsn='
													+ data.next
													+ '>Next Species</a>';
											
											$(".sp-next").attr("href",  htmlPath+ '?tsn='+ data.next);
											$(".sp-next").attr("data-content",  "<p>Next Species Profile<br><Strong> " + data.nextCommon + "</strong> <br><i>" + data.nextScientific +"</i></p>");
											
											$(".sp-next").show();
											
											$(".sp-next").popover({
												html : true
											});
										}
									}

								});

					}
					/* display details in html */
					function _initSpeciesDetailsHtml(data) {

						$.each(data, function(key, element) {
							if (key == "WebPhotos") {
								$.each(element, function(index, row) {
									$("." + row.imageType).attr("src",
											row.imageLink);
								});
							} else {
								$("." + key).html(element);

								$("." + key).show();
							}
						});

					}

					function getMaps() {

						var externalMapUrl = "http://species.abmi.ca/contents/species/"
								+ gExternalSppFolder[gSpeciesGroupId - 1] + "/";

						var externalPageUrl = "http://sc-dev.abmi.ca/development/pages/species/"
								+ gExternalSppFolder[gSpeciesGroupId - 1] + "/";

						var externalFtpPageUrl = "http://ftp.public.abmi.ca/species.abmi.ca/species/"
								+ gExternalSppFolder[gSpeciesGroupId - 1] + "/";

						// //Birds//Relationship_to_Linear_Footprint//north//Empidonaxalnorum_north.png"

						// display distributino maps;
						/*
						 * var distributionMapUrl =
						 * "/FileDownloadServlet?dir=WEB_GRAPH&filename=/distributionMaps/"; //
						 * //Birds//Relationship_to_Linear_Footprint//north//Empidonaxalnorum_north.png"
						 * $(".distribution-map").attr( "src",
						 * distributionMapUrl + "/" + gSpeciesGroupId + "_" +
						 * gTsn + ".png");
						 */
						// display maps by getting external url from
						// species.abmi.ca
						/**
						 * http://species.abmi.ca/contents/species/birds/map-rf/BairdsSparrow.png
						 * http://species.abmi.ca/contents/species/birds/map-cr/BairdsSparrow.png
						 * http://species.abmi.ca/contents/species/birds/map-df/BairdsSparrow.png
						 */

						var sppName = ""; // gCommonName.replace(/\s/g, "");

						if (gSpeciesGroupId == 1 || gSpeciesGroupId == 7) {
							sppName = gCommonName.replace(/\s/g, "");
							sppName = sppName.replace(/'/g, "");
						} else {
							// mooses and lichen use dot to replace space, but
							// the result group remove the space
							if (gSpeciesGroupId == 2 || gSpeciesGroupId == 4 || gSpeciesGroupId == 5) {
								sppName = gScientificName.replace(/\s/g, ".");
							} else {
								sppName = gScientificName.replace(/\s/g, "");
							}
						}
						sppName = sppName.replace(/-/g, "");

						$("img.diff-map").attr("src",
								externalMapUrl + "/map-df/" + sppName + ".png");

						$("a.diff-map").attr("href",
								externalMapUrl + "/map-df/" + sppName + ".png");

						$("img.refe-map").attr("src",
								externalMapUrl + "/map-rf/" + sppName + ".png");

						$("a.refe-map").attr("href",
								externalMapUrl + "/map-rf/" + sppName + ".png");

						$("img.curr-map").attr("src",
								externalMapUrl + "/map-cr/" + sppName + ".png");

						$("a.curr-map").attr("href",
								externalMapUrl + "/map-cr/" + sppName + ".png");
//
//						$("#external_link").attr("href",
//								externalPageUrl + sppName + ".html");

//						$("#external_link").html(
//								externalPageUrl + sppName + ".html");

						// bind map download button
						$("#raset-download-btn").attr("href",
								externalFtpPageUrl + sppName + ".zip");

					} // end of function

					/*--------------------------------------
					 * generate pdf and update link
					 -----------------------------------------*/
					function generatePDF() {

						if (!$(".download-report").attr("href")) {
							$
									.ajax(
											{
												type : "POST",
												// contentType :
												// "application/json",
												url : "/.ajax/bioBrowser/geSpeciesPDF",
												contentType : "application/x-www-form-urlencoded;charset=ISO-8859-15", // since
																														// words
																														// characters
																														// are
																														// included,
																														// send
																														// the
																														// string
																														// encoded

												data : {
													tsn : gTsn,
													data : collectSpeciesContent()

												},
												dataType : "json"
											})
									.done(
											function(data) {
												if (data && data.path) {
													$(".download-report")
															.attr(
																	"href",
																	"/FileDownloadServlet?dir=WEB_GRAPH&filename="
																			+ data.path);
													$(".download-report")
															.show();
												}
											});
						}

					} // end of generate pdfs

					function collectSpeciesContent() {
						/*
						 * String header; String description; List<Map<String,String>>
						 * readMore; List<SpeciesProfileGraph> graphSection;
						 */
						// static public String[] keywords = { "banner",
						// "introduction",
						// "hfAssociations", "graphForestAssociation",
						// "graphPrairie", "graphLinearFP", "impactFP"
						// , "map", "climate",
						// "reference" };
						// first get all .sec-title h2
						/*******************************************************
						 * banner index = 0
						 ******************************************************/

						var result = [];

						result.push({
							"details" : $(".species-details").first().html()
						});

						/*******************************************************
						 * introduction part index = 1
						 ******************************************************/

						// later
						var readMore = getReadMoreObj("#sec1", true);

						result.push({
							"description" : getNonReadMoreObj("#introduction-div"),
							"readMore" : readMore
						});

						/*******************************************************
						 * first main section with one large graph index = 2
						 ******************************************************/
						// species habitat forest
						// section["header"] = $("#sec2 h2").text();
						readMore = getReadMoreObj(".forestry-sec", true);

						result.push({
							"readMore" : readMore
						});

						/*******************************************************
						 * treed /non-treed graph index = 3
						 ******************************************************/

						readMore = getReadMoreObj(".paririe-sec");

						result.push({
							"readMore" : readMore,
						});

						/*******************************************************
						 * Linear Footprint graph index = 4
						 ******************************************************/
						readMore = getReadMoreObj(".linear-fp-sec");

						result.push({
							"readMore" : readMore
						});

						/*******************************************************
						 * impact of footprint with two graphs index = 5
						 ******************************************************/

						readMore = getReadMoreObj("#sec3", true);

						result.push({
							"readMore" : readMore,
						});

						/*******************************************************
						 * 3 maps for range index = 6
						 ******************************************************/
						var readMore = getReadMoreObj("#sec4", true);
						var graph = {};
						graph["description1"] = "";
						graph["description2"] = "";

						result.push({
							"readMore" : readMore,
							"graph" : graph
						});

						/*******************************************************
						 * reference and links index = 7
						 ******************************************************/
						section = {};
						section["data-source"] = $("#data-source").html();
						section["citation"] = $("#citation").html();

						result.push({
							"section" : section
						});

						/*******************************************************
						 * few detection north index = 8
						 ******************************************************/

						var readMore = getReadMoreObj(".few-detection-north",
								false);

						result.push({
							"readMore" : readMore
						});

						/*******************************************************
						 * few detection north index = 9
						 ******************************************************/

						var readMore = getReadMoreObj(".few-detection-south",
								false);

						result.push({
							"readMore" : readMore
						});
						// return JSON.stringify(result);

						return encodeURI(JSON.stringify(result));

					} // end of gathering parameters for pdfs

					function getReadMoreObj(divObj, bNext) {
						var readMore = {};
						var searchObj = $(divObj);
						if (bNext)
							searchObj = $(divObj).next();

						$.each(searchObj.find(".togglelink"), function(index,
								htmlObj) {
							var key = $(htmlObj).find(".readmoreTxt").text();
							var value = $(htmlObj).find(".togglesec").html();
							if (key== 'Read More' || key.indexOf("Interpreting") != -1 )
								readMore[key] = value;

						});

						return readMore;
					} // end of read more
					
					function getNonReadMoreObj(divObj) {
						var readMore = "";
						var searchObj = $(divObj + " .richtext");
					

						$.each(searchObj, function(index,
								htmlObj) {
						
								readMore += $(htmlObj).html();

						});

						return readMore;
					} // end of read more

				});