$(function() {

	$("#load")
			.bind(
					"click",
					function() {
						if (confirm(
								"Are you sure you want to reload all publication data? it will overwrite the changes mannually made inCMS.",
								"Yes, Reload", "Cancel")) {
							$.post("/.ajax/publication/updatePublication", function(data) {
								alert(data);
							});
						}
					});

	$("#export").bind("click", function() {

		$.post("/.ajax/publication/exportPublication", function(data)

		{
			window.open("/FileDownloadServlet?filename=" + data);
		});

	});

	$("#update").bind("click", function() {

		$.post("/.ajax/publication/updatePublicationVersion", function(data) {
			alert(data);
		});

	});
});