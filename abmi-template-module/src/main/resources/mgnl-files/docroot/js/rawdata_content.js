/* -----------------------------------------------------------------------


----------------------------------------------------------------------- */

function getRawdata(data) {
	var htmlStr = '<a id="download-rawdata" class="download-button" href="/FileDownloadServlet?filename='
			+ data.file
			+ '">Raw Data File <em>(zip format '
			+ data.filesize
			+ ')</em></a>';
	$('#download-data-area').fadeOut(400, function() {
		$('#download-data-area').html(htmlStr).fadeIn(800);
	});
	$("#download-data-area").show();
	$("#get-data-btn").show();
	$("#loading-div").hide();

	$("#download-rawdata").unbind();
	$("#download-rawdata").click(
			function(event) { // when someone clicks these links
				var href = $(this).attr("href");
				var target = $(this).attr("target");

				event.preventDefault(); // don't open the link yet
				var queryStr = "";
				var divs = [ "terrestrial-species", "terrestrial-habitat",
						"wetland-species", "wetland-habitat", "async-region",
						"async-years" ];
				$.each(divs, function(index, row) {
					if ($("#" + row + " .noselect").length == 0) {
						queryStr += $("#" + row).text() + ";";
					}
				});
				ga('send', 'event', 'link', 'download', "Raw Data:" + queryStr
						+ "]");

				var thisEvent = $(this).attr("onclick");
				setTimeout(function() { // now wait 300 milliseconds
					eval(thisEvent); // and continue with the onclick event
					window.open(href, (!target ? "_self" : target)); // and
																		// open
					// the link // as usual
				}, 300);
			});
}

function getCheckBoxResult(param, name) {
	var selected = "";

	var divname = (name ? name : param);
	$(".rawdatadownload input[name=" + divname + "]:checked").each(function() {
		var idStr = $(this).attr("id");
		var idInt = idStr.substring(divname.length, idStr.length);
		selected += param + "=" + idInt + "&";
	});
	return selected;
}

function initrawdatamenu() {

	/* use ajax get menu and display them */

	$(".rawdatadownload").appendTo($(".navigation li a.parent").parent());

	var ajax1 = SendAjax("/.ajax/rawdata/getDataCategory", null, addDataCategory);
	var ajax2 = SendAjax("/.ajax/rawdata/getRegion", null, addRegion);
	var ajax3 = SendAjax("/.ajax/rawdata/getRotation", null, addRotation);
	var ajax4 = SendAjax("/.ajax/rawdata/getOffgridOption", null, addOffgridOption);

	$.when(ajax1, ajax2, ajax3, ajax4).done(

	initMenuHandler);

	$("#get-data-btn").unbind();
	$("#get-data-btn")
			.click(
					function() {
						console.log(new Date().getTime() + ":generate zip");
						// get parameters
						$("#get-data-btn").hide();
						$("#download-data-area").hide();
						$("#loading-div").show();
						var params = "";
						var dataStr = getCheckBoxResult("rawdata", "category");
						if (dataStr.length < 5) {
							alert("Please select at least one species or habitat data.");
							$("#get-data-btn").show();
							$("#loading-div").hide();
							return;
						}
						params = dataStr + getCheckBoxResult("region")
								+ getCheckBoxResult("rotation")
								+ getCheckBoxResult("offgridId", "coreoffgrid")
								+";jsessionid=" + new Date().getTime();

						// send request
						SendAjax("/.ajax/rawdata/getRawdataFile", params, getRawdata);
						// download files
					});

}

function SendAjax(url, param, callback) {
	var def = $.Deferred();
	 $.ajax({
		type : "GET",

		url : url,
		data : param,

		dataType : "json",
		success : function(data) {
			callback(data);
			def.resolve();
		},
		cache: false
	});
	return def.promise();
}

function addRotation(data) {
	for ( var i = 0; i < data.length; i++) {
		// region and name
		var contentDiv = $("#rotationdetail");
		var row = data[i];
		var innerHtml = "<li >  <label for='rotation"
				+ row.id
				+ "' class='radio-item'>"
				+ "<input type='checkbox'  name='rotation' data-heading='async-years' id='rotation"
				+ row.id + "' value='" + row.name + "'>"
				+ "<span class='outer'><span class='inner'></span></span>"
				+ row.name + "<br><span class='text-align'>" + row.years;

		innerHtml += "</label>";

		innerHtml += "</li> ";

		$(contentDiv).append(innerHtml);
	}

}

function addOffgridOption(data) {
	for ( var i = 0; i < data.length; i++) {
		// region and name
		var contentDiv = $("#datadetail");
		var row = data[i];
		var innerHtml = "<li >  <label for='coreoffgrid"
				+ row.id
				+ "' class='radio-item'>"
				+ "<input type='radio'  name='coreoffgrid' data-heading='async-data' value='"
				+ row.name + "' id='coreoffgrid" + row.id + "'>"
				+ "<span class='outer'><span class='inner'></span></span>"
				+ row.name;

		innerHtml += "</label>";

		innerHtml += "</li> ";

		$(contentDiv).append(innerHtml);
	}

}

function addDataCategory(data) {
	// alert("add data!");
	for ( var i = 0; i < data.length; i++) {
		// protocol and name
		var currentProtocol = data[i].protocol.toLowerCase();
		var speciesHabitatName = data[i].name.toLowerCase();
		var currentProtocolDiv = "." + currentProtocol + "-slide-nav";
		if ($(currentProtocolDiv).length == 0) {// need to create first wetland
			// and terrestrial

			var innerHtml = "<li class='level-2'><a id= 'a-" + currentProtocol
					+ "'  class='first-child " + currentProtocol
					+ "' href='javascript:;'>" + data[i].protocol.toUpperCase()
					+ "</a>" + "<div class='" + currentProtocol
					+ "-slide-nav sub-slide-nav'><form class='"
					+ currentProtocol + "-form menu-child'><ul></ul>";

			innerHtml += "<div class='buttons-box'> <input type='button' value='SELECT ALL' class='button select' id='select-"
					+ currentProtocol
					+ "'>"
					+ "               <input type='reset' value='CLEAR ALL' class='button clear' id='clear-"
					+ currentProtocol + "'> "

					+ "</div>";
			innerHtml += "</form></div>";
			$("#category-list").append(innerHtml);
		}

		// species habitat lititle
		if ($(currentProtocolDiv + " li." + speciesHabitatName).length == 0) {
			var innerHtml = "<li class='" + speciesHabitatName + " liTitle '> "
					+ data[i].name + "</li><ul class='ul" + speciesHabitatName
					+ "'></ul>";

			$(currentProtocolDiv + " ." + currentProtocol + "-form >ul")
					.append(innerHtml);
		}

		// already exist then add data category
		for ( var j = 0; j < data[i].detail.length; j++) {
			var subData = data[i].detail[j];
			var innerHtml = "<li>  <label for='category" + subData.id
					+ "' class='radio-item'>"
					+ "<input type='checkbox' value='" + subData.name
					+ "' name='category' id='category"

					+ subData.id + "' data-heading='" + currentProtocol + "-"
					+ speciesHabitatName + "'  data-name='" + currentProtocol
					+ "-" + speciesHabitatName + "'>" /* terrestrial-species */
					+ "<span class='outer'><span class='inner'></span></span> "
					+ subData.name + "</label> " + "</li>";

			// append below the liTitle <species habitat>
			$(currentProtocolDiv + " ul.ul" + speciesHabitatName).append(
					innerHtml);

		}

	}

}

function addRegion(data) {

	for ( var i = 0; i < data.length; i++) {
		// region and name
		var regionDiv = "#regiondetail";
		var row = data[i];
		var innerHtml = "<li class='groupregion' id='li"
				+ row.id
				+ "'>  <label for='rl"
				+ row.id
				+ "' class='radio-item'>"
				+ "<input type='checkbox' value='' class='has-child-sub regiongroupinput' name='regionBig' id='rl"
				+ row.id + "'>"
				+ "<span class='outer'><span class='inner'></span></span>"
				+ row.name;

		if (row.subregion.length) {
			innerHtml += " <i class='arrow-small'></i> ";
		}
		innerHtml += "</label>";
		if (row.subregion.length) {
			innerHtml += "<ul class='sub-options1 sub-optns' style='display: none;'></ul>";
		}
		innerHtml += "</li> ";

		$(regionDiv).append(innerHtml);

		var subregionDiv = "#li" + row.id + "> ul";
		// already exist then add data category
		for ( var j = 0; j < row.subregion.length; j++) {
			var subData = row.subregion[j];
			var innerHtml = "<li>  <label for='region" + subData.id
					+ "' class='sub-radio-item'>"
					+ "<input type='checkbox' value='" + subData.name
					+ "' name='region' data-heading='async-region' id='region"
					+ subData.id + "'>"
					+ "<span class='outer'><span class='inner'></span></span> "
					+ subData.name + "</label> " + "</li>";

			if (subData.parentid) {
				if ($(" #parentregion" + subData.parentid).length == 0) {
					// need to add a parent div to hold subregions
					var innerParentHtml = "<li >  <label for='parentregion"
							+ subData.parentid
							+ "' class='sub-radio-item'>"
							+ "<input type='checkbox' class='has-child-sub subregiongroupinput' value='' name='regionparent' id='parentregion"
							+ subData.parentid
							+ "'>"
							+ "<span class='outer'><span class='inner'></span></span> <span class='sub-expand-title'>  "
							+ subData.parentname
							+ "</span> <i class='arrow-xsmall'></i></label><ul class='sub-optns sub-sub-options' id='parentid"
							+ subData.parentid
							+ "' style='display:none;'></ul></li>";

					$(subregionDiv).append(innerParentHtml);

				}
				$("#parentid" + subData.parentid).append(innerHtml);

			} else

				$(subregionDiv).append(innerHtml);

		}

	}

}