/**
 * External plugins added through the server-side FieldFactory are automatically registered.
 * Other external plugins (e.g. client-only) may still be registered here (and subsequently added via config.extraPlugins).
 *
 * e.g. if your plugin resides in src/main/resources/VAADIN/js:
 * CKEDITOR.plugins.addExternal("abbr", CKEDITOR.vaadinDirUrl + "js/abbr/");
 */




CKEDITOR.plugins.addExternal("magnolialink", CKEDITOR.vaadinDirUrl + "js/magnolialink/");
//CKEDITOR.plugins.addExternal("magnoliaFileBrowser", CKEDITOR.vaadinDirUrl + "js/magnoliaFileBrowser/");

CKEDITOR.plugins.addExternal("eqneditor", "/docroot/js/ckeditor/eqneditor/"); // plugin to create formula
/*
CKEDITOR.plugins.addExternal("clipboard","/docroot/js/ckeditor/clipboard/");
CKEDITOR.plugins.addExternal("lineutils","/docroot/js/ckeditor/lineutils/");
CKEDITOR.plugins.addExternal("widget","/docroot/js/ckeditor/widget/");
CKEDITOR.plugins.addExternal("mathjax","/docroot/js/ckeditor/mathjax/");*/

/*
 * 	config.mathJaxLib = '//cdn.mathjax.org/mathjax/2.6-latest/MathJax.js?config=TeX-AMS_HTML';
	config.mathJaxClass = 'my-math';

 */
//CKEDITOR.plugins.addExternal("wsc", "/docroot/js/ckeditor/wsc/");
CKEDITOR.editorConfig = function( config ) {
//	config.disableNativeSpellChecker = false;
	// MIRROR info.magnolia.ui.form.field.definition.RichTextFieldDefinition
	definition = {
		alignment: false,
		images: true,
		lists: true,
		source: true,
		tables: true,

		colors: false,
		fonts: false,
		fontSizes: false,
	}

	// MIRROR info.magnolia.ui.form.field.factory.RichTextFieldFactory
	removePlugins = [];

	// CONFIGURATION FROM DEFINITION
	if (!definition.alignment) {
		removePlugins.push("justify");
	}
	if (!definition.images) {
		removePlugins.push("image");
	}
	if (!definition.lists) {
		// In CKEditor 4.1.1 enterkey depends on indent which itself depends on list
		removePlugins.push("enterkey");
		removePlugins.push("indent");
		removePlugins.push("list");
	}
	if (!definition.source) {
		removePlugins.push("sourcearea");
	}
	if (!definition.tables) {
		removePlugins.push("table");
		removePlugins.push("tabletools");
	}

	if (definition.colors != null) {
		config.colorButton_colors = definition.colors;
		config.colorButton_enableMore = false;
		removePlugins.push("colordialog");
	} else {
		removePlugins.push("colorbutton");
		removePlugins.push("colordialog");
	}
	/*if (definition.fonts != null) {
		config.font_names = definition.fonts;
	} else {
		config.removeButtons = "Font";
	}
	if (definition.fontSizes != null) {
		config.fontSize_sizes = definition.fontSizes;
	} else {
		config.removeButtons = "FontSize";
	}
	
	if (definition.fonts == null && definition.fontSizes == null) {
		removePlugins.push("font");
		removePlugins.push("fontSize");
	}*/
	
	if (definition.fonts == null && definition.fontSizes == null) {
		removePlugins.push("font");
		removePlugins.push("fontSize");
	}

	// DEFAULT CONFIGURATION FROM FIELD FACTORY
	removePlugins.push("elementspath");
	removePlugins.push("filebrowser");
	removePlugins.push("wsc");
	config.removePlugins = removePlugins.join(",");
	config.extraPlugins = "magnolialink,magnoliaFileBrowser,eqneditor";
	
	config.scayt_autoStartup = true;
	
	config.scayt_maxSuggestions =3;
	config.scayt_contextCommands = 'add|ignoreall';
	
	config.baseFloatZIndex = 150;
	config.resize_enabled = false;
	config.toolbar = "Magnolia";
	// mathjax starts since 4.4
	
	
	config.toolbar_Magnolia = [
		{ name: "basicstyles",   items: [ "Bold", "Italic", "Underline", "SpecialChar" , "Subscript", "Superscript",'EqnEditor'] },
		{ name: "paragraph",     items: [ "NumberedList", "BulletedList", "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "Image", "Table" ] },
		{ name: "links",         items: [ "Link", "InternalLink", "DamLink", "Unlink" ] },
		{ name: "styles",        items: [ "Font", "FontSize", "TextColor" ] },
		{ name: "clipboard",     items: [ "Cut", "Copy", "Paste", "PasteText", "PasteFromWord" ] },
		{ name: "undo",          items: [ "Undo", "Redo" ] },
		{ name: "tools",         items: [ "Source", "Scayt"] }
	
	];
	
};
