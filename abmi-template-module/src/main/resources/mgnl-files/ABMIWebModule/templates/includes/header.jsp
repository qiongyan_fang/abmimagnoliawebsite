<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="ABMI, ${content.keyword }">
    <title>ABMI - ${content.title}</title>

    <!--[if lt IE 10]>
		<script src="js/modernizr-2.6.2.min.js"></script>
    <![endif]--> 

    <link href="https://cdn.rawgit.com/natureapp/ABMI/master/css/bootstrap.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Roboto+Slab:300,400,700' rel='stylesheet' type='text/css'>	
	<link href="https://rawgit.com/natureapp/ABMI/master/css/style.css" rel="stylesheet">
	
	

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
   <![endif]-->

<!--[if gt IE 8]>
<p> here</p>
      <script src="/docroot/js/customEvent.js"></script>
    <![endif]-->
    
  
     <cms:init />
	<link rel="shortcut icon" href="/docroot/assets/favicon.png" type="image/x-icon" />
  
  </head>