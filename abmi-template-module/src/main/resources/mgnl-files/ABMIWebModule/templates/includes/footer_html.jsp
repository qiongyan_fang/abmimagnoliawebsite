
<footer class="row">
	<div class="container">
		<div class="row">
			<cms:area name="footer" />

			<div class="col-sm-12 col-md-5 col-md-push-1 col-lg-4 col-lg-push-4">
				<h3>Stay Connected</h3>
				<p>
					Sign Up for Our Newsletters
					<!-- <span class="enews_pastissue_divider">|</span>
<a target="_blank" href="/home/past-enews.html">Past Issues</a>-->
				</p>
				<div class="input-group">
					<!--  <input type="text" class="form-control"> -->
					<form
						action="http://abmi.us4.list-manage.com/subscribe?u=de3236a0b48dda3b348d3943b&id=560ef11599"
						method="post" id="mc-embedded-subscribe-form"
						name="mc-embedded-subscribe-form" class="validate" target="_blank">

						<div class="input-group-field">
							<input type="email" value="" name="EMAIL"
								class="newslettersignup email newsletter_email form-control"
								id="mce-EMAIL" placeholder="email address" title="email address"
								required>
						</div>
						<div class="input-group-btn">
							<input type="submit" class="newslettersignup btn btn-default"
								value="Sign Up" class="btn btn-default">
						</div>

					</form>
				</div>
				<p style="margin: -20px 0 10px 0;">
					Past Issues:<a href="/home/newsletters/past-issues.html"
						target="_blank"> ABMI Newsletter</a> | <a
						href="/home/newsletters/scienceletter-past-issues.html"
						target="_blank"> Science Letters</a> | <a
						href="/home/newsletters/science-in-progress-past-issues">Science in Progress</a>
				</p>
				<div class="connect">
					<button type="button" onclick="location.href='http://blog.abmi.ca'"
						class="btn btn-primary btn-lg">It's Our Nature to Know
						Blog</button>

					<a href="https://twitter.com/ABBiodiversity" target=_blank
						class="twitter icon"></a> <a
						href="https://www.facebook.com/pages/Alberta-Biodiversity-Monitoring-Institute/115899788478502"
						target=_blank class="facebook icon"></a> <a
						href="http://vimeo.com/user22688936" target=_blank
						class="vimeo icon"></a>
				</div>
			</div>
		</div>
	</div>
</footer>


<div class="row fine-print">
	<div class="container-fluid">
		<div class="col-lg-12">
			<img src="/docroot/assets/abmi_logo-sm.png"
				alt="Alberta Biodiversity Monitoring Institute Logo, Small"
				class="pull-left" height="33" width="34">
			<cms:area name="finePrintFooter" />

		</div>
	</div>
</div>
<div class="socialflag hidden-sm hidden-xs">
	<a
		href="https://www.facebook.com/pages/Alberta-Biodiversity-Monitoring-Institute/115899788478502"
		title="Facebook"><img src="/docroot/assets/fb-icon.png"
		alt="Facebook"></a> <a href="https://twitter.com/ABBiodiversity"
		title="Twitter"><img src="/docroot/assets/twitter-icon.png"
		alt="twitter"></a>
</div>
