<%@ include file="../includes/taglibs.jsp"%>
<c:choose>
    <c:when test="${not empty content.url}">
    
       <a href="${content.url}">${empty content.text?cmsfn:page(cmsfn:asContentMap(cmsfn:content(content.url,'website'))).title:content.text}</a>
    </c:when>
      <c:when test="${not empty content.externalurl}">
       <a href="${content.externalurl}" target=_blank>${content.text} </a>
    </c:when>
    <c:otherwise>
       ${content.text}
    </c:otherwise>
</c:choose>