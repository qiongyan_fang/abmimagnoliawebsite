<%@ include file="../includes/taglibs.jsp" %>

<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />

<c:choose>
<c:when test="${content.type eq 'twitter'}">
		
<a class="${content.type}" href="https://twitter.com/ABBiodiversity" target=_blank></a>
</c:when>
<c:when test="${content.type eq 'facebook'}">
		
<a class="${content.type}" href="https://www.facebook.com/pages/Alberta-Biodiversity-Monitoring-Institute/115899788478502" target=_blank></a>
</c:when>
<c:when test="${content.type eq 'vimeo'}">
		
<a class="${content.type}" href="http://vimeo.com/user22688936" target=_blank></a>
</c:when>
</c:choose>

