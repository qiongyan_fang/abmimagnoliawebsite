
<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="uri" value="${empty currentLink?state.handle:currentLink}" />
<ul class="navigation">
	<c:forEach items="${subpage}" var="row">
		<c:set var="classname" value="" />
		<c:if test="${row.path eq uri}">
			<c:set var="classname" value="parent  parent-active" />
		</c:if>
		
		
		<c:if test="${empty row.childpages}">
			<li class="${classname}"><a
				href="${pageContext.request.contextPath}${row.path}.html?scroll=true">${row.title}
			</a></li>
		</c:if>
		<!--  show pages have subpages -->
		<c:if test="${not empty row.pages}">
			<li class="parent-gis-data ${classname}"><a class="${classname}"
				href="javascript:;">GIS Data <i class="arrow"></i></a>
				<ul class="gis-data">
					<c:forEach items="${row.pages}" var="row">
						<c:set var="classname2" value="" />
						<c:if test="${row.path eq uri}">
							<c:set var="classname2" value="parent parent-active" />
						</c:if>
		
						<li class="${classname2}"><a
							href="${pageContext.request.contextPath}${row.path}.html?scroll=true">${row.title}
						</a></li>


					</c:forEach>
				</ul>
				</li>
		</c:if>


	</c:forEach>

</ul>