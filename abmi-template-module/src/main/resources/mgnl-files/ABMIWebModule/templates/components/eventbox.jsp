<%@ include file="../includes/taglibs.jsp"%>

					<div class="tile half events last">
						<a title="" href="${content.url}">
							<h4>${content.headerline}</h4>
							<p><strong>${content.date}</strong>
								<br><strong>${content.location}</strong>
								<br>${content.content}</p>
						
						</a>
					</div>