
<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 

<c:set var="uri" value="${content.rootpath}.html" />

<a href="${uri}"><h4>${content.generalheader }</h4></a>

	<c:if test="${documentType.size() > 0}">
	<div class="sidemenu publications-side">
		<ul class="sidemenu-nav-expand">
			<!-- EXPANDABLE BLOCK -->
			<li><input type="button" data-toggle="collapse"
				data-target="#document-type"
				value="${empty content.documenttypeheader?'Document Type':content.documenttypeheader}"
				class="${empty param.documenttype?'collapsed':''}">
				<div id="document-type" class="${empty param.documenttype?'collapse':'collapse in'}">
					<div class="expandrow">
						<ul>

							<c:if test="${empty param.documenttype}">
								<li class="active"><a href="#">All Document Types</a></li>
							</c:if>
							<c:if test="${not empty param.documenttype}">
								<li><a href="${uri}?mode=${mode}">All Document Types</a></li>
							</c:if>
							<c:forEach items="${documentType}" var="row">

								<c:if test="${row eq param.documenttype}">
									<li class="active"><a href="#">${row} </a></li>
								</c:if>
								<c:if test="${row ne param.documenttype}">
<c:url var="queryuri" value="${uri}">
  <c:param name="documenttype" value="${row}"/>
  <c:param name="mode" value="${mode}"/>
</c:url>
									<li><a
										href="${queryuri}">${row}</a></li>
								</c:if>

							</c:forEach>
						</ul>

					</div>
				</div></li>

		</ul>
		</div>
	</c:if>
	<c:if test="${subjectList.size() > 0}">
	<div class="sidemenu publications-side">
		<ul class="sidemenu-nav-expand">
			<!-- EXPANDABLE BLOCK -->
			<li><input type="button" data-toggle="collapse"
				data-target="#subject-area"
				value="${empty content.subjectheader?'Subject Area':content.subjectheader}"
				class="${empty param.subject?'collapsed':''}">
				<div id="subject-area" class="collapse ${empty param.subject?'':'in'}">
					<div class="expandrow">
						<ul>
							<c:if test="${empty param.subject}">
								<li class="active"><a href="#">All Subjects</a></li>
							</c:if>
							<c:if test="${not empty param.subject}">
								<li><a href="${uri}?mode=${mode}">All Subjects</a></li>
							</c:if>
							<c:forEach items="${subjectList}" var="subjectrow">

								<c:if test="${subjectrow eq param.subject}">
									<li class="active"><a href="#">${subjectrow}</a></li>
								</c:if>
								<c:if test="${subjectrow ne param.subject}">

<c:url var="subjectqueryuri" value="${uri}">
  <c:param name="subject" value="${subjectrow}"/>
  <c:param name="mode" value="${mode}"/>
</c:url>
									
									<li><a
										href="${subjectqueryuri}">${subjectrow}</a></li>
								</c:if>
							</c:forEach>
						</ul>

					</div>
				</div></li>

		</ul>
		</div>
	</c:if>
	<c:if test="${archiveNews.size() > 0}">
	<div class="sidemenu publications-side">
		<ul class="sidemenu-nav-expand">
			<!-- EXPANDABLE BLOCK -->
			<li><input type="button" data-toggle="collapse"
				data-target="#archive-area" value="${empty content.archiveheader?'Archive':content.archiveheader}"
				class="${empty param.time?'collapsed':''}">
				<div id="archive-area" class="collapse ${empty param.time?'':'in'}">
					<div class="expandrow">
						<ul>

							<c:if test="${empty param.time}">
								<li class="active"><a href="#">All</a></li>
							</c:if>
							<c:if test="${not empty param.time}">
								<li><a href="${uri}?mode=${mode}">All</a></li>
							</c:if>


							<c:forEach items="${archiveNews}" var="archiveTime">

								<c:if test="${archiveTime eq param.time}">
									<li class="active"><a href="#">${archiveTime}</a></li>
								</c:if>
								<c:if test="${archiveTime ne param.time}">
									<li><a
										href="${pageContext.request.contextPath}${uri}?time=${archiveTime}&mode=${mode}">${archiveTime}</a></li>
								</c:if>
							</c:forEach>
						</ul>

					</div>
				</div></li>

		</ul>
</div>
	</c:if>
	</div>