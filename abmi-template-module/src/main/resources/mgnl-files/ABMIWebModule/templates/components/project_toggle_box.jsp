<%@ include file="../includes/taglibs.jsp"%>
<div class="proj_more_info">
					<div class="proj_toggle_box">
						<span class="toggle_link header-${content.headercolor} ${content.bold?'bold':''}">${content.header}</span>
						<div class="toggle_info" style="display: none;">
							${cmsfn:decode(content).description }
						</div>
					</div>
					</div>