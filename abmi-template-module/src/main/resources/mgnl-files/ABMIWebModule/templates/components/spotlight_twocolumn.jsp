<%@ include file="../includes/taglibs.jsp" %>

		<div class="row">
				<div class="col-xs-12 col-sm-8 col-sm-push-2 col-md-6 col-md-push-0 col-lg-6">
					<cms:area name="left" />
				</div>
				<div class="col-xs-12 col-sm-8 col-sm-push-2 col-md-6 col-md-push-0 col-md-6 col-lg-6">
					<cms:area name="right" />
				</div>
			</div>