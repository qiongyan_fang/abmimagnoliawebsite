<%@ include file="../includes/taglibs.jsp"%>
<c:set var="image" value="${cmsfn:content(content.imageUrl,'dam')}" />

<div class="right_icon_button_box">
	<div class="padbox">
		<c:if test="${not empty image}">
			<p>
				<img src="${cmsfn:link(cmsfn:asContentMap(image))}"
					alt="${content.headerText }">
			</p>
		</c:if>
		<c:if test="${not empty content.headerText}">
			<h5>${content.headerText }</h5>
		</c:if>
		<c:if test="${not empty content.text}">
			<p>${content.text }</p>
		</c:if>
	</div>
	<c:if test="${not empty content.buttonText}">
		<a class="btn-common  orange"
			${fn:startsWith(content.url,
		"http")?"target=_blank":""}
			href="${content.url}" title="${content.buttonText}">${content.buttonText}</a>
	</c:if>
</div>
