<%@ include file="../includes/taglibs.jsp"%>

<c:set var="divId"
	value="${empty content.id ? 'carousel-news':'carousel-news'}" />
<c:set var="divId">${state.getCurrentContentNode().getUUID()} </c:set>

<c:if test="${newsItem.size() > 0}">
	<div class="tile">
		<div data-ride="carousel"
			class="carousel slide carousel-${content.imageposition}-image"
			id="${divId}">

			<ol class="carousel-indicators">
				<c:forEach var="i" begin="0" end="${newsItem.size() -1}">
					<c:if test="${i eq 0}">
						<li class="active" data-slide-to="${i}"
							data-target="#${divId}"></li>
					</c:if>
					<c:if test="${i > 0}">
						<li class="" data-slide-to="${i}" data-target="#${divId}"></li>
					</c:if>



				</c:forEach>
			</ol>

			<div class="carousel-inner">

				<c:forEach var="i" begin="0" end="${newsItem.size() -1}">

					<c:set var="urlNode"
						value="${cmsfn:content(newsItem[i].link,'website')}" />
					<c:choose>
						<c:when test="${not empty newsItem[i].smallimage}">
							<c:set var="imagelink"
								value="${cmsfn:link(cmsfn:asContentMap(cmsfn:content(newsItem[i].smallimage,'dam')))}" />
						</c:when>
						<c:when test="${not empty newsItem[i].largeimage}">
							<c:set var="imagelink"
								value="/.imaging/carousel_thumbnail/dam/${newsItem[i].largeimage}." />
						</c:when>
<c:otherwise>
<c:set var="imagelink" value="" />
</c:otherwise>
					</c:choose>

					<c:if test="${i eq 0}">
						<c:set var="activeClass" value="active" />
					</c:if>

					<c:if test="${i ne 0}">
						<c:set var="activeClass" value="" />
					</c:if>
					<div class="item ${activeClass}">

						<a title="${newsItem[i].title}" href="${newsItem[i].link}"> <img
							class="pull-${content.imageposition} img-responsive  hidden-xs hard35_7p"
							alt="${content.headerline1}" src="${imagelink}">

							<div class="column pull-${content.imageposition} ">
								<h4>${newsItem[i].title}</h4>
								<p class="timestamp">${newsItem[i].date}</p>
								<p>${newsItem[i].description}</p>
							</div>
						</a>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>

</c:if>

