
<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<h4>${content.header}</h4>

<!-- < c : set var="uri" value="{empty currentLink?state.handle:currentLink}" / -->
<c:if test="${content.showparent=='true'}">
<c:set var="uri" value="${state.mainContentNode.parent.path}" />
</c:if>
<c:if test="${not content.showparent=='true'}">
<c:set var="uri" value="${empty currentLink?state.handle:currentLink}" />
</c:if>
<ul>


	<c:forEach items="${subpage}" var="row">

		<c:if test="${row.key eq uri}">
			<li class="active"><a href="${pageContext.request.contextPath}${row.key}.html?scroll=true">${row.value} </a></li>
		</c:if>
		<c:if test="${row.key ne uri}">
			<li><a href="${pageContext.request.contextPath}${row.key}.html?scroll=true">${row.value}</a></li>
		</c:if>

	</c:forEach>

</ul>
<c:if test="${not empty param.scroll}">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 

<script>
$(function(){
	if ($(".breadcrumb").length){
 $('html,body').animate({
        scrollTop: $(".breadcrumb").offset().top},
        'slow');
	}
});
</script>
</c:if>