<%@ include file="../includes/taglibs.jsp"%>
<!--  right side glossary area. inside users can add glossary related to pages. -->
<h4>${content.header}</h4>
<ul class="glossary">
<cms:area name="GlossaryArea" />
</ul>