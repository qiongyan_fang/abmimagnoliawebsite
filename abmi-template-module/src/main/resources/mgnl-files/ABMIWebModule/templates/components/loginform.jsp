<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>     
<%@ include file="../includes/taglibs.jsp"%>

<security:authorize  access="isAuthenticated()">
Welcome <security:authentication property='principal.fullname' htmlEscape='false'/>
</security:authorize>


<security:authorize access="! isAuthenticated()">

   <div id="login-box" style="background-color:white;color:black;">
 
		<h3>Login with Username and Password</h3>

		<c:if test="${not empty param.error}">
			<div class="error">You login failed.</div>
		</c:if>
		
 <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
      <font color="red">
        Your login attempt was not successful due to <br/><br/>
        <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>.
      </font>
</c:if>
		

<form action="<c:url value="/j_spring_security_check"></c:url>" method="post" role="form">
 <table>
    <tbody><tr><td>User:</td><td><input name="j_username" value="" type="text"></td></tr>
    <tr><td>Password:</td><td><input name="j_password" type="password"></td></tr>
    <tr><td><input name="rememberMe" type="checkbox"></td><td>Remember me on this computer.</td></tr>
    <tr><td colspan="2"><input name="submit" value="Login" type="submit"></td></tr>
  </tbody></table>

		   <input type="hidden" 
                     name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
	</div>

    </security:authorize>
