<%@ include file="../includes/taglibs.jsp"%>

	<c:set var="image" value="${cmsfn:content(content.image,'dam')}" />
		
<div class="jumbotron" style=" background: url('${cmsfn:link(cmsfn:asContentMap(image))}') no-repeat scroll center top, none repeat scroll 0 0 rgb(58, 58, 17);">
			<div class="overlay">
				<h1 class="text-center">${content.header}</h1>
				<h1 class="text-center sub-heading">${content.subheader}</h1>
			</div>
		</div>