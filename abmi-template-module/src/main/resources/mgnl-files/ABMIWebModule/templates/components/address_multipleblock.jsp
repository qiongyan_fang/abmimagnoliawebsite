<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<div class="col-xs-12 col-md-6 col-lg-4">
	<h4>${content.centername}</h4>
	<h6><c:if test="${not empty content.centernamenote}">(${content.centernamenote})
	</c:if>
	</h6>

	<c:if test="${not empty content.mailingaddress}">
		<p>
			<strong>Mailing Address:</strong><br> ${content.mailingaddress}
		</p>
	</c:if>
	<c:if test="${not empty content.streetaddress}">
		<p>
			<strong>Street Address:</strong><br> ${content.streetaddress}
		</p>
	</c:if>
	<p>
		<c:if test="${not empty content.phone}">
			<strong>Phone:</strong> ${content.phone}<br>
		</c:if>
		<c:if test="${not empty content.fax}">
			<strong>Fax:</strong> ${content.fax} <br>
		</c:if>
		<c:if test="${not empty content.email}">
			<a href="mailto:${content.email}">${content.email }</a>
		</c:if>
	</p>



</div>
