<%@ include file="../includes/taglibs.jsp"%>

<div class="row contact-form">
<form:form action="?" commandName="contactForm" method="POST">
    <blossom:pecid-input />
    <div class="col-xs-12 col-md-6">
							<label for="input-yourName">Your Name:</label>
							<form:input path="senderName" placeholder="What do they call you?" />&nbsp;<form:errors path="senderName" /><br/>
							
						</div>
    
    <div class="col-xs-12 col-md-6">
							<label for="input-yourEmail">Your Email:</label>
							
							   <form:input path="senderEmail" placeholder="you@youremail.com"  />&nbsp;<form:errors path="senderEmail" /><br/>
						</div>
   
 <!--
 <div class="col-xs-12 col-md-6">
							<label for="select-department">Department:</label>
							<form:select path="department">
					  <form:option value="NONE" label="--- Please Choose ---" />
					  <form:options items="${departmentList}" />
				       </form:select>
						</div>
-->						
   <div class="col-xs-12 col-md-6">
							<label for="input-yourSubject">Your Subject:</label>
							 <form:input id="input-yourSubject" path="subject" placeholder="your subject"/>&nbsp;<br/>
							
						</div>
						
						<div class="col-xs-12">
							<label for="textarea-yourMessage">Your Message:</label>
							 <form:textarea path="message" cols="60" rows="20" placeholder="What's on your mind?" id="textarea-yourMessage"/><br/>
							
						</div>
<div class="col-xs-12">
							 <input type="submit" value="Send" />
						</div>
   
<form:hidden path="receiverEmail" />
</form:form>
</div>