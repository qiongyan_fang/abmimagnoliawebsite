<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="main careers">
	<c:forEach items="${jobs}" var="job">
		<c:set var="image" value="${cmsfn:content(member.photo,'dam')}" />
<c:set var="jobstatus" value="${job.status=='Open' or empty job.status?'openforapps':'reviewingapps'}" />

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="col-lg-12 ${jobstatus}">
				<div class="heading">

					<h4>${job.position}</h4>
					<c:if test="${not empty job.deadline}">
						<p class="date note">Application Deadline:&nbsp;&nbsp;${job.deadline}</p>
					</c:if>
					<c:if test="${ empty job.deadline}">
						<p class="date note">Application Deadline:&nbsp;&nbsp;as soon as possible</p>
					</c:if>
					<div class="minheight120">${job.description}</div>
				</div>
				<a href="${pageContext.request.contextPath}${job.path}.html"
					class="btn btn-learn">LEARN MORE</a>
				<!--<a href="${job.applylink}" class="btn btn-learn">APPLY NOW</a> -->
				<!-- <p class="status note">STATUS: ${job.status}</p>-->
			</div>
		</div>

	</c:forEach>
</div>