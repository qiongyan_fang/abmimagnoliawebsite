<%@ include file="../includes/taglibs.jsp" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<c:set var="blockid" value="${fn:replace(content.header,' ', '')}" />	
 <div id="${blockid}">
 <div class="${content.imageposition}">
 <c:if test="${empty content.imagepath}" >
<img alt="" src="${cmsfn:link(cmsfn:asContentMap(cmsfn:content(content.imagepath,'dam')))}">
</c:if>

 </div> 
${cmsfn:decode(content).text}
</div>