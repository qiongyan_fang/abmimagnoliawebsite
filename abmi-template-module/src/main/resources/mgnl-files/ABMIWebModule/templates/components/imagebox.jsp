<%@ include file="../includes/taglibs.jsp"%>



<div class="projectboxes">
	<c:choose>
		<c:when test="${content.boxnum == 'narrow'}">
			<c:set var="divclass" value="col-sm-4 col-xs-12" />
		</c:when>
		<c:when test="${content.boxnum == 'wide'}">
			<c:set var="divclass" value="widebox col-lg-11  col-xs-12" />
		</c:when>
		<c:when test="${content.boxnum == 'medium'}">
			<c:set var="divclass" value="widebox col-md-6 col-xs-12" />
		</c:when>
		<c:otherwise>
			<c:set var="divclass" value="col-lg-4 col-md-4 col-sm-4 col-xs-12" />
		</c:otherwise>
	</c:choose>
	<div class="${divclass}">
		<c:if test="${not empty content.imageUrl}">
			<c:set var="topImage"
				value="${cmsfn:content(content.imageUrl,'dam')}" />
			<div class="logo-heading">
				<img alt="" src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
			</div>
		</c:if>
		<div class="col-lg-12 ${content.backgroundcolor} colorbox">
			<c:if test="${not empty content.logoUr}">
				<c:set var="logoImage"
					value="${cmsfn:content(content.logoUrl,'dam')}" />
				<img alt="logo" src="${cmsfn:link(cmsfn:asContentMap(logoImage))}">
			</c:if>
			<c:if test="${not empty content.header}">
				<h4>${content.header}</h4>
			</c:if>
			<c:if test="${content.descriptionrow}">
				<c:set var="minheightclass"
					value="style='min-height:${1.6*(content.descriptionrow+1)}em;'" />
			</c:if>
			<p ${minheightclass}>${cmsfn:decode(content).description}</p>
			<c:if test="${not empty content.buttonText}">
				<a class="btn btn-learn" target=_blank href="${content.externalUrl}">${content.buttonText}</a>
			</c:if>
		</div>
	</div>
</div>

