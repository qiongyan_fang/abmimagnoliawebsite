<%@ include file="../includes/taglibs.jsp"%>
<div class="row contact-form">
	<form:form action="?" commandName="contactForm" method="POST"
		class="col-xs-12">
		<blossom:pecid-input />
		<form:errors class="errorMsg" path="senderName" />
		<form:input class="form-input" path="senderName"
			placeholder="Your Name" />&nbsp;
	
		<form:errors class="errorMsg" path="senderEmail" />
		<form:input class="form-input" path="senderEmail"
			placeholder="Your Email" />&nbsp;
		
		<form:input class="form-input" id="input-yourSubject" path="subject"
			placeholder="Your subject" />&nbsp;<br />


		<form:textarea class="form-input" path="message" cols="60" rows="20"
			placeholder="What's on your mind?" id="textarea-yourMessage" />



		<input class="submit" type="submit" value="Send" />


		<form:hidden path="receiverEmail" />
	</form:form>
</div>