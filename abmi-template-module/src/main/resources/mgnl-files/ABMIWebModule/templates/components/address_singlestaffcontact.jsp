<%@ include file="../includes/taglibs.jsp" %>

<c:set var="classname">${state.getCurrentContentNode().getParent().getParent().getUUID()}</c:set>

<div class="col-xs-12 col-sm-4 col-md-3 a${classname}">
<p><strong>${content.staffname }</strong>
<BR>${content.stafftitle}
<c:if test="${not empty content.phone}">
						<br>
						<b>Phone</b>: <a href="tel:1${content.phone}">${content.phone}</a>
      </c:if>
					<c:if test="${not empty content.cell}">
						<br>
						<b>Cell</b>: ${content.cell}
      </c:if>
					<c:if test="${not empty content.email}">
						<br><a href="mailto:${content.email}">${content.email}</a>
      </c:if>
</P>
</div>
