<%@ include file="../includes/taglibs.jsp"%>
<!--  header, clear style -->
<c:if test="${not empty content.paddingtop}">
<div style="margin-top:${content.paddingtop}px;"></div>
</c:if>

<c:if test="${not empty content.color}">
<c:set var="className">
class="txt-${content.color}"
</c:set>
</c:if>

<c:choose>

  <c:when test="${content.type eq 'header 4' or content.type eq 'h4'}">
    <h4 ${className} style="padding-left:${content.padding}px">${content.text}</h4>
  </c:when>
  <c:when test="${content.type eq 'header 5' or content.type eq 'h5' }">
   <h5 ${className} style="padding-left:${content.padding}px">${content.text}</h5>
  </c:when>
  <c:when test="${content.type eq 'h1' }">
   <h1 ${className} style="padding-left:${content.padding}px">${content.text}</h1>
  </c:when>
  <c:when test="${content.type eq 'h2' }">
   <h2 ${className} style="padding-left:${content.padding}px">${content.text}</h2>
  </c:when>
  <c:when test="${content.type eq 'h3' }">
   <h3 ${className} style="padding-left:${content.padding}px">${content.text}</h3>
  </c:when>
  <c:when test="${content.type eq 'h6' }">
   <h6 ${className} style="padding-left:${content.padding}px">${content.text}</h6>
  </c:when>
  
    <c:when test="${content.type eq 'start at new line'}">
   <div style="clear:both;"></div>
  </c:when>
   <c:when test="${content.type eq 'horizontal line'}">
   <hr/>
  </c:when>
 
</c:choose>
<c:if test="${not empty content.paddingbottom}">
<div style="margin-bottom:${content.paddingbottom}px;"></div>
</c:if>
