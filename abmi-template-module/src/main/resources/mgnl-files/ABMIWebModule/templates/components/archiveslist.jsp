
<%@ include file="../includes/taglibs.jsp"%>

<c:if test="${archiveNews.size() > 0}">
	<h4>Archive</h4>
</c:if>

<ul>


	<c:forEach items="${archiveNews}" var="archiveTime">

		<c:if test="${archiveTime eq param.time}">
			<li class="active"><a href="#">${archiveTime}</a></li>
		</c:if>
		<c:if test="${archiveTime ne param.time}">
			<li><a
				href="${pageContext.request.contextPath}?time=${archiveTime}">${archiveTime}</a></li>
		</c:if>
	</c:forEach>
</ul>