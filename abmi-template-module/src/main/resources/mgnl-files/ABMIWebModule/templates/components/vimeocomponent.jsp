<%@ include file="../includes/taglibs.jsp"%>
<c:set var="contentMap" value="${cmsfn:content(content.imageurl,'dam')}" />
<c:set var="imagePath"
	value="${empty content.imageurl?'/docroot/assets/faux-vid.jpg':cmsfn:link(cmsfn:asContentMap(contentMap))}" />
	
	<c:set var="divId">${state.getCurrentContentNode().getUUID()}</c:set>
	
<div aria-hidden="true" aria-labelledby="${content.videoId}"
	role="dialog" tabindex="-1" id="${divId}"
	class="modal fade" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<iframe frameborder="0" width="853" height="480"
					src="//player.vimeo.com/video/${content.videoId}"></iframe>
			</div>
		</div>
	</div>
</div>
<c:if test="${not empty content.videotext}">
	<c:if test="${content.showgreenbox}">
		<div class="postheader">

			<a href="#" data-toggle="modal" data-target="#${divId}">

				<div style="position: relative;">

					<img alt=""  ${content.useimagesize?"":"style='width:100%;height:auto;'"} src="${imagePath}"> <span class="playbutton"> </span>

				</div>
			
			 <h5>${content.videotext }</h5>
		</div>
	</c:if>
	<c:if test="${not content.showgreenbox}">
		<h5>${content.videotext }</h5>
		<a href="#" data-toggle="modal" data-target="#${divId}"><div style="position: relative;">

					<img alt=""  ${content.useimagesize?"":"style='width:100%;height:auto;'"} src="${imagePath}"> <span class="playbutton"> </span>

				</div></a>
	</c:if>
</c:if>
<p>