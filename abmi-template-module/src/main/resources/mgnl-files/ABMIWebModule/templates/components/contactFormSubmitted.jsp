<%@ include file="../includes/taglibs.jsp"%>

<c:if test="${empty param.error}">
<h2>Thanks for your message</h2>

<p>An ABMI staff (${content.sendto}) will be in contact with you shortly.</p>
</c:if>
<c:if test="${not empty param.error}">
<h2> sorry but an error occurred, please send email directly to abmiinfo@ualberta.ca.
${param.error}
</h2>
</c:if>