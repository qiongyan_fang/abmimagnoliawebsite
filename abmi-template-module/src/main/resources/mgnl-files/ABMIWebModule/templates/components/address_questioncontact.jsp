
<%@ include file="../includes/taglibs.jsp"%>
<div class=""> <!-- it has paddings col-lg-6 col-md-6 col-sm-6 col-xs-12"> -->
	<h4 class="contact question-contact">${content.staffname}</h4>
	<c:if test="${not empty content.stafftitle}">
		<p>
			${content.stafftitle}<br />
	</c:if>
<c:if test="${not empty content.phone}">
	${content.phone}<br />
</c:if>
	<c:if test="${not empty content.address}">
${content.address}<br />
	</c:if>
	<c:if test="${not empty content.email}">
		<a href="mailto:${content.email}">${content.email}</a>
	</c:if>
	</p>
</div>

