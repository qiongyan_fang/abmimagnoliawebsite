<%--@elvariable id="components" type="java.util.Collection"--%>
<%@ include file="../includes/taglibs.jsp"%>

<c:if test="${components.size() > 0 }">
	<h4>${ empty content.header?"Board of Directors": content.header}</h4>
	<ul>
		<c:forEach items="${components}" var="component">
			<cms:component content="${component}" />
		</c:forEach>
	</ul>

</c:if>
