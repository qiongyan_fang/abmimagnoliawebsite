<%@ include file="../includes/taglibs.jsp"%>



<c:if test="${components.size() > 0}">
	<!-- c:set var="divId" value="{content.id}" /-->
	<c:set var="divId">${state.getCurrentContentNode().getUUID()}</c:set>
	<div class="tile">
		<div data-ride="carousel" class="carousel slide carousel-${content.imageposition}-image" id="${divId}">

			<ol class="carousel-indicators">
				<c:forEach var="i" begin="0" end="${components.size() -1}">
					<c:if test="${i eq 0}">
						<li class="active" data-slide-to="${i}" data-target="#${divId}"></li>
					</c:if>
					<c:if test="${i > 0}">
						<li class="" data-slide-to="${i}" data-target="#${divId}"></li>
					</c:if>



				</c:forEach>
			</ol>

			<div class="carousel-inner">

				<c:forEach items="${components}" var="component">
					<cms:component content="${component}" />
				</c:forEach>
			</div>
		</div>
	</div>
</c:if>