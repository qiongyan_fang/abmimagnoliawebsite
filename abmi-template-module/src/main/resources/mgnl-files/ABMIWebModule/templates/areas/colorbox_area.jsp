<%@ include file="../includes/taglibs.jsp"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<c:forEach items="${components}" var="component"  varStatus="status">

<div class="prj${status.index%6 + 1 }">
<cms:component content="${component}" />

</div>
</c:forEach>
