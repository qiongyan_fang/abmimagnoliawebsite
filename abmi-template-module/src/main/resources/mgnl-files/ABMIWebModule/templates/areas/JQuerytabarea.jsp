 
<%@ include file="../includes/taglibs.jsp" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

 <div id="jqueryTabs">

<ul>
	<c:forEach items="${components}" var="component" varStatus="indexrow">
	<c:set var="blockid" value="${fn:replace(component.header,' ', '')}" />		
	<li ><a href="#${blockid}"><h5>${component.header}</h5></a></li>
	</c:forEach>
	</ul>
	<hr>
	
  <c:forEach items="${components}" var="component">
    <cms:component content="${component}" />
  </c:forEach>
  </div>