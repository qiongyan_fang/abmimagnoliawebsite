<%@ include file="../includes/taglibs.jsp" %>

  <c:forEach items="${components}" var="component">
    <div class="col-sm-6 col-md-6 col-lg-3 partner">
    <cms:component content="${component}" />
    </div>
  </c:forEach>

