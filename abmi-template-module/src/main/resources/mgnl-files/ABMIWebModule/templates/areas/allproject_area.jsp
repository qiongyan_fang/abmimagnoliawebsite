
<%@ include file="../includes/taglibs.jsp"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:choose>
<c:when test="${boxWidth == 'narrow'}">
<c:set var="divclass" value="col-sm-4 col-xs-12" />
</c:when>
<c:when test="${boxWidth == 'wide'}">
<c:set var="divclass" value="col-sm-6 col-xs-12" />
</c:when>
<c:otherwise>
<c:set var="divclass" value="col-lg-4 col-md-4 col-sm-4 col-xs-12" />
</c:otherwise>
</c:choose>

<div class="projectboxes">
<c:forEach items="${components}" var="component"  varStatus="status">
<div class="${divclass}">
<div class="prj${status.index%6 + 1 }">
<cms:component content="${component}" />
</div>
</div>
</c:forEach>

<c:forEach items="${projects}" var="row" varStatus="rowStatus">
<div class="${divclass}">
<div class="prj${(components.size() + rowStatus.index)%6 +1}">



<c:if test="${not empty row.imageUrl}">
<c:set var="topImage" value="${cmsfn:content(row.imageUrl,'dam')}" />
<div class="logo-heading">
<img alt="top image"  src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
</div>
</c:if>
<div class="col-lg-12 colorbox">
<c:if test="${not empty row.logoUrl}">
<c:set var="logoImage" value="${cmsfn:content(row.logoUrl,'dam')}" />
<div class="projectshorttitle"><img alt="logo" src="${cmsfn:link(cmsfn:asContentMap(logoImage))}"></div>
</c:if>
<c:if test="${empty row.logoUrl and not empty row.projecttitle}">
<div class="projectshorttitle">${row.projecttitle}</div>
</c:if>
<p>${row.description}</p>
<c:if test="${empty row.externalurl}">
<a class="btn btn-learn" href="${row.path}.html">${empty content.buttontext?"VIEW PROJECT":content.buttontext}</a>
</c:if>
<c:if test="${not empty row.externalurl}">
<a class="btn btn-learn" target=_blank href="${row.externalurl}">${empty content.buttontext?"VIEW WEBSITE":content.buttontext}</a>
</c:if>

</div>

</div>
</div>
</c:forEach>

					
</div>