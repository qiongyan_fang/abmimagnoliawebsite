<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
  
  <body class="container-fluid preload">
   <%@ include file="../includes/menu.jsp"%>
	
	<div class="row">
		<cms:area name="jumbotron"/>
	</div>
	<div class="row breadcrumb" >
		<div class="container">
			<div class="col-lg-12">
				<h3><cms:area name="titleArea" /></h3>
			</div>
		</div>
	
	</div>
	<div class="row breadcrumb-triangle" >
		<div class="container">
			<div class="col-lg-12">
			<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
		<div class="col-lg-8 main">	
			<cms:area name="leftColumnArea" />
		</div>	
		<div class="col-lg-4 aside">
				
					<cms:area name="rightColumnArea" />
					
					<cms:area name="rightWidgetColumnArea" />	
				
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<div class="row"><!--
				<div class="col-lg-12">
					<h3><cms:area name="bottomHeaderArea" /></h3>
				</div> -->
			</div>
			<div class="col-lg-12">
			<cms:area name="bottomRowBoxArea" />
			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
    
  </body>
</html>