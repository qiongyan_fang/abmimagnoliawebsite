
<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>


<body class="container-fluid events-page all-events preload">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
			<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
<div class="col-lg-9 main">
			
			<c:set var="topImage"
				value="${cmsfn:content(content.imageUrl,'dam')}" />

			<div class="post">
				<div class="postheader ${empty topImage?"no-image":""}">
					<c:if test="${not empty topImage }">
						<img alt="top image"
							src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
					</c:if>
					<h3>${content.imagetitle}</h3>
				</div>
				<div class="arrowcontainer">
					<div class="postmeta">
<c:set var="startDate"><fmt:formatDate value='${content.eventstartdate.getTime()}'/></c:set>
<c:set var="endDate"><fmt:formatDate value="${content.eventenddate.getTime() }"/></c:set>
<p>

											${startDate}
											<c:if test="${startDate ne endDate}">
											- ${endDate}
											
											</c:if>

</p>
					</div>
					
				</div>
				<div class="postactions">
						<div class="post-box-share addthis_sharing_toolbox" 
						data-url="http://${pageContext.request.serverName}${pageContext.request.contextPath}${state.handle}.html"></div>
				</div>
				<div class="postexerpt"> <!--  with indent -->
				
					<h4>${content.title}</h4>
				
					<p>${content.description}</p>
					<h5><b>Time: </b>${content.eventstarttime} - ${content.eventendtime}</h5>
					<h5><b>Location: </b>${content.eventlocation}</h5>

				


				<cms:area name="leftColumnArea" />
</div>
				


			</div>

</div>
			<div class="col-lg-3 col-md-12 col-sm-12 aside">
				
					<cms:area name="rightColumnArea" />
				

				<cms:area name="rightWidgetColumnArea" />

			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
	
</body>
</html>