
<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>

<body class="container-fluid">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
<div class="col-lg-9 main">
			<c:set var="viewLink"
				value="${cmsfn:content(content.viewlink,'dam')}" />
		


			<c:set var="topImage"
				value="${cmsfn:content(content.imageUrl,'dam')}" />

			<div class="post">
				<div class="postheader ${empty topImage?"no-image":""}">
					<c:if test="${not empty topImage }">
						<img alt="top image"
							src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
					</c:if>
					<h3>${content.imagetitle}</h3>
				</div>
				<div class="arrowcontainer">
					<div class="postmeta">
					<p>
										<c:if test="${not empty content.author}">${cmsfn:decode(content).author}<br> </c:if> 
										<c:if test="${not empty content.publishdate}"> <fmt:formatDate pattern="MMMM dd, yyyy" value="${content.publishdate.getTime() }"/></c:if>
										</p>
					
						
					</div>
					<!-- 
					<div class="postmeta arrow"
						style="border-width: 10px 290px 0px 0px;"></div>
						-->
				</div>
				<div class="postactions">
				<c:if test="${not empty viewLink}">
					<p>
						<a href="${cmsfn:link(cmsfn:asContentMap(viewLink))}" class="pdf">VIEW PDF</a> <!-- | <a
							download href="${cmsfn:link(cmsfn:asContentMap(downloadLink))}">DOWNLOAD</a> -->
					</p>
					</c:if>
					<div class="post-box-share addthis_sharing_toolbox" data-url="http://${pageContext.request.serverName}${pageContext.request.contextPath}${state.handle}
.html"></div>
				</div>
				<div class="postexerpt">
					<h4>${cmsfn:decode(content).title}</h4>
						<h4>${cmsfn:decode(content).doctitle}</h4>
									<p>${cmsfn:decode(content).description}</p>
									<h5>${cmsfn:decode(content).subtitle}</h5>
								
							<c:if test="${not empty content_documenttype}">
										<p class="documenttype">DOCUMENT TYPE:
											<c:forEach items="${content_documenttype}" var="row"
												varStatus="tagStatus">
												<a href="${state.originalBrowserURI}?documenttype=${row}">${row}</a><c:if test="${not tagStatus.last }">,
										</c:if>
											</c:forEach>
										</p>
									</c:if>
									
										<c:if test="${not empty content_subject}">
										<p class="subjectarea">SUBJECT AREA: 
											<c:forEach items="${content_subject}" var="row"
												varStatus="tagStatus">
												<a href="${state.originalBrowserURI}?subject=${row}">${row}</a><c:if test="${not tagStatus.last }">,
										</c:if>
											</c:forEach>
										</p>
									</c:if>
									
									<c:if test="${not empty content_keyword}">
										<p class="subjectarea">KEYWORDS:
											<c:forEach items="${content_keyword}" var="tag"
												varStatus="tagStatus">
												<a href="${state.originalBrowserURI}?keyword=${tag}">${tag}</a><c:if test="${not tagStatus.last }">,</c:if>
											</c:forEach>
										</p>
									</c:if>
				<cms:area name="leftColumnArea" />

				</div>


			</div>

</div>
			<div class="col-lg-3 col-md-12 col-sm-12 aside">
				
					<cms:area name="rightColumnArea" />
				


				<cms:area name="rightWidgetColumnArea" />

			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
	
</body>
</html>