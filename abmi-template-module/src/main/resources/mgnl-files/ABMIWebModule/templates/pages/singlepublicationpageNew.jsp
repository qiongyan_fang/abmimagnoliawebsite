
<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>

<body class="container-fluid">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<!--  ===================  FLOATING NAV =============== -->
			<div class="side-top-nav col-xs-12 col-lg-3">

<c:set var="parentUri" value="${not empty content.rootpath?content.rootpath:'/home/publications'}.html" />
				<div class="widget listSyle">
				
<c:if test="${futureRelease}">				<!--  encode url -->
<h5>This publication will be released in future</h5>
</c:if>

				<c:url var="queryuri" value="${parentUri}">
  <c:param name="documenttype" value="${param.documenttype}"/>
  <c:if test='${not empty param.mode}'> <c:param name="mode" value="${param.mode}"/></c:if>
  <c:if test='${not empty param.subject}'><c:param name="subject" value="${param.subject}" /></c:if>
  
										<c:if test='${not empty param.time}'><c:param name="time" value="${param.time}" /></c:if>
										<c:if test='${not empty param.keyword}'><c:param name="keyword" value="${param.keyword}" /></c:if>
										<c:if test='${not empty param.page}'><c:param name="page" value="${param.page}" /></c:if>
										
</c:url>
				<!-- 	<a href="${parentUri}?mode=detail"
						class="sidemenuFullPostBtn">&nbsp;</a> <a
						href="${parentUri}?mode=list"
						class="sidemenuListPostBtn">&nbsp;</a> -- -->
						<a href="${parentUri}?${pageContext.request.queryString}" class="backtoallBtn">Back</a>
					
					</div>

			<%@ include file="publicationsearch.jsp"%>
			</div>
			<div class="clear-to-lg"></div>
			<!--  ================================== -->
		<c:if test="${not empty content.doi}">
				<c:set var="viewLinkUrl">
				<c:if test="${fn:startsWith(content.doi, 'http')}">
				${content.doi}
				</c:if>
				<c:if test="${not fn:startsWith(content.doi, 'http')}">
				http://doi.org/${content.doi}
				</c:if>
				</c:set> 
				</c:if>
			<!--  ===================  MAIN COLUMN =============== -->
			<div class="col-lg-9 main">

				<c:set var="viewLink"
					value="${cmsfn:content(content.viewlink,'dam')}" />
				<c:set var="topImage"
					value="${cmsfn:content(content.imageUrl,'dam')}" />

		<c:set var="ftpprefix" value="http://ftp.public.abmi.ca/" />
				<div class="publication-post fullpost">
					<div class="post-featureimg-large">

						<c:if test="${not empty topImage }">
							<img alt="top image"
								src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
						</c:if>
					</div>
					<div class="post-details">
						<div class="row">
							<div class="post-header col-sm-9">
								<h4>${not empty content.title? content.title:
									content.imagetitle}</h4>
								<div class="post-data">
									<a href="#" class="post-by">${cmsfn:decode(content).author}</a>
									<span class="post-date">${content.displaydate}</span>
									<c:if test="${content_supplementalreportslink.size() >0}"><span
											class="post-date">${content_supplementalreportslink.size()} ${content_supplementalreportslink.size()>1?"Resources":"Resource"}</span></c:if>
											
											<c:if test="${allVersions.size() >1}">
											<c:if test="${content.isoldversion}">
											<span
											class="post-date">Newer Version Available</span>
											</c:if>
											<c:if test="${not content.isoldversion}">
											<span
											class="post-date">${allVersions.size()-1} ${allVersions.size()>1?"Past Document":"Past Documents"}</span></c:if>
											</c:if>
	<c:set var="downloadclass" value="${not empty content.flipbooklink?'post-date':''}" />		
<c:choose>
											<c:when test="${not empty viewLinkUrl}">
											<a  type="text/html" class="pdf ${downloadclass }"
										href="${viewLinkUrl}" target=_blank>VIEW
										</a>
											</c:when>
											<c:when test="${not empty content.viewlink}">
									<c:set var="viewlink"
										value="${cmsfn:content(content.viewlink,'dam')}" />
							<a download class="post-pdf ${downloadclass}"
										href="${ftpprefix}${content.viewlink}">DOWNLOAD</a>
										
								
								</c:when>
								</c:choose>

						<!--  show flipbook link -->
								<c:if test="${not empty content.flipbooklink}">
								<a  class="post-flipbook" target="_blank"
										href="${ftpprefix}${content.flipbooklink}">${empty content.flipbooklinktext?"FLIPBOOK":content.flipbooklinktext}</a>
								</c:if>
	

							<!--  end of show flipbook link -->

								</div>
							</div>
							<div class="postactions col-sm-3">
								<div class="post-box-share addthis_sharing_toolbox"
									data-url="http://${pageContext.request.serverName}${pageContext.request.contextPath}${content.path}.html"></div>
							</div>
						</div>
						<div class="post-desc">
							<p>${cmsfn:decode(content).description}</p>
							<p class="documenttype">
								DOCUMENT TYPE:
								<c:forEach items="${content_documenttype}" var="row"
									varStatus="tagStatus">
									<a href="<c:url value='${parentUri}'><c:param name='documenttype' value='${row.key}' /> </c:url>">${row.value}<c:if test="${not tagStatus.last }">,
										</c:if></a>
									
								</c:forEach>
							</p>
							<p class="subjectarea">
								SUBJECT AREA:
								<c:forEach items="${content_subject}" var="row"
									varStatus="tagStatus">
									<a href="<c:url value='${parentUri}'><c:param name='subject' value='${row.key}' /> </c:url>"">${row.value}<c:if test="${not tagStatus.last }">,
										</c:if></a>
									
										</c:forEach>
							</p>
							<p class="subjectarea">
								TAGS:
								<c:forEach items="${content_keyword}" var="row"
									varStatus="tagStatus">
									<a href="<c:url value='${parentUri}'><c:param name='keyword' value='${row}' /> </c:url>"">${row}<c:if test="${not tagStatus.last }">,</c:if></a>
									
								</c:forEach>
							</p>
<!--  show flipbook link -->
<!--  show flipbook link -->
	<c:if test="${not empty content.flipbooklink and not empty content.previewflipimg}">
		<c:set var="flipbookImage"
					value="${cmsfn:content(content.previewflipimg,'dam')}" />
<p class="flipbook_img"><a  type="text/html" target=_blank href="${ftpprefix}${content.flipbooklink}">
	<img  src="${cmsfn:link(cmsfn:asContentMap(flipbookImage))}" alt="flipbook cover" />
	</a></P>

	</c:if>
	
	

							<!--  end of show flipbook link -->


						</div>
					

						<c:if test="${content_supplementalreportslink.size()>0}">
							<ul class="faq supplementary">
								<li><input type="button" class="collapsed"
									value="Supplementary Reports" data-target="#report1"
									data-toggle="collapse">
									<div class="collapse" id="report1" style="height: auto;">
<div class="postactions">										
<c:forEach items="${content_supplementalreportslink}"
												var="suppdoc">
												<!-- c:set var="supViewLink"
													value="{cmsfn:content(suppdoc.viewlink,'dam')}" />
													 -->
												
													<p>
													  <a class="pdf"
															href="${ftpprefix}${suppdoc.viewlink}">
															${suppdoc.title}<c:if test="${not empty suppdoc.extension}"><span class="pdf-size">(${suppdoc.extension} ${suppdoc.size} )</span></c:if>
															</a>
														<!-- <a class="pdf"
															href="{cmsfn:link(cmsfn:asContentMap(supViewLink))}">
															{suppdoc.title}c:if test="${not empty suppdoc.extension}"><span class="pdf-size">(${suppdoc.extension} ${suppdoc.size} )</span>/c:if
															</a>
															 -->
													</p>

												

											</c:forEach>
</div>
									
									</div></li>
							</ul>

						</c:if>

<!--  section for multiple versions -->
	<c:if test="${totalVersionCount > 0 and allVersions.size() > 1}">
							<ul class="faq supplementary">
								<li><input type="button" class="collapsed"
									value="Past Documents" data-target="#version1"
									data-toggle="collapse">
									<div class="collapse" id="version1" style="height: auto;">
<div class="postactions">										
<c:forEach items="${allVersions}"
												var="versiondoc">
												<!-- c:set var="supViewLink"
													value="{cmsfn:content(suppdoc.viewlink,'dam')}" />
													 -->
												
													<p>
													  <a 
															href="${versiondoc.path}.html">
															${versiondoc.title} - ${versiondoc.Version}
															</a> <c:if test="${not versiondoc.isoldversion && versiondoc.Version!= content.version}">Newest Version</c:if>
														</p>
											</c:forEach>
</div>
									</div</li>
							</ul>

						</c:if>

				</div>
					
					</div>
					<cms:area name="leftColumnArea" />


				</div>
				





				<div class="col-lg-3 col-md-12 col-sm-12 aside">

					<cms:area name="rightColumnArea" />



					<cms:area name="rightWidgetColumnArea" />

				</div>
			</div>
		</div>
		<%@ include file="../includes/footer.jsp"%>
	<%@ include file="publicationgooglesearch.jsp"%>
</body>
</html>