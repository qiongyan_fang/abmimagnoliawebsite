<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>

<body class="container-fluid careers">
	<%@ include file="../includes/menu.jsp"%>
	<!--  header -->
	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>
	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<div class="col-xs-12 col-lg-8 main breathH">


				<h2>${content.position}</h2>
				<c:if test="${not empty content.postdate }">
					<h5>
						Posted Date:
						<fmt:formatDate value="${content.postdate.getTime() }" />
					</h5>
					</if>

					<c:if test="${not empty content.postdate }">
						<h5>
							Application Deadline:
							<c:if test="${empty content.deadline}">
					 as soon as possible
					 </c:if>
							<c:if test="${not empty content.deadline}">
								<fmt:formatDate value="${content.deadline.getTime()}" />
							</c:if>
						</h5>
						</if>
						<p class="bold-green">${cmsfn:decode(content).description}</p>
						<c:if test="${not empty content.jobpostupload}">
							<c:set var="urlPost"
								value="${cmsfn:content(content.jobpostupload,'dam')}" />

							<p>
								<a
									href="${pageContext.request.contextPath}${cmsfn:link(content.jobpostupload)}"
									download>DOWNLOAD</a>
							</p>


						</c:if>


						<c:set var="curDate" value="<%=new java.util.Date()%>" />
						<h4>
							Status:
							<c:if test="${empty content.deadline}">Open</c:if>
							<c:if test="${not empty content.deadline}">${curDate< content.deadline.getTime()
					?"Open":"Closed"} ${content.status}</c:if>
						</h4>

						<cms:area name="jobLeftColumnArea" />
			</div>
			<div class="col-lg-3 col-lg-offset-1 col-md-3 col-sm-12 aside">


				<cms:area name="rightColumnArea" />


				<cms:area name="rightWidgetArea" />


			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>