
<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>


<body class="container-fluid events-page all-events preload">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
		
<div class="side-top-nav col-xs-12 col-lg-3">
				<div class="widget listSyle">
<c:set var="parentUri" value="${not empty content.rootpath?content.rootpath:state.mainContentNode.parent.path}.html" />
	<a class="sidemenuFullPostBtn" href="${parentUri}?mode=detail">&nbsp;</a>
						<a class="sidemenuListPostBtn"
							href="${parentUri}?mode=list">&nbsp;</a>

<a class="sidemenuEventPostBtn"
href="${parentUri}?mode=calendar&scroll">&nbsp;</a>


						<a href="${parentUri}?mode=${empty param.mode?'detail':param.mode}
										<c:if test='${not empty param.page}'>&page=${param.page}</c:if>
										<c:if test='${not empty param.time}'>&time=${param.time}</c:if>
										"
							class="backtoallBtn">Back</a>
</div>
</div>
	<div class="col-lg-9 main">
		<c:set var="viewLink"
					value="${cmsfn:content(content.viewlink,'dam')}" />
				<c:set var="topImage"
					value="${cmsfn:content(content.imageUrl,'dam')}" />
					
				<div class="event-post fullpost">
					<div class="post-featureimg-large">
					<c:if test="${not empty topImage}">
						<img 
							src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
					</c:if>
					</div>
					<div class="post-details">
						<div class="row">
							<div class="post-header col-sm-9">
								<h4>${content.title}</h4>
								<c:set var="startDate">
									<fmt:formatDate pattern="MMMM dd, yyyy" value='${content.eventstartdate.getTime()}' />
								</c:set>
								<c:set var="endDate">
									<fmt:formatDate pattern="MMMM dd, yyyy" value="${content.eventenddate.getTime() }" />
								</c:set>
								<div class="post-data">
									
								<c:set var="publishDate">
									<fmt:formatDate pattern="MMMM dd, yyyy"  value='${content.publishdate.getTime()}' />
								</c:set>
									<span>POSTED ON: </span> <span class="post-date1">${publishDate}</span>
								</div>

							
							</div>
							<div class="postactions col-sm-3">
								<div class="post-box-share addthis_sharing_toolbox"
									data-url="http://${pageContext.request.serverName}${pageContext.request.contextPath}${state.handle}.html"
									data-title="ABMI"></div>

							</div>
						</div>
						<div class="post-desc">
							<div class="event-detail">
								<p>
									DATE:<span><span>${startDate}<c:if
												test="${startDate ne endDate}">
											- ${endDate}
											
											</c:if></span></span>
								<br>
									TIME:<span>${content.eventstarttime} -
										${content.eventendtime}</span>
								<br>
									LOCATION:<span> ${content.eventlocation}</span>
								</p>
								
							</div>

							<p>${content.description}
						</div>
						
						</div>
					</div>
				<cms:area name="leftColumnArea" />
				



			</div>

	
		<div class="col-lg-3 col-md-12 col-sm-12 aside">

		<cms:area name="rightColumnArea" />


		<cms:area name="rightWidgetColumnArea" />

		</div>
	</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>

</body>
</html>