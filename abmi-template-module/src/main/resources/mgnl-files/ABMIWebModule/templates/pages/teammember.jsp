<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
  
  <body class="container-fluid board-of-directors teammember">
  <%@ include file="../includes/menu.jsp"%>
	<div class="row">
	<cms:area name="jumbotron"/>
 <!--  header -->
	
	</div>
	<div class="row breadcrumb" >
		<div class="container">
			<div class="col-lg-12">
				<h3><cms:area name="titleArea" /></h3>
			</div>
		</div>
	</div>
	<div class="row breadcrumb-triangle" >
		<div class="container">
			<div class="col-lg-12">
			<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<div class="col-lg-9 main bod">
				 
            <div class="post1">
			<c:set var="image" value="${cmsfn:content(content.photo,'dam')}" />
			
					<div class="postheader">
						<img alt="" src="${cmsfn:link(cmsfn:asContentMap(image))}">
						<h3>${empty content.personname?title:content.personname}</h3>
					</div>
					<c:if test="${not empty content.position}">
						<div class="arrowcontainer">
							<div class="postmeta">
								<p>${cmsfn:decode(content).position }</p>
							</div>
						
						</div>
					</c:if>
					
					<div class="postexerpt">
						<h4>${cmsfn:decode(content).otherposition}</h4>
						<cms:area name="leftColumnArea" />
						
					</div>
				</div>
				
			</div>
			<div class="col-lg-3 col-md-12 col-sm-12 aside">
				
				
					<cms:area name="rightColumnArea"  />

				
				<cms:area name="rightWidgetArea" />	
				
				
			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
  </body>
</html>