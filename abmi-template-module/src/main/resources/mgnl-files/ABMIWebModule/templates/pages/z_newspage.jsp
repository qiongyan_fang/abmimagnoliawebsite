<%@ include file="../includes/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${content.title}</title>

    <link href="/docroot/css/bootstrap.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Roboto+Slab:300,400,700' rel='stylesheet' type='text/css'>
	
	<link href="/docroot/css/style.css" rel="stylesheet">
	
	
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      <cms:init />
  </head>
  <body class="container-fluid">
	<%@ include file="../includes/menu.jsp"%>
	<div class="row">
		<div class="jumbotron">
			<!-- Replace with html5 video tag -->
				<video width="100%" height="auto" autoplay poster="/docroot/assets/grasslands_video.jpg">
				  <source src="/docroot/assets/Video-01-Monitoring.mp4" type="video/mp4">
				  <source src="/docroot/assets/Video-01-Monitoring.ogv" type="video/ogg">
				  <source src="/docroot/assets/Video-01-Monitoring.webm" type="video/webm">
				</video>
			<div class="overlay">
				<h1 class="text-center">It's Our Nature to Know</h1>
				<h1 class="text-center sub-heading">2,564 species monitored to date</h1>
			</div>
		</div>
	</div>
	
	<div class="row divider">
		<h2 class="text-center">${content.title}</h2>
	</div>
	<div>
	<h1>${content.position}
<p><fmt:formatDate value="${content.deadline.getTime() }" />
<p><fmt:formatDate value="${content.postdate.getTime() }" />
<p>${content.status}
<p>${cmsfn:decode(content).description}
	<cms:area name="jobLftColumnArea" />
	</div>
	
	<div>right part
	<cms:area name="rightColumnArea" />
	</div>
	
<%@ include file="../includes/footer.jsp"%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/docroot//js/bootstrap.min.js"></script>
  </body>
</html>