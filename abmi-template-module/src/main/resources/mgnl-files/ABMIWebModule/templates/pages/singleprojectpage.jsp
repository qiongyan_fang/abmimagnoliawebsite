
<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>

<body class="container-fluid">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<div class="col-xs-12 col-lg-8 main">

				<div class="row dbm">
					<div class="section-photobox cyan">
						<div class="col-xs-12 col-sm-6 content">
							<h3>${content.projectName}</h3>
							<p>${content.description}</p>
						</div>
					<c:if test="${not empty content.imageUrl }">
<c:set var="topImage"
					value="${cmsfn:content(content.imageUrl,'dam')}" />
						<div class="col-xs-12 col-sm-6 image">				
							
							<img alt="top image"
								src="${cmsfn:link(cmsfn:asContentMap(topImage))}">

						</div>
						</c:if>
					</div>
					<div class="row">					
						<div class="col-xs-12 arrowcontainer">	
							<div class="photobox postmeta">
								<div class="meta-date"></div>
								<div class="postactions photo-box-share addthis_sharing_toolbox" data-url="http://${pageContext.request.serverName}${state.originalBrowserURI}"></div>
							</div>
							<div class="postmeta arrow"></div>
						</div>
					</div>	
				</div>


				
				<c:set var="logoImage"
					value="${cmsfn:content(content.logoUrl,'dam')}" />

				<div class="post">
					<!--<div class="postheader ${empty topImage?"no-image":""}">
						<c:if test="${not empty topImage }">
							<img alt="sampleImage"
								src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
						</c:if>
						<h3>${content.projectName}</h3>
					</div>
					<div class="arrowcontainer">
						<div class="postmeta">
							<c:if test="${not empty content.logoUrl }">
								<img alt="" src="${cmsfn:link(cmsfn:asContentMap(logoImage))}">
							</c:if>
						</div>

					</div>

					<div class="postexerpt">
						<h4></h4>
						<p>${content.description}</p>
					</div>
-->

					<cms:area name="leftColumnArea" />


				</div>

			</div>
			<div class="col-lg-3 col-lg-offset-1 col-sm-12 aside">
				
					<cms:area name="rightColumnArea" />
				

				<cms:area name="rightWidgetColumnArea" />

			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
	
</body>
</html>