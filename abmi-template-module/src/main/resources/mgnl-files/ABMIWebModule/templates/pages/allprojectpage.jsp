<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
  
  <body class="container-fluid">
   <%@ include file="../includes/menu.jsp"%>
	
	<div class="row">
		<cms:area name="jumbotron"/>
	</div>
	<div class="row breadcrumb" >
		<div class="container">
			<div class="col-lg-12">
				<h3><cms:area name="titleArea" /></h3>
			</div>
		</div>
	
	</div>
	<div class="row breadcrumb-triangle" >
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<div class="col-lg-9 main">
			<cms:area name="topLeftColumnArea" />
			<cms:area name="projectBoxArea" />
			
			<cms:area name="bottomLeftColumnArea" />
			</div>
			
			<div class="col-lg-3 col-md-12 col-sm-12 aside">
				
					<cms:area name="rightColumnArea" />
				
			
				
					<cms:area name="rightWidgetColumnArea" />	
				
			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
    <script>
    	$(document).ready(function(){
	    	var arrowwidth = $('.postmeta').width() + 40;
	    	$('.postmeta.arrow').css('border-width','10px '+arrowwidth+'px 0 0');
    	});
    </script>
  </body>
</html>