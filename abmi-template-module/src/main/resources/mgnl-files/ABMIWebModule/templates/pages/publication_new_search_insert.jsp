<div class="searchbysec" id="publication_search">
<h3>Browse our Library</h3>
<p><i>Browse our publication library below, or search to the right.</i>
<div class="search-group" id="type_selection">
	<div class="searchBy">
		<h6 class="title">Filter by Document Type</h6>
		<div class="data-selected"></div>
	</div>
	<div class="searchdata" style="display: none;">
		<div class="breadcrumb-block">
			<ol class="breadcrumb listnav-breadcrumb">
			  <li><a href="javascript:void(0)" class="backTomain">Back</a></li>
			  
			</ol>
		</div>
		<ul id="publication_type" class="group-list" style="display: block;">
			<li><a href="javascript:void(0)" class="selectall">All</a></li>
		
			
		</ul>
		
		

	</div>
	
</div>
<div class="search-group" id="subject_selection">
	<div class="searchBy">
		<h6 class="title">Filter by Subject Area</h6>
		<div class="data-selected"></div>
	</div>
	<div class="searchdata" style="display: none;">
		<div class="breadcrumb-block">
			<ol class="breadcrumb listnav-breadcrumb">
			  <li><a href="javascript:void(0)" class="backTomain">Back</a></li>
			  
			</ol>
		</div>
		<ul id="publication_subject" class="group-list" style="display: block;">
			<li><a href="javascript:void(0)" class="selectall">All</a></li>
			
		</ul>
		
		

	</div>
	
</div>
<div class="search-group" id="time_selection">
	<div class="searchBy">
		<h6 class="title">Filter by Year</h6>
		<div class="data-selected"></div>
	</div>
	<div class="searchdata" style="display: none;">
		<div class="breadcrumb-block">
			<ol class="breadcrumb listnav-breadcrumb">
			  <li><a href="javascript:void(0)" class="backTomain">Back</a></li>
			  
			</ol>
		</div>
		<ul id="publication_time" class="group-list" style="display: block;">
			<li><a href="javascript:void(0)" class="selectall">All</a></li>
			
			
		</ul>
		
		

	</div>
	
</div>

	<c:if test="${not empty param.keyword}">
			<div id="tagDiv"><p>Tag:<span id="tagSearch">${param.keyword}</span>  <a id="cleartaglink" href="javascript:void(0);"><img src="/docroot/assets/close-btn.png"></a></div>
	</c:if>
				
<input class="submit btn btn-default" type="submit" id="publication_search_btn" value="GET RESULTS">
<input class="submit btn btn-default" type="reset" id="publication_clear_btn" value="RESET SEARCH">					
</div>	

<div style="padding:20px 0;">
<p>Sort By:&nbsp;&nbsp;<select id="sortPublicaiton">
<option value="date">Date</option>
<option value="hit">Relevance</option>
</select>
</p></div>	
<div></div>