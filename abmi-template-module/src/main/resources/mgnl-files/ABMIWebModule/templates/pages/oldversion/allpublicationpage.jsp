<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>

<body class="container-fluid publications">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<!--  ===================  FLOATING NAV List view detail view Search button =============== -->
			<div class="side-top-nav col-xs-12 col-lg-3">
				<div class="widget listSyle">
					<c:if test="${mode eq 'detail'}">
						<a class="sidemenuFullPostBtn active" href="#">&nbsp;</a>
						<a class="sidemenuListPostBtn"
							href="${state.originalBrowserURI}?mode=list
										<c:if test='${not empty param.documenttype}'>&documenttype=${param.documenttype}</c:if>
										<c:if test='${not empty param.subject}'>&subject=${param.subject}</c:if>
										<c:if test='${not empty param.time}'>&time=${param.time}</c:if>
										<c:if test='${not empty param.keyword}'>&keyword=${param.keyword}</c:if>"scroll">&nbsp;</a>

					</c:if>

					<c:if test="${mode eq 'list'}">
						<a class="sidemenuFullPostBtn"
							href="${state.originalBrowserURI}?mode=detail&scroll
										<c:if test='${not empty param.documenttype}'>&documenttype=${param.documenttype}</c:if>
										<c:if test='${not empty param.subject}'>&subject=${param.subject}</c:if>
										<c:if test='${not empty param.time}'>&time=${param.time}</c:if>
										<c:if test='${not empty param.keyword}'>&keyword=${param.keyword}</c:if>"">&nbsp;</a>
						<a class="sidemenuListPostBtn active" href="#">&nbsp;</a>

					</c:if>
				</div>
				<div class="widget searchbox">

					<input type="text" id="q2" name="q" class="sidemenuSearch"
						placeholder="Search all ${searchFolder}">
					<button onclick="javascript:openSearch('Publications');"
						class="sidemenuSearchBtn"></button>
				</div>
			</div>
			<div class="clear-to-lg"></div>
			<!--  ================================== -->

			<!--  ===================  MAIN COLUMN =============== -->

			<!--  ===================  MAIN COLUMN =============== -->
			<div class="col-lg-9 main">
				<cms:area name="topLeftColumnArea" />

				<div class="row post-${mode}">
					<c:if test="${empty publicationList }">
						<p>No result matches your query.</p>
					</c:if>

					<c:set var="defaultHeaderImage"
						value="${cmsfn:content(content.placeholderimg,'dam')}" />


					<c:forEach items="${publicationList}" var="content"
						varStatus="rowStatus">
						<c:if test="${mode ne 'list'}">
							<c:set var="topImage"
								value="${cmsfn:content(content.imageUrl,'dam')}" />

							<div class="post">
								<div
									class="postheader ${empty topImage and empty defaultHeaderImage?"no-image":""}">
									<a href="${content.path}.html"> <c:if
											test="${not empty content.coverImageUrl }">
											<c:set var="coverImage"
												value="${cmsfn:content(content.coverImageUrl,'dam')}" />

											<div class="post-feature-overlay hidden-xs hidden-sm">
												<img alt=""
													src="${cmsfn:link(cmsfn:asContentMap(coverImage))}">
											</div>
										</c:if> <c:if
											test="${not empty topImage or not empty defaultHeaderImage}">
											<img alt="top image"
												src="${cmsfn:link(cmsfn:asContentMap(not empty topImage?topImage:defaultHeaderImage))}">
										</c:if>
										<div class="green_title_bar">
											<div class="title_line">${empty content.doctitle? content.imagetitle:content.doctitle}</div>
										</div>

									</a>
								</div>
								<div class="arrowcontainer">
									<div class="postmeta">
										<p>
											<c:if test="${not empty content.author}"> ${content.author} <br>
											</c:if>
											<c:if test="${not empty content.displaydate}">  ${content.displaydate}</c:if>
										</p>
									</div>

								</div>
								<div class="postactions">
									<p>
										<c:if test="${not empty content.viewlink}">
											<c:set var="viewLink"
												value="${cmsfn:content(content.viewlink,'dam')}" />

											<a class="pdf"
												href="${cmsfn:link(cmsfn:asContentMap(viewLink))}">VIEW
												PDF</a>
										</c:if>

									</p>

									<div
										class="post-box-share addthis_sharing_toolbox st_sharethis_custom"
										data-url="http://${pageContext.request.serverName}${pageContext.request.contextPath}${content.path}.html"></div>
								</div>
								<div class="postexerpt">

									<p>${content.description}</p>
									<c:if test="${not empty content.documenttype }">

										<p class="documenttype">
											DOCUMENT TYPE:
											<c:forEach items="${content.documenttype}" var="row"
												varStatus="tagStatus">
												<c:if test="${row ne param.documenttype}">
													<a
														href="${state.originalBrowserURI}?mode=${mode}&documenttype=${row}">${row}</a>
												</c:if>
												<c:if test="${row eq param.documenttype}">
													<span class="queryparam">${row}</span>
												</c:if>
												<c:if test="${not tagStatus.last }">,</c:if>
											</c:forEach>
										</p>
									</c:if>

									<c:if test="${not empty content.docsubject  }">
										<p class="subjectarea">
											SUBJECT AREA:
											<c:forEach items="${content.docsubject}" var="row"
												varStatus="tagStatus">
												<c:if test="${row ne param.subject}">
													<a
														href="${state.originalBrowserURI}?mode=${mode}&subject=${row}">${row}</a>
												</c:if>
												<c:if test="${row eq param.subject}">
													<span class="queryparam">${row}</span>
												</c:if>
												<c:if test="${not tagStatus.last }">,</c:if>
											</c:forEach>
										</p>
									</c:if>

									<c:if test="${not empty content.dockeyword  }">
										<p class="subjectarea">
											KEYWORDS:
											<c:forEach items="${content.dockeyword}" var="row"
												varStatus="tagStatus">
												<c:if test="${row ne param.keyword}">
													<a
														href="${state.originalBrowserURI}?mode=${mode}&keyword=${row}">${row}</a>
												</c:if>
												<c:if test="${row eq param.keyword}">
													<span class="queryparam">${row}</span>
												</c:if>
												<c:if test="${not tagStatus.last }">,</c:if>
											</c:forEach>
										</p>
									</c:if>
									<br>
									<c:if test="${hasMultiversion}">
										<a href="${content.path}.html"><h5>CONTINUE READING</h5></a>
									</c:if>
								</div>


								<c:if test="${content.suppdocs.size()>0}">
									<ul class="faq supplementary">
										<li><input type="button" class=""
											value="Supplementary Reports"
											data-target="#supplementpost1_${rowStatus.index}"
											data-toggle="collapse">
											<div class="collapse in"
												id="supplementpost1_${rowStatus.index}"
												style="height: auto;">
												<div class="row">

													<c:forEach items="${content.suppdocs}" var="suppdoc">
														<c:set var="supViewLink"
															value="${cmsfn:content(suppdoc[1],'dam')}" />
														<div class="col-xs-12">
															<h3>${suppdoc[0]}</h3>
															<div class="postactions">
																<p>
																	<a class="pdf"
																		href="${cmsfn:link(cmsfn:asContentMap(supViewLink))}">VIEW
																		PDF</a>
																</p>
																<div class="post-box-share addthis_sharing_toolbox"
																	data-url="http://${pageContext.request.serverName}${cmsfn:link(cmsfn:asContentMap(supViewLink))}"></div>
															</div>
														</div>


													</c:forEach>
												</div>
											</div></li>
									</ul>
								</c:if>

								<c:if test="${content.suppdocslink.size()>0}">
									<ul class="faq supplementary">
										<li><input type="button" class=""
											value="Supplementary Reports"
											data-target="#supplementpost1_${rowStatus.index}"
											data-toggle="collapse">
											<div class="collapse in"
												id="supplementpost1_${rowStatus.index}"
												style="height: auto;">
												<div class="row">

													<c:forEach items="${content.suppdocslink}" var="suppdoc">
														<c:set var="supViewLink"
															value="${cmsfn:content(suppdoc.viewlink,'dam')}" />
														<!-- 	<c:set var="supDownloadLink"
														value="${cmsfn:content(suppdoc[2],'dam')}" /> -->

														<div class="col-xs-12">
															<h3>${suppdoc.title}</h3>
															<div class="postactions">
																<p>
																	<a class="pdf"
																		href="${cmsfn:link(cmsfn:asContentMap(supViewLink))}">VIEW
																		PDF</a>
																</p>
																<div class="post-box-share addthis_sharing_toolbox"
																	data-url="http://${pageContext.request.serverName}${cmsfn:link(cmsfn:asContentMap(supViewLink))}"></div>
															</div>
														</div>


													</c:forEach>
												</div>
											</div></li>
									</ul>

								</c:if>

							</div>
						</c:if>

						<c:if test="${mode eq 'list'}">
							<div class="col-xs-12">
								<h4>${content.doctitle}</h4>
								<p>

									<a class="postLink" href="${content.path}.html">READ
										PUBLICATION</a>
									<c:if test="${not empty content.viewlink}">
										<c:set var="viewlink"
											value="${cmsfn:content(content.viewlink,'dam')}" />

										<a download class="pdf"
											href="${cmsfn:link(cmsfn:asContentMap(viewlink))}">VIEW
											PDF</a>
									</c:if>
								</p>
							</div>

						</c:if>

					</c:forEach>
					<!--   -->
					<div class="pagination">
						<c:if test="${mode eq 'detail' and totalPage > 0 }">
							<p class="next">
								<c:if test="${page > 1}">
									<span><a
										href="${state.originalBrowserURI}?&mode=${mode}&page=${page-1}
										<c:if test='${not empty param.documenttype}'>&documenttype=${param.documenttype}</c:if>
										<c:if test='${not empty param.subject}'>&subject=${param.subject}</c:if>
										<c:if test='${not empty param.time}'>&time=${param.time}</c:if>
										<c:if test='${not empty param.keyword}'>&keyword=${param.keyword}</c:if>"><img
											alt="previous page" src="/docroot/assets/prevarrow2.png"></a>

									</span>
								</c:if>

								Page ${page} of ${totalPage}
								<c:if test="${page < totalPage}">
									<span><a
										href="${state.originalBrowserURI}?&mode=${mode}&page=${page+1}
										<c:if test='${not empty param.documenttype}'>&documenttype=${param.documenttype}</c:if>
										<c:if test='${not empty param.subject}'>&subject=${param.subject}</c:if>
											<c:if test='${not empty param.time}'>&time=${param.time}</c:if>
												
										<c:if test='${not empty param.keyword}'>&keyword=${param.keyword}</c:if>">
											<img alt="next page" src="/docroot/assets/nextarrow.png">
									</a></span>
								</c:if>
							</p>
						</c:if>
						&nbsp;
					</div>

					<cms:area name="bottomLeftColumnArea" />
				</div>
			</div>

			<div class="col-lg-3 col-md-12 col-sm-12 aside">


				<cms:area name="rightColumnArea" />

				<cms:area name="rightWidgetColumnArea" />
			</div>

		</div>
	</div>

	<%@ include file="../includes/footer.jsp"%>
	<div id="publicationgoogle">
		<div id='csepublication'
			style='z-index: 1000; width: 100%; display: none;'>Loading</div>

		<script src='http://www.google.com/jsapi' type='text/javascript'></script>
		<script type='text/javascript'>
			google.load('search', '1', {
				language : 'en',
				style : google.loader.themes.MINIMALIST
			});
			google
					.setOnLoadCallback(
							function() {
								var customSearchOptions = {};
								// customSearchOptions['overlayResults'] = true;
								var customSearchControl = new google.search.CustomSearchControl(
										'006207892962066587082:dr2mt_fuqly',
										customSearchOptions);
								customSearchControl
										.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
								customSearchControl
										.setLinkTarget(GSearch.LINK_TARGET_PARENT);
								var options = new google.search.DrawOptions();
								customSearchControl.draw('csepublication',
										options);
							}, true);
		</script>

	</div>
	<script type="text/javascript"
		src="http://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">
		stLight.options({
			publisher : '12345',
		});
	</script>

	<style>
.st_sharethis_custom {
	background: url("http://path/to/image/file") no-repeat scroll left top
		transparent;
	padding: 0px 16px 0 0;
}
</style>
</body>
</html>