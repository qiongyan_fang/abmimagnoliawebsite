
	<script src="/docroot/js/dhtmlxscheduler/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="/docroot/css/dhtmlxscheduler_glossy.css" type="text/css" media="screen" title="no title"
	      charset="utf-8">
	<script src="/docroot/js/dhtmlxscheduler/ext/dhtmlxscheduler_readonly.js" type="text/javascript" charset="utf-8"></script>
	<script src="/docroot/js/dhtmlxscheduler/ext/dhtmlxscheduler_container_autoresize.js" type="text/javascript" charset="utf-8"></script>

	

	<script type="text/javascript" charset="utf-8">
		function initcalendar() {
			scheduler.config.multi_day = true;
			scheduler.xy.margin_top = 80;
			scheduler.config.details_on_dblclick = true;
			scheduler.config.container_autoresize = false;

			scheduler.config.xml_date = "%Y-%m-%d %H:%i";
			scheduler.config.month_day_min_height = 150;
			scheduler.config.hour_size_px = 80;

			scheduler.config.left_border = true;
		
			scheduler.config.limit_time_select = true;
			scheduler.config.first_hour = 7;
			scheduler.config.last_hour = 18;
			scheduler.config.show_loading = true
			scheduler.config.lightbox.sections=[    
			                                    { name:"description", height:50, type:"textarea", map_to:"text", focus:true},
			                                    { name:"location",    height:43, type:"textarea", map_to:"event_location"},
			                                    { name:"time",        height:72, type:"time",     map_to:"auto"}    
			                                ];


			scheduler.skin = "glossy";
			scheduler.init('scheduler_here', new Date(), "month");
			scheduler.parse(events, "json");



function block_readonly(id){
			//if (!id) return true;
			return !this.getEvent(id).readonly;
		}
		scheduler.attachEvent("onBeforeDrag",block_readonly)
		scheduler.attachEvent("onClick",block_readonly)




		}
		
	</script>


<div style="row">
	<div id="scheduler_here" class="dhx_cal_container" style='width:90%;min-height: 400px;'>
		<div class="dhx_cal_navline">
			<div class="dhx_cal_prev_button">&nbsp;</div>
			<div class="dhx_cal_next_button">&nbsp;</div>
			<div class="dhx_cal_today_button"></div>
			<div class="dhx_cal_date"></div>
			<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
			<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
			<div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
		</div>
		<div class="dhx_cal_header">
		</div>
		<div class="dhx_cal_data">
		</div>
	</div>
</div>
