<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>

<link rel="stylesheet" href="/docroot/css/rawdata_nav.css">



<body class="container-fluid rawdata">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row page-content">
		<div class="container">
			<!--  ===================  MAIN COLUMN =============== -->

			<div class="row" id="rawdata-container">
				<div id="data-input-container" class="col-sm-3 raw-sidenav">
				<!-- 
					<div class="searchbox">
						<form action="" class="search">
							<input placeholder="Search" type="search">
							<button type="submit"></button>
						</form>
					</div>
 			-->
					<cms:area name="leftColumnArea" />
					


				</div>
				
				<div class="col-sm-9 white-page" id="rawdata-content">
						<cms:area name="rightColumnArea" />
					</div>
				<!-- END row #rawdata-container -->

			</div>
		</div>
		<!-- END container -->
	</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>

</body>
</html>