
<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<body class="container-fluid events-page all-events preload">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
		
		<!--  ===================  FLOATING NAV List view detail view Search button =============== -->
			<div class="side-top-nav col-xs-12 col-lg-3">
				<div class="widget listSyle">
					<c:if test="${mode eq 'detail'}">
						<a class="sidemenuFullPostBtn active" href="#">&nbsp;</a>
						<a class="sidemenuListPostBtn"
							href="${state.originalBrowserURI}?mode=list&scroll">&nbsp;</a>

					<a class="sidemenuEventPostBtn"
							href="${state.originalBrowserURI}?mode=calendar&scroll">&nbsp;</a>
					</c:if>

					<c:if test="${mode eq 'list'}">
						<a class="sidemenuFullPostBtn"
							href="${state.originalBrowserURI}?mode=detail&scroll">&nbsp;</a>
						<a class="sidemenuListPostBtn active"
							href="${state.originalBrowserURI}?mode=list&scroll">&nbsp;</a>
							<a class="sidemenuEventPostBtn"
							href="${state.originalBrowserURI}?mode=calendar&scroll">&nbsp;</a>

					</c:if>
					
						<c:if test="${mode eq 'calendar'}">
						<a class="sidemenuFullPostBtn"
							href="${state.originalBrowserURI}?mode=detail&scroll">&nbsp;</a>
						<a class="sidemenuListPostBtn "
							href="${state.originalBrowserURI}?mode=list&scroll">&nbsp;</a>
							<a class="sidemenuEventPostBtn active"
							href="${state.originalBrowserURI}?mode=calendar&scroll">&nbsp;</a>

					</c:if>
				</div>
				<div class="widget searchbox">
					
					<input type="text" id="q2" name="q" class="sidemenuSearch"
						placeholder="Search all ${searchFolder}"><button onclick="javascript:openSearch('Publications');"
						class="sidemenuSearchBtn"></button>
				</div>
				</div>
			<div class="clear-to-lg"></div>		
			<!--  ================================== -->
			
			<!--  ===================  MAIN COLUMN =============== -->
			<div class="col-lg-9 main">
				<cms:area name="topLeftColumnArea" />

				<div class="row post-${mode}">

	<c:if test="${mode eq 'calendar'}">
	<%@ include file="eventcalendar.jsp"%>
<script>
var events = new Array();
</script>



	</c:if>
					<c:forEach items="${publicationList}" var="content"
						varStatus="rowStatus">



						<c:if test="${mode eq 'detail'}">
							<c:set var="topImage"
								value="${cmsfn:content(content.imageUrl,'dam')}" />

							<div class="post">
								<div class="postheader ${empty topImage?"no-image":""}">


									<c:if test="${not empty topImage }">
										<img alt="banner image"
											src="${cmsfn:link(cmsfn:asContentMap

(topImage))}">
									</c:if>

									<h3>${content.imagetitle}</h3>
								</div>
								<div class="arrowcontainer">
									<div class="postmeta">
<c:set var="startDate"><fmt:formatDate value='${content.eventstartdate.getTime()}'/></c:set>
<c:set var="endDate"><fmt:formatDate value="${content.eventenddate.getTime() }"/></c:set>
<p>

											${startDate}
											<c:if test="${startDate ne endDate}">
											- ${endDate}
											
											</c:if>

</p>
									</div>
								
									</div>
								

							

								<div class="postactions">


									<div class="post-box-share addthis_sharing_toolbox"
										data-url="http://${pageContext.request.serverName}${pageContext.request.contextPath}${content.path}.html"></div>
								</div>
								<div class="postexerpt">
									<h4>${content.doctitle}</h4>
									<p>${content.description}</p>
									<h5>
										<b>Time: </b>${content.eventstarttime} - ${content.eventendtime}
									</h5>
									<h5>
										<b>Location: </b>${content.eventlocation}
									</h5>

									<a href="${pageContext.request.contextPath}${content.path}.html"><h5>CONTINUE
										READING</h5></a>
								</div>


							</div>
						</c:if>

						<c:if test="${mode eq 'list'}">
							<div class="col-xs-12">
								<h4>${content.doctitle}</h4>
								<p>
																	<a class="postLink" href="${content.path}.html">READ EVENTS</a>

								</p>
							</div>

						</c:if>
							<c:if test="${mode eq 'calendar'}">
						<script>
						events[${rowStatus.index}] = {id:${rowStatus.index+1}, text:"${fn:escapeXml(content.doctitle)} <br><a href='${pageContext.request.contextPath}${content.path}.html'>Details</a>",  	readonly:true, start_date:"<fmt:formatDate pattern='yyyy-MM-dd'  value='${content.eventstartdate.getTime()}' /> ${content.eventstarttime}",
												end_date:"<fmt:formatDate pattern='yyyy-MM-dd'  value='${content.eventenddate.getTime()}' /> ${content.eventendtime}"};
						</script>	
					</c:if>

					</c:forEach>
					<div class="pagination">
						<c:if test="${mode ne 'calendar'}">
							<p class="next">
								<c:if test="${page > 1}">
									<span><a
										href="${state.originalBrowserURI}?page=${page-1}"><img
											alt="previous page" src="/docroot/assets/prevarrow2.png"></span>
									</a>
									</span>
								</c:if>

								Page ${page} of ${totalPage}
								<c:if test="${page < totalPage}">
									<span><a
										href="${state.originalBrowserURI}?page=${page+1}"><img
											alt="next page" src="/docroot/assets/nextarrow.png"></span>
									</a>
								</c:if>
							</p>
						</c:if>
					</div>

					<cms:area name="bottomLeftColumnArea" />
				</div>
			</div>

			<div class="col-lg-3 col-md-12 col-sm-12 aside">
				
				<cms:area name="rightColumnArea" />
				
				<cms:area name="rightWidgetColumnArea" />

			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>


	

	<div id="publicationgoogle">
		<div id='csepublication'
			style='z-index: 1000; width: 100%; display: none;'>Loading</div>

		<script src='http://www.google.com/jsapi' type='text/javascript'></script>
		<script type='text/javascript'>
			google.load('search', '1', {
				language : 'en',
				style : google.loader.themes.MINIMALIST
			});
			google
					.setOnLoadCallback(
							function() {
								var customSearchOptions = {};
								// customSearchOptions['overlayResults'] = true;
								var customSearchControl = new google.search.CustomSearchControl(
										'006207892962066587082:dr2mt_fuqly',
										customSearchOptions);
								customSearchControl
										.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
								customSearchControl
										.setLinkTarget(GSearch.LINK_TARGET_PARENT);
								var options = new google.search.DrawOptions();
								customSearchControl.draw('csepublication',
										options);
							}, true);
		</script>

	</div>

</body>
</html>