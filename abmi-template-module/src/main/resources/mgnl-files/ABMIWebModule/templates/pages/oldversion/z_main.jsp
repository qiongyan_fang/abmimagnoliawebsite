<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
     
  <body class="container-fluid home">
	<%@ include file="../includes/menu.jsp"%>
	<div class="row">
	<cms:area name="jumbotron"/>
	</div>
	<div class="row company-info">
		<div class="container">
			<h2 class="col-sm-12 text-center">The ABMI is a Leader in Biodiversity Monitoring</h2>
			<p class="col-sm-12 col-lg-10 col-lg-offset-1 text-center col-lg-ex-pad">We track changes in Alberta's wildlife and their habitats from border to border, and provide ongoing, relevant, scientifically credible information on Alberta's living resources. For our province's land use decision-makers.<br />For Alberta's future land stewards. For Albertans.</p>
			<h3 class="col-sm-12 text-center">Why We Monitor Biodiversity</h3>
			<div class="col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 btn-grouping">
			<cms:area name="greenButtonArea"/>				
			</div>
		</div>
	</div>
	<div class="row divider">
		<h2 class="text-center">Explore The ABMI</h2>
	</div>
	<div class="row explore">
	<div class="triangle hidden-xs hidden-sm"></div>
		<div class="container">
		  <cms:area name="rowArea" />
	
		</div>
	</div>
	<div class="row company-info">
		<div class="container">
			<div class="row">
				<h2 class="col-lg-12 text-center partners">Our Partners</h2>
				<p class="col-lg-12 text-center">Without the support of these wonderful people, the work we do would simply not be possible. Thank you!</p>
				<cms:area name="partnerIconArea" />
				
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-6">
					<h3 class="text-center">ABMI Sponsors</h3>
					<div class="row withmarg">
					<cms:area name="leftSponsorIconArea"/>
					</div>
				</div>
				<div class="col-md-12 col-lg-6">
					<h3 class="text-center">Project Sponsors</h3>
					<div class="row withmarg">
					<cms:area name="rightSponsorIconArea"/>
					</div>
				</div>
				<div class="center">
					<cms:area name="bottomLearnMoreButtonArea"/>
					
				</div>
				
			</div>
			
		</div>
	</div>
<%@ include file="../includes/footer.jsp"%>
 

  </body>
</html>