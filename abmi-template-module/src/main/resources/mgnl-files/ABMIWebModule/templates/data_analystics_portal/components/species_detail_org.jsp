<%@ include file="../../includes/taglibs.jsp"%>

<script>
	var gScientificName = "${param.sname}";
	var gSpeciesGroupId = "${param.groupId}";
	var gTsn = "${param.tsn}";
</script>

<!--  detailed species page header -->

<div id="species-profile-details" class="row sp-main sp-main-top">
	<div class="cell-img species-banner-height s-profilepic">
		<img class="mainBanner" src="" alt="" />
	</div>
	<div class="cell-img map-portal-sec species-banner-height formobile">
		<div class="btn-pos">
			<a  class="mapPortal-btn">View Map Portal</a>
		</div>
		<img class="distribution-map"
			src="/FileDownloadServlet?dir=WEB_GRAPH&filename=/distributionMaps/${param.groupId}_${param.tsn}.png"
			alt="Alberta Map" />
	</div>

	<div class="banner-txt species-banner-height fordesktop watermark-${param.groupId}" id="spp-top">
		<h2 class="auto CommonName"></h2>
		<h3 class="auto ScientificName"></h3>
		<div class="species-desc">
			<div class="auto WebMainDescription"></div>
		</div>
		<div class="species-details">
			
		</div>

		<a  class="download-report">Download Report</a>
	</div>
	<div class="cell-img map-portal-sec species-banner-height fordesktop">
		<div class="btn-pos">
			<a  class="mapPortal-btn">View Map Portal</a>
		</div>
		<img class="distribution-map"
			src="/FileDownloadServlet?dir=WEB_GRAPH&filename=/distributionMaps/${param.groupId}_${param.tsn}.png"
			alt="Alberta Map" />

	</div>

</div>



</div>
<div class="row subNav">
	<div class="col-lg-12 species-titlebar">
		<div class="pageTitle">
			<div class="titleWrap">
				<div class="pagetitleimg">
					<img class="miniCircle" alt="" />
				</div>
				<span class="auto CommonName"></span>
			</div>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#subnavbar"
				aria-expanded="false" aria-controls="navbar">
				<span class="nav-trigger"><span></span></span> <span class="sr-only">Select</span>
			</button>
		</div>
	</div>
	<div class="col-col-lg-12">
		<nav class="navbar">
			<div class="pagetitleimg desktoppic">
				<img class="miniCircle" alt="">
			</div>
			<div id="subnavbar" class="collapse navbar-collapse sp-menu">
				<ul class="nav navbar-nav">
					<li><a href="#sec1">Introduction</a></li>
					<li><a href="#sec2">Habitat & Human <br />Footprint
							Associations
					</a></li>
					<li><a href="#sec3">Impacts of Human <br />Footprint
					</a></li>
					<li><a href="#sec4">Predicted Relative <br />Abundance
					</a></li>
					<li><a href="#sec5">Impacts of <br />Climate Change
					</a></li>
					<li><a href="#sec6">Other Issues</a></li>
					<li><a href="#sec7">References</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->

		</nav>
	</div>
</div>
<div class="row sec-title" id="sec1">
	<div class="container">
		<h2>Introduction</h2>
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-lg-12">
				<cms:area name="introduction" />
			</div>
		</div>
	</div>
</div>
<div class="row sec-title" id="sec2">
	<div class="container">
		<h2 id="header20">Habitat & Human Footprint Associations</h2>
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-lg-12">
				<div class="auto WebHabitat"></div>
					<cms:area name="method_hhfa" />
					
				
				<div class="Forested-Region">
					<div class="head-s">
						<img src="/docroot/da/assets/Forested-Region.png" alt="" />
						<h3>Species-habitat Associations in the Forested Region</h3>
					</div>
				</div>
				<div class="mapImgSec ForestedRegion-map">
					<a href="javascript:void(0)" class="zoom-graph"
						data-open-id="graph"></a>
					<div class="album">
						<a rel="graph" class="facybox species_graph_large" href=""><img
							class="species_graph_large" src="" alt=""></a>
					</div>
					<img id="species_graph" src="" alt="" class="auto mapImg">
				</div>
				<br /> <br />
				<div class="auto WebForestedRegionGraphText"></div>


			</div>

		</div>
	</div>
</div>
<div class="row sec paririe-sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-lg-12">
				<div class="Prairie-Region">
					<div class="head-s">
						<img src="/docroot/da/assets/Prairie-Region.png" alt="" />
						<h3>Species-habitat Associations in the Prairie Region</h3>
					</div>
				</div>
					<cms:area name="method_graph1" />
			</div>
			<div class="col-md-6">
				<h4>Non-Treed Sites in the Prairie Region</h4>
				<img class="auto" id="non_treed_graph" src=""
					alt="Non-Treed Sites in the Prairie Region" />
			</div>
			<div class="col-md-6">
				<h4>Treed Sites in the Prairie Region</h4>
				<img class="auto" src="" id="treed_graph"
					alt="Treed Sites in the Prairie Region" />
			</div>
			<div class="col-lg-12">
				<div class="auto WebNonTreeGraphText"></div>
				<div class="auto WebTreeGraphText"></div>
			</div>

		</div>
	</div>
</div>
<div class="row sec linear-fp-sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-md-12">
				<h3>Relationship to Linear Footprint</h3>
				<br/>
				<cms:area name="method_graph2" />
			</div>
			<div class="col-md-6">
				<div class="Forested-Region">
					<div class="head-s">
						<img src="/docroot/da/assets/Forested-Region.png" alt="" />
						<h4>Relationship to Linear Footprint in the Forest Region</h4>
					</div>
				</div>

				<img id="linear_north_graph" class="auto" src="" alt="" /><br /> <br />
			</div>
			<div class="col-md-6">
				<div class="Prairie-Region">
					<div class="head-s">
						<img src="/docroot/da/assets/Prairie-Region.png" alt="" />
						<h4>Relationship to Linear Footprint in the Prairie Region</h4>
					</div>
				</div>
				<img id="linear_south_graph" class="auto" src="" alt="" /><br /> <br />
			</div>
			<div class="col-lg-12">


				<div class="auto WebFLinearHFGraphText"></div>
				<div class="auto WebPLinearHFGraphText"></div>
				<div class="btnr clearboth">
	 				
		    		<div class="select-download">
			    		<div class="select-list " style="display: none;">
				    		<div class="select-from">
										<ul class="tree">
											<li>
												<input id="Species-Habitat-Associations" type="checkbox" name="check" value="Species Habitat Associations in Prarie Region">
												<label for="Species-Habitat-Associations">Species Habitat Associations in Prairie Region</label>
											</li>
											<li>
												<input id="Non-Tree-sites" type="checkbox" name="check" value="Non-Tree Sites in the Prairie Region">
												<label for="Non-Tree-sites">Non-Tree Sites in the Prairie Region</label>
											</li>
											<li>
												<input id="treed-sites" type="checkbox" name="check" value="Treed Sites in Prairie Region">
												<label for="treed-sites">Treed Sites in Prairie Region</label>
											</li>
										</ul>
										
									</div>
				    	</div>
			    		<span class="selectionLink"></span><a  class="downloaddata-btn">Download Data</a></div>
	    		</div>
			</div>

		</div>
	</div>
</div>
<div class="row sec-title" id="sec3">
	<div class="container">
		<h2>Impacts of Human Footprint</h2>
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-md-12">
				<div class="auto WebHFEffect"></div>
				<cms:area name="method_ihf" />

			</div>
			<div class="col-md-6 Forested-Region">
				<div class="head-s">
					<img src="/docroot/da/assets/Forested-Region.png" alt="" />
					<h4>Human Footprint Effects in the Forested Region</h4>
				</div>
				<img class="auto" src="" id="sector_effect_north_graph"
					alt="Human Footprint Effects in the Forested Region" /><br />
				<div class="auto WebHFForestRegionGraph"></div>
			</div>
			<div class="col-md-6 Prairie-Region">
				<div class="head-s">
					<img src="/docroot/da/assets/Prairie-Region.png" alt="" />
					<h4>Human Footprint Effects in the Prairie Region</h4>
				</div>
				<img class="auto" id="sector_effect_south_graph"
					alt="Human Footprint Effects in the Prairie Region" /><br />
				<div class="auto WebPrarieRegionGraphText"></div>
			</div>
			<div class="btnr">
				<a  class="downloaddata-btn">Download Data</a>
			</div>

		</div>
	</div>
</div>
<!--  end -->
<div class="row sec-title" id="sec4">
	<div class="container">
		<h2>Predicted Relative Abundance</h2>
	</div>
</div>

<div class="row sec">
	<div class="container">
		<div class="wrap">
			<div class="auto WebRange"></div>
			
					<cms:area name="method_pra" />
			<div class="row relation_sec">
				<div class="col-md-4">
					<h4>Reference Conditions</h4>
					<div class="mapImgSec map-ref">
						<a href="javascript:void(0)" class="open-modalwindow"
							data-open-id="map-2"></a>
						<div class="album">
							<a rel="map-2" class="facybox refe-map" href=""><img
								class="refe-map" src="" alt="" /></a>
						</div>
						
						<img src="" alt="" class="mapImg refe-map" />
						
					</div>
		<div class="btnr-map">
				<a  class="downloaddata-btn refe-map" download>Download Map Image</a>
				</div>
					<ul>
						<li>The reference condition shows the predicted relative
							abundance of the <span class="CommonName">this species</span>
							after all human footprint had been backfilled based on native
							vegetation in the surrounding area.
						</li>
					</ul>
					
				</div>
				<div class="col-md-4">
					<h4>Current Conditions</h4>
					<div class="mapImgSec map-current">
						<a href="javascript:void(0)" class="open-modalwindow"
							data-open-id="map-3"></a>
						<div class="album">
							<a rel="map-3" class="facybox curr-map" href=""><img
								class="curr-map" alt="" /></a>
						</div>
					
						<img alt="" class="mapImg curr-map" />
						
					</div>
	<div class="btnr-map">
				<a  class="downloaddata-btn curr-map" download>Download Map Image</a>
				</div>
					<ul>
						<li>The current condition is the predicted relative abundance
							of the <span class="CommonName">this species</span> taking
							current human footprint (circa 2012) into account.
						</li>
					</ul>
			
				</div>
				<div class="col-md-4">
					<h4 class="colored">Difference Conditions</h4>
					<div class="mapImgSec">
						<a href="javascript:void(0)" class="open-modalwindow"
							data-open-id="map-4"></a>
						<div class="album">
							<a rel="map-4" class="facybox diff-map" href=""><img
								class="diff-map" src="" alt="" /></a>
						</div>
						
						<img src="/docroot/da/assets/Difference-map.png" alt=""
							class="mapImg diff-map" />
						
					</div>
		<div class="btnr-map">
				<a  class="downloaddata-btn diff-map" download>Download Map Image</a>
				</div>
					<div class="auto WebDifferenceMapText"></div>
				
				</div>
			</div>
			<div class="btnr">
				<a  class="downloaddata-btn" id="raset-download-btn" download>Download Raster Data</a>
			</div>

		</div>
	</div>
</div>
<div class="row sec-title" id="sec5">
	<div class="container">
		<h2>Impacts of Climate Change</h2>
		<cms:area name="method_icc" />
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-md-12">
				<h4 class="nomrg">Results Coming Soon</h4>
			</div>
		</div>
	</div>
</div>
<div class="row sec-title" id="sec6">
	<div class="container">
		<h2>Other Issues</h2>
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-md-12">
				<h4 class="nomrg">Not Available</h4>
			</div>
		</div>
	</div>
</div>
<div class="row sec-title" id="sec7">
	<div class="container">
		<h2>References</h2>
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-md-6">
				<h3>References</h3>
				<div class="auto  WebReference"></div>
				<h3>Photo & Credits</h3>
				<p>Photos: TBD</p>
			</div>
			<div class="col-md-6">
				<h3>Data Sources</h3>
				<div id="data-source">
				<p>Information from ABMI bird point counts was combined with
					information from other organizations and individuals:</p>
				<ul>
					<li>Environment Canada (North American Breeding Bird Survey
						and Joint Oil Sands Monitoring programs)</li>
					<li>Ecological Monitoring Committee for the Lower Athabasca
						(EMCLA)</li>
					<li>Dr. Erin Bayne (University of Alberta)</li>
				</ul>
				</div>
				<h3>Recommended Citation</h3>
				<div id="citation">
				<p>
					ABMI (2016). <span class="CommonName"></span> <i><span
						class="ScientificName"></span></i>. ABMI Species Website, version 3.1
					(2016-01-07). URL: <a id="external_link" href="" target="_blank"></a>.
				</p>
				</div>
			</div>
		</div>
	</div>
</div>


</div>
<!--  <link rel="stylesheet" href="https://cdn.jsdelivr.net/leaflet/1.0.0-rc.1/leaflet.css" />
  <script src="https://cdn.jsdelivr.net/leaflet/1.0.0-rc.1/leaflet-src.js"></script>

  <!-- Load Esri Leaflet from CDN --
  <script src="https://cdn.jsdelivr.net/leaflet.esri/2.0.0/esri-leaflet.js"></script>
   -->
<link rel="stylesheet"
	href="https://js.arcgis.com/3.16/dijit/themes/claro/claro.css">
<link rel="stylesheet"
	href="https://js.arcgis.com/3.16/esri/css/esri.css">


<script defer src="https://js.arcgis.com/3.16/"></script>
