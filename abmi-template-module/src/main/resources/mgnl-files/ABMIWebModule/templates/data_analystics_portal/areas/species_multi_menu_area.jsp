<%@ include file="../../includes/taglibs.jsp"%>
<!--  
the top blue menu for multiple species summary page. 
related to: species_multi_menu.jsp species_section.jsp -->
<c:set var="imageContentMap"
	value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />

<div class="row subNav">
	<div class="col-lg-12 species-titlebar">
		<div class="pageTitle">
			<div class="titleWrap">
				<div class="pagetitleimg">
					<img src="${imageUrl}" alt="">
				</div>
				<span>${content.speciesText}</span>
			</div>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#subnavbar"
				aria-expanded="false" aria-controls="navbar">
				<span class="nav-trigger"><span></span></span> <span class="sr-only">Select</span>
			</button>
		</div>
	</div>
	<div class="col-col-lg-12">
		<nav class="navbar">
			<div class="pagetitleimg desktoppic">
				<img src="${imageUrl}" alt="">
			</div>
			<div id="subnavbar" class="collapse navbar-collapse sp-menu">
				<ul class="nav navbar-nav">
					<c:forEach items="${components}" var="component"
						varStatus="indexrow">
						<c:set var="blockid"
							value="${fn:replace(component.header,' ', '')}" />
						<li><a href="#${blockid}">${component.header}</a></li>
					</c:forEach>
				
				<li class="verticle-line"></li>
					<!--  to add a verticle line here -->
				</ul>
			</div>
			<!--/.nav-collapse -->

		</nav>
	</div>
</div>

 <c:forEach items="${components}" var="component" varStatus="indexrow">
 	<div class="index-${theCount.index%2}" >
    <cms:component content="${component}" />
    </div>
  </c:forEach>
  