<%@ include file="../../includes/taglibs.jsp"%>
<!--  list of all files associated with the product. it has both list view and grid view -->
<c:choose>
	<c:when test="${fn:startsWith(content.placeholderimg, 'http')}">
		<c:set var="defaultHeaderImage" value="${content.placeholderimg}" />
	</c:when>
	<c:otherwise>
		<c:set var="defaultHeaderImage"
			value="${cmsfn:link(cmsfn:content(content.placeholderimg,'dam'))}" />
	</c:otherwise>
</c:choose>

<c:set var="mode" value="${param.mode}" />
<c:url var="queryuri" value="${state.originalBrowserURI}?">
</c:url>

<c:set var="title1"
	value="${state.mainContentNode.getProperty('title').getString()}" />
<h3>${empty content.header?title1:content.header}</h3>

<div>${cmsfn:decode(content).description}</div>

<c:if test="${not empty products}">
	<div class="listSyle">
		<a class="sidemenuGridview active" href="javascript:void(0)">&nbsp;</a>
		<a class="sidemenuListview" href="javascript:void(0)">&nbsp;</a>
	</div>

	<div class="data-post grid-view">

		<c:forEach items="${products}" var="product" varStatus="rowStatus">
			<c:set var="boxImage"
				value="${cmsfn:content(product.details.boxImage,'dam')}" />



			<div class="shortpost">
				<a href="${product.path}.html">
					<div class="post-feature-img">
						<!-- 1. cover image 2. default place holder image !-->
						<c:choose>
							<c:when test="${not empty boxImage }">
								<img alt="" src="${cmsfn:link(cmsfn:asContentMap(boxImage))}">
							</c:when>

							<c:when test="${not empty defaultImg }">
								<img alt="" src="${defaultImg}">
							</c:when>

						</c:choose>
					</div>
					<div class="post-text col-sm-8 col-md-9">
						<div class="post-header">
							<h4>${not empty product.details.customTitle?

								product.details.customTitle: product.title}</h4>

						</div>
						<div class="post-exerpt">
							<p>${product.details.boxDescription}</p>
						</div>

					</div>
				</a>
			</div>




		</c:forEach>
	</div>
</c:if>