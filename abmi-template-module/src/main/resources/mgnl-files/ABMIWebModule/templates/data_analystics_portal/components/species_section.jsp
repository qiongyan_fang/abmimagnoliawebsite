<%@ include file="../../includes/taglibs.jsp"%>
<!--  single section details. the header is used on the top blue menu 
related: species_multi_menu.jsp
-->
<c:set var="blockid" value="${fn:replace(content.header,' ', '')}" />
<div class="row sec-title" id="${blockid}">
	<div class="container">
		<h2>${content.header}</h2>
	</div>
</div>
<div class="row sec creambg">
	<div class="container">
		<div class="row wrap">
			<div class="col-lg-12">
				<cms:area name="textArea" />
			</div>
		</div>
	</div>
</div>