<%@ include file="../../includes/taglibs.jsp"%>
<!--  single download link with name, icon, and size of the files etc. -->
<c:set var="ftplink" value="http://ftp.public.abmi.ca/" />

<c:if test="${not empty content.url and content.fileicon != 'ftp'}">
		<a href="${fn:startsWith(content.url, '/')? ftplink:''}${content.url}" class="type-${content.fileicon}"
			target=_blank}>
			${content.productname} <c:if test="${not empty content.size}">(${content.size})</c:if>
		</a>
		</c:if>
		
		<c:if test="${empty content.url or content.fileicon == 'ftp'}">
			<span class="type-${content.fileicon}">${content.productname} 
			<c:if 	test="${not empty content.size}">(${content.size})</c:if>
			</span>

		</c:if>
		
		
		
	