<%@ include file="../../includes/taglibs.jsp"%>

<!--  single version of species product. such as bird intactness -->
<c:set var="imageContentMap"
	value="${cmsfn:content(content.image,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />


<a ${fn:startsWith(content.url, "http")?"target=_blank":""}
	href="${content.url}">
	<div class="species-product-bar">

		<div class="pagetitleimg desktoppic">
			<img src="${imageUrl}" alt="">
		</div>
		<span> ${content.productname}</span>

	</div>

</a>



