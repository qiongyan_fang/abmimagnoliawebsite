<%@ include file="../../includes/taglibs.jsp"%>

<c:if test="${not empty content.imageUrl}">
	<c:set var="topImage" value="${cmsfn:content(content.imageUrl,'dam')}" />

</c:if>

<li><c:if test="${not empty content.url}">
		<a href="${content.url}"
			target=${fn:startsWith(content.url, 'http')?'_blank':''}> <img
			alt="" src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
		</a>
	</c:if> <c:if test="${empty content.url }">
		<img alt="" src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
	</c:if> <c:if test="${not empty content.header or not empty content}">

		<div class="content-area${cmsfn:isEditMode()?'-x':''}">
			<c:if test="${not empty content.header}">
				<h4>${content.header }</h4>

			</c:if>
			<c:if test="${not empty content.description}">
				<h4>${cmsfn:decode(content).description }</h4>
			</c:if>
		</div>
	</c:if></li>
