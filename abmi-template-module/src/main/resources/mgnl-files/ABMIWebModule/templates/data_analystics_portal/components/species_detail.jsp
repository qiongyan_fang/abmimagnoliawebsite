<%@ include file="../../includes/taglibs.jsp"%>

<script>
	var gTsn = "${tsn}";
	var gCommonName = "${CommonName}";
	var gScientificName = "${ScientificName}";
	var gSpeciesGroupId = parseInt("${groupId}");

	function hideMap(imgObj) {
		$(imgObj).hide();
		$(".banner-txt").addClass("banner-txt-no-map");

	}

	function hideImage(imgObj) {
		//$(imgObj).hide();

	}
</script>

<!--  detailed species page header -->

<c:set var="servletPath"
	value="/FileDownloadServlet?dir=WEB_GRAPH&filename=" />

<c:set var="ftpPath"
	value="http://ftp.public.abmi.ca/OtherDataSets/Species/Profile_Pages/" />


<c:if test="${not empty profile.miniCircle}">
	<c:set var="iconUrl">
		${servletPath}${profile.miniCircle}
	</c:set>
</c:if>
<c:if test="${empty profile.miniCircle}">
	<c:set var="iconContentMap"
		value="${cmsfn:content(content.iconUrl,'dam')}" />
	<c:set var="iconUrl"
		value="${cmsfn:link(cmsfn:asContentMap(iconContentMap))}" />
</c:if>


<div id="species-profile-details" class="row sp-main sp-main-top">
	<div class="cell-img species-banner-height s-profilepic">
		<c:if test="${empty profile.mainBanner}">
			<c:set var="imageContentMap"
				value="${cmsfn:content(content.imageUrl,'dam')}" />
			<c:set var="imageUrl"
				value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />
			<c:if test="${not empty imageUrl}">
				<img class="mainBanner" src="${imageUrl}" alt="" />
			</c:if>
		</c:if>

		<c:if test="${not empty profile.mainBanner}">
			<img class="mainBanner" src="${servletPath}${profile.mainBanner}"
				alt="" />
		</c:if>


	</div>
	<div class="cell-img map-portal-sec species-banner-height formobile">
		<!--  <div class="btn-pos">
			<a class="mapPortal-btn">View Map Portal</a>
		</div>
		-->
		<c:if test="${not empty tsn}">
			<img class="distribution-map"
				src="${servletPath}/distributionMaps/${groupId}_${tsn}.png"
				alt="Alberta Map" onerror="hideMap('.map-portal-sec')" />
		</c:if>
	</div>

	<div class="banner-txt species-banner-height watermark-${groupId}"
		id="spp-top">
		<h2 class="auto CommonName">${CommonName}</h2>
		<h3 class="auto ScientificName">${ScientificName}</h3>
		<div class="species-desc">
			<div class="auto WebMainDescription">${profile.WebMainDescription}

			</div>

		</div>

		<c:choose>

			<c:when test="${forestModel == 1 and  prairieModel == 1}">
				<c:set var="modelType" value="Full" />
				<c:set var="modelLabel" value="Full" />
			</c:when>
			<c:when test="${forestModel == 1}">
				<c:set var="modelType" value="Forest" />
				<c:set var="modelLabel" value="Forest" />
			</c:when>
			<c:when test="${prairieModel == 1}">
				<c:set var="modelType" value="Prairie" />
				<c:set var="modelLabel" value="Prairie" />
			</c:when>
			<c:when test="${prairieModel == 2 or forestModel ==2}">
				<c:set var="modelLabel" value="Basic" />
				<c:set var="modelType" value="Few" />
			</c:when>

			<c:otherwise>
				<c:set var="modelType" value="Basic" />
				<c:set var="modelLabel" value="Basic" />
			</c:otherwise>
		</c:choose>


		<div class="species-details">
			<!-- 
1	Full	Full models
2	Few	Few detections
3	None	No data -->


			<p>
				Data Summary: <span> ${modelLabel}</span>
				<c:if test="${not empty modelComments}">
						- ${modelComments}
				</c:if>

			</p>
			<c:if test="${groupId==7}">
				<p class="smallnote">Note: Mammal results presented in this
					profile are based on ABMI winter snow tracking data collected from
					2001-2014. The ABMI is switching to camera-based indices to monitor
					mammals, but these results are not yet available.</p>
			</c:if>
		</div>

		<c:if test="${not empty report}">
			<a class="download-report" href="${servletPath}${report}" download>Download
				Report</a>
		</c:if>
		<c:if test="${ empty report}">
			<a style="display: none;" class="download-report" href="">Download
				Report</a>
		</c:if>
	</div>
	<div class="cell-img map-portal-sec species-banner-height fordesktop">
		<!-- <div class="btn-pos">
			<a class="mapPortal-btn">View Map Portal</a>
		</div>
		 -->
		<c:if test="${not empty tsn}">
			<div class="mapImgSec" style="margin-bottom: 0px;">
				<a href="javascript:void(0)" class="open-modalwindow"
					data-open-id="distribution-map"></a>
				<div class="album">
					<a rel="distribution-map" class="facybox" href=""><img
						src="${servletPath}/distributionMaps/${groupId}_${tsn}.png"
						" alt="" /></a>
				</div>


				<img class="distribution-map"
					src="${servletPath}/distributionMaps/${groupId}_${tsn}.png"
					alt="Alberta Map" />

			</div>
		</c:if>

	</div>

</div>



<div class="row subNav">
	<div class="col-lg-12 species-titlebar">
		<div class="pageTitle">
			<div class="titleWrap">
				<div class="pagetitleimg">
					<img class="miniCircle scrollTop" alt="" src="${iconUrl}" />
				</div>
				<span class="auto CommonName">${CommonName}</span>
			</div>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#subnavbar"
				aria-expanded="false" aria-controls="navbar">
				<span class="nav-trigger"><span></span></span> <span class="sr-only">Select</span>
			</button>
		</div>
	</div>
	<div class="col-col-lg-12">
		<nav class="navbar">
			<div class="pagetitleimg desktoppic">
				<img class="miniCircle scrollTop" title="go to top" src="${iconUrl}">
			</div>
			<a href="" class="sp-prev data-hover" data-toggle="popover"
				data-trigger="hover" data-placement="right" data-content=""
				style="display: none;">prev</a>
			<div id="subnavbar" class="collapse navbar-collapse sp-menu">
				<ul class="nav navbar-nav">
					<c:if
						test="${cmsfn:isEditMode() or not (forestModel != 1 and prairieModel != 1)}">
						<li><a href="#sec1">Introduction</a></li>
					</c:if>

					<c:if
						test="${cmsfn:isEditMode() or forestModel!= 3 or prairieModel != 3 }">
						<li><a href="#sec2">Habitat & Human <br />Footprint
								Associations
						</a></li>
					</c:if>
					<c:if
						test="${cmsfn:isEditMode() or forestModel== 1 or prairieModel == 1 }">
						<li><a href="#sec3">Impacts of Human <br />Footprint
						</a></li>
						<c:if test="${hasMap==1}">
							<li><a href="#sec4">Predicted Relative <br />Abundance
							</a></li>
						</c:if>
					</c:if>
					<!--  <li><a href="#sec5">Impacts of <br />Climate Change 
					</a></li> -->
					<c:if
						test="${cmsfn:isEditMode() or not empty profile.WebOtherIssue}">
						<li><a href="#sec6">Other Issues</a></li>
					</c:if>
					<li><a href="#sec7">References</a></li>
					<li class="verticle-line"></li>
					<!--  to add a verticle line here -->
				</ul>
			</div>
			<!--/.nav-collapse -->
			<a href="" class="sp-next data-hover" data-toggle="popover"
				data-trigger="hover" data-placement="left" data-content=""
				data-html="true" style="display: none;">next</a>

		</nav>
	</div>
</div>
<c:if
	test="${cmsfn:isEditMode() or not (forestModel != 1 and prairieModel != 1)}">
	<div class="row sec-title" id="sec1">
		<div class="container">
			<h2>Introduction</h2>
		</div>
	</div>
	<div class="row sec">
		<div class="container">
			<div class="row wrap">
				<!--  when either forest or prairie model is full -->
				<div class="col-lg-12" id="introduction-div">
					<c:if
						test="${cmsfn:isEditMode() or (forestModel == 1 or prairieModel == 1) }">

						<c:if test="${cmsfn:isEditMode()}">
							<h3>Introduction part for full models</h3>
						</c:if>
						<cms:area name="introduction" />
					</c:if>

					<c:if
						test="${cmsfn:isEditMode() or (not (forestModel == 1 or prairieModel == 1)) }">

						<c:if test="${cmsfn:isEditMode()}">
							<h3>Introduction part for not full models</h3>
						</c:if>
						<cms:area name="introduction_basic" />
					</c:if>
				</div>
			</div>
		</div>
	</div>
</c:if>

<c:if
	test="${cmsfn:isEditMode() or (not (forestModel == 3 and prairieModel == 3)) }">

	<div class="row sec-title" id="sec2">
		<div class="container">
			<h2 id="header20">Habitat & Human Footprint Associations</h2>
		</div>
	</div>
</c:if>

<c:if test="${forestModel ==2 or cmsfn:isEditMode() }">
	<!--  forest few detection -->
	<div class="row sec  few-detection-north">
		<div class="container">
			<div class="row wrap">
				<div class="col-lg-12">


					<div class="Forested-Region" style="margin-top: 20px;">
						<div class="head-s">
							<img src="/docroot/da/assets/Forested-Region.png" alt="">
							<h3>Habitat Associations for Species with Few Detections in
								the Forested Region</h3>
						</div>
					</div>

					<cms:area name="method_fdfa" />
					<div class="mapImgSec ForestedRegion-map">
						<img id="few_detection_north_graph" class="auto"
							src="${servletPath}${graphs.few_detection_north_graph.path}"
							alt="" />
					</div>


				</div>
			</div>
		</div>


		<!--  download button for few model only if prairie is no few as if prairie is few, the download button will appear with prairie few detection graph -->
		<c:if test="${prairieModel !=2 or cmsfn:isEditMode() }">
			<div class="col-lg-12">
				<div class="btnr">

					<a
						href="${ftpPath}/Few/${groupName}_Use-Availability_Index_Forested_and_Prairie_Region.zip"
						download class="downloaddata-btn" target=_blank>Download Data</a>
				</div>
			</div>
		</c:if>
		<!--  end of download button -->
	</div>
</c:if>
<c:if
	test="${forestModel==1 or prairieModel == 1 or cmsfn:isEditMode() }">


	<c:if test="${forestModel==1 or cmsfn:isEditMode()}">
		<!--  forest big graph -->
		<div class="row sec forestry-sec">
			<div class="container">
				<div class="row wrap">
					<div class="col-lg-12">
						<div class="auto WebHabitat">${profile.WebHabitat}</div>

						<cms:area name="method_hhfa" />


						<div class="Forested-Region">
							<div class="head-s">
								<img src="/docroot/da/assets/Forested-Region.png" alt="" />
								<h3>Species-habitat Associations in the Forested Region</h3>
							</div>
						</div>
						<div class="mapImgSec ForestedRegion-map">
							<a href="javascript:void(0)" class="zoom-graph"
								data-open-id="graph"></a>
							<div class="album">
								<a rel="graph" class="facybox species_graph_large"
									href="${servletPath}${graphs.species_graph.large_path}"><img
									class="species_graph_large"
									src="${servletPath}${graphs.species_graph.large_path}" alt=""></a>
							</div>
							<img id="species_graph"
								src="${servletPath}${graphs.species_graph.path}" alt=""
								class="auto mapImg">
						</div>
						<br /> <br />
						<div class="auto WebForestedRegionGraphText">${profile.WebForestedRegionGraphText }</div>


					</div>

				</div>
			</div>
		</div>
	</c:if>

	<c:if test="${prairieModel==1 or cmsfn:isEditMode()}">
		<!--  treed and non-tree graph for prairie -->
		<div class="row sec ${forestModel==1?"paririe":"forest"}-sec">
			<!--  if both are full model, display a divide orange line. -->
			<div class="container">
				<div class="row wrap">
					<div class="col-lg-12">
						<div class="Prairie-Region">
							<div class="head-s">
								<img src="/docroot/da/assets/Prairie-Region.png" alt="" />
								<h3>Species-habitat Associations in the Prairie Region</h3>
							</div>
						</div>
						<cms:area name="method_graph1" />
					</div>
					<div class="col-md-6">
						<h4>Non-Treed Sites in the Prairie Region</h4>
						<img class="auto" id="non_treed_graph"
							src="${servletPath}${graphs.non_treed_graph.path }"
							alt="Non-Treed Sites in the Prairie Region" />
					</div>
					<div class="col-md-6">
						<h4>Treed Sites in the Prairie Region</h4>
						<img class="auto" src="${servletPath}${graphs.treed_graph.path }"
							id="treed_graph" alt="Treed Sites in the Prairie Region" />
					</div>
					<div class="col-lg-12">
						<div class="auto WebNonTreeGraphText">${profile.WebNonTreeGraphText}</div>
						<div class="auto WebTreeGraphText">${profile.WebTreeGraphText}</div>
					</div>

				</div>
			</div>
		</div>
	</c:if>
	<div class="row sec linear-fp-sec">
		<div class="container">
			<div class="row wrap">
				<div class="col-md-12">
					<h3>Relationship to Linear Footprint</h3>
					<br />
					<cms:area name="method_graph2" />
				</div>
				<c:if test="${forestModel == 1 and prairieModel==1}">
					<!--  when both graphs exist, display side by side -->

					<div class="col-md-6">
						<div class="Forested-Region">
							<div class="head-s">
								<img src="/docroot/da/assets/Forested-Region.png" alt="" />
								<h4>Relationship to Linear Footprint in the Forest Region</h4>
							</div>
						</div>

						<img id="linear_north_graph" class="auto"
							src="${servletPath}${graphs.linear_north_graph.path}" alt="" /><br />
						<br />
						<div class="auto WebFLinearHFGraphText">${profile.WebFLinearHFGraphText}</div>
					</div>
					<div class="col-md-6">
						<div class="Prairie-Region">
							<div class="head-s">
								<img src="/docroot/da/assets/Prairie-Region.png" alt="" />
								<h4>Relationship to Linear Footprint in the Prairie Region</h4>
							</div>
						</div>
						<img id="linear_south_graph" class="auto"
							src="${servletPath}${graphs.linear_south_graph.path}" alt="" /><br />
						<br />
						<div class="auto WebPLinearHFGraphText">${profile.WebPLinearHFGraphText}</div>
					</div>



				</c:if>
				<c:if test="${forestModel == 1 and prairieModel !=1}">
					<div class="col-md-12">
						<div class="Forested-Region">
							<div class="head-s">
								<img src="/docroot/da/assets/Forested-Region.png" alt="" />
								<h4>Relationship to Linear Footprint in the Forest Region</h4>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<img id="linear_north_graph" class="auto"
							src="${servletPath}${graphs.linear_north_graph.path}" alt="" />
					</div>

					<div class="col-md-6 auto WebFLinearHFGraphText">${profile.WebFLinearHFGraphText}</div>

				</c:if>
				<c:if test="${forestModel != 1 and prairieModel ==1}">
					<div class="col-md-12">
						<div class="Prairie-Region">
							<div class="head-s">
								<img src="/docroot/da/assets/Prairie-Region.png" alt="" />
								<h4>Relationship to Linear Footprint in the Prairie Region</h4>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<img id="linear_south_graph" class="auto"
							src="${servletPath}${graphs.linear_south_graph.path}" alt="" />
					</div>
					<div class="col-md-6 auto WebPLinearHFGraphText">${profile.WebPLinearHFGraphText}</div>

				</c:if>
				<!--  download button for habitat: full model, forest model or prairie model-->
				<div class="col-lg-12">
					<div class="btnr">

						<a
							href="${ftpPath}/Habitat/Habitat_Coefficients_${modelType}_Region/${groupName}_Habitat_Coefficients_${modelType}_Region.zip"
							download class="downloaddata-btn" target=_blank>Download Data</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</c:if>

<c:if test="${prairieModel ==2 or cmsfn:isEditMode() }">
	<!--  prairie few detection -->
	<div class="row sec few-detection-south">
		<div class="container">
			<div class="row wrap">
				<div class="col-lg-12">


					<div class="Prairie-Region" style="margin-top: 20px;">
						<div class="head-s">
							<img src="/docroot/da/assets/Prairie-Region.png" alt="">
							<h3>Habitat Associations for Species with Few Detections in
								the Prairie Region</h3>
						</div>
					</div>





					<cms:area name="method_fdpa" />
					<div class="mapImgSec ForestedRegion-map">
						<img id="few_detection_south_graph" class="auto"
							src="${servletPath}${graphs.few_detection_south_graph.path}"
							alt="" />
					</div>


				</div>

				<!--  download button for few model-->
				<div class="col-lg-12">
					<div class="btnr">

						<a
							href="${ftpPath}/Few/${groupName}_Use-Availability_Index_Forested_and_Prairie_Region.zip"
							download class="downloaddata-btn" target=_blank>Download Data</a>
					</div>
				</div>
				<!--  end button -->
			</div>
		</div>
	</div>





</c:if>
<c:if
	test="${forestModel==1 or prairieModel == 1 or cmsfn:isEditMode() }">

	<div class="row sec-title" id="sec3">
		<div class="container">
			<h2>Impacts of Human Footprint</h2>
		</div>
	</div>
	<div class="row sec">
		<div class="container">
			<div class="row wrap">
				<div class="col-md-12">
					<div class="auto WebHFEffect">${profile.WebHFEffect}</div>
					<cms:area name="method_ihf" />

				</div>
				<c:if test="${forestModel == 1 and prairieModel==1}">
					<!--  when both graphs exist, display side by side -->
					<div class="col-md-6 Forested-Region">
						<div class="head-s">
							<img src="/docroot/da/assets/Forested-Region.png" alt="" />
							<h4>Human Footprint Effects in the Forested Region</h4>
						</div>
						<img class="auto"
							src="${servletPath}${graphs.sector_effect_north_graph.path}"
							id="sector_effect_north_graph"
							alt="Human Footprint Effects in the Forested Region" /><br />
						<div class="auto WebHFForestRegionGraph">${profile.WebHFForestRegionGraph}</div>
					</div>
					<div class="col-md-6 Prairie-Region">
						<div class="head-s">
							<img src="/docroot/da/assets/Prairie-Region.png" alt="" />
							<h4>Human Footprint Effects in the Prairie Region</h4>
						</div>
						<img class="auto"
							src="${servletPath}${graphs.sector_effect_south_graph.path}"
							id="sector_effect_south_graph"
							alt="Human Footprint Effects in the Prairie Region" /><br />
						<div class="auto WebPrarieRegionGraphText">${profile.WebPrarieRegionGraphText}</div>
					</div>
				</c:if>
				<c:if test="${forestModel == 1 and prairieModel!=1}">
					<div class="col-md-12">
						<div class="Forested-Region">
							<div class="head-s">
								<img src="/docroot/da/assets/Forested-Region.png" alt="" />
								<h4>Human Footprint Effects in the Forested Region</h4>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<img id="sector_effect_north_graph" class="auto"
							src="${servletPath}${graphs.sector_effect_north_graph.path}"
							alt="" />
					</div>

					<div class="col-md-6 auto WebHFForestRegionGraph">${profile.WebHFForestRegionGraph}</div>
				</c:if>

				<c:if test="${forestModel != 1 and prairieModel ==1}">
					<div class="col-md-12">
						<div class="Prairie-Region">
							<div class="head-s">
								<img src="/docroot/da/assets/Prairie-Region.png" alt="" />
								<h4>Human Footprint Effects in the Prairie Region</h4>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<img id="sector_effect_south_graph" class="auto"
							src="${servletPath}${graphs.sector_effect_south_graph.path}"
							alt="" />
					</div>

					<div class="col-md-6 auto WebPrarieRegionGraphText">${profile.WebPrarieRegionGraphText}</div>
				</c:if>
				<div class="col-lg-12">
					<div class="btnr">

						<a
							href="${ftpPath}/Sector-Effect/${groupName}_Sector Effects_Forested_and_Praire Region.zip"
							download class="downloaddata-btn" target=_blank>Download Data</a>
					</div>
				</div>


			</div>
		</div>
	</div>
	<!--  end -->

	<c:if test="${cmsfn:isEditMode() or hasMap==1 }">
		<div class="row sec-title map-section-div" id="sec4">
			<div class="container">
				<h2>Predicted Relative Abundance</h2>
			</div>
		</div>


		<div class="row sec map-section-div">
			<div class="container">
				<div class="wrap">
					<div class="auto WebRange">${profile.WebRange}</div>

					<cms:area name="method_pra" />
					<div class="row relation_sec">
						<div class="col-md-4">
							<h4>Reference Conditions</h4>
							<div class="mapImgSec map-ref">
								<a href="javascript:void(0)" class="open-modalwindow"
									data-open-id="map-2"></a>
								<div class="album">
									<a rel="map-2" class="facybox refe-map" href=""><img
										class="refe-map" src="" alt="" /></a>
								</div>

								<img src="" alt="" class="mapImg refe-map"
									onerror="hideImage('.map-section-div');" />

							</div>
							<div class="btnr-map">
								<a class="downloaddata-btn refe-map" href="" download>Download
									Map Image</a>
							</div>
							<ul>
								<li>The reference condition shows the predicted relative
									abundance of the <span class="CommonName">${CommonName}</span>
									after all human footprint had been backfilled based on native
									vegetation in the surrounding area.
								</li>
							</ul>

						</div>
						<div class="col-md-4">
							<h4>Current Conditions</h4>
							<div class="mapImgSec map-current">
								<a href="javascript:void(0)" class="open-modalwindow"
									data-open-id="map-3"></a>
								<div class="album">
									<a rel="map-3" class="facybox curr-map" href=""><img src=""
										class="curr-map" alt=""
										onerror="hideImage('.map-section-div');" /></a>
								</div>

								<img alt="" src="" class="mapImg curr-map" />

							</div>
							<div class="btnr-map">
								<a class="downloaddata-btn curr-map" href="" download>Download
									Map Image</a>
							</div>
							<ul>
								<li>The current condition is the predicted relative
									abundance of the <span class="CommonName">${CommonName}</span>
									taking current human footprint (circa 2012) into account.
								</li>
							</ul>

						</div>
						<div class="col-md-4">
							<h4 class="colored">Difference Conditions</h4>
							<div class="mapImgSec">
								<a href="javascript:void(0)" class="open-modalwindow"
									data-open-id="map-4"></a>
								<div class="album">
									<a rel="map-4" class="facybox diff-map" href=""><img
										class="diff-map" src="" alt="" /></a>
								</div>

								<img src="" alt="" class="mapImg diff-map"
									onerror="hideImage('.map-section-div');" />

							</div>
							<div class="btnr-map">
								<a class="downloaddata-btn diff-map" href="" download>Download
									Map Image</a>
							</div>
							<div class="auto WebDifferenceMapText">${profile.WebDifferenceMapText}</div>

						</div>
					</div>
					<div class="btnr">
						<a class="downloaddata-btn" id="raset-download-btn" download>Download
							Raster Data</a>
					</div>

				</div>
			</div>
		</div>
	</c:if>

</c:if>
<!--  end of check if any of the forest or prairie mode is full -->

<!--  start of climate changes 
<div class="row sec-title" id="sec5">
	<div class="container">
		<h2>Impacts of Climate Change</h2>
		<cms:area name="method_icc" />
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-md-12">
				<h4 class="nomrg">Results Coming Soon</h4>
			</div>
		</div>
	</div>
</div>
 -->
<!--  end of climate changes  -->
<c:if test="${not empty profile.WebOtherIssue}">
	<div class="row sec-title" id="sec6">
		<div class="container">
			<h2>Other Issues</h2>
		</div>
	</div>

	<div class="row sec">
		<div class="container">
			<div class="row wrap">
				<div class="col-md-12">
					<h4 class="nomrg">${profile.WebOtherIssue}</h4>
				</div>
			</div>
		</div>
	</div>
</c:if>
<div class="row sec-title" id="sec7">
	<div class="container">
		<h2>References</h2>
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<c:if test="${not empty profile.WebReference}">
				<div class="col-md-6">
					<h3>References</h3>
					<div class="auto  WebReference">${profile.WebReference}</div>
					<div class="clearboth"></div>
					<h3>Photo & Credits</h3>
					<div class="clearboth"></div>
					<p>Photos: TBD</p>
				</div>
			</c:if>
			<div class="col-md-6">
				<h3>Data Sources</h3>
				<div id="data-source">
					<c:if test="${groupId == 1}">
						<p>Information from ABMI bird point counts was combined with
							information from other organizations and individuals:</p>
						<ul>
							<li>Environment Canada (North American Breeding Bird Survey
								and Joint Oil Sands Monitoring programs)</li>
							<li>Ecological Monitoring Committee for the Lower Athabasca
								(EMCLA)</li>
							<li>Dr. Erin Bayne (University of Alberta)</li>
						</ul>
					</c:if>
					<c:if test="${groupId != 1}">
						<p>Data collected by ABMI.</p>
					</c:if>
				</div>
				<div class="clearboth"></div>
				<h3>Recommended Citation</h3>
				<div id="citation">
					<p>
						ABMI (2016). <span class="CommonName">${CommonName}</span> <i><span
							class="ScientificName">${ScientificName}</span></i>. ABMI Website
						URL: <a id="external_link"
							href="http://www.abmi.ca/home/data-analytics/biobrowser-home/species-profile?tsn=${tsn}">http://www.abmi.ca/home/data-analytics/biobrowser-home/species-profile?tsn=${tsn}</a>.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>


</div>

<div class="row sec ">
	<div class="container btnr">
		<p class="backTop scrollTop">Back to Top</p>
	</div>
</div>

