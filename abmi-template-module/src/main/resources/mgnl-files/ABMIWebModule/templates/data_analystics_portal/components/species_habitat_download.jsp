<%@ include file="../../includes/taglibs.jsp"%>
<!--  raw data download page -->
<c:set var="imageContentMap"
	value="${cmsfn:content(content.backgroundImageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />


<c:set var="iconContentMap"
	value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="iconUrl"
	value="${cmsfn:link(cmsfn:asContentMap(iconContentMap))}" />




<div id="species-habitat-download-component" class="species-habitat-sec">
	<h3 id="top-pos">
		<cms:area name="dadownloadheader" />
	</h3>
	<div>
		<cms:area name="dadownloaddescription" />
	</div>
	<!-- Multistep Form -->
	<div class="btn-sec">
		<a href="javascript:void(0);" id="download-all-btn"
			class="direct-download download-btn btn-solid btn-green">Download
			ALL Data</a>
		<div style="margin-top: 15px;">
			<span class="mrg-lr">OR</span>
		</div>
		<!-- <input
			class="start-steps next_btn btn-solid btn-orange" name="next"
			type="button" value="1. Choose Variables"> -->
	</div>

	<!-- Progress Bar -->
	<ul id="progressbar">
		<li class="step1" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Select from a list of terrestrial and aquatic variables." data-div="first">Choose
				Variables</li>
		<li class="step2" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Apply one or more filters to the data."  data-div="second">Choose Filters</li>
		<li class="step3" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Confirm variables and filters applied to the data in the previous two steps and then download the subset of the data."  data-div="third">Review & Download Data</li>
	</ul>
	<!-- Fieldsets -->
	<div class="detailed-form">

		<div class="selection-sec" id="first" style="display: none;">
			<h3 class="steps">Step 1 of 3 : Choose Variables</h3>
			<div class="filter-sec">
				<span class="toggle-bar"></span> <span class="choose-toggle-link">Choose
					Variables:</span> <span class="clear-all">Clear All</span> <span
					class="select-divider">|</span> <span class="select-all">Select
					All</span>
			</div>
			<div id="step1-filter" class="choose-filters">
				<div class="alert-area">
					<div class="infotip"></div>
					<span>Please choose one or more variables.</span>
				</div>

				<div class="terrestrial select-cat">
					<div class="selection-bar">
						<span class="s-title" class="data-hover" data-toggle="popover"
							data-trigger="hover" data-placement="right"
							data-content="Terrestrial variables are collected using the ABMI's <a href='/home/publications/401-450/432.html' target=_blank>Terrestrial Data Collection Protocols.</a>">Terrestrial
							Variables</span> <span class="clear-selected">Clear</span> <span
							class="select-divider black-font">|</span> <span
							class="select-selected">Select</span>
					</div>
					<div class=" select-from">
						<div class="row">
							<div class="species col-md-6">
								<ul class="tree">
									<li class="has-child"><span class="toggle-bar"></span> <input
										id="SpeciesVariables" type="checkbox"
										name="terrestrial-species" class="excludeFromSummary"
										value="Species Variables" /> <label for="SpeciesVariables"
										class="data-hover" data-toggle="popover" data-trigger="hover"
										data-placement="right" data-content="">Species
											Variables</label>
										<ul class="sub-selection">
										</ul></li>
								</ul>
							</div>
							<div class="habitat col-md-6">
								<ul class="tree">
									<li class="has-child"><span class="toggle-bar"></span> <input
										id="HabitatVariables" type="checkbox"
										name="terrestrial-habitat" class="excludeFromSummary"
										value="Habitat Variables" /> <label for="HabitatVariables"
										class="data-hover" data-toggle="popover" data-trigger="hover"
										data-placement="right" data-content="">Habitat
											Variables</label>
										<ul class="sub-selection">

										</ul></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="wetland select-cat">
					<div class="selection-bar">
						<span class="s-title" class="data-hover" data-toggle="popover"
							data-trigger="hover" data-placement="right"
							data-content="Wetland variables are collected using the ABMI's  <a href='http://abmi.ca/home/publications/401-450/433.html' target=_blank>Wetland Field Collection Protocols.</a>">WETLAND
							VARIABLES</span> <span class="clear-selected">Clear</span> <span
							class="select-divider black-font">|</span> <span
							class="select-selected">Select</span>
					</div>
					<div class=" select-from">
						<div class="row">
							<div class="species col-md-6">
								<ul class="tree">
									<li class="has-child"><span class="toggle-bar"></span> <input
										id="SpeciesVariables1" type="checkbox" name="wetland-species"
										class="excludeFromSummary" value="Species Variables1" /> <label
										for="SpeciesVariables1" class="data-hover"
										data-toggle="popover" data-trigger="hover"
										data-placement="right" data-content="">Species
											Variables</label>
										<ul class="sub-selection">

										</ul></li>
								</ul>
							</div>
							<div class="habitat col-md-6">
								<ul class="tree">
									<li class="has-child"><span class="toggle-bar"></span> <input
										id="HabitatVariables1" type="checkbox" name="wetland-habitat"
										class="excludeFromSummary" value="Habitat Variables1" /> <label
										for="HabitatVariables1" class="data-hover"
										data-toggle="popover" data-trigger="hover"
										data-placement="right" data-content="">Habitat
											Variables</label>
										<ul class="sub-selection">

										</ul></li>
								</ul>
							</div>
						</div>
					</div>
				</div>


			</div>
			<div class="" style="font-size: 15px;">
				<div class="infotip" style="float: left;"></div>
				<div
					style="margin-left: 15px; color: #cc0000; margin-top: 5px; float: left;">hover
					over text for details</div>
			</div>





			<div class="btn-sec">

				<div class="flt-right right-btns">
					<input class="next_btn btn-solid btn-blue flt-right" name="next"
						id="step1-btn-next" type="button" value="2. Choose Filters   >">
					<div class="mrg-lr"
						style="clear: both; text-align: center; margin-bottom: 7px;">OR</div>
					<a href="javascript:void(0);" id="step1-direct-download"
						class="direct-download download-btn btn-solid btn-green">3.
						Review & Download Data</a>
				</div>
			</div>
		</div>

		<div class="selection-sec" id="second">
			<h3 class="steps">Step 2 of 3 : Choose Filters</h3>
			<div class="filter-sec">
				<span class="toggle-bar"></span> <span class="choose-toggle-link">Choose
					Filters:</span> <span class="clear-all">Clear All</span>
			</div>
			<div class="choose-filters">
				<div class="select-cat" id="site-rotation-checkbox">
					<div class="selection-bar">
						<span class="s-title" class="data-hover" data-toggle="popover"
							data-trigger="hover" data-placement="right" data-content="">SITE
							VISITS</span> <span class="clear-selected">Clear</span>
						<div class="alert-area">
							<div class="infotip"></div>
							<span>Please select Survey Period and Data Type, or
								Repeated Surveys</span>
						</div>
					</div>
					<div class="select-from">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<ul class="tree">
										<li class="has-child"><span class="toggle-bar"></span> <input
											id="SurveyPeriod" type="checkbox" name="rotation"
											class="excludeFromSummary" value="Survey Period" /> <label
											for="SurveyPeriod" class="data-hover" data-toggle="popover"
											data-trigger="hover" data-placement="right" data-content="">Survey
												Period</label>
											<ul class="sub-selection" id="survey-periond-detail">

											</ul></li>
									</ul>
								</div>


								<div class="col-md-6">
									<ul class="tree">
										<li class="has-child"><span class="toggle-bar"></span> <input
											id="datatype" type="checkbox" name="data-type1"
											class="excludeFromSummary" value="Data Type" /> <label
											for="datatype" class="data-hover" data-toggle="popover"
											data-trigger="hover" data-placement="right" data-content="">Data
												Type</label>
											<ul class="sub-selection" id="data-type-detail1">

											</ul></li>
									</ul>
								</div>
							</div>


							<div class="col-md-12">
								<ul class="tree">
									<li class="has-child"><span class="toggle-bar"></span> <input
										id="Repeat-Surveys" type="checkbox" name="data-type2"
										class="excludeFromSummary" value="Repeat Surveys" /> <label
										for="Repeat-Surveys" class="data-hover" data-toggle="popover"
										data-trigger="hover" data-placement="right"
										data-content="Refers to data collected at sites that have been visited at least twice. ">Repeat
											Surveys</label>
										<ul class="sub-selection" id="data-type-detail2">

										</ul></li>
								</ul>
							</div>

						</div>
					</div>
					<!--  end of form -->

				</div>
				<!--  end of select-cat -->
				<div class="select-cat" id="region-checkbox">
					<div class="selection-bar">
						<span class="s-title" class="data-hover" data-toggle="popover"
							data-trigger="hover" data-placement="right"
							data-content="Some text here">REGION</span> <span
							class="clear-selected">Clear</span>
						<div class="alert-area">
							<div class="infotip"></div>
							<span>Please select one or more region</span>
						</div>
					</div>
					<div class="select-from">
						<ul class="tree" id="regiondetail">
							<li class="level1list"><input id="region" type="checkbox"
								name="region" value="region" checked /> <label for="region">All
									Alberta</label></li>

						</ul>
					</div>
				</div>
				<!--  end of select-cat -->

			</div>

			<div class="" style="font-size: 15px;">
				<div class="infotip" style="float: left;"></div>
				<div
					style="margin-left: 15px; color: #cc0000; margin-top: 5px; float: left;">hover
					over text for details</div>
			</div>

			<div class="btn-sec">
				<input class="pre_btn btn-solid btn-orange flt-left" name="previous"
					type="button" value="<   Previous"> <input
					id="step2-btn-next" class="next_btn btn-solid btn-blue flt-right"
					name="next" type="button" value="3. Review & Download  >">
			</div>
		</div>



		<div class="selection-sec" id="third">
			<h3 class="steps">Step 3 of 3 : Review & Download Data</h3>
			<div class="review-data-sec">
				<h4 class="review-title">Review Your Selection:</h4>
				<div class="selected-data" id="data-summary-details">
					<div class="col-sm-4">
						<h4>Filters:</h4>
						<div id="region-summary">
							<span class="selection-title">Region:</span>
							<div class="data-list">
								<ul>
									<li>Alberta</li>
								</ul>
							</div>
						</div>
						<div id="rotation-summary">
							<span class="selection-title">Survey Period:</span>
							<div class="data-list">
								<ul>

								</ul>
							</div>
						</div>
						<div id="data-type1-summary">
							<span class="selection-title">Data Type:</span>
							<div class="data-list">
								<ul>
								</ul>
							</div>
						</div>
						<div id="data-type2-summary">
							<span class="selection-title">Repeat Surveys:</span>
							<div class="data-list">
								<ul>
								</ul>
							</div>
						</div>

					</div>
					<div class="col-sm-4 mid-sec">
						<h4>Species:</h4>
						<div id="terrestrial-species-summary">
							<span class="selection-title">Terrestrial:</span>
							<div class="data-list">
								<ul>
									<li>none selected</li>
								</ul>
							</div>
						</div>
						<div id="wetland-species-summary">
							<span class="selection-title">Wetlands:</span>
							<div class="data-list">
								<ul>
									<li>none selected</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<h4>Habitat:</h4>
						<div id="terrestrial-habitat-summary">
							<span class="selection-title">Terrestrial:</span>
							<div class="data-list">
								<ul>
									<li>none selected</li>
								</ul>
							</div>
						</div>
						<div id="wetland-habitat-summary">
							<span class="selection-title">Wetlands:</span>
							<div class="data-list">
								<ul>
									<li>none selected</li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
			<h3 class="txtcenter mrgBnone" id="download-msg"></h3>
			<div class="btn-sec">
				<input class="pre_btn btn-solid btn-orange" type="button"
					value="<   Previous"><span class="mrg-lr">OR</span><a
					id="final-data-download" href=""
					class="progress-button download-btn btn-solid btn-green"
					data-loading="Downloading Selected Data"
					data-finished="Download completed!" data-type="background-bar">Download
					Selected Data</a><span class="tz-bar background-bar"
					style="display: none; width: 0%;"></span><a href="#"
					class="restart btn-solid">Restart</a>



			</div>


		</div>
	</div>
	<!--  end of detailed form -->
	<div class="clearboth"></div>
	<div id="raw-data-status" style="margin-top: 40px; display: none;">
		<div class="review-data-sec">
			<a id="overview-header" data-toggle="collapse"
				data-target="#detailed-data-overview">
				<h4 class="review-title">Summary of Available Data</h4>
			</a>

			<div class="overview-table collapse in" id="detailed-data-overview"
				style="height: auto;">

				<!--  terrestrial -->
				<h4>Terrestrial Variables</h4>
				<h5>Species</h5>
				<div id="terrestrial-species-overview">
					<div class="col-sm-12 data-overview header">
						<div class="col-sm-4">Variable</div>
						<div class="col-sm-4">Public Release Date</div>
						<div class="col-sm-4">Currently Available (Years)</div>
					</div>
				</div>
				<h5>Habitat</h5>
				<div id="terrestrial-habitat-overview">
					<div class="col-sm-12  data-overview header">
						<div class="col-sm-4">Variable</div>
						<div class="col-sm-4">Public Release Date</div>
						<div class="col-sm-4">Currently Available (Years)</div>
					</div>
				</div>



				<!--  wetland -->
				<h4>Wetland Variables</h4>
				<h5>Species</h5>
				<div id="wetland-species-overview">
					<div class="col-sm-12  data-overview header">
						<div class="col-sm-4">Variable</div>
						<div class="col-sm-4">Public Release Date</div>
						<div class="col-sm-4">Currently Available (Years)</div>
					</div>
				</div>
				<h5>Habitat</h5>
				<div id="wetland-habitat-overview">
					<div class="col-sm-12  data-overview header">
						<div class="col-sm-4">Variable</div>
						<div class="col-sm-4">Public Release Date</div>
						<div class="col-sm-4">Currently Available (Years)</div>
					</div>
				</div>

				<!--  end of wetland -->
			</div>
			<!--  end of overview -->
		</div>
		<!-- end of review sec -->

	</div>
	<!--  end of raw data overview -->

	<!--  end of summary data -->
</div>


