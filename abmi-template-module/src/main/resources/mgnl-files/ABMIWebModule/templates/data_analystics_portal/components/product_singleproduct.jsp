<%@ include file="../../includes/taglibs.jsp"%>

<!--  single version of product. e.g. 2010 human footprint. inside this users can put in download links -->

<div class="databy-year">
	<h4>${content.productname}</h4>
	<div class="yearData">

		<cms:area name="links" />
		<c:if test="${not empty content.lastupdated}">
			<span class="type-info">Updated ${content.lastupdated}</span>
		</c:if>
	</div>
</div>
