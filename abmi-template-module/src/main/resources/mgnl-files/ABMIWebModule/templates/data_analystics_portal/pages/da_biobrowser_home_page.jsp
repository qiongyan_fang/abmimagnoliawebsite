<%@ include file="../../includes/taglibs.jsp"%>
<%@ include file="../includes/da_header.jsp"%>

<body class="container-fluid data-analytics bio-browser-page">
	<%@ include file="../includes/da_top_menu.jsp"%>

	<cms:area name="bannerArea" />
	<div class="row species-monitored-counter">
		<div class="container">
			<h3>
				<span id="spp-count" class="counter1"></span>
				<cms:area name="speciesTextArea" />
			</h3>
		</div>
	</div>
	<div class="row species-sec">
		<div class="container">
			<h3>Choose a Species Category</h3>
		</div>
		<div id="species-category" class="species-category">
			<div class="col-md-4 col-sm-6 nopadding">
				<div class="item abmi-box1">
					<cms:area name="speciesBlockArea1" />
				</div>
				<div class="item abmi-box2">
					<cms:area name="speciesBlockArea2" />
				</div>
			</div>
			<div class="col-md-8 col-sm-6 nopadding">
				<div class="col-lg-5 col-md-6 nopadding">
					<div class="item abmi-box3">
						<cms:area name="speciesBlockArea3" />
					</div>
				</div>
				<div class="col-lg-7 col-md-6 nopadding">
					<div class="item  abmi-box4">
						<cms:area name="speciesBlockArea4" />
					</div>
				</div>
				<div class="col-lg-7 col-md-6 nopadding">
					<div class="item abmi-box5">
						<cms:area name="speciesBlockArea5" />
					</div>
				</div>
				<div class="col-lg-5 col-md-6 nopadding">
					<div class="item abmi-box6">
						<cms:area name="speciesBlockArea6" />
					</div>
					<div class="item abmi-box7">
						<cms:area name="speciesBlockArea7" />
					</div>
				</div>
			</div>
		</div>


	</div>
	<div class="row guild-sec">
		<div class="container">
			<cms:area name="guildArea" />
		</div>
	</div>
	<div class="row stat-analysis-collaborations">
		<div class="container">
			<div class="row">
				<cms:area name="learnMoreArea" />
			</div>
		</div>
	</div>
	<cms:area name="highlightArea" />

	<%@ include file="../includes/da_footer.jsp"%>
</body>
</html>