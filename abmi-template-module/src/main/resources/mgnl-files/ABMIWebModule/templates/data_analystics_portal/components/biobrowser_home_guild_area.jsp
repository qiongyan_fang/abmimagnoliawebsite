<%@ include file="../../includes/taglibs.jsp"%>
<!--  guild slider area -->
<c:set var="mode" value="${cmsfn:isEditMode()?'edit': ''}" />
		<h3>${content.guildTitle}</h3>
		<div class="guild-slider${mode}">
			<cms:area name="single_guild_area" />
		</div>
