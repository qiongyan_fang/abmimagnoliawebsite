<%@ include file="../../includes/taglibs.jsp"%>
<!-- (initially used in richness page)  map viewer:  with a list of maps and users can navigate left or right, and open up one and slide left right too.-->
<div class="col-xs-12 mapsWrapper">
	<ul class="mapslider">

		<c:forEach items="${maps}" var="row" varStatus="rowStatus">
			<c:if test="${not fn:startsWith(row.iconUrl, 'http')}">

				<c:set var="imageContentMap"
					value="${cmsfn:content(row.iconUrl,'dam')}" />
				<c:set var="iconUrl"
					value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />
			</c:if>

			<c:if test="${fn:startsWith(row.iconUrl, 'http')}">
				<c:set var="iconUrl" value="${row.iconUrl}" />

			</c:if>
			<c:if test="${not fn:startsWith(row.imageUrl, 'http')}">
				<c:set var="largeImageContentMap"
					value="${cmsfn:content(row.imageUrl,'dam')}" />
				<c:set var="largeImageUrl"
					value="${cmsfn:link(cmsfn:asContentMap(largeImageContentMap))}" />
			</c:if>
			<c:if test="${fn:startsWith(row.imageUrl, 'http')}">
				<c:set var="largeImageUrl" value="${row.imageUrl}" />

			</c:if>
			<a class="fancybox-thumb" rel="fancybox-thumb"
				href="${empty iconUrl?largeImageUrl:iconUrl}" title="${row.title}">
				<img src="${empty largeImageUrl? iconUrl:largeImageUrl}" /> 
				<div class="bx-caption"><span>${row.title}</span></div>
			</a>

		</c:forEach>

	</ul>
</div>
<div class="clearboth"></div>
