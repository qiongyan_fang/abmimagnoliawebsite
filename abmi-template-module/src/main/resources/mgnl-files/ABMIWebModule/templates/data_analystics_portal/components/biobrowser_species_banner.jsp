<%@ include file="../../includes/taglibs.jsp"%>
<!--  this page  displays (left image) middle text without species watermarks. 
used in any page.
-->
<c:set var="imageContentMap"
	value="${cmsfn:content(content.backgroundImageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />


    <div class="row browser-banner sp-main-top"  style="background-image:url(${imageUrl});background-repeat: no-repeat;
    background-size: cover;">
	    <div class="banner-caption">
		    <div class="container">
			    <div class="textwrap">
				    <h1>${content.header}</h1>
				    <h3>${cmsfn:decode(content).header2 }</h3>
				    <p>${cmsfn:decode(content).description }</p>
		    	</div>
		    </div>
	    </div>
	</div>