<%@ include file="../../includes/taglibs.jsp"%>
<c:set var="groupId">${param.groupId}</c:set>
<!--  this component  displays current selection of species groups, and a drop down graph menus to switch to a different species group. 
used in species list page.
-->
<script>
var gSpeciesGroupId = "${groupId}";
var gDetailedUrlParam = "${content.speciesDetailUrl}";

</script>

<div class="row secondarybar">
	<div class="container">
		<div class="row col-md-7">
			<div class="totalNumbers">
				<h4>
					<span id="sppGroupName"></span> <span id="sppCount"></span>
				</h4>
			</div>
			<div class="slectGroup">Choose Species Group</div>
		</div>

		<div class="search hidden-sm hidden-xs">
			<div class="push-right">
				<input type="text" placeholder="Search" id="searchSpecies" name="q"> <input
					type="submit" class="btn" id="searchSpeciesBtn"
					value="">
			</div>
		</div>
	</div>
</div>

<!--Choose Species Group-->
<div class="row species-groupList">
	<div class="container">
		<div class="row">
			<c:if test="${groupId != 1}">
				<div class="col-sm-2 col-xs-4">
					<a href="?groupId=1" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/birds-sm.jpg" alt="mamals" />
						</div> <span>Birds</span>
					</a>
				</div>
			</c:if>

			<c:if test="${groupId != 7}">
				<div class="col-sm-2 col-xs-4">
					<a href="?groupId=7" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/mammals-sm.jpg" alt="mamals" />
						</div> <span>Mammals</span>
					</a>
				</div>
			</c:if>

			<c:if test="${groupId != 2}">
				<div class="col-sm-2 col-xs-4">
					<a href="?groupId=2" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/Vascular-Plants-sm.jpg"
								alt="Vascular Plants" />
						</div> <span>Vascular Plants</span>
					</a>
				</div>
			</c:if>
			<c:if test="${groupId != 3}">
				<div class="col-sm-2 col-xs-4">
					<a href="?groupId=3" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/soil-mites-sm.jpg" alt="Soil Mites" />
						</div> <span>Soil Mites</span>
					</a>
				</div>
			</c:if>
			<c:if test="${groupId != 4}">
				<div class="col-sm-2 col-xs-4">
					<a href="?groupId=4" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/Lichens.jpg" alt="Lichens" />
						</div> <span>Lichens</span>
					</a>
				</div>
			</c:if>
			<c:if test="${groupId != 6}">
				<div class="col-sm-2 col-xs-4">
					<a href="?groupId=6" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/Aquatic-Invertabrates-sm.jpg"
								alt="Aquatic Invertabrates" />
						</div> <span>Aquatic Invertabrates</span>
					</a>
				</div>
			</c:if>
			<c:if test="${groupId != 5}">
				<div class="col-sm-2 col-xs-4">
					<a href="?groupId=5" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/Bryophytes-sm.jpg" alt="Bryophytes" />
						</div> <span>Bryophytes</span>
					</a>
				</div>
			</c:if>

		</div>
	</div>
</div>

<!--Data Filter/ Sort-->
<div class="row data-section">
	<div class="container">
		<div class="data-summaries">
			
			<p><span class="full">Full</span><span class="forested">Forested Region</span><span class="prairie">Prairie Region</span><span class="basic">Basic</span></p>
		    
		</div>
		<c:if test="${groupId == 2 }"> <!--  vascular plant -->
			<div id="filters" class="button-group">
				<button class="button is-checked" id="all-filter" data-filter="*">ALL</button>
				<button class="button" id="terrestrial-filter" data-filter=".terrestrial">TERRESTRIAL</button>
				<button class="button" id="aquatic-filter" data-filter=".aquatic">AQUATIC</button>
			</div>
		</c:if>

		<div class="dataWrapper">
			<div class="data-block" id="data-block">

				<table id="species-data">
					<thead>
						<tr>
						
							<th>&nbsp;</th>
							<th>Summary</th>
							<th>Common Name</th>
							<th>Scientific Name</th>
							<c:if test="${groupId == 2}">
								<th>Taxa Name</th>
							</c:if>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody id="dataBody" ${groupId == 2? "class='isotope'":""}>
					</tbody>
				</table>
			</div>
		
		
		</div>
	</div>
</div>
