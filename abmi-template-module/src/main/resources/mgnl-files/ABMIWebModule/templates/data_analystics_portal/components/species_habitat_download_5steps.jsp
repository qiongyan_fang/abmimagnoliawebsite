<%@ include file="../../includes/taglibs.jsp"%>
<!--  raw data download page -->
<c:set var="imageContentMap"
	value="${cmsfn:content(content.backgroundImageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />


<c:set var="iconContentMap"
	value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="iconUrl"
	value="${cmsfn:link(cmsfn:asContentMap(iconContentMap))}" />




<div id="species-habitat-download-component" class="species-habitat-sec">
	<h3 id="top-pos">
		<cms:area name="dadownloadheader" />
	</h3>
	<div>
		<cms:area name="dadownloaddescription" />
	</div>
	<!-- Multistep Form -->

	<!-- Progress Bar -->
	<ul id="progressbar">
		<li class="selected-step step1" data-div="first">Download Type</li>
		<li class="step2" data-div="second">Choose Filters</li>
		<li class="step3" data-div="third">Choose Protocol</li>
		<li class="step4" data-div="fourth">Choose Variables</li>
		<li class="step5" data-div="fifth">Review and Download Data</li>
	</ul>
	<!-- Fieldsets -->
	<div class="detailed-form">

		<div class="selection-sec" id="first">


			<div class="btn-sec">
				<a href="javascript:void(0);"
					class="direct-download download-btn btn-solid btn-green">Download
					ALL Data</a><span class="mrg-lr">OR</span><input
					class=" next_btn btn-solid btn-orange" name="next" type="button"
					value="2. Choose Filters">
			</div>

			<div class="review-data-sec">
				<h4 class="review-title"><cms:area name="dadownload_table_title" /></h4>
				<div class="selected-data">
					<cms:area name="dadownload_table_content" />
				</div>
			</div>
		</div>
		<div class="selection-sec" id="second">
			<h3 class="steps">Step 2 of 5 : Choose Filters</h3>
			<div class="filter-sec">
				<span class="toggle-bar"></span> <span class="choose-toggle-link">Choose
					Filters:</span> <span class="clear-all">Clear All</span>
			</div>
			<div class="choose-filters">
				<div class="select-cat" id="site-rotation-checkbox">
					<div class="selection-bar">
						<span class="s-title" class="data-hover" data-toggle="popover"
							data-trigger="hover" data-placement="right"
							data-content="Some text here">SITE VISITS</span>
						
						<span class="clear-selected">Clear</span>
						<div class="alert-area">
							<div class="infotip"></div>
							<span>Please select Survey Period and Data Type, or
								Repeated Surveys</span>
						</div>
					</div>
					<div class="select-from">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<ul class="tree">
										<li class="has-child"><span class="toggle-bar"></span> <input
											id="SurveyPeriod" type="checkbox" name="rotation"
											class="excludeFromSummary" value="Survey Period" /> <label
											for="SurveyPeriod" class="data-hover" data-toggle="popover"
											data-trigger="hover" data-placement="right"
											data-content="Some text here">Survey Period</label>
											<ul class="sub-selection" id="survey-periond-detail">

											</ul></li>
									</ul>
								</div>


								<div class="col-md-6">
									<ul class="tree">
										<li class="has-child"><span class="toggle-bar"></span> <input
											id="datatype" type="checkbox" name="data-type1"
											class="excludeFromSummary" value="Data Type" /> <label
											for="datatype" class="data-hover" data-toggle="popover"
											data-trigger="hover" data-placement="right"
											data-content="Some text here">Data Type</label>
											<ul class="sub-selection" id="data-type-detail1">

											</ul></li>
									</ul>
								</div>
							</div>


							<div class="col-md-12">
								<ul class="tree">
									<li class="has-child"><span class="toggle-bar"></span> <input
										id="Repeat-Surveys" type="checkbox" name="data-type2"
										class="excludeFromSummary" value="Repeat Surveys" /> <label
										for="Repeat-Surveys" class="data-hover" data-toggle="popover"
										data-trigger="hover" data-placement="right"
										data-content="Some text here">Repeat Surveys</label>
										<ul class="sub-selection" id="data-type-detail2">

										</ul></li>
								</ul>
							</div>

						</div>
					</div>
					<!--  end of form -->

				</div>
				<!--  end of select-cat -->
				<div class="select-cat" id="region-checkbox">
					<div class="selection-bar">
						<span class="s-title" class="data-hover" data-toggle="popover"
							data-trigger="hover" data-placement="right"
							data-content="Some text here">REGION</span>
						
						<span class="clear-selected">Clear</span>
						<div class="alert-area">
							<div class="infotip"></div>
							<span>Please select one or more region</span>
						</div>
					</div>
					<div class="select-from">
						<ul class="tree" id="regiondetail">
							<li class="level1list"><input id="region" type="checkbox"
								name="region" value="region" checked /> <label for="region">All
									Alberta</label></li>

						</ul>
					</div>
				</div>
				<!--  end of select-cat -->

			</div>
			<div class="btn-sec">
				<input class="pre_btn btn-solid btn-orange flt-left" name="previous"
					type="button" value="<   Previous"> <input
					id="step2-btn-next" class="next_btn btn-solid btn-blue flt-right"
					name="next" type="button" value="3. Choose Protocol  >">
			</div>
		</div>


		<div class="selection-sec" id="third">
			<h3 class="steps">Step 3 of 5 : Choose Protocol</h3>
			<div class="filter-sec">
				<span class="toggle-bar"></span> <span class="choose-toggle-link">Choose
					Protocol:</span> <span class="clear-all">Clear All</span>
			</div>
			<div id="step3-filter" class="choose-filters">
				<div class="select-cat">
					<div class="selection-bar">
						<span class="s-title" class="data-hover" data-toggle="popover"
							data-trigger="hover" data-placement="right"
							data-content="Some text here">PROTOCOL</span>
						<div class="alert-area">
							<div class="infotip"></div>
							<span>There are incomplete required fields. Please
								complete them.</span>
						</div>
					</div>
					<div class="select-from">
						<ul class="tree">
							<li><input id="Terrestrial" type="checkbox" name="check"
								value="Terrestrial" /> <label for="Terrestrial"
								class="data-hover" data-toggle="popover" data-trigger="hover"
								data-placement="right" data-content="Some text here">Terrestrial</label>
							</li>
							<li><input id="Wetland" type="checkbox" name="check"
								value="Wetland" /> <label for="Wetland" class="data-hover"
								data-toggle="popover" data-trigger="hover"
								data-placement="right" data-content="Some text here">Wetland</label></li>
						</ul>

					</div>
				</div>
			</div>



			<div class="btn-sec">
				<input class="pre_btn btn-solid btn-orange flt-left" type="button"
					value="<   Previous">
				<div class="flt-right right-btns">
					<input class="next_btn btn-solid btn-blue" name="next"
						type="button" id="step3-btn-next" value="4. Choose Variables   >">
					<div class="mrg-lr" style="text-align: center; margin-bottom: 7px;">OR</div>
					<a href="javascript:void(0);" id="step3-direct-download"
						class="direct-download download-btn btn-solid btn-green">5.
						Review & Download Data</a>
				</div>
			</div>
		</div>
		<div class="selection-sec" id="fourth">
			<h3 class="steps">Step 4 of 5 : Choose Variables</h3>
			<div class="filter-sec">
				<span class="toggle-bar"></span> <span class="choose-toggle-link">Choose
					Variables:</span> <span class="clear-all">Clear All</span>
			</div>
			<div id="step4-filter" class="choose-filters">
				<div class="alert-area">
				<div class="infotip"></div>
				<span>Please one or more variables.</span>
			</div>
			
				<div class="terrestrial select-cat">
					<div class="selection-bar">
						<span class="s-title" class="data-hover" data-toggle="popover"
							data-trigger="hover" data-placement="right"
							data-content="Some text here">Terrestrial Veriables</span> <span
							class="clear-selected">Clear</span>
					</div>
					<div class=" select-from">
						<div class="row">
							<div class="species col-md-6">
								<ul class="tree">
									<li class="has-child"><span class="toggle-bar"></span> <input
										id="SpeciesVariables" type="checkbox"
										name="terrestrial-species" class="excludeFromSummary"
										value="Species Variables" /> <label for="SpeciesVariables"
										class="data-hover" data-toggle="popover" data-trigger="hover"
										data-placement="right" data-content="Some text here">Species
											Variables</label>
										<ul class="sub-selection">
										</ul></li>
								</ul>
							</div>
							<div class="habitat col-md-6">
								<ul class="tree">
									<li class="has-child"><span class="toggle-bar"></span> <input
										id="HabitatVariables" type="checkbox"
										name="terrestrial-habitat" class="excludeFromSummary"
										value="Habitat Variables" /> <label for="HabitatVariables"
										class="data-hover" data-toggle="popover" data-trigger="hover"
										data-placement="right" data-content="Some text here">Habitat
											Variables</label>
										<ul class="sub-selection">

										</ul></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="wetland select-cat">
					<div class="selection-bar">
						<span class="s-title" class="data-hover" data-toggle="popover"
							data-trigger="hover" data-placement="right"
							data-content="Some text here">WETLAND VARIABLES</span> <span
							class="clear-selected">Clear</span>
					</div>
					<div class=" select-from">
						<div class="row">
							<div class="species col-md-6">
								<ul class="tree">
									<li class="has-child"><span class="toggle-bar"></span> <input
										id="SpeciesVariables1" type="checkbox" name="wetland-species"
										class="excludeFromSummary" value="Species Variables1" /> <label
										for="SpeciesVariables1" class="data-hover"
										data-toggle="popover" data-trigger="hover"
										data-placement="right" data-content="Some text here">Species
											Variables</label>
										<ul class="sub-selection">

										</ul></li>
								</ul>
							</div>
							<div class="habitat col-md-6">
								<ul class="tree">
									<li class="has-child"><span class="toggle-bar"></span> <input
										id="HabitatVariables1" type="checkbox" name="wetland-habitat"
										class="excludeFromSummary" value="Habitat Variables1" /> <label
										for="HabitatVariables1" class="data-hover"
										data-toggle="popover" data-trigger="hover"
										data-placement="right" data-content="Some text here">Habitat
											Variables</label>
										<ul class="sub-selection">

										</ul></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>

		
			<div class="btn-sec">
				<input class="pre_btn btn-solid btn-orange flt-left" type="button"
					value="<   Previous"> <input
					class="next_btn btn-solid btn-blue flt-right" name="next"
					id="step4-btn-next" type="button" value="5. Review & Download   >">
			</div>
		</div>
		<div class="selection-sec" id="fifth">
			<h3 class="steps">Step 5 of 5 : Review and Download Data</h3>
			<div class="review-data-sec">
				<h4 class="review-title">Review Your Selection:</h4>
				<div class="selected-data" id="data-summary-details">
					<div class="col-sm-4">
						<h4>Filters:</h4>
						<div id="region-summary">
							<span class="selection-title">Region:</span>
							<div class="data-list">
								<ul>
									<li>Alberta</li>
								</ul>
							</div>
						</div>
						<div id="rotation-summary">
							<span class="selection-title">Survey Period:</span>
							<div class="data-list">
								<ul>

								</ul>
							</div>
						</div>
						<div id="data-type1-summary">
							<span class="selection-title">Data Type:</span>
							<div class="data-list">
								<ul>
								</ul>
							</div>
						</div>
						<div id="data-type2-summary">
							<span class="selection-title">Repeat Surveys:</span>
							<div class="data-list">
								<ul>
								</ul>
							</div>
						</div>

					</div>
					<div class="col-sm-4 mid-sec">
						<h4>Species:</h4>
						<div id="terrestrial-species-summary">
							<span class="selection-title">Terrestrial:</span>
							<div class="data-list">
								<ul>

								</ul>
							</div>
						</div>
						<div id="wetland-species-summary">
							<span class="selection-title">Wetlands:</span>
							<div class="data-list">
								<ul>

								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<h4>Habitat:</h4>
						<div id="terrestrial-habitat-summary">
							<span class="selection-title">Terrestrial:</span>
							<div class="data-list">
								<ul>

								</ul>
							</div>
						</div>
						<div id="wetland-habitat-summary">
							<span class="selection-title">Wetlands:</span>
							<div class="data-list">
								<ul>

								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
			<h3 class="txtcenter mrgBnone" id="download-msg"></h3>
			<div class="btn-sec">
				<input class="pre_btn btn-solid btn-orange" type="button"
					value="<   Previous"><span class="mrg-lr">OR</span><a
					id="final-data-download" href=""
					class="progress-button download-btn btn-solid btn-green"
					data-loading="Downloading Selected Data"
					data-finished="Download completed!" data-type="background-bar">Download
					Selected Data<span class="tz-bar background-bar"
					style="display: none; width: 0%;"></span><a href="#" class="restart btn-solid">Restart</a>
				</a>


			</div>			
			

		</div>
	</div>
	<!--  end of detailed form -->
</div>


