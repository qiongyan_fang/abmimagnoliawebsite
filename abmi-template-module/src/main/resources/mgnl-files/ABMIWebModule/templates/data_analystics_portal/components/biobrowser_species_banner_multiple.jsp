<%@ include file="../../includes/taglibs.jsp"%>
<!--  this page  displays (left image) middle text with species watermarks. 
used in species list page.
-->
<c:forEach var="row" items="${banner}">

	<c:if test="${row.speciesGroup == param.groupId}">
		<c:set var="imagerowMap" value="${cmsfn:content(row.imageurl,'dam')}" />
		<c:set var="imageUrl"
			value="${cmsfn:link(cmsfn:asContentMap(imagerowMap))}" />

		<div class="row ms-banner sp-main-top watermark-${param.groupId}">


			<div class="banner-img col-md-4">
				<img src="${imageUrl}" alt="" />
			</div>
			<div class=" col-md-8 msbanner-txt">
				<h1>${row.header}</h1>
				<h3>${row.header2 }  <c:if test="${row.includeCounter}"><span id="spp-count" class="counter"></span></c:if></h3>
				<p>${row.description }</p>
			</div>
		</div>

	</c:if>
</c:forEach>