<%@ include file="../../includes/taglibs.jsp"%>
<!--  detailed production header: with a box header including images, color background, description on how to download. and description outside of the box -->
<c:set var="imageContentMap"
	value="${cmsfn:content(content.image,'dam')}" />
<c:set var="image"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />

<c:set var="title1"
	value="${state.mainContentNode.getProperty('title').getString()}" />


<div
	class="col-md-6 single-data ${empty content.backgroundColor?'orange':content.backgroundColor}-download-bg">
	<h3>${empty content.customHeader?title1:content.customHeader}</h3>
	<div class="post-data">

		<c:if test="${content.productCount > 0}">
			<span class="post-file white"></span>
			<span>${content.productCount} DATA SET${content.productCount > 1?"S":""}</span>
		</c:if>
	</div>
	<p>${content.description }</p>
</div>
<div class="col-md-6 nopadding single-data-img">
	<img src="${image}" alt="" />
</div>


<c:if test="${(empty content.bHideDownload or !content.bHideDownload) || not empty content.embed}">
	<div class="col-xs-12 data-ftpData">
		<c:if test="${empty content.bHideDownload or !content.bHideDownload}">
			<!--  display download options -->
			<img src="/docroot/da/assets/${content.fileicon}-white-icon.png"
				alt="Download using ${content.fileicon}">
			<div class="ftp-details">${cmsfn:decode(content).downloadDescription}</div>
		</c:if>
		<c:if test="${not empty content.embed}">
			<p>${cmsfn:decode(content).embed }</p>
		</c:if>
	</div>
</c:if>

<div class="col-xs-12">

	<cms:area name="Header" />


	<div>
		<cms:area name="Description" />
	</div>
	<div class="clearboth"></div>
	<div class="databy">
		<cms:area name="downloadproducts" />
	</div>
</div>