<%@ include file="../../includes/taglibs.jsp"%>
<c:set var="groupId">${param.groupId}</c:set>
<!--  this component  displays current selection of species groups, and a drop down graph menus to switch to a different species group. 
used in species list page.
-->
<script>
	var gSpeciesGroupId = "${groupId}";
	var gDetailedUrlParam = "${content.speciesDetailUrl}";
</script>

<div class="row secondarybar">
	<div class="container">
		<div class="row col-md-7">
			<div class="totalNumbers">
				<h4>
					<span id="sppGroupName"></span> <span id="sppCount"></span>
				</h4>
			</div>
			<div class="slectGroup">Choose Species Group</div>
		</div>

		<div class="search hidden-sm hidden-xs">
			<div class="push-right">
				<input type="text" placeholder="Search" id="searchSpecies" name="q">
				<input type="submit" class="btn" id="searchSpeciesBtn" value="">
			</div>
		</div>
	</div>
</div>

<!--Choose Species Group-->
<div class="row species-groupList">
	<div class="container">
		<div class="row">
			<c:forEach var="sppGroupName" items="${speciesGroups}"
				varStatus="myIndex">
				<c:if test="${sppGroupName != 'Aquatic Invertabrates'}">
					<c:set var="imageUrl" value="" />
					<c:forEach var="iconRow" items="${speciesIcons}">

						<c:if test="${iconRow.speciesGroup == (myIndex.index+1)}">
							<c:set var="imagerowMap"
								value="${cmsfn:content(iconRow.imageurl,'dam')}" />
							<c:set var="imageUrl"
								value="${cmsfn:link(cmsfn:asContentMap(imagerowMap))}" />
						</c:if>
					</c:forEach>

					<c:if test="${(myIndex.index+1) != groupId}">
						<!--  only show other groups not current groups -->


						<div class="col-sm-2 col-xs-4">
							<a href="?groupId=${myIndex.index+1 }" class="species-item">
								<div class="speciesImg-block">
									<c:if test="${empty imageUrl}">
										<c:set var="imageUrl"
											value="/docroot/da/assets/${fn:toLowerCase(fn:replace(sppGroupName,' ','-'))}-sm.jpg" />
									</c:if>
									<img src="${imageUrl}" alt="${sppGroupName }" />
								</div> <span>${sppGroupName}</span>
							</a>
						</div>
					</c:if>
				</c:if>
			</c:forEach>

		</div>
	</div>
</div>

<!--Data Filter/ Sort-->
<div class="row data-section">
	<div class="container">
		
		<div class="dataWrapper clearboth">
			<div class="data-block" id="data-block">

				<table id="species-data">
					<thead>
						<tr>

							<th class="white-col"></th>
							<th class="summary-col"><div class="sort-title">
									Summary<span class="static-sorting sorting_desc"></span>
								</div>
								<div class="filtering" style="width: 100%;">
									<span class="all-selected active">ALL</span><span
										class="data-hover" data-toggle="popover" data-trigger="hover"
										data-placement="auto" data-container="body"
										data-content="<p><img src='/docroot/da/assets/map-Full.png'><span>Full:</span> The ABMI has detected the species enough times in the northern analysis unit (forested region) and the southern analysis unit (prairie region) to model how species relative abundance differed among vegetation, soil and, human footprint types for both these regions.</p>">Full</span>
									<span class="data-hover" data-toggle="popover"
										data-trigger="hover" data-placement="auto"
										data-container="body"
										data-content="<p><img src='/docroot/da/assets/map-Forested.png'>&nbsp;<span	>Forested Region:</span>The ABMI has detected the species enough times in the northern analysis unit (i.e. forested region) to model how species relative abundance differed among vegetation and human footprint types for the forested region only. A coarse index of habitat use may be available for prairie region.">Forested</span><span
										class="data-hover" data-toggle="popover" data-trigger="hover"
										data-placement="auto" data-container="body"
										data-content="<p><img src='/docroot/da/assets/map-Prairie.png'><span>Prairie Region:</span> The ABMI has detected the species enough times in the southern analysis unit (i.e. prairie region) to model how species relative abundance differed among soil types and human footprint types for the prairie region only. A coarse index of habitat use may be available for the forested region.">Prairie</span><span class="data-hover" data-toggle="popover"
										data-trigger="hover" data-placement="auto"
										data-container="body"
										data-content="<p><img src='/docroot/da/assets/map-Basic.png'><span>Basic Region:</span> For species with few detections in both the northern analysis unit (forested region) and southern analysis unit (prairie region), a coarse index of habitat use may be presented along with the species occurrence map.">Basic</span>
								</div></th>
							<th class="common-name-col a-z"><div class="sort-title">
									Common Name<span class="static-sorting sorting_asc"></span>
								</div>
								<div class="filtering" style="width: 70%;">
									<span class="all-selected active">ALL</span><span class="">A</span><span>B</span><span>C</span><span>D</span><span>E</span><span>F</span><span>G</span><span>H</span><span>I</span><span>J</span><span>K</span><span>L</span><span>M</span><span>N</span><span>O</span><span>P</span><span>Q</span><span>R</span><span>S</span><span>T</span><span>U</span><span>V</span><span>W</span><span>X</span><span>Y</span><span>Z</span>
								</div></th>
							<th class="scientific-name-col a-z"><div class="sort-title">
									Scientific Name<span class="static-sorting sorting_asc"></span>
								</div>
								<div class="filtering" style="width: 70%;">
									<span class="all-selected active">ALL</span><span class="">A</span><span>B</span><span>C</span><span>D</span><span>E</span><span>F</span><span>G</span><span>H</span><span>I</span><span>J</span><span>K</span><span>L</span><span>M</span><span>N</span><span>O</span><span>P</span><span>Q</span><span>R</span><span>S</span><span>T</span><span>U</span><span>V</span><span>W</span><span>X</span><span>Y</span><span>Z</span>
								</div></th>
						</tr>
					</thead>
					<tbody id="dataBody">
					</tbody>
				</table>
			</div>


		</div>
	</div>
</div>
