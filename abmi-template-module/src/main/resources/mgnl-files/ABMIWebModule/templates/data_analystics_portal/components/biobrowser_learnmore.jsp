<%@ include file="../../includes/taglibs.jsp"%>

<!--  learn more box: a wide box with text, link -->
<div class="col-md-6 sec">
	<h2>${content.headerText}</h2>
	<p>${cmsfn:decode(content).description}</p>
	<c:if test="${not empty content.buttonText}">
		<a class="line-btn blue-btn"
			${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""}
			href="${content.buttonUrl}">${content.buttonText}</a>
	</c:if>
</div>