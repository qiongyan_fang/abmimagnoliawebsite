<%@ include file="../../includes/taglibs.jsp"%>
<!--  species browser home page, the main species groups box. this is a single species. the rest layout are predefined. -->
<c:set var="imageContentMap"
	value="${cmsfn:content(content.backgroundImageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />

<div
	style="background-image:url(${imageUrl});background-repeat: no-repeat; background-size: cover; width:100%;height:100%;">
	<a ${fn:startsWith(listUrl, "http")?"target=_blank":""}
		href="${listUrl}?groupId=${content.speciesGroup}">
		<div class="overlay-box">
			<div class="text-wrapper">
				<div class="tab-cell">
					<h3>${content.headerText}</h3>
					
					<!--  show counter when there is button text -->
					<c:if test="${not empty content.buttonText}">
					<h4>(<span id="spp-count-${content.speciesGroup}"></span> ${empty content.headerText2?"Species Detected":content.headerText2})</h4>
						<span class="line-btn btn-white">${content.buttonText}</span>
					</c:if>
					<!--  hide counter and button text -->
					<c:if test="${empty content.buttonText}">
					<h4>(${empty content.headerText2?"Species Detected":content.headerText2})</h4>
					
					</c:if>
				</div>
			</div>
		</div>
	</a>
</div>