<%@ include file="../../includes/taglibs.jsp"%>
<!--  right side menu. with icon next to the top header. otherwise same as the main website menu -->
<c:set var="iconContentMap"
	value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="icon"
	value="${cmsfn:link(cmsfn:asContentMap(iconContentMap))}" />

<c:if test="${content.showparent=='true'}">
<c:set var="uri" value="${state.mainContentNode.parent.path}" />
</c:if>
<c:if test="${not content.showparent=='true'}">
<c:set var="uri" value="${empty currentLink?state.handle:currentLink}" />
</c:if>


<div class="sidemenu widget">
	<div class="module-title">
		<c:if test="${not empty icon}">
			<img src="${icon}" alt="" />
		</c:if>
		<h4>${content.header} </h4>
	</div>
	<ul>

<c:forEach items="${subpage}" var="row">

		<c:if test="${row.path eq uri}">
			<li class="active"><a href="${pageContext.request.contextPath}${row.path}.html?scroll=true">${row.title} </a></li>
		</c:if>
		<c:if test="${row.path ne uri}">
			<li><a href="${pageContext.request.contextPath}${row.path}.html?scroll=true">${row.title}</a></li>
		</c:if>
		<c:if test="${not empty row.childrenpage}">
			<ul class="submenu">
				<c:forEach items="${row.childrenpage}" var="subrow">
					<c:if test="${subrow.path eq uri}">
						<li class="active"><a
							href="${pageContext.request.contextPath}${subrow.path}.html?scroll=true">${subrow.title}
						</a></li>
					</c:if>
					<c:if test="${subrow.path ne uri}">
						<li><a
							href="${pageContext.request.contextPath}${subrow.path}.html?scroll=true">${subrow.title}</a></li>
					</c:if>
				</c:forEach>
			</ul>
		</c:if>

	</c:forEach>
	</ul>
</div>