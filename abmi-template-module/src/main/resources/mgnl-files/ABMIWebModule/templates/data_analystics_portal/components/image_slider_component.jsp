<%@ include file="../../includes/taglibs.jsp"%>

<div class="abmi_slider">
<link href="/docroot/da/css/jquery.bxslider.css" rel="stylesheet" />
<ul class="large_image_slider${cmsfn:isEditMode()?'-x':''} ${content.onRight?'slider-text-right':''}">
	<cms:area name="SingleImages" />
</ul>
</div>
