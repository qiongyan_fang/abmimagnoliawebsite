<%@ include file="../../includes/taglibs.jsp"%>
<!--  customizable banner: with left side image, right side text (selectable background color + watermark) -->
<c:set var="imageContentMap"
	value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />

<c:set var="bgImageContentMap"
	value="${cmsfn:content(content.backgroundImageUrl,'dam')}" />
<c:set var="bgImageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(bgImageContentMap))}" />

<c:if test="${not empty bgImageUrl}">
	<div class="row ms-banner sp-main-top ${content.backgroundColor}-bg"
		style="background-repeat:no-repeat; background-position:bottom right; background-image: url('${bgImageUrl}');">
</c:if>
<c:if test="${ empty bgImageUrl}">
	<div class="row ms-banner sp-main-top">
</c:if>

<div class="banner-img col-md-4">
	<img src="${imageUrl}" alt="" />
</div>
<div class=" col-md-8 msbanner-txt">
	<h1>${content.header}</h1>
	<h3>${cmsfn:decode(content).header2 }</h3>
	<p>${cmsfn:decode(content).description }</p>


	
	<c:if test="${not empty content.reportPath}">
	<a class="download-report" download	href="/FileDownloadServlet?dir=WEB_GRAPH&filename=/report/${content.reportPath}">Download Report</a>
	</c:if>
</div>
</div>