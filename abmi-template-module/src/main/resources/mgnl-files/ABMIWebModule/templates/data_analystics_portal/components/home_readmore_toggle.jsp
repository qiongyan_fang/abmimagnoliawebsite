<%@ include file="../../includes/taglibs.jsp"%>
<!--  read more toggle box. a header with collapse-able content. 
e.g. species detail pages -->
 <div style="clear:both;"></div>
<div class="togglelink">
	<span class="readmoreTxt">${content.headerText }</span>
	<div class="togglesec" style="display: none;">
		${cmsfn:decode(content).description }
		<cms:area name="collapsible_area" />
	</div>
</div>
<div style="clear:both;"></div>