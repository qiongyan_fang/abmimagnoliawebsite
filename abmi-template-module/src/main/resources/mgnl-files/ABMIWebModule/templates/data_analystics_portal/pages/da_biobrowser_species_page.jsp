<%@ include file="../../includes/taglibs.jsp"%>
<%@ include file="../includes/da_header.jsp"%>

<body class="container-fluid data-analytics bio-browser-page">
	<%@ include file="../includes/da_top_menu.jsp"%>
	 <cms:area name="bannerArea" />
	
		<cms:area name="leftColumnArea" />
	
	<%@ include file="../includes/da_footer.jsp"%>
</body>
</html>