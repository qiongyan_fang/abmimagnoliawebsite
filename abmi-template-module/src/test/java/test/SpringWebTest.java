package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import abmi.model.services.ProjectPropertyService;
import abmi.model.services.RegionsService;
import abmi.model.services.rawdata.CacheRawdataService;
import abmi.model.services.rawdata.DataCategoryService;
import abmi.model.services.rawdata.DownloadPackageGenerator;
import abmi.model.services.rawdata.RawdataQueryCondition;

import abmi.model.util.ModuleParameters;

import org.apache.commons.io.FileUtils;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-config.xml")
//@ImportResource({"classpath:/applicationContext.xml", "classpath:/blossom-servlet.xml"})
//@WebAppConfiguration

/**
 * Note: before run this, please comment out @session annotation from SingleCSVFileCreator.java and DownloadPackageGenerator.java
 * after finish, uncomment them.
 * @author Qiongyan
 *
 */
public class SpringWebTest {
	
	 
	
	@PersistenceContext(unitName="data")
	private EntityManager em;
	@Autowired DataCategoryService dataDao;
	@Autowired
	RegionsService regionDao;
	@Autowired ProjectPropertyService prjDao;
	
	@Autowired
	CacheRawdataService cacheRawdataService;
	
	@Autowired
	DownloadPackageGenerator zipFileDAO;
	 @BeforeClass
	    public static void setUpClass() throws Exception {
		
	    }

	    @AfterClass
	    public static void tearDownClass() throws Exception {
	    }

	    @Before
	    public void setUp() throws Exception {
	    }

	    @After
	    public void tearDown() throws Exception {
	    }

//	    @Test
	    // create a packaged zip file
	    public void generateSomeFiles() throws Exception{
	    	
	    	if (em == null) {
	    		System.out.println("em is null");
				em = (EntityManager) Persistence.createEntityManagerFactory(
						ModuleParameters.DATA_ENTITY_MANAGER).createEntityManager();
			}
	    	
	    	EntityManager	tempEM = (EntityManager) Persistence.createEntityManagerFactory(
	    			ModuleParameters.DATA_ENTITY_MANAGER).createEntityManager();
	    	tempEM.getTransaction().begin();
	    	tempEM.getTransaction().commit();
	    	
//	    	CacheRawdataService cacheRawdataService = new CacheRawdataService();
//	    	cacheRawdataService.setEm(em);
//	    	cacheRawdataService.setDataCategoryService(dataDao);
//	    	cacheRawdataService.setProjectPropertyService(prjDao);
//	    	cacheRawdataService.setRegionsService(regionDao);
//	    	cacheRawdataService.testsaveSimple();
	    	RawdataQueryCondition queryCondition = new RawdataQueryCondition();
	    	queryCondition.setProjectPropertyService(this.prjDao); // autowired not working on "new" object
	    	queryCondition.setDataCategoryService(this.dataDao);
	    
	    	queryCondition.setRegionsService(this.regionDao);
//	    	DownloadPackageGenerator downloadPackage = new DownloadPackageGenerator(
//					em);
	    	ArrayList<Integer> regionIDList = new ArrayList<Integer>();
	    	regionIDList.add(1);
	    
//	    	
	    	ArrayList<Integer> rotations = new ArrayList<Integer>();
	    	rotations.add(2);
	    
	    	
	    	ArrayList<Integer> rawdata = new ArrayList<Integer>();
	    	/* terrestrial  *
	    	rawdata.add(	1	);
	    	rawdata.add(	101	);
	    	rawdata.add(	3	);
	    	rawdata.add(	4	);
	    	rawdata.add(	2	);
	    	rawdata.add(	102	);
	    	rawdata.add(	103	);
	    	rawdata.add(	5	);
	    	rawdata.add(	6	);
	    	rawdata.add(	106	);
	    	rawdata.add(	105	);
	    	rawdata.add(	104	);*/

	    /* all wetland*/	
	    	rawdata.add(	12	);
	    	rawdata.add(	11	);
	    	rawdata.add(	110	);
	    	rawdata.add(	10	);
	    	rawdata.add(	9	);
	    	rawdata.add(	7	);
	    	rawdata.add(	8	);
 
	    

	    
	    	ArrayList<String> mSiteList = new ArrayList<String>();
	    
	    	/* 2015 terrestrial 
	    	mSiteList.add("273");
	    	mSiteList.add("303");
	    	mSiteList.add("355");
	    	mSiteList.add("573");
	    	mSiteList.add("680");
	    	mSiteList.add("710");
	    	mSiteList.add("1180");
	    	mSiteList.add("1631");
	    */
	    	
	    	/*
	    	// wetland sites;
	    	 */

	    	mSiteList.add("1471");
	    	mSiteList.add("303");
	    	mSiteList.add("710");
	    	mSiteList.add("1297");
	    
	    	
	    	
			queryCondition.setmRotationList(rotations);
			queryCondition.setOffgridId(3);
			queryCondition.setmRegionIds(null);
			queryCondition.setbQuerySite(true);
//			queryCondition.setmSiteList(mSiteList, true);
			for (String singleSite:mSiteList) {
				ArrayList<String> siteList = new ArrayList<String>();
				siteList.add(singleSite);
			queryCondition.setmSiteList(siteList, true);
//			zipFileDAO = new  DownloadPackageGenerator(em);
			
				try {
					zipFileDAO.setGeneralQuery(queryCondition,
							rawdata, null);
					
					File zipFile = zipFileDAO.createZipFile();
					
					File tmpZip = new File("C:\\Users\\Qiongyan\\AppData\\Local\\Temp\\TerrestrialSite_" + singleSite
							+ "_ABMIGuide.zip");
					
					
					 if(zipFile.renameTo(tmpZip)){
				            System.out.println("File renamed");
				        }else{
				            System.out.println("Sorry! the file can't be renamed");
				            try {
				                FileUtils.moveFile(zipFile, tmpZip);
				              } catch (IOException e) {
				                e.printStackTrace();
				              }
				        }
					 
				
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
/*
	    	ArrayList<Integer> regionIDList = new ArrayList<Integer>();
//	    	regionIDList.add(1);
//	    	regionIDList.add(2);
//	    	
	    	ArrayList<Integer> rotations = new ArrayList<Integer>();
//	    	rotations.add(1);
//	    	rotations.add(2);
	    	
	    	ArrayList<String> siteList = new ArrayList<String>();
	    	
	    	siteList.add("83");
//	    	siteList.add("19");
//	    	siteList.add("20");
//	    	siteList.add("84");
//	    	siteList.add("111");
//	    	siteList.add("112");
//	    	siteList.add("113");
//	    	siteList.add("269");
	    	
	    	

	    	

	    	for (String site:siteList){
	    		ArrayList<String> singleSiteList = new ArrayList<String>();
	    		singleSiteList.add(site);
		    	
	    	RawdataQueryCondition queryCondition = new RawdataQueryCondition();
	    	queryCondition.setProjectPropertyService(this.prjDao); // autowired not working on "new" object
	    	queryCondition.setDataCategoryService(dataDao);
//	    
	    	queryCondition.setRegionsService(regionDao);
//			queryCondition.setmRotationList(rotations);
			queryCondition.setOffgridId(3);
//			queryCondition.setmRegionIds(regionIDList);
			queryCondition.setmSiteList(singleSiteList, true);
			
//			queryCondition.setbQuerySite(true);
//			System.out.println("rotation = " + queryCondition.getmRotationNameList());
			
			ArrayList<Integer> raw = new ArrayList<Integer>(); 
		
			raw.add(1);
			raw.add(2);
			raw.add(3);
			raw.add(4);
			raw.add(101);
			raw.add(102);
			raw.add(103);
			raw.add(109);
			raw.add(7);
			raw.add(8);
			raw.add(9);

			raw.add(5);
			raw.add(6);
			raw.add(104);
			raw.add(105);
			raw.add(106);
			raw.add(10);
			raw.add(11);
			raw.add(12);
			raw.add(110);

			
			
	    	DownloadPackageGenerator	downloadPackage = new DownloadPackageGenerator(em);
	     	downloadPackage.setGeneralQuery(queryCondition, raw, null);
	     	
        	File zipFile = downloadPackage.createZipFile();
	    	}
 */
	    }
	    
	    @Test
	    /**
	     * used create cache files 
	     * 
	     * 
	     * @throws Exception
	     */
	    @Transactional(value="myTxManager") 
	    public void generateAllFiles() throws Exception{
	    	
	    	if (em == null) {
				em = (EntityManager) Persistence.createEntityManagerFactory(
						ModuleParameters.DATA_ENTITY_MANAGER).createEntityManager();
			}
	    	
	    
	    	cacheRawdataService.setEm(em);
	    
	    	cacheRawdataService.generateCSVFile2();
	    	

	    }
}
