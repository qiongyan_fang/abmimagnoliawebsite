package test;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import abmi.model.services.ProjectPropertyService;
import abmi.model.services.RegionsService;
import abmi.model.services.biobrowser.BioBrowserService;
import abmi.model.services.biobrowser.GraphService;
import abmi.model.services.biobrowser.PdfDownload;

import abmi.model.services.biobrowser.chart.SectorEffectorChart;
import abmi.model.services.biobrowser.species.SpeciesProfileContent;
import abmi.model.services.rawdata.CacheRawdataService;
import abmi.model.services.rawdata.DataCategoryService;
import abmi.model.services.rawdata.DownloadPackageGenerator;
import abmi.model.services.rawdata.RawdataQueryCondition;

import abmi.model.util.ModuleParameters;

import org.apache.commons.io.FileUtils;

import test.abmi.model.util.chart.BarMultiMinMaxChart;
import test.abmi.model.util.chart.ChartBuilder;
import test.abmi.model.util.chart.CombinedCateogryPlotChart;
import test.abmi.model.util.chart.CombinedManyCateogryPlotChart;
import test.abmi.model.util.chart.XYIntervalBarChart;
import test.abmi.model.util.chart.XYLineChart;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/graph-config.xml")
//@ImportResource({"classpath:/applicationContext.xml", "classpath:/blossom-servlet.xml"})
//@WebAppConfiguration

/**
 * Note: before run this, please comment out @session annotation from SingleCSVFileCreator.java and DownloadPackageGenerator.java
 * after finish, uncomment them.
 * @author Qiongyan
 *
 */
public class PDFGeneratorTest {
	
	@Autowired 
	BioBrowserService bioBrowserService;
	
	 @BeforeClass
	    public static void setUpClass() throws Exception {
		
	    }

	    @AfterClass
	    public static void tearDownClass() throws Exception {
	    }

	    @Before
	    public void setUp() throws Exception {
	    }

	    @After
	    public void tearDown() throws Exception {
	    }

	    @Autowired
	    GraphService graphDao;
	    
	    @Test
	       public void testGraph() throws Exception{
	
	    
//	    	ChartBuilder chart = new ChartBuilder();
	    	
//	    	XYIntervalBarChart chart = new XYIntervalBarChart();
//	    	XYLineChart chart = new XYLineChart();
//	    	BarMultiMinMaxChart chart = new BarMultiMinMaxChart();
//	    	CombinedCateogryPlotChart chart = new CombinedCateogryPlotChart();
	    	
//	    	CombinedManyCateogryPlotChart chart = new CombinedManyCateogryPlotChart();
	    	
//	    	
	    	String contentStr="%5B%7B%22details%22:%22%3Cp%3EConversation%20Status:%20%20AEP%20-%20%3Cspan%3ESecure%3C/span%3E%3C/p%3E%5Cn%5Ct%5Ct%5Ct%3C!--%20%5Cn1%5CtFull%5CtFull%20models%5Cn2%5CtFew%5CtFew%20detections%5Cn3%5CtNone%5CtNo%20data%20--%3E%5Cn%5Cn%5Cn%5Ct%5Ct%5Ct%3Cp%3E%5Cn%5Ct%5Ct%5Ct%5CtData%20Summary:%20%3Cspan%3E%20Full%20%20%20%20%5Cn%5Cn%5Cn%5Ct%5Ct%5Ct%5Ct%3C/span%3E%5Cn%5Ct%5Ct%5Ct%5Ct%5Cn%5Cn%5Ct%5Ct%5Ct%3C/p%3E%5Cn%5Ct%5Ct%22%7D,%7B%22description%22:%22%20%3Cp%3EOver%20its%20decade-plus%20operations,%20the%20ABMI%20has%20generated%20a%20comprehensive%20dataset%20on%20Alberta%E2%80%99s%20species,%20their%20habitats,%20as%20well%20as%20the%20extent%20and%20type%20of%20human%20footprint%20across%20the%20province.&nbsp;%3Cspan%20style=%5C%22color:rgb(0,%200,%200)%5C%22%3EWith%20the%20accumulation%20of%20this%20information,%20the%20ABMI%20has%20developed%20analysis%20methods%20to%20predict%20species%20relative%20abundance,%20and%20examine%20species%20responses%20to%20vegetation%20and%20soil%20types,%20and%20human%20footprint%20in%20Alberta.%20These%20methods%20have%20been%20applied%20to%20hundreds%20of%20species;%20this%20profile%20provides%20summary%20results%20for%20one.&nbsp;%3C/span%3E%3C/p%3E%5Cn%22,%22readMore%22:%7B%22Read%20More%22:%22%5Cn%5Ct%5Ct%3Cp%3EThere%20are%20three%20main%20result%20sections%20in%20this%20species%20profile.%20The%20first%20section%20summarizes%20what%20vegetation,%20soil,%20and%20human%20footprint%20types%20the%20species%20uses%20in%20Alberta.%20Next,%20the%20data%20are%20used%20to%20identify%20which%20land%20use%20activities%20have%20the%20biggest%20impact%20(positive%20or%20negative)%20on%20species%20relative%20abundance.%20And%20finally,%20a%20series%20of%20relative%20abundance%20maps%20illustrate%20the%20predicted%20species%20distribution%20under%20current%20and%20reference%20conditions,%20and%20where%20it%E2%80%99s%20expected%20to%20have%20increased%20or%20decreased%20as%20a%20result%20of%20human-caused%20changes%20to%20its%20habitat.&nbsp;%3C/p%3E%5Cn%5Cn%3Cp%3EThe%20target%20audiences%20for%20species%20profiles%20are%20resource%20managers%20in%20Alberta.%20Summary%20of%20data%20can%20be%20used%20to%20support%20land%20use%20planning%20and%20mitigate%20risk%20of%20development%20on%20a%20particular%20species%20of%20interest.%20While%20developed%20to%20support%20resource%20management,%20these%20species%20profiles%20are%20also%20of%20wider%20interest%20to%20anyone%20wanting%20to%20find%20out%20more%20about%20species%20that%20make%20their%20home%20in%20Alberta,%20what%20habitats%20they%20are%20found%20in,%20and%20how%20our%20land%20use%20affects%20their%20populations.&nbsp;&nbsp;%3C/p%3E%5Cn%5Cn%3Cp%3EPlease%20note%20that%20the%20results%20are%20predictions%20based%20on%20the%20best%20available%20data%20at%20the%20current%20time.%20All%20results%20must%20be%20considered%20with%20caution;%20interpretation%20caveats%20are%20presented%20with%20each%20result.%20As%20with%20any%20statistical%20model,%20our%20confidence%20in%20the%20modeled%20outputs%20will%20increase%20as%20we%20gather%20more%20data%20and%20refine%20our%20models;%20to%20that%20end%20we%20update%20the%20summary%20results%20annually%20based%20on%20new%20data.%20As%20an%20internal%20check,%20for%20species%20with%20additional%20information%20in%20the%20literature,%20we%20examine%20whether%20our%20models%20produce%20ecologically%20meaningful%20results.%20For%20data%20poor%20species,%20our%20predictions%20are%20the%20first%20contribution%20towards%20developing%20an%20understanding%20of%20the%20species%20ecology.&nbsp;%3C/p%3E%5Cn%5Cn%5Ct%5Ct%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%20%20%5Cn%5Cn%5Cn%5Ct%22%7D%7D,%7B%22readMore%22:%7B%7D%7D,%7B%22readMore%22:%7B%7D%7D,%7B%22readMore%22:%7B%7D%7D,%7B%22readMore%22:%7B%22Interpreting%20the%20Graphs%22:%22%5Cn%5Ct%5Ct%3Cp%3EThe%20x-axis%20represents%20the%20per%20cent%20area%20of%20the%20footprint%20within%20each%20industrial%20sector%20for%20a%20region.%20The%20y-axis%20shows%20that%20sector's%20footprint%20per%20unit%20area%20(the%20'unit%20effect')%20which%20is%20an%20index%20of%20how%20strongly%20the%20footprint%20type%20impacts%20a%20given%20species%20combined%20with%20where%20the%20footprint%20occurs.%20The%20effect%20of%20footprint%20is%20highest%20when%20footprint%20occurs%20in%20areas%20of%20high%20quality%20habitat%20for%20a%20species,%20and%20lowest%20when%20footprint%20occurs%20in%20marginal%20habitat.%20Therefore,%20a%20footprint%20which%20covers%20a%20larger%20area%20or%20with%20a%20stronger%20unit%20effect%20will%20result%20in%20a%20greater%20sector%20effect.%20The%20area%20of%20the%20rectangle%20for%20each%20industrial%20sector%20is%20proportionate%20to%20the%20effect%20of%20the%20sector%20on%20the%20species'%20population%20(numbers%20on%20the%20top/bottom%20of%20the%20bars)%20in%20a%20given%20region.%20Absolute%20values%20close%20to%20100%25%20on%20the%20y-axis%20indicate%20predicted%20abundance%20changes%20in%20proportion%20to%20changes%20in%20a%20particular%20footprint%20type%20while%20absolute%20values%20higher%20than%20100%25%20indicate%20disproportionate%20effects.%3C/p%3E%5Cn%5Cn%5Ct%5Ct%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%20%20%5Cn%5Cn%5Cn%5Ct%22%7D%7D,%7B%22readMore%22:%7B%22Interpreting%20the%20Maps%22:%22%5Cn%5Ct%5Ct%5Cn%5Ct%5Ct%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%5Cn%20%20%5Cn%5Cn%5Cn%5Ct%22%7D,%22graph%22:%7B%22description1%22:%22%22,%22description2%22:%22%22%7D%7D,%7B%22section%22:%7B%22data-source%22:%22%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%3Cp%3EInformation%20from%20ABMI%20bird%20point%20counts%20was%20combined%20with%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%5Ctinformation%20from%20other%20organizations%20and%20individuals:%3C/p%3E%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%3Cul%3E%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%3Cli%3EEnvironment%20Canada%20(North%20American%20Breeding%20Bird%20Survey%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%5Ctand%20Joint%20Oil%20Sands%20Monitoring%20programs)%3C/li%3E%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%3Cli%3EEcological%20Monitoring%20Committee%20for%20the%20Lower%20Athabasca%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct(EMCLA)%3C/li%3E%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%3Cli%3EDr.%20Erin%20Bayne%20(University%20of%20Alberta)%3C/li%3E%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct%3C/ul%3E%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Cn%5Ct%5Ct%5Ct%5Ct%22,%22citation%22:%22%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%3Cp%3E%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5CtABMI%20(2016).%20%3Cspan%20class=%5C%22CommonName%5C%22%3EAlder%20Flycatcher%3C/span%3E%20%3Ci%3E%3Cspan%20class=%5C%22ScientificName%5C%22%3EEmpidonax%20alnorum%3C/span%3E%3C/i%3E.%20ABMI%20Website%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5CtURL:%20%3Ca%20id=%5C%22external_link%5C%22%20href=%5C%22http://sc-dev.abmi.ca/development/pages/species/birds/AlderFlycatcher.html%5C%22%3Ehttp://sc-dev.abmi.ca/development/pages/species/birds/AlderFlycatcher.html%3C/a%3E.%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%3C/p%3E%5Cn%5Ct%5Ct%5Ct%5Ct%22%7D%7D,%7B%22readMore%22:%7B%7D%7D,%7B%22readMore%22:%7B%7D%7D%5D";
	    	Gson g = new Gson();
			System.out.println("************************************");
			System.out.println(contentStr);
			System.out.println("************************************");
			System.out.println(URLDecoder.decode(contentStr));
			
			List<Map<String, Object>> profile = (List<Map<String, Object>>) g.fromJson(URLDecoder.decode(contentStr),
					List.class);
			System.out.println("************************************");
			System.out.println(profile);
			System.out.println("************************************");
			System.out.println(profile.get(0));
			Map<String, String> result = new HashMap<String, String>();
			PdfDownload pdfDownload = new PdfDownload ();
			pdfDownload.setRootPath(graphDao.getRootPath());
//			BioBrowserService bioBrowserService = new BioBrowserService();
			result.put("path", pdfDownload.createPDFBackEnd(bioBrowserService.getSppMixedRow(99002622) , bioBrowserService.getSppGraphStatus(99002622), profile));
			
	    	
	    	
	    	
//	    	graphDao.getAllGraphs(99002622, true);
	    	
	    }
}
