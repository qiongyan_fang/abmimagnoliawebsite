package test.abmi.model.util.chart;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Paint;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.swing.JPanel;
import org.jfree.chart.*;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPosition;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.CategoryLabelWidthType;
import org.jfree.chart.axis.ExtendedCategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SubCategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.imagemap.ImageMapUtilities;
import org.jfree.chart.imagemap.StandardURLTagFragmentGenerator;
import org.jfree.chart.imagemap.ToolTipTagFragmentGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.ClusteredXYBarRenderer;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.text.TextBlockAnchor;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.RefineryUtilities;
import org.jfree.ui.TextAnchor;
import org.springframework.mock.web.MockHttpSession;

import abmi.model.util.chart.ext.ABMICategoryItemLabelGenerator;
import abmi.model.util.chart.ext.ABMICategoryAxis;
import abmi.model.util.chart.renderer.ABMIMultiColorBarRenderer;

public class BarMultiMinMaxChart {
	String[] columnKeys = {"Upland Spurce",
			"Pine",
			"Deciduous",
			"Mixedwood",
			"Black Spruce",
			"Larch",
			"Grass",
			"Shrub",
			"Wetland",
			"Cultivated HF",
			"Urban/Industry HF"};

	String[] rowKeys = { "0","10", "20", "40", "60", "80", "100", "120", "140","other" };
	CategoryDataset createDataset() {
		DefaultCategoryDataset category = new DefaultCategoryDataset();
		
//		String[] rowKeys = { "0","0", "0", "0", "0", "0", "0", "0", "0" };
		category.addValue(10d, rowKeys[0], columnKeys[0]);
		category.addValue(10d, rowKeys[1], columnKeys[0]);
		category.addValue(10d, rowKeys[2], columnKeys[0]);
		category.addValue(10d, rowKeys[3], columnKeys[0]);
		category.addValue(10d, rowKeys[4], columnKeys[0]);
		category.addValue(10d, rowKeys[5], columnKeys[0]);
		category.addValue(10d, rowKeys[6], columnKeys[0]);
		category.addValue(10d, rowKeys[7], columnKeys[0]);
		category.addValue(10d, rowKeys[8], columnKeys[0]);

		category.addValue(10d, rowKeys[0], columnKeys[1]);
		category.addValue(10d, rowKeys[1], columnKeys[1]);
		category.addValue(10d, rowKeys[2], columnKeys[1]);
		category.addValue(10d, rowKeys[3], columnKeys[1]);
		category.addValue(10d, rowKeys[4], columnKeys[1]);
		category.addValue(10d, rowKeys[5], columnKeys[1]);
		category.addValue(10d, rowKeys[6], columnKeys[1]);
		category.addValue(10d, rowKeys[7], columnKeys[1]);
		category.addValue(10d, rowKeys[8], columnKeys[1]);

		category.addValue(10d, rowKeys[0], columnKeys[2]);
		category.addValue(10d, rowKeys[1], columnKeys[2]);
		category.addValue(10d, rowKeys[2], columnKeys[2]);
		category.addValue(10d, rowKeys[3], columnKeys[2]);
		category.addValue(10d, rowKeys[4], columnKeys[2]);
		category.addValue(10d, rowKeys[5], columnKeys[2]);
		category.addValue(15d, rowKeys[6], columnKeys[2]);
		category.addValue(10d, rowKeys[7], columnKeys[2]);
		category.addValue(10d, rowKeys[8], columnKeys[2]);

		category.addValue(10d, rowKeys[0], columnKeys[3]);
		category.addValue(10d, rowKeys[1], columnKeys[3]);
		category.addValue(15d, rowKeys[2], columnKeys[3]);
		category.addValue(10d, rowKeys[3], columnKeys[3]);
		category.addValue(10d, rowKeys[4], columnKeys[3]);
		category.addValue(10d, rowKeys[5], columnKeys[3]);
		category.addValue(10d, rowKeys[6], columnKeys[3]);
		category.addValue(10d, rowKeys[7], columnKeys[3]);
		category.addValue(10d, rowKeys[8], columnKeys[3]);

		category.addValue(10d, rowKeys[0], columnKeys[4]);
		category.addValue(10d, rowKeys[1], columnKeys[4]);
		category.addValue(10d, rowKeys[2], columnKeys[4]);
		category.addValue(10d, rowKeys[3], columnKeys[4]);
		category.addValue(10d, rowKeys[4], columnKeys[4]);
		category.addValue(10d, rowKeys[5], columnKeys[4]);
		category.addValue(15d, rowKeys[6], columnKeys[4]);
		category.addValue(10d, rowKeys[7], columnKeys[4]);
		category.addValue(10d, rowKeys[8], columnKeys[4]);

		category.addValue(10d, rowKeys[0], columnKeys[5]);
		category.addValue(10d, rowKeys[1], columnKeys[5]);
		category.addValue(10d, rowKeys[2], columnKeys[5]);
		category.addValue(10d, rowKeys[3], columnKeys[5]);
		category.addValue(10d, rowKeys[4], columnKeys[5]);
		category.addValue(14d, rowKeys[5], columnKeys[5]);
		category.addValue(10d, rowKeys[6], columnKeys[5]);
		category.addValue(10d, rowKeys[7], columnKeys[5]);
		category.addValue(10d, rowKeys[8], columnKeys[5]);
		
	

		return category;
	}
	
	DefaultCategoryDataset getCategory2(){
		DefaultCategoryDataset category2 = new DefaultCategoryDataset();
		category2.addValue(10d,  rowKeys[9], columnKeys[6]);
		
		category2.addValue(10d,  rowKeys[9], columnKeys[7]);
		
		category2.addValue(10d,  rowKeys[9], columnKeys[8]);
		
		category2.addValue(16d,  rowKeys[9], columnKeys[9]);
		
		category2.addValue(14d,  rowKeys[9], columnKeys[10]);
		return category2;
	}

	JFreeChart createChart() {

		JFreeChart jfreechart = // legend
		// tool tips
		// URLs

		ChartFactory.createBarChart("Title", "X label", "",
				this.createDataset(), PlotOrientation.VERTICAL, false, true,
				true);
		ChartFactory.setChartTheme(StandardChartTheme.createLegacyTheme());

		return jfreechart;
	}

	public String exportChart() {

		JFreeChart chart = createChart();
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		chart.setBackgroundPaint(Color.white);

		plot.setBackgroundPaint(Color.lightGray);
		plot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);

		NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
		numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

		ArrayList<String> labels = new ArrayList<String>();

		labels.add("10");
		labels.add("20");
		labels.add("30");
		labels.add("40");
		labels.add("50");
		labels.add("60");
		labels.add("70");
		labels.add("80");
		labels.add("90");
		labels.add("100");

		String[] colorCode = { "#ca5f46", "#163545", "#c47c34", "#c2B1A2",
				"#B2A1C2", "#FFAABB" };

		Paint[] colors = new Paint[colorCode.length];
		for (int i = 0; i < colorCode.length; i++) {
			colors[i] = Color.decode(colorCode[i]);
		}

		ABMIMultiColorBarRenderer renderer = new ABMIMultiColorBarRenderer(
				colors);
		
		
		renderer.setShadowVisible(false);
		plot.setRenderer(renderer);

		// ABMIMultiColorXYBarRenderer renderer = new
		// ABMIMultiColorXYBarRenderer(colors);
		// renderer.setShadowVisible(false);
		// renderer.setBarPainter(new StandardXYBarPainter());
		// plot.setRenderer (renderer);

		plot.setBackgroundPaint(new Color(255, 255, 255));
		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		// NumberAxis yAxis = new NumberAxis(null);
		yAxis.setAutoRangeIncludesZero(true);
		yAxis.setUpperBound(100d);

		plot.setRangeAxis(yAxis);

		// CategoryAxis xAxis = plot.getDomainAxis();
		// xAxis.setVisible(true);
		// xAxis.setMaximumCategoryLabelLines(3);

		 ABMICategoryAxis subcategoryaxis = new ABMICategoryAxis("test group");

		 subcategoryaxis.addSubCategory("0");//10.255");
		 subcategoryaxis.addSubCategory("20");//10.255");
		 subcategoryaxis.addSubCategory(" ");//20.255");
		 subcategoryaxis.addSubCategory("60");//30.255")
		 subcategoryaxis.addSubCategory(" ");//30.255")
		 subcategoryaxis.addSubCategory("100");//30.255");
		 subcategoryaxis.addSubCategory(" ");//30.255");
		 subcategoryaxis.addSubCategory("140");//30.255");
		 
		 subcategoryaxis.setLabelFont(new Font("Arial", Font.PLAIN, 9));
		 subcategoryaxis.setSubLabelFont(new Font("Arial", Font.PLAIN, 6));
		 subcategoryaxis.setCategoryLabelPositionOffset(10);
		 subcategoryaxis.setCategoryMargin(0.2);
		
		 renderer.setItemMargin(0.2); 
		 
		 CategoryLabelPositions axisLabelPosition = new CategoryLabelPositions(
		            new CategoryLabelPosition(
			                RectangleAnchor.BOTTOM, TextBlockAnchor.BOTTOM_LEFT), // TOP
			            new CategoryLabelPosition(
			                RectangleAnchor.LEFT, TextBlockAnchor.TOP_CENTER), // BOTTOM
			            new CategoryLabelPosition(
			                RectangleAnchor.LEFT, TextBlockAnchor.CENTER_LEFT,
			                CategoryLabelWidthType.RANGE, 0.30f), // LEFT
			            new CategoryLabelPosition(
			                RectangleAnchor.LEFT, TextBlockAnchor.CENTER_LEFT,
			                CategoryLabelWidthType.RANGE, 0.30f) // RIGHT
			        );
		 subcategoryaxis.setCategoryLabelPositions( axisLabelPosition);
		 plot.setDomainAxis(subcategoryaxis);

	

//		// itemLabelGenerator will only display items in plot (drawing area). not on axis
//		ABMICategoryItemLabelGenerator itemLabelGenerator = new ABMICategoryItemLabelGenerator(
//				labels);
//
//		renderer.setBaseItemLabelGenerator(itemLabelGenerator);
//		ItemLabelPosition position = new ItemLabelPosition(
//				ItemLabelAnchor.OUTSIDE6, TextAnchor.BOTTOM_CENTER);
//		renderer.setBasePositiveItemLabelPosition(position);
		 
//		renderer.setBaseItemLabelsVisible(true);
		// renderer.setBaseItemLabelsVisible(false);
//		 renderer.setBaseItemLabelFont(new Font("Arial", Font.PLAIN, 5));
		 
		 renderer.setMaximumBarWidth(0.1);
		
		
		//
		 
		 plot.setDataset(1, this.getCategory2());
		

		ChartRenderingInfo info = new ChartRenderingInfo(
				new StandardEntityCollection());

		MockHttpSession session = new MockHttpSession();

		String filename = "";
		try {
			filename = ServletUtilities.saveChartAsPNG(chart, 1233, 337, info,
					session);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Write the image map to the PrintWriter
		// test
		// ChartUtilities.writeImageMap(pw, filename, info);
		// pw.flush();

		return filename;
	}

}
