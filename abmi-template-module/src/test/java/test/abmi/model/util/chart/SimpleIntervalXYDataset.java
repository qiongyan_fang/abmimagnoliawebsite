package test.abmi.model.util.chart;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.AbstractIntervalXYDataset;
import org.jfree.data.xy.IntervalXYDataset;

public class SimpleIntervalXYDataset extends AbstractIntervalXYDataset
		implements IntervalXYDataset {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public SimpleIntervalXYDataset(String[] series, Double[] xStart, Double[] xEnd, Double[] yValues) {
		this.xStart = xStart;
		this.xEnd = xEnd;
		this.yValues = yValues;
		this.seriesKey = series;
		return;
//		yValues = new Double[4];
//		
//		xStart[0] = new Double(0.0D);
//		xStart[1] = new Double(2D);
//		xStart[2] = new Double(3.5D);
//		xStart[3] = new Double(4D);
//		
//		xEnd[0] = new Double(2D);
//		xEnd[1] = new Double(3.5D);
//		xEnd[2] = new Double(4D);
//		xEnd[3] = new Double(8D);
//		
//		yValues[0] = new Double(3D);
//		yValues[1] = new Double(4.5D);
//		yValues[2] = new Double(-2.5D);
//		yValues[3] = new Double(-0.5D);
	}

	public int getSeriesCount() {
		return this.seriesKey.length;
	}

	public Comparable getSeriesKey(int i) {
		if (this.seriesKey != null && i <this.seriesKey.length)
			return this.seriesKey[i];
		else
			return "Series 1";
	}

	public int getItemCount(int i) {
		return yValues.length;
	}

	public Number getX(int i, int j) {
		return xStart[j];
	}

	public Number getY(int i, int j) {
		return yValues[j];
	}

	public Number getStartX(int i, int j) {
		return xStart[j];
	}

	public Number getEndX(int i, int j) {
		return xEnd[j];
	}

	public Number getStartY(int i, int j) {
		return yValues[j];
	}

	public Number getEndY(int i, int j) {
		return yValues[j];
	}

	public void addChangeListener(DatasetChangeListener datasetchangelistener) {
	}

	public void removeChangeListener(DatasetChangeListener datasetchangelistener) {
	}

	private Double xStart[];
	private Double xEnd[];
	private Double yValues[];
	private String seriesKey[];
}
