package test.abmi.model.util.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Paint;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JPanel;
import org.jfree.chart.*;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SubCategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.imagemap.ImageMapUtilities;
import org.jfree.chart.imagemap.StandardURLTagFragmentGenerator;
import org.jfree.chart.imagemap.ToolTipTagFragmentGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedRangeCategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.IntervalBarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.MinMaxCategoryRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.category.StatisticalLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.category.DefaultIntervalCategoryDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.RefineryUtilities;
import org.springframework.mock.web.MockHttpSession;

import abmi.model.util.chart.ext.ABMICategoryAxis;
import abmi.model.util.chart.renderer.ABMIMultiColorBarRenderer;
import abmi.model.util.chart.renderer.IntervalLineRenderer;

public class CombinedCateogryPlotChart {

	String[] columnKeys = { "Upland Spurce", "Pine", "Deciduous", "Mixedwood",
			"Black Spruce", "Larch", "Grass", "Shrub", "Wetland",
			"Cultivated HF", "Urban/Industry HF" };

	String[] rowKeys = { "0", "10", "20", "40", "60", "80", "100", "120",
			"140", "other" };

	CategoryDataset createDataset1() {
		DefaultCategoryDataset category = new DefaultCategoryDataset();

		// String[] rowKeys = { "0","0", "0", "0", "0", "0", "0", "0", "0" };
		category.addValue(10d, rowKeys[0], columnKeys[0]);
		category.addValue(10d, rowKeys[1], columnKeys[0]);
		category.addValue(10d, rowKeys[2], columnKeys[0]);
		category.addValue(10d, rowKeys[3], columnKeys[0]);
		category.addValue(10d, rowKeys[4], columnKeys[0]);
		category.addValue(10d, rowKeys[5], columnKeys[0]);
		category.addValue(10d, rowKeys[6], columnKeys[0]);
		category.addValue(10d, rowKeys[7], columnKeys[0]);
		category.addValue(10d, rowKeys[8], columnKeys[0]);

		category.addValue(10d, rowKeys[0], columnKeys[1]);
		category.addValue(10d, rowKeys[1], columnKeys[1]);
		category.addValue(10d, rowKeys[2], columnKeys[1]);
		category.addValue(10d, rowKeys[3], columnKeys[1]);
		category.addValue(10d, rowKeys[4], columnKeys[1]);
		category.addValue(10d, rowKeys[5], columnKeys[1]);
		category.addValue(10d, rowKeys[6], columnKeys[1]);
		category.addValue(10d, rowKeys[7], columnKeys[1]);
		category.addValue(10d, rowKeys[8], columnKeys[1]);

		category.addValue(10d, rowKeys[0], columnKeys[2]);
		category.addValue(10d, rowKeys[1], columnKeys[2]);
		category.addValue(10d, rowKeys[2], columnKeys[2]);
		category.addValue(10d, rowKeys[3], columnKeys[2]);
		category.addValue(10d, rowKeys[4], columnKeys[2]);
		category.addValue(10d, rowKeys[5], columnKeys[2]);
		category.addValue(15d, rowKeys[6], columnKeys[2]);
		category.addValue(10d, rowKeys[7], columnKeys[2]);
		category.addValue(10d, rowKeys[8], columnKeys[2]);

		category.addValue(10d, rowKeys[0], columnKeys[3]);
		category.addValue(10d, rowKeys[1], columnKeys[3]);
		category.addValue(15d, rowKeys[2], columnKeys[3]);
		category.addValue(10d, rowKeys[3], columnKeys[3]);
		category.addValue(10d, rowKeys[4], columnKeys[3]);
		category.addValue(10d, rowKeys[5], columnKeys[3]);
		category.addValue(10d, rowKeys[6], columnKeys[3]);
		category.addValue(10d, rowKeys[7], columnKeys[3]);
		category.addValue(10d, rowKeys[8], columnKeys[3]);

		category.addValue(10d, rowKeys[0], columnKeys[4]);
		category.addValue(10d, rowKeys[1], columnKeys[4]);
		category.addValue(10d, rowKeys[2], columnKeys[4]);
		category.addValue(10d, rowKeys[3], columnKeys[4]);
		category.addValue(10d, rowKeys[4], columnKeys[4]);
		category.addValue(10d, rowKeys[5], columnKeys[4]);
		category.addValue(15d, rowKeys[6], columnKeys[4]);
		category.addValue(10d, rowKeys[7], columnKeys[4]);
		category.addValue(10d, rowKeys[8], columnKeys[4]);

		category.addValue(10d, rowKeys[0], columnKeys[5]);
		category.addValue(10d, rowKeys[1], columnKeys[5]);
		category.addValue(10d, rowKeys[2], columnKeys[5]);
		category.addValue(10d, rowKeys[3], columnKeys[5]);
		category.addValue(10d, rowKeys[4], columnKeys[5]);
		category.addValue(14d, rowKeys[5], columnKeys[5]);
		category.addValue(10d, rowKeys[6], columnKeys[5]);
		category.addValue(10d, rowKeys[7], columnKeys[5]);
		category.addValue(10d, rowKeys[8], columnKeys[5]);

		return category;
	}

	DefaultCategoryDataset createDataset2() {
		DefaultCategoryDataset category2 = new DefaultCategoryDataset();
		category2.addValue(10d, rowKeys[9], columnKeys[6]);

		category2.addValue(10d, rowKeys[9], columnKeys[7]);

		category2.addValue(10d, rowKeys[9], columnKeys[8]);

		category2.addValue(16d, rowKeys[9], columnKeys[9]);

		category2.addValue(14d, rowKeys[9], columnKeys[10]);
		return category2;
	}

	private JFreeChart createChart() {
		/**
		 * first plot
		 * 
		 */
		CategoryDataset categoryBardataset = createDataset1();
		SubCategoryAxis subcategoryaxis = new SubCategoryAxis("test group");

		subcategoryaxis.addSubCategory("0");// 10.255");
		subcategoryaxis.addSubCategory("20");// 10.255");
		subcategoryaxis.addSubCategory(" ");// 20.255");
		subcategoryaxis.addSubCategory("60");// 30.255")
		subcategoryaxis.addSubCategory(" ");// 30.255")
		subcategoryaxis.addSubCategory("100");// 30.255");
		subcategoryaxis.addSubCategory(" ");// 30.255");
		subcategoryaxis.addSubCategory("140");// 30.255");

		subcategoryaxis.setLabelFont(new Font("Arial", Font.PLAIN, 9));
		subcategoryaxis.setSubLabelFont(new Font("Arial", Font.PLAIN, 6));
		subcategoryaxis.setCategoryLabelPositionOffset(10);
		subcategoryaxis.setCategoryMargin(0.2);

		/**
		 * first plot -- high low lines
		 */

		String[] rowKeys = { "0", "10", "20", "40", "60", "80", "100", "120",
				"140" };
		String[] columnKeys = { "Upland Spurce", "Pine", "Deciduous",
				"Mixedwood", "Black Spruce", "Larch" };

		Integer[][] starts = { { 0, 0, 0, 0, 0, 0 }, { 6, 6, 6, 6, 6, 6 },
				{ 5, 5, 5, 5, 5, 5 }, { 2, 2, 2, 2, 2, 2 },
				{ 1, 1, 1, 1, 1, 1 }, { 8, 8, 8, 8, 8, 8 },
				{ 10, 10, 10, 10, 10, 10 }, { 20, 20, 20, 20, 20, 20 },
				{ 12, 12, 12, 12, 12, 12 } };

		Integer[][] ends = { { 10, 10, 10, 10, 10, 10 },
				{ 15, 15, 15, 15, 15, 15 }, { 5, 15, 5, 15, 5, 5 },
				{ 25, 25, 2, 25, 25, 25 }, { 11, 11, 11, 11, 11, 11 },
				{ 38, 38, 38, 38, 38, 38 }, { 13, 13, 13, 13, 13, 13 },
				{ 17, 17, 17, 17, 17, 17 }, { 25, 25, 25, 25, 25, 25 } };
		// IntervalBarRenderer barRenderer = new IntervalBarRenderer(); // high
		// low bars
		IntervalLineRenderer minMaxRenderer = new IntervalLineRenderer();
		
		minMaxRenderer.setBaseStroke(new BasicStroke(1.2f) );
		minMaxRenderer.setPaint(Color.black);
		minMaxRenderer.setOffsetX(-0.25);
//		IntervalBarRenderer intervalBar = new IntervalBarRenderer();
		DefaultIntervalCategoryDataset categoryIntervalDataset = new DefaultIntervalCategoryDataset(
				rowKeys, columnKeys, starts, ends);

		CategoryPlot categoryplot = new CategoryPlot(categoryIntervalDataset,
				subcategoryaxis, null, minMaxRenderer);
		categoryplot.setDomainGridlinesVisible(true);

		
		IntervalLineRenderer minMaxRenderer2 = new IntervalLineRenderer();
		
		minMaxRenderer2.setBaseStroke(new BasicStroke(1f) );
		minMaxRenderer2.setPaint(Color.blue);
		minMaxRenderer2.setOffsetX(0.25);
		categoryplot.setDataset(1, categoryIntervalDataset);
		categoryplot.setRenderer(1, minMaxRenderer2);
		
		
		String[] colorCode = { "#ca5f46", "#163545", "#c47c34", "#c2B1A2",
				"#B2A1C2", "#FFAABB" };

		Paint[] colors = new Paint[colorCode.length];
		for (int i = 0; i < colorCode.length; i++) {
			colors[i] = Color.decode(colorCode[i]);
		}

		ABMIMultiColorBarRenderer renderer = new ABMIMultiColorBarRenderer(
				colors);
		renderer.setShadowVisible(false);
		renderer.setBarPainter(new StandardBarPainter());

	
		
		categoryplot.setDataset(2, categoryBardataset);
		categoryplot.setRenderer(2, renderer);

		/**
		 * --------------------------- second plot
		 */

		CategoryDataset categorydataset1 = createDataset2();

		CategoryAxis categoryaxis1 = new CategoryAxis("Class 2");
		categoryaxis1.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		categoryaxis1.setMaximumCategoryLabelWidthRatio(5F);
		BarRenderer barrenderer = new BarRenderer();
		barrenderer
				.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());
		CategoryPlot categoryplot1 = new CategoryPlot(categorydataset1,
				categoryaxis1, null, barrenderer);

		categoryplot1.setDomainGridlinesVisible(true);
		NumberAxis numberaxis = new NumberAxis("Value");
		CombinedRangeCategoryPlot combinedrangecategoryplot = new CombinedRangeCategoryPlot(
				numberaxis);
		combinedrangecategoryplot.add(categoryplot, 3);
		combinedrangecategoryplot.add(categoryplot1, 2);

		categoryplot1.setOutlinePaint(Color.white);
		categoryplot.setOutlinePaint(Color.white);

		combinedrangecategoryplot.setGap(0);
		combinedrangecategoryplot.setOutlinePaint(Color.white);

		JFreeChart jfreechart = new JFreeChart(
				"Combined Range Category Plot Demo", new Font("SansSerif", 1,
						12), combinedrangecategoryplot, true);
		return jfreechart;
	}

	public String exportChart() {

		JFreeChart chart = createChart();

		ChartRenderingInfo info = new ChartRenderingInfo(
				new StandardEntityCollection());

		MockHttpSession session = new MockHttpSession();

		String filename = "";
		try {
			filename = ServletUtilities.saveChartAsPNG(chart, 800, 600, info,
					session);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Write the image map to the PrintWriter
		// test
		// ChartUtilities.writeImageMap(pw, filename, info);
		// pw.flush();

		return filename;
	}

}
