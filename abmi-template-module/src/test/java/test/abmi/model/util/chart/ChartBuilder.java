package test.abmi.model.util.chart;

import info.magnolia.context.MgnlContext;
import info.magnolia.repository.RepositoryConstants;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.jcr.LoginException;
import javax.jcr.RepositoryException;
import javax.servlet.http.HttpSession;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.imagemap.ImageMapUtilities;
import org.jfree.chart.imagemap.StandardURLTagFragmentGenerator;
import org.jfree.chart.imagemap.ToolTipTagFragmentGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.MinMaxCategoryRenderer;
import org.jfree.chart.renderer.category.StatisticalBarRenderer;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.chart.urls.CustomCategoryURLGenerator;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.category.DefaultIntervalCategoryDataset;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;

import org.jfree.ui.TextAnchor;
import org.springframework.mock.web.MockHttpSession;

import abmi.model.util.chart.ext.ABMICategoryItemLabelGenerator;
import abmi.model.util.chart.ext.ABMICategoryToolTipGenerator;
import abmi.model.util.chart.ext.ABMIOverLibToolTipTagFragmentGenerator;
import abmi.model.util.chart.renderer.ABMIMultiColorBarRenderer;

public class ChartBuilder {

	String fromPage = "";
	String fStrMapInfo;
	int fWidth = 600, fHeight = 300;

	public void setFWidth(int fWidth) {
		this.fWidth = fWidth;
	}

	public int getFWidth() {
		return fWidth;
	}

	public void setFHeight(int fHeight) {
		this.fHeight = fHeight;
	}

	public int getFHeight() {
		return fHeight;
	}

	

	ABMICategoryItemLabelGenerator fItemLabel;
	ABMICategoryToolTipGenerator fTooltip;

	

	/**
	 * 
	 * input: abundance or occurrence tsnIds
	 */
	public String getTestBarPlots() {
		// get user input

		/**
	         *
	         */

		//
		DefaultCategoryDataset minMaxDataset = new DefaultCategoryDataset();
		minMaxDataset.addValue(1.0D, "Min", "Productive");
		minMaxDataset.addValue(2D, "Mean", "Productive");
		minMaxDataset.addValue(3D, "Max", "Productive");
		minMaxDataset.addValue(1.0D, "Min", "Clay");
		minMaxDataset.addValue(4D, "Mean", "Clay");
		minMaxDataset.addValue(5D, "Max", "Clay");
		
		minMaxDataset.addValue(1.0D, "Min", "Saline");
		minMaxDataset.addValue(2D, "Mean", "Saline");
		minMaxDataset.addValue(3D, "Max", "Saline");
		minMaxDataset.addValue(1.0D, "Min", "Rapid Drain");
		minMaxDataset.addValue(4D, "Mean", "Rapid Drain");
		minMaxDataset.addValue(5D, "Max", "Rapid Drain");
		minMaxDataset.addValue(1.0D, "Min", "Cultivated");
		minMaxDataset.addValue(4D, "Mean", "Cultivated");
		minMaxDataset.addValue(5D, "Max", "Cultivated");

		minMaxDataset.addValue(1.0D, "Min", "Urban/Industry");
		minMaxDataset.addValue(4D, "Mean", "Urban/Industry");
		minMaxDataset.addValue(5D, "Max", "Urban/Industry");
		
		String[][] rows = { { "Productive", "1", "#ca5f46" },
				{ "Clay", "2", "#163545" }, { "Saline", "3", "#c47c34" },
				{ "Rapid Drain", "4", "#c2B1A2" },
				{ "Cultivated", "5", "#B2A1C2" },
				{ "Urban/Industry", "6", "#FFAABB" } };
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		ArrayList<String> labelEmpty = new ArrayList<String>();
		Paint colors[] = new Paint[rows.length];
		ArrayList<String> urls = new ArrayList<String>();
		ArrayList<String> toolTips = new ArrayList<String>();
		ValueMarker target = null;

		CustomCategoryURLGenerator urlGenerator = new CustomCategoryURLGenerator();

		for (int i = 0; i < rows.length; i++) {

//			System.out.println("row #" + i);
			String singleUrl = "";

			DecimalFormat mFormatter = new DecimalFormat("#,###.#");
			double dValue = Double.parseDouble(rows[i][1]);
			dataset.addValue(dValue, "1", rows[i][0]);
			urls.add(singleUrl);
			/**
			 * add a label to indicate occurrence bars's numbers, where species
			 * detected
			 */

			labelEmpty.add(" " + rows[i][0]);
			// toolTips.add(fOccurrenceAbundanceStr + " value in " + regionName
			// + " region: " + dValue);

			toolTips.add("," + rows[i][1] + "," + rows[i][0] + ", welcome here"); // region
																					// name,
																					// value,
																					// site
																					// detected,
																					// sampled
																					// site

			colors[i] = Color.decode(rows[i][2]);

		}

		urlGenerator.addURLSeries(urls);

		// final StandardCategoryToolTipGenerator ttg = new
		// StandardCategoryToolTipGenerator();

		/*------------------------------------------------------------------
		 *
		 * start to create plot
		 *
		 * ----------------------------------------------------------------------*/

		// HttpSession session = null;
		MockHttpSession session = new MockHttpSession();

		String yAxisLabel;
		NumberTickUnit tickUnit;

		String xAxisLabel = null;

		try {

			/**
			 * createBarChart(java.lang.String title, java.lang.String
			 * categoryAxisLabel, java.lang.String valueAxisLabel,
			 * CategoryDataset dataset, PlotOrientation orientation, boolean
			 * legend, boolean tooltips, boolean urls)
			 */
			JFreeChart chart = // legend
			// tool tips
			// URLs

			ChartFactory.createBarChart("", xAxisLabel, "", dataset,
					PlotOrientation.VERTICAL, false, true, true);
			ChartFactory.setChartTheme(StandardChartTheme.createLegacyTheme());

			chart.setBackgroundPaint(new Color(237, 240, 250));

			CategoryPlot plot = chart.getCategoryPlot();

			plot.setBackgroundPaint(new Color(237, 240, 250));

			CategoryAxis xAxis = plot.getDomainAxis();
			xAxis.setVisible(true);
			xAxis.setMaximumCategoryLabelLines(3);
			xAxis.setLowerMargin(0);

			xAxis.setTickLabelFont(new Font("SansSerif", Font.PLAIN, 9));

			plot.setDomainAxis(xAxis);

			NumberAxis yAxis = new NumberAxis(null);
			yAxis.setAutoRangeIncludesZero(false);
			yAxis.setLowerBound(0);
			yAxis.setAutoRange(true);
			// yAxis.setUpperBound(100);

			// if (tickUnit != null)
			// yAxis.setTickUnit(tickUnit);

			plot.setRangeAxis(yAxis);

			plot.setRangeGridlinePaint(Color.darkGray);
			final ABMIMultiColorBarRenderer renderer = new ABMIMultiColorBarRenderer(colors);

			if (urlGenerator != null)
				renderer.setBaseItemURLGenerator(urlGenerator);

			renderer.setBarPainter(new org.jfree.chart.renderer.category.StandardBarPainter());
			renderer.setBaseOutlinePaint(new Color(82, 82, 82));
			renderer.setDrawBarOutline(true);

			this.fItemLabel = new ABMICategoryItemLabelGenerator(
					labelEmpty);
			renderer.setBaseItemLabelGenerator(fItemLabel);
			renderer.setBaseItemLabelsVisible(true);
			// renderer.setBaseItemLabelsVisible(false);
			renderer.setMaximumBarWidth(0.05);
			renderer.setBaseItemLabelFont(new Font("Arial", Font.PLAIN, 9));
			if (toolTips != null & toolTips.size() > 0) {
				fTooltip = new ABMICategoryToolTipGenerator(toolTips);
				renderer.setBaseToolTipGenerator(this.fTooltip);
			}
			renderer.setShadowVisible(false);

			if (target != null) {
				float dash[] = { 3.0f };

				BasicStroke sk = new BasicStroke(1.5f, BasicStroke.CAP_BUTT,
						BasicStroke.JOIN_ROUND, 3.0f, dash, 0.0f);

				target.setStroke(sk);
				// target.setOutlineStroke(sk);
				target.setPaint(new Color(255, 0, 0));
				plot.addRangeMarker(target);
			}

			// plot.setRenderer(1, renderer);
			// plot.setDataset(1, dataset);
			// StatisticalBarRenderer highlowRenderer = new
			// StatisticalBarRenderer();

			plot.setDataset(1, dataset);

			plot.setRenderer(1, renderer);

			MinMaxCategoryRenderer minMaxRenderer = new MinMaxCategoryRenderer();
			minMaxRenderer.setDrawLines(false);
			minMaxRenderer.setDefaultEntityRadius(2);
			minMaxRenderer.setGroupPaint(Color.red);
			minMaxRenderer.setAutoPopulateSeriesStroke(true);

			plot.setDataset(0, minMaxDataset);
			plot.setRenderer(0, minMaxRenderer);

			renderer.setAutoPopulateSeriesFillPaint(false);

			// Write the chart image to the temporary directory

			ChartRenderingInfo info = new ChartRenderingInfo(
					new StandardEntityCollection());

			String filename = ServletUtilities.saveChartAsPNG(chart, fWidth,
					fHeight, info, session);

			// Write the image map to the PrintWriter
			// test
			// ChartUtilities.writeImageMap(pw, filename, info);
			// pw.flush();

			String strMapInfo = "";
			if (urlGenerator != null || fTooltip != null) {
				StringWriter oStringWriter = new StringWriter();
				PrintWriter printwriter = new PrintWriter(oStringWriter);

				ToolTipTagFragmentGenerator toolTipTagFragmentGenerator = new ABMIOverLibToolTipTagFragmentGenerator();
				ImageMapUtilities.writeImageMap(printwriter, "chart", info,
						toolTipTagFragmentGenerator,
						new StandardURLTagFragmentGenerator());
				strMapInfo = oStringWriter.toString();
			}

			fStrMapInfo = strMapInfo;

			return filename;
		} catch (Exception e) {

			return null;

		}

	}

}
