package test.abmi.model.util.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Paint;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JPanel;
import org.jfree.chart.*;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SubCategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.imagemap.ImageMapUtilities;
import org.jfree.chart.imagemap.StandardURLTagFragmentGenerator;
import org.jfree.chart.imagemap.ToolTipTagFragmentGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedRangeCategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.IntervalBarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.MinMaxCategoryRenderer;
import org.jfree.chart.renderer.category.StackedBarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.category.StatisticalLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.category.DefaultIntervalCategoryDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.RefineryUtilities;
import org.springframework.mock.web.MockHttpSession;

import abmi.model.util.chart.ext.ABMICategoryAxis;
import abmi.model.util.chart.renderer.ABMILineAndShapeCategoryRenderer;
import abmi.model.util.chart.renderer.ABMIMultiColorBarRenderer;
import abmi.model.util.chart.renderer.IntervalLineRenderer;

public class CombinedManyCateogryPlotChart {

	String[] seriesKeys = { "Upland Spurce", "Pine", "Deciduous", "Mixedwood",
			"Black Spruce", "Larch" };

	String[] othercategoryKeys = { "Grass", "Shrub", "Wetland", "Cultivated HF",
			"Urban/Industry HF" };

	String[] categoryKeys = { "0", "10", "20", "40", "60", "80", "100", "120", "140" };

	CategoryDataset createDataset1(int seriesIndex) {
		DefaultCategoryDataset category = new DefaultCategoryDataset();

		// String[] rowKeys = { "0","0", "0", "0", "0", "0", "0", "0", "0" };

		for (int j = 0; j < categoryKeys.length; j++) {
			category.addValue(Math.random() * 100d, seriesKeys[seriesIndex], categoryKeys[j]
					);
		}

		return category;
	}

	DefaultIntervalCategoryDataset createHighLowDataset1(int seriesIndex) {

		// String[] rowKeys = { "0","0", "0", "0", "0", "0", "0", "0", "0" };

		Double starts[][] = new Double[1][categoryKeys.length];
		Double ends[][] = new Double[1][categoryKeys.length];
		for (int j = 0; j < categoryKeys.length; j++) {
			starts[0][j] = Math.random() * 80;
			ends[0][j] = starts[0][j]+ Math.random() * 25;
		}

		String[] singleSeriesKeys = new String[1];
		singleSeriesKeys[0] = seriesKeys[seriesIndex];
		DefaultIntervalCategoryDataset category = new DefaultIntervalCategoryDataset(singleSeriesKeys,
				categoryKeys,  starts, ends);
		return category;
	}

	DefaultCategoryDataset createDataset2() {
		DefaultCategoryDataset category2 = new DefaultCategoryDataset();
		String rowKeys9 = "other";
		for (int i = 0; i < othercategoryKeys.length; i++) {
			category2.addValue(Math.random() * 80, rowKeys9, othercategoryKeys[i]);
		}

		return category2;
	}

	private JFreeChart createChart() {
		/**
		 * first plot
		 * 
		 */

		String[] colorCode = { "#ca5f46", "#163545", "#c47c34", "#c2B1A2",
				"#B2A1C2", "#FFAABB" };

		Paint[] colors = new Paint[colorCode.length];
		for (int i = 0; i < colorCode.length; i++) {
			colors[i] = Color.decode(colorCode[i]);
		}

		// main plot
		NumberAxis numberaxis = new NumberAxis("Value");
		CombinedRangeCategoryPlot combinedrangecategoryplot = new CombinedRangeCategoryPlot(
				numberaxis);

		/**
		 * first plot -- high low lines
		 */

		for (int i = 0; i < seriesKeys.length; i++) { // only first 6
		

			

			/* line plot */

			// IntervalBarRenderer intervalBar = new IntervalBarRenderer();
			DefaultIntervalCategoryDataset categoryIntervalDataset = this
					.createHighLowDataset1(i);

			DefaultCategoryDataset categoryLineDataset = new DefaultCategoryDataset();
			for (Object row : categoryIntervalDataset.getRowKeys()) {
				for (Object column : categoryIntervalDataset.getColumnKeys()) {
					Double start = (Double) categoryIntervalDataset
							.getStartValue(row.toString(), column.toString());
					Double end = (Double) categoryIntervalDataset.getEndValue(
							row.toString(), column.toString());

					categoryLineDataset.addValue((start + end) * 0.5,
							row.toString(), column.toString());

				}

			}

			ABMILineAndShapeCategoryRenderer lineRenderer = new ABMILineAndShapeCategoryRenderer();
			lineRenderer.setBaseStroke(new BasicStroke(1f));
			lineRenderer.setPaint(Color.yellow);
			lineRenderer.setOffsetX(-0.01);
			// lineRenderer.setOffsetX(0.25);
			lineRenderer.setSeriesVisibleInLegend(false);
			
			lineRenderer.setItemMargin(0);
//			lineRenderer.setUseSeriesOffset(true);
//			lineRenderer.setItemMargin(0.01);	use for series
			
			
			CategoryAxis subcategoryaxis = new CategoryAxis(this.seriesKeys[i]);
//			subcategoryaxis
//					.setCategoryLabelPositions(CategoryLabelPositions.UP_45); // adjust axis label angle
			
			for (int c=0; c< categoryKeys.length; c+=2) {
				subcategoryaxis.setTickLabelPaint(categoryKeys[c],  new Color(0,0,0,0));
			}
//			subcategoryaxis.setCategoryMargin(-4); // set space between bars within same categories. use as 100%. -3 seems to be-300% somehow worked.
//			
//			subcategoryaxis.setLowerMargin(-0.2);
//			subcategoryaxis.setUpperMargin(-0.2);
			
			CategoryPlot categoryplot = new CategoryPlot(categoryLineDataset,
					subcategoryaxis, null, lineRenderer);

		
			/* high low line 1 */
			IntervalLineRenderer minMaxRenderer = new IntervalLineRenderer();

			minMaxRenderer.setBaseStroke(new BasicStroke(1.2f));
			minMaxRenderer.setPaint(Color.black);
//			minMaxRenderer.setOffsetX(-0.1);
			minMaxRenderer.setSeriesVisibleInLegend(false);
			minMaxRenderer.setItemMargin(0);
			
			System.out.println(" categoryIntervalDataset size="
					+ categoryIntervalDataset.getRowCount() + " "
					+ categoryIntervalDataset.getColumnCount());

			categoryplot.setDomainGridlinesVisible(true);
			categoryplot.setDataset(1, categoryIntervalDataset);
			categoryplot.setRenderer(1, minMaxRenderer);
			categoryplot.setDomainGridlinesVisible(false);
			
			/* high low line 2 */

			DefaultIntervalCategoryDataset categoryIntervalDataset2 = this
					.createHighLowDataset1(i);

			IntervalLineRenderer minMaxRenderer2 = new IntervalLineRenderer();
			minMaxRenderer2.setBaseStroke(new BasicStroke(1f));
			minMaxRenderer2.setPaint(Color.blue);
			minMaxRenderer2.setOffsetX(0.01);
			minMaxRenderer2.setSeriesVisibleInLegend(false);
			
			minMaxRenderer2.setItemMargin(0);
			categoryplot.setDataset(2, categoryIntervalDataset2);
			categoryplot.setRenderer(2, minMaxRenderer2);

			/**
			 * main bar plot
			 */

			ABMIMultiColorBarRenderer renderer = new ABMIMultiColorBarRenderer(
					colors);
			renderer.setShadowVisible(false);
			renderer.setBarPainter(new StandardBarPainter());
			renderer.setSeriesVisibleInLegend(false);
			renderer.setMaximumBarWidth(0.1);
			
			renderer.setItemMargin(0.0);
			
			CategoryDataset categoryBardataset = this.createDataset1(i);

			// CategoryPlot categoryplot = new CategoryPlot(
			// categoryBardataset, subcategoryaxis, null,
			// renderer);

			categoryplot.setDataset(3, categoryBardataset);
			categoryplot.setRenderer(3, renderer);

			System.out.println(" categoryBardataset size="
					+ categoryBardataset.getRowCount() + " "
					+ categoryBardataset.getColumnCount());

			combinedrangecategoryplot.add(categoryplot, 9); // 9 is the weight.
															// make it easier I
															// used the number
															// of bars

			combinedrangecategoryplot.getRangeAxis().setUpperBound(100);
			
			categoryplot.setOutlinePaint(Color.white);

		}

		combinedrangecategoryplot.setGap(0);
		combinedrangecategoryplot.setOutlinePaint(Color.white);

		/**
		 * --------------------------- second group , another plot
		 */

		CategoryDataset categorydataset1 = createDataset2();

		CategoryAxis categoryaxis1 = new CategoryAxis("Class 2");
		categoryaxis1.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		categoryaxis1.setMaximumCategoryLabelWidthRatio(5F);
		
		categoryaxis1.setCategoryMargin(0);
		categoryaxis1.setLowerMargin(0);
		categoryaxis1.setUpperMargin(0);
		// categoryaxis1.setFixedDimension(23); // doesn't work
		BarRenderer  barrenderer = new BarRenderer ();
		//
		
		//
		barrenderer
				.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());

		barrenderer.setMaximumBarWidth(0.1);
		barrenderer.setSeriesVisibleInLegend(false);
		barrenderer.setShadowVisible(false);
		barrenderer.setBarPainter(new StandardBarPainter());
		barrenderer.setItemMargin(0);
		CategoryPlot categoryplot1 = new CategoryPlot(categorydataset1,
				categoryaxis1, null, barrenderer);

		categoryplot1.setDomainGridlinesVisible(false);

		combinedrangecategoryplot.add(categoryplot1, 15); // 5: is the weight,
															// porpotion; or use
															// the number of
															// bars

		JFreeChart jfreechart = new JFreeChart(
				"Combined Range Category Plot Demo", new Font("SansSerif", 1,
						12), combinedrangecategoryplot, true);
		return jfreechart;
	}

	public String exportChart() {

		JFreeChart chart = createChart();

		ChartRenderingInfo info = new ChartRenderingInfo(
				new StandardEntityCollection());

		MockHttpSession session = new MockHttpSession();

		String filename = "";
		try {
			filename = ServletUtilities.saveChartAsPNG(chart, 1170, 377, info,
					session);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Write the image map to the PrintWriter
		// test
		// ChartUtilities.writeImageMap(pw, filename, info);
		// pw.flush();

		return filename;
	}

}
