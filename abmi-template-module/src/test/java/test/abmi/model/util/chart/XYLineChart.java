package test.abmi.model.util.chart;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JPanel;
import org.jfree.chart.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.imagemap.ImageMapUtilities;
import org.jfree.chart.imagemap.StandardURLTagFragmentGenerator;
import org.jfree.chart.imagemap.ToolTipTagFragmentGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.RefineryUtilities;
import org.springframework.mock.web.MockHttpSession;



public class XYLineChart  {
	
	XYDataset createDataset()   
    {   
        XYSeries xyseries = new XYSeries("Soft Linear");   
        xyseries.add(1.0D, 1.0D);   
        xyseries.add(2D, 4D);   
      
        XYSeries xyseries1 = new XYSeries("Hard Linear");   
        xyseries1.add(1.0D, 1.0D);   
        xyseries1.add(2D, 7D);   
      
      
        XYSeriesCollection xyseriescollection = new XYSeriesCollection();   
        xyseriescollection.addSeries(xyseries);   
        xyseriescollection.addSeries(xyseries1);   
      
        return xyseriescollection;   
    }   
   
	JFreeChart createChart(XYDataset xydataset)   
    {   
        JFreeChart jfreechart = ChartFactory.createXYLineChart("Line Chart Demo 2", "X", "Y", xydataset, PlotOrientation.VERTICAL, true, true, false);   
        jfreechart.setBackgroundPaint(Color.white);   
        XYPlot xyplot = (XYPlot)jfreechart.getPlot();   
        xyplot.setBackgroundPaint(Color.lightGray);   
        xyplot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));   
        xyplot.setDomainGridlinePaint(Color.white);   
        xyplot.setRangeGridlinePaint(Color.white);   
      
        XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer)xyplot.getRenderer();   
        xylineandshaperenderer.setShapesVisible(true);   
        xylineandshaperenderer.setShapesFilled(true);   
        
        NumberAxis numberaxis = (NumberAxis)xyplot.getRangeAxis();   
        numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());   
        return jfreechart;   
    }   


	public  String exportChart() {
		
		
		JFreeChart chart = createChart(this.createDataset());

		XYPlot plot = chart.getXYPlot();
		
		String[] colorCode = { "#ca5f46",			  "#163545" , "#c47c34", "#c2B1A2" , "#B2A1C2",  "#FFAABB" } ;
		
		Paint [] colors = new Paint[colorCode.length];
		for(int i=0; i < colorCode.length; i++) {
			colors[i] = Color.decode(colorCode[i]);
		}
			
			
//		ABMIMultiColorXYBarRenderer renderer = new ABMIMultiColorXYBarRenderer(colors);
//		renderer.setShadowVisible(false);
//		renderer.setBarPainter(new StandardXYBarPainter());
//		plot.setRenderer (renderer);
		
		plot.setBackgroundPaint(new Color(237, 240, 250));
		NumberAxis yAxis = (NumberAxis)plot.getRangeAxis();
//		NumberAxis yAxis = new NumberAxis(null);
		yAxis.setAutoRangeIncludesZero(true);
		
		
		plot.setRangeAxis(yAxis);
		

		ChartRenderingInfo info = new ChartRenderingInfo(
				new StandardEntityCollection());
		
		
		MockHttpSession session = new MockHttpSession();
		
		String filename = "";
		try {
			filename = ServletUtilities.saveChartAsPNG(chart, 800,
					600, info, session);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Write the image map to the PrintWriter
		// test
		// ChartUtilities.writeImageMap(pw, filename, info);
		// pw.flush();

		

		return filename;
	}

	

	
}
