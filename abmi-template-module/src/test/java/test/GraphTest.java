package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import abmi.model.services.ProjectPropertyService;
import abmi.model.services.RegionsService;
import abmi.model.services.biobrowser.GraphService;
import abmi.model.services.biobrowser.chart.SectorEffectorChart;
import abmi.model.services.rawdata.CacheRawdataService;
import abmi.model.services.rawdata.DataCategoryService;
import abmi.model.services.rawdata.DownloadPackageGenerator;
import abmi.model.services.rawdata.RawdataQueryCondition;

import abmi.model.util.ModuleParameters;

import org.apache.commons.io.FileUtils;

import test.abmi.model.util.chart.BarMultiMinMaxChart;
import test.abmi.model.util.chart.ChartBuilder;
import test.abmi.model.util.chart.CombinedCateogryPlotChart;
import test.abmi.model.util.chart.CombinedManyCateogryPlotChart;
import test.abmi.model.util.chart.XYIntervalBarChart;
import test.abmi.model.util.chart.XYLineChart;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/graph-config.xml")
//@ImportResource({"classpath:/applicationContext.xml", "classpath:/blossom-servlet.xml"})
//@WebAppConfiguration

/**
 * Note: before run this, please comment out @session annotation from SingleCSVFileCreator.java and DownloadPackageGenerator.java
 * after finish, uncomment them.
 * @author Qiongyan
 *
 */
public class GraphTest {
	
	 
	
	 @BeforeClass
	    public static void setUpClass() throws Exception {
		
	    }

	    @AfterClass
	    public static void tearDownClass() throws Exception {
	    }

	    @Before
	    public void setUp() throws Exception {
	    }

	    @After
	    public void tearDown() throws Exception {
	    }

	    @Autowired
	    GraphService graphDao;
	    
	    @Test
	       public void testGraph() throws Exception{
	
	    
//	    	ChartBuilder chart = new ChartBuilder();
	    	
//	    	XYIntervalBarChart chart = new XYIntervalBarChart();
//	    	XYLineChart chart = new XYLineChart();
//	    	BarMultiMinMaxChart chart = new BarMultiMinMaxChart();
//	    	CombinedCateogryPlotChart chart = new CombinedCateogryPlotChart();
	    	
//	    	CombinedManyCateogryPlotChart chart = new CombinedManyCateogryPlotChart();
	    	
	    	
	    	graphDao.createGraphsForAllSpp();
//	    	graphDao.getAllGraphs(99003390, true); // few detection
//	    	graphDao.getAllGraphs(99001434, true);
	    	
	    	
	    	
	    }
}
