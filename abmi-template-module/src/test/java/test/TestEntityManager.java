package test;

import junit.framework.TestCase;


import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import abmi.model.services.rawdata.SingleCSVFileCreator;
import abmi.model.util.ModuleParameters;

//START SNIPPET: code
public class TestEntityManager extends TestCase {

	 private static EntityManager em = null;

	    @BeforeClass
	    public static void setUpClass() throws Exception {
	        if (em == null) {
	            em = (EntityManager) Persistence.createEntityManagerFactory(ModuleParameters.DATA_ENTITY_MANAGER).createEntityManager();
	        }
	    }

	    @AfterClass
	    public static void tearDownClass() throws Exception {
	    }

	    @Before
	    public void setUp() throws Exception {
	    }

	    @After
	    public void tearDown() throws Exception {
	    }

	    @Test
	    public void testAllOps(){
	        // Start a transaction
	    	  if (em == null) {
		            em = (EntityManager) Persistence.createEntityManagerFactory(ModuleParameters.DATA_ENTITY_MANAGER).createEntityManager();
		        }
	        em.getTransaction().begin();

	    

	        SingleCSVFileCreator singleCSVFileCreator = new SingleCSVFileCreator(em, "C:\\Users\\Qiongyan\\AppData\\Local\\Temp"); //new Integer(82), "RT_SITE_PHYCHAR","test");
	    	try{
	    		singleCSVFileCreator.getFile();
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    }
	

}
