jQuery( document ).ready(function( $ ) {
  
	// NO TRANSITIONS ON LOAD
	if( jQuery("body").hasClass("preload") ){ jQuery("body").removeClass("preload"); };
	// ---------------------

	// ==============================================

    if($('.postmeta.arrow').length){

			var arrowwidth = 0;
			
			$( '.postmeta.arrow' ).each(function( index ) {
				if( !$(this).prev().hasClass('photobox') ){
					arrowwidth = $(this).prev().width() + 40;
					$(this).css('border-width','10px '+arrowwidth+'px 0 0');
				}
			});
	    	

	};
	
	// ==============================================

	function initializeGmap( lat , lon, maptitle ) {

		var myLatlng = new google.maps.LatLng(lat , lon);
		
		var MY_MAPTYPE_ID = 'abmi_style';
		
		var mapOptions = {
			zoom: 14,
			center: myLatlng,
			mapTypeControlOptions: { mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID] }, 
			mapTypeId: MY_MAPTYPE_ID 
			};		
			
		var map = new google.maps.Map(document.getElementById('gmap-canvas'), mapOptions);
		
		var marker = new google.maps.Marker({ position: myLatlng, map: map, title: maptitle});
		
		var featureOpts = [
		    { stylers: [ { hue: '#74882E' }, { visibility: 'simplified' }, { gamma: 0.5 }, { weight: 0.5 } ] },
		    { elementType: 'labels', stylers: [ { visibility: 'on' } ] },
		    { featureType: 'water', stylers: [ { color: '#8AB0A3' } ] }
			];
		  
		var styledMapOptions = { name: 'ABMI' };
		
		var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
		
		map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
  
	}
	
	// ==============================================
		
	if(jQuery("#gmap-canvas").length ){
		
		initializeGmap($("#gmap-canvas").data('gmaplat'), $("#gmap-canvas").data('gmaplon') , 
				$("#gmap-canvas").data('gmaptitle') );
	}

	// ==============================================

	/* LOAD MAP VIA CLICK */
	jQuery("body").on("click", "a.gmap-content", function(event) {
	 	 	
	 	 event.preventDefault();
	 	 	
	 	 initializeGmap(  jQuery(this).data('gmaplat'), jQuery(this).data('gmaplon') , jQuery(this).data

('gmaptitle') );
	 	 
	 	 $('html, body').animate({scrollTop: parseInt($('#gmap-canvas').offset().top - 20)}, 800);
	 	 		
	});

	// ==============================================
	
	if($(".glossarycite").length){
		// tool tip on glossary list
			$(".glossarycite").each(function(index){
				 var currentGlossary = $(this);
				 var orgterm = $(this).text();
				 
				 var leadLetter = orgterm.substring(0,1);
				
	
					
				var url ="/.ajax/getGlossaryDetails?name=" + orgterm;
					$.get( url, function( data ) {
						var termname = orgterm.replace(/([. *+?^=!:${}()|\[\]\/\\])/g, "-");
						var term = data.name;
						$(currentGlossary).attr("title",  data.detail);
						$(currentGlossary).attr("id",  termname);
						
						$(".container p").replaceText(term, "<a link-to='" + termname+"'><strong>"+term+"</strong></a>");
						$(".container p").replaceText(term.toLowerCase(), "<a link-to='" + termname+"'><strong>"+term.toLowerCase()+"</strong></a>");
						$(".container strong").replaceText(term.toLowerCase(), "<a link-to='" + termname+"'>"+term.toLowerCase()+"</a>");
						$(".container li").replaceText(term.toLowerCase(), "<a link-to='" + termname+"'><strong>"+term.toLowerCase()+"</strong></a>");
						$(".container li").replaceText(term, "<a link-to='" + termname+"'><strong>"+term+"</strong></a>");
						$(".container td").replaceText(term.toLowerCase(), "<a link-to='" + termname+"'><strong>"+term.toLowerCase()+"</strong></a>");
						$(".container td").replaceText(term, "<a link-to='" + termname+"'><strong>"+term+"</strong></a>");
						
						
						},  "json");
					
			 })	
	
		// show tool tips on main content 
			$(".container").tooltip({
				items: "[link-to], [title]",
				content: function() {
						var element = $( this );
						if ( element.is( "[link-to]" ) ) {
						
						    var targetId = element.attr( "link-to" );
						
						   return $("#" + targetId).attr("title")
						
						  }
						
						  if ( element.is( "[title]" ) ) {
						
						    return element.attr( "title" );
						
						  }
				}
			});
	}
	// ==================================================
	if ($("#scheduler_here").length) {
		initcalendar();
	}
	
	if ($(".rawdatadownload").length) {
		initrawdatamenu();
	}
});// ON DOC READY


function fetchGlossary(term){
	var leadLetter = term.substring(0,1);
	var termname = term.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "-");

//	var url ="/.rest/properties/v1/website/home/glossary/leftColumnArea/"+leadLetter +"/GlossaryArea/" + termname + 
//
//"/detail";
	
	var url ="/.ajax/getGlossaryDetails?name=" + term;
	
	$.get( url, function( data ) {
		
		alert( data.values);
		},  "json");
	
	
	
}


function openSearch(e) {
	if(e=="Publications"){
		
		try {
			
			if ($("#gsc-i-id2").val() != $(" #q2").val()){ 
				$("#gsc-i-id2").val($("#q2").val());
			}
			// bind two input
			$("#gsc-i-id2").unbind();
			$("#gsc-i-id2").change(function() {

				$("#q2").val($("#gsc-i-id2").val());

			});

			if ($("#gsc-i-id2").val()){
				$("#publicationgoogle .gsc-search-button").click();
			}

			if ($("#csepublication").dialog("option", "width")) {
				$("#csepublication").dialog('open');
			} else {
				$("#csepublication").dialog({
					height : 600,
					width : 800,
					title : "Search ABMI.ca Publication"
				});
			}
		} catch (e) {
			$("#csepublication").dialog({
				height : 600,
				width : 800,
				title : "Search ABMI.ca Publication"
			});
		}
		
	}
	else{
		try {
			
			if ($("#gsc-i-id1").val() != $("#q").val()){ 
				$("#gsc-i-id1").val($("#q").val());
			}
			// bind two input
			$("#gsc-i-id1").unbind();
			$("#gsc-i-id1").change(function() {
	
				$("#q").val($("#gsc-i-id1").val());
	
			});
	
			if ($("#gsc-i-id1").val()){
				$(".gsc-search-button").click();
			}
	
			if ($("#cse").dialog("option", "width")) {
				$("#cse").dialog('open');
			} else {
				$("#cse").dialog({
					height : 600,
					width : 800,
					title : "Search ABMI.ca"
				});
			}
		} catch (e) {
			$("#cse").dialog({
				height : 600,
				width : 800,
				title : "Search ABMI.ca"
			});
		}
	}
}




// google track

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-55642625-1', 'auto');
ga('send', 'pageview');

