/* -----------------------------------------------------------------------


----------------------------------------------------------------------- */

function initMenuHandler() {

	/* GENERAL SECTION */
	$("#map-container").hide();
	$('.sub-sub-options').hide();
	$('.sub-optns').hide();
	$(".sub-slide-nav").hide();
	$(".slide-nav > .menu-child").hide();
    $(".slide-nav").hide();
    
    /* ----- .parent BTN ------- */
    $(".parent").click(function() {

		
		$(".slide-nav > .menu-child").hide();

        $(".slide-nav").animate({width: 'toggle'}, function(){
            $(".slide-nav > .menu-child").fadeToggle(200, "linear");    
        });
		
		if ($("#map-container").is(":visible")) {
			$("#map-container").fadeToggle(200, "linear");
		//map-container
		}

    });//click function
	/* ----- .parent BTN ------- */
    
    
	/* ----- .parent-gis-data BTN,  SIDENAV GIS DATA SUB MENU ------- */
	$(".parent-gis-data").click(function() {
		
		$(".gis-data").slideToggle(200, function(){
			if( $(".gis-data").css('display') == 'none' ){ $(".parent-gis-data > a > i").css({'background-position':'-48px 0px'}); }
			else{ $(".parent-gis-data > a > i").css({'background-position':'-28px 0px'}); }	
		});

	});//click function
	/* ----- .parent-gis-data BTN,  SIDENAV GIS DATA SUB MENU ------- */

	
	/* -----  a.first-child BTN,  SIDENAV SUB MENU ------- */			
    $("a.first-child").click(function(){
        
        var thisE = $(this);

		console.log('has child '+ thisE.prop('id'));
    
		thisE.addClass('sub-parent-active');
		
		thisE.next("div.sub-slide-nav").children(".menu-child").hide();

        thisE.next("div.sub-slide-nav").animate({width: 'toggle'}, function(){
            thisE.next("div.sub-slide-nav").children(".menu-child").fadeToggle(200, "linear", function(){
	            
	            $("a.first-child").each(function(){

					if( thisE.prop('id') != $(this).prop('id') ){
						
						if( $(this).hasClass('sub-parent-active') ){
							$(this).removeClass('sub-parent-active');
						}
						
						if( $(this).next("div.sub-slide-nav").is(":visible") ){
							
							var tnext = $(this).next("div.sub-slide-nav");
							
							tnext.children(".menu-child").fadeOut(100, function(){
								tnext.fadeOut(200);
							});
	
						}
						
					}//if not this
					
				});//for each cleanup
				
				//----------------------------------
				if ( (thisE.next("div.sub-slide-nav").is(":visible") && !$("#map-container").is(":visible")) || (!thisE.next("div.sub-slide-nav").is(":visible") && $("#map-container").is(":visible")) ) {
					$("#map-container").fadeToggle(200, "linear");
				//map-container
				}
	            //----------------------------------
	            			            
            });    
        });

	});//a.first-child.click function
	/* -----  a.first-child BTN,  SIDENAV SUB MENU ------- */
	
    
	// $(".slide-nav").hide();
	$('.parent').unbind("mouseover");

	$("li.level-2 div").hide();
	$('li.level-2 a').unbind("mouseover");
	$("li.level-2 ul.sub-optns").hide();

	$("li a.parent>a").mouseover(function() {
		if ($(".slide-nav").is(":hidden")) {
			$(".slide-nav").toggle();
			$('.parent').toggleClass('parent-active');

		} else {

		}
	});

	$("li a.parent>a").click(function(event) {
		event.preventDefault();
		if ($(".slide-nav").is(":hidden")) {
			$(".slide-nav").toggle();
			$('.parent').toggleClass('parent-active');

		}

		$(this).bind("mouseover");

	});
	/* END GENERAL SECTION */

	/* TERRESTRIAL SECTION */

	/* $("li.level-2> a").mouseover(function(event) {

		$(".sub-slide-nav").hide();
//		$(".sub-slide-nav").removeClass("sub-parent-active");

		$(this).parent().find(".sub-slide-nav").show();
		$(this).parent().find(".buttons-box").show();
//		$(this).parent().find(".sub-slide-nav").addClass("sub-parent-active");

	});
*/
	$("li.level-2>  a").click(
			function(event) {

				event.preventDefault();

				var isHidden = $(this).parent().find(".sub-slide-nav").is(
						":hidden");
				$(this).bind("mouseover");

				$(".sub-slide-nav").hide();
				$(".sub-slide-nav").removeClass("sub-parent-active");
				$(this).bind("mouseover");

				if (isHidden) {

					$(this).parent().find(".sub-slide-nav").show();
					$(this).parent().find(".buttons-box").show();
					$(this).parent().find(".sub-slide-nav").addClass(
							"sub-parent-active");
				}

			});

	$("input.groupinput").change(
			function() {

				if ($(this).is(':checked')) {
					$(this).parent().parent().find('.sub-optns').show();
				} else
					$(this).parent().parent().find('.sub-optns').hide();

				$(this).parent().parent().find('input[name=detail]').attr(
						'checked', false);
				$(this).parent().parent().find('input[name=detail]').parent()
						.removeClass('highlight', this.checked);
			}).change();

	$("input.regiongroupinput").change(
			// if one region selected, no other regions/subregion can be
			// selected.
			function() {

				if ($(this).is(':checked')) {// mark all siblings/children as
												// uncheck
					$(this).parent().parent().find('.sub-optns').show();
					$(this).parent().parent().siblings().find(
							'input[type=checkbox]').attr('checked', false);
					$(this).parent().parent().siblings().find(
							'input[type=checkbox]').removeClass('highlight');
					$(this).parent().parent().siblings().find('.sub-optns')
							.hide();
				} else
					$(this).parent().parent().find('.sub-optns').hide();
				
			
				
//				

			}).change();
	
	$("input.subregiongroupinput").change(
			// if one region selected, no other regions/subregion can be
			// selected.
			function() {

				if ($(this).is(':checked')) {// mark all siblings/children as
												// uncheck
					$(this).parent().parent().find('.sub-sub-options').show();
					
				} else
					$(this).parent().parent().find('.sub-sub-options').hide();
				
			
				
			
			
			}).change();

	$("input.select").click(
			function() {
				$(this).parent().parent().find("li input[type=checkbox]").prop(
						"checked", true);
				$(this).parent().parent().find("li label").addClass(
						'highlight', this.checked);

				var parentDiv = $(this).parent().parent();
				setTimeout(function() {
					parentDiv.find('.sub-optns').show();
				}, 180);

				setTimeout(function() {
					parentDiv.find('input[name=detail]').prop('checked', true);
					parentDiv.find('input[name=detail]').parent().addClass(
							'highlight', this.checked);
				}, 200);

			});

	$("input.clear").click(
			function() {
				$(this).parent().parent().find("li input[type=checkbox]").prop(
						"checked", false);
				$(this).parent().parent().find("li label").removeClass(
						'highlight', this.checked);

				var parentDiv = $(this).parent().parent();
				setTimeout(function() {
					parentDiv.find('.sub-optns').hide();
				}, 180);

				setTimeout(
						function() {
							parentDiv.find('input[name=detail]').prop(
									'checked', false);
							parentDiv.find('input[name=detail]').parent()
									.removeClass('highlight', this.checked);
						}, 200);

			});

	$('.data-form').on('change', 'input:checkbox', function() {
		$(this).parent().toggleClass('highlight', this.checked);
	});

	$("#getRawdataBtn").click(
			function() {
				// get parameters
				var params = getCheckBoxResult("region")
						+ getCheckBoxResult("rotation")
						+ getCheckBoxResult("offgridId", "coreoffgrid")
						+ getCheckBoxResult("rawdata", "category");

				// send request
				SendAjax("/.ajax/getRawdataFile", params, getRawdata);
				// download files
			});

}

function getRawdata(data) {
	var htmlStr = '<a class="download-button" href="/FileDownloadServlet?file="' + data.file +'>Raw Data File <em>(zip format ' + data.size + ')</em></a>';
	$('#download-data-area').fadeOut( 400, function() {	
		$('#download-data-area').html( htmlStr).fadeIn( 800 );  
	});
}

function getCheckBoxResult(param, name) {
	var selected = "";

	var divname = (name ? name : param);
	$("#rawdatadownload input[name=" + divname + "]:checked").each(function() {
		var idStr = $(this).attr("id");
		var idInt = idStr.substring(divname.length, idStr.length);
		selected += param + "=" + idInt + "&";
	});
	return selected;
}

function initrawdatamenu() {

	/* use ajax get menu and display them */
	

	$("#rawdatadownload").appendTo($(".navigation li a.parent").parent());

	var ajax1 = SendAjax("/.ajax/getDataCategory", null, addDataCategory);
	var ajax2 = SendAjax("/.ajax/getRegion", null, addRegion);
	var ajax3 = SendAjax("/.ajax/getRotation", null, addRotation);
	var ajax4 = SendAjax("/.ajax/getOffgridOption", null, addOffgridOption);

	$.when(ajax1, ajax2, ajax3, ajax4).done(

	initMenuHandler);
}

function SendAjax(url, param, callback) {
	var def = $.Deferred();
	return $.ajax({
		type : "GET",

		url : url,
		data: param,

		dataType : "json",
		success : function(data) {
			callback(data);
			def.resolve();
		}
	});
	return def.promise();
}

function addRotation(data) {
	for ( var i = 0; i < data.length; i++) {
		// region and name
		var contentDiv = $("#rotationdetail");
		var row = data[i];
		var innerHtml = "<li >  <label for='rotation" + row.id
				+ "' class='radio-item'>" + "<input type='checkbox' value='"
				+ row.id + "' name='rotation' id='rotation" + row.id + "'>"
				+ "<span class='outer'><span class='inner'></span></span>"
				+ row.name;

		innerHtml += "</label>";

		innerHtml += "</li> ";

		$(contentDiv).append(innerHtml);
	}

}

function addOffgridOption(data) {
	for ( var i = 0; i < data.length; i++) {
		// region and name
		var contentDiv = $("#datadetail");
		var row = data[i];
		var innerHtml = "<li >  <label for='coreoffgrid" + row.id
				+ "' class='radio-item'>" + "<input type='radio' value='"
				+ row.id + "' name='coreoffgrid' id='coreoffgrid" + row.id
				+ "'>"
				+ "<span class='outer'><span class='inner'></span></span>"
				+ row.name;

		innerHtml += "</label>";

		innerHtml += "</li> ";

		$(contentDiv).append(innerHtml);
	}

}

function addDataCategory(data) {
	// alert("add data!");
	for ( var i = 0; i < data.length; i++) {
		// protocol and name
		var currentProtocol = data[i].protocol.toLowerCase();
		var currentProtocolDiv = "#category-list ." + currentProtocol
				+ "-slide-nav .data-form > ul";
		if ($(currentProtocolDiv).length == 0) {// need to create first wetland
												// and terrestrial

			var innerHtml = "<li class='level-2'><a class='"
					+ currentProtocol
					+ "' href='javascript:;'>"
					+ data[i].protocol.toUpperCase()
					+ "</a>"
					+ "<div class='"
					+ currentProtocol
					+ "-slide-nav sub-slide-nav'><form class='data-form'><ul></ul>";

			innerHtml += "<div class='buttons-box'> <input type='button' value='SELECT ALL' class='button select' id='select-wetland'>"
					+ "               <input type='reset' value='CLEAR ALL' class='button clear' id='clear-wetland'> "
					+ "<input type='submit' value='ACCEPT' class='button accept'>"
					+ "</div>";
			innerHtml += "</form></div>";
			$("#category-list").append(innerHtml);
		}

		// species habitat lititle
		if ($("#category-list ." + currentProtocol + "-slide-nav li."
				+ data[i].name).length == 0) {
			var innerHtml = "<li class='" + data[i].name + " liTitle '> "
					+ data[i].name + "</li><ul class='ul" + data[i].name
					+ "'></ul>";

			$(currentProtocolDiv).append(innerHtml);
		}

		// already exist then add data category
		for ( var j = 0; j < data[i].detail.length; j++) {
			var subData = data[i].detail[j];
			var innerHtml = "<li>  <label for='category"
					+ subData.id
					+ "' class='radio-item'>"
					+ "<input type='checkbox' value='' name='category' id='category"
					+ subData.id + "'>"
					+ "<span class='outer'><span class='inner'></span></span> "
					+ subData.name + "</label> " + "</li>";

			// append below the liTitle <species habitat>
			$(
					"#category-list ." + currentProtocol + "-slide-nav ul.ul"
							+ data[i].name).append(innerHtml);

		}

	}

}

function addRegion(data) {

	for ( var i = 0; i < data.length; i++) {
		// region and name
		var regionDiv = "#regiondetail";
		var row = data[i];
		var innerHtml = "<li id='li"
				+ row.id
				+ "'>  <label for='rl"
				+ row.id
				+ "' class='radio-item'>"
				+ "<input type='checkbox' value='' class='regiongroupinput' name='regionBig' id='rl"
				+ row.id + "'>"
				+ "<span class='outer'><span class='inner'></span></span>"
				+ row.name;

		if (row.subregion.length) {
			innerHtml += " <i class='arrow-small'></i> ";
		}
		innerHtml += "</label>";
		if (row.subregion.length) {
			innerHtml += "<ul class='sub-options1 sub-optns' style='display: none;'></ul>";
		}
		innerHtml += "</li> ";

		$(regionDiv).append(innerHtml);

		var subregionDiv = "#li" + row.id + "> ul";
		// already exist then add data category
		for ( var j = 0; j < row.subregion.length; j++) {
			var subData = row.subregion[j];
			var innerHtml = "<li>  <label for='region"
					+ subData.id
					+ "' class='radio-item'>"
					+ "<input type='checkbox' value='' name='region' id='region"
					+ subData.id + "'>"
					+ "<span class='outer'><span class='inner'></span></span> "
					+ subData.name + "</label> " + "</li>";
			
			if (subData.parentid ){
			if ($( " #parentregion" + subData.parentid).length  ==0 ){
				// need to add a parent div to hold subregions
				var innerParentHtml = "<li >  <label for='parentregion"
					+ subData.parentid
					+ "' class='radio-item'>"
					+ "<input type='checkbox' class='subregiongroupinput' value='' name='regionparent' id='parentregion"
					+ subData.parentid + "'>"
					+ "<span class='outer'><span class='inner'></span></span> <span class='sub-expand-title'>  "
					+ subData.parentname + "</span> <i class='arrow-xsmall'></i></label><ul class='sub-sub-options' id='parentid" + subData.parentid + "' style='display:none;'></ul></li>";
				
				$(subregionDiv).append(innerParentHtml);
				
			}
			$( "#parentid" + subData.parentid  ).append(innerHtml);
			
			}
			else 
			
			$(subregionDiv).append(innerHtml);

		}

	}

}