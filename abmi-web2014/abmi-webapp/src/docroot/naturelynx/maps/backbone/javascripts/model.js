
/**
 *  Model
 */


window.Content = Backbone.Model.extend({
	urlRoot: './backbone',
	
	defaults: {
		writer:'sensorup',
		subject:'',
		content:''
	},
	
	initialize : function(){
		this.on('add', this.addModel);
		this.on('change:subject', this.updateSubject);
		this.on('change:content', this.updateContent);
		this.on('remove', this.removeModel);
	},
	
	addModel : function(model){
		model.save({},{contentType : 'application/json',
			success:function(result){
				var json = result.toJSON();
				console.log(json);
				model.set({'id': json.id});
			},
			error:function(e){
				alert('error occurs. the order isn\'t completed');
			}
		});
	},
	
	updateSubject:function(model){
		model.save({},{contentType : 'application/json',
			error: function(){
				alert('error occurs. the order isn\'t completed');
			}
		});
	},
	
	updateContent:function(model){
		model.save({},{contentType : 'application/json',
			error: function(error){
				alert('error occurs. the order isn\'t completed');
			}
		});
	},
	
	removeModel:function(model){
		model.destroy({
			contentType:'application/json', 
			success : function(result){
				
			},
			error:function(e){
				alert('error occurs. the order isn\'t completed');
			} 
		});
	}
});


var Contents = Backbone.Collection.extend({
	model : Content,
	url: './backbone/list'
});

