
/**
 *  Viewer.
 */

window.ListView = Backbone.View.extend({
	template: _.template("<tr id='tr-<%=id%>'><td><%=no%></td><td class='tbl-subject' id='sub-<%=id%>'><%=subject%></td><td class='tbl-content' id='con-<%=id%>'><%=content%></td><td><%=writer%></td><td><button class='btn btn-danger removeBtn' id='btn-<%=id%>'><i class='icon-remove icon-white'></i></button></td></tr>"),
	
	initialize: function(options){
		this.options.models.on('reset', this.reload, this);
	},
	
	render : function(model, no){
		$('#tableView').prepend(
			this.template({
				no : no,
				subject : model.get('subject'), 
				content : model.get('content'),
				id : model.get('id'), 
				writer: model.get('writer')}
			)
		);
	},
	
	
	events :{
		"click #contentSubmitBtn": "submitContent",
		"click .removeBtn" : "removeContent",
		"dblclick .tbl-subject" : "changeElement",
		"dblclick .tbl-content" : "changeElement",
		"focusout .sub-convert" : "updateDone",
		"focusout .con-convert" : "updateDone"
	},
	
	
	reload:function(models){
		var contents = models.models;
		
		for(var i = 1;  i <= contents.length ; i++){
			this.render(contents[i-1], i);
		}
	},
	
	
	removeContent : function(e){
		var elem = e.currentTarget;
		var contentId = elem.id.replace('btn-', '');
		
		var collection = this.options.models;
		var removalModel = collection.get(contentId);
		collection.remove(removalModel);
		
		$('#tr-'+contentId).remove();
	},
	
	
	submitContent : function(e){
		
		var subject = $('input[name="subject"]').val();
		var content = $('textarea[name="content"]').val();
		var model = new Content({subject:subject, content:content});
		
		this.options.models.add(model);
		this.render(model, this.options.models.length);
	},
	
	
	changeElement : function(e){
		var elem = $('#'+e.currentTarget.id);
		var type;
		elem.hasClass('tbl-subject')? (type='sub') : (elem.hasClass('tbl-content')? type='con' : type=''); 
		
		var original = elem.text();
		elem.text('');
		
		elem.append($('<input type="text" class="'+ type +'-convert" id="' + elem.attr('id').replace(type, 'con-'+type) + '" value="'+original+'"/>'));
	},
	
	
	updateDone : function(e){
		var elem = $('#'+e.currentTarget.id);
		var type;
		elem.hasClass('sub-convert')? (type='sub') : (elem.hasClass('con-convert')? type='con' : type='');
		
		var newValue = elem.val();
		var target = $('#' + elem.attr('id').replace('con-'+type, type));
		target.text(newValue);
		
		var collection = this.options.models;
		var content = collection.get(target.attr('id').replace(type+'-', ''));

		if(type == 'sub')
			content.set({subject : newValue});
		else if(type == 'con')
			content.set({content : newValue});
	}
	
});

