// JavaScript Document

							
$( window ).resize(function() {
	
	/*Section and images Height*/
	$( ".parent" ).each(function( index, element ) {
		var child_H = $( element ).outerHeight();
		$(this).find( ".child" ).css("height", child_H);	
    });        
	if($(window).width() >1200){
		$( ".parenth" ).each(function( index, element ) {
			var childH = $( element ).outerHeight();
			$(this).find( ".childh" ).css("height", childH);	
	    });
	}
	if($(window).width() >=767 && $(window).width() <1200){
		$('.home-half-box-image-background').css("height", "auto");
		
		var min_height = $('.home-half-box-color-background').outerHeight();
		$('.home-half-box-color-background .bg-block').css("height", min_height);
		var min_height2 = $('.home-sec2').outerHeight();
		
	}

});

$(window).load(function() {
	/*Home page Slider*/	
	$('.flexslider').flexslider({
		animation: "fade",
		controlNav: true,
		directionNav: false
	});
	
	
 });


 $(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });

jQuery(document).ready(function($) {



	//Search Module
	$(".search-area").hide();
	$('.search-link').on('click', function(event){
		if($(this).hasClass('search-icon')) {
			$(".search-link").removeClass("search-icon").addClass("backTomap");
			$(".map-area").hide();
			$(".search-area").show();
		}else{
			$(".search-link").addClass("search-icon").removeClass("backTomap");
			$(".map-area").show();
			$(".search-area").hide();
		}
	});
	//search toggle
	$(".searchdata").hide();
	jQuery(".searchBy").click(function () {
		jQuery(this).toggleClass("open");
		jQuery(this).next(".searchdata").slideToggle();
	});
	
	$('.counter').counterUp({
        delay: 50,
        time: 1000
    });
    
	/*Section and images Height*/
	$( ".parent" ).each(function( index, element ) {
		var child_H = $( element ).outerHeight();
		$(this).find( ".child" ).css("height", child_H);	
    });

	if($(window).width() >1200){
		$( ".parenth" ).each(function( index, element ) {
			var childH = $( element ).outerHeight();
			$(this).find( ".childh" ).css("height", childH);	
	    });
			
	}
	if($(window).width() >767 && $(window).width() <1200){
		$('.home-half-box-image-background').css("height", "auto");
	
		var min_height = $('.home-half-box-color-background').outerHeight();
		$('.home-half-box-color-background .bg-block').css("height", min_height);
		var min_height2 = $('.home-sec2').outerHeight();
	
	}
	
	/*Search show/Hide*/
	var $ctsearch = $( '#ct-search' ),
		$ctsearchinput = $ctsearch.find('input.ct-search-input'),
		$body = $('html,body'),
		openSearch = function() {
			$ctsearch.data('open',true).addClass('ct-search-open');
			$ctsearchinput.focus();
			return false;
		},
		closeSearch = function() {
			$ctsearch.data('open',false).removeClass('ct-search-open');
		};

	$ctsearchinput.on('click',function(e) { e.stopPropagation(); $ctsearch.data('open',true); });
	$ctsearch.on('click',function(e) {
		e.stopPropagation();
		if( !$ctsearch.data('open') ) {

			openSearch();

			$body.off( 'click' ).on( 'click', function(e) {
				closeSearch();
			} );

		}
		else {
			if( $ctsearchinput.val() === '' ) {
				closeSearch();
				return false;
			}
		}
	});
	
	
});

/*Navigarion Position on Scroll*/
var $j = jQuery;
						
window.navi = {
	fixed: false,
	visible: true,
	lastST: 0,
	lastSTup: 0
};
window.placeholder_mobile = false;

$j(document).ready(function(){
	window.navi.elm = $j("#header-box");
	
	$j(window).on("scroll", function(event) {
		var navH = $("nav").outerHeight();
		var st = $j(this).scrollTop();
		if (st > navH && !window.navi.fixed) {
			window.navi.fixed = true;
			window.navi.elm.addClass("fixed");
			
		} else if (st < 10 && window.navi.fixed) {
			window.navi.fixed = false;
			window.navi.visible = true;
			window.navi.elm.removeClass("fixed invisible visible").css("position", "absolute");
			window.setTimeout('window.navi.elm.css("position", ""); ', 1); // css position wegen safari in ios7
		}
		var test = true;
		if (st > navH && window.navi.visible && (st - window.navi.lastST > 0)) {
			window.navi.visible = false;
			test = false;
			window.setTimeout('window.navi.elm.addClass("invisible").removeClass("visible"); ', 1);
		}
		if (!window.navi.visible && (st - window.navi.lastSTup < -100) && test) {
			window.navi.visible = true;
			window.navi.elm.addClass("visible").removeClass("invisible");
		}
		if (st - window.navi.lastST > 0)
			window.navi.lastSTup = st;
			window.navi.lastST = st;
	});

});



//Sigin/ Sign up Modal window

jQuery(document).ready(function($){
	var $form_modal = $('.sy-user-modal'),
		$form_login = $form_modal.find('#sy-login'),
		$form_signup = $form_modal.find('#sy-signup'),
		$form_forgot_password = $form_modal.find('#sy-reset-password'),
		$form_modal_tab = $('.sy-switcher'),
		$tab_login = $form_modal_tab.children('li').eq(0).children('a'),
		$tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
		$forgot_password_link = $form_login.find('a.forgot-password'),
		$back_to_login_link = $form_forgot_password.find('a.back-to-login'),
		$main_nav = $('.sign-up-in');

	//open modal
	$main_nav.on('click', function(event){

		if( $(event.target).is($main_nav) ) {
			// on mobile open the submenu
			$(this).children().toggleClass('is-visible');
		} else {
			// on mobile close submenu
			$main_nav.children().removeClass('is-visible');
			//show modal layer
			$form_modal.addClass('is-visible');	
			//show the selected form
			( $(event.target).is('.sy-signup') ) ? signup_selected() : login_selected();
		}

	});

	//close modal
	$('.sy-user-modal').on('click', function(event){
		if( $(event.target).is($form_modal) || $(event.target).is('.sy-close-form') ) {
			$form_modal.removeClass('is-visible');
		}	
	});
	//close modal when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$form_modal.removeClass('is-visible');
	    }
    });

	//switch from a tab to another
	$form_modal_tab.on('click', function(event) {
		event.preventDefault();
		( $(event.target).is( $tab_login ) ) ? login_selected() : signup_selected();
	});

	//show forgot-password form 
	$forgot_password_link.on('click', function(event){
		event.preventDefault();
		forgot_password_selected();
	});

	//back to login from the forgot-password form
	$back_to_login_link.on('click', function(event){
		event.preventDefault();
		login_selected();
	});

	function login_selected(){
		$form_login.addClass('is-selected');
		$form_signup.removeClass('is-selected');
		$form_forgot_password.removeClass('is-selected');
		$tab_login.addClass('selected');
		$tab_signup.removeClass('selected');
	}

	function signup_selected(){
		$form_login.removeClass('is-selected');
		$form_signup.addClass('is-selected');
		$form_forgot_password.removeClass('is-selected');
		$tab_login.removeClass('selected');
		$tab_signup.addClass('selected');
	}

	function forgot_password_selected(){
		$form_login.removeClass('is-selected');
		$form_signup.removeClass('is-selected');
		$form_forgot_password.addClass('is-selected');
	}

	//IE9 placeholder fallback
	//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
	if(!Modernizr.input.placeholder){
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
		  	}
		}).blur(function() {
		 	var input = $(this);
		  	if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.val(input.attr('placeholder'));
		  	}
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
		  	$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
			 		input.val('');
				}
		  	})
		});
	}

});

jQuery.fn.putCursorAtEnd = function() {
	return this.each(function() {
    	// If this function exists...
    	if (this.setSelectionRange) {
      		// ... then use it (Doesn't work in IE)
      		// Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
      		var len = $(this).val().length * 2;
      		this.setSelectionRange(len, len);
    	} else {
    		// ... otherwise replace the contents with itself
    		// (Doesn't work in Google Chrome)
      		$(this).val($(this).val());
    	}
	});
};
//--------------------------------------------
//  Banner image parallax effect
//--------------------------------------------

/* detect touch */
if("ontouchstart" in window){
    document.documentElement.className = document.documentElement.className + " touch";
}
if(!$("html").hasClass("touch")){
    /* background fix */
    $(".parallax").css("background-attachment", "scroll");
}

/* fix vertical when not overflow
call fullscreenFix() if .fullscreen content changes */
function fullscreenFix(){
    var h = $('body').height();
    // set .fullscreen height
    $(".content-b").each(function(i){
        if($(this).innerHeight() <= h){
            $(this).closest(".fullscreen").addClass("not-overflow");
        }
    });
}
$(window).resize(fullscreenFix);
fullscreenFix();

/* resize background images */
function backgroundResize(){
    var windowH = $(window).height();
    $(".background").each(function(i){
        var path = $(this);
        // variables
        var contW = path.width();
        var contH = path.height();
        var imgW = path.attr("data-img-width");
        var imgH = path.attr("data-img-height");
        var ratio = imgW / imgH;
        // overflowing difference
        var diff = parseFloat(path.attr("data-diff"));
        diff = diff ? diff : 0;
        // remaining height to have fullscreen image only on parallax
        var remainingH = 0;
        if(path.hasClass("parallax") && !$("html").hasClass("touch")){
            var maxH = contH > windowH ? contH : windowH;
            remainingH = windowH - contH;
        }
        // set img values depending on cont
        imgH = contH + remainingH + diff;
        imgW = imgH * ratio;
        // fix when too large
        if(contW > imgW){
            imgW = contW;
            imgH = imgW / ratio;
        }
        //
        path.data("resized-imgW", imgW);
        path.data("resized-imgH", imgH);
        path.css("background-size", imgW + "px " + imgH + "px");
    });
}
$(window).resize(backgroundResize);
$(window).focus(backgroundResize);
backgroundResize();

/* set parallax background-position */
function parallaxPosition(e){
    var heightWindow = $(window).height();
    var topWindow = $(window).scrollTop();
    var bottomWindow = topWindow + heightWindow;
    var currentWindow = (topWindow + bottomWindow) / 2;
    $(".parallax").each(function(i){
        var path = $(this);
        var height = path.height();
        var top = path.offset().top;
        var bottom = top + height;
        // only when in range
        if(bottomWindow > top && topWindow < bottom){
            var imgW = path.data("resized-imgW");
            var imgH = path.data("resized-imgH");
            // min when image touch top of window
            var min = 0;
            // max when image touch bottom of window
            var max = - imgH + heightWindow;
            // overflow changes parallax
            var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow; // fix height on overflow
            top = top - overflowH;
            bottom = bottom + overflowH;
            // value with linear interpolation
            var value = min + (max - min) * (currentWindow - top) / (bottom - top);
            // set background-position
            var orizontalPosition = path.attr("data-oriz-pos");
            orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
            $(this).css("background-position", orizontalPosition + " " + value + "px");
        }
    });
}
if(!$("html").hasClass("touch")){
    $(window).resize(parallaxPosition);
    //$(window).focus(parallaxPosition);
    $(window).scroll(parallaxPosition);
    parallaxPosition();
}


