/**/
var gRootPath = "http://136.159.118.70:8080/NatureLynxV03/";

$( document ).ready(function(){
		if ($("#sppIded").length) {
			displayHomeSppNum	();
		}
		
	}
);

function displayHomeSppNum(){
	
	$.ajax({
			method: "get",	
			url: gRootPath + "getSpeciesIdentified",
			dataType: "json",
			crossDomain: true		})

		  .done(function( data ) {

			  $("#sppIded").text(data);
			  
			  $('#sppIded').counterUp({
			        delay: 50,
			        time: 1000
			    });

		  });


	$.ajax({
		method: "get",	
		url: gRootPath + "getSpeciesVerified",
		dataType: "json",
		crossDomain: true})

	  .done(function( data ) {

		  $("#sppVerified").text(data);
		  $('#sppVerified').counterUp({
		        delay: 50,
		        time: 1000
		    });

	  });
}