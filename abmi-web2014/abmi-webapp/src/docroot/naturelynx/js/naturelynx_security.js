$(document).ready(function() {
	$("#signin").bind("click", ajaxLogin);
});
function ajaxLogin() {

 var username = $("#username").val();
 var password = $("#password").val();
 var rememberme = $("#remember-me:checked").val();
 $(".sy-user-error-message").text("");
 $(".sy-pwd-error-message").text("");
 $(".sy-error-message").removeClass("is-visible");
 
 if (username === undefined || username == null || username== ""){
	$(".sy-user-msg").text("User name can't be empty");
	$(".sy-user-msg").addClass("is-visible");
	 return;
 }
 
 if (password === undefined || password == null || password== ""){
		$(".sy-pwd-msg").text("password can't be empty");
		$(".sy-pwd-msg").addClass("is-visible");
		 return;
	 }
 
	// Ajax login - we send credentials to j_spring_security_check (as in form
	// based login
	$
			.ajax({
				url : "/j_spring_security_check",
				data : {
					j_username :username,
					j_password :password,
					"remember-me": rememberme
				},
				type : "POST",
				beforeSend : function(xhr) {
					xhr.setRequestHeader("X-Ajax-call", "true");
				},
				success : function(result) {
					// if login is success, hide the login modal and
					// re-execute the function which called the protected
					// resource
					// (#7 in the diagram flow)
					if (result == "ok") {

											console.log("ok");
						return true;
					} else {

						$("#ajax_login_error_" + suffix)
								.html(
										'<span  class="alert display_b clear_b centeralign">Bad user/password</span>');
						return false;
					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					$("#ajax_login_error_" + suffix).html("Bad user/password");
					return false;
				}
			});
}
