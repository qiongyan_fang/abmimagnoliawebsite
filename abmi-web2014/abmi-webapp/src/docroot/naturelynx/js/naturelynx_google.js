google.load('search', '1', {
	language : 'en',
	style : google.loader.themes.MINIMALIST
});
google.setOnLoadCallback(function() {
	var customSearchOptions = {};
	// customSearchOptions['overlayResults'] = true;
	var customSearchControl = new google.search.CustomSearchControl(
			'006207892962066587082:sakmm7npo-g', customSearchOptions);
	customSearchControl
			.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
	customSearchControl.setLinkTarget(GSearch.LINK_TARGET_PARENT);
	var options = new google.search.DrawOptions();
	customSearchControl.draw('cse', options);
}, true);

function openSearch(e) {

	try {

		if ($("#gsc-i-id1").val() != $("#q").val()) {
			$("#gsc-i-id1").val($("#q").val());
		}
		// bind two input
		$("#gsc-i-id1").unbind();
		$("#gsc-i-id1").change(function() {

			$("#q").val($("#gsc-i-id1").val());

		});

		if ($("#gsc-i-id1").val()) {
			$(".gsc-search-button").click();
		}

		if ($(".ui-dialog #cse").length) {
			$("#cse").dialog('open');
		} else {
			$("#cse").dialog({

				width : 800,
				height: 700,
				responsive : true,
				
				position: { my: "center", at: "center", of: window },
				title : "Search Naturalynx.ca"
			});
		}
	} catch (e) {
		$("#cse").dialog({
			width : 800,
			responsive : true,
		
			height: 700,
			position: { my: "center", at: "center", of: window },
			title : "Search Naturalynx.ca"
		});
	}

}