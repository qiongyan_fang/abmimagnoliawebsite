// JavaScript Document
	
jQuery(document).ready(function($) {
	
	
	
	//--------------------------------------------
	// loadmore
	//--------------------------------------------
	
			
			
	//--------------------------------------------
	// Section and images Height
	//--------------------------------------------
	/*Section and images Height*/


	$( ".parent" ).each(function( index, element ) {
		var child_H = $( element ).outerHeight();
		$(this).find( ".child" ).css("height", child_H);	
    });
	if($(window).width() >1200){
		$( ".parenth" ).each(function( index, element ) {
			var childH = $( element ).outerHeight();
			$(this).find( ".childh" ).css("height", childH);	
	    });	
	}
	if($(window).width() >=767 && $(window).width() <1200){
		$('.home-half-box-image-background').css("height", "auto");
		
		var min_height = $('.home-half-box-color-background').outerHeight();
		$('.home-half-box-color-background .bg-block').css("height", min_height);
		var min_height2 = $('.home-sec2').outerHeight();
		
	}

	if($(window).width() >991){
		$( ".sech" ).each(function( index, element ) {
			var secChildh = $( element ).outerHeight();
			$(this).find( ".secChildh" ).css("height", secChildh);	
	    });	
	}

	//--------------------------------------------
	// Side nav
	//--------------------------------------------
	$('ul.sidenav > li').each(function() {
		if($(this).hasClass("open")){
			$(this).find("ul").show();
		}else{
			$(this).find("ul").hide();
			
		}
	  });
	$('ul.sidenav li').click(function(event) {
        event.stopPropagation();
        $(this).toggleClass("open");
        $('> ul', this).toggle();

    });
    
    //--------------------------------------------
	// FancyBox
	//--------------------------------------------
	$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		width		: '70%',
		height		: '70%',
		openEffect  : 'fade',
		closeEffect : 'fade',
		helpers : {
			media : {}
		}
	});
	
	$(".facybox").fancybox({
	    helpers : {
		        title: {
		            type: 'inside',
		            position: 'top'
		        }
		    },
	    nextEffect: 'fade',
	    prevEffect: 'fade',
	    afterShow   : addLinks
	});
	function addLinks() {
		    var list = $("#links");
		    
		    if (!list.length) {    
		        list = $('<ul id="links">');
		    
		        for (var i = 0; i < this.group.length; i++) {
		            $('<li data-index="' + i + '"><label></label></li>').click(function() { $.fancybox.jumpto( $(this).data('index'));}).appendTo( list );
		        }
		        
		        list.appendTo( '.fancybox-wrap' );
		    }
		
		    list.find('li').removeClass('active').eq( this.index ).addClass('active');
		}
		
		function removeLinks() {
		    $("#links").remove();    
		}
	$('.open-modalwindow').click(function(e) {
	    var el, id = $(this).data('open-id');
	    if(id){
	        el = $('.facybox[rel=' + id + ']:eq(0)');
	        e.preventDefault();
	        el.click();
	    }
	}); 

    //--------------------------------------------
	// Commonly Mistaken
	//--------------------------------------------
    $(".mistaken-btn").click(function () {
        $(".mistaken-list").slideToggle();
        $(this).toggleClass("open");
    });
    
	//--------------------------------------------
	// Comment section
	//--------------------------------------------
	
		jQuery(".comment-reply-link").click(function () {
			jQuery(this).next(".comment-reply").slideToggle();
		});
		
		jQuery(".comment-reply-link").click(function () {
			jQuery(this).parents('.comment-actions').next(".comment-reply").slideToggle();
		});
		
	
	//--------------------------------------------
	// Date and Time picker
	//--------------------------------------------
		//datepicker and timepicker bootstrap
		$('#datepicker, .jqdatepicker').datepicker({
			format: "dd/mm/yyyy",
			keyboardNavigation: false,
			autoclose: true,
			
		}); 
		$('#timepicker, .jqtimepicker').timepicker();
		
		 $("#guideBtn").popover({
	        html : true,
	        placement : 'top'
	    });
		
	//--------------------------------------------
	// Home page Couner
	//--------------------------------------------
	$('.counter').counterUp({
        delay: 50,
        time: 1000
    });
    
    
    //--------------------------------------------
	// Join Button
	//--------------------------------------------
	jQuery(".join-btn").click(function () {
		 $(this).html('Member');
	})
	
	//--------------------------------------------
	// Add LYNX Button
	//--------------------------------------------
	jQuery(".addlynx-btn").click(function () {
		
		$(this).html('Added').addClass("lynxadded");
	})
	
	//--------------------------------------------
	// Add Button
	//--------------------------------------------
	jQuery(".cellAction .add").click(function () {
		$(this).removeClass("add");
		$(this).addClass("added");
	})
	//--------------------------------------------
	//Search show/Hide
	//--------------------------------------------
	var $ctsearch = $( '#ct-search' ),
		$ctsearchinput = $ctsearch.find('input.ct-search-input'),
		$body = $('html,body'),
		openSearch = function() {
			$ctsearch.data('open',true).addClass('ct-search-open');
			$ctsearchinput.focus();
			return false;
		},
		closeSearch = function() {
			$ctsearch.data('open',false).removeClass('ct-search-open');
		};

	$ctsearchinput.on('click',function(e) { e.stopPropagation(); $ctsearch.data('open',true); });
	$ctsearch.on('click',function(e) {
		e.stopPropagation();
		if( !$ctsearch.data('open') ) {

			openSearch();

			$body.off( 'click' ).on( 'click', function(e) {
				closeSearch();
			} );

		}
		else {
			if( $ctsearchinput.val() === '' ) {
				closeSearch();
				return false;
			}
		}
	});
	
	//--------------------------------------------
	// Mission Page
	//--------------------------------------------

	$('.select-guide').click(function(e) {
	    e.preventDefault(); //prevent the link from being followed
	    $('.select-guide').removeClass('selected');
	    $(this).addClass('selected');
	});
    
    $('#filterByAlphabet').listnav({
	    filterSelector: '.profileName',
	    includeNums: false,
	    showCounts: false,
	    noMatchText: 'There are no matching entries.'
	});
	
	//--------------------------------------------
	// Sightings Page
	//--------------------------------------------
	
	//Layout switch
		/*$(".list-area").hide();
		$(".layout-icons a.globe").on('click', function(event){
		    $('.layout-icons a.list').removeClass('selected');
		    $(this).addClass('selected');
		    $(".map-area").show();
		    $(".list-area").hide();
		});
		$(".layout-icons a.list").on('click', function(event){
		    $('.layout-icons a.globe').removeClass('selected');
		    $(this).addClass('selected');
		    $(".map-area").hide();
		    $(".list-area").show();
		});
		
		*/
		//Search Module
		
		/*$('.search-link').on('click', function(event){
			if($(this).hasClass('search-icon')) {
				$(".search-link").removeClass("search-icon").addClass("backTolayout");
				$(".map-area").hide();
				$(".list-area").hide();
				$(".layout-icons a").css({"pointer-events": "none"});
				$(".search-area").show();
			}else{
				$(".search-link").addClass("search-icon").removeClass("backTolayout");
				if($(".layout-icons a.globe").hasClass('selected')) {
					$(".map-area").show();
				}else{
					$(".list-area").show();
				}
				$(".layout-icons a").css({"pointer-events": "auto"});
				$(".search-area").hide();
			}
		});*/

});  //End Document Ready 

//***********************************************************************************************************************//

$( window ).resize(function() {
	$( ".parent" ).each(function( index, element ) {
		var child_H = $( element ).outerHeight();
		$(this).find( ".child" ).css("height", child_H);	
    });
	if($(window).width() >1200){
		$( ".parenth" ).each(function( index, element ) {
			var childH = $( element ).outerHeight();
			$(this).find( ".childh" ).css("height", childH);	
	    });	
	}
	if($(window).width() >767 && $(window).width() <1200){
		$('.about-sec').css("height", "auto");
		$('.toknow-sec').css("height", "auto");
		var min_height = $('.mission-sec').outerHeight();
		$('.mission-sec .bg-block').css("height", min_height);
		var min_height2 = $('.home-sec2').outerHeight();
		$('.identify-sec .bg-block').css("height", min_height);
	}
	if($(window).width() >991){
		$( ".sech" ).each(function( index, element ) {
			var secChildh = $( element ).outerHeight();
			$(this).find( ".secChildh" ).css("height", secChildh);	
	    });	
	}

});  //End Window Resize 

//***********************************************************************************************************************//


$(window).load(function() {
	
		
	//--------------------------------------------
	// Home page Slider
	//--------------------------------------------
	$('.flexslider').flexslider({
		animation: "fade",
		controlNav: true,
		directionNav: false
	});
	

	
 });   //End Window Load

//***********************************************************************************************************************//


//--------------------------------------------
// Sicky Sidebar
//--------------------------------------------


//--------------------------------------------
// Sicky Sidebar
//--------------------------------------------
(function ($) {

	var stickybar = function(elem, options) {
		this.elem = elem;
		this.$elem = $(elem);
		this.options = options;
		this.metadata = this.$elem.data("stickybar-options");
		this.$win = $(window);
	};

	stickybar.prototype = {
		defaults: {
			item: '.stickybar',
			container: '.stickybar-container',
			stickClass: 'stickto',
			endStickClass: 'stickyend',
			offset: 0,
			start: 0,
			onStick: null,
			onUnstick: null
		},

		init: function() {
			var _self = this;

			//Merge options
			_self.config = $.extend({}, _self.defaults, _self.options, _self.metadata);

			_self.setWindowHeight();
			_self.getItems();
			_self.bindEvents();

			return _self;
		},

		bindEvents: function() {
			var _self = this;

			_self.$win.on('scroll.stickybar', $.proxy(_self.handleScroll, _self));
			_self.$win.on('resize.stickybar', $.proxy(_self.handleResize, _self));
		},

		destroy: function() {
			var _self = this;

			_self.$win.off('scroll.stickybar');
			_self.$win.off('resize.stickybar');
		},

		getItem: function(index, element) {
			var _self = this;
			var $this = $(element);
			var item = {
				$elem: $this,
				elemHeight: $this.height(),
				$container: $this.parents(_self.config.container),
				isStuck: false
			};

			//If the element is smaller than the window
			if(_self.windowHeight > item.elemHeight) {
				item.containerHeight = item.$container.outerHeight();
				item.containerInner = {
					border: {
						bottom: parseInt(item.$container.css('border-bottom'), 10) || 0,
						top: parseInt(item.$container.css('border-top'), 10) || 0
					},
					padding: {
						bottom: parseInt(item.$container.css('padding-bottom'), 10) || 0,
						top: parseInt(item.$container.css('padding-top'), 10) || 0
					}
				};

				item.containerInnerHeight = item.$container.height();
				item.containerStart = item.$container.offset().top - _self.config.offset + _self.config.start + item.containerInner.padding.top + item.containerInner.border.top;
				item.scrollFinish = item.containerStart - _self.config.start + (item.containerInnerHeight - item.elemHeight);

				//If the element is smaller than the container
				if(item.containerInnerHeight > item.elemHeight) {
					_self.items.push(item);
				}
			} else {
				item.$elem.removeClass(_self.config.stickClass + ' ' + _self.config.endStickClass);
			}
		},

		getItems: function() {
			var _self = this;

			_self.items = [];

			_self.$elem.find(_self.config.item).each($.proxy(_self.getItem, _self));
		},

		handleResize: function() {
			var _self = this;

			_self.getItems();
			_self.setWindowHeight();
		},

		handleScroll: function() {
			var _self = this;

			if(_self.items.length > 0) {
				var pos = _self.$win.scrollTop();

				for(var i = 0, len = _self.items.length; i < len; i++) {
					var item = _self.items[i];

					//If it's stuck, and we need to unstick it, or if the page loads below it
					if((item.isStuck && (pos < item.containerStart || pos > item.scrollFinish)) || pos > item.scrollFinish) {
						item.$elem.removeClass(_self.config.stickClass);
						
						//only at the bottom
						if(pos > item.scrollFinish) {
							item.$elem.addClass(_self.config.endStickClass);
						}

						item.isStuck = false;

						//if supplied fire the onUnstick callback
						if(_self.config.onUnstick) {
							_self.config.onUnstick(item);
						}

						console.log("unstick");
					//If we need to stick it
					} else if(item.isStuck === false && pos > item.containerStart && pos < item.scrollFinish) {
						
							item.$elem.removeClass(_self.config.endStickClass).addClass(_self.config.stickClass);
							item.isStuck = true;

							//if supplied fire the onStick callback
							if(_self.config.onStick) {
								_self.config.onStick(item);
							}
							
							console.log("stick");
					}
					else{
						console.log("nothing "+ item + " pos="+pos  );
					}
				}
			}
		},

		setWindowHeight: function() {
			var _self = this;

			_self.windowHeight = _self.$win.height() - _self.config.offset;
		}
	};

	stickybar.defaults = stickybar.prototype.defaults;

	$.fn.stickybar = function(options) {
		//Create a destroy method so that you can kill it and call it again.
		this.destroy = function() {
			this.each(function() {
				new stickybar(this, options).destroy();
			});
		};

		return this.each(function() {
			new stickybar(this, options).init();
		});
	};

}(jQuery));

$(document).ready(function() {
	if($(window).width() >767){

	
		
			$("#filterByAlphabet-nav").addClass("stickybar");
			$('.stickybar-container').stickybar();
			var navH = $("nav").outerHeight();
			var headerH = $("#header-box").outerHeight();
			var headerHl = $("#header-box").outerHeight() + 20;
			$(window).on("scroll", function(event) {
				var st = $(this).scrollTop();
				if (st > navH && !window.navi.fixed) {
					$(".stickto").css({"top":"20px"});
					$(".listNav.stickto").css({"top":"0px"});
					
				} else if (st < 10 && window.navi.fixed) {
					$(".stickto").css({"top":"20px"});
					$(".listNav.stickto").css({"top":"0px"});
				}
				var test = true;
				if (!window.navi.visible && (st - window.navi.lastSTup < -100) && test) {
					$(".stickto").css('top', headerHl);
					
					$(".listNav.stickto").css('top', headerH);	
				}
				if (st - window.navi.lastST > 0){
					$(".stickto").css({"top":"20px"});
					$(".listNav.stickto").css({"top":"0px"});
				}
			});
		
		
	}
});


//--------------------------------------------
// Navigarion Position on Scroll
//--------------------------------------------

var $j = jQuery;
						
window.navi = {
	fixed: false,
	visible: true,
	lastST: 0,
	lastSTup: 0
};
window.placeholder_mobile = false;

$j(document).ready(function(){
	window.navi.elm = $j("#header-box");
	
	$j(window).on("scroll", function(event) {
		var navH = $("nav").outerHeight();
		var st = $j(this).scrollTop();
		if (st > navH && !window.navi.fixed) {
			window.navi.fixed = true;
			window.navi.elm.addClass("fixed");
			
		} else if (st < 10 && window.navi.fixed) {
			window.navi.fixed = false;
			window.navi.visible = true;
			window.navi.elm.removeClass("fixed invisible visible").css("position", "absolute");
			window.setTimeout('window.navi.elm.css("position", ""); ', 1); // css position wegen safari in ios7
		}
		var test = true;
		if (st > navH && window.navi.visible && (st - window.navi.lastST > 0)) {
			window.navi.visible = false;
			test = false;
			window.setTimeout('window.navi.elm.addClass("invisible").removeClass("visible"); ', 1);
		}
		if (!window.navi.visible && (st - window.navi.lastSTup < -100) && test) {
			window.navi.visible = true;
			window.navi.elm.addClass("visible").removeClass("invisible");
		}
		if (st - window.navi.lastST > 0)
			window.navi.lastSTup = st;
			window.navi.lastST = st;
	});
	
	
	   

});


//--------------------------------------------
// Sigin/ Sign up Modal window
//--------------------------------------------

jQuery(document).ready(function($){
	var $form_modal = $('.sy-user-modal'),
		$form_login = $form_modal.find('#sy-login'),
		$form_signup = $form_modal.find('#sy-signup'),
		$form_forgot_password = $form_modal.find('#sy-reset-password'),
		$form_modal_tab = $('.sy-switcher'),
		$tab_login = $form_modal_tab.children('li').eq(0).children('a'),
		$tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
		$forgot_password_link = $form_login.find('a.forgot-password'),
		$back_to_login_link = $form_forgot_password.find('a.back-to-login'),
		$main_nav = $('.sign-up-in');

	//open modal
	$main_nav.on('click', function(event){

		if( $(event.target).is($main_nav) ) {
			// on mobile open the submenu
			$(this).children().toggleClass('is-visible');
		} else {
			// on mobile close submenu
			$main_nav.children().removeClass('is-visible');
			//show modal layer
			$form_modal.addClass('is-visible');	
			//show the selected form
			( $(event.target).is('.sy-signup') ) ? signup_selected() : login_selected();
		}

	});

	//close modal
	$('.sy-user-modal').on('click', function(event){
		if( $(event.target).is($form_modal) || $(event.target).is('.sy-close-form') ) {
			$form_modal.removeClass('is-visible');
		}	
	});
	//close modal when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$form_modal.removeClass('is-visible');
	    }
    });

	//switch from a tab to another
	$form_modal_tab.on('click', function(event) {
		event.preventDefault();
		( $(event.target).is( $tab_login ) ) ? login_selected() : signup_selected();
	});

	//show forgot-password form 
	$forgot_password_link.on('click', function(event){
		event.preventDefault();
		forgot_password_selected();
	});

	//back to login from the forgot-password form
	$back_to_login_link.on('click', function(event){
		event.preventDefault();
		login_selected();
	});

	function login_selected(){
		$form_login.addClass('is-selected');
		$form_signup.removeClass('is-selected');
		$form_forgot_password.removeClass('is-selected');
		$tab_login.addClass('selected');
		$tab_signup.removeClass('selected');
	}

	function signup_selected(){
		$form_login.removeClass('is-selected');
		$form_signup.addClass('is-selected');
		$form_forgot_password.removeClass('is-selected');
		$tab_login.removeClass('selected');
		$tab_signup.addClass('selected');
	}

	function forgot_password_selected(){
		$form_login.removeClass('is-selected');
		$form_signup.removeClass('is-selected');
		$form_forgot_password.addClass('is-selected');
	}

	//IE9 placeholder fallback
	//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
	if(!Modernizr.input.placeholder){
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
		  	}
		}).blur(function() {
		 	var input = $(this);
		  	if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.val(input.attr('placeholder'));
		  	}
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
		  	$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
			 		input.val('');
				}
		  	})
		});
	}

});

jQuery.fn.putCursorAtEnd = function() {
	return this.each(function() {
    	// If this function exists...
    	if (this.setSelectionRange) {
      		// ... then use it (Doesn't work in IE)
      		// Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
      		var len = $(this).val().length * 2;
      		this.setSelectionRange(len, len);
    	} else {
    		// ... otherwise replace the contents with itself
    		// (Doesn't work in Google Chrome)
      		$(this).val($(this).val());
    	}
	});
};

//--------------------------------------------
//  Banner image parallax effect
//--------------------------------------------

// detect touch
if("ontouchstart" in window){
    document.documentElement.className = document.documentElement.className + " touch";
}
if(!$("html").hasClass("touch")){
    //background fix 
    $(".parallax").css("background-attachment", "scroll");
}

/* fix vertical when not overflow
call fullscreenFix() if .fullscreen content changes */
function fullscreenFix(){
    var h = $('body').height();
    // set .fullscreen height
    $(".content-b").each(function(i){
        if($(this).innerHeight() <= h){
            $(this).closest(".fullscreen").addClass("not-overflow");
        }
    });
}
$(window).resize(fullscreenFix);
fullscreenFix();

// resize background images 
function backgroundResize(){
    var windowH = $(window).height();
    $(".background").each(function(i){
        var path = $(this);
        // variables
        var contW = path.width();
        var contH = path.height();
        var imgW = path.attr("data-img-width");
        var imgH = path.attr("data-img-height");
        var ratio = imgW / imgH;
        // overflowing difference
        var diff = parseFloat(path.attr("data-diff"));
        diff = diff ? diff : 0;
        // remaining height to have fullscreen image only on parallax
        var remainingH = 0;
        if(path.hasClass("parallax") && !$("html").hasClass("touch")){
            var maxH = contH > windowH ? contH : windowH;
            remainingH = windowH - contH;
        }
        // set img values depending on cont
        imgH = contH + remainingH + diff;
        imgW = imgH * ratio;
        // fix when too large
        if(contW > imgW){
            imgW = contW;
            imgH = imgW / ratio;
        }
        //
        path.data("resized-imgW", imgW);
        path.data("resized-imgH", imgH);
        path.css("background-size", imgW + "px " + imgH + "px");
    });
}
$(window).resize(backgroundResize);
$(window).focus(backgroundResize);
backgroundResize();

// set parallax background-position 
function parallaxPosition(e){
    var heightWindow = $(window).height();
    var topWindow = $(window).scrollTop();
    var bottomWindow = topWindow + heightWindow;
    var currentWindow = (topWindow + bottomWindow) / 2;
    $(".parallax").each(function(i){
        var path = $(this);
        var height = path.height();
        var top = path.offset().top;
        var bottom = top + height;
        // only when in range
        if(bottomWindow > top && topWindow < bottom){
            var imgW = path.data("resized-imgW");
            var imgH = path.data("resized-imgH");
            // min when image touch top of window
            var min = 0;
            // max when image touch bottom of window
            var max = - imgH + heightWindow;
            // overflow changes parallax
            var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow; // fix height on overflow
            top = top - overflowH;
            bottom = bottom + overflowH;
            // value with linear interpolation
            var value = min + (max - min) * (currentWindow - top) / (bottom - top);
            // set background-position
            var orizontalPosition = path.attr("data-oriz-pos");
            orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
            $(this).css("background-position", orizontalPosition + " " + value + "px");
        }
    });
}
if(!$("html").hasClass("touch")){
    $(window).resize(parallaxPosition);
    //$(window).focus(parallaxPosition);
    $(window).scroll(parallaxPosition);
    parallaxPosition();
}


