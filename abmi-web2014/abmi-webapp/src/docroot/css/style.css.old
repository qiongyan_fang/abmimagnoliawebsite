body {
  background-color: #262626;
  max-width: 100%; }
.container-fluid {
  color: white;
  margin: 0;
  padding: 0; }

.logo {
  background-color: #677523;
  height: 100px;
  float: left;
  width: 100px; }

  .logo a {
	  background: url(/docroot/assets/abmi_logo.png) no-repeat center center #677523;
	  display: block;
	  height: 100%;
	  width: 100%; 
	  transition: .3s; }
  .logo a:hover {
	  background-color: #626B22;
  }
  .logo a:active {
	  background-color: #4B4C19;
  }
  
.top-bar {
  background-color: #3a3a11;
  height: 35px; }

.search a {
  background-color: #677523;
  background: url(/docroot/assets/search.png) no-repeat center right 15px #677523;
  top: 35px;
  height: 65px;
  float: right;
  margin-left: 15px;
  position: relative;
  -webkit-transition: background-color 0.5s ease-out, width 0.5s ease-out;
  -moz-transition: background-color 0.5s ease-out, width 0.5s ease-out;
  transition: background-color 0.5s ease-out, width 0.5s ease-out;
  width: 65px;
  overflow: hidden; }

.search a input {
  background-color: transparent;
  background-color: rgba(0, 0, 0, 0);
  border: none;
  color: white;
  height: 65px;
  font-size: 2em;
  padding-left: 15px;
  position: absolute;
  right: 65px;
  width: 80px; }

.jumbotron {
  min-height: 270px;
  max-width: 100%;
  overflow: hidden;
  margin: 0;
  padding: 0;
  position: relative;
  top: 0;
  left: 0;
  -webkit-transform-style: preserve-3d; }

.jumbotron img {
  width: 100%;
  height: auto;
  min-height: 270px; }

.overlay {
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 999;
  text-align: center;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  -o-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  width: 80%; }

.company-info {
  background-color: #f3f0df; }

.small-container {
  background-color: #900;
  margin: 0 auto;
  max-width: 968px; }

.divider {
  background-color: #74882e; }

.explore {
  background: url(/docroot/assets/parkridge_banff.jpg) no-repeat center top;
  background-size: cover;
  filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/docroot/assets/parkridge_banff.jpg', sizingMethod='scale');
  -ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/docroot/assets/parkridge_banff.jpg', sizingMethod='scale')"; }

.triangle {
  width: 0;
  height: 0;
  border-left: 53.5px solid transparent;
  border-right: 53.5px solid transparent;
  border-bottom: 55px solid transparent;
  border-top: 55px solid #74882e;
  margin: 0 auto; }

.tile {
  background-color: #bb6b0c;
  clear: left;
  margin-bottom: 30px;
  max-width: 570px;
  overflow: auto; }

.tile .column {
  width: 64%;
  padding: 2em 3em; }

.learn-more {
  background-color: #74882e; }

.learn-more .column {
  min-height: 283px;
  position: relative; }

.social {
  background-color: #8ab0a3;
  text-align: center; }

.events {
  background-color: #9b8732; }

.half {
  clear: none;
  float: left;
  margin-bottom: 50px;
  margin-right: 5.2%;
  min-height: 283px;
  max-width: 570px;
  width: 47.4%; 
  overflow: hidden; }
  .half a {
  	padding: 2em 3em;
  	min-height: 283px;
  }
ol.carousel-indicators {
  text-align: left;
  bottom: 0;
  left: 0;
  margin-left: 0;
  padding: 0 3em 2em;
  width: 50%; }
.carousel {
	overflow: hidden !important;
}
#carousel-news ol.carousel-indicators {
  left: 36%; }

 .carousel-left-image ol.carousel-indicators {
  left: 36%; }
  
.carousel-indicators li {
  background-color: #313436;
  border: 1px #313436 solid;
  border-radius: 0; }

.carousel-indicators li.active {
  border: none; }
.carousel-inner>.item {
	width: 100%;}
.social img.icon {
  display: block;
  float: none;
  margin: 2em auto 0;
  width: 47px; }

.right {
  float: right;
  margin-left: 2%;
  margin-right: 0; }

.sponsor {
  background-color: #e8e4bf;
  height: 102px;
  float: left;
  margin: 12px 5% 30px 0;
  width: 30%; }

footer {
  background-color: #313436; }

.fine-print {
  padding: 20px 15px; }

/* typography */
h1, h2, h3, h4, a.button {
  font-family: "Roboto Slab", serif;
  font-weight: bold;
  line-height: 1em; }

.jumbotron h1.sub-heading {
  font-size: 3.2em;
  font-weight: 400; }

h2 {
  color: #262626;
  font-size: 3.5em;
  line-height: 36pt;
  margin: 0;
  padding: 59px 0 39px; }

.divider h2 {
  color: white;
  font-size: 3.7em;
  margin: 0;
  padding: 0.7em 0 0.9em; }

h2.partners {
  font-size: 5em;
  padding: 52px 0 25px; }

h3 {
  color: #262626;
  font-size: 2.4em;
  font-weight: 400;
  line-height: 1.4em;
  margin: 42px 0 48px; }

.tile h4 {
  color: #ffffff;
  font-size: 2.4em;
  line-height: 1.2em; }

.social h4 {
  font-size: 1.8em;
  margin-top: 1.5em;
  padding-top: 0; }

.spotlight {
  margin: 0 0 30px;
  position: relative;
  max-width: 570px; }

.spotlight .overlay {
  top: 0;
  left: 0;
  height: 100%;
  text-align: left;
  padding: 2.6em 2.9em;
  width: 100%;
  -webkit-transform: translate(0, 0);
  -moz-transform: translate(0, 0);
  -ms-transform: translate(0, 0);
  -o-transform: translate(0, 0);
  transform: translate(0, 0); }

.spotlight .overlay h3 {
  color: white;
  font-size: 3.7em;
  line-height: 1em;
  text-shadow: 0 0 0.1em #a6a8ab;
  margin-top: 20px; }

.spotlight .overlay h4 {
  color: white;
  font-size: 2.4em;
  font-weight: 400;
  line-height: 1.2em;
  text-shadow: 0 0 0.1em #a6a8ab;
  max-width: 360px;
  
  margin-top: 10px; }

.spotlight .overlay h4.text-right {
  float: right; }

.spotlight .overlay h4.text-left {
  float: left; }

.spotlight .spotlight-base {
  position: absolute;
  bottom: 0px;
  right: 2.9em;
  left: 2.9em; }

footer h3 {
  color: #fff;
  clear: left;
  line-height: 1.2em;
  margin: 48px 0 15px; }

a, a:link {
  color: #a6a8ab;
  font-family: "Open Sans", sans-serif;
  text-decoration: none; }

a:hover {
  color: #74882e; }

.top-bar ul, footer ul {
  list-style: none;
  padding: 0; }

.top-bar ul {
  float: right;
  margin-bottom: 0; }

.top-bar ul li {
  float: left;
  margin: 6px 0 0 22px; }

.top-bar ul li a {
  color: white;
  font-family: "Roboto Slab", serif;
  font-size: 1.5em;
  font-weight: 300; }

.search a:hover, .search a:focus {
  background-color: #5d691f;
  width: 140px; }

.search a:active {
  background-color: #333a11; }

footer ul {
  margin-bottom: 55px; }

footer ul li {
  padding-bottom: 12px; }

footer ul li a {
  color: #a6a8ab;
  font-size: 11pt; }

p {
  color: #262626;
  font-family: "Open Sans", sans-serif;
  font-size: 1.5em;
  line-height: 1.6em; }

.tile p {
  color: white;
  font-size: 1.4em;
  line-height: 1.6em; }

.tile p.timestamp {
  font-size: 1.2em;
  margin-bottom: 5px; }

footer p {
  color: #a6a8ab; }
.events a {
	display: block;
	height: 100%;
	width: 100%;
}
.events .fauxa {
  color: white;
  display: block;
  font-family: "Roboto Slab", serif;
  font-size: 1.4em;
  text-transform: uppercase;
  padding: 15px 0 0; }
  
  .events .fauxa:hover {
	text-decoration: underline; }
	  
.overlay p {
  color: white;
  text-shadow: 0 0 0.1em #313436;
  max-width: 320px;
  padding: 0 0 2em; }

.overlay p.text-right {
  float: right; }

.overlay p.text-left {
  float: left; }

.fine-print p {
  color: #a6a8ab;
  font-size: 1.4em;
  line-height: 1.8em;
  padding: 0 0 0px 45px; }

a.button {
  background-color: #74882e;
  color: white;
  font-family: "Roboto Slab", serif;
  font-size: 1.6em;
  font-weight: normal;
  float: left;
  letter-spacing: 0;
  line-height: 130%;
  text-align: left;
  text-transform: none;
  -webkit-transition: background-color 0.5s ease-out;
  -moz-transition: background-color 0.5s ease-out;
  transition: background-color 0.5s ease-out;
  margin-bottom: 2%;
  padding: 17px 30px 17px 25px;
  min-height: 75px;
  width: 30%; }

.icon {
  float: left;
  margin-right: 20px; }

a.button:hover {
  background-color: #6a7d2a; }

a.button:active {
  background-color: #434f1b; }
.social div {
	display: block;
	height: 100%;
	width: 100%;
}
.social .fauxa {
  color: white;
  font-weight: bold; }
  .social .fauxa:hover {
	  text-decoration: underline;	}
.center {
  clear: left;
  text-align: center;
  margin: 0 auto;
  padding: 57px 0 76px; }

a.btn-learn, button.btn-learn {
  background-color: #8ab0a3;
  color: white;
  font-family: "Roboto Slab", serif;
  font-size: 1.6em;
  float: none;
  letter-spacing: 2px;
  text-align: center;
  text-transform: uppercase;
  margin: 0;
  padding: 15px 40px;
  -webkit-transition: background-color 0.5s ease-out;
  -moz-transition: background-color 0.5s ease-out;
  transition: background-color 0.5s ease-out; }

a.btn-learn:hover, button.btn-learn:hover {
  background-color: #81aa9c; }

a.btn-learn:active, buttona.btn-learn:active {
  background-color: #608e7e; }

.learn-more a.btn-learn, .learn-more button.btn-learn {
  background-color: #74882e;
  border: 1px solid white;
  float: right;
  margin-top: 25px;
  padding: 13px 32px;
  position: absolute;
  bottom: 30px;
  right: 30px; }

.learn-more a.btn-learn:hover, .learn-more button.btn-learn:hover {
  background-color: #6a7d2a; }

.learn-more a.btn-learn:active, .learn-more button.btn-learn:active {
  background-color: #434f1b; }

.last {
  margin-right: 0; }

.company-info a.last {
  margin-right: 0; }

.partner {
  height: 65px;
  margin: 60px 0 25px; }

.ai {
  background: url(/docroot/assets/partner_ai-logo.png) no-repeat center center; }

.ram {
  background: url(/docroot/assets/partner_ram-logo.png) no-repeat center center; }

.uoc {
  background: url(/docroot/assets/partner_uoc-logo.png) no-repeat center center; }

.uoa {
  background: url(/docroot/assets/partner_uoa-logo.png) no-repeat center center; }

.input-group {
  float: left;
  margin: 10px 0 30px 0; }

.btn {
  border-radius: 0; }

.btn-primary {
  background-color: #677523;
  border: #677523;
  border-radius: 18px;
  float: left;
  font-family: "Roboto Slab", serif;
  font-size: 1.3em;
  padding: 10px 26px;
  -webkit-transition: background-color 0.5s ease-out;
  -moz-transition: background-color 0.5s ease-out;
  transition: background-color 0.5s ease-out; }

.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
  background-color: #5d691f; }

.connect a {
  background-color: #a6a8ab;
  border: #a6a8ab;
  border-radius: 18px;
  color: white;
  display: block;
  float: left;
  font-size: 1.3em;
  height: 37px;
  margin: 0 0 50px 11px;
  width: 37px; }

.connect a.twitter {
  background: url(/docroot/assets/twitter.png) no-repeat center center #a6a8ab; }

.connect a.facebook {
  background: url(/docroot/assets/facebook.png) no-repeat center center #a6a8ab; }

.connect a.vimeo {
  background: url(/docroot/assets/vimeo.png) no-repeat center center #a6a8ab; }

.connect a:hover {
  background-color: #9ea0a4; }

.connect a:active {
  background-color: #7f8286; }

.form-control {
  background-color: #a6a8ab;
  border: none;
  border-radius: 0;
  color: #313436;
  font-size: 1.3em;
  padding: 23px 15px; }

.btn-default {
  background-color: #677523;
  border: 1px solid #677523;
  color: white;
  font-family: "Roboto Slab", serif;
  font-size: 17px;
  letter-spacing: 1px;
  text-transform: uppercase;
  -webkit-transition: background-color 0.5s ease-out;
  -moz-transition: background-color 0.5s ease-out;
  transition: background-color 0.5s ease-out;
  padding: 10px 30px; }

.form-control:focus {
  border: none;
  box-shadow: none; }

.connect {
  float: left; }

.navbar-default {
  background-color: #74882e;
  border: 0;
  border-radius: 0;
  margin-bottom: 0; }

.navbar-collapse {
  border: none;
  box-shadow: none;
  margin-right: 15px;
  padding: 0; }

.navbar-toggle {
  margin-top: 15px; }

.navbar-default .navbar-toggle:hover {
  background-color: #6a7d2a; }

.navbar-default .navbar-toggle:active {
  background-color: #434f1b; }

.navbar-default .navbar-toggle .icon-bar {
  background-color: white; }

.navbar-default .navbar-nav > li > a {
  color: white;
  font-family: "Roboto Slab", serif;
  font-size: 2em;
  font-weight: 300;
  -webkit-transition: background-color 0.5s ease-out;
  -moz-transition: background-color 0.5s ease-out;
  transition: background-color 0.5s ease-out; }

.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:visited {
  background-color: #3a3a11;
  color: white; }

.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
  background-color: #3a3a11;
  color: white; }
  
/* Social flag ----- */
.socialflag {
	position: fixed;
	top: 48%;
	left: 0;
	background: url('/docroot/assets/social.png') no-repeat top left;
	width: 90px;
	height: 45px; }
.socialflag a {
	display: inline-block;
	transition: .25s;
	margin: 9px;
	opacity: .7; }
.socialflag a:hover {
	opacity: 1; }
	
@media only screen and (min-width: 320px) {
  body {
    font-size: 90%; }

  .jumbotron h1 {
    font-size: 4em; }

  .jumbotron h1.sub-heading {
    font-size: 2.2em; }

  .learn-more .column {
    min-height: 0; }

  .btn-primary {
    margin-bottom: 30px;
    width: 100%; }

  a.button {
    width: 100%; }

  .btn-grouping {
    margin-bottom: 62px;
    overflow: auto; }

  .half {
    margin-bottom: 30px;
    min-height: 0;
    width: 100%; }

  .carousel-inner p {
    margin-bottom: 40px; }

  .navbar-nav {
    margin: 0; }

  .navbar-default .navbar-nav > li > a {
    padding: 22px 3em 23px; } }
@media only screen and (min-width: 480px) {
  .btn-primary {
    width: auto; }

  .input-group {
    margin-right: 30px; }

  a.button {
    font-size: 1.9em; }

  .tile {
    /* max-height: 229px; */
    max-height: 282px; }

  .search a:hover, .search a:focus {
    width: 280px; }

  .search a input {
    width: 215px; } }
@media only screen and (min-width: 768px) {
  .jumbotron h1 {
    font-size: 8em; }

  .jumbotron h1.sub-heading {
    font-size: 4em; }

  footer ul {
    margin-bottom: 0px; }

  a.button {
    margin-bottom: 1%; }

  h2 {
    font-size: 4em; }

  .half {
    min-height: 282px;
    width: 47.4%; } }
@media only screen and (min-width: 992px) {
  .tile {
    max-height: 283px; }

  .input-group {
    margin-right: 0; }

  a.button {
    font-size: 1.6em;
    margin: 0 1.6%;
    width: 30.2%; }

  h2 {
    font-size: 3.5em; }

  footer ul {
    margin-bottom: 66px; } }
@media (max-width: 1199px) {
  header .container {
    width: 100%; }

  .navbar-header {
    float: none; }

  .navbar-toggle {
    display: block; }

  .navbar-collapse.collapse {
    display: none !important;
    margin-left: -15px;
    margin-right: -15px; }

  .navbar-nav {
    float: none !important; }

  .navbar-nav > li {
    float: none; }

  .navbar-nav > li > a {
    padding-top: 10px;
    padding-bottom: 10px; }

  /* since 3.1.0 */
  .navbar-collapse.collapse.in {
    display: block !important; }

  .collapsing {
    overflow: hidden !important; }

  .navbar-nav .open .dropdown-menu, .navbar-right .dropdown-menu {
    background-color: #f3f0df;
    left: auto;
    right: auto;
    padding: 0;
    width: 100%; }

  .navbar-right .dropdown-menu .row {
    width: auto; }

  .navbar-right .dropdown-menu .row ul li a {
    color: #3a3a11;
    display: block;
    font-family: "Roboto Slab", serif;
    font-size: 1.2em;
    font-weight: 700;
    padding: 0.8em 1em 0.8em 4.5em;
    -webkit-transition: background-color 0.5s ease-out;
    -moz-transition: background-color 0.5s ease-out;
    transition: background-color 0.5s ease-out; }

  .navbar-right .dropdown-menu .row ul li a:hover, .dropdown-menu .row ul li a:active {
    background-color: #677523;
    color: white; } }
@media only screen and (min-width: 1200px) {
  body {
    font-size: 100%; }

  .dropdown-menu {
    background-color: #3a3a11;
    border: 0;
    border-radius: 0;
    box-shadow: none;
    padding-top: 0;
    padding-bottom: 0; }

  .dropdown-menu li ul {
    padding: 30px 15px 40px; }

  .dropdown-menu li a {
    color: #e8e4bf;
    display: block; 
    line-height: 2em; }

  .dropdown-menu li a:hover {
    background-color: #3a3a11;
    color: white; }

  ul.dropdown-menu li .row ul li {
    padding: 5px 0; }

  .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
    background-color: #3a3a11;
    color: white; }

  .dropdown-menu .row {
    width: 960px; }
    
  .dropdown-menu div.visible-lg {
	  float: left;
  }
  .dropdown-menu .list-unstyled {
	  display: inline-block;
	  margin: 0 10px;
	  vertical-align: top;
  }
  .biodiversity ul.dropdown-menu {
	  width: 788px;
  }
  .about ul.dropdown-menu {
	  width: 860px;
  }
  .whatwedo ul.dropdown-menu {
	  width: 750px;
  }
  .ourdata ul.dropdown-menu {
	  width: 590px;
  }
  .naturelynx ul.dropdown-menu {
	  width: 470px;
  }
  .contact ul.dropdown-menu {
	  width: 380px;
  }
  .navbar-right .dropdown-menu {
    left: -300px;
    right: auto; }

  .navbar-nav {
    margin: 0 -15px 0; }

  .navbar-default .navbar-nav > li > a {
    padding: 22px 0.9em 23px; }

  .container {
    max-width: 1170px;
    padding: 0; }

  .learn-more .column {
    min-height: 283px; }

  .fine-print {
    padding: 20px 15px 20px 0; } }