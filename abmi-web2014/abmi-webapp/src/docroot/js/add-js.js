jQuery(document)
		.ready(
				function($) {
					/** fill publication search tables */
					if ($("#publication_search").length) {
						var dataMap;
						$
								.get(
										"/.ajax/publication/getPublicationTags",
										{},
										function(data) {
											/*
											 * result.put("documentMap",
											 * typeMap);
											 * result.put("subjectMap",
											 * subjectMap);
											 * result.put("mainTypeMap",
											 * mainTypeMap);
											 */
											dataMap = data;
											$
													.each(
															data.mainTypeMap,
															function(key, value) {
																var htmlStr = '<li  class="sublistmenu"><a href="javascript:void(0)">'
																		+ value
																		+ '</a><ul id="type'
																		+ key
																		+ '" class="sublist"  style="display: none;"><li><a href="javascript:void(0)" class="selectall sublistitem type'
																		+ key
																		+ '">All</a></li></ul></li>';
																$(
																		"#publication_type")
																		.append(
																				htmlStr);

															});
											$
													.each(
															data.documentMap,
															function(key, value) {
																var htmlStr = '<li><a href="javascript:void(0)" class="type'
																		+ key
																		+ ' sublistitem">'
																		+ value
																				.substring(
																						value
																								.indexOf(" - ") + 3)
																				.trim()
																		+ '</a></li>';
																$(
																		"#type"
																				+ key
																						.substring(
																								0,
																								1))
																		.append(
																				htmlStr);

															});

											$
													.each(
															data.subjectMap,
															function(key, value) {
																var htmlStr = '<li><a class="subject'
																		+ key
																		+ '" href="javascript:void(0)">'
																		+ value
																		+ '</a></li>';
																$(
																		"#publication_subject")
																		.append(
																				htmlStr);

															});

											var d = new Date();
											var n = month[d.getMonth()];
											var htmlStr = '<li class="sublistmenu "><a href="javascript:void(0)">'
													+ d.getFullYear()
													+ '</a>'
													+ '<ul id="current_year" class="sublist"  style="display: none;"> '
													+ '<li><a href="javascript:void(0)" class="selectall year'
													+ d.getFullYear()
													+ '">All</a></li>' +

													'</ul>' + '</li>';
											$("#publication_time").append(
													htmlStr);
											for ( var i = d.getMonth(); i >= 0; i--) {
												var htmlStr = '<li><a href="javascript:void(0)" class="year'
														+ month[i]
														+ d.getFullYear()
														+ '">'
														+ month[i]
														+ '</a></li>';
												$("#current_year").append(
														htmlStr);
											}

											for ( var i = d.getFullYear() - 1; i >= 2003; i--) {
												var htmlStr = '<li><a href="javascript:void(0)" class= "year'
														+ d.getFullYear()
														+ '">'
														+ i
														+ '</a></li>';
												$("#publication_time").append(
														htmlStr);

											}
											publicationSearchInit();
											$.each(subject,
													function(index, val) {
														$(".subject" + val)
																.click();
													});
											$.each(documentType, function(
													index, val) {
												$(".type" + val).click();
											});
											$
													.each(
															time,
															function(index, val) {
																$(
																		".year"
																				+ val
																						.replace(
																								" ",
																								"")
																						.replace(
																								",",
																								""))
																		.click();
															});

											if (!featuredOnly
													|| $("#tagSearch").length > 0
													|| subject.length > 0
													|| documentType.length > 0) {
												getPublicationList(dataMap);
											} else {
												getFeaturedPublication();
											}

										}, "json");

						// bind get result button
						$("#publication_search_btn").bind("click", function() {
							getPublicationList(dataMap);
						});
						// reset selection
						$("#publication_clear_btn").bind("click", function() {
							$('.data-selected .item').click();
						});
						$("#cleartaglink").bind("click", function() {
							$("#tagDiv").remove();
						});
						/**/
						$("#sortPublicaiton").bind("change", function() {
							getPublicationList(dataMap);
						});

						/* List View/ Grid View */
						$(".sidemenuFullPostBtn").click(
								function() {
									$(".sidemenuFullPostBtn")
											.addClass("active");
									$(".sidemenuListPostBtn").removeClass(
											"active");
									$(".publication-post .post-text").addClass(
											"col-sm-8");
									$(".publication-post .post-text").addClass(
											"col-sm-9");
									$(".publication-post .shortpost-list")
											.addClass("shortpost");
									$(".publication-post .shortpost")
											.removeClass("shortpost-list");

								});
						$(".sidemenuListPostBtn").click(
								function() {
									$(".sidemenuListPostBtn")
											.addClass("active");
									$(".sidemenuFullPostBtn").removeClass(
											"active");
									$(".publication-post .post-text")
											.removeClass("col-sm-8");
									$(".publication-post .post-text")
											.removeClass("col-sm-9");
									$(".publication-post .shortpost").addClass(
											"shortpost-list");
									$(".publication-post .shortpost-list")
											.removeClass("shortpost");
								});

					} // end of publicaiton part

				});

var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";

function getFeaturedPublication() {
	$("#publication-list").empty();
	$("#sortPublicaiton").parent().parent().hide();
	$("#publication-list").append("<h3>Featured Publications</h3>");

	$("#publication-list").scrollPagination({
		url : "/.ajax/publication/getPublication", // the url you are fetching
													// the results
		offset : 1,
		pageSize : pageSize,

		params : {
			root : "/home/newpublications/",
			descriptionCount : descriptionCount,
			titleCount : titleCount,
			featuredOnly : true
		} // these are the variables you can pass to the request, for example:
			// children().size() to know which page you are
		,
		itemDiv : ".publication-post .publication_row ",

		callback : displayPublicationItem,
		failCallback : showPublicaitonError,
		scroll : false
	});

}

function getPublicationList(dataMap) {
	$("#publication-list").empty();
	$("#sortPublicaiton").parent().parent().show();

	var typeParam = [];
	var subjectParam = [];
	var timeParam = [];

	var paramStr = "";
	$.each($("#type_selection .data-selected .item"), function(index, row) {
		var valueStr;

		var p = $(row).find(".parenttag").text();
		var c = $(row).find(".childtag").text();

		if (c != "All") {
			valueStr = p + " - " + c;
			for ( var key in dataMap.documentMap) {
				if (valueStr == dataMap.documentMap[key]) {
					typeParam[typeParam.length] = key;
					paramStr += "&documenttype=" + key;
					break;
				}
			}
		} else {
			valueStr = p;
			for ( var key in dataMap.mainTypeMap) {
				if (valueStr == dataMap.mainTypeMap[key]) {
					typeParam[typeParam.length] = key;
					paramStr += "&documenttype=" + key;
					break;
				}
			}
		}

	});

	$.each($("#subject_selection .data-selected .item .selectedtag"), function(
			index, row) {
		var valueStr;

		var p = $(row).text();

		if (p != "All") {
			valueStr = p;
			for ( var key in dataMap.subjectMap) {
				if (valueStr == dataMap.subjectMap[key]) {
					subjectParam[subjectParam.length] = key;
					paramStr += "&subject=" + key;
					break;
				}
			}
		}

	});

	$.each($("#time_selection .data-selected .item"), function(index, row) {
		var valueStr;

		if ($(row).hasClass("suballtag") || $(row).hasClass("withchild")) {
			var p = $(row).find(".parenttag").text();
			var c = $(row).find(".childtag").text();

			if (c != "All") {
				timeParam[timeParam.length] = c + ", " + p;
				paramStr += "&time=" + c + ", " + p;
			} else {
				timeParam[timeParam.length] = p;
				paramStr += "&time=" + p;
			}

		} else {

			var p = $(row).find(".selectedtag").text();
			timeParam[timeParam.length] = p;
			paramStr += "&time=" + p;

		}
	});

	var sorting = $("#sortPublicaiton option:selected").val();
	paramStr += "&sortBy=" + sorting;
	var keyword = $("#tagSearch").text();
	paramStr += "&keyword=" + keyword;
	paramStr += "&featuredOnly=false";
	var params = {
		root : "/home/newpublications/",
		documenttype : typeParam,
		subject : subjectParam,
		archive : timeParam,
		descriptionCount : descriptionCount,
		titleCount : titleCount,
		sortByRelevant : false,
		sortBy : sorting,
		tag : keyword,
		featuredOnly : false
	};

	var batch = Math.floor(offset / pageSize);
	if (offset > pageSize) { // prefetch records
		var batch = Math.floor(offset / pageSize)
		preFetchPublication(params, batch * pageSize);

	}

	$("#publication-list").scrollPagination({
		url : "/.ajax/publication/getPublication", // the url you are fetching
													// the results
		offset : (offset > pageSize ? batch * pageSize + 1 : 1),
		pageSize : pageSize,

		params : params// these are the variables you can pass to the request,
						// for example: children().size() to know which page you
						// are
		,
		uriParams : paramStr,
		itemDiv : ".publication-post .publication_row ",

		callback : displayPublicationItem,
		failCallback : showPublicaitonError,
		scroll : false
	});

}

/* display single publication item */
function displayPublicationItem(alldata, uriParams) {

	var ftpprefix = "http://ftp.public.abmi.ca/";
	var isListView = $(".sidemenuListPostBtn").hasClass("active");
	var listClass1, listClass2;
	if (isListView) {
		listClass1 = "shortpost-list";
		listClass2 = "";

	} else {
		listClass1 = "shortpost";
		listClass2 = " col-sm-8 col-md-9 ";
	}

	// display all items one by one
	$
			.each(
					alldata,
					function(index, data) {
						var link, linktext, linkclass;
						if (!data.doi || data.doi == "") {
							link = data.viewlink;
							linktext = "DOWNLOAD";
							linkclass = "post-pdf";
						} else {
							link = data.doi;
							linktext = "VIEW";
							linkclass = "pdf";
						}

						var imageurl;
						if (data.coverImageUrl != "") {
							imageurl = data.coverImageUrl;
						} else if (data.imageUrl != "") {
							imageurl = "/.imaging/coverthumbnail/dam/"
									+ data.imageUrl + ".";
						} else {
							imageurl = publicationDefaultCoverImage;
						}

						var htmlStr = '	<div class="publication_row '
								+ listClass1 + '"> '
								+ '<div class="post-feature-img">' +
								/*-- 1. cover image 2.resized large image 3. default place holder image !--*/

								'<img src="' + imageurl + '" alt="">'
								+ '</div>' +

								'<div class="post-text ' + listClass2 + '">'
								+ '<div class="post-header">' + '<h4><a href="'
								+ data.path + '">' + data.doctitle
								+ '</a></h4> ' + '<div class="post-data">'
								+ '<a class="post-by" href="#">' + data.author
								+ '</a> <span class="post-date">'
								+ data.displaydate + '</span> '
								+ '<a target="_blank" href="' + link
								+ '" class="' + linkclass
								+ '" type="text/html">' + linktext + '</a> ';
						if (data.flipbookLink != "") {
							htmlStr += '<a  class="post-flipbook" target="_blank" '
									+ ' href="'
									+ ftpprefix
									+ data.flipbookLink
									+ '">'
									+ (data.flipbookLinkText == "" ? "FLIPBOOK"
											: data.flipbookLinkText) + '</a> ';

						}
						htmlStr += '	</div> '
								+ '</div>'
								+ '<div class="post-exerpt">'
								+ '<p>'
								+ data.description
								+ '</p>'
								+ '	<a data-path="'
								+ data.path
								+ '?'
								+ uriParams
								+ '" href="#" onclick="javascript:openPublicationPage(this);" class="readmore-link">CONTINUE READING</a>'
								+ '</div> ' + '</div>' + '</div>';

						$("#publication-list").append(htmlStr);
					});

	if (alldata.length == pageSize) {
		$(".loadMore").show();
	} else {
		$(".loadMore").hide();
	}

}

function showPublicaitonError() {
	if (!$(".publication-post .publication_row").length) { // show no result
															// message when no
															// result at all
		$("#publication-list").append(
				"<div><p>There are no matching publications.</p></div>");
	}

}

function openPublicationPage(link) {
	var rowNum = $(".publication-post .publication_row").length;
	window.location.href = $(link).attr("data-path") + "&offset=" + rowNum;
}

function preFetchPublication(params, nRecord) {
	params.offset = 1;
	params.pageSize = nRecord;
	$.get("/.ajax/publication/getPublication", // the url you are fetching the
												// results
	params, displayPublicationItem);

}
/**
 * --------------------------------
 * 
 * ------------------------------------
 */

function publicationSearchInit() {
	$(document).mouseup(function(e) {
		var container = $(".search-group");

		if (!container.is(e.target) // if the target of the click isn't the
									// container...
				&& container.has(e.target).length === 0) // ... nor a
															// descendant of the
															// container
		{
			$('.search-group').removeClass("open");
			$(".searchdata").slideUp();
			jQuery('.backTomain').click();
		}
	});

	// search toggle
	$(".searchdata").hide();
	jQuery(".searchBy .title").click(function() {

		if ($(this).parents(".search-group").hasClass("open")) {
			jQuery(this).parents('.search-group').removeClass("open");
			jQuery(this).parent('.searchBy').next(".searchdata").slideUp();
		} else {
			jQuery(this).parents('.search-group').addClass("open");
			jQuery(this).parent('.searchBy').next(".searchdata").slideDown();
		}
	});

	// search temp
	$(".breadcrumb-block, .sublist").hide();
	jQuery('.sublistmenu > a').click(
			function() {

				var items = '<li class="active"> ' + $(this).text() + '</li>';
				$(this).parents(".searchdata").find(".listnav-breadcrumb")
						.append(items);
				$(this).parents(".searchdata").children(".breadcrumb-block")
						.show();
				$(this).next(".sublist").show();
				$(this).parents(".group-list").children("li").hide();
				$(this).hide();
				$(this).parents(".sublistmenu").show();
			});

	// Back to main list
	jQuery('.backTomain')
			.click(
					function() {
						$(this).parents(".listnav-breadcrumb").find(".active")
								.remove();
						$(this).parents(".breadcrumb-block").hide();
						$(this).parents(".searchdata").find(".sublist").hide();
						$(this).parents(".searchdata").find(".group-list")
								.show();
						$(this).parents(".searchdata").children(".group-list")
								.find("li, li a").show();
					});

	// Selection tags
	jQuery(".group-list li a")
			.click(
					function() {
						var itemT = $(this).text();
						var parentT = $(this).parents(".sublistmenu").children(
								"a").text();
						// 1st level click
						if (!($(this).parents().hasClass("sublist"))
								&& !($(this).parents(".sublistmenu").children()
										.is('.sublist'))) {
							if (!$(this).hasClass("selected")) {
								if ($(this).hasClass("selectall")) {
									$(this).parents(".search-group").find(
											".item").fadeOut(100, function() {
										$(this).remove()
									});
									$(this).parents(".search-group").find(
											".data-selected").append(
											'<div class="item alltag"><span class="selectedtag">'
													+ itemT + '</span></div>');
									$(this).parents(".group-list").addClass(
											"allselected");
									$(this).parents(".group-list").find("li a")
											.addClass("selected");

								} else {
									$(this).parents(".search-group").find(
											".data-selected").append(
											'<div class="item"><span class="selectedtag">'
													+ itemT + '</span></div>');
									$(this).addClass("selected");
								}
							}
						}
						// 2nd level click
						else if ($(this).parents().hasClass("sublist")) {
							if (!$(this).hasClass("selected")) {

								if ($(this).hasClass("selectall")) {
									var thisparenttxt = $(this).parents(
											".sublistmenu").children("a")
											.text();
									$(this).parents(".search-group").find(
											'.item .parenttag:contains('
													+ thisparenttxt + ')')
											.fadeOut(
													100,
													function() {
														$(this).parent(".item")
																.remove()
													});
									$(this)
											.parents(".search-group")
											.find(".data-selected")
											.append(
													'<div class="item suballtag"><span class="parenttag">'
															+ parentT
															+ '</span> <span class="dash">-</span> <span class="childtag">'
															+ itemT
															+ '</span></div>');
									$(this).parents(".sublistmenu").addClass(
											"allselected");
									$(this).parents(".sublistmenu").find("a")
											.addClass("selected");

								} else {
									$(this)
											.parents(".search-group")
											.find(".data-selected")
											.append(
													'<div class="item withchild"><span class="parenttag">'
															+ parentT
															+ '</span> <span class="dash">-</span> <span class="childtag">'
															+ itemT
															+ '</span></div>');
									$(this).addClass("selected");

								}
							}

						}

						// Remove selected search tag
						$('.data-selected .item').click(
								function() {

									var tagtxt = $(this).find(".selectedtag")
											.text();
									var tagchildtxt = $(this).find(".childtag")
											.text();
									var tagparenttxt = $(this).find(
											".parenttag").text();
									if ($(this).hasClass("suballtag")) {
										$(this).parents(".search-group").find(
												'.group-list .sublistmenu a:contains('
														+ tagparenttxt + ')')
												.filter(function() {
													return tagparenttxt
												});
										$(this).parents(".search-group").find(
												'.group-list .sublistmenu a:contains('
														+ tagparenttxt + ')')
												.parents(".sublistmenu").find(
														"a").removeClass(
														"selected");
										$(this).parents(".search-group").find(
												'.group-list .sublistmenu a:contains('
														+ tagparenttxt + ')')
												.parents(".sublistmenu")
												.removeClass("allselected");

										$(this).fadeOut(300, function() {
											$(this).remove()
										});
									} else if ($(this).hasClass("alltag")) {
										$(this).parents(".search-group").find(
												".searchdata li a")
												.removeClass("selected");
										$(this).parents(".search-group").find(
												".group-list, .sublistmenu")
												.removeClass("allselected");

										$(this).fadeOut(300, function() {
											$(this).remove()
										});
									} else if ($(this).hasClass("withchild")) {
										$(this).parents(".search-group").find(
												'.group-list a:contains('
														+ tagchildtxt + ')')
												.filter(function() {
													return tagchildtxt
												}).removeClass("selected");

										$(this).fadeOut(300, function() {
											$(this).remove()
										});

									} else {

										$(this).parents(".search-group").find(
												'.group-list a:contains('
														+ tagtxt + ')').filter(
												function() {
													return tagtxt
												}).removeClass("selected");

										$(this).fadeOut(300, function() {
											$(this).remove()
										});
									}

								});
					});
}