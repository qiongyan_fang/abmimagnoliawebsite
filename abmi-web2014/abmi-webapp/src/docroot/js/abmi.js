

jQuery(document)
		.ready(
				function($) {

					// NO TRANSITIONS ON LOAD
					if (jQuery("body").hasClass("preload")) {
						jQuery("body").removeClass("preload");
					}
					;
					// ---------------------

					// ==============================================

					if ($('.postmeta.arrow').length) {

						var arrowwidth = 0;

						$('.postmeta.arrow')
								.each(
										function(index) {
											if (!$(this).prev().hasClass(
													'photobox')) {
												arrowwidth = $(this).prev()
														.width() + 40;
												$(this).css(
														'border-width',
														'10px ' + arrowwidth
																+ 'px 0 0');
											}
										});

					}
					;

					// ==============================================

					function initializeGmap(lat, lon, maptitle) {

						var myLatlng = new google.maps.LatLng(lat, lon);

						var MY_MAPTYPE_ID = 'abmi_style';

						var mapOptions = {
							zoom : 14,
							center : myLatlng,
							mapTypeControlOptions : {
								mapTypeIds : [ google.maps.MapTypeId.ROADMAP,
										MY_MAPTYPE_ID ]
							},
							mapTypeId : MY_MAPTYPE_ID
						};

						var map = new google.maps.Map(document
								.getElementById('gmap-canvas'), mapOptions);

						var marker = new google.maps.Marker({
							position : myLatlng,
							map : map,
							title : maptitle
						});

						var featureOpts = [ {
							stylers : [ {
								hue : '#74882E'
							}, {
								visibility : 'simplified'
							}, {
								gamma : 0.5
							}, {
								weight : 0.5
							} ]
						}, {
							elementType : 'labels',
							stylers : [ {
								visibility : 'on'
							} ]
						}, {
							featureType : 'water',
							stylers : [ {
								color : '#8AB0A3'
							} ]
						} ];

						var styledMapOptions = {
							name : 'ABMI'
						};

						var customMapType = new google.maps.StyledMapType(
								featureOpts, styledMapOptions);

						map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

					}

					// ==============================================

					if (jQuery("#gmap-canvas").length) {

						initializeGmap($("#gmap-canvas").data('gmaplat'), $(
								"#gmap-canvas").data('gmaplon'), $(
								"#gmap-canvas").data('gmaptitle'));
					}

					// ==============================================

					/* LOAD MAP VIA CLICK */
					jQuery("body").on(
							"click",
							"a.gmap-content",
							function(event) {

								event.preventDefault();

								initializeGmap(jQuery(this).data('gmaplat'),
										jQuery(this).data('gmaplon'), jQuery(
												this).data

										('gmaptitle'));

								$('html, body')
										.animate(
												{
													scrollTop : parseInt($(
															'#gmap-canvas')
															.offset().top - 20)
												}, 800);

							});

					// ==============================================

					if ($("#jqueryTabs").length) {
						$("#jqueryTabs").tabs({
							collapsible : true
						});
					}

					/*
					 * if ($(".raw-sidenav").length){ $( window
					 * ).resize(function() { var min_height =
					 * $(".raw-sidenav").outerHeight();
					 * $(".rawdata-container").css("min-height", min_height);
					 * });
					 * 
					 * 
					 * var min_height = $("#rawdata-content").outerHeight();
					 * 
					 * 
					 * $(".raw-sidenav").css("min-height", min_height);
					 *  }
					 * 
					 */

					if ($(".glossarycite").length) {
						// tool tip on glossary list
						$("footer").after("<div class='md-overlay'></div>"); 
						
						var deferreds = []; // run fire events after all the glossary terms are feteched and content udpated.
					   	$(".glossarycite").each(
								function(index) {
									var currentGlossary = $(this);
									var orgterm = $(this).text();

									var leadLetter = orgterm.substring(0, 1);

									var url = "/.ajax/glossary/getGlossaryDetails?name="
											+ orgterm;
									
									deferreds.push($.getJSON(url, function(data) {
										 updateGlossaryDialogBox(orgterm, data);	
									 	}));

								});
						
					   	//* do the modalEffect after everything is done.
						$.when.apply($, deferreds).done(function(){
								try {

									var ajaxEvent = new CustomEvent('ajaxLoaded');

									document.dispatchEvent(ajaxEvent);
								} catch (e) { // for ie 9 and above CustomEvent is not
										// supported. tried to jquery, but
										// jquery trigger doesn't fire event
									
									var evt = document.createEvent("CustomEvent");
									evt.initCustomEvent('ajaxLoaded', false, false, null);

									document.dispatchEvent(evt);
								}
							}
							);
							

						function updateGlossaryDialogBox(orgterm, data) {
							var termname = orgterm.replace(
									/([. *+?^=!:${}()|\[\]\/\\])/g, "_");
							var term = data.name;
							$("footer")
									.after(
											'<div id="'
													+ termname
													+ '" class="md-modal md-effect-1"><a class="md-close">Close me!</a><div class="md-content"></div></div>');
							$("#" + termname + " .md-content").append(data.detail);

							var aLink = "<a class='md-trigger' data-modal='"
									+ termname + "'>";

							$(".container strong").replaceText(term.toLowerCase(),
									aLink + term.toLowerCase() + "</a>");
							$(".container p").replaceText(term,
									aLink + "" + term + "</a>");
							$(".container p").replaceText(term.toLowerCase(),
									aLink + term.toLowerCase() + "</a>");

							$(".container li").replaceText(term.toLowerCase(),
									aLink + term.toLowerCase() + "</a>");
							$(".container li").replaceText(term,
									aLink + term + "</a>");
							$(".container td").replaceText(term.toLowerCase(),
									aLink + term.toLowerCase() + "</a>");
							$(".container td").replaceText(term,
									aLink + "" + term + "</a>");

							
						}

					}

					
					
					
					// ==================================================
					if ($("#scheduler_here").length) {
						initcalendar();
					}

					if ($(".rawdatadownload").length) {
						initrawdatamenu();
					}

					if ($("#da-specieslist").length) {
						initSpeciesList();
					}
					
					if($(".toggle_info").length) {
					$(".toggle_info").hide();
					jQuery(".toggle_link").click(function () {
						
						if($(this).parents(".proj_toggle_box").hasClass("open")){
							jQuery(this).parents('.proj_toggle_box').removeClass("open");
							jQuery(this).next(".toggle_info").slideUp();
						}else{
							jQuery(this).parents('.proj_toggle_box').addClass("open");
							jQuery(this).next(".toggle_info").slideDown();
						}
					});
					}
					
					

				
					
					
				});// ON DOC READY

function fetchGlossary(term) {
	var leadLetter = term.substring(0, 1);
	var termname = term.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "-");

	// var url
	// ="/.rest/properties/v1/website/home/glossary/leftColumnArea/"+leadLetter
	// +"/GlossaryArea/" + termname +
	//
	// "/detail";

	var url = "/.ajax/glossary/getGlossaryDetails?name=" + term;

	$.get(url, function(data) {

		alert(data.values);
	}, "json");

}

/* top google search box */
$('#qsmall').keyup(function(e) {
	/** the small search box * */
	$("#q").val($("#qsmall").val());
	if (e.keyCode == 13) {

		openSearch();
	}
});

$('#q').keyup(function(e) {
	if (e.keyCode == 13) {
		openSearch();
	}
});

function openSearch(e) {
	if (e == "Publications") {

		try {

			if ($("#gsc-i-id2").val() != $(" #q2").val()) {
				$("#gsc-i-id2").val($("#q2").val());
			}
			// bind two input
			$("#gsc-i-id2").unbind();
			$("#gsc-i-id2").change(function() {

				$("#q2").val($("#gsc-i-id2").val());

			});

			if ($("#gsc-i-id2").val()) {
				$("#publicationgoogle .gsc-search-button").click();
			}

			if ($("#csepublication").dialog("option", "width")) {
				$("#csepublication").dialog('open');
			} else {
				$("#csepublication").dialog({
					height : 600,
					width : 800,
					title : "Search ABMI.ca Publication"
				});
			}
		} catch (e) {
			$("#csepublication").dialog({
				height : 600,
				width : 800,
				title : "Search ABMI.ca Publication"
			});
		}

	} else {
		try {

			if ($("#gsc-i-id1").val() != $("#q").val()) {
				$("#gsc-i-id1").val($("#q").val());
			}
			// bind two input
			$("#gsc-i-id1").unbind();
			$("#gsc-i-id1").change(function() {

				$("#q").val($("#gsc-i-id1").val());

			});

			if ($("#gsc-i-id1").val()) {
				$(".gsc-search-button").click();
			}

			if ($("#cse").dialog("option", "width")) {
				$("#cse").dialog('open');
			} else {
				$("#cse").dialog({
					height : 600,
					width : 800,
					title : "Search ABMI.ca"
				});
			}
		} catch (e) {
			$("#cse").dialog({
				height : 600,
				width : 800,
				title : "Search ABMI.ca"
			});
		}
	}
}

/*
 * _____________________________________________________________________________
 * 
 * 
 * _____________________________________________________________________________
 */
String.prototype.endsWith = function(suffix) {
	return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

String.prototype.endsWithIgnoreCase = function(suffix) {
	return this.toUpperCase().indexOf(suffix.toUpperCase(),
			this.length - suffix.length) !== -1;
};


