var rootPath = "/.ajax/bioBrowser/"
var searchAutoCompleteUrl = rootPath +"geAutoCompeleteSpecies";
var speciesListUrl = rootPath + "getSpeciesList";
function initSpeciesList() {

					// set search input as autocomplete
					var myCombo = new dhtmlXCombo("searchSpecies", "combo", 230);
				//	myCombo.enableFilteringMode(true, searchAutoCompleteUrl, true);

			myCombo.enableFilteringMode(true,"dummy"); 
			myCombo.attachEvent("onDynXLS", myComboFilter);
 
			function myComboFilter(text){//where 'text' is the text typed by the user in the combo
   			 myCombo.clearAll();
   				$.get(searchAutoCompleteUrl, {mask:text}, function(data){
      	 			 myCombo.load(data);
				 myCombo.show();
    				}, "json");
		};

					
					// bind search button to load species information
					$("#searchBtn").bind("click", function(){loadSpecies(null, myCombo.getComboText());} );
				}
	


function loadSpecies(taxonGroup, speciesName){

			
			$.get(

				   speciesListUrl,

				   {group:taxonGroup, speciesName:speciesName},

				  displaySpecies,

				  "json"

				);


}

function displaySpecies(data){
	jQuery.each( data.rows, function( i, row ) {
		var htmlStr = "<li>";
		htmlStr += "<span>"+row.common_name + "</span>";
		htmlStr += "<span>"+row.scientific_name + "</span>";
		htmlStr += "<span>"+row.rank + "</span>";
		htmlStr  += "</li>";
		
		$("#speciesContent").append(htmlStr);
		});

	if (data.hasMore == "true"){
		$("#speciesContent").append("load more");
	}

}