// google track

(function(i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] || function() {
		(i[r].q = i[r].q || []).push(arguments)
	}, i[r].l = 1 * new Date();
	a = s.createElement(o), m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-58757400-1', 'auto');
ga('send', 'pageview');

// collect link to pdf, zip, click

$(document).ready(function() {
/*ignore header, only search in main body*/
	$('.row a[href]').each(function() {
		var href = $(this).attr("href");
		if (href.endsWithIgnoreCase(".zip") || 
			href.endsWithIgnoreCase(".pdf") ||
			href.endsWithIgnoreCase(".xlsx") ||
			href.endsWithIgnoreCase(".docs") 
			||
			href.endsWithIgnoreCase("#") 
			){
					var target = $(this).attr("target");
					var text = $(this).text();
					var thisEvent = $(this).attr("onclick"); // grab the original onclick
																// event
					$(this).click(function(event) { // when someone clicks these links
						event.preventDefault(); // don�t open the link yet
						ga('send', 'event', 'button', 'click', window.location + "[" + text + "] (" + href + ")");
						
						setTimeout(function() { // now wait 300 milliseconds�
							eval(thisEvent); // � and continue with the onclick event
							window.open(href, (!target ? "_self" : target)); // �and open
																				// the link
																				// as usual
						}, 300);
					});
		}
	});

});
