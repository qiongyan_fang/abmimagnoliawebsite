/* -----------------------------------------------------------------------

ABMI Slide Navigation Menu / JS File (Menu System)
Version: 1.0
Latest Update: 20/11/2014
By: Syncopate Media
Email: hosting@syncopatemedia.com

----------------------------------------------------------------------- */

function initMenuHandler() { 
	
			/* ----- GET DATA BTN ------- 
			$("div#get-data-btn").click(function(e) {

				e.preventDefault();
				
				var allDataFields = $('#data-input-container').find('input:checkbox');
				var jsonData = [{}];

	            allDataFields.each(function(){
					
					jsonData[$(this).prop('id')] = $(this).prop('checked');
					
					//DEBUG
					console.log($(this).prop('id') + ' >> '+$(this).prop('checked') );
						
	            });  
	            
	          
            
            /* ++++++++++++++++++++++++   *
                        
				//AJAX
				$.ajax({
				  type: "POST",
				  cache: false,
				  url: "processingfile.html",
				  data: jsonData
				})
					.done(function( html ) {
					
						$('#download-data-area').fadeOut( 400, function() {	
							$('#download-data-area').html( html ).fadeIn( 800 );  
						});

					});//done ajax
				
				
			});//function 
			*/
			/* ----- GET DATA BTN ------- */


		/* --------------------------------------- */


            /* GENERAL SECTION */ 
			$("#map-container").hide();
			$('.sub-sub-options').hide();
			$('.sub-optns').hide();
			$(".sub-slide-nav").hide();
			$(".slide-nav > .menu-child").hide();
            $(".slide-nav").hide();
            /* END GENERAL SECTION */
  
            
		/* --------------------------------------- */


			/* ----- .parent BTN ------- */
            $(".parent").click(function() {

				
				$(".slide-nav > .menu-child").hide();

	            $(".slide-nav").animate({width: 'toggle'}, function(){
		            $(".slide-nav > .menu-child").fadeToggle(20, "linear");    
		        });
				
				if ($("#map-container").is(":visible")) {
					$("#map-container").fadeToggle(200, "linear");
				//map-container
				}
				
				var arrowDiv =  $(this).find("i");
				if (arrowDiv.hasClass("leftarrow")){
					arrowDiv.removeClass('leftarrow');
					arrowDiv.addClass('rightarrow');
				}
				else{
					arrowDiv.removeClass('rightarrow');
					arrowDiv.addClass('leftarrow');
				}

            });//click function
			/* ----- .parent BTN ------- */
			            
 
		/* --------------------------------------- */

		
			/* ----- .parent-gis-data BTN,  SIDENAV GIS DATA SUB MENU ------- */
			$(".parent-gis-data").click(function() {
				
				$(".gis-data").slideToggle(200, function(){
					if( $(".gis-data").css('display') == 'none' ){ $(".parent-gis-data > a > i").css({'background-position':'-48px 0px'}); }
					else{ $(".parent-gis-data > a > i").css({'background-position':'-28px 0px'}); }	
				});

			});//click function
			/* ----- .parent-gis-data BTN,  SIDENAV GIS DATA SUB MENU ------- */


		/* --------------------------------------- */

			
			/* -----  a.first-child BTN,  SIDENAV SUB MENU ------- */			
            $("a.first-child").click(function(){
	            
	            var thisE = $(this);

				console.log('has child '+ thisE.prop('id'));
            
				thisE.addClass('sub-parent-active');
				
				thisE.next("div.sub-slide-nav").children(".menu-child").hide();

	            thisE.next("div.sub-slide-nav").animate({width: 'toggle'}, function(){
		            thisE.next("div.sub-slide-nav").children(".menu-child").fadeToggle(200, "linear", function(){
			            
			            $("a.first-child").each(function(){
	
							if( thisE.prop('id') != $(this).prop('id') ){
								
								if( $(this).hasClass('sub-parent-active') ){
									$(this).removeClass('sub-parent-active');
								}
								
								if( $(this).next("div.sub-slide-nav").is(":visible") ){
									
									var tnext = $(this).next("div.sub-slide-nav");
									
									tnext.children(".menu-child").fadeOut(100, function(){
										tnext.fadeOut(200);
									});
			
								}
								
							}//if not this
							
						});//for each cleanup
						
						//----------------------------------
						if ( (thisE.next("div.sub-slide-nav").is(":visible") && !$("#map-container").is(":visible")) || (!thisE.next("div.sub-slide-nav").is(":visible") && $("#map-container").is(":visible")) ) {
							$("#map-container").fadeToggle(200, "linear");
						//map-container
						}
			            //----------------------------------
			            			            
		            });    
		        });

			});//a.first-child.click function
			/* -----  a.first-child BTN,  SIDENAV SUB MENU ------- */
			
			
			/* ==================================================================================== */
			
			
			/* -----  TERRESTRIAL SIDENAV SUB SUB MENU ------- */
            $('.terrestrial-form').on('change', 'input:checkbox', function() {
	            
	            updateOptions( $(this).data('name'), $(this).data('heading') );
                $(this).parent().toggleClass('highlight', this.checked);
                
            });

            $("#select-terrestrial").click(function() {

                $('.terrestrial-form input[type=checkbox]').attr('checked', true);
             
                $('.terrestrial-form input[type=checkbox]').parent().addClass('highlight', this.checked);
                
               updateOptions( 'terrestrial-habitat', 'terrestrial-habitat' );
               updateOptions( 'terrestrial-species', 'terrestrial-species' );
               
            });
            
            
            $("#clear-terrestrial").click(function(e) {
	           
	           e.preventDefault();
	           
	           $("#clear-terrestrial").trigger("reset");

	           $('.terrestrial-form input[type=checkbox]').attr('checked', false);
	             
               $('.terrestrial-form input[type=checkbox]').parent().addClass('highlight', this.checked);
               
              updateOptions( 'terrestrial-habitat', 'terrestrial-habitat' );
              updateOptions( 'terrestrial-species', 'terrestrial-species' );
               
            });
			/* -----  TERRESTRIAL SIDENAV SUB SUB MENU ------- */


			/* ==================================================================================== */


			/* -----  WETLAND SIDENAV SUB SUB MENU ------- */
            $('.wetland-form').on('change', 'input:checkbox', function() {
	            
	            updateOptions( $(this).data('name'), $(this).data('heading') );
                $(this).parent().toggleClass('highlight', this.checked);
                
            });

            $("#select-wetland").click(function() {

            	  $('.wetland-form input[type=checkbox]').attr('checked', true);
                  
                  $('.wetland-form input[type=checkbox]').parent().addClass('highlight', this.checked);
                  
                 updateOptions( 'wetland-habitat', 'wetland-habitat' );
                 updateOptions( 'wetland-species', 'wetland-species' );
                 
              
               
            });
            
            
            $("#clear-wetland").click(function(e) {
	           
	           e.preventDefault();
	           
	           $("#clear-wetland").trigger("reset");

	           $('.wetland-form input[type=checkbox]').attr('checked', false);
               
               $('.wetland-form input[type=checkbox]').parent().addClass('highlight', this.checked);
               
              updateOptions( 'wetland-habitat', 'wetland-habitat' );
              updateOptions( 'wetland-species', 'wetland-species' );
               
            });
			/* -----  WETLAND SIDENAV SUB SUB MENU ------- */
        

			/* ==================================================================================== */


			$('.region-form').on('change', 'input:checkbox', function() {
				
				if( !$(this).hasClass('has-child-sub') ){
					updateOptions( $(this).prop('name'), $(this).data('heading') );
				}
				
				
				//change on region changes
				/*
				 * <a href="assets/g_forest.jpg" class="zoom fullzoom">
	<img src="assets/g_forest_thumb.jpg" alt="sample" />
</a>

				 */
				if( $(this).hasClass('regiongroupinput') ){
					var idStr = $(this).attr("id"); // id is inform of rl4 rl5
					var idInt = idStr.substring(2, idStr.length);
					
					var newhtml = "";
					
					if (idInt != '0') {					
						$("#maphref").show();	
						$("#maphref").attr('href',  "/docroot/assets/maps/rawdata-map-full-"+ idInt + ".jpg");
						$("#mapimg").attr('src',  "/docroot/assets/maps/rawdata-map-"+ idInt + ".jpg");
					}
					else{
						$("#maphref").attr('href',  "#");
						$("#mapimg").attr('src',  "/docroot/assets/maps/rawdata-map-"+ idInt + ".jpg");
						$("#maphref").hide();	
					}
					
					$('#map-container > #html-map').fadeOut( 400, function() {	
						$('#map-container > #html-map').fadeIn( 800 );  
					});
					
					
					  // collapse other regions, and uncheck others
	                $(this).parentsUntil("#regiondetail").filter('.groupregion').siblings().find("input").attr('checked', false);
	                $(this).parentsUntil("#regiondetail").filter('.groupregion').siblings().find("input").removeClass('highlight');
	                $(this).parentsUntil("#regiondetail").filter('.groupregion').siblings().find('ul.sub-optns:first').slideUp();
	       
	                updateOptions( "region", "async-region" );
				}
				
            });			

			/*$(".region-form input[type=checkbox]").click(function() {
				
				var newhtml = '';

					var allDataFields = $('#data-input-container').find('input:checkbox');
					var jsonData = [{'region-select': $(this).prop('id')}];

					allDataFields.each(function(){
					
						jsonData[$(this).prop('id')] = $(this).prop('checked');
						
	            	}); 

					//AJAX
					$.ajax({
					  type: "POST",
					  cache: false,
					  url: "getmapfile.html",
					  data: jsonData
					})
					  .done(function( html ) { newhtml = html; });
				
				//}//IF checked
					
				$('#map-container > #html-map').fadeOut( 400, function() {	
					$('#map-container > #html-map').html( newhtml ).fadeIn( 800 );  
				});
				

			});//function
			

            $("#clear-region-form").click(function() {
	            
	            console.log("#clear-region-form");
                
                $('.region-form').trigger("reset");
                $('.region-form ul.sub-optns').slideUp();
                $('#map-container > #html-map').fadeOut( 300, function() {	
					$('#map-container > #html-map').html('').fadeIn( 600 );  
				});
            });//function
            
            */
            /* ------------------------- */
            
            //natural-regions
            
            $('.has-child-sub').click(function(e) {
                
                var thisbox = $(this);
                var childul = $(this).parent().next('ul.sub-optns');
                
                if ( !childul.is(':visible') ){

                    childul.slideDown();
                    thisbox.prop('checked', true);

                }
                else{

	                childul.slideUp();
	                
	                var boxes = childul.find('input:checkbox');
	                var active = false;
	                
	                boxes.each(function(){
		                if( $(this).prop('checked') ){
			                active = true;
			                thisbox.prop('checked', true);
		                }
	                });
	                
	                if( !active ){
		                boxes.each(function(){
		                	$(this).prop('checked', false);
	                	});
	                }
	                                
				}//else
                
              
					
            }).change();//function
            
            /* ---------------when on region is choose uncheck other regions under different namaes-------------------- */

  $('input[name=region]').click(function(e) {
                
                var thisbox = $(this);
                 $(this).prop('checked');
                if (!$(this).prop('checked')){
                	//when uncheck do nothing
                	return;
                }
                
                // when check; uncheck other input box
               $(this).parentsUntil("#regiondetail").filter('.groupregion').siblings().find("input").attr('checked', false);
               $(this).parentsUntil("#regiondetail").filter('.groupregion').siblings().find("input").removeClass('highlight');
               $(this).parentsUntil("#regiondetail").filter('.groupregion').siblings().find('ul.sub-optns:first').slideUp();
                // collapse other regions, and uncheck others
                
					
            }).change();//function
           

            $('.years-form').on('change', 'input:checkbox', function() {
	            updateOptions( $(this).prop('name'), $(this).data('heading') );
                $(this).parent().toggleClass('highlight', this.checked);
            });

            $("#select-years").click(function() {
	          
                $('input[name=rotation]').attr('checked', true);
                $('input[name=rotation]').parent().removeClass('highlight', this.checked);
                updateOptions( "rotation", "async-year" );
            });                 

            $("#clear-years").click(function() {
  	          
                $('input[name=rotation]').attr('checked', false);
                $('input[name=rotation]').parent().removeClass('highlight', this.checked);
                updateOptions( "rotation", "async-year" );
            });    

/* END YEARS SECTION */

            $('.data-type-form').on('change', 'input:radio', function() {  
	            updateOptions( $(this).prop('name'), $(this).data('heading') );
                $(this).parent().toggleClass('highlight', this.checked);
            });

/* END DATA TYPE SECTION */

// set default options
            $("#rl0").trigger("click");  // alberta region
            $("#coreoffgrid3").trigger("click"); // both off and core data
            $(".years-form input[type=checkbox]").trigger("click");
            $(".parent-active").trigger("click"); 
            
           

}// end primary load function

			var updateOptions = function(thisName,thisID){
//			        console.log('Array: '+ thisName );
			        
			        var vals = '';
			        var sep = '';
			      
			        $('input[data-name='+thisName+']').each(function(){
			        	
			        	if( $(this).prop('checked') ){
				        	
//				        	console.log( $(this).val() );
				        	vals += sep + $(this).val();
				        	
					        sep = ', ';
		
				        }
				        
			        });
			        
			        if (thisID == "async-years" &
			        $('input[name='+thisName+']:checkbox:not(:checked)').length == 0 ){
			        	vals = "All Years";
			        }
			        else {
			        $('input[name='+thisName+']').each(function(){
			        	
			        	if( $(this).prop('checked') ){
				        	
//				        	console.log( $(this).val() );
				        	vals += sep + $(this).val();
				        	
					        sep = ', ';
		
				        }
				        
			        });
			        }
			        
//			        console.log( 'to id '+ thisID + '' + vals );
			        if(vals.length == 0){ vals = '<span class="noselect">none selected</span>'; }
			        $('#map-container #async-map-details #'+  thisID).html(vals); 
			    };


