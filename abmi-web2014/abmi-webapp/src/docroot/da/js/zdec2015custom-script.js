document.write('<script type="text/javascript" src="/docroot/da/js/jquery.dataTables.js"></script>');
document.write('<script type="text/javascript" src="https://cdn.datatables.net/scroller/1.3.0/js/dataTables.scroller.min.js"></script>');

document.write('<script type="text/javascript" src="/docroot/da/js/jquery.bxslider.js"></script>');
document.write('<script type="text/javascript" src="/docroot/da/js/waypoints.min.js"></script>');
document.write('<script type="text/javascript" src="/docroot/da/js/jquery.counterup.min.js"></script>');
document.write('<script type="text/javascript" src="/docroot/da/js/isotope.pkgd.min.js"></script>');

document.write('<script type="text/javascript" src="/docroot/da/js/da_abmi.js"></script>');

//$(window).load(function(){
//$('#data').dataTable({
//    "aaSorting": [[0, "desc"]]
//});
//
//});//]]> 

/*$(function() {
  $('#subnavbar a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

*/
jQuery(document).ready(function($){
	
	$('.subNav').each(function(){
		var secondaryNav = $('.subNav'),
			secondaryNavTopPosition = $('.browser-banner').offset().top + $('.browser-banner').height() - $('.data-nav').height();
		
		$(window).on('scroll', function(){
				
			//on desktop - fix secondary navigation on scrolling
			if($(window).scrollTop() > secondaryNavTopPosition ) {
				//fix secondary navigation
				secondaryNav.addClass('is-fixed');
				
			} else {
				secondaryNav.removeClass('is-fixed');
			}
	
			
		});
		secondaryNav.find('ul a').on('click', function(event){
	        event.preventDefault();
	        var target= $(this.hash);
	        $('body,html').animate({
	        	'scrollTop': target.offset().top - secondaryNav.height()
	        	}, 400
	        ); 
	        //on mobile - close secondary navigation
	        $('.subNav .collapse').removeClass('in');
	        $('.subNav .navbar-toggle').addClass('collapsed');
	        
	    });
	});
	
    
});

jQuery(document).ready(function($) {

	
	
	/* getting viewport width */
    var responsive_viewport = $(window).width();
    var ar = $.makeArray()
		
	//Set equal Height
	if (responsive_viewport > 992) {
		
		//Set equal Height
		$('.set-height').each(function(){
			ar.push($(this).outerHeight() + $(".quickbox-link").outerHeight());
		});
		$('.set-height').css('height', Math.max.apply( Math, ar ));
		
		//For Biodiversity Browser page
		$('.stat-analysis-collaborations h2').each(function(){
			ar.push($(this).outerHeight());
		});
		$('.stat-analysis-collaborations h2').css('height', Math.max.apply( Math, ar ));
		
		
	} /* end larger than 992px */
		
	if (responsive_viewport > 767) {
		//For Data Download Overview page
		$('.overview-link .caption').each(function(){
			ar.push($(this).outerHeight());
		});
		$('.overview-link .caption').css('height', Math.max.apply( Math, ar ));
		
		// Single Data Page set height for image block
		var h_single_data = $(".single-data").outerHeight()
		$('.single-data-img').css('height', h_single_data);
		
	} /* end larger than 767px */	
	/* counter for Species Monitored */ 
    $('.counter').counterUp({
        delay: 50,
        time: 1000
    });

	/* Bx Slider for News post*/
	$('.bxslider').bxSlider({
		minSlides: 1,
	  maxSlides: 1,
	  nextSelector: '#slider-next',
	  nextText: '',
	  pager:false
	});
	
	
	
	/*List View/ Grid View */
	$(".sidemenuListview").click(function() {
		$( ".sidemenuListview" ).addClass("active");
		$( ".sidemenuGridview" ).removeClass("active");
		$( ".data-post" ).addClass("list-view");
		$( ".data-post" ).removeClass("grid-view");
		
	});
	$(".sidemenuGridview").click(function() {
		$( ".sidemenuGridview" ).addClass("active");
		$( ".sidemenuListview" ).removeClass("active");
		$( ".data-post" ).addClass("grid-view");
		$( ".data-post" ).removeClass("list-view");
	});

	/* Toggle databy Year */
	$(".yearData").hide();
	$(".databy-year h4").click(function () {
		$(this).toggleClass("open");
		$(this).next(".yearData").slideToggle();
	});
	
	
	/*Select Species Group */
	$(".species-groupList").hide();
	$(".slectGroup").click(function () {
		$(this).toggleClass("open");
		$(".species-groupList").slideToggle();
	});
	
	// init Isotope
	  var $container = $('.isotope').isotope({
	    itemSelector: '.element-item',
	    layoutMode: 'fitRows',
	    getSortData: {
	      name: '.name',
	      symbol: '.symbol',
	      number: '.number parseInt',
	      category: '[data-category]',
	      weight: function( itemElem ) {
	        var weight = $( itemElem ).find('.weight').text();
	        return parseFloat( weight.replace( /[\(\)]/g, '') );
	      }
	    }
	  });
	
	  // filter functions
	  var filterFns = {
	    // show if number is greater than 50
	    numberGreaterThan50: function() {
	      var number = $(this).find('.number').text();
	      return parseInt( number, 10 ) > 50;
	    },
	    // show if name ends with -ium
	    ium: function() {
	      var name = $(this).find('.name').text();
	      return name.match( /ium$/ );
	    }
	  };
	
	  // bind filter button click
	  $('#filters').on( 'click', 'button', function() {
	    var filterValue = $( this ).attr('data-filter');
	    // use filterFn if matches value
	    filterValue = filterFns[ filterValue ] || filterValue;
	    $container.isotope({ filter: filterValue });
	  });
	
	  // bind sort button click
	  $('#sorts').on( 'click', 'button', function() {
	    var sortByValue = $(this).attr('data-sort-by');
	    $container.isotope({ sortBy: sortByValue });
	  });
	  
	  // change is-checked class on buttons
	  $('.button-group').each( function( i, buttonGroup ) {
	    var $buttonGroup = $( buttonGroup );
	    $buttonGroup.on( 'click', 'button', function() {
	      $buttonGroup.find('.is-checked').removeClass('is-checked');
	      $( this ).addClass('is-checked');
	    });
	  });
  

	/*Navigarion Position on Scroll*/
	var $j = jQuery;
							
	window.navi = {
		fixed: false,
		visible: true,
		lastST: 0,
		lastSTup: 0
	};
	window.placeholder_mobile = false;

	window.navi.elm = jQuery(".data-nav");
	
	//--------------------------------------------
	
	jQuery(window).on("scroll", function(event) {
		
		var st = jQuery(this).scrollTop();
		if (st > 350 && !window.navi.fixed) {
			window.navi.fixed = true;
			window.navi.elm.addClass("fixed");
		} else if (st < 10 && window.navi.fixed) {
			window.navi.fixed = false;
			window.navi.visible = true;
			window.navi.elm.removeClass("fixed invisible visible").css("position", "inherit");
			window.setTimeout('window.navi.elm.css("position", ""); ', 1); // css position wegen safari in ios7
		}
		var test = true;
		if (st > 350 && window.navi.visible && (st - window.navi.lastST > 0)) {
						
			window.navi.visible = false;
			test = false;
			window.setTimeout('window.navi.elm.addClass("invisible").removeClass("visible"); ', 1);
		}
		if (!window.navi.visible && (st - window.navi.lastSTup < -100) && test) {
			window.navi.visible = true;
			window.navi.elm.addClass("visible").removeClass("invisible");
		}
		if (window.navi.visible && (st - window.navi.lastST > 0) && $(".navbar-collapse.collapse").hasClass("in")) {
			window.navi.visible = true;
			window.navi.elm.addClass("visible").removeClass("invisible");
		}

		if (st - window.navi.lastST > 0)
			window.navi.lastSTup = st;
			window.navi.lastST = st;
	
	});//On Scroll
	


});//Document read