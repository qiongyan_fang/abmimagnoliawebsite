var gCurrentIndex;
var gCount = 100;
/*
 * @RequestParam(value = "start", required = false) Integer startFrom,
 * @RequestParam(value = "count", required = false) Integer count,
 */
function loadSpecies(taxonGroup, speciesName,  start,count) {

	$.get(

	speciesListUrl,

	{
		group : taxonGroup,
		speciesName : speciesName,
		start:start,
		count:count
	},

	function(data) {displaySpecies(data, speciesName, start, count);},

	"json"

	);

}
/*
 * <td width="12%"><div class="pagetitleimg"><img src="assets/birds-sm.jpg"
 * alt=""></div></a></td> <td><a href="#" class="basic">Basic</a></td>
 * <td><a href="#">Bird Common Name1</a></td> <td><a href="#">Scientific
 * Name5</a></td>
 */

function displaySpecies(data,speciesName, start, count) {
	if (start == 0){
		$("#dataBody").empty();
	}
	jQuery
			.each(
					data.rows,
					function(i, row) {
						var htmlStr = "<tr>";
						htmlStr += '<td width="12%"><div class="pagetitleimg"><img src="assets/birds-sm.jpg" alt=""></div></a></td>';
						htmlStr += '<td class="'+ row.summary_type.toLowerCase() 

+ '">' + row.summary_type
								+ '</td>';
						htmlStr += '<td>' + row.common_name + '</td>';
						htmlStr += "<td>" + row.scientific_name + "</td>";
						if (row.taxa) {
							htmlStr += "<td>" + row.taxa + "</td>";
						}

						htmlStr += "</tr>";

						$("#dataBody").append(htmlStr);
					});
	
	

	gCurrentIndex = data.start;
	
	$("#load-more").hide();
	if (data.hasMore == "true") {
		$("#load-more").show();
		$("#load-more").unbind();
		$("#load-more").bind("click", function() {
		loadSpecies(gSpeciesGroup, speciesName, gCurrentIndex+data.rows.length+1, gCount);});
	
		
	}

}


// test 2

//

// Pipelining function for DataTables. To be used to the `ajax` option of
// DataTables

//

$.fn.dataTable.pipeline = function ( opts ) {

    // Configuration options

    var conf = $.extend( {

        pages: 5,     // number of pages to cache

        url: '',      // script url

        data: null,   // function or object with parameters to send to the
						// server

                      // matching how `ajax.data` works in DataTables

        method: 'GET' // Ajax HTTP method,
	

    }, opts );

 

    // Private variables for storing the cache

    var cacheLower = -1;

    var cacheUpper = null;

    var cacheLastRequest = null;

    var cacheLastJson = null;

 

    return function ( request, drawCallback, settings ) {

        var ajax          = false;

        var requestStart  = request.start;

        var drawStart     = request.start;

        var requestLength = request.length;

        var requestEnd    = requestStart + requestLength;

         

        if ( settings.clearCache ) {

            // API requested that the cache be cleared

            ajax = true;

            settings.clearCache = false;

        }

        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {

            // outside cached data - need to make a request

            ajax = true;

        }

        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||

                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||

                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )

        ) {

            // properties changed (ordering, columns, searching)

            ajax = true;

        }

         

        // Store the request for checking next time around

        cacheLastRequest = $.extend( true, {}, request );

 

        if ( ajax ) {

            // Need data from the server

            if ( requestStart < cacheLower ) {

                requestStart = requestStart - (requestLength*(conf.pages-1));

 

                if ( requestStart < 0 ) {

                    requestStart = 0;

                }

            }

             

            cacheLower = requestStart;

            cacheUpper = requestStart + (requestLength * conf.pages);

 

            request.start = requestStart;

            request.length = requestLength*conf.pages;

 

            // Provide the same `data` options as DataTables.

            if ( $.isFunction ( conf.data ) ) {

                // As a function it is executed with the data object as an arg

                // for manipulation. If an object is returned, it is used as the

                // data object to submit

                var d = conf.data( request );

                if ( d ) {

                    $.extend( request, d );

                }

            }

            else if ( $.isPlainObject( conf.data ) ) {

                // As an object, the data given extends the default

                $.extend( request, conf.data );

            }

 

            settings.jqXHR = $.ajax( {

                "type":     conf.method,

                "url":      conf.url,

                "data":     request,

                "dataType": "json",

                "cache":    false,

                "success":  function ( json ) {

                    cacheLastJson = $.extend(true, {}, json);

 

                    if ( cacheLower != drawStart ) {

                        json.data.splice( 0, drawStart-cacheLower );

                    }

                    json.data.splice( requestLength, json.data.length );

                     

                    drawCallback( json );

                }

            } );

        }

        else {

            json = $.extend( true, {}, cacheLastJson );

            json.draw = request.draw; // Update the echo for each response

            json.data.splice( 0, requestStart-cacheLower );

            json.data.splice( requestLength, json.data.length );

 

            drawCallback(json);

        }

    }

};

 

// Register an API method that will empty the pipelined data, forcing an Ajax

// fetch on the next draw (i.e. `table.clearPipeline().draw()`)

$.fn.dataTable.Api.register( 'clearPipeline()', function () {

    return this.iterator( 'table', function ( settings ) {

        settings.clearCache = true;

    } );

} );

 

 
function planify(data) {
    for (var i = 0; i < data.columns.length; i++) {
        column = data.columns[i];
        column.searchRegex = column.search.regex;
        column.searchValue = column.search.value;
        delete(column.search);
    }
}