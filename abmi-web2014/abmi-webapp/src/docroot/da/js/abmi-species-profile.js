var root = ""; // "http://abmi-wp.ccis.ualberta.ca:8080";
var speciesDetailsUrl = root + "/.ajax/speciesProfile/getSingleSpeciesProfile";
var distributionMapUrl = root + "/.ajax/bioBrowser/geSpeciesDetection";
$(document).ready(function() {

	if ($("#species-profile-details").length) {
		initSpeciesDetails();
		initDistributionMap();
	}
});

/* send request to get species profiles from profile workspace*/

function initSpeciesDetails() {
	$.ajax({
		type : "GET",

		url : speciesDetailsUrl,
		data : {
			forApps : false,
			scientificName : scientificName
		},
		dataType : "json"
	}).done(function(data) {
		_initSpeciesDetailsHtml(data);
	})
}

/* display details in html*/
function _initSpeciesDetailsHtml(data) {
	$(".auto").hide();
	$.each(data, function(key, element) {
		if (key == "WebPhotos") {
			$.each(element, function(index, row) {
				$("." + row.imageType).attr("src", row.imageLink);
			});
		} else {
			$("." + key).html(element);

			$("." + key).show();
		}
	});
}

/* GET distribution map */

function initDistributionMap() {
	$.ajax({
		type : "GET",

		url : distributionMapUrl,
		data : {
			speciesGroup : speciesGroup,
			scientificName : scientificName
		},
		dataType : "json"
	}).done(function(data) {
		displayDistributionMap(data);
	})
}

function displayDistributionMapUsingLeaflet(data) {
	var map = L.map('map').setView([ 43.526, -122.667 ], 6);

	// L.esri.basemapLayer('Streets').addTo(map);
	var background = L.esri
			.featureLayer(
					{
						url : 'http://parkland.abmi.ualberta.ca:6080/arcgis/rest/services/distributionMap/MapServer/3',
					}).addTo(map);

	var visitedLayer = L.esri
			.featureLayer(
					{
						url : 'http://parkland.abmi.ualberta.ca:6080/arcgis/rest/services/distributionMap/MapServer/0',
					}).addTo(map);

	if (data.visitedSites) {
		visitedLayer.setWhere("SITE_ID in (" + data.visitedSites + ")");
	}

	var detectedLayer = L.esri
			.featureLayer(
					{
						url : 'http://parkland.abmi.ualberta.ca:6080/arcgis/rest/services/distributionMap/MapServer/1',
					}).addTo(map);

	if (data.detectedSites) {
		detectedLayer.setWhere("SITE_ID in (" + data.detectedSites + ")");
	}
}

/*
 * display distribution maps 1. no base map projection with basemap don't align
 * 
 * this is a temporary display. we may 1. switch to different library or 2.
 * export the results and save them in png/jpeg
 */
function displayDistributionMap(data) {
	require(
			[ "dojo/dom-construct", "esri/map",

			"esri/layers/FeatureLayer", "esri/geometry/Extent",
					"esri/InfoTemplate", "dojo/_base/array", "dojo/parser",
					"dojo/domReady!" ],
			function(domConstruct, Map, FeatureLayer, Extent, InfoTemplate,
					arrayUtils, parser) {

				parser.parse();
				var bounds = new Extent({
					xmin : -1.3458783962553922E7,
					ymin : 6257067.924742254,
					xmax : -1.2073414522442492E7,
					ymax : 8420958.726308778,
					"spatialReference" : {
						"wkid" : 102100
					// 3857
					}

				});

				var map = new Map("map", {

					extent : bounds
				});

				var visitedLayer = new FeatureLayer(
						"http://parkland.abmi.ualberta.ca:6080/arcgis/rest/services/distributionMap/MapServer/1",
						{
							mode : FeatureLayer.MODE_SNAPSHOT,
							outFields : [ "SITE_ID" ]
						});
				var detectedLayer = new FeatureLayer(
						"http://parkland.abmi.ualberta.ca:6080/arcgis/rest/services/distributionMap/MapServer/2",
						{
							mode : FeatureLayer.MODE_SNAPSHOT,
							outFields : [ "SITE_ID" ]
						});

				var backgroundLayer = new FeatureLayer(
						"http://parkland.abmi.ualberta.ca:6080/arcgis/rest/services/distributionMap/MapServer/3",
						{
							mode : FeatureLayer.MODE_SNAPSHOT
						});

				if (data.detectedSites) {
					detectedLayer.setDefinitionExpression("SITE_ID in ("
							+ data.detectedSites + ")");
				}

				if (data.visitedSites) {
					visitedLayer.setDefinitionExpression("SITE_ID in ("
							+ data.visitedSites + ")");
				}

				// backgroundLayer
				// .on(
				// "load",
				// function(evt) {
				// // project the extent if the map's spatial
				// // reference is different that the layer's
				// // extent.
				// var gs = esriConfig.defaults.geometryService;
				// var extent = evt.layer.fullExtent;
				// if (extent.spatialReference.wkid ===
				// map.spatialReference.wkid) {
				// map.setExtent(extent);
				// } else {
				// gs.project([ extent ],
				// map.spatialReference).then(
				// function(results) {
				// map.setExtent(results[0]);
				// });
				// }
				// });

				// add the legend
				map.on("layers-add-result", function(evt) {
					var layerInfo = arrayUtils.map(evt.layers, function(layer,
							index) {
						return {
							layer : layer.layer,
							title : layer.layer.name
						};
					});
					if (layerInfo.length > 0) {
						var legendDijit = new Legend({
							map : map,
							layerInfos : layerInfo
						}, "legendDiv");
						legendDijit.startup();
					}
				});

				map.addLayers([ backgroundLayer, visitedLayer, detectedLayer ]);
				// map.addLayer(backgroundLayer);
			});
}

function displayDistributionMap1() {
	require(
			[ "dojo/dom-construct", "esri/map",

			"esri/layers/FeatureLayer", "esri/geometry/Extent",
					"esri/InfoTemplate", "dojo/_base/array", "dojo/parser",
					"dojo/domReady!" ],
			function(domConstruct, Map, FeatureLayer, Extent, InfoTemplate,
					arrayUtils, parser) {

				parser.parse();
				var bounds = new Extent({
					"xmax" : -108.45729455140287,
					"xmin" : -120.90229404877495,
					"ymax" : 60.09517070654722,
					"ymin" : 48.89479469766573,
					"spatialReference" : {
						"wkid" : 4326
					}
				});

				var map = new Map("map", {

					extent : bounds
				});

				var url =

				"http://parkland.abmi.ualberta.ca:6080/arcgis/rest/services/distributionMap/MapServer/3";

				var fl = new FeatureLayer(url, {
					mode : FeatureLayer.MODE_ONDEMAND
				});

				var fl2 = new FeatureLayer

				(
						"http://parkland.abmi.ualberta.ca:6080/arcgis/rest/services/distributionMap/MapServer/1",
						{
							mode : FeatureLayer.MODE_ONDEMAND
						});

				// add the legend
				/*
				 * map.on("layers-add-result", function(evt) { var layerInfo =
				 * arrayUtils.map(evt.layers, function(layer, index) { return {
				 * layer : layer.layer, title : layer.layer.name }; }); if
				 * (layerInfo.length > 0) { var legendDijit = new Legend({ map :
				 * map, layerInfos : layerInfo }, "legendDiv");
				 * legendDijit.startup(); } });
				 */

				map.addLayers([ fl2, fl ]);
			});
}