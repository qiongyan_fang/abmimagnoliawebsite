var rootPath = "/.ajax/bioBrowser/"
var searchAutoCompleteUrl = rootPath + "geAutoCompeleteSpecies";
var speciesListUrl = rootPath + "getSpeciesList";

//

// DataTables initialisation

//
var speciesTable;
$(document)
		.ready(
				function() {

					if ($("#data-block").length > 0) {
						var fullCount = 0;
						var basicCount = 0;

						var dataDef = [ {
							"data" : "image",
							"orderable" : false,
							"searchable" : false
						}, {
							"data" : "summary_type",
							"searchable" : false
						}, {
							"data" : "common_name",
							"searchable" : true
						}, {
							"data" : "scientific_name",
							"searchable" : true
						} ];

						if (gSpeciesGroup == "Vascular Plants") {
							dataDef.push({
								"data" : "protocol_location",
								"searchable" : true
							});
						}

						speciesTable = $('#species-data')
								.on(
										'preXhr.dt',
										function(e, settings, data) { /*
																		 * change
																		 * data
																		 * format
																		 */

											for ( var i = 0; i < data.columns.length; i++) {
												column = data.columns[i];
												column.searchRegex = column.search.regex;
												column.searchValue = column.search.value;
												delete (column.search);
											}

										})

								.DataTable(
										{
											processing : true,

											serverSide : true,
											scroller : {

									            loadingIndicator: true

									        },
											scrollY : 200,
											search : false,
											deferRender : true, // render node only when needed
											paging : true,
											pageLength : 50,
											language : {

												infoEmpty : "No records to show",
												info : "Showing _START_ to _END_ of _TOTAL_ species"

											},
											ajax : speciesListUrl
													+ "?groupName="
													+ gSpeciesGroup
											/*
											 * "ajax": {
											 * //$.fn.speciesTable.pipeline(
											 * data: function(data) {
											 * planify(data); }, url:
											 * speciesListUrl+"?groupName="+gSpeciesGroup }
											 */,
											columns : dataDef,

											"createdRow" : function(row, data,
													index) {
												// Bold the grade for all 'A'
												// grade browsers
												if (data.summary_type == "Full") {
													$('td:eq(1)', row)
															.addClass("full");

												}
												else if (data.summary_type == "Forested region") {
													$('td:eq(1)', row)
															.addClass("forested");

												}else if (data.summary_type == "Prairie region") {
													$('td:eq(1)', row)
													.addClass("prairie");

												}
												else {
													$('td:eq(1)', row)
															.addClass("basic");

												}
												
												var img = $('td:eq(0)', row)
														.text();
												$('td:eq(0)', row)
														.html(
																'<div class="pagetitleimg"><img alt="" src="/docroot/da/assets/birds-sm.jpg"></div>');
												$('td:eq(0)', row).addClass(
														"datatableimg");
												
												$(row).unbind().bind("click", function(){ 
													window.open(gDetailedUrlParam + "?sname=" + data.scientific_name + "&group=" + gSpeciesGroup);
													
												});
												
											}

										});

						/* hide header, but somethinge is there */
						$('#species-data').on(
								'draw.dt',
								function() {
									$('.dataTables_scrollBody thead tr')
											.addClass('hidden');

								});

						$("#searchSpeciesBtn").bind("click", function() {
							$("#dataBody").empty();

							speciesTable

							.columns(2)

							.search($("#searchSpecies").val())

							.draw();

						});

						$("#terrestrial-filter").bind("click", function() {
							$("#dataBody").empty();

							speciesTable

							.columns(4)

							.search("Terrestrial")

							.draw();
						});
						$("#aquatic-filter").bind("click", function() {
							$("#dataBody").empty();

							speciesTable

							.columns(4)

							.search("Aquatic")

							.draw();
						});

						$("#all-filter").bind("click", function() {
							$("#dataBody").empty();

							speciesTable.columns(4).search("").draw();
						});

						$("#searchSpecies").autocomplete({
							source : function(request, response) {

								$.ajax({

									url : searchAutoCompleteUrl,

									dataType : "json",

									data : {

										mask : request.term,
										group : gSpeciesGroup

									},

									success : function(data) {

										response(data);

									}

								});

							},

							minLength : 2

						});

					}
				});