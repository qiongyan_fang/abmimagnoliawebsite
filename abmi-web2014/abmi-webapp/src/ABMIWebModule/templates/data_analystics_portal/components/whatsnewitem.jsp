<%@ include file="../../includes/taglibs.jsp"%>
<div class="news_post">
	<a class="post_title" href="#"><h4>${content.header}</h4></a>
	<h4 class="news_postdate">${content.date}</h4>
	<p>${cmsfn:decode(content).description }</p>
	<c:if test="${not empty content.url}">
		<a class="news_post_more"
			${fn:startsWith(content.url, "http")?"target=_blank":""}
			href="${content.url}>READ MORE</a>
	</c:if>
</div>