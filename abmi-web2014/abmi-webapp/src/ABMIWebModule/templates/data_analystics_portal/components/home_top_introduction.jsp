<%@ include file="../../includes/taglibs.jsp"%>
<div class="portal-info">
	<div class="container">
		<h2 class="col-sm-12 text-center">${cmsfn:decode(content).header}</h2>

		<p
			class="col-sm-12 col-lg-10 col-lg-offset-1 text-center col-lg-ex-pad">
			${cmsfn:decode(content).description}</p>

		<div
			class="col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 btn-grouping">
			<cms:area name="greenButtonArea"></cms:area>
		</div>
	</div>
</div>