<%@ include file="../../includes/taglibs.jsp"%>

<c:set var="imageContentMap"
	value="${cmsfn:content(content.backgroundImageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />


    <div class="row browser-banner"  style="background-image:url(${imageUrl});background-repeat: no-repeat;
    background-size: cover;">
	    <div class="banner-caption">
		    <div class="container">
			    <div class="textwrap">
				    <h1>${content.header}</h1>
				    <h3>${cmsfn:decode(content).header2 }</h3>
				    <p>${cmsfn:decode(content).description }</p>
		    	</div>
		    </div>
	    </div>
	</div>