<%@ include file="../../includes/taglibs.jsp"%>

<c:set var="imageContentMap"
	value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />

<c:set var="iconContentMap"
	value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="iconUrl"
	value="${cmsfn:link(cmsfn:asContentMap(iconContentMap))}" />

<c:if test="${not empty imageUrl}">
<style> 
.imgbg-${state.getCurrentContentNode().getUUID()} {
background-image: url(${imageUrl});background-repeat: no-repeat; background-size: cover;
}
</style>
</c:if>
 
                     
                    

<div class="home-quick-box set-height ${content.backgroundColor} <c:if test="${content.imagePosition == 'background'}">
		imgbg-${state.getCurrentContentNode().getUUID()}
	</c:if>"
	>
	<c:if test="${content.imagePosition == 'left'}">
		<div class="box-img imgbg-${state.getCurrentContentNode().getUUID()}">
			
		 </div>
	</c:if>
	<div class="box-text">
		<div class="quickbox-title">
			<c:if test="${not empty iconUrl}">
				<img src="${iconUrl}" alt="${content.header}">
			</c:if>
			<h3>${content.header}</h3>
		</div>

		<p>${content.description}</p>
		<c:if test="${not empty content.embed}">
							   ${content.embed}
							   </c:if>
		<div class="quickbox-link">
			<a class="line-btn btn-white"
				${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""}
				href="${content.buttonUrl}">${content.buttonText}</a>
		</div>
	</div>
	<c:if test="${content.imagePosition == 'right'}">
		<div class="box-img imgbg-${state.getCurrentContentNode().getUUID()}">
		 </div>
	</c:if>
</div>