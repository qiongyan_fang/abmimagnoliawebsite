<%@ include file="../../includes/taglibs.jsp"%>

<c:if test="${not empty content.url}">
		<a href="${content.url}" class="type-${content.fileicon}"
			target=${fn:startsWith(content.headerline1url, "/")?"":"target=_blank"}>
			${content.productname} <c:if test="${not empty content.size}">(${content.size})</c:if>
		</a>
		</c:if>
		
		<c:if test="${empty content.url}">
			<a class="type-${content.fileicon}">${content.productname} 
			<c:if 	test="${not empty content.size}">(${content.size})</c:if>
			</a>

		</c:if>
		
		
		
	