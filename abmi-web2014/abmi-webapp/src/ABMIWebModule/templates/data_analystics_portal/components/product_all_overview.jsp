<%@ include file="../../includes/taglibs.jsp"%>

<c:choose>
	<c:when test="${fn:startsWith(content.placeholderimg, 'http')}">
		<c:set var="defaultHeaderImage" value="${content.placeholderimg}" />
	</c:when>
	<c:otherwise>
		<c:set var="defaultHeaderImage"
			value="${cmsfn:link(cmsfn:content(content.placeholderimg,'dam'))}" />
	</c:otherwise>
</c:choose>

<c:set var="mode" value="${param.mode}" />
<c:url var="queryuri" value="${state.originalBrowserURI}?">
</c:url>

<c:set var="title1"
	value="${state.mainContentNode.getProperty('title').getString()}" />
<h3>${empty content.header?title1:content.header}</h3>

<div>${cmsfn:decode(content).description}</div>




<div class="row overview-quicklinks">

	<c:forEach items="${products}" var="product" varStatus="rowStatus">
		<c:set var="boxImage"
			value="${cmsfn:content(product.details.boxImage,'dam')}" />

		<div class="col-sm-6">
			<a href="${product.path}.html" class="overview-link">
				<div class="img-box">
					<!-- 1. cover image 2. default place holder image !-->
					<c:choose>
						<c:when test="${not empty boxImage }">
							<img alt="" src="${cmsfn:link(cmsfn:asContentMap(boxImage))}">
						</c:when>

						<c:when test="${not empty defaultImg }">
							<img alt="" src="${defaultImg}">
						</c:when>

					</c:choose>
				</div>
				<div class="caption box${rowStatus.index%6+1}">
					<h4>${not empty
						product.details.customTitle?product.details.customTitle:
						product.title}</h4>
					<p>${product.details.boxDescription}</p>
				</div>
			</a>
		</div>
	</c:forEach>
	</div>