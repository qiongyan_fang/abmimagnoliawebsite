<%@ include file="../../includes/taglibs.jsp" %>
<c:set var="imageContentMap"
	value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />
	
<div class="row abmi-data">
        <div class="container">
	        <div class="row">
	            <div class="col-md-6">
		            <img alt="ABMI Data" src="${imageUrl}">
	            </div>
	            <div class="col-md-6">
		            <div>
			            <h4>${content.headerText}<span>${content.headerTextOrange}</span></h4>
						<p>${cmsfn:decode(content).description}</p>
						
							<a class="solid-btn blue-btn"
				${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""}
				href="${content.buttonUrl}">${content.buttonText}</a>
		            </div>
	            </div>
	        </div>
        </div>
    </div>