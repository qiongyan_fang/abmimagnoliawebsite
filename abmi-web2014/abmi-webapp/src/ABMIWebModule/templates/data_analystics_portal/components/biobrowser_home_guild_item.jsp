<%@ include file="../../includes/taglibs.jsp"%>

<c:set var="imageContentMap"
	value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />


<div class="col-sm-2 col-xs-4 slide">
	<a href="${content.url}" class="species-item">
		<div class="speciesImg-block">
			<img src="${imageUrl}" alt="${content.guildTitle}" />
		</div> <span>${content.guildTitle}</span>
	</a>
</div>