<script>
	var scientificName = "${param.sname}";
	var speciesGroup = "${param.group}";
</script>

<!--  detailed species page header -->

<div id="species-profile-details" class="row sp-main">
	<div class="cell-img species-banner-height s-profilepic">
		<img class="mainBanner" src="" alt="" />
	</div>
	<div class="cell-img map-portal-sec species-banner-height formobile">
		<div class="btn-pos">
			<a href="#" class="mapPortal-btn">View Map Portal</a>
		</div>
		<img src="assets/albertaMap.jpg" alt="Alberta Map" />
	</div>

	<div class="banner-txt species-banner-height fordesktop" id="spp-top">
		<h2 class="auto CommonName"></h2>
		<h3 class="auto ScientificName"></h3>
		<div class="species-desc">
			<div class="auto WebMainDescription"></div>
		</div>
		<div class="species-details">
			<p class="auto">
				Protocol Type: <span>Terrestrial</span>
			</p>
			<p class="auto">
				Native Status: <span>Native</span>
			</p>
			<p class="auto">
				Concervation <span>Status: Automated</span>
			</p>
			<p class="auto">
				Guild Membership: <span>Automated</span>
			</p>
		</div>

		<a href="#" class="download-report">Download Report</a>
	</div>
	<div class="cell-img map-portal-sec species-banner-height fordesktop">
		<div class="btn-pos">
			<a href="#" class="mapPortal-btn">View Map Portal</a>
		</div>
		<div id="map"></div>
	</div>

</div>



</div>
<div class="row subNav">
	<div class="col-lg-12 species-titlebar">
		<div class="pageTitle">
			<div class="titleWrap">
				<div class="pagetitleimg">
					<img class="miniCircle" alt="" />
				</div>
				<span class="auto CommonName"></span>
			</div>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#subnavbar"
				aria-expanded="false" aria-controls="navbar">
				<span class="nav-trigger"><span></span></span> <span class="sr-only">Select</span>
			</button>
		</div>
	</div>
	<div class="col-col-lg-12">
		<nav class="navbar">
			<div class="pagetitleimg desktoppic">
				<img class="miniCircle" alt="">
			</div>
			<div id="subnavbar" class="collapse navbar-collapse sp-menu">
				<ul class="nav navbar-nav">
					<li><a href="#sec2">Habitat & Human <br />Footprint
							Associations
					</a></li>
					<li><a href="#sec3">Impacts of Human <br />Footprint
					</a></li>
					<li><a href="#sec4">Predicted Relative <br />Abundance
					</a></li>
					<li><a href="#sec5">Impacts of <br />Climate Change
					</a></li>
					<li><a href="#sec6">Other Issues</a></li>
					<li><a href="#sec7">References</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->

		</nav>
	</div>
</div>
<div class="row sec-title" id="sec2">
	<div class="container">
		<h2>Habitat & Human Footprint Associations</h2>
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-lg-12">
				<div class="auto WebHabitat"></div>
				<div class="togglelink">
					<h4>Methods</h4>
					<div class="togglesec">
						<cms:area name="method-hhfa" />

					</div>
				</div>
				<div class="Forested-Region">
					<div class="head-s">
						<img src="assets/Forested-Region.png" alt="" />
						<h3>Species-habitat Associations in the Forested Region</h3>
					</div>
				</div>
				<div class="mapImgSec ForestedRegion-map">
					<a href="javascript:void(0)" class="zoom-graph"
						data-open-id="graph"></a> <img class="auto" src="" alt=""
						class="mapImg" />
				</div>
				<br />
				<br />
				<div class="auto WebForestedRegionGraphText"></div>


			</div>

		</div>
	</div>
</div>
<div class="row sec paririe-sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-lg-12">
				<div class="Prairie-Region">
					<div class="head-s">
						<img src="assets/Prairie-Region.png" alt="" />
						<h3>Species-habitat Associations in the Prairie Region</h3>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<h4>Non-Treed Sites in the Prairie Region</h4>
				<img class="auto" src="" alt="Non-Treed Sites in the Prairie Region" />
			</div>
			<div class="col-md-6">
				<h4>Treed Sites in the Prairie Region</h4>
				<img class="auto" src="" alt="Treed Sites in the Prairie Region" />
			</div>
			<div class="col-lg-12">
				<div class="auto WebNonTreeGraphText"></div>
				<div class="auto WebTreeGraphText"></div>
			</div>

		</div>
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-md-12">
				<h3>Relationship to Linear Footprint</h3>
				<br />
				<br />
			</div>
			<div class="col-md-6">
				<div class="Forested-Region">
					<div class="head-s">
						<img src="" alt="" />
						<h4>Relationship to Linear Footprint in the Forest Region</h4>
					</div>
				</div>
				<img class="auto" src="" alt="" /><br />
				<br />
			</div>
			<div class="col-md-6">
				<div class="Prairie-Region">
					<div class="head-s">
						<img src="" alt="" />
						<h4>Relationship to Linear Footprint in the Prairie Region</h4>
					</div>
				</div>
				<img class="auto" src="" alt="" /><br />
				<br />
			</div>
			<div class="col-lg-12">


				<div class="auto WebFLinearHFGraphText"></div>
				<div class="auto WebPLinearHFGraphText"></div>
				<div class="btnr">
					<a href="#" class="downloaddata-btn">Download Data</a>
				</div>
			</div>

		</div>
	</div>
</div>
<div class="row sec-title" id="sec3">
	<div class="container">
		<h2>Impacts of Human Footprint</h2>
	</div>
</div>
<div class="row sec">
	<div class="container">
		<div class="row wrap">
			<div class="col-md-12">
				<div class="auto WebHFEffect"></div>
				<div class="togglelink">
					<h4>Methods</h4>
					<div class="togglesec">
						<cms:area name="method-ifh" />
					</div>
				</div>
				<div class="col-md-6 Forested-Region">
					<div class="head-s">
						<img src="assets/Forested-Region.png" alt="" />
						<h4>Human Footprint Effects in the Forested Region</h4>
					</div>
					<img class="auto" src=""
						alt="Human Footprint Effects in the Forested Region" /><br />
					<div class="auto WebHFForestRegionGraph"></div>
				</div>
				<div class="col-md-6 Prairie-Region">
					<div class="head-s">
						<img class="auto" src="" alt="" />
						<h4>Human Footprint Effects in the Prairie Region</h4>
					</div>
					<img class="auto" src=""
						alt="Human Footprint Effects in the Prairie Region" /><br />
					<div class="auto WebPrarieRegionGraphText"></div>
				</div>
				<div class="btnr">
					<a href="#" class="downloaddata-btn">Download Data</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row sec-title" id="sec4">
		<div class="container">
			<h2>Predicted Relative Abundance</h2>
		</div>
	</div>
	<div class="row sec">
		<div class="container">
			<div class="wrap">
				<div class="auto WebRange"></div>
				<div class="togglelink">
					<h4>Methods</h4>
					<div class="togglesec">
						<cms:area name="method-rra" />
					</div>
				</div>
				<div class="row relation_sec">
					<div class="col-md-4">
						<h4>Reference Conditions</h4>
						<div class="mapImgSec map-ref">
							<a href="javascript:void(0)" class="open-modalwindow"
								data-open-id="map-2"></a>
							<div class="album">
								<a rel="map-2" class="facybox" href="assets/Reference-map.png"><img
									src="assets/Reference-map.png" alt="" /></a>
							</div>
							<img src="assets/Reference-map.png" alt="" class="mapImg" />
						</div>

						<ul>
							<li>The reference condition shows the predicted relative
								abundance of the <span class="CommonName">this species</span>
								after all human footprint had been backfilled based on native
								vegetation in the surrounding area.
							</li>
						</ul>
					</div>
					<div class="col-md-4">
						<h4>Current Conditions</h4>
						<div class="mapImgSec map-current">
							<a href="javascript:void(0)" class="open-modalwindow"
								data-open-id="map-3"></a>
							<div class="album">
								<a rel="map-3" class="facybox" href="assets/Current-map.png"><img
									src="assets/Current-map.png" alt="" /></a>
							</div>
							<img src="assets/Current-map.png" alt="" class="mapImg" />
						</div>

						<ul>
							<li>The current condition is the predicted relative
								abundance of the <span class="CommonName">this species</span>
								taking current human footprint (circa 2012) into account.
							</li>
						</ul>
					</div>
					<div class="col-md-4">
						<h4 class="colored">Difference Conditions</h4>
						<div class="mapImgSec">
							<a href="javascript:void(0)" class="open-modalwindow"
								data-open-id="map-4"></a>
							<div class="album">
								<a rel="map-4" class="facybox" href="assets/Difference-map.png"><img
									src="assets/Difference-map.png" alt="" /></a>
							</div>
							<img src="assets/Difference-map.png" alt="" class="mapImg" />
						</div>

						<div class="auto WebDifferenceMapText"></div>
					</div>
				</div>
				<div class="btnr">
					<a href="#" class="mapPortal-btn mrgr">View Map Portal</a> <a
						href="#" class="downloaddata-btn">Download Data</a>
				</div>

			</div>
		</div>
	</div>
	<div class="row sec-title" id="sec5">
		<div class="container">
			<h2>Impacts of Climate Change</h2>
			<cms:area name="method-icc" />
		</div>
	</div>
	<div class="row sec">
		<div class="container">
			<div class="row wrap">
				<div class="col-md-12">
					<h4 class="nomrg">Results Coming Soon</h4>
				</div>
			</div>
		</div>
	</div>
	<div class="row sec-title" id="sec6">
		<div class="container">
			<h2>Other Issues</h2>
		</div>
	</div>
	<div class="row sec">
		<div class="container">
			<div class="row wrap">
				<div class="col-md-12">
					<h4 class="nomrg">Not Available</h4>
				</div>
			</div>
		</div>
	</div>
	<div class="row sec-title" id="sec7">
		<div class="container">
			<h2>References</h2>
		</div>
	</div>
	<div class="row sec">
		<div class="container">
			<div class="row wrap">
				<div class="col-md-6">
					<h3>References</h3>
					<div class="auto  WebReference"></div>
					<h3>Photo & Credits</h3>
					<p>Photos: TBD</p>
				</div>
				<div class="col-md-6">
					<h3>Data Sources</h3>
					<p>Information from ABMI bird point counts was combined with
						information from other organizations and individuals:</p>
					<ul>
						<li>Environment Canada (North American Breeding Bird Survey
							and Joint Oil Sands Monitoring programs)</li>
						<li>Ecological Monitoring Committee for the Lower Athabasca
							(EMCLA)</li>
						<li>Dr. Erin Bayne (University of Alberta)</li>
					</ul>
					<h3>Recommended Citation</h3>
					<p>
						ABMI (2016). <span class="CommonName"></span> <span
							class="ScientificName"></span>. ABMI Species Website, version 3.1
						(2016-01-07). URL: <a
							href="http://sc-dev.abmi.ca/development/pages/species/birds/PileatedWoodpecker.html"
							target="_blank">http://sc-dev.abmi.ca/development/pages/species/birds/PileatedWoodpecker.html</a>.
					</p>
				</div>
			</div>
		</div>
	</div>


</div>
<!--  <link rel="stylesheet" href="https://cdn.jsdelivr.net/leaflet/1.0.0-rc.1/leaflet.css" />
  <script src="https://cdn.jsdelivr.net/leaflet/1.0.0-rc.1/leaflet-src.js"></script>

  <!-- Load Esri Leaflet from CDN --
  <script src="https://cdn.jsdelivr.net/leaflet.esri/2.0.0/esri-leaflet.js"></script>
   -->
<link rel="stylesheet"
	href="https://js.arcgis.com/3.16/dijit/themes/claro/claro.css">
<link rel="stylesheet"
	href="https://js.arcgis.com/3.16/esri/css/esri.css">


<script defer src="https://js.arcgis.com/3.16/"></script>
