<%@ include file="../../includes/taglibs.jsp"%>
<c:set var="speciesGroup">${param.speciesGroup}</c:set>

<script>
var gSpeciesGroup = "${speciesGroup}";
var gDetailedUrlParam = "${content.speciesDetailUrl}";
</script>

<div class="row secondarybar">
	<div class="container">
		<div class="row col-md-7">
			<div class="totalNumbers">
				<h4>
					${speciesGroup} <span id="sppCount"></span>
				</h4>
			</div>
			<div class="slectGroup">Choose Species Group</div>
		</div>

		<div class="search hidden-sm hidden-xs">
			<div class="push-right">
				<input type="text" placeholder="Search" id="searchSpecies" name="q"> <input
					type="submit" class="btn" id="searchSpeciesBtn"
					value="">
			</div>
		</div>
	</div>
</div>

<!--Choose Species Group-->
<div class="row species-groupList">
	<div class="container">
		<div class="row">
			<c:if test="${speciesGroup != 'Birds'}">
				<div class="col-sm-2 col-xs-4">
					<a href="?speciesGroup=Birds" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/birds-sm.jpg" alt="mamals" />
						</div> <span>Birds</span>
					</a>
				</div>
			</c:if>

			<c:if test="${speciesGroup != 'Mammals'}">
				<div class="col-sm-2 col-xs-4">
					<a href="?speciesGroup=Mammals" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/mammals-sm.jpg" alt="mamals" />
						</div> <span>Mammals</span>
					</a>
				</div>
			</c:if>

			<c:if test="${speciesGroup != 'Vascular Plants'}">
				<div class="col-sm-2 col-xs-4">
					<a href="?speciesGroup=Vascular Plants" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/Vascular-Plants-sm.jpg"
								alt="Vascular Plants" />
						</div> <span>Vascular Plants</span>
					</a>
				</div>
			</c:if>
			<c:if test="${speciesGroup != 'Soil Mites'}">
				<div class="col-sm-2 col-xs-4">
					<a href="?speciesGroup=Soil Mites" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/soil-mites-sm.jpg" alt="Soil Mites" />
						</div> <span>Soil Mites</span>
					</a>
				</div>
			</c:if>
			<c:if test="${speciesGroup != 'Lichens'}">
				<div class="col-sm-2 col-xs-4">
					<a href="?speciesGroup=Lichens" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/Lichens.jpg" alt="Lichens" />
						</div> <span>Lichens</span>
					</a>
				</div>
			</c:if>
			<c:if test="${speciesGroup != 'Aquatic Invertabrates'}">
				<div class="col-sm-2 col-xs-4">
					<a href="?speciesGroup=Aquatic Invertabrates" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/Aquatic-Invertabrates-sm.jpg"
								alt="Aquatic Invertabrates" />
						</div> <span>Aquatic Invertabrates</span>
					</a>
				</div>
			</c:if>
			<c:if test="${speciesGroup != 'Bryophytes'}">
				<div class="col-sm-2 col-xs-4">
					<a href="?speciesGroup=Bryophytes" class="species-item">
						<div class="speciesImg-block">
							<img src="/docroot/da/assets/Bryophytes-sm.jpg" alt="Bryophytes" />
						</div> <span>Bryophytes</span>
					</a>
				</div>
			</c:if>

		</div>
	</div>
</div>

<!--Data Filter/ Sort-->
<div class="row data-section">
	<div class="container">
		<div class="data-summaries">
			
			<p><span class="full">Full</span><span class="forested">Forested Region</span><span class="prairie">Prairie Region</span><span class="basic">Basic</span></p>
		    
		</div>
		<c:if test="${speciesGroup == 'Vascular Plants'}">
			<div id="filters" class="button-group">
				<button class="button is-checked" id="all-filter" data-filter="*">ALL</button>
				<button class="button" id="terrestrial-filter" data-filter=".terrestrial">TERRESTRIAL</button>
				<button class="button" id="aquatic-filter" data-filter=".aquatic">AQUATIC</button>
			</div>
		</c:if>

		<div class="dataWrapper">
			<div class="data-block" id="data-block">

				<table id="species-data">
					<thead>
						<tr>
							<th></th>
							<th>Summary</th>
							<th>Common Name</th>
							<th>Scientific Name</th>
							<c:if test="${speciesGroup == 'Vascular Plants'}">
								<th>Taxa Name</th>
							</c:if>
						</tr>
					</thead>
					<tbody id="dataBody" ${speciesGroup == 'Vascular Plants'? "class='isotope'":""}>
					</tbody>
				</table>
			</div>
		
		
		</div>
	</div>
</div>
