<%@ include file="../../includes/taglibs.jsp"%>
<div class="row">
	<div class="col-xs-12">
		<div class="news-sec">
			<div class="box-text">
				<div class="news-link">
					<h3>${content.header}</h3>
					<a class="line-btn btn-white"
						${fn:startsWith(content.listpath, "http")?"target=_blank":""}
						href="${content.listpath}">${content.buttonText}</a>
                  </div>

                                <div class="news-desc">
                                  
                                    <ul class="bxslider" >
                                 	<c:forEach items="${newlist}" var="row" varStatus="rowStatus">
									
                                   
                                    <li>
									  	<a href="#">
										  <h3><span class="newspost-title">${row.title}</span><span class="newspost-date">${row.date}</span></h3>
										</a>
										${row.description }
									  </li>
									 </c:forEach> 
									  </ul>
							    </div>

                                <div class="next-newspost">
                                    <span id="slider-next"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
             