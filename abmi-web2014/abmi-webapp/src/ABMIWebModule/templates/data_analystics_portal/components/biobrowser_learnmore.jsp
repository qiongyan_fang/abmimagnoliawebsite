<%@ include file="../../includes/taglibs.jsp"%>

   <div class="col-md-6 sec">
<h2>${content.headerText}</h2>
<p>${cmsfn:decode(content).description}</p>

<a class="line-btn blue-btn"
	${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""}
	href="${content.buttonUrl}">${content.buttonText}</a>
	</div>