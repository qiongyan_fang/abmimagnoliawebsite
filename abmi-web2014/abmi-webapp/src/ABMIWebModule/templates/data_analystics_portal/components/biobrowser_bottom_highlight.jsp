<%@ include file="../../includes/taglibs.jsp"%>



<c:set var="imageContentMap"
	value="${cmsfn:content(content.backgroundImageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />

<c:set var="iconContentMap"
	value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="iconUrl"
	value="${cmsfn:link(cmsfn:asContentMap(iconContentMap))}" />

<div class="row mapping-sec" style="background-image:url(${imageUrl}); background-repeat: no-repeat;
    background-size: cover;">
	<div class="container">
		<img alt="Mapping Portal" src="${iconUrl}" />
		<h2 class="text-center">${cmsfn:decode(content).header}</h2>
		<a class="line-btn btn-white"
			${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""}
			href="${content.buttonUrl}">${content.buttonText}</a>
	</div>
</div>