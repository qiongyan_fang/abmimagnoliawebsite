<%@ include file="../../includes/taglibs.jsp"%>

<c:forEach var="row" items="${banner}">

	<c:if test="${row.speciesGroup == param.speciesGroup}">
		<c:set var="imagerowMap"
			value="${cmsfn:content(row.imageurl,'dam')}" />
		<c:set var="imageUrl"
			value="${cmsfn:link(cmsfn:asContentMap(imagerowMap))}" />


		<div class="row browser-banner"
			style="background-image:url(${imageUrl});background-repeat: no-repeat;
    background-size: cover;">
			<div class="banner-caption">
				<div class="container">
					<div class="textwrap">
						<h1>${row.header}</h1>
						<h3>${row.header2 }</h3>
						<p>${row.description }</p>
					</div>
				</div>
			</div>
		</div>
	</c:if>
</c:forEach>