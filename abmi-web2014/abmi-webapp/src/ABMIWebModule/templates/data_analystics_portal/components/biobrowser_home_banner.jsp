<%@ include file="../../includes/taglibs.jsp"%>

<c:set var="imageContentMap"
	value="${cmsfn:content(content.backgroundImageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />


<c:set var="iconContentMap"
	value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="iconUrl"
	value="${cmsfn:link(cmsfn:asContentMap(iconContentMap))}" />




<div class="row bio-browser-banner" style="background-image:url(${imageUrl});background-repeat: no-repeat;
    background-size: cover;">
	<div class="bio-browser-img" style="background-image:url(${iconUrl};);background-repeat: no-repeat scroll right 20% top / 730px auto">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<c:if test="${not empty iconUrl}">
				<img src="${iconUrl}" alt="${content.header}" class="bio-browser-graphic">
				</c:if>
					<h1>${content.header}</h1>
					<p>${cmsfn:decode(content).description}</p>
					${content.embed }
				</div>
			</div>
		</div>
	</div>
</div>