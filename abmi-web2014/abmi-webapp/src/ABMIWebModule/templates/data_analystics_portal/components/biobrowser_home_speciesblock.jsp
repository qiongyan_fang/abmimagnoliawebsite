<%@ include file="../../includes/taglibs.jsp"%>

<c:set var="imageContentMap"
	value="${cmsfn:content(content.backgroundImageUrl,'dam')}" />
<c:set var="imageUrl"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />

<div style="background-image:url(${imageUrl});background-repeat: no-repeat;
    background-size: cover; width:100%;height:100%;">
	<a ${fn:startsWith(listUrl, "http")?"target=_blank":""}
		href="${listUrl}?groupId=${content.speciesGroup}">
		<div class="overlay-box">
			<div class="text-wrapper">
				<div class="tab-cell">
					<h3>${content.headerText}</h3>
					<h4>${content.headerText2}</h4>
					<span class="line-btn btn-white">${content.buttonText}</span>
				</div>
			</div>
		</div>
	</a>
</div>