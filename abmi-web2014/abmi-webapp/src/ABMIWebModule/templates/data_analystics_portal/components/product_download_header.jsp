<%@ include file="../../includes/taglibs.jsp"%>

<c:set var="imageContentMap"
	value="${cmsfn:content(content.image,'dam')}" />
<c:set var="image"
	value="${cmsfn:link(cmsfn:asContentMap(imageContentMap))}" />

<c:set var="title1"
	value="${state.mainContentNode.getProperty('title').getString()}" />


<div class="col-sm-6 single-data">
	<h3>${empty content.customHeader?title1:content.customHeader}</h3>
	<div class="post-data">
		<span class="post-pdf white"></span> <span class="post-zip white"></span>
		<span>${content.productCount} DATA SETS</span>
	</div>
	<p>${content.description }</p>
</div>
<div class="col-sm-6 nopadding single-data-img">
	<img src="${image}" alt="" />
</div>
<div class="col-xs-12 data-ftpData">
	<img src="/docroot/da/assets/${content.fileicon}-white-icon.png"
		alt="Download using ${content.fileicon}">
	<div class="ftp-details">${cmsfn:decode(content).downloadDescription}</div>
</div>

<div class="col-xs-12">
	<h4>
		<cms:area name="h4title" />
	</h4>

	<div>
		<cms:area name="richtext" />
	</div>
	<div class="databy">
		<cms:area name="downloadproducts" />
	</div>
</div>