<%@ include file="../../includes/taglibs.jsp"%>
<%@ include file="../includes/da_header.jsp"%>

<body class="container-fluid data-analytics">
	<%@ include file="../includes/da_top_menu.jsp"%>
	<div class="row page-content">
		<div class="container">
			<div class="side-top-nav col-xs-12 col-lg-3">
				<div class="widget searchbox">
					<input type="text" id="" name="" class="sidemenuSearch"
						placeholder="Search...">
					<button onclick="" class="sidemenuSearchBtn"></button>
				</div>
			</div>
			<div class="clear-to-lg"></div>
			<!--  ===================  MAIN COLUMN =============== -->
			<div class="col-lg-9 main single-datapage">
			<cms:area name="leftColumnArea" />
			</div>
		
			<div class="col-lg-3 col-md-12 col-sm-12 aside">
			
					<cms:area name="rightColumnArea" />
			

			</div>
		</div>
	</div>

	<%@ include file="../includes/da_footer.jsp"%>
</body>
</html>