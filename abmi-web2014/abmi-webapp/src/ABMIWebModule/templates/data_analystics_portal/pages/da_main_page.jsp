<%@ include file="../../includes/taglibs.jsp"%>
<%@ include file="../includes/da_header.jsp"%>

<body class="container-fluid data-analytics">
	<%@ include file="../../includes/menu.jsp"%>
	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row portal-main">

		<cms:area name="introductionTopArea" />
		<div class="col-xs-12 flt-none">
			<div class="row breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h3>Explore Our Data</h3>
							<a class="feedback-link" href="#">Feedback?</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row breadcrumb-triangle">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="triangle hidden-xs hidden-sm"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="portal-quicksec">
			<div class="container">
				<cms:area name="rowArea" />
				<cms:area name="whatnewlistArea" />
			</div>
		</div>
	</div>

	
	<cms:area name="bottomLearnMoreRowArea" />
	
	<%@ include file="../includes/da_footer.jsp"%>
</body>
</html>