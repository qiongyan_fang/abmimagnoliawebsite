<div class="row data-nav">
		<div class="logo mainNavLink">
            <a href="/home/data-analytics.html"></a>
        </div>
        <div class="backtoHome">
            <a href="index.html"></a>
        </div>
        <div class="data-nav-links">
	        <a class="datalink" href="/home/data-analytics/da-top/da-product-overview">
		        <img alt="Data Download" src="/docroot/da/assets/data-download-icon.png"/><span>Data Download</span>
		    </a>
		    <a class="datalink" href="/home/data-analytics/biobrowser-home">
		        <img alt="Biodiversity Browser" src="/docroot/da/assets/bio-browser-icon.png"/><span>Biodiversity Browser</span>
		    </a>
		    <a class="datalink" href="#">
		        <img alt="Mapping Portal" src="/docroot/da/assets/mapping-portal-icon.png"/><span>Mapping Portal</span>
		    </a>
		    <a class="datalink" href="#">
		        <img alt="Custom Reporting" src="/docroot/da/assets/custom-reporting-icon.png"/><span>Custom Reporting</span>
		    </a>
	        <a href="#" class="feedback-link">Feedback?</a>
        </div>
	</div>