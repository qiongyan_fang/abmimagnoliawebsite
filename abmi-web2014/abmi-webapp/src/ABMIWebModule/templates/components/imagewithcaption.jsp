<%@ include file="../includes/taglibs.jsp"%>
<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />

<div class="wp-caption align${content.imageposition}"
	style="width: ${content.boxwdith}px">
	
	<c:if test="${empty content.largeimageUrl and not content.zoomable}">
		<img alt="" src="${cmsfn:link(cmsfn:asContentMap(contentMap))}">
		
	</c:if>
	<c:if test="${not empty content.largeimageUrl or content.zoomable}">
		<c:set var="largeContentMap"
			value="${cmsfn:content(content.largeimageUrl,'dam')}" />
		<a href="${not empty content.largeimageUrl?cmsfn:link(cmsfn:asContentMap(largeContentMap)):cmsfn:link(cmsfn:asContentMap(contentMap))}"
			class="zoom fullzoom" style="width:100%;"> <img
			alt="" src="${cmsfn:link(cmsfn:asContentMap(contentMap))}">
		</a>


	</c:if>
	<p class="wp-caption-text">${content.caption}</p>
</div>


