<%@ include file="../includes/taglibs.jsp"%>


<div class="jumbotron jumbotron${videonum}">
<video loop="" autoplay=""  class="hidden-xs fullWidth html5video" poster="/docroot/assets/Video-0${videonum}.jpg" >
				  <source type="video/mp4" src="/docroot/assets/Video-0${videonum}.mp4"></source>
				  <source type="video/ogg" src="/docroot/assets/Video-0${videonum}.ogv"></source>
				  <source type="video/webm" src="/docroot/assets/Video-0${videonum}.webm"></source>
				</video>

			<div class="overlay">
				<h1 class="text-center">${headertext.header}</h1>
				<h1 class="text-center sub-heading">${headertext.subheader}</h1>
			</div>

		</div>		