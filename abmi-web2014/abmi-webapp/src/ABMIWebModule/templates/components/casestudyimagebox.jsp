<%@ include file="../includes/taglibs.jsp"%>
<c:set var="topImage" value="${cmsfn:content(content.imageUrl,'dam')}" />
<script async="" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5425da0c508c3835" type="text/javascript"></script>

<div class="row dbm">
	<div class="section-photobox black-bg">
		<div class="col-xs-12 col-sm-6 content">
			<h3>${content.header}</h3>
			<p>${content.description}</p>
		</div>

		<div class="col-xs-12 col-sm-6 image">
			<img alt="" src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 arrowcontainer">
			<div class="photobox postmeta">
			
				<div class="meta-date">by ${content.author} on <fmt:formatDate value="${content.publishdate.getTime() }"/> | INDUSTRY: ${content.industry}</div>
<div class="photo-box-share addthis_sharing_toolbox" data-url="http://${pageContext.request.serverName}${pageContext.request.contextPath}${state.originalBrowserURI}
"></div>

				
			</div>
			
		</div>
	</div>
</div>