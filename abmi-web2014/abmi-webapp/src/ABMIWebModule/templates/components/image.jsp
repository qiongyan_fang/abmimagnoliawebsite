<%@ include file="../includes/taglibs.jsp"%>

<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />

<c:if test="${empty content.url}" >
<img alt="" src="${cmsfn:link(cmsfn:asContentMap(contentMap))}">
</c:if>
<c:if test="${not empty content.url}">
<a title="" href="${content.url}">
<img alt="" src="${cmsfn:link(cmsfn:asContentMap(contentMap))}">
</a>
</c:if>

						