<%@ include file="../includes/taglibs.jsp"%>

<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
	<h3>${content.header}</h3>
	<ul>
		<c:forEach var="row" items="${menu}">
			<c:set var="contentMap"
				value="${cmsfn:content(row.rootpath,'website')}" />
			<li><a 
				href="${cmsfn:link(cmsfn:asContentMap(contentMap))}">${empty
					row.linktext?cmsfn:page(cmsfn:asContentMap(contentMap)).title:row.linktext}
			</a></li>
		</c:forEach>

	</ul>
</div>

