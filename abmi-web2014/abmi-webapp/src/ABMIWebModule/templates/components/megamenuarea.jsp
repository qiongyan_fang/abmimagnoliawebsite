
<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="menuclass" value="${fn:replace(content.menutitle,' ', '')}"/>

<style>
@media only screen and (min-width:1200px) { .navbar-right ul.${menuclass} {left:-${content.menushift}px; right:0px;} }
</style>
<c:set var="mode" value="${cmsfn:isEditMode() and state.handle eq '/home'
?'indeed':''}" />
<c:set var="contentMap" value="${cmsfn:content(content.imageurl,'dam')}" />
<li><a href="#" class="dropdown-toggle" data-toggle="dropdown${mode}">${content.menutitle}</a>
	<ul class="dropdown-menu${mode} ${menuclass}" style="width:${content.menuwidth}px; ">
		<li>
			<div class="visible-lg">
				<img alt=""  src="${cmsfn:link(cmsfn:asContentMap(contentMap))}" width="210" height="190">
			</div>
 <cms:area name="SubMenuArea" />
	</ul></li>




