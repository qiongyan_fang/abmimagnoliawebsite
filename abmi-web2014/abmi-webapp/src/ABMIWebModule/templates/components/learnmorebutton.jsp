<%@ include file="../includes/taglibs.jsp"%>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

 <a class="button btn-learn" href="${content.url}" ${fn:startsWith(content.url, "http")?"target=_blank":""}>${content.buttontext}</a>