<%@ include file="../includes/taglibs.jsp"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="id">${fn:replace(content.glossary,' ','_')}</c:set>
<li><a data-modal="${id}" class="md-trigger glossarycite">${content.glossary}</a></li>
