 <%@ include file="../includes/taglibs.jsp" %>
 <c:set var="contentMap" value="${cmsfn:content(content.twittericon,'dam')}" />
<% response.setHeader("Cache-Control", "no-cache"); %>
 <div class="tile half social">
<a title="twitter" href="${empty twitterUrl?'https://twitter.com/ABbiodiversity':'https://twitter.com/ABbiodiversity' }" >
						
						<img class="icon" alt="Twitter" src="${cmsfn:link(cmsfn:asContentMap(contentMap))}">
						<h4>${content.twittername}</h4>
						<p>${twitterItem}</p>
						</a>
					</div>