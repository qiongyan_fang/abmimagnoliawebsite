<%@ include file="../includes/taglibs.jsp"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="bod board-of-directors">
<div class="main">
				 <c:forEach items="${members}" var="member">
            	 <c:set var="image" value="${cmsfn:content(member.photo,'dam')}" />
            	 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<img src="${cmsfn:link(cmsfn:asContentMap(image))}" alt="${member.name}">
					<div class="col-lg-12 ${fn:contains(member.position, 'Chair')?"d":""}green">
						<h4>${member.personname}</h4>
						<h5>${member.position}</h5>
						<p>${member.otherposition}</p>
						<a class="btn btn-learn" href="${pageContext.request.contextPath}${member.path}">READ BIO</a>
					</div>
				</div>
            </c:forEach>
</div>
</div>