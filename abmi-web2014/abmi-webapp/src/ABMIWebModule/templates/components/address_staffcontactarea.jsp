<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="blockid">${state.getCurrentContentNode().getUUID()}</c:set>
<c:set var="minheight"> ${content.minheight*31}</c:set>
<style>
@media only screen and (min-width:768px){
.a${blockid} {min-height:${minheight}px!important;}
}
</style>


<li><input type="button" class="collapsed"
	value="${content.centername}" data-target="#${blockid}staff"
	data-toggle="collapse">

	<div class="collapse" id="${blockid}staff" style="height: auto;">
		<div class="row">
			<cms:area name="staffarea" />
		</div>
	</div></li>



