
<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="uri" value="${empty currentLink?state.handle:currentLink}" />
<ul class="navigation">
	<c:forEach items="${subpage}" var="row">
		<c:set var="classname" value="" />
		<c:if test="${row.path eq uri}">
			<c:set var="classname" value="parent  parent-active" />
		</c:if>


		<c:if test="${empty row.pages}">
			<li class="large-menu ${row.classname}">
			<c:if test="${row.path eq uri}">
			<a class="${classname}"
				href="javascript:;">${row.title}<c:if test="${row.classname eq 'species-habitat-data'}"><i class="leftarrow"></i></c:if></a>
			</c:if>
			<c:if test="${row.path ne uri}">
			<a class=""
				href="${pageContext.request.contextPath}${row.path}.html?scroll=true">${row.title}<c:if test="${row.classname eq 'species-habitat-data'}"><i class="leftarrow"></i></c:if></a>
			
			</c:if>
			
			</li>
		</c:if>
		<!--  show pages have subpages -->
		<c:if test="${not empty row.pages}">
			<li class="parent-gis-data ${classname}"><a class="${classname}"
				href="javascript:;">${row.title} <i class="arrow"></i></a>
				<ul class="gis-data ${fn:startsWith(uri, row.path)?'expanded':'expanded' }">
					<c:forEach items="${row.pages}" var="subrow">
						<c:set var="classname2" value="" />
						<c:if test="${subrow.path eq uri}">
							<c:set var="classname2" value="active" />
							</c:if>

						<li><a  class="${classname2}"
							href="${pageContext.request.contextPath}${subrow.path}.html?scroll=true">${subrow.title}
						</a></li>

				<c:if test="${not empty subrow.children}">
<ul class="subsubmenu">
				<c:forEach items="${subrow.children}" var="subsubrow">
						<c:set var="classname2" value="" />
						<c:if test="${subsubrow.path eq uri}">
							<c:set var="classname2" value="active" />
							</c:if>

						<li><a  class="${classname2}"
							href="${pageContext.request.contextPath}${subsubrow.path}.html?scroll=true">${subsubrow.title}
						</a></li>

			
					</c:forEach>
				</ul></c:if>
					</c:forEach>
						
				</ul></li>
				
			
				<li class="parent-gis-data-spacer">
							&nbsp;
						</li>
		</c:if>


	</c:forEach>

</ul>