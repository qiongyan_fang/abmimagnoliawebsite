<%@ include file="../includes/taglibs.jsp"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="blockid" value="${fn:replace(content.centername,' ', '')}" />
<ul class="extradocs">
	<li><input type="button" class="collapsed"
		value="${content.centername}" data-target="#${blockid}staff"
		data-toggle="collapse">

		<div class="collapse" id="${blockid}staff" style="height: auto;">

			<cms:area name="staffarea" />

		</div>
</ul>


<hr style="clear:both;"/>
