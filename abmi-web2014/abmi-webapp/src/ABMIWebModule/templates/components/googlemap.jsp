<%--@elvariable id="content" type="info.magnolia.jcr.util.ContentMap"--%>
<%@ include file="../includes/taglibs.jsp"%>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

<c:choose>
<c:when test="${content.boxwidth == 'narrow'}">
<c:set var="divclass" value="col-lg-8 col-md-8" />
</c:when>

<c:otherwise>
<c:set var="divclass" value="" />
</c:otherwise>
</c:choose>
<div class="${divclass }">
<div id="gmap-canvas" data-gmaplon="${content.longitude}" data-gmaplat="${content.lat}" data-gmaptitle="${content.mapname}">

</div>
</div>

