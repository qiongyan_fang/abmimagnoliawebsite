<%@ include file="../includes/taglibs.jsp"%>

<c:choose>
<c:when
	test="${not empty content.backgroundcolor and content.backgroundcolor ne '  ' and content.backgroundcolor ne 'nocolor'
	and content.backgroundcolor ne 'transparent'}">
	<div class="${content.nopadding ==true ? "richtext":"richtext col-xs-12 quote" } ${content.backgroundcolor}">${cmsfn:decode(content).body}</div>
</c:when>
<c:otherwise>
<div class="richtext" > ${cmsfn:decode(content).body}</div>
</c:otherwise>
</c:choose>

<c:if test="${content.clearfloat}">
<div class="clearboth"></div>
</c:if>