 <%@ include file="../includes/taglibs.jsp" %>
 <hr>
<h4 class="dbm">${content.headertext}</h4>
<h4 class="contact question-contact">${content.staffname}</h4>
<c:if test="${not empty content.stafftitle}">
<p>${content.stafftitle}<br />
</c:if>
${content.phone}<br />
<c:if test="${not empty content.address}">
${content.address}<br />
</c:if>
<c:if test="${not empty content.email}">
<a href="mailto:${content.email}">${content.email}</a>
</c:if>
</p>
