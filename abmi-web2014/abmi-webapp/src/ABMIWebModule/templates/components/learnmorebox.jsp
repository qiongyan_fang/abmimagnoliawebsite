<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />

<div class="tile learn-more">
						<a ${fn:startsWith(content.url, "http")?"target=_blank":""} href="${content.url}">
							<img  class="pull-${content.imageposition}  hidden-xs hard35_7p" alt="" src="${cmsfn:link(cmsfn:asContentMap(contentMap))}">
							<div class="column pull-${content.imageposition}">
								<h4>${content.headerline}</h4>
								<p>${content.content}</p>
								<div class="btn-learn btn-learn-green">Learn More</div>
							</div>
						</a>
					</div>