<%@ include file="../includes/taglibs.jsp"%>
<c:choose>
    <c:when test="${not empty content.url}">
    <c:set var="urlNode" value="${cmsfn:content(content.url,'dam')}" />
     <li class="clearboth"> <a class="pdf" href="${cmsfn:link(cmsfn:asContentMap(urlNode))}">${content.text}</a>
     
</li>
    </c:when>
      <c:when test="${not empty content.externalurl}">
     <li class="clearboth">  <a class="pdf" href="${content.externalurl}" target=_blank>${content.text} </a></li>
    </c:when>
    <c:otherwise>
      <li class="clearboth"> ${content.text}</li>
    </c:otherwise>
</c:choose>