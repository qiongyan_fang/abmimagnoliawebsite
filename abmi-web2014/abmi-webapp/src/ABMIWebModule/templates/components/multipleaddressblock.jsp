<%@ include file="../includes/taglibs.jsp" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="blockid" value="${fn:replace(content.centername,' ', '')}"/>
<ul class="extradocs">
	<li><input type="button" class="collapsed"
		value="${content.centername}" data-target="#${blockid}off"
		data-toggle="collapse">
		<div class="collapse"
			id="${blockid}off" style="height: auto;">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<h5>Mailing Address</h5>
				<p>${content.mailingaddress}
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<h5>Street Address</h5>
				<p>${content.streetaddress}
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<p>
					<c:if test="${not empty content.phone}">
						<br>
						<b>Phone</b>:${content.phone}
      </c:if>
					<c:if test="${not empty content.fax}">
						<br>
						<b>Fax</b>:${content.fax}
      </c:if>
					<c:if test="${not empty content.email}">
						<br>${content.email}
      </c:if>
			</div>
		</div></li>
</ul>
<div style="clear:both"></div>