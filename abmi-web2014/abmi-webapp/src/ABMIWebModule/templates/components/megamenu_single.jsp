
<%@ include file="../includes/taglibs.jsp"%>
<c:set var="mode" value="${cmsfn:isEditMode()?'color:black;':''}" />
<ul class="list-unstyled">

	<c:forEach var="row" items="${menu}">
		<c:set var="contentMap"
			value="${fn:startsWith(row.rootpath, 'http')?'':cmsfn:content(row.rootpath,'website')}" />
		<c:if test="${not empty contentMap}">
			<li><a style="${mode}"
				href="${cmsfn:link(cmsfn:asContentMap(contentMap))}">${empty
					row.titletext?cmsfn:page(cmsfn:asContentMap(contentMap)).title:row.titletext}
			</a>
			</li>
		</c:if>
		<c:if test="${empty contentMap and not empty row.titletext}">
			<li><a style="${mode}"
				href="${row.rootpath}" target='${fn:startsWith(row.rootpath, "http")?"target=_blank":""}'>${empty
					row.titletext?cmsfn:page(cmsfn:asContentMap(contentMap)).title:row.titletext}
			</a>
			</li>
		</c:if>
	</c:forEach>
</ul>
<c:if test="${cmsfn:isEditMode()}">
	<hr>
</c:if>
