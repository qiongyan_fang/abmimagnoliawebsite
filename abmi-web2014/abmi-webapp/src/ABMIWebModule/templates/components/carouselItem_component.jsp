<%@ include file="../includes/taglibs.jsp"%>

<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />


  <c:if test="${content.itemIndex == 0}">
	<c:set var="activeClass" value="active" />
</c:if>


<div class="item ${activeClass}">

	<a title="${content.headerline}"
		href="${content.url}"> <img
		 class="pull-${content.imageposition} hidden-xs hard35_7p"
		alt="${content.headerline1}"
		src="${cmsfn:link(cmsfn:asContentMap(contentMap))}">
		<div class="column pull-left">
			<h4>${content.headerline}</h4>
			<p class="timestamp">${content.date}</p>
			
			<p><c:if test="${not empty content.subheaderline}"><strong>${content.subheaderline}</strong><br></c:if>${content.content}</p>
		</div>
	</a>
</div>






