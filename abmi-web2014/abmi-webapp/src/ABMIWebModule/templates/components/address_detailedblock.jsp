<%@ include file="../includes/taglibs.jsp"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
	<h5>${content.type}</h5>
	<p class="notebelowheader">(${content.function})
	<br>
<br>
		${cmsfn:decode(content).address} <br>${content.city}, ${content.province} <br>${content.country},
		${content.postcode}
		<c:if test="${not empty content.phone}">
			<br>
			<b>Phone</b>:${content.phone}
      </c:if>
		<c:if test="${not empty content.fax}">
			<br>
			<b>Fax</b>:${content.fax}
      </c:if>
		<c:if test="${not empty content.email}">
			<br><a href="mailto:${content.email}">${content.email}</a>
      </c:if>
	</p>

	<c:if test="${content.type eq 'Office Address'}">
	<p><a href="#" data-gmaplat="${content.lat}" data-gmaplon="${content.longitude}" data-gmaptitle="${content.addressname}" class="gmap-content">VIEW ON MAP</a></p>

	</c:if>
</div>