<%@ include file="../includes/taglibs.jsp" %>
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
<p>${content.staffname }</p>
<p>${content.stafftitle}
<c:if test="${not empty content.phone}">
						<br>
						<b>Phone</b>:${content.phone}
      </c:if>
					<c:if test="${not empty content.cell}">
						<br>
						<b>Cell</b>:${content.cell}
      </c:if>
					<c:if test="${not empty content.email}">
						<br><a href="mailto:${content.email}">Email</a>
      </c:if>
</div>
