<%@ include file="../includes/taglibs.jsp"%>

 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="id">${state.getCurrentContentNode().getUUID()}</c:set>
<li><input type="button" class="collapsed"
	value="${content.question}"
	data-target="#${id}" data-toggle="collapse">
	<div class="collapse" id="${id}" style="height: auto;">
		${cmsfn:decode(content).answer}

	</div></li>
