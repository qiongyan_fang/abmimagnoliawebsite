<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />


<div class="spotlight">
<c:if test="${not empty content.url}">
 <a ${fn:startsWith(content.url, "http")?"target=_blank":""}
		href="${content.url}" title="${content.headerline1}, ${content.headerline2}">
	<img src="${cmsfn:link(cmsfn:asContentMap(contentMap))}"
		alt="${content.headerline1}" class="img-responsive">
		<div class="overlay">
			<h3 class="text-right">${cmsfn:decode(content).headerline1}</h3>
			<div class="spotlight-base">
				<h4 class="text-right">${cmsfn:decode(content).headerline2}</h4>
				<div style="clear:both"></div>
				<p class="text-right">${cmsfn:decode(content).content}</p>
			</div>
		</div>
	</a>
	</c:if>
	<c:if test="${empty content.url}"> <!--  separate urls for headers -->
	<img src="${cmsfn:link(cmsfn:asContentMap(contentMap))}"
		alt="${content.headerline1}" class="img-responsive">
		<div class="overlay spotlink">
			<a ${fn:startsWith(content.headerline1url, "http")?"target=_blank":""}
		href="${content.headerline1url}"> <h3 class="text-right">${cmsfn:decode(content).headerline1}</h3></a>
			<div class="spotlight-base">
			<a ${fn:startsWith(content.headerline2url, "http")?"target=_blank":""}
		href="${content.headerline2url}">	<h4 class="text-right">${cmsfn:decode(content).headerline2}</h4></a>
				<div style="clear:both"></div>
			<a ${fn:startsWith(content.contenturl, "http")?"target=_blank":""}
		href="${content.contenturl}">	<p class="text-right">${cmsfn:decode(content).content}</p></a>
			</div>
		</div>
	</c:if>
</div>