<%@ include file="../includes/taglibs.jsp"%>
<c:set var="topImage" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="logoImage" value="${cmsfn:content(content.logoUrl,'dam')}" />


<img alt="top image" src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
<div class="col-lg-12 colorbox">
<img alt="logo" src="${cmsfn:link(cmsfn:asContentMap(logoImage))}">
<p>${content.description}</p>
<a class="btn btn-learn" target=_blank href="${content.externalUrl}">${content.buttonText}</a>
</div>

