<%@ include file="../includes/taglibs.jsp"%>
<c:set var="urlNode" value="${cmsfn:content(content.url,'website')}" />
<c:set var="contentMap" value="${cmsfn:content(content.imageurl,'dam')}" />

<a class="button ${content.oneLine=='true'?'oneLine':''} ${content.last=='true'?'last':''}" href="${cmsfn:link(cmsfn:asContentMap(urlNode))}">
<img
	width="42" height="42" class="icon" alt="${content.buttontext}"
	src="${cmsfn:link(cmsfn:asContentMap(contentMap))}">${content.buttontext}</a>