 <%@ include file="../includes/taglibs.jsp" %>
 
 
 <ul>
            <c:forEach items="${currentNews}" var="navigationEntry">
                <li><a href="${pageContext.request.contextPath}${navigationEntry.key}">${navigationEntry.value}</a></li>
            </c:forEach>
            </ul>
            
            <div>${empty content.archiveheader?"Archive":content.archiveheader}</div>
             <c:forEach items="${archiveNews}" var="archiveTime">
                <li><a href="${pageContext.request.contextPath}/news?time=${archiveTime}">${archiveTime}</a></li>
            </c:forEach>
            </ul>