<%@ include file="../includes/taglibs.jsp"%>
<c:set var="topImage" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="viewUrl" value="${cmsfn:content(content.viewUrl,'dam')}" />
<c:set var="downloadUrl" value="${cmsfn:content(content.downloadUrl,'dam')}" />



<div class="col-lg-3 download-resource">
					<img alt="smapleImage" src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
					<div class="col-lg-12 ${content.backgroundcolor} colorbox">
						<h4>${content.header}</h4>
						<p>${content.description}</p>
						<c:if test="${not empty content.viewUrl}" >
						<a class="btn btn-learn" href="${cmsfn:link(cmsfn:asContentMap(viewUrl))}">VIEW</a>
						</c:if>
						<c:if test="${not empty content.downloadUrl}" >
						<a class="btn btn-learn pdf" download target=_blank href="${cmsfn:link(cmsfn:asContentMap(downloadUrl))}">DOWNLOAD</a>
						</c:if>
					</div>
				</div>