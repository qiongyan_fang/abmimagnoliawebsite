
<%@ include file="../includes/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="/docroot/js/rawdata_content.js" type="text/javascript"
	charset="utf-8"></script>

<script src="/docroot/js/rawdata_menu.js" type="text/javascript"
	charset="utf-8"></script>

<!-- show raw data selection options and results -->

	<div class="slide-nav rawdatadownload" id="slide-navID" style="display:none;">
		<div class="menu-child">
			<!-- <div class="return"><a href="javascript:;">Return</a></div> -->
			<div class="choose-data">
				<a>CHOOSE DATA</a>
				<ul id="category-list">
				</ul>
			</div>


			<div class="filter-by">
				<a>FILTER BY:</a>
				<ul id="filter-list">
					<li class="level-2"><a id="a-region"  href="javascript:;"
						class="region first-child">REGION</a>

						<div class="region-slide-nav sub-slide-nav"
							style="display: block;">
							<form class="data-form region-form menu-child">
								<ul id="regiondetail">
									<li class="groupregion"><label class="radio-item" for="rl0">
									<input data-heading='async-region'
											type="checkbox" id="rl0" name="region"
											class='regiongroupinput' value="Alberta"><span class="outer"><span
												class="inner"></span></span>All Alberta</label></li>
								</ul>

							</form>
						</div></li>

					<li class="level-2"><a id="a-year" href="javascript:;"
						class="years first-child">YEARS</a>
						<div class="years-slide-nav sub-slide-nav" style="display: none;">
							<form class="data-form years-form  menu-child">
								<ul id="rotationdetail">

								</ul>
								
								<div class="buttons-box">
								<p style="font-size:1em; color:#ffffff;margin-right:35px;">* During the 4-year prototype program (2003--2006), ABMI only collected
								 information at 125 unique core terrestrial sites using a subset of ABMI protocols: 
								 vascular plants, bryophytes, lichens, birds, trees &amp; snags, down woody material, ground cover.</p>
									<input type="button" value="SELECT ALL" class="button select"
										id="select-years"> <input type="reset"
										value="CLEAR ALL" class="button clear" id="clear-years">


								</div>

							</form>
						</div></li>

					<li class="level-2"><a id="a-data-type" href="javascript:;"
						class="data-type first-child">DATA TYPE</a>

						<div class="data-type-slide-nav sub-slide-nav"
							style="display: none;">
							<form class="data-form data-type-form menu-child">
								<ul id="datadetail">


								</ul>

								

							</form>
						</div></li>
				</ul>
				
			</div>
		</div>

		<div id="map-container">
			<h4>Raw Data</h4>
			<div id="async-map-details">
				<p>
					<strong>Terrestrial Species:</strong> <span
						id="terrestrial-species"><span class="noselect">none
							selected</span></span>
				</p>
				<p>
					<strong>Terrestrial Habitat:</strong> <span
						id="terrestrial-habitat"><span class="noselect">none
							selected</span></span>
				</p>
				<p>
					<strong>Wetland Species:</strong> <span id="wetland-species"><span
						class="noselect">none selected</span></span>
				</p>
				<p>
					<strong>Wetland Habitat:</strong> <span id="wetland-habitat"><span
						class="noselect">none selected</span></span>
				</p>
				<p>
					<strong>Regions:</strong> <span id="async-region"><span
						class="noselect">none selected</span></span>
				</p>
				<p>
					<strong>Years:</strong> <span id="async-years"><span
						class="noselect">none selected</span></span>
				</p>
				<p>
					<strong>Data Type:</strong> <span id="async-data"><span
						class="noselect">none selected</span></span>
				</p>
				
			</div>
				<div id="get-data-btn" class='button'>GET DATA</div>
			<div id="loading-div" style="z-index: 999; display:none ;width:100%;text-align:center;"><img alt="" src="/docroot/assets/ajax_loader_blue_512.gif" width="50"></div>
			<div id="download-data-area"></div>
			<div id="html-map"><div class="ajax-map-image">
									<a href="#" id="maphref" class="zoom fullzoom"><img alt="" id="mapimg" src="/docroot/assets/maps/rawdata-map-0.jpg">
									</a></div>
					</div>
		
			
		</div>
		<!-- map-container -->
	</div>
	<!-- slide-nav -->



