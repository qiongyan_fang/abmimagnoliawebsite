<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>

  
  <body class="container-fluid">
   <%@ include file="../includes/menu.jsp"%>
	
	<div class="row">
		<cms:area name="jumbotron"/>
	</div>
	<div class="row breadcrumb" >
		<div class="container">
			<div class="col-lg-12">
				<h3><cms:area name="titleArea" /></h3>
			</div>
		</div>
	
	</div>
	<div class="row breadcrumb-triangle" >
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
		
			<div class="${content.narrowcontent eq 'true'? 'col-xs-12 col-md-9 col-lg-8 main':'col-lg-9 main'}">
			<cms:area name="leftColumnArea" />
			</div>
			
			<div class="${content.narrowcontent eq 'true'?'col-lg-3 col-lg-offset-1 col-sm-12 aside':'col-lg-3 col-md-12 col-sm-12 aside'}">
				
					<cms:area name="rightColumnArea" />
				
			
				
					<cms:area name="rightWidgetColumnArea" />	
				
			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
   
  </body>
</html>