<%@ include file="../includes/taglibs.jsp"%>
<div class="pagination">
				<c:if test="${mode eq 'detail'}">
					<p class="next">
						<c:if test="${page > 1}">
							<span><a href="${state.originalBrowserURI}?page=${page-1}"><img
									alt="previous page" src="/docroot/assets/prevarrow2.png"></span>
							</a>
							</span>
						</c:if>

						Page ${page} of ${totalPage}
						<c:if test="${page < totalPage}">
							<span><a href="${state.originalBrowserURI}?page=${page+1}"><img
									alt="next page" src="/docroot/assets/nextarrow.png"></span>
							</a>
						</c:if>
					</p>
				</c:if>
			</div>