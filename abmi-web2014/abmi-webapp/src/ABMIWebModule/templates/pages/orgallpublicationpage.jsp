<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<body class="container-fluid publications">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<!--  ===================  FLOATING NAV List view detail view Search button 

=============== -->
			<div class="side-top-nav col-xs-12 col-lg-3">
			<c:url var="queryuri" value="${state.originalBrowserURI}">
				<c:if test='${not empty param.documenttype}'><c:param name="documenttype" value="${param.documenttype}"/></c:if>
				<c:if test='${not empty param.subject}'><c:param name="subject" value="${param.subject}" /></c:if>
				<c:if test='${not empty param.time}'><c:param name="time" value="${param.time}" /></c:if>
				<c:if test='${not empty param.keyword}'><c:param name="keyword" value="${param.keyword}" /></c:if>
				<c:param name="scroll" value="" />
			</c:url>

				<div class="widget listSyle">
					<c:if test="${mode eq 'detail'}">
						<a class="sidemenuFullPostBtn active" href="#">&nbsp;</a>
						<a class="sidemenuListPostBtn"
							href="${queryuri}&mode=list">&nbsp;</a>

					</c:if>

							<c:if test="${mode eq 'list'}">
						<a class="sidemenuFullPostBtn"
							href="${queryuri}?mode=detail">&nbsp;</a>
						<a class="sidemenuListPostBtn active" href="#">&nbsp;</a>

					</c:if>
					<c:if
						test="${not empty  param.documenttype or not empty param.subject or not empty param.keyword or not empty param.time}">
						<a href="${state.originalBrowserURI}?mode=${mode}"
							class="backtoallBtn">Back To All</a>
					</c:if>
				</div>
					<%@ include file="publicationsearch.jsp"%>

			</div>
			<div class="clear-to-lg"></div>
			<!--  ================================== -->


			<!--  ===================  MAIN COLUMN =============== -->
			<div class="col-lg-9 main publication-post">
				<cms:area name="topLeftColumnArea" />


				<c:if test="${empty publicationList }">
					<p>No result matches your query.</p>
				</c:if>

<c:choose>
<c:when test="${fn:startsWith(content.placeholderimg, 'http')}">
<c:set var="defaultHeaderImage"
					value="${content.placeholderimg}" />
					</c:when>
<c:otherwise>
				<c:set var="defaultHeaderImage"
					value="${cmsfn:link(cmsfn:content(content.placeholderimg,'dam'))}" />
</c:otherwise>
</c:choose>
				<c:forEach items="${publicationList}" var="content"
					varStatus="rowStatus">
				<c:url var="publicationlink" value="${content.path}.html">
										<c:param name="mode" value="${mode}" />
										<c:if test='${not empty param.documenttype}'><c:param name="documenttype" value="${param.documenttype}"/></c:if>
										<c:if test='${not empty param.subject}'><c:param name="subject" value="${param.subject}" /></c:if>
										<c:if test='${not empty param.time}'><c:param name="time" value="${param.time}" /></c:if>
										<c:if test='${not empty param.keyword}'><c:param name="keyword" value="${param.keyword}" /></c:if>
										<c:if test='${not empty param.page}'><c:param name="page" value="${param.page}" /></c:if>
										
						</c:url>
						<c:set var="viewLinkUrl" value="" />
						<c:if test="${not empty content.doi}">
				<c:set var="viewLinkUrl">
				<c:if test="${fn:startsWith(content.doi, 'http')}">
				${content.doi}
				</c:if>
				<c:if test="${not fn:startsWith(content.doi, 'http')}">
				http://doi.org/${content.doi}
				</c:if>
				</c:set> 
				</c:if>
						
			<c:set var="downloadlink" value="http://ftp.public.abmi.ca/${content.viewlink}" />
					<c:if test="${mode ne 'list'}">
						<c:set var="topImage"
							value="${cmsfn:content(content.imageUrl,'dam')}" />

						<div class="shortpost">
							<div class="post-feature-img">
								<!-- 1. cover image 2.resized large image 3. default place holder image !-->
								<c:set var="coverImage"
									value="${cmsfn:content(content.coverImageUrl,'dam')}" />
									<c:choose>
									<c:when test="${not empty coverImage }">
										<img alt=""
											src="${cmsfn:link(cmsfn:asContentMap(coverImage))}">
									</c:when>
									<c:when test="${not empty topImage }">
  
     						  	<img alt=""
											src="/.imaging/coverthumbnail/dam/${content.imageUrl}.">
									</c:when>
									<c:when test="${not empty defaultHeaderImage }">
										<img alt=""
											src="${defaultHeaderImage}">
									</c:when>

								</c:choose>

							</div>

							<div class="post-text col-sm-8 col-md-9">
								<div class="post-header">
									<h4><a href="${publicationlink}">${content.doctitle}</a></h4>
									<div class="post-data">
										<a href="#" class="post-by">${content.author }</a> <span
											class="post-date">${content.displaydate}</span>
											<c:if test="${content.suppdocslink.size() >0}"><span
											class="post-date">${content.suppdocslink.size()} ${content.suppdocslink.size()>1?"Resources":"Resource"}</span></c:if>
											<c:if test="${content.versioncount >1}"><span
											class="post-date">${content.versioncount} ${content.versioncount >1?"Versions":"Version"}</span></c:if>
												<c:set var="downloadclass" value="${not empty content.flipbookLink?'post-date':''}" />										
												<c:choose>
											<c:when test="${not empty viewLinkUrl}">
											<a  type="text/html" class="pdf ${downloadclass }"
										href="${viewLinkUrl}" target=_blank>VIEW
										</a>
											</c:when>
											<c:when test="${not empty content.viewlink}">
									<c:set var="viewlink"
										value="${cmsfn:content(content.viewlink,'dam')}" />
							<a download class="post-pdf ${downloadclass }"
										href="${downloadlink}">DOWNLOAD</a>
										
									<!--<a download class="pdf"
										href="${cmsfn:link(cmsfn:asContentMap(viewlink))}">DOWNLOAD
										c:if test="${not empty content.extension}">${content.extension} ${content.size}
										c:if</a>
										-->
								</c:when>
								</c:choose>
								<!--  show flipbook link -->
								<c:if test="${not empty content.flipbookLink}">
								<a  class="post-flipbook" target="_blank"
										href="${ftpprefix}${content.flipbookLink}">${empty content.flipbookLinkText?"FLIPBOOK":content.flipbookLinkText}</a>
								</c:if>
								<!--  end of show flipbook link -->
									</div>
								</div>
								<div class="post-exerpt">
									<p>${content.description }</p>
									<a class="readmore-link" href="${publicationlink}">CONTINUE READING</a>
								</div>
							</div>

						</div>


					</c:if>

					<c:if test="${mode eq 'list'}">
					<div class="shortpost-list">
                	<div class="post-text">
                        <div class="post-header">
                      <h4><a href="${publicationlink}">${content.doctitle}</a></h4>
                            <div class="post-data">
                            <a href="#" class="post-by">${content.author }</a> <span
											class="post-date">${content.displaydate}</span>
											<c:if test="${content.suppdocslink.size() >0}"><span
											class="post-date">${content.suppdocslink.size()} ${content.suppdocslink.size()>1?"Resources":"Resource"}</span></c:if>
											<c:if test="${content.versioncount >1}"><span
											class="post-date">${content.versioncount} ${content.versioncount >1?"Versions":"Version"}</span></c:if>
											<c:set var="downloadclass" value="${not empty content.flipbookLink?'post-date':''}" />
												<c:choose>
											<c:when test="${not empty viewLinkUrl}">
											<a  type="text/html" class="pdf ${downloadclass }"
										href="${viewLinkUrl}" target=_blank>VIEW
										</a>
											</c:when>
											<c:when test="${not empty content.viewlink}">
									<c:set var="viewlink"
										value="${cmsfn:content(content.viewlink,'dam')}" />

										<a download class="${downloadclass } post-pdf"
										href="${downloadlink}">DOWNLOAD</a>
								</c:when>
								</c:choose>
								
									<!--  show flipbook link -->
								<c:if test="${not empty content.flipbookLink}">
								<a  class="post-flipbook" target="_blank"
										href="${ftpprefix}${content.flipbookLink}">${empty content.flipbookLinkText?"FLIPBOOK":content.flipbookLinkText}</a>
								</c:if>
								<!--  end of show flipbook link -->
							</div>
                        </div>
                    	<a href="${publicationlink}" class="readmore-link">CONTINUE READING</a>
					</div>
				</div>
				
						
				</c:if>

				</c:forEach>
				<!--   -->
				<div class="pagination">
					<c:if test="${totalPage > 0 }">
						<p class="next">
							<c:if test="${page > 1}">
								<span><a
									href="${queryuri}&mode=${mode}&page=${page-1}"><img
										alt="previous page" src="/docroot/assets/prevarrow2.png"></a>

								</span>
							</c:if>

							Page ${page} of ${totalPage}
							<c:if test="${page < totalPage}">
								<span><a
									href="${queryuri}&mode=${mode}&page=${page+1}">
										<img alt="next page" src="/docroot/assets/nextarrow.png">
								</a></span>
							</c:if>
						</p>
					</c:if>
					&nbsp;
				</div>

				<cms:area name="bottomLeftColumnArea" />

			</div>

			<div class="col-lg-3 col-md-12 col-sm-12 aside">


				<cms:area name="rightColumnArea" />

				<cms:area name="rightWidgetColumnArea" />
			</div>

		</div>
	</div>

	<%@ include file="../includes/footer.jsp"%>
<%@ include file="publicationgooglesearch.jsp"%>
	
</body>
</html>