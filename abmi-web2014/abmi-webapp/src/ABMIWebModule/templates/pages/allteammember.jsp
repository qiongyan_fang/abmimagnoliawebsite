<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>

  <body class="container-fluid board-of-directors">
  <%@ include file="../includes/menu.jsp"%>
 <!--  header -->
	<div class="row">
		<cms:area name="jumbotron"/>
	</div>
	<div class="row breadcrumb" >
		<div class="container">
			<div class="col-lg-12">
				<h3>${content.title }</h3>
			</div>
		</div>
	</div>
	<div class="row breadcrumb-triangle" >
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<div class="col-lg-9 main bod">
				 <c:forEach items="${members}" var="member">
            	 <c:set var="image" value="${cmsfn:content(member.photo,'dam')}" />
            	 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<img src="${cmsfn:link(cmsfn:asContentMap(image))}" alt="${member.name}">
					<div class="col-lg-12 ${fn:contains(member.position, 'Chair')?"d":""}dgreen">
						<h4>${member.personname}</h4>
						<h5>${member.position}</h5>
						<p>${member.otherposition}</p>
						<a class="btn btn-learn" href="${pageContext.request.contextPath}${member.otherposition}">READ BIO</a>
					</div>
				</div>
            </c:forEach>
			
				
				<cms:area name="leftColumnArea" />
			</div>
			<div class="col-lg-3 col-md-12 col-sm-12 aside">
				<div class="sidemenu">
					<cms:area name="memberRightColumnArea" />
				</div>
				<div class="list widget">
				<cms:area name="memberWidgetArea" />	
				
				</div>
			</div>
		</div>
	</div>
<%@ include file="../includes/footer.jsp"%>
  </body>
</html>