
<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>

<body class="container-fluid">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
<div class="col-lg-9 main">
			<c:set var="viewLink"
				value="${cmsfn:content(content.viewlink,'dam')}" />
		


			<c:set var="topImage"
				value="${cmsfn:content(content.imageUrl,'dam')}" />

			<div class="post">
				<div class="postheader ${empty topImage?"no-image":""}">
					<c:if test="${not empty topImage }">
						<img alt="top image"
							src="${cmsfn:link(cmsfn:asContentMap(topImage))}">
					</c:if>
					<h3>${not empty content.title? content.title: content.imagetitle}</h3>
				</div>
				<div class="arrowcontainer">
					<div class="postmeta">
					<p>
										<c:if test="${not empty content.author}">${cmsfn:decode(content).author}<br> </c:if> 
										<c:if test="${not empty content.displaydate}">${content.displaydate }</c:if>
										</p>
					
						
					</div>
					<!-- 
					<div class="postmeta arrow"
						style="border-width: 10px 290px 0px 0px;"></div>
						-->
				</div>
				<div class="postactions">
				<c:if test="${not empty viewLink}">
					<p>
						<a href="${cmsfn:link(cmsfn:asContentMap(viewLink))}" class="pdf">VIEW PDF</a> <!-- | <a
							download href="${cmsfn:link(cmsfn:asContentMap(downloadLink))}">DOWNLOAD</a> -->
					</p>
					</c:if>
					<div class="post-box-share addthis_sharing_toolbox" data-url="http://${pageContext.request.serverName}${pageContext.request.contextPath}${state.handle}
.html"></div>
				</div>
				<div class="postexerpt">
					<h4>${cmsfn:decode(content).title}</h4>
						<h4>${cmsfn:decode(content).doctitle}</h4>
									<p>${cmsfn:decode(content).description}</p>
									<h5>${cmsfn:decode(content).subtitle}</h5>
								
							<c:if test="${not empty content_documenttype}">
										<p class="documenttype">DOCUMENT TYPE:
											<c:forEach items="${content_documenttype}" var="row"
												varStatus="tagStatus">
												<a href="${state.originalBrowserURI}?documenttype=${row}">${row}</a><c:if test="${not tagStatus.last }">,
										</c:if>
											</c:forEach>
										</p>
									</c:if>
									
										<c:if test="${not empty content_subject}">
										<p class="subjectarea">SUBJECT AREA: 
											<c:forEach items="${content_subject}" var="row"
												varStatus="tagStatus">
												<a href="${state.originalBrowserURI}?subject=${row}">${row}</a><c:if test="${not tagStatus.last }">,
										</c:if>
											</c:forEach>
										</p>
									</c:if>
									
									<c:if test="${not empty content_keyword}">
										<p class="subjectarea">KEYWORDS:
											<c:forEach items="${content_keyword}" var="tag"
												varStatus="tagStatus">
												<a href="${state.originalBrowserURI}?keyword=${tag}">${tag}</a><c:if test="${not tagStatus.last }">,</c:if>
											</c:forEach>
										</p>
									</c:if>
				<cms:area name="leftColumnArea" />

				</div>


				
<!--  old -->
				<c:if test="${content.suppdocs.size()>0}">
					<ul class="faq supplementary">
						<li><input type="button" class="collapsed"
							value="Supplementary Reports" data-target="#1"
							data-toggle="collapse">
							<div class="collapse" id="1" style="height: auto;">
								<c:forEach items="${content.suppdocs}" var="suppdoc">
									<c:set var="supViewLink"
										value="${cmsfn:content(suppdoc[1],'dam')}" />
								

									<p>${suppdoc[0]}</p>
									<p>
										<a href="${cmsfn:link(cmsfn:asContentMap(supViewLink))}">VIEW</a>
										
									</p>

								</c:forEach>
							</div></li>
					</ul>
				</c:if>
				<!--  new  -->
				
					<c:if test="${content_supplementalreportslink.size()>0}">
					<ul class="faq supplementary"><li>
										<input type="button" class=""
											value="Supplementary Reports" data-target="#supplementpost1_${rowStatus.index}"
											data-toggle="collapse">
											<div class="collapse in" id="supplementpost1_${rowStatus.index}"
												style="height: auto;">
												<div class="row">

												<c:forEach items="${content_supplementalreportslink}" var="suppdoc">
													<c:set var="supViewLink"
														value="${cmsfn:content(suppdoc.viewlink,'dam')}" />
												
														<div class="col-xs-12"><h3>${suppdoc.title}</h3>
															<div class="postactions">
																<p>
																	<a class="pdf" href="${cmsfn:link(cmsfn:asContentMap(supViewLink))}">VIEW PDF</a>
																</p>
																<div class="post-box-share addthis_sharing_toolbox" data-url="http://${pageContext.request.serverName}${cmsfn:link(cmsfn:asContentMap(supViewLink))}"></div>
															</div>	
														</div>												
													

												</c:forEach>
											</div></div</li>
									</ul>

				</c:if>


			</div>

</div>
			<div class="col-lg-3 col-md-12 col-sm-12 aside">
				
					<cms:area name="rightColumnArea" />
				


				<cms:area name="rightWidgetColumnArea" />

			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
	
</body>
</html>