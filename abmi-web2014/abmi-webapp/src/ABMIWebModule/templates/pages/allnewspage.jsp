
<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
 
<body class="container-fluid news-page all-news">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<!--  ===================  FLOATING NAV List view detail view Search button =============== -->
			<div class="side-top-nav col-xs-12 col-lg-3">
				<div class="widget listSyle">
					<c:if test="${empty mode or mode eq 'detail'}">
						<a class="sidemenuFullPostBtn active" href="#">&nbsp;</a>
						<a class="sidemenuListPostBtn"
							href="${state.originalBrowserURI}?mode=list&scroll">&nbsp;</a>

					</c:if>

					<c:if test="${mode eq 'list'}">
						<a class="sidemenuFullPostBtn"
							href="${state.originalBrowserURI}?mode=detail&scroll">&nbsp;</a>
						<a class="sidemenuListPostBtn active"
							href="${state.originalBrowserURI}?mode=list&scroll">&nbsp;</a>

					</c:if>
				</div>
				<%@ include file="publicationsearch.jsp"%>
			</div>
			<div class="clear-to-lg"></div>
			<!--  ================================== -->

			<!--  ===================  MAIN COLUMN =============== -->
			<div class="col-lg-9 main news-post">
				<cms:area name="topLeftColumnArea" />

				<c:choose>
				<c:when test="${fn:startsWith(content.placeholderimg, 'http')}">
				<c:set var="defaultHeaderImage"
									value="${content.placeholderimg}" />
									</c:when>
				<c:otherwise>
								<c:set var="defaultHeaderImage"
									value="${cmsfn:link(cmsfn:content(content.placeholderimg,'dam'))}" />
				</c:otherwise>
				</c:choose>
				<c:forEach items="${publicationList}" var="content"
					varStatus="rowStatus">
					
					<c:set var="newslink">${content.path}.html?mode=${mode}&scroll
										<c:if test='${not empty param.time}'>&time=${param.time}</c:if>
										<c:if test='${not empty param.page}'>&page=${param.page}</c:if>
						</c:set>


					<c:if test="${empty mode or mode eq 'detail'}">

						<div class="shortpost">
							<div class="post-feature-img">
								<!-- 1. cover image 2.resized large image 3. default place holder image !-->
								<c:set var="coverImage"
									value="${cmsfn:content(content.coverImageUrl,'dam')}" />

								<c:set var="topImage"
									value="${cmsfn:content(content.imageUrl,'dam')}" />

								<c:choose>
									<c:when test="${not empty coverImage }">
										<img alt=""
											src="${cmsfn:link(cmsfn:asContentMap(coverImage))}">
									</c:when>
									<c:when test="${not empty topImage }">

										<img alt=""
											src="/.imaging/coverthumbnail/dam/${content.imageUrl}.">
									</c:when>
									<c:when test="${not empty defaultHeaderImage }">
										<img alt=""
											src="${defaultHeaderImage}">
									</c:when>

								</c:choose>
							</div>
							<div class="post-text col-sm-8 col-md-9">
								<div class="post-header">
									<h4><a href="${newslink}">${content.doctitle}</a></h4>
									<div class="post-data">
										<span>POSTED ON: </span>
										<span class="post-date1">
										<c:choose>
										<c:when test="${not empty content.publishdateStr }">
										${content.publishdateStr}
										</c:when>
										<c:otherwise>
										<fmt:formatDate pattern="MMMM dd, yyyy" value='${content.publishdate.getTime()}' />
										</c:otherwise>
										</c:choose></span>
									</div>
								</div>
								<div class="post-exerpt">

									<p>${content.description}</p>
									<a class="readmore-link" href="${newslink}">CONTINUE
										READING</a>
								</div>
							</div>

						</div>

					</c:if>

					<c:if test="${mode eq 'list'}">
						<div class="shortpost-list">
							<div class="post-text">
								<div class="post-header">
									<h4><a href="${newslink}">${content.doctitle}</a></h4>
									<div class="post-data">
										<span>POSTED ON: </span><span class="post-date1">
										<c:choose>
										<c:when test="${not empty content.publishdateStr }">
										${content.publishdateStr}
										</c:when>
										<c:otherwise>
										<fmt:formatDate pattern="MMMM dd, yyyy" value='${content.publishdate.getTime()}'/>
										</c:otherwise>
										</c:choose></span>
									</div>
								</div>
								<a class="readmore-link" href="${newslink}">CONTINUE
									READING</a>
							</div>
						</div>



					</c:if>
			
				</c:forEach>
			
			<%@ include file="newspaging.jsp"%>

			<cms:area name="bottomLeftColumnArea" />
		
	</div>

	<div class="col-lg-3 col-md-12 col-sm-12 aside">



		<cms:area name="rightColumnArea" />


		<cms:area name="rightWidgetColumnArea" />

	</div>
	</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
	<%@ include file="newseventsgooglesearch.jsp"%>


</body>
</html>