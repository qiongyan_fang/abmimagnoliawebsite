<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<body class="container-fluid publications">
	<%@ include file="../includes/menu.jsp"%>

	<div class="row">
		<cms:area name="jumbotron" />
	</div>
	<div class="row breadcrumb">
		<div class="container">
			<div class="col-lg-12">
				<h3>
					<cms:area name="titleArea" />
				</h3>
			</div>
		</div>

	</div>
	<div class="row breadcrumb-triangle">
		<div class="container">
			<div class="col-lg-12">
				<div class="triangle hidden-xs hidden-sm"></div>
			</div>
		</div>
	</div>
	<div class="row page-content">
		<div class="container">
			<!--  ===================  FLOATING NAV List view detail view Search button 

=============== -->
			<div class="side-top-nav col-xs-12 col-lg-3">
		
				<div class="widget listSyle">
				
						<a class="sidemenuFullPostBtn active" href="javascript:void(0);">&nbsp;</a>
						<a class="sidemenuListPostBtn"
							href="javascript:void(0);">&nbsp;</a>

								
					
					<c:choose>
<c:when test="${fn:startsWith(content.placeholderimg, 'http')}">
<c:set var="defaultHeaderImage"
					value="${content.placeholderimg}" />
					</c:when>
<c:otherwise>
				<c:set var="defaultHeaderImage"
					value="${cmsfn:link(cmsfn:content(content.placeholderimg,'dam'))}" />
</c:otherwise>
</c:choose>
<script>
var publicationDefaultCoverImage = "${defaultHeaderImage}";
var featuredList = "${featured}";
var descriptionCount = ${empty content.descriptioncount? 300: content.descriptioncount};
var titleCount = ${empty content.titlecount? 100: content.titlecount};
var pageSize = ${empty content.detailcount?20:content.detailcount};
var documentType = [];
var subject = [];

<c:if test="${not empty param.documenttype}">
<c:forEach items="${paramValues.documenttype}" var="row">
documentType[documentType.length] = "${row}";
 </c:forEach>
</c:if>


<c:if test="${not empty param.subject}">
<c:forEach items="${paramValues.subject}" var="row">
 subject[subject.length] = "${row}";
 </c:forEach>
</c:if>

var time = [];
<c:if test="${not empty param.time}">
<c:forEach items="${paramValues.time}" var="row">
time[time.length] = "${row}";
 </c:forEach>
</c:if>


var keyword = "${not empty param.keyword?param.keyword:''}";

var offset = "${empty param.offset?1:param.offset}";
var featuredOnly = ${empty param.featuredOnly?true:param.featuredOnly};
</script>
				</div>
					<%@ include file="publicationsearch.jsp"%>

			</div>
			<div class="clear-to-lg"></div>
			<!--  ================================== -->


			<!--  ===================  MAIN COLUMN =============== -->
			<div class="col-lg-9 main publication-post">
				<cms:area name="topLeftColumnArea" />
			
			<%@ include file="publication_new_search_insert.jsp"%>

			
				<!--   -->
				<div id="publication-list">
				</div>
				
				<div class="loadMore  ">
				<span>Click to Load More</span>
			</div>
				<cms:area name="bottomLeftColumnArea" />

			</div>

				
			<div class="col-lg-3 col-md-12 col-sm-12 aside">


				<cms:area name="rightColumnArea" />

				<cms:area name="rightWidgetColumnArea" />
			</div>

		</div>
	</div>

	<%@ include file="../includes/footer.jsp"%>
<%@ include file="publicationgooglesearch.jsp"%>
	
</body>
</html>