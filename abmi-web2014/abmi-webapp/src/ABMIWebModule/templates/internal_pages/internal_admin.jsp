<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

<script src="/docroot/js/internal/publication_update.js"
	type="text/javascript" charset="utf-8"></script>

<h1>Administrator Task</h1>
<ul>
	<li>load publications (it will overwrite existing records)<a
		href="javascript:void();" id="load">here</a></li>
	<li>export publications <a id="export">here</a></li>
	<li>update version/supplementary document settings (hide

		supplementary documents, hide old version documents) <a id="update">here</a>
	</li>
</ul>
