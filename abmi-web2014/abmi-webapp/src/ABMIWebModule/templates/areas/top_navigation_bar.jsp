<%--@elvariable id="components" type="java.util.Collection"--%>
<%@ include file="../includes/taglibs.jsp" %>

<div class="top-bar">
			<div class="container hidden-xs hidden-sm">
				<ul>
				 <c:forEach items="${components}" var="component">
      <li>  <cms:component content="${component}" /> </li>
    </c:forEach>			
				</ul>
			</div>
		</div>
		
