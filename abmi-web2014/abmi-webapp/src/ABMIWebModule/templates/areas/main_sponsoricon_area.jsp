
<%@ include file="../includes/taglibs.jsp" %>
<c:if test="${cmsfn:isEditMode() }">
	<c:forEach var="i" begin="1" end="${components.size()}">
   		<c:set var="divClass" value="" />
  	<c:if test="${i%3 eq  0 }" >
  	<c:set var="divClass" value="last" />
  	</c:if>
    <div class="col-xs-12 col-md-4 sponsor ${divClass}">

    <cms:component content="${components[i-1]}" />
    </div>
  </c:forEach>
</c:if>
<c:if test="${not cmsfn:isEditMode() }">
   	<c:forEach var="i" begin="1" end="${components.size() > 3? 3:components.size()}">
   		<c:set var="divClass" value="" />
  	<c:if test="${i%3 eq  0 }" >
  	<c:set var="divClass" value="last" />
  	</c:if>
    <div class="col-xs-12 col-md-4 sponsor ${divClass}">

    <cms:component content="${components[order[i-1]]}" />
    </div>
  </c:forEach>
</c:if>