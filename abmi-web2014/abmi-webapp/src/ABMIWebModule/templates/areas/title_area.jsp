<%--@elvariable id="components" type="java.util.Collection"--%>
<%@ include file="../includes/taglibs.jsp"%>

<c:if test="${components.size() > 0 }">
		<c:forEach items="${components}" var="component">
			<cms:component content="${component}" />
		</c:forEach>
</c:if>
<c:if test="${components.size() ==0 }">
${state.mainContentNode.getProperty("title").getString()}
</c:if>
