<%--@elvariable id="components" type="java.util.Collection"--%>
<%@ include file="../../includes/taglibs.jsp" %>


  <c:forEach items="${components}" var="component">
<div class="sidemenu widget">
    <cms:component content="${component}" />
</div>
  </c:forEach>

