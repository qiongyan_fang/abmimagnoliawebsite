<%@ include file="../includes/taglibs.jsp" %>
<div class="clearboth"></div>
<c:choose>
<c:when test="${content.areacount eq '2'}">
<c:set var="divclass" value="col-md-6 col-xs-12" />
</c:when>
<c:when test="${content.areacount eq '3'}">
<c:set var="divclass" value="col-md-4 col-xs-12" />
</c:when>
<c:when test="${content.areacount eq '4'}">
<c:set var="divclass" value="col-md-4 col-xs-12" />
</c:when>
<c:otherwise>
<c:set var="divclass" value="lg-" />
</c:otherwise>
</c:choose>

  <c:forEach items="${components}" var="component">
   <div class="spacebelow ${divclass}">
    <cms:component content="${component}" />
    </div>
  </c:forEach>
<div class="clearboth"></div>