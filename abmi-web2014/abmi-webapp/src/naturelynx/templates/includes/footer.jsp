 <footer class="navyblue">
    	<div class="container">
        	<form action="" class="newsletter-form">
            	<input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  placeholder="ENTER YOUR EMAIL TO RECEIVE NATUREHOOD NEWS & UPDATES">
                <button type="submit">Sign up</button>
            </form>
            <div class="footer-links">
                <ul class="footer-nav">
                	<cms:area name="footer"/>
                  
                </ul>
                <ul class="social-nav">
                    <li><a href="#" class="social-link"><img src="/docroot/naturelynx/images/facebook-icon.svg" alt="facebook link"></a></li>
                    <li><a href="#" class="social-link"><img src="/docroot/naturelynx/images/twitter-icon.svg" alt="Twitter link"></a></li>
                    <li><a href="#" class="social-link"><img src="/docroot/naturelynx/images/instagram-icon.svg" alt="Instagram link"></a></li>
                </ul>
            </div>
            <p class="copyright">
            	
            <span>&copy;2015 Alberta Biodiversity Monitoring Institute</span><span>All Rights Reserved</span><cms:area name="finePrintFooter"/><a href="#">Privacy & Terms of Use</a></p>
        </div>
    </footer>
    <!--/.Footer-->
    
   
  