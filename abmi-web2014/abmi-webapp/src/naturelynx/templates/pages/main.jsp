<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
     
  <body>
	<%@ include file="../includes/menu.jsp"%>
	
	<cms:area name="banner"/>
	    
  
	<!--/.Banner-->
	<section class="container species-count">
		<div class="row">
			<div class="col-md-6 s_identified">
				<span><cms:area name="sppIdArea"/></span><span id="sppIded" class="count counter"></span> 
			</div>
			<div class="col-md-6 s_verified">
				<span><cms:area name="sppVerifyArea"/></span><span id="sppVerified" class="count counter"></span>
			</div>
		</div> 
    </section>
    
    <cms:area name="rowArea" />
    
<%@ include file="../includes/footer.jsp"%>
 

  </body>
</html>