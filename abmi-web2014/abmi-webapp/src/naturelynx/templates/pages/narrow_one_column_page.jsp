<%@ include file="../includes/taglibs.jsp"%>
<%@ include file="../includes/header.jsp"%>
     
  <body>
	<%@ include file="../includes/menu.jsp"%>
	
	<cms:area name="banner"/>
	    
  
	<!--/.Banner-->
	<section class="container sub-nav">
	<cms:area name="navigationArea" />
    </section>
    
    <cms:area name="rowArea" />
    
<%@ include file="../includes/footer.jsp"%>
 

  </body>
</html>