<%@ include file="../includes/taglibs.jsp"%>
<c:set var="contentMap" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl" value = "${cmsfn:link(cmsfn:asContentMap(contentMap))}" />

					
<div data-diff="200" style="background-image: url(${imageUrl}); background-attachment: scroll; background-position: 50% -96.4545px;" class="mainbanner fullscreen background banner parallax">
	 	<img class="banner-img" src="${imageUrl}">
	 	<div class="banner-txt">
	        <h1>${content.header}</h1>
	        <p>${content.description }</p>
	        <a class="btn light-btn" title="" ${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""} href="${content.buttonUrl}">${content.buttonText }</a>
	        <h6>${content.subheader}</h6>
	        ${cmsfn:decode(content).additional}
	    </div>
        
    </div>