<%@ include file="../includes/taglibs.jsp"%>
<c:set var="contentMap" value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="imageUrl" value = "${cmsfn:link(cmsfn:asContentMap(contentMap))}" />

					
<section class="container callout ${backgroundcolor}-background">
	    <div class="row">
		    <div class="col-lg-9">
			    <div class="col-lg-2">
			    <img alt="ABMI Logo" src="${imageUrl}">
			    </div>
			    <div class="callout-text col-lg-10"><h3>${content.header}</h3>
			    <p>${content.description}</p></div>
		    </div>
		    <div class="col-lg-3">
		    	<a class="btn btn-line" ${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""} href="${content.buttonUrl}">${content.buttonText}</a>
			    
		    </div>
	    </div>
    </section>