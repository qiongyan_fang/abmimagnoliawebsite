<%@ include file="../includes/taglibs.jsp"%>
<c:set var="iconMap" value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="iconUrl" value = "${cmsfn:link(cmsfn:asContentMap(iconMap))}" />

<section class="container recent-update">
    	<div class="row">
	    	<div class="col-lg-8 col-md-7 recent-sightings">
		    	<img class="sightings-icon" alt="" src="${iconUrl}">
		    	<h2>${content.header}</h2>
		    	<p>${content.description}</p>
		    	<a class="btn btn-line" ${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""} href="${content.buttonUrl}">${content.buttonText}</a>
	    	</div>
	    	<c:forEach items="${projects}" var="row" varStatus="rowStatus">
	    	<c:forEach var="row" items="${content.sightings}">
	    	${row.headertextcompo}
	    	<br>
	    	${row.headertextcompo.sppname}
	    	</c:forEach>
	   	<div class="col-lg-4 col-md-5 sightings-slider">
		    	<div class="flexslider">
			        <ul class="slides">
						<li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class="">
							<div class="caption">
						    	<div class="verified status"><span>VERIFIED</span></div>
						    	<h4>Blue Bird</h4>
					    	</div>
			                <img src="images/bird-1.jpg">
			            </li>
			            <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class="">
							<div class="caption">
						    	<div class="verified status"><span>VERIFIED</span></div>
						    	<h4>Fox</h4>
					    	</div>
			                <img src="images/slide-2.jpg">
			            </li>
			            <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: list-item;" class="flex-active-slide">
							<div class="caption">
						    	<div class="verified status"><span>VERIFIED</span></div>
						    	<h4>Striped Hyena</h4>
					    	</div>
			                <img src="images/slide-3.jpg">
			            </li>
			        </ul>
			    <ol class="flex-control-nav flex-control-paging"><li><a class="">1</a></li><li><a class="">2</a></li><li><a class="flex-active">3</a></li></ol></div>
		    	
			</div>
    	</div>
    </section>