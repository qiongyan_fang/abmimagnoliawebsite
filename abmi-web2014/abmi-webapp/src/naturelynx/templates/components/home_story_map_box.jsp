<%@ include file="../includes/taglibs.jsp"%>
<c:set var="iconMap" value="${cmsfn:content(content.iconUrl,'dam')}" />
<c:set var="iconUrl" value = "${cmsfn:link(cmsfn:asContentMap(iconMap))}" />

<c:set var="imageMap" value="${cmsfn:content(content.imageUrl,'dam')}" />
<c:set var="imageUrl" value = "${cmsfn:link(cmsfn:asContentMap(imageMap))}" />

<section class="container story-map ${backgroundcolor}-background">
    	<div class="row">
        	<div class="col-lg-4 map-img">
            	<img alt="" src="${imageUrl}">
            </div>
            <div class="col-lg-8 story-map-info">
            	<img class="map-icon" src="${iconUrl}">
                <h2>${content.header}</h2>
                <p>${content.description}</p>
            	<a class="btn btn-line" ${fn:startsWith(content.buttonUrl, "http")?"target=_blank":""} href="${content.buttonUrl}">${content.buttonText}</a>
            </div>
        </div>
    </section>
  