package abmi.api.controller.internal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
@RequestMapping(value = "/publication")
public class InternalPublicationController {
	@PersistenceContext(unitName="data")
	EntityManager em;

	private static final Logger log = LoggerFactory
			.getLogger(InternalPublicationController.class);

	@RequestMapping(value = "/updatePublication", produces = "application/json" , method=RequestMethod.GET)
	public @ResponseBody String updatePublication(
			@RequestParam(value = "type", required = false) String gridType

	) {
		log.debug("start ...");
		InternalDataImporterService.generatePublicationPages(em);
//		try {
//			DataImporterService.movePublications();
//		} catch (PathNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return "Publication reloaded.";
	}
	
	
	@RequestMapping(value = "/exportPublication", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody String exportPublication(
			@RequestParam(value = "rootPath", required = false) String localRootPage	
	) {
		log.debug("start ...");
		return InternalDataImporterService.exportPublicationPage(localRootPage);

		
	}
	
	@RequestMapping(value = "/updatePublicationVersion", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody String updatePublicationVersion(
			@RequestParam(value = "rootPath", required = false) String localRootPage	
	) {
		log.debug("start ...");
		return DataImporterService.updateSupplementaryVersionDocument(localRootPage) + "";

		
	}
	
}
