package abmi.api.controller.internal.profile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import abmi.api.controller.internal.InternalPublicationController;
import abmi.model.services.internal.profile.SpeciesProfileBatchDAO;
@Controller
@RequestMapping(value = "/profileInternal")
public class InternalSpeciesProfileController {
	@PersistenceContext(unitName="data")
	EntityManager em;

	private static final Logger log = LoggerFactory
			.getLogger(InternalPublicationController.class);

	@RequestMapping(value = "/loadProfiles", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody String updatePublication(
			@RequestParam(value = "type", required = false) String gridType

	) {
		log.debug("start ...");
//		SpeciesProfileBatchDAO.generatePublicationPages(em);
//		SpeciesProfileBatchDAO.batchUpdateStatus();
		
//		try {
//			DataImporterService.movePublications();
//		} catch (PathNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return "hello";
	}
	
	
	@RequestMapping(value = "/updateStatus", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody String updatePublicationStatus(
			@RequestParam(value = "type", required = false) String gridType

	) {
		log.debug("start ...");
//		SpeciesProfileBatchDAO.generatePublicationPages(em);
		SpeciesProfileBatchDAO.batchUpdateStatus();
		
//		try {
//			DataImporterService.movePublications();
//		} catch (PathNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return "hello";
	}
}
