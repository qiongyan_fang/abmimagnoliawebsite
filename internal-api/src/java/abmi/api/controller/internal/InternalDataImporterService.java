package abmi.api.controller.internal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import abmi.model.entity.internal.TmpReloadPublication;

import abmi.module.template.util.TemplateUtil;


import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.objectfactory.Components;
import info.magnolia.repository.RepositoryConstants;
import info.magnolia.templating.functions.TemplatingFunctions;

@Service
@Transactional
public class InternalDataImporterService {
	static final String rootPath = "/home/newpublications";

	private static final Logger log = LoggerFactory
			.getLogger(InternalDataImporterService.class);

	static boolean createOnePublicationRecord(TmpReloadPublication row,Map<String, Object> publicationMeta

	) {
		Session session;

		String imgPath1 = "/home/publications/images/"; // under assets
//		String pdfPath = "/home/publications/documents/";
		Calendar date = new GregorianCalendar(1990, 1, 1);
		try {
			session = MgnlContext.getJCRSession(RepositoryConstants.WEBSITE);
			Node c = session.getNode(rootPath);

			Node newPage = null;
			String uId = "" + String.valueOf(row.getUniqueFileNumber());

			int uniqueId = Integer.parseInt(uId);
			int roundFifty = (uniqueId / 50) * 50 + 1;

			log.debug("uniqueId ={} round to {}", uId, roundFifty);
			String folderName = roundFifty + "-" + (roundFifty + 49);
			Node folderPage ;
			if (!c.hasNode(folderName)) {
				folderPage = c.addNode(folderName, NodeTypes.Page.NAME);
				folderPage.setProperty("mgnl:template",
						"abmiModule:pages/blank");
				folderPage.setProperty("title", folderName);
				folderPage.addNode("jumbotron", NodeTypes.Area.NAME);
				folderPage.addNode("titleArea", NodeTypes.Area.NAME);
				folderPage.addNode("footer", NodeTypes.Area.NAME);
				folderPage.addNode("fineprintFooter", NodeTypes.Area.NAME);

				folderPage.addNode("rightColumnArea", NodeTypes.Area.NAME);
				folderPage.addNode("rightWidgetColumnArea", NodeTypes.Area.NAME);
				folderPage.setProperty("mgnl:template",
						"abmiModule:pages/newpublicationpageNew");
				folderPage.setProperty("hideInNavigation", false);
			}
			else {
				folderPage = c.getNode(folderName);
			}
			if (folderPage.hasNode(uId)) {
				System.out
						.println("node "
								+ row.getUniqueFileNumber()
								+ " hass already exist and the action is not update, so skip");
				newPage = folderPage.getNode(uId);
				// newPage.setProperty("mgnl:template","abmiModule:pages/publicationpage");
			} else {
				System.out.println("node " + uId
						+ " doesn't exist, add it now!");
				newPage = folderPage.addNode(uId, NodeTypes.Page.NAME);
				newPage.addNode("megaMenuArea", NodeTypes.Area.NAME);
				newPage.addNode("jumbotron", NodeTypes.Area.NAME);
				newPage.addNode("titleArea", NodeTypes.Area.NAME);
				newPage.addNode("footer", NodeTypes.Area.NAME);
				newPage.addNode("fineprintFooter", NodeTypes.Area.NAME);

				newPage.addNode("rightColumnArea", NodeTypes.Area.NAME);
				newPage.addNode("rightWidgetColumnArea", NodeTypes.Area.NAME);
				newPage.setProperty("mgnl:template",
						"abmiModule:pages/newpublicationpageNew");
				newPage.setProperty("hideInNavigation", false);

			}

//			System.out.println("set properties" + pdfPath);
			newPage.setProperty("rootpath", rootPath);
			// setting the template & components details�
			newPage.setProperty("title", row.getFullTitle());
			
			newPage.setProperty("author", row.getAuthor());
			newPage.setProperty("abstract", row.getAbstract_());
			newPage.setProperty("center", row.getCentre());
			newPage.setProperty("contactperson", row.getContactPerson());
			newPage.setProperty("description", row.getDescription());
			newPage.setProperty("doi", row.getDoi());
			newPage.setProperty("fileextention", row.getFormat());
			newPage.setProperty("fileid", row.getUniqueFileNumber());
			String fileName = row.getFileName();
			// int lastDot = fileName.lastIndexOf(".");
			// if (lastDot != -1){
			// fileName = fileName.substring(0, lastDot-1);
			// }
			newPage.setProperty("filename", row.getFileName());
			newPage.setProperty("viewlink",  fileName);
			newPage.setProperty("fordisplay", row.getForDisplay());
			newPage.setProperty("flipbooklink", row.getExtraLink());
			newPage.setProperty("flipbooklinktext", row.getExtraLinkType());
			
			newPage.setProperty("version", row.getVersionstr());
			newPage.setProperty("versiongrouping", row.getGroupid());
			
//			System.out.println("set properties" + pdfPath + fileName);
			if (row.getImageurl() != null) {
				String imgFileName = row.getImageurl();
				// lastDot = imgFileName.lastIndexOf(".");
				// if (lastDot != -1){
				// imgFileName = imgFileName.substring(0, lastDot-1);
				// }
				//
				newPage.setProperty("imageUrl",  imgFileName);
			}

			if (row.getCoverimageurl() != null) {
				newPage.setProperty("coverImageUrl",
						 row.getCoverimageurl());
			}

		

			newPage.setProperty("issue", row.getIssue());
			newPage.setProperty("journal", row.getJournal());
			newPage.setProperty("legacynumber", row.getLegacyNumber());
			newPage.setProperty("pagesnumber", row.getNumberOfPages());
			newPage.setProperty("pagesrange", row.getPageRange());
			if (row.getPublishDate() != null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(row.getPublishDate());
				newPage.setProperty("publishdate", cal);
			} else {
				newPage.setProperty("publishdate", date);
			}
			newPage.setProperty("displaydate", row.getDisplayDate());

			newPage.setProperty("volume", row.getVolume());
		
			String documentType = "";
			String limitor = "";
			if (row.getDocumentType1() != null
					&& !"".equals(row.getDocumentType1())) {
				documentType += getKeyFromValue(row.getDocumentType1(), (HashMap<String,String>)publicationMeta.get("mainTypeMap")) ;
				limitor = ",";
			}

			if (row.getDocumentType2() != null
					&& !"".equals(row.getDocumentType2())) {
				documentType += limitor + getKeyFromValue(row.getDocumentType2(),(HashMap<String,String>)publicationMeta.get("mainTypeMap"));
			}

			newPage.setProperty("documentmaintype", documentType);

			String documentSubtype = "";
			if (row.getDocumentSubtype1() != null
					&& !"".equals(row.getDocumentSubtype1())) {
				documentSubtype += getKeyFromValue(row.getDocumentSubtype1(),(HashMap<String,String>)publicationMeta.get("documentMap"));
				limitor = ",";
			}

			if (row.getDocumentSubtype2() != null
					&& !"".equals(row.getDocumentSubtype2())) {
				documentSubtype += limitor + getKeyFromValue(row.getDocumentSubtype2(),(HashMap<String,String>)publicationMeta.get("documentMap"));
			}

			newPage.setProperty("documenttype", documentSubtype);
			
			String subject = "";
			limitor = "";
			if (row.getSubjectArea1() != null
					&& !"".equals(row.getSubjectArea1())) {
				subject += limitor + getKeyFromValue(row.getSubjectArea1(),(HashMap<String,String>)publicationMeta.get("subjectMap"));
				
				limitor = ",";
			}

			if (row.getSubjectArea2() != null
					&& !"".equals(row.getSubjectArea2())) {
				subject += limitor + getKeyFromValue(row.getSubjectArea2(),(HashMap<String,String>)publicationMeta.get("subjectMap"));
				limitor = ",";
			}

			if (row.getSubjectArea3() != null
					&& !"".equals(row.getSubjectArea3())) {
				subject += limitor + getKeyFromValue(row.getSubjectArea3(),(HashMap<String,String>)publicationMeta.get("subjectMap"));
				limitor = ",";
			}

			newPage.setProperty("docsubject", subject);

			String dockeyword = "";
			limitor = "";
			if (row.getKeyword1() != null && !"".equals(row.getKeyword1())) {
				dockeyword += limitor + row.getKeyword1();
				limitor = ",";
			}

			if (row.getKeyword2() != null && !"".equals(row.getKeyword2())) {
				dockeyword += limitor + row.getKeyword2();
				limitor = ",";
			}
			if (row.getKeyword3() != null && !"".equals(row.getKeyword3())) {
				dockeyword += limitor + row.getKeyword3();
				limitor = ",";
			}
			if (row.getKeyword4() != null && !"".equals(row.getKeyword4())) {
				dockeyword += limitor + row.getKeyword4();
				limitor = ",";
			}
			if (row.getKeyword5() != null && !"".equals(row.getKeyword5())) {
				dockeyword += limitor + row.getKeyword5();
				limitor = ",";
			}

			newPage.setProperty("dockeyword", dockeyword);
			newPage.setProperty("keyword", documentType + subject + dockeyword);

			/*
			 * Node keyword =
			 * newPage.addNode("keyword_contentnode",MgnlNodeTypes.
			 * NT_CONTENTNODE); Node keyword2 =
			 * newPage.addNode("keyword_content",MgnlNodeTypes.NT_CONTENT);
			 */

			/** supplementary reports **/
			List<TmpReloadPublication> supList = row.getTmpReloadPublications();
			if (supList != null && supList.size() > 0) {
				Node supNode;
				if (!newPage.hasNode("supplementalreportslink"))
					supNode = newPage.addNode("supplementalreportslink",
							NodeTypes.ContentNode.NAME);
				else
					supNode = newPage.getNode("supplementalreportslink");

				for (TmpReloadPublication singlePub : supList) {
					
					int tmpUniqueId = (int)singlePub.getUniqueFileNumber();
					int tmpRoundFifty = (tmpUniqueId / 50) * 50 + 1;

					log.debug("uniqueId ={} round to {}", uId, roundFifty);
					String subPathName = tmpRoundFifty + "-" + (tmpRoundFifty + 49);
					
					supNode.setProperty(singlePub.getUniqueFileNumber() + "",
							rootPath + "/" +subPathName + "/" + singlePub.getUniqueFileNumber());

				}
			}
			
			if (row.getTmpReloadPublication()!= null) {
				newPage.setProperty("hideInNavigation", true);
			}
			session.save();

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return true;

	}

	public static void generatePublicationPages(EntityManager em) {
		Map<String, Object> publicationMeta = TemplateUtil
				.getPublicationTags(true);
		String queryStr = "select p from TmpReloadPublication p";
		TypedQuery<TmpReloadPublication> query = em.createQuery(queryStr,
				TmpReloadPublication.class);
		for (TmpReloadPublication row : query.getResultList()) {
			createOnePublicationRecord(row, publicationMeta);
		}
		
		
		
	}

	
	public static String exportPublicationPage(String localRootPage) {

		if (localRootPage == null) {
			localRootPage = rootPath;
		}
		// create a temporary file
		FileWriter fw = null;
		BufferedWriter bw = null;
		PrintWriter out = null;
		System.out.println("System.getProperty(java.io.tmpdir) = "
				+ System.getProperty("java.io.tmpdir") + "\nlocalRootPage="
				+ localRootPage);
		File exportFile = null;
		try {
			exportFile = File.createTempFile("public_docMetadata", ".csv",
					new File(System.getProperty("java.io.tmpdir")));

			/* create file writer handler for storing raw data */
			fw = new FileWriter(exportFile);
			bw = new BufferedWriter(fw);
			out = new PrintWriter(bw);

			out.println("Unique File Number,Legacy Number,Abridged Title,Full Title,Version,Version Grouping,"
					+ "Description,Author(s),Publish Date ,	Display Date,Document Type 1,Document Type 2 (optional),Subject Area 1,"
					+ "Subject Area 2,Subject Area 3,Keyword 1,Keyword 2,Keyword 3,Keyword 4,Keyword 5,"
					+ "Concatenated File Name,File Extension Name,Multi-Versions,Centre,Contact Person,Abstract,Journal,Volume,Issue,Page Range,Number of Pages,doi*,For Display,Image");

			// read all publications from repository
			try {
				Session session = MgnlContext.getJCRSession("website");

				QueryManager manager = session.getWorkspace().getQueryManager();
				String queryString = "select * from [mgnl:page] as t where ISDESCENDANTNODE(["
						+ localRootPage
						+ "]) and [mgnl:template] ='abmiModule:pages/publicationpage' ";
				javax.jcr.query.Query query = manager.createQuery(queryString,
						javax.jcr.query.Query.JCR_SQL2);

				// query2.setOffset(startCount); not work
				// query2.setLimit(itemPerPage);
				QueryResult result = query.execute();

				NodeIterator nodeIter = result.getNodes();
				while (nodeIter != null && nodeIter.hasNext()) {

					Node node = (Node) nodeIter.next();
					out.print(readOnePublication(node));

				}

			} catch (LoginException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			try {
				// finish writing
				if (out != null)
					out.close();
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();

			} catch (IOException e3) {
				e3.printStackTrace();
			}
		}
		// write publications to the tmp file

		// save and close file.

		// return file name
		return (exportFile == null ? "emptyfile" : exportFile.getName());
	}

	static String readOnePublication(Node node) {
		//
		String content = "";
		content += PropertyUtil.getString(node, "fileid") + ",";
		content += "\"" + PropertyUtil.getString(node, "legacynumber", "")
				+ "\",";
		content += "\""
				+ PropertyUtil.getString(node, "imagetitle", "").replaceAll(
						"\"", "''") + "\",";
		content += "\""
				+ PropertyUtil.getString(node, "title", "").replaceAll("\"",
						"''") + "\",";
		content += "\"" + PropertyUtil.getString(node, "Version", "") + "\",";
		content += "\"" + PropertyUtil.getString(node, "versiongrouping", "")
				+ "\",";
		content += "\""
				+ PropertyUtil.getString(node, "description", "").replaceAll(
						"\"", "''") + "\",";
		content += "\"" + PropertyUtil.getString(node, "author", "") + "\",";
		content += "\"" + PropertyUtil.getString(node, "publishdate", "")
				+ "\",";
		content += "\"" + PropertyUtil.getString(node, "displaydate", "")
				+ "\",";

		ArrayList<String> documentType = TemplateUtil.getMultiFieldJSONValues(
				node, "documenttype");

		for (int i = 0; i < 2; i++) {
			content += "\"";
			if (documentType != null && documentType.size() >= i + 1) {
				content += documentType.get(i);
			}
			content += "\",";
		}

		ArrayList<String> docSubject = TemplateUtil.getMultiFieldJSONValues(
				node, "docsubject");

		for (int i = 0; i < 3; i++) {
			content += "\"";
			if (docSubject != null && docSubject.size() >= i + 1) {
				content += docSubject.get(i);
			}
			content += "\",";
		}

		ArrayList<String> docKeyword = TemplateUtil.getMultiFieldJSONValues(
				node, "dockeyword");

		for (int i = 0; i < 5; i++) {
			content += "\"";
			if (docKeyword != null && docKeyword.size() >= i + 1) {
				content += docKeyword.get(i);
			}
			content += "\",";
		}
		content += "\"" + PropertyUtil.getString(node, "viewlink", "") + "\",";
		content += "\"" + PropertyUtil.getString(node, "fileextetion", "")
				+ "\",";

		content += "\"" + PropertyUtil.getString(node, "ismultiversions", "")
				+ "\",";
		content += "\"" + PropertyUtil.getString(node, "center", "") + "\",";
		content += "\"" + PropertyUtil.getString(node, "contactperson", "")
				+ "\",";

		content += "\""
				+ PropertyUtil.getString(node, "abstract", "").replaceAll("\"",
						"''") + "\",";
		content += "\"" + PropertyUtil.getString(node, "journal", "") + "\",";
		content += "\"" + PropertyUtil.getString(node, "volume", "") + "\",";
		content += "\"" + PropertyUtil.getString(node, "issue", "") + "\",";
		content += "\"" + PropertyUtil.getString(node, "pagesrange", "")
				+ "\",";
		content += "\"" + PropertyUtil.getString(node, "pagesnumber", "")
				+ "\",";
		content += "\"" + PropertyUtil.getString(node, "doi", "") + "\",";
		content += "\"" + PropertyUtil.getString(node, "doidisplay", "")
				+ "\",";
		content += "\"" + PropertyUtil.getString(node, "imageUrl", "") + "\",";
		content += "\"" + PropertyUtil.getString(node, "coverImageUrl", "") + "\",";
		
		content += "\"";
		for (String row : TemplateUtil.emptyIfNull(TemplateUtil
				.getMultiFieldJSONValues(node, "supplementalreportslink"))) {

			content += row + "\n";

		}

		content += "\"\n";
		return content;
	}

	/**
	 * check to make sure supplementary documents are hidden as well as mark the
	 * newest version document as unhidden, and the rest older version documents
	 * are hidden
	 * 
	 * @return
	 */
	public static boolean updateSupplementaryVersionDocument(
			String localRootPage) {
		/**
		 * first, get all items that has supplementalreportslink, and then loop
		 * through them and get the values, and set the page with the link
		 * values as hidden /mgnl:contentNode select t from [mgnl:contentNode]
		 * as t where ISDESCENDANTNODE([/home/publications/]) and
		 * name(t)='supplementalreportslink'
		 */
		System.out.println("test if it is a author instance "
				+ Components.getComponent(TemplatingFunctions.class)
						.isAuthorInstance());
		
		if (localRootPage == null) {
			localRootPage = rootPath;
		}
		
		try {
			Session session = MgnlContext.getJCRSession("website");

			QueryManager manager = session.getWorkspace().getQueryManager();
			String queryString = "select * from [mgnl:contentNode] as t where ISDESCENDANTNODE(["
					+ localRootPage
					+ "]) and name(t)='supplementalreportslink' ";
			javax.jcr.query.Query query = manager.createQuery(queryString,
					javax.jcr.query.Query.JCR_SQL2);

			QueryResult result = query.execute();

			NodeIterator nodeIter = result.getNodes();
			while (nodeIter != null && nodeIter.hasNext()) {

				Node node = (Node) nodeIter.next();
				System.out.println("hide "
						+ TemplateUtil.getMultiFieldValues(node.getParent(),
								"supplementalreportslink"));
				for (String pathLink : TemplateUtil.emptyIfNull(TemplateUtil
						.getMultiFieldValues(node.getParent(),
								"supplementalreportslink"))) {
					System.out.println("hide " + pathLink);
					Node page = session.getNode(pathLink);
					page.setProperty("hideInNavigation", true);
				}

			}

			session.save();
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * check all version document, and hide old versions, and only show new
		 * versions.
		 * 
		 * select * from [mgnl:page] as t where
		 * ISDESCENDANTNODE([/home/publications/]) and versiongrouping is not
		 * null
		 */

		try {
			Session session = MgnlContext.getJCRSession("website");

			QueryManager manager = session.getWorkspace().getQueryManager();
			String queryString = "select * from [mgnl:page] as t where ISDESCENDANTNODE(["
					+ localRootPage + "]) and versiongrouping is not null ";
			javax.jcr.query.Query query = manager.createQuery(queryString,
					javax.jcr.query.Query.JCR_SQL2);

			QueryResult result = query.execute();

			NodeIterator nodeIter = result.getNodes();

			ArrayList<String> processedGroup = new ArrayList<String>();
			while (nodeIter != null && nodeIter.hasNext()) {

				Node node = (Node) nodeIter.next();
				String versionGroup = PropertyUtil.getString(node,
						"versiongrouping", "");
				if (processedGroup.contains(versionGroup)) { // if it has been
																// processed,
																// skip
					continue;
				}

				processedGroup.add(versionGroup);

				TemplateUtil.updateSingleVersion(session,
						 localRootPage,  versionGroup);

			}

			
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	
	static public int getKeyFromValue(String value, Map<String, String> map) {
		  for (Entry<String, String> entry : map.entrySet()) {
	            if (entry.getValue().endsWith(value)) {
	               return Integer.parseInt(entry.getKey());
	            }
	        }
		  
		  System.out.println("can' find " + value);
		  return -1;
	}
}
