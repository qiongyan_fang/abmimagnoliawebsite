package abmi.model.services.internal.profile;


import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.PropertyUtil;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import abmi.model.entity.biobrowser.WebSpeciesBrowser;

@Service
public class SpeciesProfileBatchDAO {

	// load species 
	static boolean createOneProfileRecord(WebSpeciesBrowser row

	) {
		Session session;

		
//		Calendar date = new GregorianCalendar(1990, 1, 1);
		try {
			session = MgnlContext.getJCRSession("profiles");
			String sppGroup = row.getSppGroup().getSgGroupName().replaceAll(" ", "");
			Node rootNode = session.getNode("/");
			if (!rootNode.hasNode(sppGroup)) {
				rootNode.addNode(sppGroup, NodeTypes.Folder.NAME);
				
			}
			
			Node rootNode2 = rootNode.getNode(sppGroup);
//			if (!rootNode2.hasNode("Full")) {
//				rootNode2.addNode("Full", MgnlNodeType.NT_FOLDER);
//				
//			}
//			
//			if (!rootNode2.hasNode("Basic")) {
//				rootNode2.addNode("Basic", MgnlNodeType.NT_FOLDER);
//				
//			}
//			
			
			Node c=	rootNode2; //.getNode(row.getWsbSummaryType().equals("TRUE")?"Full":"Basic");
				
			
			
			Node newPage = null;
			String uId = "";
			if ((row.getSppGroup().getSgGroupName().equals("Birds") ||
					row.getSppGroup().getSgGroupName().equals("Mammals")) && row.getWsbCommonName() != null
					) {
				uId = String.valueOf(row.getWsbCommonName().replaceAll(" ",""));
			}
			else {
				uId = String.valueOf(row.getWsbScientificName().replaceAll(" ",""));
			}
			
			String indexLetter = uId.substring(0, 1);
			if (!c.hasNode(indexLetter)) {
				c.addNode(indexLetter, NodeTypes.Folder.NAME);
				
			}
			
			Node subFolder =	c.getNode(indexLetter);

			if (subFolder.hasNode(uId)) {
				System.out
						.println("node "
								+ uId
								+ " hass already exist and the action is not update, so skip");
				newPage = subFolder.getNode(uId);
				// newPage.setProperty("mgnl:template","abmiModule:pages/publicationpage");
			} else {
				System.out.println("node " + uId
						+ " doesn't exist, add it now!");
				newPage = subFolder.addNode(uId, NodeTypes.MGNL_PREFIX + "profile");
			

			}

			
			
			// setting the template & components details�
			newPage.setProperty("TSN", row.getWsbTsn());
			newPage.setProperty("CommonName", row.getWsbCommonName());
			newPage.setProperty("ScientificName", row.getWsbScientificName());
			newPage.setProperty("SummaryType", row.getSppSummaryType().getSstDisplay());
			newPage.setProperty("SpeciesGroup", row.getSppGroup().getSgGroupName());
			
			session.save();

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}

		return true;

	}

	public static void generatePublicationPages(EntityManager em) {

		String queryStr = "select p from WebSpeciesBrowser p where p.wsbGroupName is not null and p.wsbSummaryType is not null and (p.wsbGroupName = 'Birds' or p.wsbGroupName = 'Mammals')order by  p.wsbCommonName";
		TypedQuery<WebSpeciesBrowser> query = em.createQuery(queryStr,
				WebSpeciesBrowser.class);
		for (WebSpeciesBrowser row : query.getResultList()) {
			createOneProfileRecord(row);
		}
	

	 queryStr = "select p from WebSpeciesBrowser p where p.wsbGroupName is not null and p.wsbSummaryType is not null and not (p.wsbGroupName = 'Birds' or p.wsbGroupName = 'Mammals')order by p.wsbScientificName";
	query = em.createQuery(queryStr,
			WebSpeciesBrowser.class);
	for (WebSpeciesBrowser row : query.getResultList()) {
		createOneProfileRecord(row);
	}
}

	
	static public void batchUpdateStatus() {
		Session session;

		try {
			session = MgnlContext.getJCRSession("profiles");
			QueryManager manager = session.getWorkspace()
					.getQueryManager();

			String queryString = "select * from [mgnl:profile] as t where QAStatus is null";
			javax.jcr.query.Query query = manager.createQuery(queryString,
					javax.jcr.query.Query.JCR_SQL2);

			QueryResult result = query.execute();

			NodeIterator nodeIter = result.getNodes();
			while (nodeIter != null && nodeIter.hasNext()) {

				Node node = (Node) nodeIter.next();
				PropertyUtil.setProperty(
						node,
						"QAStatus", "Unassigned");
			}
			session.save();
			
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
}
