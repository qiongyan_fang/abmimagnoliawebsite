package abmi.model.services.internal.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TMP_LOAD_PUBLICATIONS database table.
 * 
 */
@Entity
@Table(schema="METADATA", name="TMP_LOAD_PUBLICATIONS")
public class TmpLoadPublication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="UNIQUE_FILE_NUMBER")
	private long uniqueFileNumber;

	@Column(name="ABRIDGED_TITLE")
	private String abridgedTitle;

	@Column(name="ABSTRACT")
	private String abstract_;

	@Column(name="\"ACTION\"")
	private String action;

	private String author;

	private String centre;

	@Column(name="CONTACT_PERSON")
	private String contactPerson;

	private String copyright;

	private String coverimageurl;

	private String description;

	@Column(name="DISPLAY_DATE")
	private String displayDate;

	@Column(name="DOCUMENT_TYPE_1")
	private String documentType1;

	@Column(name="DOCUMENT_TYPE_2")
	private String documentType2;

	private String doi;

	@Column(name="FILE_NAME")
	private String fileName;

	@Column(name="FOR_DISPLAY")
	private String forDisplay;

	private String format;

	@Column(name="FULL_TITLE")
	private String fullTitle;

	private String groupid;

	private String image;

	private String imageurl;

	private String issue;

	private String journal;

	@Column(name="KEYWORD_1")
	private String keyword1;

	@Column(name="KEYWORD_2")
	private String keyword2;

	@Column(name="KEYWORD_3")
	private String keyword3;

	@Column(name="KEYWORD_4")
	private String keyword4;

	@Column(name="KEYWORD_5")
	private String keyword5;

	@Column(name="LEGACY_NUMBER")
	private String legacyNumber;

	private String multiversions;

	@Column(name="NUMBER_OF_PAGES")
	private String numberOfPages;

	private String orgimagename;

	@Column(name="PAGE_RANGE")
	private String pageRange;

	@Temporal(TemporalType.DATE)
	@Column(name="PUBLISH_DATE")
	private Date publishDate;

	@Column(name="SUBJECT_AREA_1")
	private String subjectArea1;

	@Column(name="SUBJECT_AREA_2")
	private String subjectArea2;

	@Column(name="SUBJECT_AREA_3")
	private String subjectArea3;

	private String versionstr;

	private String volume;

	//bi-directional many-to-one association to TmpLoadPublication
	@ManyToOne
	@JoinColumn(name="SUPPGROUPID")
	private TmpLoadPublication tmpLoadPublication;

	//bi-directional many-to-one association to TmpLoadPublication
	@OneToMany(mappedBy="tmpLoadPublication")
	private List<TmpLoadPublication> tmpLoadPublications;

	public TmpLoadPublication() {
	}

	public long getUniqueFileNumber() {
		return this.uniqueFileNumber;
	}

	public void setUniqueFileNumber(long uniqueFileNumber) {
		this.uniqueFileNumber = uniqueFileNumber;
	}

	public String getAbridgedTitle() {
		return this.abridgedTitle;
	}

	public void setAbridgedTitle(String abridgedTitle) {
		this.abridgedTitle = abridgedTitle;
	}

	public String getAbstract_() {
		return this.abstract_;
	}

	public void setAbstract_(String abstract_) {
		this.abstract_ = abstract_;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCentre() {
		return this.centre;
	}

	public void setCentre(String centre) {
		this.centre = centre;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getCopyright() {
		return this.copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public String getCoverimageurl() {
		return this.coverimageurl;
	}

	public void setCoverimageurl(String coverimageurl) {
		this.coverimageurl = coverimageurl;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayDate() {
		return this.displayDate;
	}

	public void setDisplayDate(String displayDate) {
		this.displayDate = displayDate;
	}

	public String getDocumentType1() {
		return this.documentType1;
	}

	public void setDocumentType1(String documentType1) {
		this.documentType1 = documentType1;
	}

	public String getDocumentType2() {
		return this.documentType2;
	}

	public void setDocumentType2(String documentType2) {
		this.documentType2 = documentType2;
	}

	public String getDoi() {
		return this.doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getForDisplay() {
		return this.forDisplay;
	}

	public void setForDisplay(String forDisplay) {
		this.forDisplay = forDisplay;
	}

	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getFullTitle() {
		return this.fullTitle;
	}

	public void setFullTitle(String fullTitle) {
		this.fullTitle = fullTitle;
	}

	public String getGroupid() {
		return this.groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImageurl() {
		return this.imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getIssue() {
		return this.issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public String getJournal() {
		return this.journal;
	}

	public void setJournal(String journal) {
		this.journal = journal;
	}

	public String getKeyword1() {
		return this.keyword1;
	}

	public void setKeyword1(String keyword1) {
		this.keyword1 = keyword1;
	}

	public String getKeyword2() {
		return this.keyword2;
	}

	public void setKeyword2(String keyword2) {
		this.keyword2 = keyword2;
	}

	public String getKeyword3() {
		return this.keyword3;
	}

	public void setKeyword3(String keyword3) {
		this.keyword3 = keyword3;
	}

	public String getKeyword4() {
		return this.keyword4;
	}

	public void setKeyword4(String keyword4) {
		this.keyword4 = keyword4;
	}

	public String getKeyword5() {
		return this.keyword5;
	}

	public void setKeyword5(String keyword5) {
		this.keyword5 = keyword5;
	}

	public String getLegacyNumber() {
		return this.legacyNumber;
	}

	public void setLegacyNumber(String legacyNumber) {
		this.legacyNumber = legacyNumber;
	}

	public String getMultiversions() {
		return this.multiversions;
	}

	public void setMultiversions(String multiversions) {
		this.multiversions = multiversions;
	}

	public String getNumberOfPages() {
		return this.numberOfPages;
	}

	public void setNumberOfPages(String numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public String getOrgimagename() {
		return this.orgimagename;
	}

	public void setOrgimagename(String orgimagename) {
		this.orgimagename = orgimagename;
	}

	public String getPageRange() {
		return this.pageRange;
	}

	public void setPageRange(String pageRange) {
		this.pageRange = pageRange;
	}

	public Date getPublishDate() {
		return this.publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getSubjectArea1() {
		return this.subjectArea1;
	}

	public void setSubjectArea1(String subjectArea1) {
		this.subjectArea1 = subjectArea1;
	}

	public String getSubjectArea2() {
		return this.subjectArea2;
	}

	public void setSubjectArea2(String subjectArea2) {
		this.subjectArea2 = subjectArea2;
	}

	public String getSubjectArea3() {
		return this.subjectArea3;
	}

	public void setSubjectArea3(String subjectArea3) {
		this.subjectArea3 = subjectArea3;
	}

	public String getVersionstr() {
		return this.versionstr;
	}

	public void setVersionstr(String versionstr) {
		this.versionstr = versionstr;
	}

	public String getVolume() {
		return this.volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public TmpLoadPublication getTmpLoadPublication() {
		return this.tmpLoadPublication;
	}

	public void setTmpLoadPublication(TmpLoadPublication tmpLoadPublication) {
		this.tmpLoadPublication = tmpLoadPublication;
	}

	public List<TmpLoadPublication> getTmpLoadPublications() {
		return this.tmpLoadPublications;
	}

	public void setTmpLoadPublications(List<TmpLoadPublication> tmpLoadPublications) {
		this.tmpLoadPublications = tmpLoadPublications;
	}

}